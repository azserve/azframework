package com.azserve.framework.test.ejb.beans.manager;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class TestEAO {

	@PersistenceContext
	private EntityManager em;

	@Resource
	private SessionContext sessionContext;

	public EntityManager getEntityManager() {
		return this.em;
	}

	public SessionContext getSessionContext() {
		return this.sessionContext;
	}

	public <T> void insert(final T entity) {
		this.getEntityManager().persist(entity);
	}

}
