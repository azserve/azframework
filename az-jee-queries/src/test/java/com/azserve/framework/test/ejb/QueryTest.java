package com.azserve.framework.test.ejb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.azserve.azframework.query.Expressions;
import com.azserve.azframework.query.Queries;
import com.azserve.azframework.query.criteria.IRoot;
import com.azserve.framework.test.ejb.beans.Gender;
import com.azserve.framework.test.ejb.beans.entity.Department;
import com.azserve.framework.test.ejb.beans.entity.DepartmentEmployee;
import com.azserve.framework.test.ejb.beans.entity.DepartmentEmployee_;
import com.azserve.framework.test.ejb.beans.entity.Department_;
import com.azserve.framework.test.ejb.beans.entity.Employee;
import com.azserve.framework.test.ejb.beans.manager.TestEAO;

//@RunWith(Arquillian.class)
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SuppressWarnings("static-method")
public class QueryTest extends Deployments {

	private final List<Department> departments = Arrays.asList(
			new Department("A", "Audio"),
			new Department("A1", "Audio"),
			new Department("B", "Bath"),
			new Department("H", "Home"),
			new Department("V", "Videogames"));

	// @Test
	public void aaaPrepare(final TestSessionBean sb, final TestEAO eao) {
		sb.clearQueryTestData();
		for (final Department department : this.departments) {
			eao.insert(department);
		}
		for (int i = 0; i < 100; i++) {
			final Employee employee = new Employee(UUID.randomUUID(), "firstName" + i, "lastName" + i, Gender.MALE, new Date(), new Date());
			eao.insert(employee);
			eao.insert(new DepartmentEmployee(Integer.valueOf(i),
					this.departments.get(i / (100 / this.departments.size())),
					employee,
					new Date(),
					new Date()));
		}
	}

	// @Test
	public void test1_FetchSimple(final TestEAO eao) {

		assertEquals(Queries.from(eao.getEntityManager(), Department.class).fetch().size(), this.departments.size());
		assertTrue(Queries.from(eao.getEntityManager(), Department.class).fetchFirst().isPresent());
	}

	// @Test
	public void test2_Eq(final TestEAO eao) {

		final Optional<Department> departmentA = Queries.from(eao.getEntityManager(), Department.class).eq(Department_.id, "A").fetchFirst();
		assertTrue(departmentA.isPresent() && "A".equals(departmentA.get().getId()));

		assertFalse(Queries.from(eao.getEntityManager(), Department.class).eq(Department_.id, "CIAO").fetchFirst().isPresent());
	}

	// @Test
	public void test3_SelectFrom(final TestEAO eao) {

		final IRoot<Department> root = Expressions.from(Department.class);
		final IRoot<DepartmentEmployee> root2 = Expressions.from(DepartmentEmployee.class);
		assertEquals(Queries.create(eao.getEntityManager()).select(Department.class, root).from(root).from(root2)
				.where(Expressions.equal(root, root2.get(DepartmentEmployee_.department))).fetch().size(), this.departments.size());
	}

	// @Test
	public void test4_Where(final TestEAO eao) {
		final IRoot<Department> root = Expressions.from(Department.class);

		final List<Department> departmentsB = Queries.create(eao.getEntityManager())
				.select(Department.class, root)
				.from(root)
				.where(Expressions.equal(root.get(Department_.id), "B")).fetch();
		assertTrue(departmentsB.size() == 1 && "B".equals(departmentsB.get(0).getId()));
	}

	// private static class TestEntity {}
}
