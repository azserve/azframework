package com.azserve.framework.test.ejb.beans.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.azserve.azframework.entity.EntityWithInteger;

@Entity
public class DepartmentEmployee extends EntityWithInteger {

	private static final long serialVersionUID = 2639622031787912162L;

	@Id
	private Integer id;
	@ManyToOne
	private Department department;
	@ManyToOne
	private Employee employee;
	private Date fromDate;
	private Date toDate;

	public DepartmentEmployee(final Integer id, final Department department, final Employee employee, final Date fromDate, final Date toDate) {
		this.id = id;
		this.department = department;
		this.employee = employee;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	public DepartmentEmployee() {}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(final Department department) {
		this.department = department;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(final Employee employee) {
		this.employee = employee;
	}

	public Date getFrom() {
		return this.fromDate;
	}

	public void setFrom(final Date from) {
		this.fromDate = from;
	}

	public Date getTo() {
		return this.toDate;
	}

	public void setTo(final Date to) {
		this.toDate = to;
	}

}
