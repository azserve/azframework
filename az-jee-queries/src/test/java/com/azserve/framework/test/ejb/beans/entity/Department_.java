package com.azserve.framework.test.ejb.beans.entity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Department.class)
public abstract class Department_ {

	public static volatile SingularAttribute<Department, String> id;
	public static volatile SingularAttribute<Department, String> name;
}