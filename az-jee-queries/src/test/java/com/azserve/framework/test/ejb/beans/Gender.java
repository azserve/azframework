package com.azserve.framework.test.ejb.beans;


import com.azserve.azframework.common.enums.EnumWithKey;

public enum Gender implements EnumWithKey<String> {

	MALE("M"),
	FEMALE("F");

	private String key;

	private Gender(final String key) {
		this.key = key;
	}

	@Override
	public String key() {
		return this.key;
	}
}
