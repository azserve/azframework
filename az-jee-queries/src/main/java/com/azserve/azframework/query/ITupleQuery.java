package com.azserve.azframework.query;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import javax.ejb.SessionContext;
import javax.persistence.Tuple;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.query.criteria.IFrom;
import com.azserve.azframework.query.criteria.IRoot;
import com.azserve.azframework.query.criteria.ISelection;

public interface ITupleQuery<F> extends IQuery<ITupleQuery<F>, F> {

	<T> ITupleQuery<T> from(IRoot<T> from, IFrom<?, ?>... froms);

	default <T> ITupleQuery<T> from(final Class<T> fromClass) {
		return this.from(Expressions.from(fromClass));
	}

	<R> List<R> fetch(TupleMapper<R> mapper);

	void fetchAndThen(TupleConsumer consumer);

	<R> R fetchAndThenMap(BiFunction<Stream<Tuple>, TupleResolver, R> mapper);

	<R> Optional<R> fetchFirst(TupleMapper<R> mapper);

	<R> Stream<R> stream(TupleMapper<R> mapper);

	<R> Optional<List<R>> fetchAsync(SessionContext sessionContext, TupleMapper<R> mapper);

	void fetchAsyncAndThen(SessionContext sessionContext, TupleConsumer consumer);

	public interface TupleMapper<R> {

		R map(@NonNull Tuple tuple, @NonNull TupleResolver resolver);
	}

	public interface TupleConsumer {

		void accept(@NonNull Tuple tuple, @NonNull TupleResolver resolver);
	}

	public interface TupleResolver {

		<T> T get(@NonNull Tuple tuple, @NonNull ISelection<T> selection);
	}
}