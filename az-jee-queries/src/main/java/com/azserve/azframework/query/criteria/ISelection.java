package com.azserve.azframework.query.criteria;

import javax.persistence.criteria.Selection;

import com.azserve.azframework.query.IQueryDataHolder;

@FunctionalInterface
public interface ISelection<T> {

	Selection<T> resolve(IQueryDataHolder data);
}
