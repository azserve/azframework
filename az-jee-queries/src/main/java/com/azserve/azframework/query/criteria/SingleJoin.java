package com.azserve.azframework.query.criteria;

import java.util.Objects;

import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.metamodel.SingularAttribute;

import com.azserve.azframework.query.IQueryDataHolder;

class SingleJoin<S, T> implements IJoin<S, T> {

	private final IFrom<?, S> parent;
	private final SingularAttribute<? super S, T> attribute;
	private final JoinType joinType;

	SingleJoin(final IFrom<?, S> parent, final SingularAttribute<? super S, T> attribute, final JoinType joinType) {
		this.parent = parent;
		this.attribute = attribute;
		this.joinType = joinType;
	}

	@Override
	public Join<S, T> create(final IQueryDataHolder data, final AbstractQuery<?> criteriaQuery) {
		return this.create(this.parent.resolve(data));
	}

	private Join<S, T> create(final From<?, S> from) {
		return from.join(this.attribute, this.joinType);
	}

	@Override
	public Join<S, T> resolve(final IQueryDataHolder data) {
		final Join<S, T> foundJoin = (Join<S, T>) data.getFrom(this);
		if (foundJoin != null) {
			return foundJoin;
		}

		final From<?, S> from = Objects.requireNonNull(
				this.parent.resolve(data),
				() -> "Cannot resolve parent join for " + this.attribute);

		final Join<S, T> join = this.create(from);
		data.registerJoin(this, join);
		return join;
	}
}