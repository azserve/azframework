package com.azserve.azframework.query;

import java.util.stream.Collector;

public final class TupleCollectors {

	public static <T, A1, A2, D1, D2> Collector<T, Tuple2<A1, A2>, Tuple2<D1, D2>> collectors(
			final Collector<? super T, A1, D1> collector1,
			final Collector<? super T, A2, D2> collector2) {
		return Collector.<T, Tuple2<A1, A2>, Tuple2<D1, D2>> of(
				() -> new Tuple2<>(
						collector1.supplier().get(),
						collector2.supplier().get()),
				(tuple, t) -> {
					collector1.accumulator().accept(tuple._1, t);
					collector2.accumulator().accept(tuple._2, t);
				},
				(tuple1, tuple2) -> new Tuple2<>(
						collector1.combiner().apply(tuple1._1, tuple2._1),
						collector2.combiner().apply(tuple1._2, tuple2._2)),
				tuple -> new Tuple2<>(
						collector1.finisher().apply(tuple._1),
						collector2.finisher().apply(tuple._2)));
	}

	public static <T, A1, A2, A3, D1, D2, D3> Collector<T, Tuple3<A1, A2, A3>, Tuple3<D1, D2, D3>> collectors(
			final Collector<? super T, A1, D1> collector1,
			final Collector<? super T, A2, D2> collector2,
			final Collector<? super T, A3, D3> collector3) {
		return Collector.<T, Tuple3<A1, A2, A3>, Tuple3<D1, D2, D3>> of(
				() -> new Tuple3<>(
						collector1.supplier().get(),
						collector2.supplier().get(),
						collector3.supplier().get()),
				(tuple, t) -> {
					collector1.accumulator().accept(tuple._1, t);
					collector2.accumulator().accept(tuple._2, t);
					collector3.accumulator().accept(tuple._3, t);
				},
				(tuple1, tuple2) -> new Tuple3<>(
						collector1.combiner().apply(tuple1._1, tuple2._1),
						collector2.combiner().apply(tuple1._2, tuple2._2),
						collector3.combiner().apply(tuple1._3, tuple2._3)),
				tuple -> new Tuple3<>(
						collector1.finisher().apply(tuple._1),
						collector2.finisher().apply(tuple._2),
						collector3.finisher().apply(tuple._3)));
	}

	public static <T, A1, A2, A3, A4, D1, D2, D3, D4> Collector<T, Tuple4<A1, A2, A3, A4>, Tuple4<D1, D2, D3, D4>> collectors(
			final Collector<? super T, A1, D1> collector1,
			final Collector<? super T, A2, D2> collector2,
			final Collector<? super T, A3, D3> collector3,
			final Collector<? super T, A4, D4> collector4) {
		return Collector.<T, Tuple4<A1, A2, A3, A4>, Tuple4<D1, D2, D3, D4>> of(
				() -> new Tuple4<>(
						collector1.supplier().get(),
						collector2.supplier().get(),
						collector3.supplier().get(),
						collector4.supplier().get()),
				(tuple, t) -> {
					collector1.accumulator().accept(tuple._1, t);
					collector2.accumulator().accept(tuple._2, t);
					collector3.accumulator().accept(tuple._3, t);
					collector4.accumulator().accept(tuple._4, t);
				},
				(tuple1, tuple2) -> new Tuple4<>(
						collector1.combiner().apply(tuple1._1, tuple2._1),
						collector2.combiner().apply(tuple1._2, tuple2._2),
						collector3.combiner().apply(tuple1._3, tuple2._3),
						collector4.combiner().apply(tuple1._4, tuple2._4)),
				tuple -> new Tuple4<>(
						collector1.finisher().apply(tuple._1),
						collector2.finisher().apply(tuple._2),
						collector3.finisher().apply(tuple._3),
						collector4.finisher().apply(tuple._4)));
	}

	public static <T, A1, A2, A3, A4, A5, D1, D2, D3, D4, D5> Collector<T, Tuple5<A1, A2, A3, A4, A5>, Tuple5<D1, D2, D3, D4, D5>> collectors(
			final Collector<? super T, A1, D1> collector1,
			final Collector<? super T, A2, D2> collector2,
			final Collector<? super T, A3, D3> collector3,
			final Collector<? super T, A4, D4> collector4,
			final Collector<? super T, A5, D5> collector5) {
		return Collector.<T, Tuple5<A1, A2, A3, A4, A5>, Tuple5<D1, D2, D3, D4, D5>> of(
				() -> new Tuple5<>(
						collector1.supplier().get(),
						collector2.supplier().get(),
						collector3.supplier().get(),
						collector4.supplier().get(),
						collector5.supplier().get()),
				(tuple, t) -> {
					collector1.accumulator().accept(tuple._1, t);
					collector2.accumulator().accept(tuple._2, t);
					collector3.accumulator().accept(tuple._3, t);
					collector4.accumulator().accept(tuple._4, t);
					collector5.accumulator().accept(tuple._5, t);
				},
				(tuple1, tuple2) -> new Tuple5<>(
						collector1.combiner().apply(tuple1._1, tuple2._1),
						collector2.combiner().apply(tuple1._2, tuple2._2),
						collector3.combiner().apply(tuple1._3, tuple2._3),
						collector4.combiner().apply(tuple1._4, tuple2._4),
						collector5.combiner().apply(tuple1._5, tuple2._5)),
				tuple -> new Tuple5<>(
						collector1.finisher().apply(tuple._1),
						collector2.finisher().apply(tuple._2),
						collector3.finisher().apply(tuple._3),
						collector4.finisher().apply(tuple._4),
						collector5.finisher().apply(tuple._5)));
	}

	public static <T, A1, A2, A3, A4, A5, A6, D1, D2, D3, D4, D5, D6> Collector<T, Tuple6<A1, A2, A3, A4, A5, A6>, Tuple6<D1, D2, D3, D4, D5, D6>> collectors(
			final Collector<? super T, A1, D1> collector1,
			final Collector<? super T, A2, D2> collector2,
			final Collector<? super T, A3, D3> collector3,
			final Collector<? super T, A4, D4> collector4,
			final Collector<? super T, A5, D5> collector5,
			final Collector<? super T, A6, D6> collector6) {
		return Collector.<T, Tuple6<A1, A2, A3, A4, A5, A6>, Tuple6<D1, D2, D3, D4, D5, D6>> of(
				() -> new Tuple6<>(
						collector1.supplier().get(),
						collector2.supplier().get(),
						collector3.supplier().get(),
						collector4.supplier().get(),
						collector5.supplier().get(),
						collector6.supplier().get()),
				(tuple, t) -> {
					collector1.accumulator().accept(tuple._1, t);
					collector2.accumulator().accept(tuple._2, t);
					collector3.accumulator().accept(tuple._3, t);
					collector4.accumulator().accept(tuple._4, t);
					collector5.accumulator().accept(tuple._5, t);
					collector6.accumulator().accept(tuple._6, t);
				},
				(tuple1, tuple2) -> new Tuple6<>(
						collector1.combiner().apply(tuple1._1, tuple2._1),
						collector2.combiner().apply(tuple1._2, tuple2._2),
						collector3.combiner().apply(tuple1._3, tuple2._3),
						collector4.combiner().apply(tuple1._4, tuple2._4),
						collector5.combiner().apply(tuple1._5, tuple2._5),
						collector6.combiner().apply(tuple1._6, tuple2._6)),
				tuple -> new Tuple6<>(
						collector1.finisher().apply(tuple._1),
						collector2.finisher().apply(tuple._2),
						collector3.finisher().apply(tuple._3),
						collector4.finisher().apply(tuple._4),
						collector5.finisher().apply(tuple._5),
						collector6.finisher().apply(tuple._6)));
	}

	public static <T, A1, A2, A3, A4, A5, A6, A7, D1, D2, D3, D4, D5, D6, D7> Collector<T, Tuple7<A1, A2, A3, A4, A5, A6, A7>, Tuple7<D1, D2, D3, D4, D5, D6, D7>> collectors(
			final Collector<? super T, A1, D1> collector1,
			final Collector<? super T, A2, D2> collector2,
			final Collector<? super T, A3, D3> collector3,
			final Collector<? super T, A4, D4> collector4,
			final Collector<? super T, A5, D5> collector5,
			final Collector<? super T, A6, D6> collector6,
			final Collector<? super T, A7, D7> collector7) {
		return Collector.<T, Tuple7<A1, A2, A3, A4, A5, A6, A7>, Tuple7<D1, D2, D3, D4, D5, D6, D7>> of(
				() -> new Tuple7<>(
						collector1.supplier().get(),
						collector2.supplier().get(),
						collector3.supplier().get(),
						collector4.supplier().get(),
						collector5.supplier().get(),
						collector6.supplier().get(),
						collector7.supplier().get()),
				(tuple, t) -> {
					collector1.accumulator().accept(tuple._1, t);
					collector2.accumulator().accept(tuple._2, t);
					collector3.accumulator().accept(tuple._3, t);
					collector4.accumulator().accept(tuple._4, t);
					collector5.accumulator().accept(tuple._5, t);
					collector6.accumulator().accept(tuple._6, t);
					collector7.accumulator().accept(tuple._7, t);
				},
				(tuple1, tuple2) -> new Tuple7<>(
						collector1.combiner().apply(tuple1._1, tuple2._1),
						collector2.combiner().apply(tuple1._2, tuple2._2),
						collector3.combiner().apply(tuple1._3, tuple2._3),
						collector4.combiner().apply(tuple1._4, tuple2._4),
						collector5.combiner().apply(tuple1._5, tuple2._5),
						collector6.combiner().apply(tuple1._6, tuple2._6),
						collector7.combiner().apply(tuple1._7, tuple2._7)),
				tuple -> new Tuple7<>(
						collector1.finisher().apply(tuple._1),
						collector2.finisher().apply(tuple._2),
						collector3.finisher().apply(tuple._3),
						collector4.finisher().apply(tuple._4),
						collector5.finisher().apply(tuple._5),
						collector6.finisher().apply(tuple._6),
						collector7.finisher().apply(tuple._7)));
	}

	public static <T, A1, A2, A3, A4, A5, A6, A7, A8, D1, D2, D3, D4, D5, D6, D7, D8> Collector<T, Tuple8<A1, A2, A3, A4, A5, A6, A7, A8>, Tuple8<D1, D2, D3, D4, D5, D6, D7, D8>> collectors(
			final Collector<? super T, A1, D1> collector1,
			final Collector<? super T, A2, D2> collector2,
			final Collector<? super T, A3, D3> collector3,
			final Collector<? super T, A4, D4> collector4,
			final Collector<? super T, A5, D5> collector5,
			final Collector<? super T, A6, D6> collector6,
			final Collector<? super T, A7, D7> collector7,
			final Collector<? super T, A8, D8> collector8) {
		return Collector.<T, Tuple8<A1, A2, A3, A4, A5, A6, A7, A8>, Tuple8<D1, D2, D3, D4, D5, D6, D7, D8>> of(
				() -> new Tuple8<>(
						collector1.supplier().get(),
						collector2.supplier().get(),
						collector3.supplier().get(),
						collector4.supplier().get(),
						collector5.supplier().get(),
						collector6.supplier().get(),
						collector7.supplier().get(),
						collector8.supplier().get()),
				(tuple, t) -> {
					collector1.accumulator().accept(tuple._1, t);
					collector2.accumulator().accept(tuple._2, t);
					collector3.accumulator().accept(tuple._3, t);
					collector4.accumulator().accept(tuple._4, t);
					collector5.accumulator().accept(tuple._5, t);
					collector6.accumulator().accept(tuple._6, t);
					collector7.accumulator().accept(tuple._7, t);
					collector8.accumulator().accept(tuple._8, t);
				},
				(tuple1, tuple2) -> new Tuple8<>(
						collector1.combiner().apply(tuple1._1, tuple2._1),
						collector2.combiner().apply(tuple1._2, tuple2._2),
						collector3.combiner().apply(tuple1._3, tuple2._3),
						collector4.combiner().apply(tuple1._4, tuple2._4),
						collector5.combiner().apply(tuple1._5, tuple2._5),
						collector6.combiner().apply(tuple1._6, tuple2._6),
						collector7.combiner().apply(tuple1._7, tuple2._7),
						collector8.combiner().apply(tuple1._8, tuple2._8)),
				tuple -> new Tuple8<>(
						collector1.finisher().apply(tuple._1),
						collector2.finisher().apply(tuple._2),
						collector3.finisher().apply(tuple._3),
						collector4.finisher().apply(tuple._4),
						collector5.finisher().apply(tuple._5),
						collector6.finisher().apply(tuple._6),
						collector7.finisher().apply(tuple._7),
						collector8.finisher().apply(tuple._8)));
	}

	public static <T, A1, A2, A3, A4, A5, A6, A7, A8, A9, D1, D2, D3, D4, D5, D6, D7, D8, D9> Collector<T, Tuple9<A1, A2, A3, A4, A5, A6, A7, A8, A9>, Tuple9<D1, D2, D3, D4, D5, D6, D7, D8, D9>> collectors(
			final Collector<? super T, A1, D1> collector1,
			final Collector<? super T, A2, D2> collector2,
			final Collector<? super T, A3, D3> collector3,
			final Collector<? super T, A4, D4> collector4,
			final Collector<? super T, A5, D5> collector5,
			final Collector<? super T, A6, D6> collector6,
			final Collector<? super T, A7, D7> collector7,
			final Collector<? super T, A8, D8> collector8,
			final Collector<? super T, A9, D9> collector9) {
		return Collector.<T, Tuple9<A1, A2, A3, A4, A5, A6, A7, A8, A9>, Tuple9<D1, D2, D3, D4, D5, D6, D7, D8, D9>> of(
				() -> new Tuple9<>(
						collector1.supplier().get(),
						collector2.supplier().get(),
						collector3.supplier().get(),
						collector4.supplier().get(),
						collector5.supplier().get(),
						collector6.supplier().get(),
						collector7.supplier().get(),
						collector8.supplier().get(),
						collector9.supplier().get()),
				(tuple, t) -> {
					collector1.accumulator().accept(tuple._1, t);
					collector2.accumulator().accept(tuple._2, t);
					collector3.accumulator().accept(tuple._3, t);
					collector4.accumulator().accept(tuple._4, t);
					collector5.accumulator().accept(tuple._5, t);
					collector6.accumulator().accept(tuple._6, t);
					collector7.accumulator().accept(tuple._7, t);
					collector8.accumulator().accept(tuple._8, t);
					collector9.accumulator().accept(tuple._9, t);
				},
				(tuple1, tuple2) -> new Tuple9<>(
						collector1.combiner().apply(tuple1._1, tuple2._1),
						collector2.combiner().apply(tuple1._2, tuple2._2),
						collector3.combiner().apply(tuple1._3, tuple2._3),
						collector4.combiner().apply(tuple1._4, tuple2._4),
						collector5.combiner().apply(tuple1._5, tuple2._5),
						collector6.combiner().apply(tuple1._6, tuple2._6),
						collector7.combiner().apply(tuple1._7, tuple2._7),
						collector8.combiner().apply(tuple1._8, tuple2._8),
						collector9.combiner().apply(tuple1._9, tuple2._9)),
				tuple -> new Tuple9<>(
						collector1.finisher().apply(tuple._1),
						collector2.finisher().apply(tuple._2),
						collector3.finisher().apply(tuple._3),
						collector4.finisher().apply(tuple._4),
						collector5.finisher().apply(tuple._5),
						collector6.finisher().apply(tuple._6),
						collector7.finisher().apply(tuple._7),
						collector8.finisher().apply(tuple._8),
						collector9.finisher().apply(tuple._9)));
	}

	private TupleCollectors() {}
}
