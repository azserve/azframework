package com.azserve.azframework.query.criteria;

import java.util.function.Function;

import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;

import com.azserve.azframework.query.IQueryDataHolder;

@FunctionalInterface
public interface IJoin<Z, X> extends IFrom<Z, X> {

	@Override
	default From<Z, X> create(final IQueryDataHolder data, final AbstractQuery<?> criteriaQuery) {
		throw new UnsupportedOperationException("Invalid join implementation");
	}

	@Override
	Join<Z, X> resolve(final IQueryDataHolder data);

	/**
	 * <p>ATTENTION</p>
	 * The joins order depends on selected fields.
	 * To force the order add the {@code IJoin} instance
	 * to the from clause.
	 *
	 *
	 * @param condition
	 * @return
	 */
	default IJoin<Z, X> on(final Function<IJoin<Z, X>, IExpression<Boolean>> condition) {
		return data -> this.resolve(data).on(condition.apply(this).resolve(data));
	}

	/**
	 * @deprecated use {@linkplain #on(Function)}
	 */
	@Deprecated
	default IJoin<Z, X> onCondition(final Function<IJoin<Z, X>, IExpression<Boolean>> condition) {
		return this.on(condition);
	}
}
