package com.azserve.azframework.query.criteria;

import javax.persistence.criteria.Order;

import com.azserve.azframework.query.IQueryDataHolder;

@FunctionalInterface
public interface IOrder {

	Order resolve(IQueryDataHolder data);

}
