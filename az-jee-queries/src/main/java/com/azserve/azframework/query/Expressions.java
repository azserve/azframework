package com.azserve.azframework.query;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.persistence.criteria.CriteriaBuilder.Trimspec;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;
import javax.persistence.metamodel.SingularAttribute;

import com.azserve.azframework.common.annotation.NonNullByDefault;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.query.criteria.IExpression;
import com.azserve.azframework.query.criteria.IOrder;
import com.azserve.azframework.query.criteria.IPath;
import com.azserve.azframework.query.criteria.IPredicate;
import com.azserve.azframework.query.criteria.IRoot;

@NonNullByDefault
public final class Expressions {

	public static <S> IRoot<S> from(final Class<S> entityClass) {
		return (data, cq) -> cq.from(entityClass);
	}

	public static ISubquery<Void, Void> subquery() {
		return new com.azserve.azframework.query.Subquery<>(Void.class);
	}

	/*
	 * BASIC PREDICATES
	 */

	/**
	 * Create an optional predicate through the specified optional.
	 *
	 * <pre>
	 * Optional&lt;String&gt; maybeGetNameFilter() {
	 * 	return ... ;
	 * }
	 *
	 * ...
	 *
	 * IPath&lt;String&gt; _name = ... ;
	 *
	 * // if the `maybeGetNameFilter()` result's wrapped value is not empty the following predicate
	 * // will check if it equals to the "name" field value.
	 *
	 * IPredicate predicate = opt(maybeGetNameFilter().map(_name::eq));
	 * </pre>
	 *
	 * @param predicate a predicate wrapped in an optional.
	 * @return a predicate which resolves to the one wrapped by the optional if present,
	 *         otherwise to a conjunction.
	 */
	public static IPredicate opt(final Optional<IPredicate> predicate) {
		return data -> predicate.isPresent()
				? predicate.get().resolve(data)
				: data.getCriteriaBuilder().conjunction();
	}

	/**
	 * Create an optional predicate through the specified supplier.
	 *
	 * <pre>
	 * String getNameFilter() {
	 * 	return ... ;
	 * }
	 *
	 * ...
	 *
	 * IPath&lt;String&gt; _name = ... ;
	 *
	 * // if the `getNameFilter()` result is not null the following predicate
	 * // will check if it equals to the "name" field value.
	 *
	 * IPredicate predicate = () -> getNameFilter() != null ? _name.eq(getNameFilter()) : null;
	 * </pre>
	 *
	 * @param predicate a function which produces the
	 *            predicate or null.
	 * @return a predicate which resolves to the one provided by the supplier if not null,
	 *         otherwise to a conjunction.
	 */
	public static IPredicate opt(final Supplier<IPredicate> predicate) {
		return data -> Optional.ofNullable(predicate.get())
				.map(p -> p.resolve(data))
				.orElse(data.getCriteriaBuilder().conjunction());
	}

	/**
	 * Create an optional predicate based on the specified argument.
	 *
	 * <pre>
	 * IPath&lt;String&gt; _name = ... ;
	 *
	 * String param = ... ;
	 *
	 * // the following predicate will check if the "name" field equals to `param`
	 * // but only if `param` is not null
	 *
	 * IPredicate predicate = opt(param, _name::eq);
	 * </pre>
	 *
	 * @param valueToBeNotNull the optional value.
	 * @param predicate a function which produces the
	 *            predicate based on the argument when the argument
	 *            in not null.
	 * @return the predicate based on the argument if the argument is not null,
	 *         otherwise a conjunction predicate.
	 */
	public static <T> IPredicate opt(final @Nullable T valueToBeNotNull, final Function<T, IPredicate> predicate) {
		return data -> valueToBeNotNull != null
				? predicate.apply(valueToBeNotNull).resolve(data)
				: data.getCriteriaBuilder().conjunction();
	}

	public static IPredicate not(final IExpression<Boolean> expr) {
		return data -> data.getCriteriaBuilder().not(expr.resolve(data));
	}

	public static IPredicate and(final IPredicate... predicates) {
		return data -> data.getCriteriaBuilder().and(resolve(data, predicates));
	}

	public static IPredicate or(final IPredicate... predicates) {
		return data -> data.getCriteriaBuilder().or(resolve(data, predicates));
	}

	public static IPredicate isNull(final IExpression<?> expr) {
		return data -> data.getCriteriaBuilder().isNull(expr.resolve(data));
	}

	public static IPredicate isNotNull(final IExpression<?> expr) {
		return data -> data.getCriteriaBuilder().isNotNull(expr.resolve(data));
	}

	public static IPredicate disjunction() {
		return data -> data.getCriteriaBuilder().disjunction();
	}

	public static IPredicate conjunction() {
		return data -> data.getCriteriaBuilder().conjunction();
	}

	/*
	 * EQUALITY PREDICATES
	 */

	public static IPredicate isTrue(final IExpression<Boolean> expr) {
		return data -> data.getCriteriaBuilder().isTrue(expr.resolve(data));
	}

	public static IPredicate isFalse(final IExpression<Boolean> expr) {
		return data -> data.getCriteriaBuilder().isFalse(expr.resolve(data));
	}

	public static <T> IPredicate equal(final IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().equal(expr.resolve(data), value);
	}

	public static <T> IPredicate equal(final IExpression<? extends T> expr1, final IExpression<? extends T> expr2) {
		return data -> data.getCriteriaBuilder().equal(expr1.resolve(data), expr2.resolve(data));
	}

	public static <T> IPredicate notEqual(final IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().notEqual(expr.resolve(data), value);
	}

	public static <T> IPredicate notEqual(final IExpression<? extends T> expr1, final IExpression<? extends T> expr2) {
		return data -> data.getCriteriaBuilder().notEqual(expr1.resolve(data), expr2.resolve(data));
	}

	public static <T> IPredicate equalOrNull(final IExpression<T> expr, final @Nullable T value) {
		return value != null ? equal(expr, value) : isNull(expr);
	}

	public static <T> IPredicate notEqualNorNull(final IExpression<T> expr, final @Nullable T value) {
		return value != null ? notEqual(expr, value) : isNotNull(expr);
	}

	/*
	 * STRING PREDICATES
	 */

	public static IPredicate like(final IExpression<String> expr, final String value) {
		return data -> data.getCriteriaBuilder().like(expr.resolve(data), value);
	}

	public static IPredicate like(final IExpression<String> expr1, final IExpression<String> expr2) {
		return data -> data.getCriteriaBuilder().like(expr1.resolve(data), expr2.resolve(data));
	}

	public static IPredicate notLike(final IExpression<String> expr, final String value) {
		return data -> data.getCriteriaBuilder().notLike(expr.resolve(data), value);
	}

	public static IPredicate notLike(final IExpression<String> expr1, final IExpression<String> expr2) {
		return data -> data.getCriteriaBuilder().notLike(expr1.resolve(data), expr2.resolve(data));
	}

	/*
	 * COMPARISON PREDICATES
	 */

	public static <T extends Comparable<? super T>> IPredicate ge(final IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().greaterThanOrEqualTo(expr.resolve(data), value);
	}

	public static <T extends Comparable<? super T>> IPredicate ge(final IExpression<T> expr1, final IExpression<T> expr2) {
		return data -> data.getCriteriaBuilder().greaterThanOrEqualTo(expr1.resolve(data), expr2.resolve(data));
	}

	public static <T extends Comparable<? super T>> IPredicate gt(final IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().greaterThan(expr.resolve(data), value);
	}

	public static <T extends Comparable<? super T>> IPredicate gt(final IExpression<T> expr1, final IExpression<T> expr2) {
		return data -> data.getCriteriaBuilder().greaterThan(expr1.resolve(data), expr2.resolve(data));
	}

	public static <T extends Comparable<? super T>> IPredicate le(final IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().lessThanOrEqualTo(expr.resolve(data), value);
	}

	public static <T extends Comparable<? super T>> IPredicate le(final IExpression<T> expr1, final IExpression<T> expr2) {
		return data -> data.getCriteriaBuilder().lessThanOrEqualTo(expr1.resolve(data), expr2.resolve(data));
	}

	public static <T extends Comparable<? super T>> IPredicate lt(final IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().lessThan(expr.resolve(data), value);
	}

	public static <T extends Comparable<? super T>> IPredicate lt(final IExpression<T> expr1, final IExpression<T> expr2) {
		return data -> data.getCriteriaBuilder().lessThan(expr1.resolve(data), expr2.resolve(data));
	}

	public static <T extends Comparable<? super T>> IPredicate between(final IExpression<T> expr, final T upperBound, final T lowerBound) {
		return data -> data.getCriteriaBuilder().between(expr.resolve(data), upperBound, lowerBound);
	}

	public static <T extends Comparable<? super T>> IPredicate between(final IExpression<T> expr, final IExpression<T> upperExpr, final IExpression<T> lowerExpr) {
		return data -> data.getCriteriaBuilder().between(expr.resolve(data), upperExpr.resolve(data), lowerExpr.resolve(data));
	}

	/*
	 * COLLECTION PREDICATES
	 */

	public static <C extends Collection<?>> IPredicate isEmpty(final IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isEmpty(collection.resolve(data));
	}

	public static <C extends Collection<?>> IPredicate isNotEmpty(final IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isNotEmpty(collection.resolve(data));
	}

	public static <E, C extends Collection<E>> IPredicate isMember(final E elem, final IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isMember(elem, collection.resolve(data));
	}

	public static <E, C extends Collection<E>> IPredicate isMember(final IExpression<E> elem, final IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isMember(elem.resolve(data), collection.resolve(data));
	}

	public static <E, C extends Collection<E>> IPredicate isNotMember(final E elem, final IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isNotMember(elem, collection.resolve(data));
	}

	public static <E, C extends Collection<E>> IPredicate isNotMember(final IExpression<E> elem, final IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isNotMember(elem.resolve(data), collection.resolve(data));
	}

	/*
	 * SUBQUERY PREDICATES
	 */

	public static IPredicate exists(final ISubquery<?, ?> subquery) {
		return data -> data.getCriteriaBuilder().exists(subquery.resolve(data));
	}

	public static <Y> IExpression<Y> all(final ISubquery<?, Y> subquery) {
		return data -> data.getCriteriaBuilder().all(subquery.resolve(data));
	}

	public static <Y> IExpression<Y> some(final ISubquery<?, Y> subquery) {
		return data -> data.getCriteriaBuilder().some(subquery.resolve(data));
	}

	public static <Y> IExpression<Y> any(final ISubquery<?, Y> subquery) {
		return data -> data.getCriteriaBuilder().any(subquery.resolve(data));
	}

	/*
	 * UTILITY FUNCTIONS
	 */

	public static <X> IPath<X> rootPath() {
		return data -> Expressions.<X> resolveRoot(data);
	}

	public static <X, T> IPath<T> path(final SingularAttribute<? super X, T> attribute) {
		return data -> Expressions.<X> resolveRoot(data).get(attribute);
	}

	public static <X, Y, T> IPath<T> path(final SingularAttribute<? super X, Y> attribute1, final SingularAttribute<? super Y, T> attribute2) {
		return data -> Expressions.<X> resolveRoot(data).get(attribute1).get(attribute2);
	}

	public static <X, Y, Z, T> IPath<T> path(final SingularAttribute<? super X, Y> attribute1, final SingularAttribute<? super Y, Z> attribute2, final SingularAttribute<? super Z, T> attribute3) {
		return data -> Expressions.<X> resolveRoot(data).get(attribute1).get(attribute2).get(attribute3);
	}

	public static <T> IExpression<T> literal(final T value) {
		return data -> data.getCriteriaBuilder().literal(value);
	}

	public static <T> IExpression<T> nullLiteral(final Class<T> valueClass) {
		return data -> data.getCriteriaBuilder().nullLiteral(valueClass);
	}

	public static <T> IExpression<T> coalesce(final IExpression<? extends T> expr, final @Nullable T value) {
		return data -> data.getCriteriaBuilder().coalesce(expr.resolve(data), value);
	}

	public static <T> IExpression<T> coalesce(final IExpression<? extends T> expr1, final IExpression<? extends T> expr2) {
		return data -> data.getCriteriaBuilder().coalesce(expr1.resolve(data), expr2.resolve(data));
	}

	public static <Y> IExpression<Y> nullif(final IExpression<Y> x, final IExpression<?> y) {
		return data -> data.getCriteriaBuilder().nullif(x.resolve(data), y.resolve(data));
	}

	public static <Y> IExpression<Y> nullif(final IExpression<Y> x, final Y y) {
		return data -> data.getCriteriaBuilder().nullif(x.resolve(data), y);
	}

	public static <T> IExpression<T> iif(final IExpression<Boolean> condition, final IExpression<? extends T> then, final IExpression<? extends T> otherwise) {
		return data -> data.getCriteriaBuilder().<T> selectCase().when(condition.resolve(data), then.resolve(data)).otherwise(otherwise.resolve(data));
	}

	public static <T> IExpression<T> iif(final IExpression<Boolean> condition, final T then, final T otherwise) {
		return data -> data.getCriteriaBuilder().<T> selectCase().when(condition.resolve(data), then).otherwise(otherwise);
	}

	public static <T> IExpression<T> function(final String name, final Class<T> type, final IExpression<?> expr) {
		return data -> data.getCriteriaBuilder().function(name, type, expr.resolve(data));
	}

	public static <T> IExpression<T> function(final String name, final Class<T> type, final IExpression<?>... exprs) {
		return data -> data.getCriteriaBuilder().function(name, type, resolve(data, exprs));
	}

	public static <T, D extends T> IPath<D> treat(final IPath<T> resolver, final Class<D> type) {
		return data -> data.getCriteriaBuilder().treat(resolver.resolve(data), type);
	}

	/*
	 * STRING FUNCTIONS
	 */

	public static IExpression<Integer> length(final IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().length(expr.resolve(data));
	}

	public static IExpression<Integer> locate(final IExpression<String> expr, final IExpression<String> patternExpr) {
		return data -> data.getCriteriaBuilder().locate(expr.resolve(data), patternExpr.resolve(data));
	}

	public static IExpression<Integer> locate(final IExpression<String> expr, final String pattern) {
		return data -> data.getCriteriaBuilder().locate(expr.resolve(data), pattern);
	}

	public static IExpression<Integer> locate(final IExpression<String> expr, final IExpression<String> patternExpr, final IExpression<Integer> fromExpr) {
		return data -> data.getCriteriaBuilder().locate(expr.resolve(data), patternExpr.resolve(data), fromExpr.resolve(data));
	}

	public static IExpression<Integer> locate(final IExpression<String> expr, final String pattern, final int from) {
		return data -> data.getCriteriaBuilder().locate(expr.resolve(data), pattern, from);
	}

	public static IExpression<String> concat(final IExpression<String> expr1, final IExpression<String> expr2) {
		return data -> data.getCriteriaBuilder().concat(expr1.resolve(data), expr2.resolve(data));
	}

	public static IExpression<String> concat(final IExpression<String> expr, final String string) {
		return data -> data.getCriteriaBuilder().concat(expr.resolve(data), string);
	}

	public static IExpression<String> concat(final String string, final IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().concat(string, expr.resolve(data));
	}

	public static IExpression<String> lower(final IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().lower(expr.resolve(data));
	}

	public static IExpression<String> upper(final IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().upper(expr.resolve(data));
	}

	public static IExpression<String> trim(final IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(expr.resolve(data));
	}

	public static IExpression<String> trim(final Trimspec ts, final IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(ts, expr.resolve(data));
	}

	public static IExpression<String> trim(final IExpression<Character> exprChar, final IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(exprChar.resolve(data), expr.resolve(data));
	}

	public static IExpression<String> trim(final Trimspec ts, final IExpression<Character> exprChar, final IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(ts, exprChar.resolve(data), expr.resolve(data));
	}

	public static IExpression<String> trim(final char c, final IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(c, expr.resolve(data));
	}

	public static IExpression<String> trim(final Trimspec ts, final char c, final IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(ts, c, expr.resolve(data));
	}

	public static IExpression<String> substring(final IExpression<String> expr, final int from) {
		return data -> data.getCriteriaBuilder().substring(expr.resolve(data), from);
	}

	public static IExpression<String> substring(final IExpression<String> expr, final int from, final int len) {
		return data -> data.getCriteriaBuilder().substring(expr.resolve(data), from, len);
	}

	public static IExpression<String> substring(final IExpression<String> expr, final IExpression<Integer> from) {
		return data -> data.getCriteriaBuilder().substring(expr.resolve(data), from.resolve(data));
	}

	public static IExpression<String> substring(final IExpression<String> expr, final IExpression<Integer> from, final IExpression<Integer> len) {
		return data -> data.getCriteriaBuilder().substring(expr.resolve(data), from.resolve(data), len.resolve(data));
	}

	/*
	 * NUMERIC FUNCTIONS
	 */

	public static <N extends Number> IExpression<N> abs(final IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().abs(expr.resolve(data));
	}

	public static <N extends Number> IExpression<N> sum(final IExpression<? extends N> expr1, final IExpression<? extends N> expr2) {
		return data -> data.getCriteriaBuilder().sum(expr1.resolve(data), expr2.resolve(data));
	}

	public static <N extends Number> IExpression<N> sum(final IExpression<? extends N> expr, final N value) {
		return data -> data.getCriteriaBuilder().sum(expr.resolve(data), value);
	}

	public static <N extends Number> IExpression<N> diff(final IExpression<? extends N> expr1, final IExpression<? extends N> expr2) {
		return data -> data.getCriteriaBuilder().diff(expr1.resolve(data), expr2.resolve(data));
	}

	public static <N extends Number> IExpression<N> diff(final IExpression<? extends N> expr, final N value) {
		return data -> data.getCriteriaBuilder().diff(expr.resolve(data), value);
	}

	public static <N extends Number> IExpression<N> prod(final IExpression<? extends N> expr1, final IExpression<? extends N> expr2) {
		return data -> data.getCriteriaBuilder().prod(expr1.resolve(data), expr2.resolve(data));
	}

	public static <N extends Number> IExpression<N> prod(final IExpression<? extends N> expr, final N value) {
		return data -> data.getCriteriaBuilder().prod(expr.resolve(data), value);
	}

	public static <N extends Number> IExpression<Number> quot(final IExpression<? extends N> expr1, final IExpression<? extends N> expr2) {
		return data -> data.getCriteriaBuilder().quot(expr1.resolve(data), expr2.resolve(data));
	}

	public static IExpression<Number> quot(final IExpression<? extends Number> expr, final Number value) {
		return data -> data.getCriteriaBuilder().quot(expr.resolve(data), value);
	}

	public static IExpression<Integer> mod(final IExpression<Integer> expr1, final IExpression<Integer> expr2) {
		return data -> data.getCriteriaBuilder().mod(expr1.resolve(data), expr2.resolve(data));
	}

	public static IExpression<Integer> mod(final IExpression<Integer> expr, final Integer value) {
		return data -> data.getCriteriaBuilder().mod(expr.resolve(data), value);
	}

	public static IExpression<Integer> mod(final Integer value, final IExpression<Integer> expr) {
		return data -> data.getCriteriaBuilder().mod(value, expr.resolve(data));
	}

	public static <N extends Number> IExpression<N> neg(final IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().neg(expr.resolve(data));
	}

	public static <N extends Number> IExpression<Double> sqrt(final IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().sqrt(expr.resolve(data));
	}

	public static <N extends Number> IExpression<BigDecimal> toBigDecimal(final IExpression<? extends N> expr) {
		return data -> data.getCriteriaBuilder().toBigDecimal(expr.resolve(data));
	}

	public static <N extends Number> IExpression<Double> toDouble(final IExpression<? extends N> expr) {
		return data -> data.getCriteriaBuilder().toDouble(expr.resolve(data));
	}

	public static <N extends Number> IExpression<Long> toLong(final IExpression<? extends N> expr) {
		return data -> data.getCriteriaBuilder().toLong(expr.resolve(data));
	}

	/*
	 * DATE FUNCTIONS
	 */

	public static IExpression<java.sql.Date> currentDate() {
		return data -> data.getCriteriaBuilder().currentDate();
	}

	public static IExpression<java.sql.Timestamp> currentTimestamp() {
		return data -> data.getCriteriaBuilder().currentTimestamp();
	}

	public static IExpression<java.sql.Time> currentTime() {
		return data -> data.getCriteriaBuilder().currentTime();
	}

	/*
	 * ACCUMULATOR FUNCTIONS
	 */

	public static IExpression<Long> count() {
		return data -> data.getCriteriaBuilder().count(data.getMainRoot().resolve(data));
	}

	public static <Y> IExpression<Long> count(final IExpression<Y> expr) {
		return data -> data.getCriteriaBuilder().count(expr.resolve(data));
	}

	public static <Y> IExpression<Long> countDistinct(final IExpression<Y> expr) {
		return data -> data.getCriteriaBuilder().countDistinct(expr.resolve(data));
	}

	public static <N extends Number> IExpression<N> sum(final IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().sum(expr.resolve(data));
	}

	public static IExpression<Long> sumAsLong(final IExpression<Integer> expr) {
		return data -> data.getCriteriaBuilder().sumAsLong(expr.resolve(data));
	}

	public static <N extends Number> IExpression<Double> avg(final IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().avg(expr.resolve(data));
	}

	public static <N extends Number> IExpression<N> min(final IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().min(expr.resolve(data));
	}

	public static <N extends Number> IExpression<N> max(final IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().max(expr.resolve(data));
	}

	public static <Y extends Comparable<? super Y>> IExpression<Y> least(final IExpression<Y> expr) {
		return data -> data.getCriteriaBuilder().least(expr.resolve(data));
	}

	public static <Y extends Comparable<? super Y>> IExpression<Y> greatest(final IExpression<Y> expr) {
		return data -> data.getCriteriaBuilder().greatest(expr.resolve(data));
	}

	/*
	 * MAP FUNCTIONS
	 */

	public static <K, V> IExpression<Set<K>> keys(final Map<K, V> map) {
		return data -> data.getCriteriaBuilder().keys(map);
	}

	public static <K, V> IExpression<Collection<V>> values(final Map<K, V> map) {
		return data -> data.getCriteriaBuilder().values(map);
	}

	/*
	 * ORDERS
	 */

	public static IOrder asc(final IExpression<?> expr) {
		return data -> data.getCriteriaBuilder().asc(expr.resolve(data));
	}

	public static IOrder desc(final IExpression<?> expr) {
		return data -> data.getCriteriaBuilder().desc(expr.resolve(data));
	}

	/*
	 * INTERNAL
	 */

	@SuppressWarnings("unchecked")
	private static <X> From<?, X> resolveRoot(final IQueryDataHolder data) {
		final IRoot<?> mainRoot = requireNonNull(data.getMainRoot(), "No root defined for this query");
		try {
			return (From<?, X>) mainRoot.resolve(data);
		} catch (final @SuppressWarnings("unused") ClassCastException ex) {
			throw new ClassCastException("Root type is not valid for this attribute");
		}
	}

	private static Predicate[] resolve(final IQueryDataHolder data, final IPredicate[] predicates) {
		if (predicates.length == 0) {
			return new Predicate[0];
		}
		final Predicate[] res = new Predicate[predicates.length];
		for (int i = 0; i < predicates.length; i++) {
			res[i] = predicates[i].resolve(data);
		}
		return res;
	}

	private static Expression<?>[] resolve(final IQueryDataHolder data, final IExpression<?>[] expressions) {
		if (expressions.length == 0) {
			return new Expression[0];
		}
		final Expression<?>[] res = new Expression[expressions.length];
		for (int i = 0; i < expressions.length; i++) {
			res[i] = expressions[i].resolve(data);
		}
		return res;
	}

	private Expressions() {}
}