package com.azserve.azframework.query;

import static com.azserve.azframework.query.Expressions.equalOrNull;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Supplier;

import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;

import com.azserve.azframework.query.criteria.IExpression;
import com.azserve.azframework.query.criteria.IFrom;
import com.azserve.azframework.query.criteria.IJoin;
import com.azserve.azframework.query.criteria.IPredicate;
import com.azserve.azframework.query.criteria.IRoot;

@SuppressWarnings("hiding")
class Subquery<F, R> implements ISubquery<F, R> {

	private final Class<R> resultClass;
	private final IExpression<R> selection;
	private boolean distinct;
	private IdentityHashMap<IFrom<?, ?>, From<?, ?>> froms;
	private IRoot<F> mainRoot;
	private List<IPredicate> predicates;
	private List<IExpression<?>> groups;
	private List<IPredicate> havingPredicates;

	private boolean invalidated;

	Subquery(final Class<R> type) {
		this(type, null, null);
	}

	Subquery(final Class<R> type, final Subquery<F, ?> query, final IExpression<R> selection) {
		this.resultClass = type;
		this.selection = selection;
		if (query != null) {
			this.distinct = query.distinct;
			this.froms = query.froms;
			this.mainRoot = query.mainRoot;
			this.predicates = query.predicates;
			this.groups = query.groups;
			this.havingPredicates = query.havingPredicates;
			query.invalidate();
		} else {
			this.froms = new IdentityHashMap<>();
		}
	}

	private void invalidate() {
		this.invalidated = true;
		this.froms = null;
		// this.subqueries = null;
		this.predicates = null;
		this.groups = null;
		this.havingPredicates = null;
		this.mainRoot = null;
	}

	@Override
	public ISubquery<F, R> distinct() {
		this.distinct = true;
		return this;
	}

	@Override
	public <T> ISubquery<F, T> select(final Class<T> resultClass, final IExpression<T> selection) {
		return new Subquery<>(resultClass, this, selection);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> ISubquery<T, R> from(final IRoot<T> root, final IFrom<?, ?>... froms) {
		final Subquery<T, R> query = (Subquery<T, R>) this;
		query.froms.clear();
		query.mainRoot = requireNonNull(root);
		query.froms.put(root, null);
		for (final IFrom<?, ?> from : froms) {
			query.froms.put(from, null);
		}
		return query;
	}

	@Override
	public ISubquery<F, R> where(final IPredicate... predicates) {
		Collections.addAll(this.getPredicates(), predicates);
		return this;
	}

	@Override
	public <V> ISubquery<F, R> eq(final IExpression<V> expr, final V value) {
		this.getPredicates().add(equalOrNull(expr, value));
		return this;
	}

	@Override
	public ISubquery<F, R> groupBy(final IExpression<?>... expressions) {
		if (this.groups == null) {
			this.groups = new ArrayList<>();
		}
		Collections.addAll(this.groups, expressions);
		return this;
	}

	@Override
	public ISubquery<F, R> having(final IPredicate... predicates) {
		if (this.havingPredicates == null) {
			this.havingPredicates = new ArrayList<>();
		}
		Collections.addAll(this.havingPredicates, predicates);
		return this;
	}

	@Override
	public javax.persistence.criteria.Subquery<R> create(final IQueryDataHolder data) {
		if (this.invalidated) {
			throw new IllegalStateException("Method select has been called, this subquery class instance should not be used");
		}
		final javax.persistence.criteria.Subquery<R> sq = data.getCurrentQuery().subquery(this.resultClass);
		// this.prepareFrom(criteriaQuery);
		// this.prepareSubqueries(criteriaQuery);
		// this.prepareSelect(criteriaQuery);
		// this.prepareWhere(criteriaQuery);
		// this.prepareOrder(criteriaQuery);
		// this.prepareGroup(criteriaQuery);
		// this.prepareHaving(criteriaQuery);
		final SubqueryDataHolder subData = new SubqueryDataHolder(data.getCriteriaBuilder(), sq, data);
		for (final Entry<IFrom<?, ?>, From<?, ?>> entry : this.froms.entrySet()) {
			entry.setValue(entry.getKey().create(subData, sq));
		}
		if (this.selection != null) {
			sq.select(this.selection.resolve(subData));
		}
		if (this.predicates != null) {
			sq.where(this.predicates.stream().map(p -> p.resolve(subData)).toArray(Predicate[]::new));
		}
		if (this.groups != null) {
			sq.groupBy(this.groups.stream().map(g -> g.resolve(subData)).collect(toList()));
		}
		if (this.havingPredicates != null) {
			sq.having(this.havingPredicates.stream().map(p -> p.resolve(subData)).toArray(Predicate[]::new));
		}
		return sq;
	}

	@Override
	public IExpression<R> getSelection() {
		return data -> this.resolve(data).getSelection();
	}

	private List<IPredicate> getPredicates() {
		if (this.predicates == null) {
			this.predicates = new ArrayList<>();
		}
		return this.predicates;
	}

	private <S, T> void registerJoin(final IJoin<S, T> iJoin, final Join<S, T> join) {
		this.froms.put(iJoin, join);
	}

	@SuppressWarnings("unchecked")
	private <S, T> From<S, T> getFrom(final IFrom<S, T> from) {
		return (From<S, T>) this.froms.get(from);
	}

	private IRoot<?> getMainRoot() {
		return this.mainRoot;
	}

	private class SubqueryDataHolder implements IQueryDataHolder {

		private final CriteriaBuilder criteriaBuilder;
		private final javax.persistence.criteria.Subquery<?> currentQuery;
		private final IQueryDataHolder parentQuery;

		SubqueryDataHolder(final CriteriaBuilder criteriaBuilder, final javax.persistence.criteria.Subquery<?> currentQuery, final IQueryDataHolder parentQuery) {
			this.criteriaBuilder = criteriaBuilder;
			this.currentQuery = currentQuery;
			this.parentQuery = parentQuery;
		}

		@Override
		public CriteriaBuilder getCriteriaBuilder() {
			return this.criteriaBuilder;
		}

		@Override
		public AbstractQuery<?> getCurrentQuery() {
			return this.currentQuery;
		}

		@Override
		public <S, T> void registerJoin(final IJoin<S, T> iJoin, final Join<S, T> join) {
			Subquery.this.registerJoin(iJoin, join);
		}

		@Override
		public <S, T> From<S, T> getFrom(final IFrom<S, T> from) {
			final From<S, T> subqueryFrom = Subquery.this.getFrom(from);
			return subqueryFrom == null ? this.parentQuery.getFrom(from) : subqueryFrom;
		}

		@Override
		public <R> javax.persistence.criteria.Subquery<R> getSubquery(final ISubquery<?, R> subquery, final Supplier<javax.persistence.criteria.Subquery<R>> supplier) {
			return this.parentQuery.getSubquery(subquery, supplier);
		}

		@Override
		public IRoot<?> getMainRoot() {
			return Subquery.this.getMainRoot();
		}
	}
}
