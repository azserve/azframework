package com.azserve.azframework.query;

import javax.persistence.TypedQuery;

final class Jpa {

	private static final boolean VERSION_GREATER_THAN21;
	static {
		boolean gt21;
		try {
			TypedQuery.class.getMethod("getResultStream");
			gt21 = true;
		} catch (@SuppressWarnings("unused") NoSuchMethodException | SecurityException ex) {
			gt21 = false;
		}
		VERSION_GREATER_THAN21 = gt21;
	}

	static boolean isVersionGreaterThan21() {
		return VERSION_GREATER_THAN21;
	}

	private Jpa() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}
