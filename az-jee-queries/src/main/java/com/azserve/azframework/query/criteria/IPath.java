package com.azserve.azframework.query.criteria;

import javax.persistence.criteria.Path;
import javax.persistence.metamodel.MapAttribute;
import javax.persistence.metamodel.PluralAttribute;
import javax.persistence.metamodel.SingularAttribute;

import com.azserve.azframework.query.IQueryDataHolder;

@FunctionalInterface
public interface IPath<X> extends IExpression<X> {

	@Override
	Path<X> resolve(IQueryDataHolder data);

	default <Y> IPath<Y> get(final SingularAttribute<? super X, Y> attribute) {
		return data -> this.resolve(data).get(attribute);
	}

	default <E, C extends java.util.Collection<E>> IExpression<C> get(final PluralAttribute<X, C, E> attribute) {
		return data -> this.resolve(data).get(attribute);
	}

	default <K, V, M extends java.util.Map<K, V>> IExpression<M> get(final MapAttribute<X, K, V> attribute) {
		return data -> this.resolve(data).get(attribute);
	}
}