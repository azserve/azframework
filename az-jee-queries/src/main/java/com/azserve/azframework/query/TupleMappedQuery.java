package com.azserve.azframework.query;

import static java.util.stream.Collectors.toCollection;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Stream;

import javax.ejb.SessionContext;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Selection;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.ex.CheckedConsumer;
import com.azserve.azframework.common.util.CollectionUtils;
import com.azserve.azframework.query.ITupleQuery.TupleMapper;
import com.azserve.azframework.query.ITupleQuery.TupleResolver;
import com.azserve.azframework.query.criteria.IFrom;
import com.azserve.azframework.query.criteria.IRoot;
import com.azserve.azframework.query.criteria.ISelection;

class TupleMappedQuery<F, R> extends Query<ISingleQuery<F, R>, F, Tuple> implements ISingleQuery<F, R>, TupleResolver {

	private final IdentityHashMap<ISelection<?>, Selection<?>> selections;
	private final TupleMapper<R> mapper;

	TupleMappedQuery(final Query<?, F, ?> query, final MappedSelectionBuilder<?, R> selectionBuilder) {
		super(query);
		final Map.Entry<TupleMapper<R>, List<ISelection<?>>> mapperAndSelections = selectionBuilder.build();
		this.mapper = mapperAndSelections.getKey();
		this.selections = new IdentityHashMap<>();
		for (final ISelection<?> selection : mapperAndSelections.getValue()) {
			this.selections.put(selection, null);
		}
	}

	TupleMappedQuery(final Query<?, F, ?> query, final TupleMapper<R> mapper, final ISelection<?>... selections) {
		super(query);
		this.mapper = mapper;
		this.selections = new IdentityHashMap<>();
		for (final ISelection<?> selection : selections) {
			this.selections.put(selection, null);
		}
	}

	@Override
	protected Class<Tuple> getResultClass() {
		return Tuple.class;
	}

	@Override
	protected void prepareSelect(final CriteriaQuery<Tuple> criteriaQuery) {
		final List<Selection<?>> l = new ArrayList<>(this.selections.size());
		for (final Entry<ISelection<?>, Selection<?>> entry : this.selections.entrySet()) {
			final Selection<?> selection = entry.getKey().resolve(this);
			entry.setValue(selection);
			l.add(selection);
		}
		criteriaQuery.multiselect(l);
	}

	@Override
	protected void invalidateBeforeQuery() {
		// invalidation will be done manually
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ISingleQuery<T, R> from(final IRoot<T> root, final IFrom<?, ?>... froms) {
		final TupleMappedQuery<T, R> query = (TupleMappedQuery<T, R>) this;
		query.fromInternal(root, froms);
		return query;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(final @NonNull Tuple tuple, final @NonNull ISelection<T> selection) {
		final Selection<T> sel = (Selection<T>) this.selections.get(selection);
		return tuple.get(sel != null ? sel : selection.resolve(this));
	}

	@Override
	public List<R> fetch() {
		return this.prepareQuery().getResultList()
				.stream()
				.onClose(this::invalidate)
				.map(tuple -> this.mapper.map(tuple, this))
				.collect(toCollection(ArrayList::new));
	}

	@Override
	public <E extends Exception> void fetchAndThen(final CheckedConsumer<R, E> consumer) throws E {
		this.tupleStream()
				.map(tuple -> this.mapper.map(tuple, this))
				.forEach(CheckedConsumer.unwrap(consumer));
		this.invalidate();
	}

	@Override
	public Optional<R> fetchFirst() {
		final TypedQuery<Tuple> query = this.prepareQuery();
		query.setMaxResults(1);
		final Optional<R> optional = CollectionUtils.findFirst(query.getResultList()).map(tuple -> this.mapper.map(tuple, this));
		this.invalidate();
		return optional;
	}

	@Override
	public R fetchSingleResult() {
		final TypedQuery<Tuple> query = this.prepareQuery();
		final Tuple singleResult = query.getSingleResult();
		this.invalidate();
		return singleResult != null ? this.mapper.map(singleResult, this) : null;
	}

	@Override
	public Stream<R> stream() {
		return this.tupleStream().map(t -> this.mapper.map(t, this));
	}

	@Override
	public Optional<List<R>> fetchAsync(final SessionContext sessionContext) {
		if (sessionContext.wasCancelCalled()) {
			this.invalidate();
			return Optional.empty();
		}
		final TypedQuery<Tuple> query = this.prepareQuery();
		if (sessionContext.wasCancelCalled()) {
			this.invalidate();
			return Optional.empty();
		}
		final List<Tuple> resultList = query.getResultList();
		if (sessionContext.wasCancelCalled()) {
			this.invalidate();
			return Optional.empty();
		}
		return Optional.of(resultList.stream().map(tuple -> this.mapper.map(tuple, this))
				.onClose(this::invalidate)
				.collect(toCollection(ArrayList::new)));
	}

	@Override
	public <E extends Exception> void fetchAsyncAndThen(final SessionContext sessionContext, final CheckedConsumer<R, E> consumer) throws E {
		this.tupleStream()
				.map(tuple -> this.mapper.map(tuple, this))
				// TODO until sessionContext.wasCancelCalled()
				.forEach(CheckedConsumer.unwrap(consumer));
	}

	private Stream<Tuple> tupleStream() {
		final Stream<Tuple> resultStream;
		if (Jpa.isVersionGreaterThan21()) {
			resultStream = this.prepareQuery().getResultStream();
		} else {
			resultStream = this.prepareQuery().getResultList().stream();
		}
		return resultStream.onClose(this::invalidate);
	}
}