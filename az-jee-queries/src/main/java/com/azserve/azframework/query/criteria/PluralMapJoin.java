package com.azserve.azframework.query.criteria;

import java.util.Objects;

import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.MapJoin;
import javax.persistence.metamodel.MapAttribute;

import com.azserve.azframework.query.IQueryDataHolder;

public class PluralMapJoin<Z, K, V> implements IMapJoin<Z, K, V> {

	private final IFrom<?, Z> parent;
	private final MapAttribute<? super Z, K, V> attribute;
	private final JoinType joinType;

	PluralMapJoin(final IFrom<?, Z> parent, final MapAttribute<? super Z, K, V> attribute, final JoinType joinType) {
		this.parent = parent;
		this.attribute = attribute;
		this.joinType = joinType;
	}

	@Override
	public MapJoin<Z, K, V> create(final IQueryDataHolder data, final AbstractQuery<?> criteriaQuery) {
		return this.create(this.parent.resolve(data));
	}

	@Override
	@SuppressWarnings("unchecked")
	public MapJoin<Z, K, V> resolve(final IQueryDataHolder data) {
		final MapJoin<Z, K, V> foundJoin = (MapJoin<Z, K, V>) data.getFrom(this);
		if (foundJoin != null) {
			return foundJoin;
		}

		final From<?, Z> from = Objects.requireNonNull(
				this.parent.resolve(data),
				() -> "Cannot resolve parent join for " + this.attribute);

		final MapJoin<Z, K, V> join = this.create(from);
		data.registerJoin(this, join);
		return join;
	}

	private MapJoin<Z, K, V> create(final From<?, Z> from) {
		return from.join(this.attribute, this.joinType);
	}
}
