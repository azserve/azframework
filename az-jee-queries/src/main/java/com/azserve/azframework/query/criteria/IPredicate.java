package com.azserve.azframework.query.criteria;

import javax.persistence.criteria.Predicate;

import com.azserve.azframework.query.IQueryDataHolder;

@FunctionalInterface
public interface IPredicate extends IExpression<Boolean> {

	@Override
	Predicate resolve(IQueryDataHolder data);

	/**
	 * Create a negation of the predicate.
	 */
	default IPredicate not() {
		return data -> this.resolve(data).not();
	}

	/**
	 * Create a negation of the predicate or return
	 * the predicate itself based on the provided
	 * boolean value.
	 *
	 * @param negate whether or not create a negation
	 *            of the predicate
	 */
	default IPredicate negate(final boolean negate) {
		return negate ? this.not() : this;
	}

	/**
	 * Create a conjunction of this predicate with
	 * the given one.
	 *
	 * @param predicate the other predicate
	 * @return and predicate
	 */
	default IPredicate and(final IPredicate predicate) {
		return data -> data.getCriteriaBuilder().and(this.resolve(data), predicate.resolve(data));
	}

	/**
	 * Create a disjunction of this predicate with
	 * the given one.
	 *
	 * @param predicate the other predicate
	 * @return or predicate
	 */
	default IPredicate or(final IPredicate predicate) {
		return data -> data.getCriteriaBuilder().or(this.resolve(data), predicate.resolve(data));
	}
}
