package com.azserve.azframework.query.criteria;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.criteria.Expression;

import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.query.Expressions;
import com.azserve.azframework.query.IQueryDataHolder;
import com.azserve.azframework.query.ISubquery;

@FunctionalInterface
public interface IExpression<T> extends ISelection<T> {

	@Override
	Expression<T> resolve(IQueryDataHolder data);

	/**
	 * Perform a typecast upon the expression, returning a new expression object
	 * (runtime type is not changed).
	 */
	default <X> IExpression<X> cast(final Class<X> type) {
		return data -> this.resolve(data).as(type);
	}

	/**
	 * Create a predicate to test whether this expression is null.
	 */
	default IPredicate isNull() {
		return data -> this.resolve(data).isNull();
	}

	/**
	 * Create a predicate to test whether this expression is not null.
	 */
	default IPredicate isNotNull() {
		return data -> this.resolve(data).isNotNull();
	}

	/**
	 * Create a predicate to test this expression with
	 * the argument for equality.
	 * If the argument is null the predicate will test
	 * whether this expression is null.
	 */
	default IPredicate eq(final @Nullable T value) {
		return Expressions.equalOrNull(this, value);
	}

	/**
	 * Create a predicate to test this expression with
	 * the argument for equality.
	 */
	default IPredicate equal(final T value) {
		return Expressions.equal(this, value);
	}

	/**
	 * Create a predicate to test this expression with
	 * another for equality.
	 */
	default IPredicate equal(final IExpression<? extends T> expr) {
		return Expressions.equal(this, expr);
	}

	/**
	 * Create a predicate to test this expression with
	 * the argument for inequality.
	 * If the argument is null the predicate will test
	 * whether this expression is not null.
	 */
	default IPredicate notEq(final @Nullable T value) {
		return Expressions.notEqualNorNull(this, value);
	}

	/**
	 * Create a predicate to test this expression with
	 * the argument for inequality.
	 */
	default IPredicate notEqual(final T value) {
		return Expressions.notEqual(this, value);
	}

	/**
	 * Create a predicate to test this expression with
	 * another for inequality.
	 */
	default IPredicate notEqual(final IExpression<? extends T> expr) {
		return Expressions.notEqual(this, expr);
	}

	/**
	 * Create a predicate testing for a true value based
	 * on this expression type.
	 * <ul>
	 * <li>boolean: true</li>
	 * <li>number: 1</li>
	 * <li>char: '1'</li>
	 * <li>string: "1" or "true" (case-insensitive)</li>
	 * </ul>
	 */
	default IPredicate isTrue() {
		return data -> ExpressionsHelper.isTrueUnchecked(data, this);
	}

	/**
	 * Create a predicate testing for a false value based
	 * on this expression type.
	 * <ul>
	 * <li>boolean: false</li>
	 * <li>number: 0</li>
	 * <li>char: '0'</li>
	 * <li>string: "0" or "false" (case-insensitive)</li>
	 * </ul>
	 */
	default IPredicate isFalse() {
		return data -> ExpressionsHelper.isFalseUnchecked(data, this);
	}

	/**
	 * Create a predicate for testing whether this expression
	 * is less than the argument.
	 *
	 * @throws IllegalArgumentException if the type of this expression is
	 *             not {@code Comparable}.
	 */
	default <U extends Comparable<? super T>> IPredicate lt(final @Nullable U value) {
		return data -> ExpressionsHelper.lessThanUnchecked(data, this, value);
	}

	/**
	 * Create a predicate for testing whether this expression
	 * is less than or equal to the argument.
	 *
	 * @throws IllegalArgumentException if the type of this expression is
	 *             not {@code Comparable}.
	 */
	default <U extends Comparable<? super T>> IPredicate le(final @Nullable U value) {
		return data -> ExpressionsHelper.lessThanOrEqualToUnchecked(data, this, value);
	}

	/**
	 * Create a predicate for testing whether this expression
	 * is greater than the argument.
	 *
	 * @throws IllegalArgumentException if the type of this expression is
	 *             not {@code Comparable}.
	 */
	default <U extends Comparable<? super T>> IPredicate gt(final @Nullable U value) {
		return data -> ExpressionsHelper.greaterThanUnchecked(data, this, value);
	}

	/**
	 * Create a predicate for testing whether this expression
	 * is greater than or equal to the argument.
	 *
	 * @throws IllegalArgumentException if the type of this expression is
	 *             not {@code Comparable}.
	 */
	default <U extends Comparable<? super T>> IPredicate ge(final @Nullable U value) {
		return data -> ExpressionsHelper.greaterThanOrEqualToUnchecked(data, this, value);
	}

	/**
	 * Create an expression that returns null if either this expression
	 * and the argument evaluate to null,
	 * and the first non-null value otherwise.
	 */
	default IExpression<T> orElse(final IExpression<? extends T> expr) {
		return Expressions.coalesce(this, expr);
	}

	/**
	 * Create an expression that returns null if either this expression
	 * and the argument evaluate to null,
	 * and the first non-null value otherwise.
	 */
	default IExpression<T> orElse(final @Nullable T value) {
		return Expressions.coalesce(this, value);
	}

	/**
	 * Create an ordering by the ascending value of this expression.
	 */
	default IOrder asc() {
		return Expressions.asc(this);
	}

	/**
	 * Create an ordering by the descending value of this expression.
	 */
	default IOrder desc() {
		return Expressions.desc(this);
	}

	/**
	 * Create a predicate to test whether this expression is a member of the argument list.
	 * <br>
	 * If the argument list is empty a disjunction (with zero disjuncts) will be returned
	 * (a disjunction with zero disjuncts is false).
	 */
	default <U extends T> IPredicate in(final @SuppressWarnings("unchecked") U... values) {
		return data -> values.length == 0
				? data.getCriteriaBuilder().disjunction()
				: this.resolve(data).in(values);
	}

	/**
	 * Create a predicate to test whether this expression is a member of the argument list.
	 * <br>
	 * If the argument list is empty a disjunction (with zero disjuncts) will be returned
	 * (a disjunction with zero disjuncts is false).
	 */
	default IPredicate in(final @SuppressWarnings("unchecked") IExpression<? extends T>... values) {
		return data -> values.length == 0
				? data.getCriteriaBuilder().disjunction()
				: this.resolve(data).in(Arrays.stream(values).map(exp -> exp.resolve(data)).toArray(Expression[]::new));
	}

	/**
	 * Create a predicate to test whether this expression is a member of the argument list.
	 * <br>
	 * If the argument list is empty a disjunction (with zero disjuncts) will be returned
	 * (a disjunction with zero disjuncts is false).
	 */
	default IPredicate in(final Collection<? extends T> values) {
		return data -> values.isEmpty()
				? data.getCriteriaBuilder().disjunction()
				: this.resolve(data).in(values);
	}

	/**
	 * Create a predicate to test whether this expression is a member of
	 * the arguments returned by the specified subquery.
	 */
	default IPredicate in(final ISubquery<?, T> subquery) {
		return data -> this.resolve(data).in(subquery.resolve(data));
	}
}
