package com.azserve.azframework.query;

public final class Tuple2<T1, T2> {

	public final T1 _1;
	public final T2 _2;

	public Tuple2(final T1 _1, final T2 _2) {
		this._1 = _1;
		this._2 = _2;
	}

	@Override
	public String toString() {
		return "[" + this._1 + ", " + this._2 + "]";
	}
}
