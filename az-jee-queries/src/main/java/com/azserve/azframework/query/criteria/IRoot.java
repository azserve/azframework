package com.azserve.azframework.query.criteria;

@FunctionalInterface
public interface IRoot<X> extends IFrom<X, X> {
	//
}
