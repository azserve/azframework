package com.azserve.azframework.vaadin8.fields;

import java.time.LocalTime;

import org.vaadin.viritin.util.HtmlElementPropertySetter;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.lang.NumberUtils;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomField;

public class HourMinuteField extends CustomField<LocalTime> {

	private static final long serialVersionUID = 2813252834448942413L;

	private LocalTime internalValue;
	private boolean ignoreValueChanges = false;

	private final IntegerField hour;
	private final IntegerField minute;

	public HourMinuteField(final String caption) {
		this();
		this.setCaption(caption);
	}

	public HourMinuteField() {
		this.setWidth(8, Unit.EM);

		this.hour = new FormattedIntegerField(0, 24);
		this.hour.setWidth(50, Unit.PERCENTAGE);

		this.minute = new FormattedIntegerField(0, 59);
		this.minute.setWidth(50, Unit.PERCENTAGE);

		this.hour.addValueChangeListener(e -> {
			if (this.ignoreValueChanges) {
				return;
			}
			final Integer value = e.getValue();
			if (value != null) {
				if (value.intValue() < 0 || value.intValue() > 23) {
					this.hour.setValue(e.getOldValue());
					return;
				}
				if (this.minute.isEmpty()) {
					this.minute.setValue(Integer.valueOf(LocalTime.now().getMinute()));
				}
				this.setValue(LocalTime.of(value.intValue(), this.minute.getValue().intValue()));
			} else {
				this.minute.clear();
				this.clear();
			}
		});

		this.minute.addValueChangeListener(e -> {
			if (this.ignoreValueChanges) {
				return;
			}
			final Integer value = e.getValue();
			if (value != null) {
				if (value.intValue() < 0 || value.intValue() > 59) {
					this.minute.setValue(e.getOldValue());
					return;
				}
				if (this.hour.isEmpty()) {
					this.hour.setValue(Integer.valueOf(LocalTime.now().getHour()));
				}
				this.setValue(LocalTime.of(this.hour.getValue().intValue(), value.intValue()));
			} else {
				this.hour.clear();
				this.clear();
			}
		});
	}

	@Override
	protected Component initContent() {
		final CssLayout cssLayout = new CssLayout(this.hour, this.minute);
		cssLayout.setWidth(100, Unit.PERCENTAGE);
		return cssLayout;
	}

	@Override
	protected void doSetValue(final LocalTime value) {
		final LocalTime truncatedValue = value != null ? LocalTime.of(value.getHour(), value.getMinute()) : null;
		this.internalValue = truncatedValue;
		this.ignoreValueChanges = true;
		try {
			if (truncatedValue != null) {
				this.hour.setValue(Integer.valueOf(truncatedValue.getHour()));
				this.minute.setValue(Integer.valueOf(truncatedValue.getMinute()));
			} else {
				this.hour.clear();
				this.minute.clear();
			}
		} finally {
			this.ignoreValueChanges = false;
		}
	}

	@Override
	public LocalTime getValue() {
		return this.internalValue;
	}

	private final class FormattedIntegerField extends IntegerField {

		private static final long serialVersionUID = 6267862011977533875L;

		private final int min;
		private final int max;

		FormattedIntegerField(final int min, final int max) {
			this.min = min;
			this.max = max;
		}

		@Override
		protected void configureHtml(final HtmlElementPropertySetter heps) {
			super.configureHtml(heps);
			heps.setProperty("maxlength", "2");
			heps.setProperty("min", "" + this.min);
			heps.setProperty("max", "" + this.max);
		}

		@Override
		protected @NonNull String convertToText(final @Nullable Integer value) {
			return NumberUtils.format(value, "00");
		}
	}
}
