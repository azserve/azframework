package com.azserve.azframework.vaadin8.components.factory;

import com.vaadin.ui.VerticalLayout;

class VerticalLayoutFactory extends OrderedLayoutFactory<VerticalLayout> {

	VerticalLayoutFactory() {
		super();
	}

	@Override
	protected VerticalLayout supplyLayout() {
		return new VerticalLayout();
	}
}
