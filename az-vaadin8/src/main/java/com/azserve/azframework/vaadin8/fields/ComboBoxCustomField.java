package com.azserve.azframework.vaadin8.fields;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;

public abstract class ComboBoxCustomField<T> extends CustomField<T> {

	private static final long serialVersionUID = 7432558287582561278L;

	private T internalValue;

	public ComboBoxCustomField() {
		this.setSizeUndefined();
	}

	@Override
	protected final Component initContent() {
		this.getField().addValueChangeListener(e -> this.setValue(e.getValue(), e.isUserOriginated()));
		return this.createContent();
	}

	protected abstract Component createContent();

	@Override
	protected void doSetValue(final T value) {
		this.internalValue = value;
		this.getField().setValue(value);
	}

	@Override
	public T getValue() {
		return this.internalValue;
	}

	/**
	 * Restituisce il combobox interno.
	 * Viene chiamato più volte pertanto
	 * non dev'essere istanziato qui.
	 */
	protected abstract ComboBox<T> getField();
}
