package com.azserve.azframework.vaadin8.components.factory;

import java.util.Objects;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

public abstract class OrderedLayoutFactory<L extends AbstractOrderedLayout> extends AbstractLayoutFactory<L, OrderedLayoutFactory<L>> {

	OrderedLayoutFactory() {
		super();
	}

	public OrderedLayoutFactory<L> withSpacing(final boolean spacing) {
		this.get().setSpacing(spacing);
		return this;
	}

	public OrderedLayoutFactory<L> withMargin(final boolean margin) {
		this.get().setMargin(margin);
		return this;
	}

	public OrderedLayoutFactory<L> withMargins(final boolean top, final boolean right, final boolean bottom, final boolean left) {
		this.get().setMargin(new MarginInfo(top, right, bottom, left));
		return this;
	}

	public OrderedLayoutFactory<L> withDefaultAlignment(final Alignment defaultAlignment) {
		this.get().setDefaultComponentAlignment(defaultAlignment);
		return this;
	}

	@Override
	public OrderedLayoutFactory<L> add(final Component c) {
		this.get().addComponent(c);
		return this;
	}

	public OrderedLayoutFactory<L> add(final Component c, final int index) {
		this.get().addComponent(c, index);
		return this;
	}

	public OrderedLayoutFactory<L> add(final Component c1, final Component c2) {
		final L l = this.get();
		l.addComponent(c1);
		l.addComponent(c2);
		return this;
	}

	public OrderedLayoutFactory<L> add(final Component c1, final Component c2, final Component c3) {
		final L l = this.get();
		l.addComponent(c1);
		l.addComponent(c2);
		l.addComponent(c3);
		return this;
	}

	@Override
	public OrderedLayoutFactory<L> add(final Component... c) {
		this.get().addComponents(c);
		return this;
	}

	public OrderedLayoutFactory<L> add(final Object obj) {
		return this.add(new Label(Objects.toString(obj, "")));
	}

	public OrderedLayoutFactory<L> align(final Component c, final Alignment alignment) {
		this.get().setComponentAlignment(c, alignment);
		return this;
	}

	public OrderedLayoutFactory<L> align(final Alignment alignment) {
		return this.align(this.last, alignment);
	}

	public OrderedLayoutFactory<L> expand(final Component c, final float ratio) {
		this.get().setExpandRatio(c, ratio);
		return this;
	}

	public OrderedLayoutFactory<L> expand(final float ratio) {
		return this.expand(this.last, ratio);
	}

	public OrderedLayoutFactory<L> expand(final Component c) {
		return this.expand(c, 1);
	}

	public OrderedLayoutFactory<L> expand() {
		return this.expand(this.last, 1);
	}
}