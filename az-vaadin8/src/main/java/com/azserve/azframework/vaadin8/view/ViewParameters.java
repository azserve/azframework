package com.azserve.azframework.vaadin8.view;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.azserve.azframework.common.annotation.NonNull;

import elemental.json.Json;
import elemental.json.JsonObject;

public final class ViewParameters {

	public static @NonNull Map<String, String> decodeJson(final String source) {
		final byte[] decodedString = Base64.getDecoder().decode(source.getBytes());
		if (decodedString.length == 0) {
			return Collections.emptyMap();
		}
		try {
			final JsonObject json = Json.parse(new String(decodedString));
			final Map<String, String> map = new HashMap<>();
			for (final String key : json.keys()) {
				map.put(key, json.getString(key));
			}
			return Collections.unmodifiableMap(map);
		} catch (final Exception ex) {
			return Collections.emptyMap();
		}
	}

	public static String encodeJson(final @NonNull Map<String, String> params) {
		if (params.isEmpty()) {
			return "";
		}
		final JsonObject json = Json.createObject();
		for (final Map.Entry<String, String> entry : params.entrySet()) {
			json.put(entry.getKey(), entry.getValue());
		}
		return Base64.getEncoder().encodeToString(json.toJson().getBytes());
	}

	public static String encodePlain(final @NonNull Map<String, String> params) {
		if (params.isEmpty()) {
			return "";
		}
		return params.entrySet().stream().map(e -> encode(e.getKey()) + "=" + encode(e.getValue())).collect(Collectors.joining("&"));
	}

	public static Map<String, String> decodePlain(final String parameterString) {
		if (parameterString.isEmpty()) {
			return Collections.emptyMap();
		}
		final Map<String, String> parameterMap = new HashMap<>();
		final String[] parameters = parameterString.split("&");
		for (int i = 0; i < parameters.length; i++) {
			final String[] keyAndValue = parameters[i]
					.split("=");
			parameterMap.put(decode(keyAndValue[0]),
					keyAndValue.length > 1 ? decode(keyAndValue[1]) : "");
		}

		return parameterMap;
	}

	private static String encode(final String value) {
		try {
			return URLEncoder.encode(value, "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			return "";
		}
	}

	public static String decode(final String value) {
		try {
			return URLDecoder.decode(value, "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			return "";
		}
	}

	private ViewParameters() {}
}
