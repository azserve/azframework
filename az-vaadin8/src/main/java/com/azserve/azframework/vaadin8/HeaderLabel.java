package com.azserve.azframework.vaadin8;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;

public class HeaderLabel extends Label {

	private static final long serialVersionUID = -2879612973232108854L;

	private String pendingText;
	private int level = 1;

	public HeaderLabel(final String value, final int level) {
		if (level < 1 || level > 6) {
			throw new IllegalArgumentException("Header levels 1-6 supported");
		}
		this.level = level;
		this.pendingText = value;
	}

	public HeaderLabel withStyleName(final String style) {
		this.addStyleName(style);
		return this;
	}

	public int getLevel() {
		return this.level;
	}

	@Override
	public void setValue(final String value) {
		this.pendingText = value;
		this.markAsDirty();
	}

	@Override
	public void beforeClientResponse(final boolean initial) {
		this.render();
		super.beforeClientResponse(initial);
	}

	private void render() {
		if (this.pendingText != null) {
			this.setContentMode(ContentMode.HTML);
			final StringBuilder sb = new StringBuilder("<h");
			sb.append(this.level);
			sb.append(">");
			sb.append(Jsoup.clean(this.pendingText, Whitelist.none()));
			sb.append("</h");
			sb.append(this.level);
			sb.append(">");
			super.setValue(sb.toString());
			this.pendingText = null;
		}
	}
}
