package com.azserve.azframework.vaadin8.components.factory;

import java.util.function.Function;

import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;

public class CssLayoutFactory extends AbstractLayoutFactory<CssLayout, CssLayoutFactory> {

	private final Function<Component, String> cssProvider;

	CssLayoutFactory() {
		this(c -> null);
	}

	CssLayoutFactory(final Function<Component, String> cssProvider) {
		super();
		this.cssProvider = cssProvider;
	}

	@Override
	public CssLayoutFactory add(final Component c) {
		this.get().addComponent(c);
		return this;
	}

	public CssLayoutFactory add(final Component c, final int index) {
		this.get().addComponent(c, index);
		return this;
	}

	@Override
	protected CssLayout supplyLayout() {
		return new CssLayout() {

			private static final long serialVersionUID = -1527003883189024066L;

			@Override
			protected String getCss(final Component c) {
				return CssLayoutFactory.this.cssProvider.apply(c);
			}
		};
	}
}
