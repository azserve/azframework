package com.azserve.azframework.vaadin8.components.factory;

import com.vaadin.ui.HorizontalLayout;

class HorizontalLayoutFactory extends OrderedLayoutFactory<HorizontalLayout> {

	HorizontalLayoutFactory() {
		super();
	}

	@Override
	protected HorizontalLayout supplyLayout() {
		return new HorizontalLayout();
	}
}
