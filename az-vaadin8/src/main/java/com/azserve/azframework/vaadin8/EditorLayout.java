package com.azserve.azframework.vaadin8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.funct.Functions;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;

/**
 * Componente che permette la gestione di una collezione di dati
 * modificabile tramite l'utilizzo di un {@linkplain GridLayout}
 * e implementa {@linkplain HasValue} per poter essere
 * collegato ad un binder.
 *
 * <p>
 * TODO : da testare bene e decidere se con {@linkplain EditorLayout#getValue() getValue()}
 * vanno restituite le righe vuote (o non correttamente validate)
 * </p>
 *
 * @author filippo
 * @since 27/02/2017
 *
 * @param <T> tipo di dato gestito.
 */
public class EditorLayout<T> extends CustomField<List<T>> {

	private static final long serialVersionUID = 3608609023730414853L;

	private final Map<T, BeanBinder<T>> binders = new IdentityHashMap<>();
	private List<T> internalValue;

	private Consumer<T> deleteHandler = Functions.nullConsumer();

	private T emptyBean;

	private final GridLayout layout;
	private final Class<T> beanType;
	private final Supplier<T> beanSupplier;
	private final BiFunction<T, BeanBinder<T>, Component[]> componentsFactory;
	private final String[] captions;

	public EditorLayout(final Class<T> beanType, final Supplier<T> beanSupplier, final BiFunction<T, BeanBinder<T>, Component[]> componentsFactory, final String[] captions) {
		this.beanType = beanType;
		this.beanSupplier = beanSupplier;
		this.componentsFactory = componentsFactory;
		this.captions = captions;
		this.layout = new GridLayout();
		this.layout.setSpacing(true);
		this.doSetValue(null);
	}

	/**
	 * Rigenera tutte le righe del layout richiamando
	 * l'apposita funzione per creare i componenti.
	 */
	public void refreshAll() {
		this.doSetValue(this.getValue());
	}

	@Override
	public List<T> getValue() {
		// TODO serve copiare su un nuovo Set?
		return this.internalValue != null ? new ArrayList<>(this.internalValue) : null;
	}

	@Override
	public void setWidth(final float width, final Unit unit) {
		super.setWidth(width, unit);
		if (this.layout != null) {
			if (width != -1) {
				this.layout.setWidth(100, Unit.PERCENTAGE);
			} else {
				this.layout.setWidth(null);
			}
		}
	}

	@NonNull
	public Consumer<T> getDeleteHandler() {
		return this.deleteHandler;
	}

	public void setDeleteHandler(final @NonNull Consumer<T> handler) {
		this.deleteHandler = handler;
	}

	@Override
	protected Component initContent() {
		return this.layout;
	}

	@Override
	public List<T> getEmptyValue() {
		return Collections.emptyList();
	}

	public void setMargin(final boolean enabled) {
		this.layout.setMargin(enabled);
	}

	@Override
	protected void doSetValue(final List<T> value) {
		if (this.layout == null) {
			return;
		}
		// rimuovo i componenti dal layout
		this.layout.removeAllComponents();

		// svuoto la mappa dei Binder
		final Iterator<Entry<T, BeanBinder<T>>> itBinders = this.binders.entrySet().iterator();
		while (itBinders.hasNext()) {
			itBinders.next().getValue().removeBean();
			itBinders.remove();
		}

		if (value == null) {
			this.internalValue = null;
			this.layout.setRows(2);
			this.addHeader();
			this.addEmptyBean(1);
		} else {
			this.layout.setRows(value.size() + 2);
			this.addHeader();
			int row = 1;
			this.internalValue = new ArrayList<>();
			for (final T bean : value) {
				this.addBean(bean, row++);
				this.internalValue.add(bean);
			}
			this.addEmptyBean(value.size() + 1);
		}
	}

	private void removeRow(final T bean, final int rowIndex) {
		if (this.emptyBean == bean) {
			return;
		}
		this.layout.removeRow(rowIndex);
		if (this.internalValue != null) {
			this.internalValue.remove(bean);
		}
		final BeanBinder<T> binder = this.binders.remove(bean);
		if (binder != null) {
			binder.removeBean();
		}
		this.deleteHandler.accept(bean);

		this.focusOnRow(rowIndex);
	}

	private List<T> ensureInternalValue() {
		if (this.internalValue == null) {
			this.internalValue = new ArrayList<>();
		}
		return this.internalValue;
	}

	//

	private void addEmptyBean(final int ix) {
		this.emptyBean = this.beanSupplier.get();
		this.addBean(this.emptyBean, ix);
	}

	private Component[] addBean(final T bean, final int row) {
		final BeanBinder<T> binder = new BeanBinder<>(this.beanType);

		binder.addStatusChangeListener(e -> {
			if (e.getBinder().getBean() == this.emptyBean && e.getBinder().isValid()) {
				this.ensureInternalValue().add(this.emptyBean);
				final int ix = this.layout.getRows();
				this.layout.insertRow(ix);
				this.addEmptyBean(ix);
			}
		});

		final Component[] rowComponents = this.componentsFactory.apply(bean, binder);

		int i = 0;
		// boolean focused = false;
		for (; i < rowComponents.length; i++) {
			final Component component = rowComponents[i];
			component.setCaption(null);
			this.layout.addComponent(component, i, row);
			// if (this.internalValue != null && !focused && component instanceof Focusable) {
			// ((Focusable) component).focus();
			// focused = true;
			// }
		}

		final Button delete = new Button(VaadinIcons.TRASH);
		delete.setTabIndex(-1);
		delete.addClickListener(e -> this.removeRow(bean, this.layout.getComponentArea(delete).getRow1()));
		this.layout.addComponent(delete, i, row);
		this.layout.setComponentAlignment(delete, Alignment.BOTTOM_RIGHT);

		binder.setBean(bean);

		this.binders.put(bean, binder);

		return rowComponents;
	}

	private void focusOnRow(final int rowIx) {
		int ix;
		if (this.layout.getRows() < rowIx) {
			ix = this.layout.getRows() - 1;
		} else {
			ix = rowIx;
		}
		for (int i = 0; i < this.layout.getColumns(); i++) {
			final Component cmp = this.layout.getComponent(i, ix);
			if (cmp instanceof Focusable) {
				((Focusable) cmp).focus();
				return;
			}
		}
	}

	private void addHeader() {
		this.layout.setColumns(Math.max(this.layout.getColumns(), this.captions.length + 1));
		for (int i = 0; i < this.captions.length; i++) {
			this.layout.addComponent(new Label(this.captions[i]), i, 0);
		}
	}
}
