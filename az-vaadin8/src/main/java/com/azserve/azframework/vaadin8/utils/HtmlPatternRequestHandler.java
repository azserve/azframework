package com.azserve.azframework.vaadin8.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.vaadin.server.StreamResource;
import com.vaadin.server.SynchronizedRequestHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinServletService;
import com.vaadin.server.VaadinSession;

public abstract class HtmlPatternRequestHandler
		extends SynchronizedRequestHandler {

	private static final long serialVersionUID = -7569927206800752382L;

	private static final Logger LOGGER = LoggerFactory.getLogger(HtmlPatternRequestHandler.class);

	private final String resourcesPath;

	public HtmlPatternRequestHandler(final String resourcesPathName) {
		this.resourcesPath = "/" + resourcesPathName + "/";
	}

	public String parseHtml(final String html) {
		return HtmlPatternParser.toHtmlContent(html, this::getImagePath);
	}

	public String toDatabaseHtml(final String html) {
		return HtmlPatternParser.toPatternContent(html,
				"[\"']" + Pattern.quote(baseUrl() + this.resourcesPath) + "([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})\\/[^\"']*[\"']",
				m -> UUID.fromString(m.group(1)));
	}

	public String getImagePath(final UUID uuid, final String fileName) {
		return baseUrl() + this.resourcesPath + uuid + "/" + fileName;
	}

	public String getImagePath(final UUID uuid) {
		return this.getImagePath(uuid, this.retrieveResourceName(uuid));
	}

	public abstract String retrieveResourceName(final @NonNull UUID uuid);

	public abstract ResourceData retrieveResourceData(final @NonNull UUID uuid);

	@Override
	public boolean synchronizedHandleRequest(final VaadinSession session, final VaadinRequest request, final VaadinResponse response) throws IOException {
		final String pathInfo = request.getPathInfo();
		if (pathInfo.startsWith(this.resourcesPath)) {

			final String[] parts = pathInfo.split("/");
			if (parts.length < 3) {
				LOGGER.warn("Invalid path info: \"" + pathInfo + "\"");
				return false;
			}
			try {
				// final String fileName = parts[parts.length - 1];
				final String uuid = parts[parts.length - 2];

				final ResourceData resourceData = this.retrieveResourceData(UUID.fromString(uuid));
				if (resourceData == null) {
					return false;
				}

				final StreamResource resource = new StreamResource(() -> new ByteArrayInputStream(resourceData.getContent()), resourceData.getTitle());
				resource.getStream().writeResponse(request, response);
				return true;
			} catch (final Exception ex) {
				LOGGER.warn("Error loading image", ex);
				return false;
			}
		}
		return false;
	}

	private static String baseUrl() {
		return VaadinServlet.getCurrent().getServletContext().getContextPath()
				+ VaadinServletService.getCurrentServletRequest().getServletPath();
	}

	protected static final class ResourceData {

		private final String title;
		private final byte[] content;

		public ResourceData(final String title, final byte[] content) {
			this.title = title;
			this.content = content;
		}

		public String getTitle() {
			return this.title;
		}

		public byte[] getContent() {
			return this.content;
		}
	}
}
