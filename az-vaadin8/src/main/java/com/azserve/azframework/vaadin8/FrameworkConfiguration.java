package com.azserve.azframework.vaadin8;

/**
 * Preferenze nel comportamento del framework.
 */
public class FrameworkConfiguration {

	public static boolean useFieldFullNameWhenGeneratingValidationErrorMessages = false;

	private FrameworkConfiguration() {}

}
