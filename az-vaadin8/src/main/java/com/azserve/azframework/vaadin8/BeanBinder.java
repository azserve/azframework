package com.azserve.azframework.vaadin8;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.util.BeanProperty;
import com.azserve.azframework.common.util.DateUtils;
import com.azserve.azframework.validation.NotBlank;
import com.azserve.azframework.validation.TemporalRange;
import com.azserve.azframework.vaadin8.fields.DoubleField;
import com.azserve.azframework.vaadin8.fields.NumberField;
import com.vaadin.data.BeanPropertySet;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.data.PropertyDefinition;
import com.vaadin.data.RequiredFieldConfigurator;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.server.SerializableFunction;
import com.vaadin.server.Setter;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component.Focusable;
import com.vaadin.ui.DateField;
import com.vaadin.ui.NativeSelect;

/**
 * Classe avanzata per effettuare il binding
 * tra componenti grafici e proprietà di bean.
 *
 * @author filippo
 * @since 10/02/2017
 *
 * @param <B> tipo di bean.
 */
public class BeanBinder<B> extends BeanValidationBinder<B> {

	private static final long serialVersionUID = -1444123456575772870L;
	private static final Logger LOGGER = LoggerFactory.getLogger(BeanBinder.class);

	private final Class<B> beanType;

	public BeanBinder(final Class<B> beanType) {
		super(beanType);
		this.beanType = beanType;

		// aggiungo le annotazioni NotEmpty e NotBlank tra quelle required
		this.setRequiredConfigurator(RequiredFieldConfigurator.DEFAULT.chain(annotation -> annotation.annotationType().equals(NotBlank.class))
				.chain(annotation -> "javax.validation.constraints.NotEmpty".equals(annotation.annotationType().getName()))
				.chain(annotation -> "javax.validation.constraints.NotBlank".equals(annotation.annotationType().getName())));
	}

	/**
	 * Effettua il binding con una proprietà del bean.
	 *
	 * <pre>
	 * beanBinder.bind(field, PersonDTO_.name);
	 * </pre>
	 *
	 * @param field componente grafico.
	 * @param property proprietà del bean.
	 * @param validator funzione per effettuare validazioni aggiuntive sul valore.
	 *
	 * @param <T> tipo della proprietà.
	 */
	@SuppressWarnings("unchecked")
	public <T> Binding<B, T> bind(final HasValue<T> field, final BeanProperty<? super B, ?, ?, T> property, final Validator<T> validator) {
		if (property.getParent() == null) {
			final BindingBuilder<B, T> bindingBuilder = this.forField(field).withValidator(validator);
			return bindingBuilder.bind(property.getName());
		}
		// solo un livello, se verà fatta una gestione a più livelli va rivista
		return this.bindNested((BeanProperty<? super B, ? super B, ? super B, T>) property, this.forField(field).withValidator(validator));
	}

	/**
	 * Effettua il binding con una proprietà del bean.
	 *
	 * <pre>
	 * beanBinder.bind(field, PersonDTO_.name);
	 * </pre>
	 *
	 * @param field componente grafico.
	 * @param property proprietà del bean.
	 *
	 * @param <T> tipo della proprietà.
	 */
	public <T> Binding<B, T> bind(final HasValue<T> field, final BeanProperty<? super B, ?, ?, T> property) {
		return this.bind(field, property, Validator.alwaysPass());
	}

	/**
	 * Effettua il binding con una proprietà del bean.
	 *
	 * @param field componente grafico.
	 * @param property proprietà del bean.
	 * @param toModel funzione per la conversione da valore
	 *            gestito dal componente al valore della proprietà.
	 * @param toPresentation funzione per la conversione
	 *            da valore della proprietà a valore gestito dal
	 *            componente.
	 * @param validator funzione per effettuare validazioni aggiuntive sul valore.
	 *
	 * @param <F> valore gestito dal componente.
	 * @param <T> tipo della proprietà innestata.
	 */
	@SuppressWarnings("unchecked")
	public <T, F> Binding<B, T> bind(final HasValue<F> field, final BeanProperty<? super B, ?, ?, T> property,
			final SerializableFunction<F, T> toModel, final SerializableFunction<T, F> toPresentation, final Validator<T> validator) {

		final BeanProperty<? super B, ?, ?, ?> parent = property.getParent();
		if (parent == null) {
			return this.forField(field)
					.withConverter(toModel, toPresentation)
					.withValidator(validator)
					.bind(property.getName());
		}
		// solo un livello, se verà fatta una gestione a più livelli va rivista
		return this.bindNested((BeanProperty<? super B, ? super B, ? super B, T>) property,
				this.forField(field).withConverter(toModel, toPresentation).withValidator(validator));
	}

	/**
	 * Effettua il binding con una proprietà del bean.
	 *
	 * @param field componente grafico.
	 * @param property proprietà del bean.
	 * @param toModel funzione per la conversione da valore
	 *            gestito dal componente al valore della proprietà.
	 * @param toPresentation funzione per la conversione
	 *            da valore della proprietà a valore gestito dal
	 *            componente.
	 *
	 * @param <F> valore gestito dal componente.
	 * @param <T> tipo della proprietà innestata.
	 */
	public <T, F> Binding<B, T> bind(final HasValue<F> field, final BeanProperty<? super B, ?, ?, T> property,
			final SerializableFunction<F, T> toModel, final SerializableFunction<T, F> toPresentation) {

		return this.bind(field, property, toModel, toPresentation, Validator.alwaysPass());
	}

	/**
	 * Effettua il binding con una proprietà di tipo {@code Date} del bean.
	 *
	 * @see #bind(HasValue, BeanProperty)
	 *
	 */
	public Binding<B, Date> bind(final DateField field, final BeanProperty<? super B, ?, ?, Date> property) {
		return this.bind(field, property, DateUtils::toDate, DateUtils::toLocalDate);
	}

	/**
	 * Gets the first bound focusable field having empty value, if present.
	 */
	public @NonNull Optional<Focusable> getFirstFocusableEmptyField() {
		return super.getFields().filter(HasValue::isEmpty).filter(f -> f instanceof Focusable)
				.map(f -> (Focusable) f).findFirst();
	}

	@Override
	protected Binder.BindingBuilder<B, ?> configureBinding(final Binder.BindingBuilder<B, ?> binding, final PropertyDefinition<B, ?> definition) {
		final BindingBuilder<B, ?> builder = super.configureBinding(binding, definition);
		this.configureField(this.beanType, false, builder, definition);
		return builder;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private <N, T> Binding<B, T> bindNested(final BeanProperty<? super B, ? super B, N, T> property, final BindingBuilder<B, T> bindingBuilder) {

		// da figlio a nonno
		final List<BeanProperty<? super B, ?, ?, ?>> propertyList = new ArrayList<>();
		BeanProperty<? super B, ?, ?, ?> property1 = property;
		while (property1.getParent() != null) {
			propertyList.add(property1);
			final BeanProperty<? super B, ?, ?, ?> parent = property1.getParent();
			property1 = parent;
		}
		propertyList.add(property1);

		// da nonno a figlio
		final List<PropertyDefinition> propertyDefinitionList = new ArrayList<>();
		for (int i = propertyList.size() - 1; i >= 0; i--) {
			final BeanProperty<? super B, ?, ?, ?> p = propertyList.get(i);
			final PropertyDefinition<?, ?> propertyDefinition = BeanPropertySet.get(p.getBeanType())
					.getProperty(p.getName())
					.orElseThrow(() -> new IllegalArgumentException(
							"Could not resolve property name " + p.getName()
									+ " from " + p.getBeanType()));
			propertyDefinitionList.add(propertyDefinition);
		}

		bindingBuilder.withValidator(new BeanValidator(property.getType(), property.getName()));

		// applico le mie personalizzazioni sul campo
		this.configureField(property.getParent().getType(), true, bindingBuilder, propertyDefinitionList.get(propertyDefinitionList.size() - 1));

		// effettuo il binding tramite getter e setter passando per la proprietà padre
		return bindingBuilder
				.bind(bean -> {
					Object temp = bean;
					for (final PropertyDefinition propertyDefinition : propertyDefinitionList) {
						if (temp == null) {
							return null;
						}
						temp = propertyDefinition.getGetter().apply(temp);
					}
					return (T) temp;
				}, (bean, value) -> {
					Object temp = bean;
					for (int i = 0; i < propertyDefinitionList.size() - 1; i++) {
						temp = propertyDefinitionList.get(i).getGetter().apply(temp);
					}
					final Optional<Setter> setter = propertyDefinitionList.get(propertyDefinitionList.size() - 1).getSetter();
					final Object _temp = temp;
					setter.ifPresent(s -> s.accept(_temp, value));
				});
	}

	protected void configureField(final Class<?> type, final boolean requiredIndicator, final Binder.BindingBuilder<?, ?> binding, final PropertyDefinition<?, ?> propertyDefinition) {

		final HasValue<?> field = binding.getField();

		try {
			final Field declaredField = findDeclaredField(propertyDefinition.getName(), type);

			if (requiredIndicator) {
				final RequiredFieldConfigurator requiredConfigurator = this.getRequiredConfigurator();
				if (requiredConfigurator != null) {
					if (field instanceof ComboBox || field instanceof NativeSelect) {
						for (final Annotation annotation : declaredField.getAnnotations()) {
							if (requiredConfigurator.test(annotation)) {
								field.setRequiredIndicatorVisible(true);
								if (field instanceof ComboBox) {
									((ComboBox<?>) field).setEmptySelectionAllowed(false);
								} else if (field instanceof NativeSelect) {
									((NativeSelect<?>) field).setEmptySelectionAllowed(false);
								}
								break;
							}
						}
					}
				}
			}

			final Class<?> propertyType = propertyDefinition.getType();
			if (propertyType == String.class && field instanceof AbstractTextField) {
				final Size sizeAnnotation = declaredField.getAnnotation(Size.class);
				if (sizeAnnotation != null) {
					((AbstractTextField) field).setMaxLength(sizeAnnotation.max());
				}
			} else if (Number.class.isAssignableFrom(propertyType) && field instanceof NumberField) {
				final Min minAnnotation = declaredField.getAnnotation(Min.class);
				if (minAnnotation != null) {
					((NumberField<?>) field).setMinAllowedValue(BigDecimal.valueOf(minAnnotation.value()));
				}
				final Max maxAnnotation = declaredField.getAnnotation(Max.class);
				if (maxAnnotation != null) {
					((NumberField<?>) field).setMaxAllowedValue(BigDecimal.valueOf(maxAnnotation.value()));
				}
				final Digits digitsAnnotation = declaredField.getAnnotation(Digits.class);
				if (digitsAnnotation != null && field instanceof DoubleField) {
					((DoubleField) field).getFormatter().setMaximumFractionDigits(digitsAnnotation.fraction());
				}
			} else if (field instanceof DateField) {
				configureDateField((DateField) field, declaredField);
			}
		} catch (final Exception ex) {
			LOGGER.warn("Class field {} not found on {}", propertyDefinition.getName(), type, ex);
		}
	}

	private static void configureDateField(final DateField field, final Field declaredField) {
		final Past pastAnnotation = declaredField.getAnnotation(Past.class);
		if (pastAnnotation != null) {
			field.setRangeEnd(LocalDate.now());
		}
		final Future futureAnnotation = declaredField.getAnnotation(Future.class);
		if (futureAnnotation != null) {
			field.setRangeStart(LocalDate.now().plusDays(1));
		}
		final TemporalRange temporalRangeAnnotation = declaredField.getAnnotation(TemporalRange.class);
		if (temporalRangeAnnotation != null) {
			final int back = temporalRangeAnnotation.back();
			final int forward = temporalRangeAnnotation.forward();
			if (back >= 0 || forward >= 0) {
				final ChronoUnit unit = temporalRangeAnnotation.unit();
				if (back >= 0) {
					field.setRangeStart(LocalDateTime.now().minus(back, unit).toLocalDate());
				}
				if (forward >= 0) {
					final LocalDate today = LocalDate.now();
					final LocalDate rangeStart = LocalDateTime.now().plus(forward, unit).toLocalDate();
					field.setRangeEnd(rangeStart.isEqual(today) ? rangeStart.plusDays(1) : rangeStart);
				}
			}
		}
	}

	private static Field findDeclaredField(final String property, final Class<?> type) throws NoSuchFieldException, SecurityException {
		final int ix = property.indexOf('.');
		if (ix < 0) {
			try {
				final Field declaredField = type.getDeclaredField(property);
				if (!declaredField.isAccessible()) {
					declaredField.setAccessible(true);
				}
				return declaredField;
			} catch (final NoSuchFieldException e) {
				if (type.getSuperclass() == null) {
					throw e;
				}
				return findDeclaredField(property, type.getSuperclass());
			}
		}
		final Field firstField = findDeclaredField(property.substring(0, ix), type);
		return findDeclaredField(property.substring(ix + 1), firstField.getType());
	}
}
