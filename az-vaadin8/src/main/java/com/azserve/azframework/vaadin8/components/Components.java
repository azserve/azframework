package com.azserve.azframework.vaadin8.components;

import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

/**
 * Classe di utilità per la generazione
 * di {@linkplain Component} generici.
 *
 * @author filippo
 * @since 15/02/2017
 */
public final class Components {

	public static Component hspacer() {
		final Label l = new Label();
		l.setWidth(100, Unit.PERCENTAGE);
		l.setHeightUndefined();
		return l;
	}

	public static Component vspacer() {
		final Label l = new Label();
		l.setWidthUndefined();
		l.setHeight(100, Unit.PERCENTAGE);
		return l;
	}

	private Components() {}
}
