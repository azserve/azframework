package com.azserve.azframework.vaadin8.theme;

/**
 * Classi CSS generali di utilità.
 *
 * @author filippo
 * @since 15/02/2017
 */
public final class Styles {

	public static final String HIDDEN = "hidden";
	public static final String MARGINS = "margins";
	public static final String PADDING = "v-margin-left v-margin-top v-margin-bottom v-margin-right";
	public static final String FORCE_SCROLL = "force-scroll";
	public static final String TEXT_ALIGN_CENTER = "text-align-center";
	public static final String TEXT_ALIGN_RIGHT = "text-align-right";
	public static final String FONT_VAADIN_ICONS = "font-vaadin-icons";

	private Styles() {}
}
