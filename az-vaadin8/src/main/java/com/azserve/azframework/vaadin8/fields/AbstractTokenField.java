package com.azserve.azframework.vaadin8.fields;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.azserve.azframework.common.annotation.NonNull;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Component.Focusable;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.ItemCaptionGenerator;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.util.ReflectTools;

public abstract class AbstractTokenField<T, F extends HasValue<T> & Component & Focusable> extends CustomField<Set<T>> {

	private static final long serialVersionUID = 2581741956821401057L;

	private final Map<T, Button> internalValue = new HashMap<>();

	protected final CssLayout content;
	protected final F field;
	private MenuBar menu;

	private ItemCaptionGenerator<T> itemCaptionGenerator;

	public AbstractTokenField(final String caption, final ItemCaptionGenerator<T> itemCaptionGenerator, final F field) {
		this.itemCaptionGenerator = itemCaptionGenerator;
		this.content = new CssLayout();
		this.field = field;
		this.setCaption(caption);
		this.addStyleName("token-field");
	}

	private Button createButton(final T value) {
		final Button btn = this.createButton(this.itemCaptionGenerator.apply(value), VaadinIcons.CLOSE_SMALL);
		btn.addClickListener(e -> {
			if (!this.isReadOnly()) {
				this.content.removeComponent(btn);
				this.getValue().remove(value);
				this.fireEvent(new TokenChangeEvent<>(this, this, value, false));
				this.field.focus();
			}
		});
		return btn;
	}

	private Button createAllButton() {
		final Button btn = this.createButton("Tutto", VaadinIcons.CLOSE_SMALL);
		btn.addClickListener(e -> {
			if (!this.isReadOnly()) {
				this.getValue().clear();
				// TODO fireEvent
				this.clearContent();
				this.createButtons(this.getValue());
				this.field.focus();
			}
		});
		return btn;
	}

	protected Button createButton(final String caption, final VaadinIcons icon) {
		return new Button(caption, icon);
	}

	public MenuItem getMenu() {
		if (this.menu == null) {
			this.menu = new MenuBar();
			this.content.addComponent(this.menu, 0);
			this.menu.addItem("", null);
		}
		return this.menu.getItems().get(0);
	}

	public ItemCaptionGenerator<T> getItemCaptionGenerator() {
		return this.itemCaptionGenerator;
	}

	public void setItemCaptionGenerator(final ItemCaptionGenerator<T> itemCaptionGenerator) {
		this.itemCaptionGenerator = itemCaptionGenerator;

		this.clearContent();

		this.createButtons(this.getValue());
	}

	@Override
	public Set<T> getValue() {
		return this.internalValue.keySet();
	}

	@Override
	public boolean isEmpty() {
		return this.internalValue.isEmpty();
	}

	@Override
	public Set<T> getEmptyValue() {
		return Collections.emptySet();
	}

	public void remove(final T value) {
		final Button btn = this.internalValue.get(value);
		if (btn != null) {
			this.content.removeComponent(btn);
		}
		if (this.internalValue.remove(value) != null) {
			this.fireEvent(new TokenChangeEvent<>(this, this, value, false));
		}
	}

	public Registration addTokenChangeListener(final TokenChangeListener<T> listener) {
		return this.addListener(TokenChangeEvent.class, listener, TokenChangeListener.TOKEN_CHANGE_METHOD);
	}

	protected T fieldValueToToken(final @NonNull T value) {
		return value;
	}

	/**
	 * @param value
	 */
	protected boolean isSelectAll(final @NonNull Set<T> value) {
		return false;
	}

	@Override
	protected Component initContent() {
		this.content.addComponent(this.field);

		this.field.addValueChangeListener(e -> {
			final T value = e.getValue();
			if (value == null) {
				return;
			}
			final T token = this.fieldValueToToken(value);
			if (token == null) {
				return;
			}
			final Map<T, Button> tokens = this.internalValue;
			if (!tokens.containsKey(token)) {
				final Button btn = this.createButton(token);
				tokens.put(token, btn);
				this.content.addComponent(btn);
				this.fireEvent(new TokenChangeEvent<>(this, this, token, true));
			}
			if (this.isSelectAll(tokens.keySet())) {
				this.clearContent();
				final Button selectAllButton = this.createAllButton();
				this.content.addComponent(selectAllButton);
				selectAllButton.focus();
			} else {
				this.field.focus();
			}
			this.field.clear();
		});

		return this.content;
	}

	@Override
	protected void doSetValue(final Set<T> value) {
		this.clearContent();
		this.getValue().clear();

		this.createButtons(value);
	}

	private void clearContent() {
		this.content.removeAllComponents();
		if (this.menu != null) {
			this.content.addComponent(this.menu);
		}
	}

	private void createButtons(final Set<T> value) {
		if (value != null) {
			if (this.isSelectAll(value)) {
				this.content.addComponent(this.createAllButton());
				for (final T token : value) {
					this.internalValue.put(token, null);
				}
			} else {
				this.content.addComponent(this.field);
				for (final T token : value) {
					final Button btn = this.createButton(token);
					this.internalValue.put(token, btn);
					this.content.addComponent(btn);
				}
			}
		}
	}

	// events

	public interface TokenChangeListener<V> {

		final Method TOKEN_CHANGE_METHOD = ReflectTools.findMethod(
				TokenChangeListener.class, "tokenChanged",
				TokenChangeEvent.class);

		void tokenChanged(TokenChangeEvent<V> event);
	}

	public static class TokenChangeEvent<V> extends EventObject {

		private static final long serialVersionUID = -4570036752625256025L;

		private final Component component;

		private final V token;
		private final boolean added;

		public TokenChangeEvent(final Component component, final HasValue<Set<V>> hasValue, final V token, final boolean added) {
			super(hasValue);
			this.component = component;
			this.token = token;
			this.added = added;
		}

		public V getToken() {
			return this.token;
		}

		public boolean isAdded() {
			return this.added;
		}

		public Component getComponent() {
			return this.component;
		}

		@SuppressWarnings("unchecked")
		@Override
		public HasValue<Set<V>> getSource() {
			return (HasValue<Set<V>>) super.getSource();
		}
	}
}
