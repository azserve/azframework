package com.azserve.azframework.vaadin8.components.factory;

import com.vaadin.ui.FormLayout;

class FormLayoutFactory extends OrderedLayoutFactory<FormLayout> {

	FormLayoutFactory() {
		super();
	}

	@Override
	protected FormLayout supplyLayout() {
		return new FormLayout();
	}
}
