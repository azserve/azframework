package com.azserve.azframework.vaadin8.fields;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.vaadin8.utils.Utils;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.ComboBox;

public class ComboBoxTokenField<T> extends AbstractTokenField<T, ComboBox<T>> {

	private static final long serialVersionUID = 2581741956821401057L;

	private int selectAllThreshold = 1;

	public ComboBoxTokenField(final String caption, final ComboBoxCustomField<T> comboBox) {
		this(caption, comboBox.getField());
	}

	public ComboBoxTokenField(final String caption, final ComboBox<T> comboBox) {
		super(caption, item -> comboBox.getItemCaptionGenerator().apply(item), comboBox);
	}

	/**
	 * Aggiunge i sottomenù per la selezione (tutti/nessuno).
	 */
	public void addSelectionOptions() {
		this.getMenu().addItem("Seleziona tutto", e -> this.setValue(new HashSet<>(Utils.getItems(this.field.getDataProvider()))));
		this.getMenu().addItem("Seleziona nessuno", e -> this.setValue(Collections.emptySet()));
	}

	@Override
	protected boolean isSelectAll(final @NonNull Set<T> value) {
		final DataProvider<T, ?> dataProvider = this.field.getDataProvider();
		if (dataProvider instanceof ListDataProvider) {
			final int size = value.size();
			return size > this.selectAllThreshold && size >= Utils.getItems(dataProvider).size();
		}
		return false;
	}

	public int getSelectAllThreshold() {
		return this.selectAllThreshold;
	}

	public void setSelectAllThreshold(final int selectAllThreshold) {
		this.selectAllThreshold = selectAllThreshold;
	}

}