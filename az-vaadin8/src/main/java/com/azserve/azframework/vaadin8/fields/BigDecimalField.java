package com.azserve.azframework.vaadin8.fields;

import java.math.BigDecimal;

import com.azserve.azframework.common.annotation.NonNull;

public class BigDecimalField extends DecimalField<BigDecimal> {

	private static final long serialVersionUID = -1908129220386864689L;

	public BigDecimalField() {
		super();
	}

	public BigDecimalField(final String caption) {
		super(caption);
	}

	@Override
	protected @NonNull BigDecimal convertFromBigDecimal(final @NonNull BigDecimal value) {
		return value;
	}
}
