package com.azserve.azframework.vaadin8.fields;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Comparator;
import java.util.Objects;

import org.vaadin.viritin.util.HtmlElementPropertySetter;

import com.vaadin.data.ValidationResult;
import com.vaadin.data.ValueContext;
import com.vaadin.data.validator.RangeValidator;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.TextField;

public class TimeField extends CustomField<LocalTime> {

	private static final long serialVersionUID = -9175966347827080263L;

	protected final TextField field = new TextField() {

		private static final long serialVersionUID = -2219620947457308947L;

		@Override
		public void beforeClientResponse(final boolean initial) {
			super.beforeClientResponse(initial);
			TimeField.this.htmlSetter.setProperty("type", "time");
		}
	};
	private final HtmlElementPropertySetter htmlSetter = new HtmlElementPropertySetter(this.field);

	private String timeOutOfRangeMessage = "Time is out of allowed range";

	private LocalTime internalValue;

	private LocalTime minAllowedValue;
	private LocalTime maxAllowedValue;

	public TimeField() {
		this(null);
	}

	public TimeField(final String caption) {
		super();
		this.setSizeUndefined();
		this.setCaption(caption);
		this.setFocusDelegate(this.field);
		this.field.setValueChangeMode(ValueChangeMode.BLUR);
		this.field.addValueChangeListener(e -> {
			final LocalTime convertedValue = this.convertToValue(e.getValue());
			final boolean userOriginated = e.isUserOriginated();

			if (!userOriginated || this.isInRange(convertedValue)) {
				/*
				 * Cambio il valore se:
				 * non è stato inserito dall'utente
				 * oppure è stato inserito dall'utente e rispetta il range di valori
				 */
				this.setValue(convertedValue, userOriginated);
			}

			this.field.setValue(this.convertToText(this.getValue()));
		});
	}

	@Override
	public boolean isReadOnly() {
		return this.field.isReadOnly();
	}

	@Override
	public void setReadOnly(final boolean readOnly) {
		this.field.setReadOnly(readOnly);
	}

	@Override
	public LocalTime getValue() {
		return this.internalValue;
	}

	@Override
	protected Component initContent() {
		return this.field;
	}

	@Override
	public void attach() {
		super.attach();
		this.field.setValue(this.convertToText(this.getValue()));
	}

	@Override
	protected void doSetValue(final LocalTime value) {
		this.internalValue = value;
		if (this.isAttached()) {
			this.field.setValue(this.convertToText(value));
		}
	}

	@Override
	public void setWidth(final float width, final Unit unit) {
		super.setWidth(width, unit);
		if (this.field != null) {
			if (width != -1) {
				this.field.setWidth(100, Unit.PERCENTAGE);
			} else {
				this.field.setWidth(null);
			}
		}
	}

	public LocalTime getMinAllowedValue() {
		return this.minAllowedValue;
	}

	public void setMinAllowedValue(final LocalTime value) {
		if (value != null && this.maxAllowedValue != null && value.compareTo(this.maxAllowedValue) > 0) {
			throw new IllegalArgumentException(
					String.format("min time (%s) is greater than max time (%s)", value, this.maxAllowedValue));
		}
		this.minAllowedValue = value;
		this.htmlSetter.setProperty("min", value);
	}

	public LocalTime getMaxAllowedValue() {
		return this.maxAllowedValue;
	}

	public void setMaxAllowedValue(final LocalTime value) {
		if (value != null && this.minAllowedValue != null && value.compareTo(this.minAllowedValue) < 0) {
			throw new IllegalArgumentException(
					String.format("min time (%s) is greater than max time (%s)", this.minAllowedValue, value));
		}
		this.maxAllowedValue = value;
		this.htmlSetter.setProperty("max", value);
	}

	public String getTimeOutOfRangeMessage() {
		return this.timeOutOfRangeMessage;
	}

	public void setTimeOutOfRangeMessage(final String timeOutOfRangeMessage) {
		this.timeOutOfRangeMessage = timeOutOfRangeMessage;
	}

	public String getPlaceholder() {
		return this.field.getPlaceholder();
	}

	public void setPlaceholder(final String placeholder) {
		this.field.setPlaceholder(placeholder);
	}

	public void selectAll() {
		this.field.selectAll();
	}

	private boolean isInRange(final LocalTime value) {
		final RangeValidator<LocalTime> validator = this.getRangeValidator();
		final ValidationResult result = validator.apply(value, new ValueContext(this));
		return !result.isError();
	}

	protected RangeValidator<LocalTime> getRangeValidator() {
		return new TimeRangeValidator(
				this.getTimeOutOfRangeMessage(),
				this.getMinAllowedValue(),
				this.getMaxAllowedValue());
	}

	private LocalTime convertToValue(final String text) {
		if (text != null) {
			try {
				return LocalTime.parse(text);
			} catch (final DateTimeParseException ex) {
				// ignoring
			}
		}
		return null;
	}

	private String convertToText(final LocalTime value) {
		return Objects.toString(value, "");
	}

	public static class TimeRangeValidator extends RangeValidator<LocalTime> {

		private static final long serialVersionUID = -7878198417179492399L;

		/**
		 * Creates a validator for checking that a LocalTime is within a given
		 * range.
		 * <p>
		 * By default the range is inclusive i.e. both minValue and maxValue are
		 * valid values. Use {@link #setMinValueIncluded(boolean)} or
		 * {@link #setMaxValueIncluded(boolean)} to change it.
		 * </p>
		 *
		 * @param errorMessage
		 *            the message to display in case the value does not validate.
		 * @param minValue
		 *            The minimum value to accept or null for no limit
		 * @param maxValue
		 *            The maximum value to accept or null for no limit
		 */
		public TimeRangeValidator(final String errorMessage, final LocalTime minValue, final LocalTime maxValue) {
			super(errorMessage, Comparator.naturalOrder(), minValue, maxValue);
		}
	}
}
