package com.azserve.azframework.vaadin8.components.factory;

import com.vaadin.server.Resource;
import com.vaadin.server.Sizeable;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.HasComponents.ComponentAttachDetachNotifier;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;

abstract class AbstractLayoutFactory<T extends AbstractComponent & HasComponents & ComponentAttachDetachNotifier, F extends AbstractLayoutFactory<T, ?>> {

	private T layout;
	protected Component last;

	AbstractLayoutFactory() {}

	AbstractLayoutFactory(final T layout) {
		this.layout = layout;
	}

	public F withCaption(final String caption) {
		this.get().setCaption(caption);
		return this.getThis();
	}

	public F withCaption(final String caption, final boolean html) {
		this.get();
		this.layout.setCaptionAsHtml(html);
		this.layout.setCaption(caption);
		return this.getThis();
	}

	public F withDescription(final String description) {
		this.get().setDescription(description);
		return this.getThis();
	}

	public F withDescription(final String description, final ContentMode contentMode) {
		this.get().setDescription(description, contentMode);
		return this.getThis();
	}

	public F withIcon(final Resource icon) {
		this.get().setIcon(icon);
		return this.getThis();
	}

	public F withId(final String id) {
		this.get().setId(id);
		return this.getThis();
	}

	public F withHeight(final float width, final Sizeable.Unit unit) {
		this.get().setHeight(width, unit);
		return this.getThis();
	}

	public F withHeightFull() {
		this.get().setHeight(100, Sizeable.Unit.PERCENTAGE);
		return this.getThis();
	}

	public F withHeightUndefinied() {
		this.get().setHeightUndefined();
		return this.getThis();
	}

	public F withWidth(final float width, final Sizeable.Unit unit) {
		this.get().setWidth(width, unit);
		return this.getThis();
	}

	public F withWidthFull() {
		this.get().setWidth(100, Sizeable.Unit.PERCENTAGE);
		return this.getThis();
	}

	public F withWidthUndefinied() {
		this.get().setWidthUndefined();
		return this.getThis();
	}

	public F withSize(final float width, final float height, final Sizeable.Unit unit) {
		this.get();
		this.layout.setWidth(width, unit);
		this.layout.setHeight(height, unit);
		return this.getThis();
	}

	public F withSizeFull() {
		this.get().setSizeFull();
		return this.getThis();
	}

	public F withSizeUndefinied() {
		this.get().setSizeUndefined();
		return this.getThis();
	}

	public F withStyle(final String style) {
		this.get().addStyleName(style);
		return this.getThis();
	}

	public F disabled() {
		this.get().setEnabled(false);
		return this.getThis();
	}

	public F invisible() {
		this.get().setVisible(false);
		return this.getThis();
	}

	public abstract F add(Component c);

	public F add(final String labelText) {
		return this.add(new Label(labelText));
	}

	public F add(final Component... components) {
		for (final Component c : components) {
			this.add(c);
		}
		return this.getThis();
	}

	public F add(final String labelText, final Component... components) {
		this.add(labelText);
		this.add(components);
		return this.getThis();
	}

	/**
	 * Restituisce il layout generato.
	 *
	 * Tramite questa factory non sarà più possibile
	 * intervenire sul layout restituito.
	 *
	 * @return il layout generato.
	 */
	public T build() {
		final T result = this.get();
		this.layout = null;
		this.last = null;
		return result;
	}

	/**
	 * Restituisce il layout all'interno di un pannello,
	 * il quale eredita caption ed icon.
	 *
	 * Tramite questa factory non sarà più possibile
	 * intervenire sul layout restituito.
	 *
	 * @return un nuovo oggetto {@linkplain Panel} contenente
	 *         il layout generato.
	 */
	public Panel buildWithPanel() {
		final Panel panel = new Panel();
		final T l = this.build();
		panel.setCaption(l.getCaption());
		panel.setCaptionAsHtml(l.isCaptionAsHtml());
		panel.setIcon(l.getIcon());
		l.setCaption(null);
		l.setIcon(null);
		panel.setContent(l);
		return panel;
	}

	/**
	 * Inserisce il layout all'interno di un pannello,
	 * il quale eredita caption ed icon.
	 *
	 * Viene restituita una factory per agire sul
	 * pannello.
	 *
	 * Tramite questa factory non sarà più possibile
	 * intervenire sul layout restituito.
	 *
	 * @return un nuovo oggetto {@linkplain PanelFactory} per
	 *         agire sul {@linkplain Panel} contenente il layout generato.
	 */
	public PanelFactory toPanel() {
		return new PanelFactory(this.buildWithPanel());
	}

	protected abstract T supplyLayout();

	@SuppressWarnings("unchecked")
	private F getThis() {
		return (F) this;
	}

	protected final T get() {
		if (this.layout == null) {
			this.layout = this.supplyLayout();
			this.layout.addComponentAttachListener(e -> this.last = e.getAttachedComponent());
			this.layout.addComponentDetachListener(e -> {
				if (this.last == e.getDetachedComponent()) {
					this.last = null;
				}
			});
		}
		return this.layout;
	}
}
