package com.azserve.azframework.vaadin8.fields;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.lang.StringUtils;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.TextField;

public class TextFieldTokenField extends AbstractTokenField<String, TextField> {

	private static final long serialVersionUID = -9222945597440236510L;

	public TextFieldTokenField(final String caption, final TextField textField) {
		super(caption, s -> s, textField);
		textField.setValueChangeMode(ValueChangeMode.BLUR);
	}

	@Override
	protected String fieldValueToToken(final @NonNull String value) {
		return StringUtils.trimToNull(value);
	}
}
