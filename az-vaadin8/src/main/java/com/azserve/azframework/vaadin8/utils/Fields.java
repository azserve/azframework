package com.azserve.azframework.vaadin8.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

import com.azserve.azframework.common.enums.EnumWithDescription;
import com.azserve.azframework.common.lang.BooleanUtils;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.RadioButtonGroup;

public class Fields {

	/**
	 * Restituisce un {@code ComboBox} contenente tutti i valori
	 * della enum specificata.
	 *
	 * @param enumClass classe della enum.
	 * @param caption caption del componente.
	 */
	public static <E extends Enum<E>> ComboBox<E> enumComboBox(final Class<E> enumClass, final String caption) {
		final ComboBox<E> comboBox = new ComboBox<>(caption);
		if (EnumWithDescription.class.isAssignableFrom(enumClass)) {
			comboBox.setItemCaptionGenerator(e -> ((EnumWithDescription) e).description());
		}
		comboBox.setItems(enumClass.getEnumConstants());
		return comboBox;
	}

	/**
	 * Imposta il {@code ComboBox} specificato in modo da
	 * permettere l'aggiunta di valori selezionabili.
	 *
	 * @param comboBox il componente da inizializzare.
	 * @param items i valori proposti.
	 * @param newItemProvider funzione che a partire
	 *            dal testo inserito dall'utente e la collezione
	 *            di valori da proporre restituisce il
	 *            valore da aggiungere alla collezione oppure
	 *            {@code null} se non va aggiunto alcun valore.
	 */
	public static <T> void initEditableComboBox(final ComboBox<T> comboBox, final Collection<T> items, final BiFunction<String, Collection<T>, T> newItemProvider) {
		comboBox.setDataProvider(DataProvider.ofCollection(items).filteringBy((t, q) -> true));
		comboBox.setNewItemProvider(text -> {
			final List<T> l = new ArrayList<>(items);
			final T value = newItemProvider.apply(text, l);
			if (value != null) {
				l.add(value);
				comboBox.setDataProvider(DataProvider.ofCollection(l).filteringBy((t1, q) -> true));
				comboBox.setSelectedItem(value);
				return Optional.of(value);
			}
			return Optional.empty();
		});
		// comboBox.setNewItemHandler(e -> {
		// final List<T> l = new ArrayList<>(items);
		// final T t = newItemProvider.apply(e, l);
		// if (t != null) {
		// l.add(t);
		// comboBox.setDataProvider(DataProvider.ofCollection(l).filteringBy((t1, q) -> true));
		// comboBox.setSelectedItem(t);
		// }
		// });
	}

	/**
	 * Imposta il {@code ComboBox} specificato in modo da
	 * permettere l'inserimento di valori di tipo {@code String}.
	 *
	 * @param comboBox il componente da inizializzare.
	 * @param items le stringhe proposte.
	 */
	public static void initStringEditableComboBox(final ComboBox<String> comboBox, final Collection<String> items) {
		initEditableComboBox(comboBox, items, (s, c) -> s);
	}

	/**
	 * Restituisce un {@code ComboBox} che permette
	 * la selezione di 3 stati.
	 *
	 * @param caption caption del componente.
	 * @param nullCaption caption da utilizzare per il valore {@code null}.
	 * @param trueCaption caption da utilizzare per il valore {@code true}.
	 * @param falseCaption caption da utilizzare per il valore {@code false}.
	 */
	public static ComboBox<Boolean> booleanComboBox(final String caption, final String nullCaption, final String trueCaption, final String falseCaption) {
		final ComboBox<Boolean> rbg = booleanComboBox(nullCaption, trueCaption, falseCaption);
		rbg.setCaption(caption);
		return rbg;
	}

	/**
	 * Restituisce un {@code ComboBox} che permette
	 * la selezione di 3 stati.
	 *
	 * @param nullCaption caption da utilizzare per il valore {@code null}.
	 * @param trueCaption caption da utilizzare per il valore {@code true}.
	 * @param falseCaption caption da utilizzare per il valore {@code false}.
	 */
	public static ComboBox<Boolean> booleanComboBox(final String nullCaption, final String trueCaption, final String falseCaption) {
		final ComboBox<Boolean> rbg = new ComboBox<>();
		rbg.setItems(Boolean.TRUE, Boolean.FALSE);
		rbg.setEmptySelectionAllowed(true);
		rbg.setEmptySelectionCaption(nullCaption);
		rbg.setItemCaptionGenerator(b -> BooleanUtils.toString(b, nullCaption, trueCaption, falseCaption));
		return rbg;
	}

	/**
	 * Restituisce un {@code RadioButtonGroup} che permette
	 * la selezione di 3 stati.
	 *
	 * @param caption caption del componente.
	 * @param trueCaption caption da utilizzare per il valore {@code true}.
	 * @param falseCaption caption da utilizzare per il valore {@code false}.
	 */
	public static RadioButtonGroup<Boolean> booleanRadioButton(final String caption, final String trueCaption, final String falseCaption) {
		final RadioButtonGroup<Boolean> rbg = booleanRadioButton(trueCaption, falseCaption);
		rbg.setCaption(caption);
		return rbg;
	}

	/**
	 * Restituisce un {@code RadioButtonGroup} che permette
	 * la selezione di 3 stati.
	 *
	 * @param trueCaption caption da utilizzare per il valore {@code true}.
	 * @param falseCaption caption da utilizzare per il valore {@code false}.
	 */
	public static RadioButtonGroup<Boolean> booleanRadioButton(final String trueCaption, final String falseCaption) {
		final RadioButtonGroup<Boolean> rbg = new RadioButtonGroup<>();
		rbg.setItems(Boolean.TRUE, Boolean.FALSE);
		rbg.setItemCaptionGenerator(b -> BooleanUtils.toString(b, null, trueCaption, falseCaption));
		return rbg;
	}

	private Fields() {}
}
