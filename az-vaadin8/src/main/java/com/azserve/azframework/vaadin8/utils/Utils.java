package com.azserve.azframework.vaadin8.utils;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.lang.StringUtils;
import com.azserve.azframework.common.web.Html;
import com.azserve.azframework.exception.AzGenericException;
import com.azserve.azframework.exception.AzInvalidDataException;
import com.azserve.azframework.interfaces.service.Communication;
import com.azserve.azframework.interfaces.service.exception.AzException;
import com.azserve.azframework.interfaces.service.exception.AzServiceDetailsException;
import com.azserve.azframework.interfaces.service.exception.ExceptionMessageKey;
import com.azserve.azframework.validation.Field;
import com.azserve.azframework.validation.ValidationError;
import com.azserve.azframework.vaadin8.FrameworkConfiguration;
import com.azserve.azframework.vaadin8.utils.el.FormatterWrapper;
import com.azserve.azframework.vaadin8.utils.el.RootResolver;
import com.azserve.azframework.vaadin8.utils.el.SimpleELContext;
import com.vaadin.data.HasValue;
import com.vaadin.data.ValidationException;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.FontIcon;
import com.vaadin.server.Page;
import com.vaadin.ui.Component;
import com.vaadin.ui.Component.Focusable;
import com.vaadin.ui.HasComponents;

public final class Utils {

	private Utils() {}

	@SuppressWarnings("unchecked")
	public static <T> Collection<T> getItems(final DataProvider<T, ?> dataProvider) {
		if (dataProvider instanceof ListDataProvider) {
			return ((ListDataProvider<T>) dataProvider).getItems();
		}
		throw new IllegalStateException("Can call getItems() only if DataProvider is a ListDataProvider instance");
	}

	public static boolean isMobile() {
		return Page.getCurrent().getBrowserWindowWidth() < 640;
	}

	public static boolean isFullHD() {
		return Page.getCurrent().getBrowserWindowWidth() > 1700;
	}

	public static String htmlStyledIcon(final FontIcon fontIcon, final String additionalStyles) {
		return "<span class=\"v-icon\" style=\"font-family: "
				+ fontIcon.getFontFamily() + ";"
				+ additionalStyles + "\">&#x" + Integer.toHexString(fontIcon.getCodepoint()) + ";</span>";
	}

	public static String htmlColoredIcon(final FontIcon fontIcon, final String cssColor) {
		return htmlStyledIcon(fontIcon, "color: " + cssColor);
	}

	public static String getHtml(final FontIcon fontIcon) {
		return fontIcon == null ? "" : fontIcon.getHtml();
	}

	public static void setTabIndexes(final int from, final Focusable... components) {
		for (int i = 0; i < components.length; i++) {
			components[i].setTabIndex(from + i);
		}
	}

	public static <T> void setReadOnlyValue(final HasValue<T> field, final T value) {
		if (field.isReadOnly()) {
			try {
				field.setReadOnly(false);
				field.setValue(value);
			} finally {
				field.setReadOnly(true);
			}
		} else {
			field.setValue(value);
		}
	}

	public static String clearUriFragmentParameters(final String uriFragment) {
		final int slashIx = uriFragment.indexOf('/');
		if (slashIx < 0) {
			return uriFragment;
		}
		return uriFragment.substring(0, slashIx);
	}

	public static void forEachChildren(final HasComponents parent, final Consumer<Component> consumer) {
		final Iterator<Component> it = parent.iterator();
		while (it.hasNext()) {
			final Component c = it.next();
			if (c instanceof HasComponents) {
				forEachChildren((HasComponents) c, consumer);
			} else {
				consumer.accept(c);
			}
		}
	}

	public static String throwableToHtml(final Throwable ex, final String genericMessage) {
		if (ex instanceof ValidationException) {
			final ValidationException vex = (ValidationException) ex;
			final Stream<String> fieldValidationErrors = vex.getFieldValidationErrors().stream()
					.map(s -> {
						final ValidationResult msg = s.getResult().get();
						if (s.getField() instanceof Component) {
							final Component c = (Component) s.getField();
							if (c.getCaption() != null) {
								return "<tr><td><b>" + c.getCaption() + "</b></td><td>" + msg.getErrorMessage() + "</td></tr>";
							}
						}
						return "<tr><td colspan=\"2\">" + msg.getErrorMessage() + "</td></tr>";
					});
			final Stream<String> beanValidationErrors = vex.getBeanValidationErrors().stream().map(e -> "<tr><td colspan=\"2\">" + e.getErrorMessage() + "</td></tr>");
			return "Dati non validi" + Stream.concat(fieldValidationErrors, beanValidationErrors)
					.collect(Collectors.joining("", "<table style=\"margin:auto\">", "</table>"));
		} else if (ex instanceof AzServiceDetailsException) {
			return ex.getLocalizedMessage() + Html.ul(((AzServiceDetailsException) ex).getDetails());
		} else if (ex instanceof AzException) {
			return ex.getLocalizedMessage();
		} else if (ex instanceof Exception) {
			return genericMessage;
		} else {
			return "Errore grave";
		}
	}

	public static String throwableToHtml(final ResourceBundle bundle, final Throwable ex, final String genericMessage) {
		if (ex instanceof ValidationException) {
			final ValidationException vex = (ValidationException) ex;
			final Stream<String> fieldValidationErrors = vex.getFieldValidationErrors().stream()
					.map(s -> {
						final ValidationResult msg = s.getResult().get();
						if (s.getField() instanceof Component) {
							final Component c = (Component) s.getField();
							if (c.getCaption() != null) {
								return "<tr><td><b>" + c.getCaption() + "</b></td><td>" + msg.getErrorMessage() + "</td></tr>";
							}
						}
						return "<tr><td colspan=\"2\">" + msg.getErrorMessage() + "</td></tr>";
					});
			final Stream<String> beanValidationErrors = vex.getBeanValidationErrors().stream().map(e -> "<tr><td colspan=\"2\">" + e.getErrorMessage() + "</td></tr>");
			return "Dati non validi" + Stream.concat(fieldValidationErrors, beanValidationErrors)
					.collect(Collectors.joining("", "<table style=\"margin:auto\">", "</table>"));

		} else if (ex instanceof AzServiceDetailsException) {
			return ex.getLocalizedMessage() + Html.ul(((AzServiceDetailsException) ex).getDetails());

		} else if (ex instanceof AzInvalidDataException) {
			final AzInvalidDataException ex2 = (AzInvalidDataException) ex;
			final String header = bundle.getString(ex2.getKey().getMessageKey());
			final List<ValidationError> details = ex2.getDetails();
			return header + " " + details.stream().map(d -> getValidationRow(bundle, d)).collect(Collectors.joining("", "<table style=\"margin:auto\">", "</table>"));

		} else if (ex instanceof com.azserve.azframework.exception.AzServiceDetailsException) {
			final com.azserve.azframework.exception.AzServiceDetailsException ex2 = (com.azserve.azframework.exception.AzServiceDetailsException) ex;
			return messageOrKey(bundle, ex2.getKey().getMessageKey(), ex2.getParams()) + detailsToHtml(ex2.getDetails());

		} else if (ex instanceof AzGenericException) {
			final AzGenericException ex2 = (AzGenericException) ex;
			return messageOrKey(bundle, ex2.getKey().getMessageKey(), ex2.getParams());

		} else if (ex instanceof AzException) {
			final ExceptionMessageKey key = ((AzException) ex).getMessageKey();
			if (key != null) {
				final String name = key.getName();
				final Object[] args = ((AzException) ex).getArgs();
				return bundle.containsKey(name)
						? MessageFormat.format(bundle.getString(name), args == null ? new Object[0] : args)
						: name;
			}
			return ex.getLocalizedMessage();

		} else if (ex instanceof Exception) {
			return genericMessage;

		} else {
			return "Errore grave";
		}
	}

	private static String detailsToHtml(final List<Communication> details) {
		return details.stream().map(c -> c.getCode() == null ? c.getMessage() : c.getCode() + " - " + c.getMessage())
				.collect(Collectors.joining("</li><li>", "<ul><li>", "</ul></li>"));
	}

	private static final String VALIDATED_VALUE_NAME = "validatedValue";
	private static final ExpressionFactory EXPRESSION_FACTORY = ExpressionFactory.newInstance();

	private static String getValidationRow(final ResourceBundle bundle, final ValidationError validationError) {
		final Field field = validationError.getFieldAnnotation();
		final String fieldName = getLocalizedFieldName(bundle, validationError, field);
		if (fieldName == null) {
			return "<tr><td colspan=\"2\">" + getString(bundle, validationError) + "</td></tr>";
		}
		return "<tr><td><b>" + fieldName + "</b></td><td>" + getString(bundle, validationError) + "</td></tr>";
	}

	private static String getString(final ResourceBundle bundle, final ValidationError validationError) {
		final String messageKey = validationError.getMessageKey();
		if (messageKey != null) {
			if (bundle.containsKey(messageKey)) {
				final String bundleMessage = bundle.getString(messageKey);

				final SimpleELContext elContext = createSimpleELContext();

				ValueExpression valueExpression = EXPRESSION_FACTORY.createValueExpression(validationError.getInvalidValue(), Object.class);
				elContext.getVariableMapper().setVariable(VALIDATED_VALUE_NAME, valueExpression);

				// bind a formatter instantiated with proper locale
				valueExpression = EXPRESSION_FACTORY.createValueExpression(new FormatterWrapper(Locale.getDefault()), FormatterWrapper.class);
				elContext.getVariableMapper().setVariable(RootResolver.FORMATTER, valueExpression);

				// map the annotation values
				for (final Map.Entry<String, Serializable> entry : validationError.getMessageParams().entrySet()) {
					valueExpression = EXPRESSION_FACTORY.createValueExpression(entry.getValue(), Object.class);
					elContext.getVariableMapper().setVariable(entry.getKey(), valueExpression);
				}

				valueExpression = EXPRESSION_FACTORY.createValueExpression(elContext, bundleMessage, String.class);

				return Objects.toString(valueExpression.getValue(elContext), "");
			}
			return messageKey;
		}
		return validationError.getMessage();
	}

	private static String getLocalizedFieldName(final @NonNull ResourceBundle bundle, final @NonNull ValidationError validationError, final Field fieldAnnotation) {
		if (fieldAnnotation != null) {
			return messageOrKey(bundle, fieldAnnotation.key(), null);
		}
		final String fullFieldName = validationError.getFullFieldName();
		if (FrameworkConfiguration.useFieldFullNameWhenGeneratingValidationErrorMessages && fullFieldName != null) {
			return messageOrKey(bundle, fullFieldName, null);
		}
		final String propertyPath = validationError.getPropertyPath();
		if (propertyPath != null && validationError.isField()) {
			return StringUtils.splitCamelCase(propertyPath.toString());
		}
		return null;
	}

	public static String messageOrKey(final @NonNull ResourceBundle bundle, final @NonNull String messageKey, final Map<String, ? extends Object> params) {
		if (!bundle.containsKey(messageKey)) {
			return messageKey;
		}
		final String bundleMessage = bundle.getString(messageKey);
		if (params == null) {
			return bundleMessage;
		}
		final SimpleELContext elContext = createSimpleELContext();

		// bind a formatter instantiated with proper locale
		ValueExpression valueExpression = EXPRESSION_FACTORY.createValueExpression(new FormatterWrapper(Locale.getDefault()), FormatterWrapper.class);
		elContext.getVariableMapper().setVariable(RootResolver.FORMATTER, valueExpression);

		// map the annotation values
		for (final Map.Entry<String, ? extends Object> entry : params.entrySet()) {
			valueExpression = EXPRESSION_FACTORY.createValueExpression(entry.getValue(), Object.class);
			elContext.getVariableMapper().setVariable(entry.getKey(), valueExpression);
		}

		valueExpression = EXPRESSION_FACTORY.createValueExpression(elContext, bundleMessage, String.class);

		return Objects.toString(valueExpression.getValue(elContext), "");
	}

	private static SimpleELContext createSimpleELContext() throws NoSuchMethodError {
		return new SimpleELContext(EXPRESSION_FACTORY);
		// /*
		// * XXX
		// * Wildfly 10 utilizza Hibernate Validator 5.2.4
		// * Wildfly 11 utilizza Hibernate Validator 5.3.5
		// *
		// * Nella versione 5.3+ è stato rimosso il costruttore vuoto di SimpleELContext
		// * e sostituito con il parametro ExpressionFactory
		// *
		// * Utilizzo la reflection per capire quale costruttore dev'essere invocato
		// * per evitare errori NoSuchMethodError
		// */
		// SimpleELContext elContext = null;
		// try {
		// for (final Constructor<?> constructor : SimpleELContext.class.getConstructors()) {
		// final Class<?>[] parameterTypes = constructor.getParameterTypes();
		// if (parameterTypes.length == 0) {
		// elContext = (SimpleELContext) constructor.newInstance();
		// break;
		// }
		// if (parameterTypes.length == 1) {
		// final Class<?> parameterType = parameterTypes[0];
		// if (parameterType == ExpressionFactory.class) {
		// elContext = (SimpleELContext) constructor.newInstance(expressionFactory);
		// break;
		// }
		// }
		// }
		// } catch (final ReflectiveOperationException ex) {
		// throw new RuntimeException(ex);
		// }
		//
		// if (elContext == null) {
		// throw new NoSuchMethodError("Cannot find a known constructor for SimpleELContext");
		// }
		// return elContext;
	}
}
