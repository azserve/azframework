package com.azserve.azframework.vaadin8.fields;

import org.vaadin.viritin.util.HtmlElementPropertySetter;

import com.azserve.azframework.common.lang.NumberUtils;

public class IntegerField extends NumberField<Integer> {

	private static final long serialVersionUID = 5205678813812249806L;

	public IntegerField(final String caption) {
		super(caption);
	}

	public IntegerField() {
		super();
	}

	@Override
	protected void configureHtml(final HtmlElementPropertySetter heps) {
		heps.setProperty("type", "number");
		// prevent all but numbers with a simple js
		heps.setJavaScriptEventHandler("keypress",
				"function(e) {var c = viritin.getChar(e); return c==null || /^[-\\d\\n\\t\\r]+$/.test(c);}");
	}

	@Override
	protected Integer convertToValue(final String text) {
		return NumberUtils.parseInteger(text);
	}
}
