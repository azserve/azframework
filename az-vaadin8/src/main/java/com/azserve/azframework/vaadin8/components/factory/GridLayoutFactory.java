package com.azserve.azframework.vaadin8.components.factory;

import java.util.Objects;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;

public final class GridLayoutFactory extends AbstractLayoutFactory<GridLayout, GridLayoutFactory> {

	private int lastCol = 0;
	private int lastRow = 0;

	private int curCol = 0;
	private int curRow = 0;

	GridLayoutFactory(final int columns, final int rows) {
		super();
		this.get().setColumns(columns);
		this.get().setRows(rows);
	}

	public GridLayoutFactory withSpacing(final boolean spacing) {
		this.get().setSpacing(spacing);
		return this;
	}

	public GridLayoutFactory withMargin(final boolean margin) {
		this.get().setMargin(margin);
		return this;
	}

	public GridLayoutFactory withMargins(final boolean top, final boolean right, final boolean bottom, final boolean left) {
		this.get().setMargin(new MarginInfo(top, right, bottom, left));
		return this;
	}

	@Override
	public GridLayoutFactory add(final Component c) {
		return this.addAt(c, this.curCol, this.curRow);
	}

	public GridLayoutFactory add(final Component c, final int columnSpan, final int rowSpan) {
		return this.addAt(c, this.curCol, this.curRow, columnSpan, rowSpan);
	}

	public GridLayoutFactory add(final Component c, final int columnSpan) {
		return this.addAt(c, this.curCol, this.curRow, columnSpan, 1);
	}

	public GridLayoutFactory nextRow() {
		if (this.curCol != 0) {
			return this.skipRow();
		}
		return this;
	}

	public GridLayoutFactory skipRow() {
		this.curCol = 0;
		this.curRow++;
		return this;
	}

	public GridLayoutFactory skipCol() {
		final GridLayout l = this.get();
		this.curCol = (this.curCol + 1) % l.getColumns();
		if (this.curCol == 0) {
			this.curRow++;
		}
		return this;
	}

	public GridLayoutFactory add(final Object obj, final int column, final int row) {
		return this.add(new Label(Objects.toString(obj, "")), column, row);
	}

	public GridLayoutFactory addAt(final Component c, final int column, final int row) {
		return this.addAt(c, column, row, 1, 1);
	}

	public GridLayoutFactory addAt(final Component c, final int startColumn, final int startRow, final int columnSpan, final int rowSpan) {
		final GridLayout l = this.get();

		this.lastCol = this.curCol;
		this.lastRow = this.curRow;
		l.addComponent(c, startColumn, startRow, startColumn + columnSpan - 1, startRow + rowSpan - 1);
		this.curCol = (startColumn + columnSpan) % l.getColumns();
		// non viene incrementata la riga con il valore di rowspan
		this.curRow = startRow + ((startColumn + columnSpan) / l.getColumns());
		this.last = c;
		return this;
	}

	public GridLayoutFactory addToColumn(final Component c, final int column) {
		final GridLayout l = this.get();
		l.addComponent(c, column, 0, column, l.getRows() - 1);
		return this;
	}

	public GridLayoutFactory addToRow(final Component c, final int row) {
		final GridLayout l = this.get();
		l.addComponent(c, 0, row, l.getColumns() - 1, row);
		return this;
	}

	public GridLayoutFactory align(final Alignment alignment) {
		return this.align(this.last, alignment);
	}

	public GridLayoutFactory align(final Component c, final Alignment alignment) {
		this.get().setComponentAlignment(c, alignment);
		return this;
	}

	public GridLayoutFactory expandCol(final float ratio) {
		return this.expandCol(this.lastCol, ratio);
	}

	public GridLayoutFactory expandCol() {
		return this.expandCol(1);
	}

	public GridLayoutFactory expandCol(final int columnIndex, final float ratio) {
		this.get().setColumnExpandRatio(columnIndex, ratio);
		return this;
	}

	public GridLayoutFactory expandRow(final float ratio) {
		return this.expandRow(this.lastRow, ratio);
	}

	public GridLayoutFactory expandRow() {
		return this.expandRow(1);
	}

	public GridLayoutFactory expandRow(final int rowIndex, final float ratio) {
		this.get().setRowExpandRatio(rowIndex, ratio);
		return this;
	}

	@Override
	protected GridLayout supplyLayout() {
		return new GridLayout();
	}
}