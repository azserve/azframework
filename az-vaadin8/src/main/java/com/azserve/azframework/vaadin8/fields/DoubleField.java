package com.azserve.azframework.vaadin8.fields;

import java.math.BigDecimal;

import com.azserve.azframework.common.annotation.NonNull;

public class DoubleField extends DecimalField<Double> {

	private static final long serialVersionUID = -1908129220386864689L;

	public DoubleField() {
		super();
	}

	public DoubleField(final String caption) {
		super(caption);
	}

	@Override
	protected @NonNull Double convertFromBigDecimal(final @NonNull BigDecimal value) {
		return Double.valueOf(value.doubleValue());
	}
}
