package com.azserve.azframework.vaadin8.fields;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.vaadin.viritin.util.HtmlElementPropertySetter;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.lang.StringUtils;

public abstract class DecimalField<N extends Number> extends NumberField<N> {

	private static final long serialVersionUID = 2792891231528871593L;

	private static final DecimalFormatSymbols SYMBOLS = DecimalFormatSymbols.getInstance(
			Locale.getDefault(Locale.Category.FORMAT));

	private final DecimalFormat formatter = new DecimalFormat("0.##########");
	private DecimalFormatSymbols symbols = SYMBOLS;

	public DecimalField() {
		super();
		this.setStyleName("numeric-field");
	}

	public DecimalField(final String caption) {
		super(caption);
		this.setStyleName("numeric-field");
	}

	@Override
	protected void configureHtml(final HtmlElementPropertySetter heps) {
		// prevent all but numbers with a simple js
		heps.setJavaScriptEventHandler("keydown",
				"function(e) {"
						+ "var x = e.which || e.keyCode;"
						+ "if(x == 110) {"
						+ "	if (this.selectionStart || this.selectionStart == 0) {"
						+ "		var start = this.selectionStart;"
						+ "		var end = this.selectionEnd;"
						+ "		var value = e.target.value;"
						+ "		e.target.value = value.substring(0, start) + '" + this.symbols.getDecimalSeparator() + "' + value.substring(end, value.length);"
						+ "	} else {"
						+ "		target.value = target.value + '" + this.symbols.getDecimalSeparator() + "';"
						+ "	}"
						+ "	return false;"
						+ "}"
						+ "var c = viritin.getChar(e); return c==null || /^[-\\\\d\\\\n\\\\t\\\\r\\\\.,]+$/.test(c);"
						+ "}");
	}

	@Override
	public void attach() {
		super.attach();
		// leggo i segni tramite il locale
		final Locale locale = this.getLocale();
		if (locale != null) {
			this.symbols = DecimalFormatSymbols.getInstance(locale);
		} else {
			this.symbols = SYMBOLS;
		}
		this.formatter.setDecimalFormatSymbols(this.symbols);
	}

	@Override
	protected @NonNull String convertToText(final @Nullable N value) {
		return value == null ? "" : this.formatter.format(value);
	}

	public DecimalFormat getFormatter() {
		return this.formatter;
	}

	@Override
	protected N convertToValue(final String text) {
		if (StringUtils.isNotNullNorBlank(text)) {
			/*
			 * Converto l'input sostituendo il punto e la virgola
			 * con il separatore dei decimali localizzato.
			 * Ciò non funziona se vengono scritti più
			 * punti e/o virgole assieme.
			 */
			try {
				final BigDecimal bd = new BigDecimal(text.replace(String.valueOf(this.symbols.getGroupingSeparator()), "")
						.replace(this.symbols.getDecimalSeparator(), '.'));
				return this.convertFromBigDecimal(bd);
			} catch (final @SuppressWarnings("unused") NumberFormatException ex) {
				return null;
			}
		}
		return null;
	}

	protected abstract @NonNull N convertFromBigDecimal(final @NonNull BigDecimal value);

}
