package com.azserve.azframework.vaadin8.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import com.vaadin.server.Page;

public class ZonedDateUtils {

	public static int getTimeZoneOffsetMs() {
		return Page.getCurrent().getWebBrowser().getTimezoneOffset();
	}

	public static int getTimeZoneOffset() {
		return getTimeZoneOffsetMs() / 1000;
	}

	public static ZoneOffset getZoneOffset() {
		return ZoneOffset.ofTotalSeconds(getTimeZoneOffset());
	}

	public static LocalDateTime offset(final LocalDateTime utcDateTime, final ZoneOffset from, final ZoneOffset to) {
		return utcDateTime == null ? null : utcDateTime.atOffset(from).withOffsetSameInstant(to).toLocalDateTime();
	}

	public static LocalDateTime toZoned(final LocalDateTime utcDateTime) {
		return offset(utcDateTime, ZoneOffset.UTC, getZoneOffset());
	}

	public static LocalDateTime toUtc(final LocalDateTime zonedDateTime) {
		return offset(zonedDateTime, getZoneOffset(), ZoneOffset.UTC);
	}

	public static LocalDateTime offset(final LocalDate utcDate, final ZoneOffset from, final ZoneOffset to) {
		return utcDate == null ? null : utcDate.atStartOfDay().atOffset(from).withOffsetSameInstant(to).toLocalDateTime();
	}

	public static LocalDateTime toZoned(final LocalDate utcDate) {
		return offset(utcDate, ZoneOffset.UTC, getZoneOffset());
	}

	public static LocalDateTime toUtc(final LocalDate zonedDate) {
		return offset(zonedDate, getZoneOffset(), ZoneOffset.UTC);
	}

	private ZonedDateUtils() {}
}
