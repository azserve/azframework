package com.azserve.azframework.vaadin8.view;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.framework.presenter.AbstractPresenter;
import com.azserve.framework.presenter.display.Display;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.ui.Component;

public abstract class MenuView<D extends Display, P extends AbstractPresenter<D>, C extends Component> implements View, Display {

	private static final long serialVersionUID = 623522272330295543L;

	private Map<String, String> viewParameters;

	@PostConstruct
	private void postConstruct() {
		this.prepare();
		final @SuppressWarnings("unchecked") D display = (D) this;
		this.getPresenter().setDisplay(display);
	}

	@Override
	public abstract C getViewComponent();

	@Override
	public void enter(final ViewChangeEvent event) {
		final Map<String, String> decodedParameters = this.getParameters(event);
		this.getPresenter().setParameters(decodedParameters);
		this.viewParameters = new HashMap<>(decodedParameters);
	}

	protected Map<String, String> getParameters(final ViewChangeEvent event) {
		return event.getParameterMap();
	}

	/**
	 * Effettua delle operazioni alla chiusura di questa view.
	 *
	 * @param closeCallback interfaccia da invocare per
	 *            la chiusura.
	 * @return {@code true} se la view può essere
	 *         chiusa subito, {@code false} se la view
	 *         non può essere chiusa per interagire con l'utente.
	 */
	public boolean closing(final Runnable closeCallback) {
		return this.getPresenter().closing(closeCallback);
	}

	/**
	 * Effettua delle operazioni prima di
	 * avviare il presenter.
	 *
	 * Generalmente disegna la view.
	 *
	 * Per effettuare operazioni all'apertura del menù,
	 * quindi dopo l'avviamento del presenter
	 * utilizzare {@linkplain #enter(ViewChangeEvent)}.
	 */
	protected abstract void prepare();

	/**
	 * Restituisce il presenter iniettato.
	 */
	protected abstract @NonNull P getPresenter();

	public final Map<String, String> getViewParameters() {
		return this.viewParameters;
	}

	/**
	 * Toglie la parte della stringa che contiene i parametri, giusto per non farli vedere all'utente
	 */
	protected void hideParameters(final ViewChangeEvent event) {
		final Page currentPage = Page.getCurrent();
		final String uriFragment = currentPage.getUriFragment();
		currentPage.setUriFragment(uriFragment.substring(0, uriFragment.length() - event.getParameters().length()), false);
	}
}
