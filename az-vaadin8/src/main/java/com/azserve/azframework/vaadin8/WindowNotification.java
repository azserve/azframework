package com.azserve.azframework.vaadin8;

import java.io.IOException;

import com.azserve.azframework.common.io.IOUtils;
import com.vaadin.server.Sizeable;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window;

public class WindowNotification extends Window {

	private static final long serialVersionUID = -8932142145792492791L;

	private final Button button;
	private String resourcePath;

	public WindowNotification(final String caption, final Button b) {
		super(caption);
		this.button = b;

		this.setResizable(false);
		this.addStyleName("window-notification");

		if (this.button != null) {
			this.addCloseListener(e -> this.button.setVisible(true));
		}
	}

	public String getResourcePath() {
		return this.resourcePath;
	}

	public void setResourcePath(final String resourcePath) {
		this.resourcePath = resourcePath;
		this.createContent();
	}

	private void createContent() {
		final Label container = new Label();
		container.setContentMode(ContentMode.HTML);
		container.setWidthUndefined();
		try {
			container.setValue(IOUtils.toString(this.getClass().getResourceAsStream(this.resourcePath)));
			this.setContent(container);
			this.getContent().setWidth(100, Sizeable.Unit.PERCENTAGE);
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

}