package com.azserve.azframework.vaadin8.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.funct.Functions;
import com.azserve.azframework.common.io.FileExtension;
import com.azserve.framework.presenter.display.UploadData;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedListener;
import com.vaadin.ui.Upload.SucceededListener;

public class FileUploader extends Upload implements Receiver, StartedListener, SucceededListener, FailedListener {

	private static final long serialVersionUID = -4407998442293921951L;

	private static final Logger LOGGER = LoggerFactory.getLogger(FileUploader.class);

	private Path tempFile;
	private UploadData uploadData;

	private Consumer<UploadData> successAction;
	private Runnable failAction = Functions.NULL_RUNNABLE;

	private Set<FileExtension> allowedExtensions = null;
	private long maxContentLength = Long.MAX_VALUE;

	public FileUploader(final String caption) {
		this.setButtonCaption(caption);
		this.addSucceededListener(this);
		this.addStartedListener(this);
		this.addFailedListener(this);
		this.setReceiver(this);
	}

	public long getMaxContentLength() {
		return this.maxContentLength;
	}

	public void setMaxContentLength(final long maxLength) {
		if (maxLength < 0) {
			throw new IllegalArgumentException("Negative max content length");
		}
		this.maxContentLength = maxLength;
	}

	public Set<FileExtension> getAllowedExtensions() {
		return Collections.unmodifiableSet(this.allowedExtensions);
	}

	public void setAllowedExtensions(final Set<FileExtension> allowedExtensions) {
		this.allowedExtensions = allowedExtensions;
	}

	public void setSuccessAction(final Consumer<UploadData> successAction) {
		this.successAction = successAction;
	}

	public void setFailAction(final @NonNull Runnable failAction) {
		this.failAction = Objects.requireNonNull(failAction, "Action cannot be null");
	}

	@Override
	public OutputStream receiveUpload(final String filename, final String mimeType) {
		try {
			final FileExtension fileExtension = FileExtension.byMimeType(mimeType);
			if (this.allowedExtensions != null && !this.allowedExtensions.contains(fileExtension)) {
				this.failAction.run();
				return null;
			}
			this.tempFile = Files.createTempFile(filename, fileExtension == null ? "" : fileExtension.getExtension());
			this.uploadData = new UploadData(filename, mimeType);
			return Files.newOutputStream(this.tempFile);
		} catch (final IOException e) {
			this.tempFile = null;
			this.uploadData = null;
			LOGGER.error("receive", e);
		}
		return null;
	}

	@Override
	public void uploadStarted(final StartedEvent event) {
		final long length = event.getContentLength();
		if (length > this.maxContentLength) {
			event.getUpload().interruptUpload();
		}
	}

	@Override
	public void uploadSucceeded(final SucceededEvent event) {
		try {
			if (this.successAction != null) {
				try (InputStream is = Files.newInputStream(this.tempFile)) {
					this.uploadData.setContent(is);
					this.uploadData.setContentLength(event.getLength());
					this.successAction.accept(this.uploadData);
				} catch (final IOException e) {
					LOGGER.error("Open tmp file", e);
				}
			}
		} finally {
			try {
				Files.delete(this.tempFile);
			} catch (final IOException e) {
				LOGGER.error("Delete tmp file", e);
			} finally {
				this.tempFile = null;
				this.uploadData = null;
			}
		}

	}

	@Override
	public void uploadFailed(final FailedEvent event) {
		try {
			this.failAction.run();
		} finally {
			try {
				Files.delete(this.tempFile);
			} catch (final IOException e) {
				LOGGER.error("Delete tmp file", e);
			} finally {
				this.tempFile = null;
				this.uploadData = null;
			}
		}
	}
}