package com.azserve.azframework.vaadin8.data.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;

public class ServiceBackEndCachedDataProvider<T> extends AbstractBackEndDataProvider<T, String> {

	private static final long serialVersionUID = -6056437387422363351L;

	private final Function<String, Stream<T>> fetchFunction;
	private final BiPredicate<T, String> cacheFetchFunction;

	private List<T> backend = Collections.emptyList();
	private List<T> currentData = Collections.emptyList();
	private boolean dataIsInMemory = false;

	private String fetchFilter = "";
	private final int minFilterLength;

	public ServiceBackEndCachedDataProvider(
			final Function<String, Stream<T>> fetchFunction,
			final BiPredicate<T, String> cacheFetchFunction,
			final int minFilterLength) {

		this.fetchFunction = fetchFunction;
		this.cacheFetchFunction = cacheFetchFunction;
		this.minFilterLength = minFilterLength;
	}

	public List<T> getBackendView() {
		return Collections.unmodifiableList(this.backend);
	}

	public void setBackend(final List<T> list) {
		this.backend = new ArrayList<>(list);
		this.refreshAll();
	}

	@Override
	protected Stream<T> fetchFromBackEnd(final Query<T, String> query) {
		this.fetchIfNecessary(query);
		return this.currentData.stream().skip(query.getOffset()).limit(query.getLimit());
	}

	@Override
	protected int sizeInBackEnd(final Query<T, String> query) {
		this.fetchIfNecessary(query);
		return this.currentData.size();
	}

	private void fetchIfNecessary(final Query<T, String> query) {
		final String newFilter = query.getFilter().orElse("");
		if (newFilter.length() < this.minFilterLength) {
			this.fetchFilter = newFilter;
			this.backend = Collections.emptyList();
			this.currentData = Collections.emptyList();
			this.dataIsInMemory = false;
		} else if (!this.fetchFilter.equals(newFilter)) { // TODO case-insensitive?
			if (newFilter.startsWith(this.fetchFilter) && this.dataIsInMemory) {
				this.currentData = this.backend.stream().skip(query.getOffset()).limit(query.getLimit())
						.filter(t -> this.cacheFetchFunction.test(t, newFilter)).collect(Collectors.toList());
			} else {
				this.backend = this.fetchFunction.apply(newFilter).collect(Collectors.toList());
				this.currentData = this.backend.stream().skip(query.getOffset()).limit(query.getLimit()).collect(Collectors.toList());
				this.dataIsInMemory = true;
				this.fetchFilter = newFilter;
			}
		}
	}
}
