package com.azserve.azframework.vaadin8.renderers;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import com.azserve.azframework.common.lang.NumberUtils.NumberPattern;
import com.azserve.azframework.common.util.DateUtils.DatePattern;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickListener;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.NumberRenderer;

public final class GridRenderers {

	public static <T> ButtonRenderer<T> htmlButton(final RendererClickListener<T> listener) {
		final ButtonRenderer<T> renderer = new ButtonRenderer<>(listener);
		renderer.setHtmlContentAllowed(true);
		return renderer;
	}

	public static DateRenderer forDate() {
		return forDate(DatePattern.DD_MM_YYYY);
	}

	public static DateRenderer forDate(final DatePattern pattern) {
		return new DateRenderer(pattern.get());
	}

	public static DateRenderer forDate(final String pattern) {
		return new DateRenderer(new SimpleDateFormat(pattern));
	}

	public static NumberRenderer forNumber(final NumberPattern pattern) {
		return forNumber(new DecimalFormat(pattern.getPattern()));
	}

	public static NumberRenderer forNumber(final DecimalFormat decimalFormat) {
		return new NumberRenderer(decimalFormat);
	}

	private GridRenderers() {}
}
