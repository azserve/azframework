package com.azserve.azframework.vaadin8.fields;

import java.math.BigDecimal;
import java.util.Objects;

import org.vaadin.viritin.util.HtmlElementPropertySetter;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.lang.NumberUtils;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.ValueContext;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.data.validator.RangeValidator;
import com.vaadin.server.UserError;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.TextField;

public abstract class NumberField<N extends Number> extends CustomField<N> {

	private static final long serialVersionUID = 5968741622995191454L;

	protected final TextField field = new TextField() {

		private static final long serialVersionUID = -2219620947457308947L;

		@Override
		public void beforeClientResponse(final boolean initial) {
			super.beforeClientResponse(initial);
			NumberField.this.configureHtml(NumberField.this.htmlSetter);
		}
	};
	private final HtmlElementPropertySetter htmlSetter = new HtmlElementPropertySetter(this.field);

	private String numberOutOfRangeMessage = "Number is out of allowed range";

	private N internalValue;

	private BigDecimal minAllowedValue;
	private BigDecimal maxAllowedValue;

	public NumberField() {
		this(null);
	}

	public NumberField(final String caption) {
		super();
		this.setSizeUndefined();
		this.setCaption(caption);
		this.setFocusDelegate(this.field);
		this.field.setValueChangeMode(ValueChangeMode.BLUR);
		this.field.addValueChangeListener(e -> {
			final N convertedValue = this.convertToValue(e.getValue());
			final boolean userOriginated = e.isUserOriginated();

			if (!userOriginated || this.isInRange(convertedValue)) {
				/*
				 * Cambio il valore se:
				 * non è stato inserito dall'utente
				 * oppure è stato inserito dall'utente e rispetta il range di valori
				 */
				this.setValue(convertedValue, userOriginated);
			}

			this.field.setValue(this.convertToText(this.getValue()));
		});
	}

	public BigDecimal getMinAllowedValue() {
		return this.minAllowedValue;
	}

	public void setMinAllowedValue(final BigDecimal value) {
		if (value != null && this.maxAllowedValue != null && value.compareTo(this.maxAllowedValue) > 0) {
			throw new IllegalArgumentException(
					String.format("min value (%s) is greater than max value (%s)", value, this.maxAllowedValue));
		}
		this.minAllowedValue = value;
		this.htmlSetter.setProperty("min", value);
	}

	public BigDecimal getMaxAllowedValue() {
		return this.maxAllowedValue;
	}

	public void setMaxAllowedValue(final BigDecimal value) {
		if (value != null && this.minAllowedValue != null && value.compareTo(this.minAllowedValue) < 0) {
			throw new IllegalArgumentException(
					String.format("min value (%s) is greater than max value (%s)", this.minAllowedValue, value));
		}
		this.maxAllowedValue = value;
		this.htmlSetter.setProperty("max", value);
	}

	public String getNumberOutOfRangeMessage() {
		return this.numberOutOfRangeMessage;
	}

	public void setNumberOutOfRangeMessage(final String numberOutOfRangeMessage) {
		this.numberOutOfRangeMessage = numberOutOfRangeMessage;
	}

	@Override
	public void setWidth(final float width, final Unit unit) {
		super.setWidth(width, unit);
		if (this.field != null) {
			if (width != -1) {
				this.field.setWidth(100, Unit.PERCENTAGE);
			} else {
				this.field.setWidth(null);
			}
		}
	}

	public String getPlaceholder() {
		return this.field.getPlaceholder();
	}

	public void setPlaceholder(final String placeholder) {
		this.field.setPlaceholder(placeholder);
	}

	public void selectAll() {
		this.field.selectAll();
	}

	@Override
	public boolean isReadOnly() {
		return this.field.isReadOnly();
	}

	@Override
	public void setReadOnly(final boolean readOnly) {
		this.field.setReadOnly(readOnly);
	}

	@Override
	public N getValue() {
		return this.internalValue;
	}

	@Override
	protected Component initContent() {
		return this.field;
	}

	@Override
	public void attach() {
		super.attach();
		this.field.setValue(this.convertToText(this.getValue()));
	}

	@Override
	protected void doSetValue(final N value) {
		this.internalValue = value;
		if (this.isAttached()) {
			this.field.setValue(this.convertToText(value));
		}

		this.setComponentError(null);

		if (value != null) {

			final RangeValidator<BigDecimal> validator = this.getRangeValidator();
			final ValidationResult result = validator.apply(this.toBigDecimal(value), new ValueContext(this));
			if (result.isError()) {
				this.setComponentError(new UserError(this.getNumberOutOfRangeMessage()));
			}

		}
	}

	private boolean isInRange(final N value) {
		final RangeValidator<BigDecimal> validator = this.getRangeValidator();
		final ValidationResult result = validator.apply(this.toBigDecimal(value), new ValueContext(this));
		return !result.isError();
	}

	protected void configureHtml(final HtmlElementPropertySetter heps) {
		// prevent all but numbers with a simple js
		heps.setJavaScriptEventHandler("keypress",
				"function(e) {var c = viritin.getChar(e); return c==null || /^[-\\d\\n\\t\\r]+$/.test(c);}");
	}

	protected BigDecimal toBigDecimal(final @NonNull N value) {
		return NumberUtils.toBigDecimal(value);
	}

	protected RangeValidator<BigDecimal> getRangeValidator() {
		return new BigDecimalRangeValidator(
				this.getNumberOutOfRangeMessage(),
				this.getMinAllowedValue(),
				this.getMaxAllowedValue());
	}

	protected abstract N convertToValue(String text);

	protected @NonNull String convertToText(final @Nullable N value) {
		return Objects.toString(value, "");
	}
}
