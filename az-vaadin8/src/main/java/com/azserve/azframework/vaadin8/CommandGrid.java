package com.azserve.azframework.vaadin8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.funct.Functions;
import com.azserve.azframework.vaadin8.utils.Utils;
import com.vaadin.data.HasValue;
import com.vaadin.data.ValidationException;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.AbstractErrorMessage.ContentMode;
import com.vaadin.server.UserError;
import com.vaadin.shared.Registration;
import com.vaadin.shared.ui.ErrorLevel;
import com.vaadin.ui.Button;

public class CommandGrid<T> extends BeanGrid<T> implements HasValue<List<T>> {

	private static final long serialVersionUID = -4935547046692653729L;

	private T emptyRow;

	private final BeanBinder<T> binder;

	private List<T> value = Collections.emptyList();
	private BeforeActionHandler<T> beforeSaveHandler;
	private BeforeActionHandler<T> beforeDeleteHandler;

	private final List<T> list = new ArrayList<>();
	private final ListDataProvider<T> dataProvider = new ListDataProvider<>(this.list);

	public CommandGrid(final Class<T> beanType) {
		super();
		this.binder = new BeanBinder<>(beanType);
		this.setDataProvider(this.dataProvider);
	}

	public BeanBinder<T> getBinder() {
		return this.binder;
	}

	public void init(final Button saveButton) {
		this.init(null, saveButton, null);
	}

	public void init(final Supplier<T> emptyRowSupplier, final Button saveButton, final Button delButton) {
		this.init(emptyRowSupplier, Functions.nullConsumer(), Functions.nullConsumer(), saveButton, delButton);
	}

	public void init(final Supplier<T> emptyRowSupplier, final Consumer<T> additionalBeanReader, final Consumer<T> additionalBeanWriter, final Button saveButton, final Button delButton) {
		this.emptyRow = emptyRowSupplier == null ? null : emptyRowSupplier.get();
		if (this.emptyRow != null) {
			this.dataProvider.getItems().add(this.emptyRow);
		}

		this.addSelectionListener(e -> {
			final Optional<T> firstSelectedItem = e.getFirstSelectedItem();
			if (!firstSelectedItem.isPresent()) {
				if (this.emptyRow != null) {
					this.getSelectionModel().select(this.emptyRow);
				} else {
					this.binder.readBean(null);
				}
			} else {
				final T bean = firstSelectedItem.get();
				this.binder.readBean(bean);
				additionalBeanReader.accept(bean);
			}
		});
		if (this.emptyRow != null) {
			this.getSelectionModel().select(this.emptyRow);
		}

		saveButton.addClickListener(e -> {

			final T bean = this.getSelectionModel().getFirstSelectedItem().orElse(null);
			if (bean == null) {
				assert false;
				return;
			}

			if (this.beforeSaveHandler == null
					|| this.beforeSaveHandler.canProceed(bean, () -> this.doSave(emptyRowSupplier, additionalBeanWriter, saveButton, bean))) {
				this.doSave(emptyRowSupplier, additionalBeanWriter, saveButton, bean);
			}
		});
		if (delButton != null) {
			delButton.addClickListener(e -> {

				final T bean = this.getSelectionModel().getFirstSelectedItem().orElse(null);
				if (bean == this.emptyRow || bean == null) {
					return;
				}
				if (this.beforeDeleteHandler == null
						|| this.beforeDeleteHandler.canProceed(bean, () -> this.doDelete(bean))) {
					this.doDelete(bean);
				}
			});
		}
	}

	private void doSave(final Supplier<T> emptyRowSupplier, final Consumer<T> additionalBeanWriter, final Button saveButton, final @NonNull T v) {
		try {
			this.binder.writeBean(v);
			saveButton.setComponentError(null);
			additionalBeanWriter.accept(v);

			this.dataProvider.refreshItem(v);
			if (emptyRowSupplier != null && v == this.emptyRow) {
				this.emptyRow = emptyRowSupplier.get();
				this.addRow();
				this.getSelectionModel().select(this.emptyRow);
			}

		} catch (final ValidationException ex) {
			saveButton.setComponentError(new UserError(Utils.throwableToHtml(ex, ""), ContentMode.HTML, ErrorLevel.ERROR));
		}
	}

	private void doDelete(final T v) {
		this.removeRow(v);
		if (this.emptyRow != null) {
			this.getSelectionModel().select(this.emptyRow);
		}
	}

	private void addRow() {
		final List<T> old = this.getValue();
		this.list.add(0, this.emptyRow);
		this.dataProvider.refreshAll();
		this.fireValueChangeEvent(old, true);
	}

	private void removeRow(final T v) {
		final List<T> old = this.getValue();
		this.list.remove(v);
		this.dataProvider.refreshAll();
		this.fireValueChangeEvent(old, true);
	}

	public BeforeActionHandler<T> getBeforeSaveHandler() {
		return this.beforeSaveHandler;
	}

	public void setBeforeSaveHandler(final BeforeActionHandler<T> beforeSaveHandler) {
		this.beforeSaveHandler = beforeSaveHandler;
	}

	public BeforeActionHandler<T> getBeforeDeleteHandler() {
		return this.beforeDeleteHandler;
	}

	public void setBeforeDeleteHandler(final BeforeActionHandler<T> beforeDeleteHandler) {
		this.beforeDeleteHandler = beforeDeleteHandler;
	}

	@Override
	public boolean isRequiredIndicatorVisible() {
		return super.isRequiredIndicatorVisible();
	}

	@Override
	public void setRequiredIndicatorVisible(final boolean visible) {
		super.setRequiredIndicatorVisible(visible);
	}

	@Override
	public boolean isReadOnly() {
		return super.isReadOnly();
	}

	@Override
	public void setReadOnly(final boolean readOnly) {
		super.setReadOnly(readOnly);
	}

	@Override
	public List<T> getEmptyValue() {
		return Collections.emptyList();
	}

	@Override
	public void setValue(final List<T> value) {
		final List<T> old = this.getValue();
		this.list.clear();
		if (this.emptyRow != null) {
			this.list.add(this.emptyRow);
		}
		if (value != null) {
			this.list.addAll(value);
		}
		this.dataProvider.refreshAll();
		this.fireValueChangeEvent(old, false);
		if (this.emptyRow != null) {
			this.getSelectionModel().select(this.emptyRow);
		} else {
			this.getSelectionModel().deselectAll();
		}
	}

	@Override
	public List<T> getValue() {
		return this.value;
	}

	@Override
	@SuppressWarnings("deprecation")
	public Registration addValueChangeListener(final ValueChangeListener<List<T>> listener) {
		return this.addListener(ValueChangeEvent.class, listener, ValueChangeListener.VALUE_CHANGE_METHOD);
	}

	private void fireValueChangeEvent(final List<T> oldValue, final boolean userOriginated) {
		this.value = this.list.stream().filter(v -> v != this.emptyRow).collect(Collectors.toList());
		this.fireEvent(new ValueChangeEvent<>(this, oldValue, userOriginated));
	}

	public interface BeforeActionHandler<T> {

		boolean canProceed(@NonNull T bean, Runnable action);
	}
}
