package com.azserve.azframework.vaadin8.data.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;

public class ServiceBackEndProvider<T> extends AbstractBackEndDataProvider<T, String> {

	private static final long serialVersionUID = -6056437387422363351L;

	private final BiFunction<String, Query<T, String>, Stream<T>> fetchFunction;

	private List<T> qdtos = Collections.emptyList();
	private String filter = "";
	private final int minFilterLength;

	public ServiceBackEndProvider(final BiFunction<String, Query<T, String>, Stream<T>> fetchFunction) {
		this(fetchFunction, 3);
	}

	public ServiceBackEndProvider(final BiFunction<String, Query<T, String>, Stream<T>> fetchFunction, final int minFilterLength) {
		this.fetchFunction = fetchFunction;
		this.minFilterLength = minFilterLength;
	}

	public List<T> getBackendView() {
		return Collections.unmodifiableList(this.qdtos);
	}

	public void setBackend(final List<T> list) {
		this.qdtos = new ArrayList<>(list);
		this.refreshAll();
	}

	@Override
	protected Stream<T> fetchFromBackEnd(final Query<T, String> query) {
		this.fetchIfNecessary(query);
		return this.qdtos.stream().skip(query.getOffset()).limit(query.getLimit());
	}

	@Override
	protected int sizeInBackEnd(final Query<T, String> query) {
		this.fetchIfNecessary(query);
		return this.qdtos.size();
	}

	private void fetchIfNecessary(final Query<T, String> query) {
		final String newFilter = query.getFilter().orElse("");
		if (newFilter.length() < this.minFilterLength) {
			this.filter = newFilter;
			this.qdtos = Collections.emptyList();
		} else if (!Objects.equals(this.filter, newFilter)) { // TODO case-insensitive?
			this.filter = newFilter;
			this.qdtos = this.fetchFunction.apply(newFilter, query).collect(Collectors.toList());
		}
	}
}
