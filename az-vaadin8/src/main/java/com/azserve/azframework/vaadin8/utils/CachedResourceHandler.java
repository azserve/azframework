package com.azserve.azframework.vaadin8.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.vaadin.server.StreamResource;
import com.vaadin.server.SynchronizedRequestHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinServletService;
import com.vaadin.server.VaadinSession;

public class CachedResourceHandler extends SynchronizedRequestHandler {

	private static final long serialVersionUID = 875134463950113262L;

	private final Map<String, StreamResource> resources = new HashMap<>();

	private boolean removeAfterRequest = false;

	/**
	 * Registra la risorsa specificata.
	 *
	 * @param resource risorsa da registrare.
	 * @param relativeUrl url relativo alla {@code UI}.
	 * @return l'url assoluto per accedere alla risorsa.
	 */
	public String register(final StreamResource resource, final String relativeUrl) {

		final String url = VaadinServlet.getCurrent().getServletContext().getContextPath()
				+ VaadinServletService.getCurrentServletRequest().getServletPath() + "/" + relativeUrl;

		this.resources.put(relativeUrl, resource);

		return url;
	}

	/**
	 * Rimuove tutte le risorse in memoria.
	 */
	public void cleanup() {
		this.resources.clear();
	}

	/**
	 * Rimuove la risorsa corrispondente all'url relativo
	 * alla {@code UI} specificato.
	 *
	 * @param relativeUrl url relativo.
	 */
	public void unregister(final String relativeUrl) {
		this.resources.remove(relativeUrl);
	}

	/**
	 * Indica se una risorsa viene subito rimossa
	 * dopo essere stata richiesta.
	 */
	public boolean isRemoveAfterRequest() {
		return this.removeAfterRequest;
	}

	/**
	 * Imposta se una risorsa dev'essere subito rimossa
	 * una volta richiesta.
	 */
	public void setRemoveAfterRequest(final boolean removeAfterRequest) {
		this.removeAfterRequest = removeAfterRequest;
	}

	@Override
	public boolean synchronizedHandleRequest(final VaadinSession session, final VaadinRequest request, final VaadinResponse response) throws IOException {
		final String pathInfo = request.getPathInfo();
		final StreamResource resource = this.resources.get(pathInfo);
		if (resource != null) {
			resource.getStream().writeResponse(request, response);
			if (this.removeAfterRequest) {
				this.resources.remove(pathInfo);
			}
			return true;
		}
		return false;
	}
}
