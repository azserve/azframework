package com.azserve.azframework.vaadin8;

import java.text.DecimalFormat;
import java.time.LocalTime;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.ChronoLocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.lang.NumberUtils.NumberPattern;
import com.azserve.azframework.common.util.BeanProperty;
import com.azserve.azframework.common.util.DateUtils.DatePattern;
import com.azserve.azframework.vaadin8.renderers.GridRenderers;
import com.azserve.azframework.vaadin8.theme.Styles;
import com.vaadin.data.HasValue;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.GridSortOrder;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.FontIcon;
import com.vaadin.server.SerializableComparator;
import com.vaadin.server.SerializableFunction;
import com.vaadin.server.Setter;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.GridSelectionModel;
import com.vaadin.ui.components.grid.MultiSelectionModel;
import com.vaadin.ui.renderers.AbstractRenderer;
import com.vaadin.ui.renderers.HtmlRenderer;

/**
 * Classe {@code Grid} avanzata.
 *
 * @author filippo
 * @since 20/02/2017
 */
public class BeanGrid<T> extends Grid<T> {

	private static final long serialVersionUID = 2967915730889170532L;

	/**
	 * Istanzia una {@code BeanGrid} che utilizza
	 * un {@code ListDataProvider} vuoto e in
	 * sola lettura.
	 */
	public BeanGrid() {
		this(Collections.emptyList());
	}

	/**
	 * Istanzia una {@code BeanGrid} che utilizza
	 * un {@code ListDataProvider} vuoto e in
	 * sola lettura. *
	 *
	 * @param caption etichetta.
	 */
	public BeanGrid(final String caption) {
		super(caption, Collections.emptyList());
	}

	/**
	 * Istanzia una {@code BeanGrid} che utilizza
	 * un {@code ListDataProvider} contenente
	 * la lista specificata.
	 *
	 * @param items lista da utilizzare.
	 */
	public BeanGrid(final List<T> items) {
		super();
		this.setItems(items);
	}

	/**
	 * Istanzia una {@code BeanGrid} che utilizza
	 * un {@code ListDataProvider} contenente
	 * la lista specificata.
	 *
	 * @param caption etichetta.
	 * @param items lista da utilizzare.
	 */
	public BeanGrid(final String caption, final List<T> items) {
		super(caption, items);
	}

	/**
	 * Istanzia una {@code BeanGrid} utilizzabile
	 * tramite le proprietà dei bean (classi "underscore").
	 *
	 * @param beanClass tipo della classe bean.
	 */
	public BeanGrid(final Class<T> beanClass) {
		super(beanClass);
	}

	@SuppressWarnings("unchecked")
	private ListDataProvider<T> getListDataProviderOrThrow() {
		// FIXME evitare il cast se possibile
		final DataProvider<T, ?> dataProvider = this.getDataProvider();
		if (dataProvider instanceof ListDataProvider) {
			return (ListDataProvider<T>) dataProvider;
		}
		throw new IllegalStateException("Data provider is not a ListDataProvider instance");
	}

	/**
	 * Aggiunge le righe specificate.
	 *
	 * @param items elementi da aggiungere.
	 * @throws IllegalStateException se il data provider
	 *             non è un'instanza di {@code ListDataProvider}.
	 * @throws UnsupportedOperationException se la lista
	 *             contenuta nel {@code ListDataProvider} non è modificabile.
	 */
	@SuppressWarnings("unchecked")
	public void addItems(final T... items) {
		final ListDataProvider<T> listDataProvider = this.getListDataProviderOrThrow();
		Collections.addAll(this.getItems(), items);
		listDataProvider.refreshAll();
	}

	/**
	 * Rimuove le righe specificate.
	 *
	 * @param items elementi da rimuovere.
	 * @throws IllegalStateException se il data provider
	 *             non è un'instanza di {@code ListDataProvider}.
	 * @throws UnsupportedOperationException se la lista
	 *             contenuta nel {@code ListDataProvider} non è modificabile.
	 */
	@SuppressWarnings("unchecked")
	public void removeItems(final T... items) {
		final ListDataProvider<T> listDataProvider = this.getListDataProviderOrThrow();
		for (final T item : items) {
			listDataProvider.getItems().remove(item);
		}
		listDataProvider.refreshAll();
	}

	/**
	 * Restituisce le righe.
	 *
	 * @throws IllegalStateException se il data provider
	 *             non è un'instanza di {@code ListDataProvider}.
	 */
	public Collection<T> getItems() {
		return this.getListDataProviderOrThrow().getItems();
	}

	/**
	 * Restituisce le righe nell'ordine definito dall'utente.
	 *
	 * @throws IllegalStateException se il data provider
	 *             non è un'instanza di {@code ListDataProvider}.
	 */
	public List<T> getSortedItems() {
		final List<GridSortOrder<T>> sortOrders = this.getSortOrder();
		final Iterator<GridSortOrder<T>> iterator = sortOrders.iterator();
		if (iterator.hasNext()) {
			GridSortOrder<T> sortOrder = iterator.next();
			Comparator<T> comparator = sortOrder.getSorted().getComparator(sortOrder.getDirection());
			while (iterator.hasNext()) {
				sortOrder = iterator.next();
				comparator = comparator.thenComparing(sortOrder.getSorted().getComparator(sortOrder.getDirection()));
			}
			return this.getItems().stream().sorted(comparator).collect(Collectors.toList());
		}
		return new ArrayList<>(this.getItems());
	}

	/**
	 * Aggiunge una colonna tramite proprietà.
	 * <br/>
	 * Il nome della proprietà verrà utilizzato
	 * come id.
	 * <p>
	 * Questo metodo è utilizzabile solamente
	 * tramite il costruttore {@linkplain #BeanGrid(Class)}.
	 * </p>
	 *
	 * @param property proprietà del bean.
	 * @return la colonna aggiunta.
	 */
	@SuppressWarnings("unchecked")
	public <X> Column<T, X> addColumn(final @NonNull BeanProperty<? super T, ?, ?, X> property) {
		return (Column<T, X>) super.addColumn(property.getName());
	}

	/**
	 * Aggiunge una colonna tramite proprietà.
	 * <br/>
	 * Il nome della proprietà verrà utilizzato
	 * come id.
	 * <p>
	 * Questo metodo è utilizzabile solamente
	 * tramite il costruttore {@linkplain #BeanGrid(Class)}.
	 * </p>
	 *
	 * @param property proprietà del bean.
	 * @param renderer il renderer da utilizzare.
	 * @return la colonna aggiunta.
	 */
	@SuppressWarnings("unchecked")
	public <X> Column<T, X> addColumn(final @NonNull BeanProperty<? super T, ?, ?, X> property,
			final @NonNull AbstractRenderer<? super T, ?> renderer) {
		return (Column<T, X>) super.addColumn(property.getName(), renderer);
	}

	/**
	 * Aggiunge una colonna per visualizzare dati numerici.
	 *
	 * @param valueProvider funzione che fornisce il valore di una riga.
	 * @param pattern pattern per la formattazione dei numeri.
	 */
	public <N extends Number> Column<T, N> addColumn(final @NonNull ValueProvider<T, N> valueProvider, final @NonNull NumberPattern pattern) {
		return this.addColumn(valueProvider, new DecimalFormat(pattern.getPattern()));
	}

	/**
	 * Aggiunge una colonna per visualizzare dati numerici.
	 *
	 * @param valueProvider funzione che fornisce il valore di una riga.
	 * @param decimalFormat formatter per la formattazione dei numeri.
	 */
	public <N extends Number> Column<T, N> addColumn(final @NonNull ValueProvider<T, N> valueProvider, final @NonNull DecimalFormat decimalFormat) {
		final Column<T, N> c = this.addColumn(valueProvider, GridRenderers.forNumber(decimalFormat));
		c.setStyleGenerator(e -> Styles.TEXT_ALIGN_RIGHT);
		return c;
	}

	/**
	 * Aggiunge una colonna per visualizzare date.
	 *
	 * @param valueProvider funzione che fornisce il valore di una riga.
	 * @param pattern pattern per la formattazione delle date.
	 */
	public Column<T, Date> addColumn(final @NonNull ValueProvider<T, Date> valueProvider, final @NonNull DatePattern pattern) {
		return this.addColumn(valueProvider, GridRenderers.forDate(pattern));
	}

	/**
	 * Aggiunge una colonna per visualizzare date/ore.
	 *
	 * @param valueProvider funzione che fornisce il valore di una riga.
	 * @param formatStyle stile di formattazione.
	 */
	public <D extends TemporalAccessor> Column<T, D> addColumn(final @NonNull ValueProvider<T, D> valueProvider, final FormatStyle formatStyle) {
		return this.addColumn(valueProvider, d -> {
			if (d == null) {
				return "";
			}
			final DateTimeFormatter pattern;
			if (d instanceof ChronoLocalDate) {
				pattern = DateTimeFormatter.ofLocalizedDate(formatStyle).withLocale(this.getLocale());
			} else if (d instanceof ChronoLocalDateTime) {
				pattern = DateTimeFormatter.ofLocalizedDateTime(formatStyle).withLocale(this.getLocale());
			} else if (d instanceof LocalTime) {
				pattern = DateTimeFormatter.ofLocalizedTime(formatStyle).withLocale(this.getLocale());
			} else {
				return d.toString();
			}
			return pattern.format(d);
		});
	}

	/**
	 * Aggiunge una colonna che renderizza un campo.
	 *
	 * <pre>
	 * grid.addFieldColumn(person -> {
	 * 	if (person.isNameShowable()) {
	 * 		TextField field = new TextField();
	 * 		field.setWidth("100%");
	 * 		return field;
	 * 	}
	 * 	return null;
	 * }, Person::getName, Person::setName);
	 * </pre>
	 *
	 * @param componentProvider funzione che fornisce il campo oppure {@code null}.
	 * @param getter funzione che restituisce i valore del campo a partire dalla riga,
	 *            se il campo è {@code null} non viene richiamata.
	 * @param setter funzione che imposta il valore del campo nella riga,
	 *            se il campo è {@code null} non viene richiamata.
	 */
	public <C extends HasValue<V> & Component, V> Column<T, C> addFieldColumn(
			final @NonNull SerializableFunction<T, C> componentProvider,
			final @NonNull SerializableFunction<T, V> getter,
			final @NonNull Setter<T, V> setter) {

		return super.addComponentColumn(item -> {
			final C field = componentProvider.apply(item);
			if (field == null) {
				return null;
			}
			field.setValue(getter.apply(item));
			field.addValueChangeListener(e -> setter.accept(item, e.getValue()));
			return field;
		});
	}

	/**
	 * Aggiunge una colonna che renderizza un campo.
	 *
	 * <pre>
	 * grid.addFieldColumn(TextField::new, Person::getName, Person::setName);
	 * </pre>
	 *
	 * @param componentProvider funzione che fornisce il campo oppure {@code null}.
	 * @param getter funzione che restituisce i valore del campo a partire dalla riga,
	 *            se il campo è {@code null} non viene richiamata.
	 * @param setter funzione che imposta il valore del campo nella riga,
	 *            se il campo è {@code null} non viene richiamata.
	 */
	public <C extends HasValue<V> & Component, V> Column<T, C> addFieldColumn(
			final @NonNull Supplier<C> componentProvider,
			final @NonNull SerializableFunction<T, V> getter,
			final @NonNull Setter<T, V> setter) {

		return this.addFieldColumn(item -> componentProvider.get(), getter, setter);
	}

	/**
	 * Aggiunge una colonna che renderizza una {@code FontIcon}.
	 * <br/>
	 * La colonna non sarà ordinabile e ridimensionabile.
	 *
	 * @param getter funzione che restituisce l'icona
	 *            a partire dalla riga.
	 * @return la colonna creata ed aggiunta.
	 */
	public Column<T, String> addIconColumn(final @NonNull SerializableFunction<T, FontIcon> getter) {
		return this.addColumn(item -> Optional.ofNullable(getter.apply(item))
				.map(FontIcon::getHtml).orElse(null), new HtmlRenderer())
				.setSortable(false).setResizable(false);
	}

	/**
	 * Deseleziona tutto e seleziona solo
	 * l'oggetto specificato se non è {@code null}.
	 *
	 * @param item oggetto da selezionare.
	 * @deprecated utilizzare {@link #setSelectedItem(Object)} che evita di propagare
	 *             un evento di deselezione per niente.
	 */
	@Deprecated
	public void selectSingle(final @Nullable T item) {
		final GridSelectionModel<T> selectionModel = this.getSelectionModel();
		selectionModel.deselectAll();
		if (item != null) {
			selectionModel.select(item);
		}
	}

	public void setSelectedItem(final @Nullable T item) {
		final GridSelectionModel<T> selectionModel = this.getSelectionModel();
		if (item == null) {
			selectionModel.deselectAll();
		} else {
			this.setSelectedItems(Collections.singleton(item));
		}
	}

	public void setSelectedItems(final Collection<T> items) {
		final GridSelectionModel<T> selectionModel = this.getSelectionModel();
		if (items.isEmpty()) {
			selectionModel.deselectAll();
			return;
		}
		if (selectionModel instanceof MultiSelectionModel) {
			final MultiSelectionModel<T> multiSelectionModel = (MultiSelectionModel<T>) selectionModel;
			multiSelectionModel.updateSelection(new HashSet<>(items), multiSelectionModel.getSelectedItems()
					.stream().filter(item -> !items.contains(item)).collect(Collectors.toCollection(HashSet::new)));
		} else if (items.size() > 1) {
			throw new IllegalArgumentException("Selection model must be a MultiSelectionModel instance to select multiple items");
		}
		selectionModel.select(items.iterator().next());
	}

	/**
	 * Seleziona tutte le righe.
	 *
	 * @throws IllegalStateException se il {@code selectionModel}
	 *             non è un'istanza di {@linkplain MultiSelectionModel}.
	 */
	public void selectAll() {
		final GridSelectionModel<T> selectionModel = this.getSelectionModel();
		if (selectionModel instanceof MultiSelectionModel) {
			((MultiSelectionModel<T>) selectionModel).selectAll();
		} else {
			throw new IllegalStateException("Selection model must be a MultiSelectionModel instance");
		}
	}

	@Override
	protected <V, P> com.vaadin.ui.Grid.Column<T, V> createColumn(final ValueProvider<T, V> valueProvider, final ValueProvider<V, P> presentationProvider, final AbstractRenderer<? super T, ? super P> renderer) {
		final com.vaadin.ui.Grid.Column<T, V> column = super.createColumn(valueProvider, presentationProvider, renderer);
		// sovreascrive il comparatore di default mettendo, solo per le stringhe, un comparatore case insesitive
		final SerializableComparator<T> c = column.getComparator(SortDirection.ASCENDING);
		column.setComparator((o1, o2) -> {
			final V v1 = valueProvider.apply(o1);
			final V v2 = valueProvider.apply(o2);
			return v1 instanceof String || v2 instanceof String
					? String.CASE_INSENSITIVE_ORDER.compare(Objects.toString(v1, ""), Objects.toString(v2, ""))
					: c.compare(o1, o2);
		});
		return column;
	}
}
