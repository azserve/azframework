package com.azserve.azframework.vaadin8.utils.el;

import java.beans.FeatureDescriptor;
import java.util.IllegalFormatException;
import java.util.Iterator;

import javax.el.ELContext;
import javax.el.ELException;
import javax.el.ELResolver;
import javax.el.PropertyNotWritableException;

/**
 * @author Hardy Ferentschik
 * @author Guillaume Smet
 */
public class RootResolver extends ELResolver {

	/**
	 * Name under which to bind a formatter to the EL context.
	 */
	public static final String FORMATTER = "formatter";
	private static final String FORMAT = "format";

	@Override
	public Class<?> getCommonPropertyType(final ELContext context, final Object base) {
		return null;
	}

	@Override
	public Iterator<FeatureDescriptor> getFeatureDescriptors(final ELContext context, final Object base) {
		return null;
	}

	@Override
	public Class<?> getType(final ELContext context, final Object base, final Object property) {
		return null;
	}

	@Override
	public Object getValue(final ELContext context, final Object base, final Object property) {
		return null;
	}

	@Override
	public boolean isReadOnly(final ELContext context, final Object base, final Object property) {
		return true;
	}

	@Override
	public void setValue(final ELContext context, final Object base, final Object property, final Object value) {
		throw new PropertyNotWritableException();
	}

	@Override
	public Object invoke(final ELContext context, final Object base, final Object method, final Class<?>[] paramTypes, final Object[] params) {
		if (!(base instanceof FormatterWrapper)) {
			return null;
		}

		// due to bugs in most EL implementations when it comes to evaluating varargs we take care of the formatter call
		// ourselves.
		return this.evaluateFormatExpression(context, method, params);
	}

	private Object evaluateFormatExpression(final ELContext context, final Object method, final Object[] params) {
		if (!FORMAT.equals(method)) {
			throw new ELException("Wrong method name 'formatter#" + method + "' does not exist. Only formatter#format is supported.");
		}

		if (params.length == 0) {
			throw new ELException("Invalid number of arguments to Formatter#format");
		}

		if (!(params[0] instanceof String)) {
			throw new ELException("The first argument to Formatter#format must be String");
		}

		final FormatterWrapper formatterWrapper = (FormatterWrapper) context.getVariableMapper()
				.resolveVariable(FORMATTER)
				.getValue(context);
		final Object[] formattingParameters = new Object[params.length - 1];
		System.arraycopy(params, 1, formattingParameters, 0, params.length - 1);

		Object returnValue;
		try {
			returnValue = formatterWrapper.format((String) params[0], formattingParameters);
			context.setPropertyResolved(true);
		} catch (final IllegalFormatException e) {
			throw new ELException("Error in Formatter#format call", e);
		}

		return returnValue;
	}

}
