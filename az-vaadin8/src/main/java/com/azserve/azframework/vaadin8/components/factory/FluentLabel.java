package com.azserve.azframework.vaadin8.components.factory;

import com.vaadin.server.Resource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;

/**
 * Classe per la costruzione rapida di una label.
 *
 * @author filippo
 * @since 02/02/2017
 */
public class FluentLabel extends Label {

	private static final long serialVersionUID = -967317106466363632L;

	public FluentLabel() {
		super();
	}

	public FluentLabel(final String text, final ContentMode contentMode) {
		super(text, contentMode);
	}

	public FluentLabel(final String text) {
		super(text);
	}

	public FluentLabel withWidthFull() {
		return this.withWidth(100, Unit.PERCENTAGE);
	}

	public FluentLabel withWidth(final float width, final Unit unit) {
		this.setWidth(width, unit);
		return this;
	}

	public FluentLabel withHeightFull() {
		return this.withHeight(100, Unit.PERCENTAGE);
	}

	public FluentLabel withHeight(final float height, final Unit unit) {
		this.setHeight(height, unit);
		return this;
	}

	public FluentLabel withSizeFull() {
		this.setSizeFull();
		return this;
	}

	public FluentLabel withContentMode(final ContentMode contentMode) {
		this.setContentMode(contentMode);
		return this;
	}

	public FluentLabel withCaption(final String caption) {
		this.setCaption(caption);
		return this;
	}

	public FluentLabel withCaption(final String caption, final boolean html) {
		this.setCaption(caption);
		this.setCaptionAsHtml(html);
		return this;
	}

	public FluentLabel withIcon(final Resource icon) {
		this.setIcon(icon);
		return this;
	}

	public FluentLabel withStyleName(final String style) {
		this.addStyleName(style);
		return this;
	}

	public FluentLabel withStyleNames(final String styles) {
		this.addStyleNames(styles);
		return this;
	}
}