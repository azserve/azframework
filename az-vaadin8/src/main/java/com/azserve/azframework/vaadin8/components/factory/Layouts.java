package com.azserve.azframework.vaadin8.components.factory;

import java.util.function.Function;

import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;

/**
 * Classe di utilità per la generazione di {@linkplain Layout}.
 *
 * @author filippo
 * @since 15/02/2017
 */
public final class Layouts {

	/**
	 * Restituisce un generatore di {@linkplain HorizontalLayout}.
	 *
	 * <p>Defaults:
	 * <ul>
	 * <li>spacing = true</li>
	 * </ul></p>
	 */
	public static OrderedLayoutFactory<HorizontalLayout> horizontal() {
		return new HorizontalLayoutFactory();
	}

	/**
	 * Restituisce un generatore di {@linkplain VerticalLayout}.
	 *
	 * <p>Defaults:
	 * <ul>
	 * <li>spacing = true</li>
	 * <li>margin = true</li>
	 * <li>width = 100%</li>
	 * </ul></p>
	 */
	public static OrderedLayoutFactory<VerticalLayout> vertical() {
		return new VerticalLayoutFactory();
	}

	/**
	 * Restituisce un generatore di {@linkplain FormLayout}.
	 *
	 * <p>Defaults:
	 * <ul>
	 * <li>spacing = true</li>
	 * <li>margin = vertical only</li>
	 * <li>width = 100%</li>
	 * </ul></p>
	 *
	 * @since 2.5.7
	 */
	public static OrderedLayoutFactory<FormLayout> form() {
		return new FormLayoutFactory();
	}

	/**
	 * Restituisce un generatore di {@linkplain GridLayout}.
	 *
	 * @param columns numero di colonne della griglia.
	 * @param rows numero di righe della griglia.
	 */
	public static GridLayoutFactory grid(final int columns, final int rows) {
		return new GridLayoutFactory(columns, rows);
	}

	/**
	 * Restituisce un generatore di {@linkplain CssLayout}.
	 *
	 * @param cssProvider funzione per la generazione di css
	 *            per i componenti figli.
	 */
	public static CssLayoutFactory css(final Function<Component, String> cssProvider) {
		return new CssLayoutFactory(cssProvider);
	}

	/**
	 * Restituisce un generatore di {@linkplain CssLayout}.
	 */
	public static CssLayoutFactory css() {
		return new CssLayoutFactory();
	}

	/**
	 * Restituisce un generatore di {@linkplain Panel}.
	 */
	public static PanelFactory panel() {
		return new PanelFactory();
	}

	private Layouts() {}
}
