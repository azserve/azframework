package com.azserve.azframework.vaadin8.fields;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import com.azserve.azframework.common.util.DateUtils;
import com.azserve.azframework.common.util.DateUtils.DatePattern;
import com.vaadin.data.Result;
import com.vaadin.shared.ui.datefield.DateTimeResolution;
import com.vaadin.ui.DateTimeField;

public class SmartDateTimeField extends DateTimeField {

	private static final long serialVersionUID = 7327218172969190540L;

	public SmartDateTimeField() {
		this(null);
	}

	public SmartDateTimeField(final String caption) {
		super(caption);
		this.setDateFormat(DatePattern.DD_MM_YYYY_HH_MM.getPattern());
	}

	@Override
	protected LocalDateTime buildDate(final Map<DateTimeResolution, Integer> resolutionValues) {
		final LocalDateTime date = super.buildDate(resolutionValues);
		if (date != null) {
			final int year = date.get(ChronoField.YEAR);
			if (year < 100) {
				if (year >= 0 && year < 100) {
					return date.withYear(DateUtils.year2Digits(year));
				} else if (year < 1000 || year > 9999) { // three o five+ digits -> wrong date
					return null;
				}
			} else if (year < 1000 || year > 9999) { // three o five+ digits -> wrong date
				return null;
			}
		}
		return date;
	}

	@Override
	protected Result<LocalDateTime> handleUnparsableDateString(final String dateString) {
		return Result.ok(DateUtils.toLocalDateTime(smartConverter(dateString)));
	}

	private static Date smartConverter(final String dateString) {
		final String str = dateString.trim();
		if (str.isEmpty()) {
			return null;
		}

		final char first = str.charAt(0);
		// "0" -> today
		if (first == '0' && str.length() == 1) {
			return DateUtils.today();
		}

		final int sign = first == '+' ? 1 : first == '-' ? -1 : 0;

		try {
			final int value = Integer.parseInt(str);
			// "1" -> day 1 of current month, "+1" -> tomorrow, "-1" -> yesterday
			if (sign == 0) {
				final Calendar c = Calendar.getInstance();
				c.set(Calendar.DAY_OF_MONTH, value);
				return c.getTime();
			}
			return DateUtils.addDays(DateUtils.today(), value);
		} catch (final Exception e) {}

		final String s1 = sign == 0 ? str : str.substring(1);
		if (s1.isEmpty()) {
			return null;
		}

		try {
			return DateUtils.syncParse(str.replaceAll("\\D", "/"));
		} catch (final Exception ex1) {
			final Calendar calendarInstance = Calendar.getInstance();
			try {
				return DateUtils.syncParse(str.replaceAll("\\D", "/") + "/" + calendarInstance.get(Calendar.YEAR));
			} catch (final Exception ex2) {}
		}
		// data non valida: la sbianco
		return null;
	}

}
