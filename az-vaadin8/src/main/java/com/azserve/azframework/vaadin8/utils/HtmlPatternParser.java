package com.azserve.azframework.vaadin8.utils;

import java.util.UUID;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.azserve.azframework.common.lang.StringUtils;

public final class HtmlPatternParser {

	private HtmlPatternParser() {}

	public static String toPatternContent(final String htmlContent, final String pattern, final Function<Matcher, UUID> resourceToPlaceholder) {
		if (StringUtils.isNullOrEmpty(htmlContent)) {
			return "";
		}
		final Pattern ptrn = Pattern.compile(pattern);
		final Matcher matcher = ptrn.matcher(htmlContent);
		final StringBuilder sb = new StringBuilder();
		int i = 0;
		while (matcher.find()) {
			sb.append(htmlContent.substring(i, matcher.start()));
			final String placeholder = resourceToPlaceholder.apply(matcher).toString();
			sb.append("\"${img:" + placeholder + "}$\"");
			i = matcher.end();
		}
		sb.append(htmlContent.substring(i));
		return sb.toString();
	}

	public static String toHtmlContent(final String patternContent, final Function<UUID, String> placeholderToResource) {
		if (StringUtils.isNullOrEmpty(patternContent)) {
			return "";
		}
		final Pattern p = Pattern.compile("\\$\\{(img:[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})\\}\\$");
		final Matcher matcher = p.matcher(patternContent);
		final StringBuilder sb = new StringBuilder();
		int i = 0;

		while (matcher.find()) {
			sb.append(patternContent.substring(i, matcher.start()));
			final String placeholder = matcher.group(1);
			if (placeholder.startsWith("img:")) {
				sb.append(placeholderToResource.apply(UUID.fromString(placeholder.substring(4))));
			}
			i = matcher.end();
		}
		sb.append(patternContent.substring(i));
		return sb.toString();
	}
}
