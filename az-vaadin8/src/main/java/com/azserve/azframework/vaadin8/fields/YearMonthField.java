package com.azserve.azframework.vaadin8.fields;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.time.format.TextStyle;
import java.util.Locale;

import org.vaadin.viritin.util.HtmlElementPropertySetter;

import com.azserve.azframework.common.util.DateUtils;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomField;

public class YearMonthField extends CustomField<YearMonth> {

	private static final long serialVersionUID = -8836192156641729468L;

	private final CssLayout root;
	private final IntegerField years;
	private final ComboBox<Month> months;

	private YearMonth internalValue;

	public YearMonthField() {
		this(null);
	}

	public YearMonthField(final String caption) {
		this.setCaption(caption);

		this.years = new FormattedIntegerField();
		this.years.setWidth(5, Unit.EM);

		this.months = new ComboBox<>();
		this.months.setTextInputAllowed(false);
		this.months.setPageLength(Month.values().length);
		this.months.setItems(Month.values());
		this.months.setEmptySelectionAllowed(false);

		this.root = new CssLayout(this.months, this.years);

		this.years.addValueChangeListener(e -> {
			final Integer value = e.getValue();
			final Month monthValue = this.months.getValue();
			if (value != null) {
				int valueAsInt = value.intValue();
				if (e.isUserOriginated()) {
					valueAsInt = DateUtils.year2Digits(valueAsInt);
				}
				this.setValue(YearMonth.of(valueAsInt, monthValue != null ? monthValue : Month.JANUARY));
			} else if (monthValue != null) {
				this.years.setValue(Integer.valueOf(LocalDate.now().getYear()));
			}
		});

		this.months.addValueChangeListener(e -> {
			final Month value = e.getValue();
			final Integer hoursValue = this.years.getValue();
			if (value != null) {
				this.setValue(YearMonth.of(hoursValue == null ? LocalDate.now().getYear() : hoursValue.intValue(), value));
			} else if (hoursValue != null) {
				this.months.setValue(Month.JANUARY);
			}
		});
	}

	@Override
	public void attach() {
		super.attach();
		final Locale clientLocale = this.getLocale();
		final Locale locale;
		if (clientLocale != null) {
			locale = clientLocale;
		} else {
			locale = Locale.getDefault();
		}
		this.months.setItemCaptionGenerator(m -> m.getDisplayName(TextStyle.FULL, locale));
	}

	@Override
	public YearMonth getValue() {
		return this.internalValue;
	}

	@Override
	protected Component initContent() {
		return this.root;
	}

	@Override
	protected void doSetValue(final YearMonth value) {
		this.internalValue = value;
		if (value == null) {
			this.years.clear();
			this.months.clear();
		} else {
			this.years.setValue(Integer.valueOf(value.getYear()));
			this.months.setValue(value.getMonth());
		}
	}

	private final class FormattedIntegerField extends IntegerField {

		private static final long serialVersionUID = 6267862011977533875L;

		FormattedIntegerField() {}

		@Override
		protected void configureHtml(final HtmlElementPropertySetter heps) {
			super.configureHtml(heps);
			heps.setProperty("maxlength", "4");
			heps.setProperty("min", "1000");
			heps.setProperty("max", "2999");
		}
	}
}
