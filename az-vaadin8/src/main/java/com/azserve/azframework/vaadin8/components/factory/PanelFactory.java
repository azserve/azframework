package com.azserve.azframework.vaadin8.components.factory;

import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;

public final class PanelFactory extends AbstractLayoutFactory<Panel, PanelFactory> {

	PanelFactory() {
		super();
	}

	PanelFactory(final Panel layout) {
		super(layout);
	}

	public PanelFactory withContent(final Component content) {
		this.get().setContent(content);
		return this;
	}

	public PanelFactory withTabIndex(final int tabIndex) {
		this.get().setTabIndex(tabIndex);
		return this;
	}

	@Override
	public PanelFactory add(final Component c) {
		final Panel panel = this.get();
		if (panel.getContent() != null) {
			throw new IllegalStateException("There is a content already set");
		}
		panel.setContent(c);
		return this;
	}

	@Override
	protected Panel supplyLayout() {
		return new Panel();
	}
}
