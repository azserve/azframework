package com.azserve.azframework.vaadin8.utils.el;

import javax.el.ArrayELResolver;
import javax.el.BeanELResolver;
import javax.el.CompositeELResolver;
import javax.el.ELResolver;
import javax.el.ExpressionFactory;
import javax.el.ListELResolver;
import javax.el.MapELResolver;
import javax.el.ResourceBundleELResolver;
import javax.el.StandardELContext;

/**
 * @author Hardy Ferentschik
 * @author Guillaume Smet
 */
public class SimpleELContext extends StandardELContext {

	private static final ELResolver DEFAULT_RESOLVER = new CompositeELResolver() {

		{
			this.add(new RootResolver());
			this.add(new ArrayELResolver(true));
			this.add(new ListELResolver(true));
			this.add(new MapELResolver(true));
			this.add(new ResourceBundleELResolver());
			this.add(new BeanELResolver(true));
		}
	};

	public SimpleELContext(final ExpressionFactory expressionFactory) {
		super(expressionFactory);

		// In javax.el.ELContext, the ExpressionFactory is extracted from the context map. If it is not found, it
		// defaults to ELUtil.getExpressionFactory() which, if we provided the ExpressionFactory to the
		// ResourceBundleMessageInterpolator, might not be the same. Thus, we inject the ExpressionFactory in the
		// context.
		this.putContext(ExpressionFactory.class, expressionFactory);
	}

	@Override
	public void addELResolver(final ELResolver cELResolver) {
		throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support addELResolver.");
	}

	@Override
	public ELResolver getELResolver() {
		return DEFAULT_RESOLVER;
	}

}