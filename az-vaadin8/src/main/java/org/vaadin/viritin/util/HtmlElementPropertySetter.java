package org.vaadin.viritin.util;

import com.vaadin.server.AbstractJavaScriptExtension;
import com.vaadin.ui.AbstractComponent;

/**
 * A simple extension to set custom properties for the html elements used in
 * Vaadin components. By default element are set to the Root element, but an
 * xpath expression can be given for setting the property to any element.
 * <p>
 * Note, be careful when using this add-on as it involves web "real"
 * development. Consider hiding all usage into your custom components.
 *
 * @author Matti Tahvonen
 */
@com.vaadin.annotations.JavaScript("viritin.js")
public class HtmlElementPropertySetter extends AbstractJavaScriptExtension {

	private static final long serialVersionUID = 6463148772177927232L;

	public HtmlElementPropertySetter(final AbstractComponent c) {
		this.extend(c);
	}

	public void setProperty(final String name, final Object value) {
		this.callFunction("xpathset", Boolean.FALSE, null, name, value);
	}

	public void setProperty(final String xpath, final String name, final Object value) {
		this.callFunction("xpathset", Boolean.FALSE, xpath, name, value);
	}

	public void setJavaScriptEventHandler(final String name, final String js) {
		this.callFunction("xpathset", Boolean.TRUE, null, name, js);
	}

	public void setJavaScriptEventHandler(final String xpath, final String name, final String js) {
		this.callFunction("xpathset", Boolean.TRUE, xpath, name, js);
	}

}
