package com.azserve.framework.test.ejb.beans.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.azserve.azframework.entity.AbstractEntity;
import com.azserve.framework.test.ejb.beans.Gender;

@Entity
public class Employee extends AbstractEntity<UUID> {

	private static final long serialVersionUID = -2454881211140864588L;

	@Id
	@Column(columnDefinition = "UUID")
	private UUID id;
	private String firstName;
	private String lastName;
	private Gender gender;
	private Date birthDate;
	private Date hireDate;

	public Employee(final UUID id, final String firstName, final String lastName, final Gender gender, final Date birthDate, final Date hireDate) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.birthDate = birthDate;
		this.hireDate = hireDate;
	}

	public Employee() {}

	@Override
	public UUID getId() {
		return this.id;
	}

	@Override
	public void setId(final UUID id) {
		this.id = id;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public Gender getGender() {
		return this.gender;
	}

	public void setGender(final Gender gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(final Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getHireDate() {
		return this.hireDate;
	}

	public void setHireDate(final Date hireDate) {
		this.hireDate = hireDate;
	}

}
