package com.azserve.framework.test.ejb.beans.manager;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.azserve.azframework.ejb.eao.ApplicationEAO;

@Stateless
@LocalBean
public class TestEAO extends ApplicationEAO {

	@PersistenceContext
	private EntityManager em;

	@Resource
	private SessionContext sessionContext;

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}

	@Override
	public SessionContext getSessionContext() {
		return this.sessionContext;
	}

}
