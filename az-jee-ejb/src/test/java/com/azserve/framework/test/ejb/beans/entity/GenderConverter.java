package com.azserve.framework.test.ejb.beans.entity;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.azserve.azframework.common.enums.EnumUtils;
import com.azserve.framework.test.ejb.beans.Gender;

@Converter(autoApply = true)
public class GenderConverter implements AttributeConverter<Gender, String> {

	@Override
	public String convertToDatabaseColumn(final Gender gender) {
		return EnumUtils.key(gender);
	}

	@Override
	public Gender convertToEntityAttribute(final String key) {
		return EnumUtils.valueOfKey(Gender.class, key);
	}

}
