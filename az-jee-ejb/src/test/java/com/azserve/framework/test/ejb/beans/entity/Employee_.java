package com.azserve.framework.test.ejb.beans.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import com.azserve.framework.test.ejb.beans.Gender;

@StaticMetamodel(Employee.class)
public abstract class Employee_ {

	public static volatile SingularAttribute<Department, UUID> id;
	public static volatile SingularAttribute<Department, String> firstName;
	public static volatile SingularAttribute<Department, String> lastName;
	public static volatile SingularAttribute<Department, Gender> gender;
	public static volatile SingularAttribute<Department, Date> birthDate;
	public static volatile SingularAttribute<Department, Date> hireDate;

}