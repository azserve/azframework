package com.azserve.framework.test.ejb.beans.manager;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.dto.DtoReferenceWithString;
import com.azserve.azframework.dto.DtoUtils;
import com.azserve.azframework.ejb.eao.ApplicationEAO;
import com.azserve.azframework.ejb.manager.EntityDtoManager;
import com.azserve.azframework.ejb.manager.EntitySecurityManager;
import com.azserve.azframework.ejb.manager.InsertHandler;
import com.azserve.azframework.ejb.manager.UpdateHandler;
import com.azserve.azframework.exception.AzInvalidDataException;
import com.azserve.azframework.exception.AzServiceException;
import com.azserve.framework.test.ejb.beans.dto.DepartmentDTO;
import com.azserve.framework.test.ejb.beans.entity.Department;

@Stateless
@LocalBean
public class DepartmentManagerHandler extends EntityDtoManager<Department, String, DepartmentDTO, String, DtoReferenceWithString<DepartmentDTO>, Serializable> {

	@EJB
	private TestEAO eao;

	@Override
	protected @NonNull Class<Department> getEntityClass() {
		return Department.class;
	}

	@Override
	protected @NonNull ApplicationEAO getEao() {
		return this.eao;
	}

	@Override
	protected @NonNull Department newEntity() {
		return new Department();
	}

	@Override
	protected void fillEntity(final @NonNull DepartmentDTO dto, final @NonNull Department entity) {
		entity.setName(dto.getName());
	}

	@Override
	protected @NonNull DepartmentDTO newDto(final @NonNull Department entity) {
		return new DepartmentDTO(this.toReference(entity));
	}

	@Override
	protected @NonNull DtoReferenceWithString<DepartmentDTO> toReference(final @NonNull String id) {
		return DtoUtils.createReference(id);
	}

	@Override
	protected void fillDto(final @NonNull DepartmentDTO dto, final @NonNull Department entity) {
		dto.setName(entity.getName());
	}

	@SuppressWarnings("unchecked")
	@Override
	protected @NonNull EntitySecurityManager<Department, Serializable> createSecurityManager() {
		return EntitySecurityManager.NO_SECURITY;
	}

	@Override
	protected @NonNull String toId(final DtoReferenceWithString<DepartmentDTO> reference) {
		return reference.getId();
	}

	@Override
	protected @NonNull InsertHandler<Department, DepartmentDTO> createInsertHandler(final @NonNull ApplicationEAO _eao) {
		return new InsertHandler<Department, DepartmentDTO>() {

			@Override
			public void beforeInsert(final @NonNull DepartmentDTO dto, final @NonNull Department entity) throws AzServiceException {
				dto.setName(dto.getName() + "B");
			}

			@Override
			public void afterInsert(final @NonNull DepartmentDTO dto, final @NonNull Department entity) throws AzServiceException {
				dto.setName(dto.getName() + "A");
			}

			@Override
			public void validateInsert(final @NonNull DepartmentDTO dto) throws AzInvalidDataException {
				dto.setName(dto.getName() + "V");
			}
		};
	}

	@Override
	protected @NonNull UpdateHandler<Department, DepartmentDTO> createUpdateHandler(final @NonNull ApplicationEAO _eao) {
		return new UpdateHandler<Department, DepartmentDTO>() {

			@Override
			public void beforeUpdate(final @NonNull DepartmentDTO dto, final @NonNull Department entity) throws AzServiceException {
				dto.setName(dto.getName() + "B");
			}

			@Override
			public void afterUpdate(final @NonNull DepartmentDTO dto, final @NonNull Department entity) throws AzServiceException {
				dto.setName(dto.getName() + "A");
			}

			@Override
			public void validateAganistEntity(final @NonNull DepartmentDTO dto, final @NonNull Department entity) throws AzInvalidDataException {
				dto.setName(dto.getName() + "E");
			}

			@Override
			public void validateUpdate(final @NonNull DepartmentDTO dto) throws AzInvalidDataException {
				dto.setName(dto.getName() + "V");
			}
		};
	}
}
