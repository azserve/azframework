package com.azserve.framework.test.ejb.beans.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.azserve.azframework.entity.EntityWithString;

@Entity
public class Department extends EntityWithString {

	private static final long serialVersionUID = 2992232614947150433L;

	@Id
	private String id;
	private String name;

	public Department() {}

	public Department(final String id, final String name) {
		this.id = id;
		this.name = name;
	}

	@Override
	public String getId1() {
		return this.id;
	}

	@Override
	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

}
