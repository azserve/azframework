package com.azserve.framework.test.ejb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.test.TestUtils;
import com.azserve.azframework.dto.DtoReferenceWithString;
import com.azserve.azframework.dto.DtoReferenceWithUuid;
import com.azserve.azframework.dto.DtoUtils;
import com.azserve.azframework.ejb.manager.UpdateValidator;
import com.azserve.azframework.exception.AzInvalidDataException;
import com.azserve.azframework.exception.AzServiceException;
import com.azserve.framework.test.ejb.beans.Gender;
import com.azserve.framework.test.ejb.beans.dto.DepartmentDTO;
import com.azserve.framework.test.ejb.beans.dto.EmployeeDTO;
import com.azserve.framework.test.ejb.beans.entity.Department;
import com.azserve.framework.test.ejb.beans.manager.DepartmentManagerHandler;
import com.azserve.framework.test.ejb.beans.manager.EmployeeManager;

@RunWith(Arquillian.class)
public class ManagerTest extends Deployments {

	@Test
	public void test(final EmployeeManager employeeManager) throws AzServiceException {

		// insert john doe
		final EmployeeDTO e1 = new EmployeeDTO();
		e1.setFirstName("John");
		e1.setLastName("Doe");
		e1.setGender(Gender.MALE);
		final DtoReferenceWithUuid<EmployeeDTO> r1;
		{
			final EmployeeDTO inserted = employeeManager.insert(e1);
			r1 = inserted.getReference();
			assertEquals("John", inserted.getFirstName());
			assertEquals("Doe", inserted.getLastName());
			assertEquals(Gender.MALE, inserted.getGender());
			assertNull(inserted.getBirthDate());
		}

		// insert john smith
		final EmployeeDTO e2 = new EmployeeDTO();
		e2.setFirstName("John");
		e2.setLastName("Smith");
		e2.setGender(Gender.MALE);
		final DtoReferenceWithUuid<EmployeeDTO> r2;
		{
			final EmployeeDTO inserted = employeeManager.insert(e2);
			r2 = inserted.getReference();
			assertEquals("John", inserted.getFirstName());
			assertEquals("Smith", inserted.getLastName());
			assertEquals(Gender.MALE, inserted.getGender());
			assertNull(inserted.getBirthDate());
		}

		// change john Doe to jane Doe
		final EmployeeDTO d1 = employeeManager.findDto(r1);
		d1.setFirstName("Jane");
		d1.setGender(Gender.FEMALE);
		employeeManager.update(d1);
		{
			final EmployeeDTO updated = employeeManager.findDto(r1);
			assertEquals("Jane", updated.getFirstName());
			assertEquals("Doe", updated.getLastName());
			assertEquals(Gender.FEMALE, updated.getGender());
			assertNull(updated.getBirthDate());

			final EmployeeDTO notUpdated = employeeManager.findDto(r2);
			assertEquals("John", notUpdated.getFirstName());
			assertEquals("Smith", notUpdated.getLastName());
			assertEquals(Gender.MALE, notUpdated.getGender());
			assertNull(notUpdated.getBirthDate());
		}

		// update not null dto constraint
		d1.setGender(null);
		TestUtils.assertThrows(AzInvalidDataException.class, () -> employeeManager.update(d1));
		assertEquals(Gender.FEMALE, employeeManager.findDto(r1).getGender());

		// insert not null dto constraint
		final EmployeeDTO e3 = new EmployeeDTO();
		e3.setFirstName("John");
		e3.setLastName("Smith");
		e3.setGender(null);
		TestUtils.assertThrows(AzInvalidDataException.class, () -> employeeManager.insert(e3).getReference());

		employeeManager.delete(r1);
		employeeManager.delete(r2);
		assertTrue(employeeManager.query(null).fetch().isEmpty());
	}

	@Test
	public void testDefaultHandler(final DepartmentManagerHandler departmentManager) throws AzServiceException {
		final DtoReferenceWithString<DepartmentDTO> ref = DtoUtils.createReference("D1");

		final DepartmentDTO d1 = new DepartmentDTO(ref);
		d1.setName("");
		final DepartmentDTO newD1 = departmentManager.insert(d1);
		assertEquals("VBA", d1.getName());
		assertEquals("V", departmentManager.findDto(ref).getName());

		newD1.setName("");
		departmentManager.update(newD1);
		assertEquals("VEBA", newD1.getName());
		final DepartmentDTO dto = departmentManager.findDto(ref);
		assertEquals("VE", dto.getName());

		// findDto
		{
			assertNull(departmentManager.findDto((String) null));
			assertNull(departmentManager.findDto(DtoUtils.createReference("ZZ")));
		}

		// findEntity
		{
			final Department eFromDto = departmentManager.findEntity(dto).get();
			assertEquals(dto.getId(), eFromDto.getId());
			assertEquals(dto.getName(), eFromDto.getName());
			assertFalse(departmentManager.findEntity((DepartmentDTO) null).isPresent());
			final DepartmentDTO dummy = new DepartmentDTO();
			assertFalse(departmentManager.findEntity(dummy).isPresent());

			final Department eFromRef = departmentManager.findEntity(ref).get();
			assertEquals(dto.getId(), eFromRef.getId());
			assertEquals(dto.getName(), eFromRef.getName());
			assertFalse(departmentManager.findEntity((DtoReferenceWithString<DepartmentDTO>) null).isPresent());
			assertFalse(departmentManager.findEntity(DtoUtils.createReference("ZZ")).isPresent());

			final Department eFromId = departmentManager.findEntity(ref.getId()).get();
			assertEquals(dto.getId(), eFromId.getId());
			assertEquals(dto.getName(), eFromId.getName());
			assertFalse(departmentManager.findEntity((String) null).isPresent());
			assertFalse(departmentManager.findEntity("ZZ").isPresent());
		}

		// findEntityReference
		{
			// hibernate necessita di una sessione
			// testSessionBean.testReference(ref, dto);

		}
		// non so come testare in modo semplice gli handler di default del metodo delete
		departmentManager.delete(ref);
	}

	@Test
	public void testCustomHandler(final DepartmentManagerHandler departmentManager) throws AzServiceException {

		final DtoReferenceWithString<DepartmentDTO> ref = DtoUtils.createReference("D2");

		final DepartmentDTO d1 = new DepartmentDTO(ref);
		d1.setName("");
		final StringBuilder xi = new StringBuilder();
		departmentManager.insert(d1, d -> xi.append("v"), (d, e) -> xi.append("b"), (d, e) -> xi.append("a"));
		assertEquals("vba", xi.toString()); // risultato handler
		final DepartmentDTO dto = departmentManager.findDto(ref);
		assertEquals("", dto.getName()); // non ha usato gli handler di default

		final StringBuilder xu = new StringBuilder();
		departmentManager.update(dto, new UpdateValidator<Department, DepartmentDTO>() {

			@Override
			public void validateUpdate(final @NonNull DepartmentDTO _dto) throws AzInvalidDataException {
				xu.append("v");

			}

			@Override
			public void validateAganistEntity(final @NonNull DepartmentDTO _dto, final @NonNull Department entity) throws AzInvalidDataException {
				xu.append("e");
			}
		}, (d, e) -> xu.append("b"), (d, e) -> xu.append("a"));
		assertEquals("veba", xu.toString());// risultato handler
		final DepartmentDTO dto2 = departmentManager.findDto(ref);
		assertEquals("", dto2.getName());// non ha usato gli handler di default

		final StringBuilder x = new StringBuilder();
		departmentManager.delete(ref, eao -> x.append("b"), eao -> x.append("a"));
		assertEquals("ba", x.toString());// risultato handler

	}
}
