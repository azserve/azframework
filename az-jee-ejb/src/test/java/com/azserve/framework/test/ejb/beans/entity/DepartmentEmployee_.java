package com.azserve.framework.test.ejb.beans.entity;

import java.util.Date;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(DepartmentEmployee.class)
public abstract class DepartmentEmployee_ {

	public static volatile SingularAttribute<DepartmentEmployee, Integer> id;
	public static volatile SingularAttribute<DepartmentEmployee, Department> department;
	public static volatile SingularAttribute<DepartmentEmployee, Employee> employee;
	public static volatile SingularAttribute<DepartmentEmployee, Date> fromDate;
	public static volatile SingularAttribute<DepartmentEmployee, Date> toDate;

}