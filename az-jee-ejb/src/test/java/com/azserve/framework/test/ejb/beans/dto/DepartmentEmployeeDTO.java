package com.azserve.framework.test.ejb.beans.dto;

import java.util.Date;

import com.azserve.azframework.dto.DtoReferenceWithInteger;
import com.azserve.azframework.dto.DtoReferenceWithString;
import com.azserve.azframework.dto.DtoReferenceWithUuid;
import com.azserve.azframework.dto.DtoWithInteger;

public class DepartmentEmployeeDTO extends DtoWithInteger<DepartmentEmployeeDTO> {

	private static final long serialVersionUID = 6192224331319925583L;

	private DtoReferenceWithString<DepartmentDTO> department;
	private DtoReferenceWithUuid<EmployeeDTO> employee;
	private Date from;
	private Date to;

	public DepartmentEmployeeDTO() {}

	public DepartmentEmployeeDTO(final DtoReferenceWithInteger<DepartmentEmployeeDTO> id) {
		super(id);
	}

	public DtoReferenceWithString<DepartmentDTO> getDepartment() {
		return this.department;
	}

	public void setDepartment(final DtoReferenceWithString<DepartmentDTO> department) {
		this.department = department;
	}

	public DtoReferenceWithUuid<EmployeeDTO> getEmployee() {
		return this.employee;
	}

	public void setEmployee(final DtoReferenceWithUuid<EmployeeDTO> employee) {
		this.employee = employee;
	}

	public Date getFrom() {
		return this.from;
	}

	public void setFrom(final Date from) {
		this.from = from;
	}

	public Date getTo() {
		return this.to;
	}

	public void setTo(final Date to) {
		this.to = to;
	}
}
