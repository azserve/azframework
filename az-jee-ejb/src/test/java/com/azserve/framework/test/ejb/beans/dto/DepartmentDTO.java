package com.azserve.framework.test.ejb.beans.dto;

import com.azserve.azframework.dto.DtoReferenceWithString;
import com.azserve.azframework.dto.DtoWithString;

public class DepartmentDTO extends DtoWithString<DepartmentDTO> {

	private static final long serialVersionUID = 2971757447952529545L;

	private String name;

	public DepartmentDTO() {
		super();
	}

	public DepartmentDTO(final DtoReferenceWithString<DepartmentDTO> reference) {
		super(reference);
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}
}
