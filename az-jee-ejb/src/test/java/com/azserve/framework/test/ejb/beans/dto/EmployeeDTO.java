package com.azserve.framework.test.ejb.beans.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.azserve.azframework.dto.DtoReferenceWithUuid;
import com.azserve.azframework.dto.DtoWithUuid;
import com.azserve.framework.test.ejb.beans.Gender;

public class EmployeeDTO extends DtoWithUuid<EmployeeDTO> {

	private static final long serialVersionUID = -824936016084564458L;

	private String firstName;
	private String lastName;
	@NotNull
	private Gender gender;
	private Date birthDate;
	private Date hireDate;

	public EmployeeDTO() {
		super();
	}

	public EmployeeDTO(final DtoReferenceWithUuid<EmployeeDTO> reference) {
		super(reference);
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public Gender getGender() {
		return this.gender;
	}

	public void setGender(final Gender gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(final Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getHireDate() {
		return this.hireDate;
	}

	public void setHireDate(final Date hireDate) {
		this.hireDate = hireDate;
	}

}
