package com.azserve.framework.test.ejb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;

import com.azserve.azframework.common.test.TestUtils;
import com.azserve.azframework.dto.DtoReferenceWithString;
import com.azserve.azframework.dto.DtoUtils;
import com.azserve.framework.test.ejb.beans.dto.DepartmentDTO;
import com.azserve.framework.test.ejb.beans.entity.Department;
import com.azserve.framework.test.ejb.beans.entity.DepartmentEmployee;
import com.azserve.framework.test.ejb.beans.entity.Employee;
import com.azserve.framework.test.ejb.beans.manager.DepartmentManagerHandler;
import com.azserve.framework.test.ejb.beans.manager.TestEAO;

@Stateless
@LocalBean
public class TestSessionBean {

	@EJB
	private TestEAO eao;
	@EJB
	private DepartmentManagerHandler departmentManager;

	public void clearQueryTestData() {
		final EntityManager em = this.eao.getEntityManager();
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		{
			final CriteriaDelete<DepartmentEmployee> cd = cb.createCriteriaDelete(DepartmentEmployee.class);
			cd.from(DepartmentEmployee.class);
			em.createQuery(cd).executeUpdate();
		}
		{
			final CriteriaDelete<Employee> cd = cb.createCriteriaDelete(Employee.class);
			cd.from(Employee.class);
			em.createQuery(cd).executeUpdate();
		}
		{
			final CriteriaDelete<Department> cd = cb.createCriteriaDelete(Department.class);
			cd.from(Department.class);
			em.createQuery(cd).executeUpdate();
		}
	}

	// il metodo getReference non può essere invocato direttamente dalla classe di test
	public void testReference(final DtoReferenceWithString<DepartmentDTO> ref, final DepartmentDTO dto) {
		final Department eFromRef = this.departmentManager.getEntityReference(ref);
		assertEquals(dto.getId(), eFromRef.getId());
		assertNull(this.departmentManager.getEntityReference((DtoReferenceWithString<DepartmentDTO>) null));
		TestUtils.assertThrows(Exception.class, true, () -> {
			final Department x = this.departmentManager.getEntityReference(DtoUtils.createReference("ZZ"));
			x.getId();
		});

		final Department eFromId = this.departmentManager.getEntityReference(ref.getId());
		assertEquals(dto.getId(), eFromId.getId());
		assertNull(this.departmentManager.getEntityReference((String) null));
		TestUtils.assertThrows(Exception.class, true, () -> {
			final Department x = this.departmentManager.getEntityReference("ZZ");
			x.getId();
		});
	}
}
