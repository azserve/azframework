package com.azserve.framework.test.ejb.beans.manager;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.ejb.eao.ApplicationEAO;
import com.azserve.azframework.ejb.manager.EntitySecurityManager;
import com.azserve.azframework.ejb.manager.UUIDEntityDtoManager;
import com.azserve.framework.test.ejb.beans.dto.EmployeeDTO;
import com.azserve.framework.test.ejb.beans.entity.Employee;

@Stateless
@LocalBean
public class EmployeeManager extends UUIDEntityDtoManager<Employee, EmployeeDTO, Serializable> {

	@EJB
	private TestEAO eao;

	@Override
	protected @NonNull Class<Employee> getEntityClass() {
		return Employee.class;
	}

	@Override
	protected @NonNull ApplicationEAO getEao() {
		return this.eao;
	}

	@Override
	protected @NonNull Employee newEntity() {
		return new Employee();
	}

	@Override
	protected void fillEntity(final @NonNull EmployeeDTO dto, final @NonNull Employee entity) {
		entity.setBirthDate(dto.getBirthDate());
		entity.setFirstName(dto.getFirstName());
		entity.setGender(dto.getGender());
		entity.setHireDate(dto.getHireDate());
		entity.setLastName(dto.getLastName());

	}

	@Override
	protected @NonNull EmployeeDTO newDto(final @NonNull Employee entity) {
		return new EmployeeDTO(this.toReference(entity));
	}

	@Override
	protected void fillDto(final @NonNull EmployeeDTO dto, final @NonNull Employee entity) {
		dto.setBirthDate(entity.getBirthDate());
		dto.setFirstName(entity.getFirstName());
		dto.setGender(entity.getGender());
		dto.setHireDate(entity.getHireDate());
		dto.setLastName(entity.getLastName());
	}

	@SuppressWarnings("unchecked")
	@Override
	protected @NonNull EntitySecurityManager<Employee, Serializable> createSecurityManager() {
		return EntitySecurityManager.NO_SECURITY;
	}
}
