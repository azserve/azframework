package com.azserve.framework.ejb.service.property;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

import com.azserve.azframework.ejb.manager.property.PropertyUtils;
import com.azserve.azframework.ejb.manager.property.PropertyUtils.ValueConverter;

public class PropertyUtilsTest {

	@Test
	public void testStringConverter() throws Exception {
		final ValueConverter<String> stringConverter = PropertyUtils.stringConverter();
		final String[] testValues = { "", "Hello", "hello", "     ", "\". %" };
		for (final String value : testValues) {
			assertTrue(this.compare(stringConverter, value));
		}
	}

	@Test
	public void testIntegerConverter() throws Exception {
		final ValueConverter<Integer> stringConverter = PropertyUtils.integerConverter();
		final int[] testValues = { 0, 1, -1, Integer.MIN_VALUE, Integer.MAX_VALUE, 255 };
		for (final int value : testValues) {
			assertTrue(this.compare(stringConverter, Integer.valueOf(value)));
		}
	}

	@Test
	public void testBigDecimalConverter() throws Exception {
		final ValueConverter<BigDecimal> stringConverter = PropertyUtils.bigDecimalConverter();
		final String[] testValues = {
				"0",
				"0.00",
				"123",
				"-123",
				"1.23E3",
				"1.23E+3",
				"12.3E+7",
				"12.0",
				"12.3",
				"0.00123",
				"-1.23E-12",
				"1234.5E-4",
				"0E+7",
				"-0"
		};
		for (final String value : testValues) {
			assertTrue(this.compare(stringConverter, new BigDecimal(value)));
		}

		final double[] testValues2 = {
				1,
				1.1,
				100000000000000.0,
				-500000000000.01,
				Math.E,
				Math.PI,
				Double.MAX_VALUE,
				Double.MIN_VALUE
		};
		for (final double value : testValues2) {
			assertTrue(this.compare(stringConverter, new BigDecimal(value)));
		}
	}

	private <T> boolean compare(final ValueConverter<T> valueConverter, final T value) throws Exception {
		final String formattedValue = valueConverter.format(value);
		final T value2 = valueConverter.parse(formattedValue);
		return value.equals(value2);
	}

	private boolean compare(final ValueConverter<BigDecimal> valueConverter, final BigDecimal value) throws Exception {
		final String formattedValue = valueConverter.format(value);
		final BigDecimal value2 = valueConverter.parse(formattedValue);
		return value.compareTo(value2) == 0;
	}
}
