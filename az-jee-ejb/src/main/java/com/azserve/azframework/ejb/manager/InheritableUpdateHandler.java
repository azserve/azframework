package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.dto.Dto;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzInvalidDataException;
import com.azserve.azframework.exception.AzServiceException;

public class InheritableUpdateHandler<E extends IEntity<?>, D extends Dto> implements UpdateHandler<E, D> {

	private final UpdateHandler<E, D> handler;

	public InheritableUpdateHandler(final @NonNull UpdateHandler<E, D> handler) {
		this.handler = handler;
	}

	@Override
	public void validateUpdate(final @NonNull D dto) throws AzInvalidDataException {
		this.handler.validateUpdate(dto);
	}

	@Override
	public void beforeUpdate(final @NonNull D dto, final @NonNull E entity) throws AzServiceException {
		this.handler.beforeUpdate(dto, entity);
	}

	@Override
	public void afterUpdate(final @NonNull D dto, final @NonNull E entity) throws AzServiceException {
		this.handler.afterUpdate(dto, entity);
	}

	@Override
	public void validateAganistEntity(final @NonNull D dto, final @NonNull E entity) throws AzInvalidDataException {
		this.handler.validateAganistEntity(dto, entity);
	}

}
