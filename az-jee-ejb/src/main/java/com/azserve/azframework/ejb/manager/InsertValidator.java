package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.exception.AzInvalidDataException;

public interface InsertValidator<D> {

	void validateInsert(@NonNull D dto) throws AzInvalidDataException;

}
