package com.azserve.azframework.ejb.manager;

import java.util.List;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.dto.DtoUtils;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzInvalidDataException;
import com.azserve.azframework.exception.AzServiceException;
import com.azserve.azframework.validation.ValidationError;

public interface UpdateHandler<E extends IEntity<?>, D>
		extends UpdateValidator<E, D>, BeforeUpdateHandler<E, D>, AfterUpdateHandler<E, D> {

	@SuppressWarnings("rawtypes")
	UpdateHandler NO_OP = new UpdateHandler() {};

	@Override
	default void validateUpdate(final @NonNull D dto) throws AzInvalidDataException {
		final List<ValidationError> errors = DtoUtils.validate(dto);
		if (!errors.isEmpty()) {
			throw new AzInvalidDataException(errors);
		}
	}

	@Override
	default void beforeUpdate(final @NonNull D dto, final @NonNull E entity) throws AzServiceException {}

	@Override
	default void afterUpdate(final @NonNull D dto, final @NonNull E entity) throws AzServiceException {}

	default BeforeInsertHandler<E, D> thenAlsoBeforeUpdate(final BeforeUpdateHandler<E, D> handler) {
		return (dto, entity) -> {
			this.beforeUpdate(dto, entity);
			handler.beforeUpdate(dto, entity);
		};
	}

	default AfterUpdateHandler<E, D> thenAlsoAfterUpdate(final AfterUpdateHandler<E, D> handler) {
		return (dto, entity) -> {
			this.afterUpdate(dto, entity);
			handler.afterUpdate(dto, entity);
		};
	}
}
