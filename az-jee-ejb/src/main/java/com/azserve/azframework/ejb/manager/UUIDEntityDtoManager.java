package com.azserve.azframework.ejb.manager;

import java.io.Serializable;
import java.util.UUID;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.dto.DtoReferenceWithUuid;
import com.azserve.azframework.dto.DtoWithUuid;
import com.azserve.azframework.entity.IEntity;

public abstract class UUIDEntityDtoManager<E extends IEntity<UUID>, D extends DtoWithUuid<D>, P extends Serializable>
		extends EntityDtoManager<E, UUID, D, UUID, DtoReferenceWithUuid<D>, P> {

	@Override
	protected @NonNull DtoReferenceWithUuid<D> newReference(final D dto) {
		return new DtoReferenceWithUuid<>(UUID.randomUUID());
	}

	@Override
	protected @NonNull UUID toId(final DtoReferenceWithUuid<D> reference) {
		return reference.getId();
	}

	@Override
	protected @NonNull DtoReferenceWithUuid<D> toReference(final @NonNull UUID id) {
		return new DtoReferenceWithUuid<>(id);
	}
}
