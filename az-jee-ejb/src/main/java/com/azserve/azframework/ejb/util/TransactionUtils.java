package com.azserve.azframework.ejb.util;

import javax.transaction.UserTransaction;

import com.azserve.azframework.common.ex.CheckedRunnable;
import com.azserve.azframework.common.ex.CheckedSupplier;
import com.azserve.azframework.ejb.eao.ApplicationEAO;

public final class TransactionUtils {

	private TransactionUtils() {
		throw new UnsupportedOperationException();
	}

	public static void runInTransaction(final ApplicationEAO eao, final CheckedRunnable<?> operation) throws Exception {
		runInTransaction(eao.getSessionContext().getUserTransaction(), operation);
	}

	public static <T> T doInTransaction(final ApplicationEAO eao, final CheckedSupplier<T, ?> operation) throws Exception {
		return doInTransaction(eao.getSessionContext().getUserTransaction(), operation);
	}

	public static <T> T doInTransaction(final UserTransaction transaction, final CheckedSupplier<T, ?> operation) throws Exception {
		transaction.begin();
		try {
			final T result = operation.get();
			transaction.commit();
			return result;
		} catch (final Exception e) {
			try {
				transaction.rollback();
			} catch (final Exception er) {
				e.addSuppressed(er);
			}
			throw e instanceof RuntimeException ? new Exception("Exception in transaction", e) : e;
		}
	}

	public static void runInTransaction(final UserTransaction transaction, final CheckedRunnable<?> operation) throws Exception {
		transaction.begin();
		try {
			operation.run();
			transaction.commit();
		} catch (final Exception e) {
			try {
				transaction.rollback();
			} catch (final Exception er) {
				e.addSuppressed(er);
			}
			throw e instanceof RuntimeException ? new Exception("Exception in transaction", e) : e;
		}
	}
}