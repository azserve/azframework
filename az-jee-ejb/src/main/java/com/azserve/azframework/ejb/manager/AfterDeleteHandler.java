package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzServiceException;

public interface AfterDeleteHandler<E extends IEntity<?>> {

	void afterDelete(@NonNull E entity) throws AzServiceException;

}
