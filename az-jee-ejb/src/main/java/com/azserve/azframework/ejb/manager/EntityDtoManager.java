package com.azserve.azframework.ejb.manager;

import java.io.Serializable;
import java.util.Optional;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.dto.DtoReference;
import com.azserve.azframework.dto.DtoWithKey;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzServiceException;

/**
 * Gestore del passaggio dati {@code DTO} - {@code Entity}.
 *
 * @author filippo
 * @since 05/09/2017
 *
 * @param <E> tipo di entity
 * @param <I> tipo di chiave dell'entity
 * @param <D> tipo di {@code DTO}
 * @param <K> tipo di chiave del {@code DTO}
 * @param <R> tipo di reference del {@code DTO}
 * @param <P> parametri per la verifica dei permessi
 */
public abstract class EntityDtoManager<E extends IEntity<I>, I extends Serializable, D extends DtoWithKey<?, K, R>, K extends Serializable, R extends DtoReference<? super D, K>, P extends Serializable>
		extends EntityBeanManager<E, I, D, P> {

	public EntityDtoManager() {}

	// deletes

	/**
	 * Effettua l'eliminazione del dato specificato.
	 *
	 * @param reference riferimento al dato da eliminare.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public void delete(final @NonNull R reference) throws AzServiceException {
		final DeleteHandler<E> handler = this.createDeleteHandler(this.getEao());
		this.delete(reference, handler, handler);
	}

	/**
	 * Effettua l'eliminazione del dato specificato.
	 *
	 * @param reference riferimento al dato da eliminare.
	 * @param beforeDeleteHandler operazioni da eseguire prima dell'eliminazione.
	 * @param afterDeleteHandler operazioni da eseguire dopo l'eliminazione.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public E delete(final @NonNull R reference, final @NonNull BeforeDeleteHandler<E> beforeDeleteHandler, final @NonNull AfterDeleteHandler<E> afterDeleteHandler) throws AzServiceException {
		return super.delete(this.toId(reference), beforeDeleteHandler, afterDeleteHandler);
	}

	// ricerca entity

	/**
	 * Cerca un entity a partire dal {@code DTO}.
	 *
	 * @param dto {@code DTO} con cui cercare.
	 * @return un {@code Optional} contenente l'entity trovata,
	 *         un {@code Optional} vuoto se non viene trovata
	 *         o {@code DTO} è {@code null}.
	 */
	public @NonNull Optional<E> findEntity(final @Nullable D dto) {
		return dto != null ? this.findEntity(dto.getReference()) : Optional.empty();
	}

	/**
	 * Cerca un entity a partire da una reference.
	 *
	 * @param reference reference con cui cercare.
	 * @return un {@code Optional} contenente l'entity trovata,
	 *         un {@code Optional} vuoto se non viene trovata
	 *         o {@code reference} è {@code null}.
	 */
	public @NonNull Optional<E> findEntity(final @Nullable R reference) {
		return reference != null ? this.findEntity(this.toId(reference)) : Optional.empty();
	}

	/**
	 * Cerca un entity a partire da una reference e restituisce
	 * il {@code DTO} corrispondente.
	 *
	 * @param reference reference con cui cercare.
	 * @return il {@code DTO} corrispondente all'entity trovata,
	 *         {@code null} se non viene trovata o {@code reference}
	 *         è {@code null}.
	 */
	public D findDto(final @Nullable R reference) {
		if (reference == null) {
			return null;
		}
		return this.findEntity(reference).<D> map(this::toDto).orElse(null);
	}

	/**
	 * Restituisce un riferimento fittizio ad un'entity per reference.
	 * Non verifica l'entity se esiste
	 *
	 * @param reference reference corrispondente all'id dell'entity.
	 * @return il riferimento alla entity, {@code null} se
	 *         la entity non esiste o {@code id} è {@code null}.
	 */
	public E getEntityReference(final @Nullable R reference) {
		return reference != null ? this.getEntityReference(this.toId(reference)) : null;
	}

	/**
	 * Restituisce la chiave contenuta nella reference specificata.
	 *
	 * @param reference
	 */
	protected abstract @NonNull I toId(final R reference);

	@Override
	protected final @NonNull I toId(final @NonNull D dto) {
		return this.toId(dto.getReference());
	}

	// creazione/compilazione entity

	/**
	 * Imposta la chiave dell'entity a partire dal {@code DTO}.
	 *
	 * @param dto {@code DTO} con reference.
	 * @param entity entity da compilare.
	 */
	@Override
	protected void fillEntityId(final @NonNull D dto, final @NonNull E entity) {
		R reference = dto.getReference();
		if (reference == null) {
			reference = this.newReference(dto);
		}
		entity.setId(this.toId(reference));
	}

	/**
	 * @param dto
	 */
	protected R newReference(final D dto) {
		return null;
	}

	// creazione/compilazione dto

	/**
	 * Restituisce una nuova reference a partire
	 * dall'entity con chiave specificata.
	 *
	 * @param entity entity avente chiave.
	 * @return una nuova reference o {@code null}
	 *         se {@code entity} è {@code null}.
	 */
	public R toReference(final @Nullable E entity) {
		return entity != null ? this.toReference(entity.getId()) : null;
	}

	/**
	 * Restituisce una nuova reference a partire
	 * dalla chiave specificata.
	 *
	 * @param id chiave da inserire nella reference.
	 */
	protected abstract @NonNull R toReference(@NonNull I id);

}
