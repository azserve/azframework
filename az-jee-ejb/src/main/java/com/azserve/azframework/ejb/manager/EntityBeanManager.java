package com.azserve.azframework.ejb.manager;

import java.io.Serializable;
import java.util.Optional;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.ejb.eao.ApplicationEAO;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzServiceException;
import com.azserve.azframework.exception.AzServiceSecurityException;
import com.azserve.azframework.query.Expressions;
import com.azserve.azframework.query.ISingleQuery;
import com.azserve.azframework.query.Queries;
import com.azserve.azframework.query.criteria.IPredicate;
import com.azserve.azframework.query.criteria.IRoot;

/**
 * Gestore del passaggio dati {@code DTO} - {@code Entity}.
 *
 * @author filippo
 * @since 05/09/2017
 *
 * @param <E> tipo di entity
 * @param <I> tipo di chiave dell'entity
 * @param <D> tipo di dto
 * @param <P> parametri per la verifica dei permessi
 */
public abstract class EntityBeanManager<E extends IEntity<I>, I extends Serializable, D, P extends Serializable> {

	private EntitySecurityManager<E, P> securityManager;

	public EntityBeanManager() {}

	// inserts

	/**
	 * Effettua l'inserimento del {@code DTO} specificato.
	 *
	 * @param dto {@code DTO} con i dati da inserire.
	 * @return un nuovo oggetto {@code DTO} compilato dopo l'inserimento.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public @NonNull D insert(final @NonNull D dto) throws AzServiceException {
		final InsertHandler<E, D> handler = this.createInsertHandler(this.getEao());
		return this.toDto(this.insert(dto, handler, handler, handler));
	}

	/**
	 * Effettua l'inserimento del {@code DTO} specificato.
	 *
	 * @param dto {@code DTO} con i dati da inserire.
	 * @param validator validatore del dato.
	 * @param beforeInsertHandler operazioni da eseguire prima dell'inserimento.
	 * @param afterInsertHandler operazioni da eseguire dopo l'inserimento.
	 * @return un nuovo oggetto {@code DTO} compilato dopo l'inserimento.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public @NonNull E insert(final @NonNull D dto, final @NonNull InsertValidator<D> validator, final @NonNull BeforeInsertHandler<E, D> beforeInsertHandler, final @NonNull AfterInsertHandler<E, D> afterInsertHandler) throws AzServiceException {
		validator.validateInsert(dto);
		final E entity = this.toEntity(dto);
		if (!this.getSecurityManager().hasInsertAccess(entity)) {
			throw new AzServiceSecurityException();
		}
		beforeInsertHandler.beforeInsert(dto, entity);
		this.performInsert(entity);
		afterInsertHandler.afterInsert(dto, entity);
		return entity;
	}

	/**
	 * Effettua l'inserimento dell'entity specificata
	 * effettuando i controlli di sicurezza.
	 *
	 * @param entity entity da inserire.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public /* final */ void insert(final @NonNull E entity) throws AzServiceException {
		if (!this.getSecurityManager().hasInsertAccess(entity)) {
			throw new AzServiceSecurityException();
		}
		this.performInsert(entity);
	}

	/**
	 * Effettua l'inserimento finale dell'entity.
	 *
	 * @param entity entity da inserire.
	 * @throws AzServiceException in caso di errori noti.
	 */
	protected void performInsert(final @NonNull E entity) throws AzServiceException {
		this.getEao().insert(entity);
	}

	// updates

	/**
	 * Effettua l'aggiornamento del {@code DTO} specificato.
	 *
	 * @param dto {@code DTO} con i dati da aggiornare.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public void update(final @NonNull D dto) throws AzServiceException {
		final UpdateHandler<E, D> handler = this.createUpdateHandler(this.getEao());
		this.update(dto, handler, handler, handler);
	}

	/**
	 * Effettua l'aggiornamento del {@code DTO} specificato
	 * e lo restituisce aggiornato.
	 *
	 * @param dto {@code DTO} con i dati da aggiornare.
	 * @return il {@code DTO} aggiornato oppure {@code null} se
	 *         il dato non è stato trovato.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public D updateAndGet(final @NonNull D dto) throws AzServiceException {
		final UpdateHandler<E, D> handler = this.createUpdateHandler(this.getEao());
		final E updatedEntity = this.update(dto, handler, handler, handler);
		return updatedEntity != null ? this.toDto(updatedEntity) : null;
	}

	/**
	 * Effettua l'aggiornamento del {@code DTO} specificato.
	 *
	 * @param dto {@code DTO} con i dati da aggiornare.
	 * @param validator validazioni da effettuare tra il {@code DTO}
	 *            e l'entity ancora da aggiornare.
	 * @param beforeUpdateHandler operazioni da eseguire prima dell'aggiornamento.
	 * @param afterUpdateHandler operazioni da eseguire dopo l'aggiornamento.
	 * @return l'entity aggiornata oppure {@code null} se il dato non è stato trovato.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public E update(final @NonNull D dto, final @NonNull UpdateValidator<E, D> validator, final @NonNull BeforeUpdateHandler<E, D> beforeUpdateHandler, final @NonNull AfterUpdateHandler<E, D> afterUpdateHandler) throws AzServiceException {
		validator.validateUpdate(dto);
		final E entity = this.findEntity(this.toId(dto)).orElse(null);
		if (entity == null) {
			beforeUpdateHandler.updateEntityNotFound();
			return null;
		}
		return this.updateInternal(dto, entity, validator, beforeUpdateHandler, afterUpdateHandler);
	}

	/**
	 * Effettua l'aggiornamento di una specifica entity utilizzando il {@code DTO} specificato.
	 *
	 * @param dto {@code DTO} con i dati aggiornati.
	 * @param entity entity da aggiornare.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public void update(final @NonNull D dto, final @NonNull E entity) throws AzServiceException {
		final UpdateHandler<E, D> handler = this.createUpdateHandler(this.getEao());
		this.update(dto, entity, handler, handler, handler);
	}

	/**
	 * Effettua l'aggiornamento di una specifica entity utilizzando il {@code DTO} specificato.
	 *
	 * @param dto {@code DTO} con i dati aggiornati.
	 * @param entity entity da aggiornare.
	 * @param validator validazioni da effettuare tra il {@code DTO}
	 *            e l'entity ancora da aggiornare.
	 * @param beforeUpdateHandler operazioni da eseguire prima dell'aggiornamento.
	 * @param afterUpdateHandler operazioni da eseguire dopo l'aggiornamento.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public @NonNull E update(final @NonNull D dto, final @NonNull E entity, final @NonNull UpdateValidator<E, D> validator, final @NonNull BeforeUpdateHandler<E, D> beforeUpdateHandler, final @NonNull AfterUpdateHandler<E, D> afterUpdateHandler) throws AzServiceException {
		validator.validateUpdate(dto);
		return this.updateInternal(dto, entity, validator, beforeUpdateHandler, afterUpdateHandler);
	}

	private @NonNull E updateInternal(final @NonNull D dto, final @NonNull E entity, final @NonNull UpdateValidator<E, D> validator, final @NonNull BeforeUpdateHandler<E, D> beforeUpdateHandler, final @NonNull AfterUpdateHandler<E, D> afterUpdateHandler) throws AzServiceException {
		if (!this.getSecurityManager().hasUpdateAccess(entity)) {
			throw new AzServiceSecurityException();
		}
		validator.checkConcurrency(dto, entity);
		validator.validateAganistEntity(dto, entity);
		this.fillEntity(dto, entity);
		beforeUpdateHandler.beforeUpdate(dto, entity);
		if (!this.getSecurityManager().hasUpdateAccess(entity)) {
			throw new AzServiceSecurityException();
		}
		this.performUpdate(entity);
		afterUpdateHandler.afterUpdate(dto, entity);
		return entity;
	}

	/**
	 * Effettua l'aggiornamento dell'entity specificata
	 * effettuando i controlli di sicurezza.
	 *
	 * @param entity entity da aggiornare.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public /* final */ void update(final @NonNull E entity) throws AzServiceException {
		if (!this.getSecurityManager().hasUpdateAccess(entity)) {
			throw new AzServiceSecurityException();
		}
		this.performUpdate(entity);
	}

	/**
	 * Effettua l'aggiornamento finale dell'entity.
	 *
	 * @param entity entity da aggiornare.
	 * @throws AzServiceException in caso di errori noti.
	 */
	protected void performUpdate(final @NonNull E entity) throws AzServiceException {
		this.getEao().update(entity);
	}

	// deletes

	/**
	 * Effettua l'eliminazione del dato specificato.
	 *
	 * @param id riferimento al dato da eliminare.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public void delete(final @NonNull I id) throws AzServiceException {
		final DeleteHandler<E> handler = this.createDeleteHandler(this.getEao());
		this.delete(id, handler, handler);
	}

	/**
	 * Effettua l'eliminazione del dato specificato.
	 *
	 * @param id riferimento al dato da eliminare.
	 * @param beforeDeleteHandler operazioni da eseguire prima dell'eliminazione.
	 * @param afterDeleteHandler operazioni da eseguire dopo l'eliminazione.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public E delete(final @NonNull I id, final @NonNull BeforeDeleteHandler<E> beforeDeleteHandler, final @NonNull AfterDeleteHandler<E> afterDeleteHandler) throws AzServiceException {
		final Optional<E> optEntity = this.findEntity(id);
		if (!optEntity.isPresent()) {
			beforeDeleteHandler.deleteEntityNotFound();
			return null;
		}
		final E entity = optEntity.get();
		this.delete(entity, beforeDeleteHandler, afterDeleteHandler);
		return entity;
	}

	/**
	 * Effettua l'eliminazione dell'entity specificata.
	 *
	 * @param entity entity da eliminare.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public void delete(final @NonNull E entity, final @NonNull BeforeDeleteHandler<E> beforeDeleteHandler, final @NonNull AfterDeleteHandler<E> afterDeleteHandler) throws AzServiceException {
		if (!this.getSecurityManager().hasDeleteAccess(entity)) {
			throw new AzServiceSecurityException();
		}
		beforeDeleteHandler.beforeDelete(entity);
		this.performDelete(entity);
		afterDeleteHandler.afterDelete(entity);
	}

	/**
	 * Effettua l'eliminazione dell'entity specificata.
	 *
	 * @param entity entity da eliminare.
	 * @throws AzServiceException in caso di errori noti.
	 */
	public /* final */ void delete(final @NonNull E entity) throws AzServiceException {
		final DeleteHandler<E> handler = this.createDeleteHandler(this.getEao());
		this.delete(entity, handler, handler);
	}

	/**
	 * Effettua l'eliminazione finale dell'entity.
	 *
	 * @param entity entity da eliminare.
	 * @throws AzServiceException in caso di errori noti.
	 */
	protected void performDelete(final @NonNull E entity) throws AzServiceException {
		this.getEao().delete(entity);
	}

	// ricerca entity

	/**
	 * Cerca un entity a partire dal suo id.
	 *
	 * @param id id con cui cercare.
	 * @return un {@code Optional} contenente l'entity trovata,
	 *         un {@code Optional} vuoto se non viene trovata
	 *         o {@code id} è {@code null}.
	 * @throws SecurityException se non si hanno i permessi
	 *             necessari alla lettura della entity.
	 */
	public @NonNull Optional<E> findEntity(final @Nullable I id) {
		if (id == null) {
			return Optional.empty();
		}
		final E entity = this.getEao().find(this.getEntityClass(), id);
		if (entity != null && !this.getSecurityManager().hasReadAccess(entity)) {
			throw new SecurityException();
		}
		return Optional.ofNullable(entity);
	}

	/**
	 * Cerca un entity a partire dall'id e restituisce
	 * il {@code DTO} corrispondente.
	 *
	 * @param id id con cui cercare.
	 * @return il {@code DTO} corrispondente all'entity trovata,
	 *         {@code null} se non viene trovata o {@code id}
	 *         è {@code null}.
	 */
	public D findDto(final @Nullable I id) {
		if (id == null) {
			return null;
		}
		return this.findEntity(id).map(this::toDto).orElse(null);
	}

	/**
	 * Restituisce un riferimento fittizio ad un'entity per id.
	 *
	 * @param id chiave dell'entity.
	 * @return il riferimento alla entity, {@code null} se
	 *         la entity non esiste o {@code id} è {@code null}.
	 */
	public E getEntityReference(final @Nullable I id) {
		return id != null ? this.getEao().getReference(this.getEntityClass(), id) : null;
	}

	/**
	 * Restituisce la chiave del dto specificato
	 *
	 * @param dto
	 */
	protected abstract I toId(final @NonNull D dto);

	// creazione/compilazione entity

	/**
	 * Crea e compila una nuova entity a partire
	 * dai dati del {@code DTO} specificato.
	 *
	 * @param dto {@code DTO} sorgente.
	 * @return la nuova entity compilata.
	 */
	public @NonNull E toEntity(final @NonNull D dto) {
		final E entity = this.newEntity();
		this.fillEntityId(dto, entity);
		this.fillEntity(dto, entity);
		return entity;
	}

	/**
	 * Instanzia una nuova entity vuota.
	 * Di default utilizza la reflection per
	 * invocare il costruttore senza parametri.
	 */
	protected @NonNull E newEntity() {
		try {
			return this.getEntityClass().getConstructor().newInstance();
		} catch (final ReflectiveOperationException ex) {
			throw new RuntimeException("Cannot find empty constructor for entity class: " + this.getEntityClass(), ex);
		}
	}

	/**
	 * Imposta la chiave dell'entity a partire dal {@code DTO}.
	 *
	 * @param dto
	 * @param entity entity da compilare.
	 */
	protected void fillEntityId(final @NonNull D dto, final @NonNull E entity) {
		entity.setId(this.toId(dto));
	}

	/**
	 * Compila i dati dell'entity specificata a partire
	 * dai dati del {@code DTO} specificato.
	 *
	 * @param dto {@code DTO} con i dati.
	 * @param entity entity da compilare.
	 */
	protected abstract void fillEntity(final @NonNull D dto, final @NonNull E entity);

	// creazione/compilazione dto

	/**
	 * Restituisce un nuovo {@code DTO} compilato
	 * con la chiave e i dati dell'entity specificata.
	 *
	 * @param entity entity contenente i dati.
	 */
	public @NonNull D toDto(final @NonNull E entity) {
		final D dto = this.newDto(entity);
		ManagerInternals.fillDtoVersion(dto, entity);
		this.fillDto(dto, entity);
		return dto;
	}

	/**
	 * Instanzia un nuovo {@code DTO} a partire dall'entity
	 *
	 * @param entity entity con i dati.
	 */
	protected abstract @NonNull D newDto(@NonNull E entity);

	/**
	 * Compila i dati del {@code DTO} specificato a partire
	 * dai dati dell'entity specificata.
	 *
	 * @param dto {@code DTO} da compilare.
	 * @param entity entity con i dati.
	 */
	protected abstract void fillDto(@NonNull D dto, @NonNull E entity);

	// query

	public @NonNull ISingleQuery<E, E> query(final P params) {
		final IRoot<E> root = Expressions.from(this.getEntityClass());
		return this.query(root, params);
	}

	public @NonNull ISingleQuery<E, E> query(final @NonNull IRoot<E> root, final P params) {
		final IPredicate queryFilter = this.getSecurityManager().prepareQueryFilter(root, params);
		final ISingleQuery<E, E> query = Queries.create(this.getEao().getEntityManager())
				.from(root).select(this.getEntityClass(), root);
		if (queryFilter != null) {
			return query.where(queryFilter);
		}
		return query;
	}

	// utilità

	protected abstract @NonNull EntitySecurityManager<E, P> createSecurityManager();

	protected final @NonNull EntitySecurityManager<E, P> getSecurityManager() {
		if (this.securityManager == null) {
			return this.securityManager = this.createSecurityManager();
		}
		return this.securityManager;
	}

	/**
	 * Restituisce un nuovo gestore dell'operazione di inserimento.
	 * <br/>
	 * Di default se questo manager implementa l'interfaccia {@code InsertHandler}
	 * viene restituito il manager stesso, altrimenti una implementazione NOP.
	 *
	 * @param eao
	 * @throws ClassCastException se questo manager implementa l'interfaccia
	 *             {@code InsertHandler} di un tipo non corrispondente a quello
	 *             gestito dal manager stesso.
	 */
	protected @NonNull @SuppressWarnings("unchecked") InsertHandler<E, D> createInsertHandler(final @NonNull ApplicationEAO eao) {
		if (this instanceof InsertHandler) {
			return (InsertHandler<E, D>) this;
		}
		return InsertHandler.NO_OP;
	}

	/**
	 * Restituisce un nuovo gestore dell'operazione di aggiornamento.
	 * <br/>
	 * Di default se questo manager implementa l'interfaccia {@code UpdateHandler}
	 * viene restituito il manager stesso, altrimenti una implementazione NOP.
	 *
	 * @param eao
	 * @throws ClassCastException se questo manager implementa l'interfaccia
	 *             {@code UpdateHandler} di un tipo non corrispondente a quello
	 *             gestito dal manager stesso.
	 */
	protected @NonNull @SuppressWarnings("unchecked") UpdateHandler<E, D> createUpdateHandler(final @NonNull ApplicationEAO eao) {
		if (this instanceof UpdateHandler) {
			return (UpdateHandler<E, D>) this;
		}
		return UpdateHandler.NO_OP;
	}

	/**
	 * Restituisce un nuovo gestore dell'operazione di eliminazione.
	 * <br/>
	 * Di default se questo manager implementa l'interfaccia {@code DeleteHandler}
	 * viene restituito il manager stesso, altrimenti una implementazione NOP.
	 *
	 * @param eao
	 * @throws ClassCastException se questo manager implementa l'interfaccia
	 *             {@code DeleteHandler} di un tipo non corrispondente a quello
	 *             gestito dal manager stesso.
	 */
	protected @NonNull @SuppressWarnings("unchecked") DeleteHandler<E> createDeleteHandler(final @NonNull ApplicationEAO eao) {
		if (this instanceof DeleteHandler) {
			return (DeleteHandler<E>) this;
		}
		return DeleteHandler.NO_OP;
	}

	protected abstract @NonNull Class<E> getEntityClass();

	protected abstract @NonNull ApplicationEAO getEao();
}
