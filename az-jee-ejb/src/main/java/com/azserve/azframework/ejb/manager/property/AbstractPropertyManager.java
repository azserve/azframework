package com.azserve.azframework.ejb.manager.property;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.HasValue;
import com.azserve.azframework.common.lang.BooleanUtils;
import com.azserve.azframework.ejb.eao.ApplicationEAO;
import com.azserve.azframework.ejb.manager.property.PropertyUtils.EnumValueConverter;
import com.azserve.azframework.ejb.manager.property.PropertyUtils.ValueConverter;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.interfaces.service.property.IPropertyKey;

public abstract class AbstractPropertyManager<P extends HasValue<String> & IEntity<PK>, PK extends Serializable, K extends Enum<K> & IPropertyKey, T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPropertyManager.class);

	public String getStringValue(final K key, final T t) {
		return this.getValue(key, t, PropertyUtils.stringConverter());
	}

	public boolean getBooleanValue(final K key, final T t) {
		return BooleanUtils.isTrue(this.getValue(key, t, PropertyUtils.booleanConverter()));
	}

	public Integer getIntegerValue(final K key, final T t) {
		return this.getValue(key, t, PropertyUtils.integerConverter());
	}

	public BigDecimal getBigDecimalValue(final K key, final T t) {
		return this.getValue(key, t, PropertyUtils.bigDecimalConverter());
	}

	public LocalDate getDateValue(final K key, final T t) {
		return this.getValue(key, t, PropertyUtils.localDateConverter());
	}

	public LocalDateTime getDateTimeValue(final K key, final T t) {
		return this.getValue(key, t, PropertyUtils.localDateTimeConverter());
	}

	public LocalTime getTimeValue(final K key, final T t) {
		return this.getValue(key, t, PropertyUtils.localTimeConverter());
	}

	public <E extends Enum<E>> E getEnumValue(final K key, final T t, final Class<E> enumClass) {
		return this.getValue(key, t, PropertyUtils.enumConverter(enumClass));
	}

	@SuppressWarnings("unchecked")
	public <S extends Serializable> S getSerializableValue(final K key, final T t) {
		return (S) this.getValue(key, t, PropertyUtils.serializableConverter());
	}

	public void setStringValue(final K key, final T t, final String value) {
		this.setValue(key, t, PropertyUtils.stringConverter(), value);
	}

	public void setBooleanValue(final K key, final T t, final boolean value) {
		this.setValue(key, t, PropertyUtils.booleanConverter(), Boolean.valueOf(value));
	}

	public void setIntegerValue(final K key, final T t, final Integer value) {
		this.setValue(key, t, PropertyUtils.integerConverter(), value);
	}

	public void setBigDecimalValue(final K key, final T t, final BigDecimal value) {
		this.setValue(key, t, PropertyUtils.bigDecimalConverter(), value);
	}

	public void setDateValue(final K key, final T t, final LocalDate value) {
		this.setValue(key, t, PropertyUtils.localDateConverter(), value);
	}

	public void setDateTimeValue(final K key, final T t, final LocalDateTime value) {
		this.setValue(key, t, PropertyUtils.localDateTimeConverter(), value);
	}

	public void setTimeValue(final K key, final T t, final LocalTime value) {
		this.setValue(key, t, PropertyUtils.localTimeConverter(), value);
	}

	public <E extends Enum<E>> void setEnumValue(final K key, final T t, final Class<E> enumClass, final E value) {
		final EnumValueConverter<E> enumConverter = PropertyUtils.enumConverter(enumClass);
		this.setValue(key, t, enumConverter, value);
	}

	public void setSerializedValue(final K key, final T t, final Serializable value) {
		this.setValue(key, t, PropertyUtils.serializableConverter(), value);
	}

	protected void validateClass(final K key, final Object value) {
		if (value == null) {
			return;
		}
		final Class<? extends Object> valueClass = value.getClass();
		if (!key.getValueClass().isAssignableFrom(valueClass)) {
			throw new IllegalArgumentException(key.name() + ".getValueClass() = " + key.getValueClass().getSimpleName() + " not assignable from " + valueClass.getSimpleName());
		}
	}

	protected P findByKey(final K key, final T t) {
		if (!this.isReadAllowed(t)) {
			LOGGER.error("Utente {} non abilitato per {}", this.getUsername(), t);
			assert false;
			return null;
		}
		final PK pk = this.createKey(key, t);
		return this.getEao().find(this.getPropertyClass(), pk);
	}

	protected <X> Object getPropertyValue(final K key, final T t, final ValueConverter<X> stringConverter) {
		// unificare questo metodo con getPropertyValue(final String key, final ValueConverter<T> stringConverter)
		// creerebbe qualche problema nel caso la proprietà abbia valore null e default value != null
		final P proprieta = this.findByKey(key, t);
		try {
			if (proprieta == null) {
				return key.getDefaultValue();
			}
			final String value = proprieta.getValue();
			return value == null ? null : stringConverter.parse(value);
		} catch (final Exception ex) {
			LOGGER.error("getPropertyValue", ex);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	protected <X> X getValue(final K key, final T t, final ValueConverter<X> stringConverter) {
		try {
			final Object value = this.getPropertyValue(key, t, stringConverter);
			this.validateClass(key, value);
			return (X) value;
		} catch (final ClassCastException ex) {
			LOGGER.error("getValue", ex);
			assert false;
			return null;

		} catch (final Exception ex) {
			LOGGER.error("getValue", ex);
			return null;
		}
	}

	private <X> void setPropertyValue(final K key, final T t, final ValueConverter<X> stringConverter, final X value) {
		final P proprieta = this.findByKey(key, t);
		if (!this.isWriteAllowed(t)) {
			LOGGER.error("Utente {} non abilitato per {}", this.getUsername(), t);
			assert false;
			throw new RuntimeException("Utente non abilitato");
		}
		if (proprieta == null) {
			this.getEao().insert(this.createProperty(key, t, value == null ? null : stringConverter.format(value)));
		} else {
			this.setValue(proprieta, value == null ? null : stringConverter.format(value));
			this.getEao().update(proprieta);
		}
	}

	private <X> void setValue(final K key, final T t, final ValueConverter<X> stringConverter, final X value) {
		this.validateClass(key, value);
		this.setPropertyValue(key, t, stringConverter, value);
	}

	private String getUsername() {
		return this.getEao().getSessionContext().getCallerPrincipal().getName();
	}

	protected abstract PK createKey(K key, T t);

	protected abstract Class<P> getPropertyClass();

	protected abstract boolean isReadAllowed(final T r);

	protected abstract boolean isWriteAllowed(final T r);

	protected abstract ApplicationEAO getEao();

	protected abstract P createProperty(K key, T t, String value);

	protected abstract void setValue(P property, String value);
}
