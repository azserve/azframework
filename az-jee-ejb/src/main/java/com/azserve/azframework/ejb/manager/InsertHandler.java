package com.azserve.azframework.ejb.manager;

import java.util.List;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.dto.DtoUtils;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzInvalidDataException;
import com.azserve.azframework.exception.AzServiceException;
import com.azserve.azframework.validation.ValidationError;

public interface InsertHandler<E extends IEntity<?>, D>
		extends InsertValidator<D>, BeforeInsertHandler<E, D>, AfterInsertHandler<E, D> {

	@SuppressWarnings("rawtypes")
	InsertHandler NO_OP = new InsertHandler() {};

	@Override
	default void validateInsert(final @NonNull D dto) throws AzInvalidDataException {
		final List<ValidationError> errors = DtoUtils.validate(dto);
		if (!errors.isEmpty()) {
			throw new AzInvalidDataException(errors);
		}
	}

	@Override
	default void beforeInsert(final @NonNull D dto, final @NonNull E entity) throws AzServiceException {}

	@Override
	default void afterInsert(final @NonNull D dto, final @NonNull E entity) throws AzServiceException {}

	default BeforeInsertHandler<E, D> thenAlsoBeforeInsert(final BeforeInsertHandler<E, D> handler) {
		return (dto, entity) -> {
			this.beforeInsert(dto, entity);
			handler.beforeInsert(dto, entity);
		};
	}

	default AfterInsertHandler<E, D> thenAlsoAfterInsert(final AfterInsertHandler<E, D> handler) {
		return (dto, entity) -> {
			this.afterInsert(dto, entity);
			handler.afterInsert(dto, entity);
		};
	}
}
