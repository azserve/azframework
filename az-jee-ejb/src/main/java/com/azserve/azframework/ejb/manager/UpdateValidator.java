package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.dto.HasVersion;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzInvalidDataException;
import com.azserve.azframework.exception.AzServiceConcurrencyException;

public interface UpdateValidator<E extends IEntity<?>, D> {

	void validateUpdate(@NonNull D dto) throws AzInvalidDataException;

	/**
	 * Checks the version of dto and entity comparing them.
	 * If their version does not match a {@link AzServiceConcurrencyException} is thrown.
	 *
	 * <p>By default this method does the validation only if either dto and entity are
	 * {@link HasVersion} instances and their {@linkplain HasVersion#getVersion()}
	 * method returns a non-null value.</p>
	 *
	 * @param dto
	 * @param entity
	 * @throws AzServiceConcurrencyException if the version does not match.
	 * @throws IncompatibileVersionTypeException if the return types of {@linkplain HasVersion#getVersion()} are incompatible.
	 */
	default void checkConcurrency(final @NonNull D dto, final @NonNull E entity) throws AzServiceConcurrencyException {
		ManagerInternals.checkVersion(dto, entity);
	}

	/**
	 * @param dto
	 * @param entity
	 * @throws AzInvalidDataException
	 */
	default void validateAganistEntity(final @NonNull D dto, final @NonNull E entity) throws AzInvalidDataException {
		// NO-OP by default
	}

	/**
	 * Returns a new validator based on this instance which does not check
	 * for concurrency problems.
	 */
	default UpdateValidator<E, D> withoutConcurrencyCheck() {
		return new UpdateValidator<E, D>() {

			@Override
			public void validateUpdate(final @NonNull D dto) throws AzInvalidDataException {
				UpdateValidator.this.validateUpdate(dto);
			}

			@Override
			public void checkConcurrency(final @NonNull D dto, final @NonNull E entity) throws AzServiceConcurrencyException {
				// prevent validation
			}

			@Override
			public void validateAganistEntity(final @NonNull D dto, final @NonNull E entity) throws AzInvalidDataException {
				UpdateValidator.this.validateAganistEntity(dto, entity);
			}
		};
	}
}
