package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.dto.HasVersion;
import com.azserve.azframework.exception.AzServiceConcurrencyException;

@SuppressWarnings({ "rawtypes", "unchecked" })
class ManagerInternals {

	static void fillDtoVersion(final Object dto, final Object entity) {
		if (dto instanceof HasVersion && entity instanceof HasVersion) {
			final Comparable entityVersion = ((HasVersion) entity).getVersion();
			try {
				((HasVersion) dto).setVersion(entityVersion);
			} catch (final ClassCastException ex) {
				throw new IncompatibileVersionTypeException(entityVersion, ex);
			}
		}
	}

	static void checkVersion(final Object dto, final Object entity) throws AzServiceConcurrencyException {
		if (dto instanceof HasVersion && entity instanceof HasVersion) {
			final Comparable dtoVersion = ((HasVersion) dto).getVersion();
			final Comparable entityVersion = ((HasVersion) entity).getVersion();
			if (dtoVersion != null && entityVersion != null) {
				final int compareResult;
				try {
					compareResult = dtoVersion.compareTo(entityVersion);
				} catch (final ClassCastException ex) {
					throw new IncompatibileVersionTypeException(dtoVersion, entityVersion, ex);
				}
				if (compareResult != 0) {
					throw new AzServiceConcurrencyException();
				}
			}
		}
	}

	private ManagerInternals() {}

}
