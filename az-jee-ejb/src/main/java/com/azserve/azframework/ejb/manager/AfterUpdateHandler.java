package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzServiceException;

public interface AfterUpdateHandler<E extends IEntity<?>, D> {

	void afterUpdate(@NonNull D dto, @NonNull E entity) throws AzServiceException;

}
