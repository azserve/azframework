package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzServiceException;

public interface BeforeUpdateHandler<E extends IEntity<?>, D> {

	/**
	 * Metodo chiamato quando l'entity da aggiornare non viene trovata.
	 * Di default non fa nulla ma è possibile sollevare un'eccezione.
	 *
	 * @throws AzServiceException
	 */
	default void updateEntityNotFound() throws AzServiceException {}

	void beforeUpdate(@NonNull D dto, @NonNull E entity) throws AzServiceException;

}
