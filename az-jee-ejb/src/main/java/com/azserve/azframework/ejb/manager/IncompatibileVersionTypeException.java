package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.dto.HasVersion;

/**
 * Exception thrown in case of incompatibile {@linkplain HasVersion} types.
 */
public class IncompatibileVersionTypeException extends RuntimeException {

	private static final long serialVersionUID = 3713962579821576341L;

	public IncompatibileVersionTypeException(final Comparable<?> entityVersion, final Throwable cause) {
		super("Incompatibile type of HasVersion.getVersion() : " + entityVersion.getClass(), cause);
	}

	public IncompatibileVersionTypeException(final Comparable<?> dtoVersion, final Comparable<?> entityVersion, final Throwable cause) {
		super("Incompatibile types of HasVersion.getVersion() : "
				+ dtoVersion.getClass() + " vs " + entityVersion.getClass(), cause);
	}
}
