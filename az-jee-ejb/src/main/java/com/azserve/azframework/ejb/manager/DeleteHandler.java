package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzServiceException;

public interface DeleteHandler<E extends IEntity<?>>
		extends BeforeDeleteHandler<E>, AfterDeleteHandler<E> {

	@SuppressWarnings("rawtypes")
	DeleteHandler NO_OP = new DeleteHandler() {};

	@Override
	default void beforeDelete(final @NonNull E entity) throws AzServiceException {}

	@Override
	default void afterDelete(final @NonNull E entity) throws AzServiceException {}

	default BeforeDeleteHandler<E> thenAlsoBeforeDelete(final BeforeDeleteHandler<E> handler) {
		return entity -> {
			this.beforeDelete(entity);
			handler.beforeDelete(entity);
		};
	}

	default AfterDeleteHandler<E> thenAlsoAfterDelete(final AfterDeleteHandler<E> handler) {
		return entity -> {
			this.afterDelete(entity);
			handler.afterDelete(entity);
		};
	}
}
