package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzServiceException;

public interface AfterInsertHandler<E extends IEntity<?>, D> {

	void afterInsert(@NonNull D dto, @NonNull E entity) throws AzServiceException;

}
