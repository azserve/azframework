package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzServiceException;

public class InheritableDeleteHandler<E extends IEntity<?>> implements DeleteHandler<E> {

	private final DeleteHandler<E> handler;

	public InheritableDeleteHandler(final @NonNull DeleteHandler<E> handler) {
		this.handler = handler;
	}

	@Override
	public void beforeDelete(final @NonNull E entity) throws AzServiceException {
		this.handler.beforeDelete(entity);
	}

	@Override
	public void afterDelete(final @NonNull E entity) throws AzServiceException {
		this.handler.afterDelete(entity);
	}
}
