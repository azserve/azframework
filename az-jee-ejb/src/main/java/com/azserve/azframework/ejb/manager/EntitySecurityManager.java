package com.azserve.azframework.ejb.manager;

import java.io.Serializable;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.query.criteria.IPredicate;
import com.azserve.azframework.query.criteria.IRoot;

public interface EntitySecurityManager<E extends IEntity<?>, P extends Serializable> {

	/**
	 * Verifica se è possibile la lettura dell'entity specificata.
	 *
	 * @param entity entity da verificare.
	 */
	boolean hasReadAccess(@NonNull E entity);

	/**
	 * Verifica se è possibile la scrittura (inserimento,modifica,cancellazione)
	 * dell'entity specificata.
	 * <p>
	 * È possibile verificare le singole operazioni in scrittura
	 * reimplementando i metodi:
	 * <br>
	 * {@link #hasInsertAccess(IEntity)},
	 * {@link #hasUpdateAccess(IEntity)},
	 * {@link #hasDeleteAccess(IEntity)}
	 * <br>
	 * In tal caso il valore restituito da questo metodo
	 * sarà insignificante per la specifica operazione.
	 *
	 * @param entity entity da verificare.
	 */
	boolean hasWriteAccess(@NonNull E entity);

	/**
	 * Verifica se è possibile l'inserimento dell'entity specificata.
	 *
	 * @param entity entity da verificare.
	 */
	default boolean hasInsertAccess(final @NonNull E entity) {
		return this.hasWriteAccess(entity);
	}

	/**
	 * Verifica se è possibile l'aggiornamento dell'entity specificata.
	 *
	 * @param entity entity da verificare.
	 */
	default boolean hasUpdateAccess(final @NonNull E entity) {
		return this.hasWriteAccess(entity);
	}

	/**
	 * Verifica se è possibile la cancellazione dell'entity specificata.
	 *
	 * @param entity entity da verificare.
	 */
	default boolean hasDeleteAccess(final @NonNull E entity) {
		return this.hasWriteAccess(entity);
	}

	/**
	 * Prepara il filtro da utilizzare per le query.
	 * In caso di permessi insufficente per utilizzare
	 * il parametro specificato lancia una eccezione
	 * {@code link SecurityException}.
	 *
	 * @param params parametri di filtro.
	 * @return filtro a partire dal parametro
	 *         da applicare alle query.
	 */
	@Nullable
	IPredicate prepareQueryFilter(@NonNull IRoot<E> root, @Nullable P params);

	static <E1 extends IEntity<?>, P1 extends Serializable> EntitySecurityManager<E1, P1> functional(
			final @NonNull Predicate<E1> readAccessTest,
			final @NonNull Predicate<E1> writeAccessTest,
			final @NonNull BiFunction<IRoot<E1>, P1, IPredicate> queryFilterFactory) {
		return new EntitySecurityManager<E1, P1>() {

			@Override
			public boolean hasReadAccess(final @NonNull E1 entity) {
				return readAccessTest.test(entity);
			}

			@Override
			public boolean hasWriteAccess(final @NonNull E1 entity) {
				return writeAccessTest.test(entity);
			}

			@Override
			public @Nullable IPredicate prepareQueryFilter(final @NonNull IRoot<E1> root, final @Nullable P1 params) {
				return queryFilterFactory.apply(root, params);
			}
		};
	}

	static <E1 extends IEntity<?>, P1 extends Serializable> EntitySecurityManager<E1, P1> functional(
			final @NonNull Predicate<E1> readAccessTest,
			final @NonNull Predicate<E1> insertAccessTest,
			final @NonNull Predicate<E1> updateAccessTest,
			final @NonNull Predicate<E1> deleteAccessTest,
			final @NonNull BiFunction<IRoot<E1>, P1, IPredicate> queryFilterFactory) {
		return new EntitySecurityManager<E1, P1>() {

			@Override
			public boolean hasReadAccess(final @NonNull E1 entity) {
				return readAccessTest.test(entity);
			}

			@Override
			public boolean hasInsertAccess(@NonNull final E1 entity) {
				return insertAccessTest.test(entity);
			}

			@Override
			public boolean hasUpdateAccess(@NonNull final E1 entity) {
				return updateAccessTest.test(entity);
			}

			@Override
			public boolean hasDeleteAccess(@NonNull final E1 entity) {
				return deleteAccessTest.test(entity);
			}

			@Override
			public boolean hasWriteAccess(@NonNull final E1 entity) {
				return false;
			}

			@Override
			public @Nullable IPredicate prepareQueryFilter(final @NonNull IRoot<E1> root, final @Nullable P1 params) {
				return queryFilterFactory.apply(root, params);
			}
		};
	}

	@SuppressWarnings("unchecked")
	static <E1 extends IEntity<?>, P1 extends Serializable> EntitySecurityManager<E1, P1> withoutSecurity() {
		return NO_SECURITY;
	}

	@SuppressWarnings("rawtypes")
	EntitySecurityManager NO_SECURITY = new EntitySecurityManager() {

		@Override
		public boolean hasReadAccess(final @NonNull IEntity entity) {
			return true;
		}

		@Override
		public boolean hasWriteAccess(final @NonNull IEntity entity) {
			return true;
		}

		@Override
		public IPredicate prepareQueryFilter(final @NonNull IRoot root, final @Nullable Serializable params) {
			return null;
		}
	};
}
