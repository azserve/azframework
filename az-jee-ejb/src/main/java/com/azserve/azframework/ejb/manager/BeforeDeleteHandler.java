package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzServiceException;

public interface BeforeDeleteHandler<E extends IEntity<?>> {

	/**
	 * Metodo chiamato quando l'entity da eliminare non viene trovata.
	 * Di default non fa nulla ma è possibile sollevare un'eccezione.
	 *
	 * @throws AzServiceException
	 */
	default void deleteEntityNotFound() throws AzServiceException {}

	void beforeDelete(@NonNull E entity) throws AzServiceException;

}
