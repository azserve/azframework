package com.azserve.azframework.ejb.manager.property;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.enums.EnumUtils;
import com.azserve.azframework.common.io.SerializeUtils;

public class PropertyUtils {

	public static ValueConverter<String> stringConverter() {
		return PropertyUtils.STRING_CONVERTER;
	}

	public static ValueConverter<Boolean> booleanConverter() {
		return PropertyUtils.BOOLEAN_CONVERTER;
	}

	public static ValueConverter<Integer> integerConverter() {
		return PropertyUtils.INTEGER_CONVERTER;
	}

	public static ValueConverter<Long> longConverter() {
		return PropertyUtils.LONG_CONVERTER;
	}

	public static ValueConverter<BigDecimal> bigDecimalConverter() {
		return PropertyUtils.BIGDECIMAL_CONVERTER;
	}

	public static ValueConverter<LocalDate> localDateConverter() {
		return PropertyUtils.LOCAL_DATE_CONVERTER;
	}

	public static ValueConverter<LocalDateTime> localDateTimeConverter() {
		return PropertyUtils.LOCAL_DATE_TIME_CONVERTER;
	}

	public static ValueConverter<LocalTime> localTimeConverter() {
		return PropertyUtils.LOCAL_TIME_CONVERTER;
	}

	public static <E extends Enum<E>> EnumValueConverter<E> enumConverter(final Class<E> enumType) {
		return new EnumValueConverter<>(enumType);
	}

	public static ValueConverter<Serializable> serializableConverter() {
		return PropertyUtils.SERIALIZABLE_CONVERTER;
	}

	private static final ValueConverter<String> STRING_CONVERTER = new ValueConverter<String>() {

		@Override
		public @NonNull String parse(final @NonNull String source) throws Exception {
			return source;
		}

		@Override
		public @NonNull String format(final @NonNull String value) {
			return value;
		}

		@Override
		public @NonNull Class<String> getValueClass() {
			return String.class;
		}
	};

	private static final ValueConverter<Boolean> BOOLEAN_CONVERTER = new ValueConverter<Boolean>() {

		@Override
		public @NonNull Boolean parse(final @NonNull String source) throws Exception {
			return Boolean.valueOf(source);
		}

		@Override
		public @NonNull String format(final @NonNull Boolean value) {
			return value.toString();
		}

		@Override
		public @NonNull Class<Boolean> getValueClass() {
			return Boolean.class;
		}
	};

	private static final ValueConverter<Integer> INTEGER_CONVERTER = new ValueConverter<Integer>() {

		@Override
		public @NonNull Integer parse(final @NonNull String source) throws Exception {
			return Integer.valueOf(source);
		}

		@Override
		public @NonNull String format(final @NonNull Integer value) {
			return value.toString();
		}

		@Override
		public @NonNull Class<Integer> getValueClass() {
			return Integer.class;
		}
	};

	private static final ValueConverter<Long> LONG_CONVERTER = new ValueConverter<Long>() {

		@Override
		public @NonNull Long parse(final @NonNull String source) throws Exception {
			return Long.valueOf(source);
		}

		@Override
		public @NonNull String format(final @NonNull Long value) {
			return value.toString();
		}

		@Override
		public @NonNull Class<Long> getValueClass() {
			return Long.class;
		}
	};

	private static final ValueConverter<BigDecimal> BIGDECIMAL_CONVERTER = new ValueConverter<BigDecimal>() {

		@Override
		public @NonNull BigDecimal parse(final @NonNull String source) throws Exception {
			return new BigDecimal(source);
		}

		@Override
		public @NonNull String format(final @NonNull BigDecimal value) {
			return value.toString();
		}

		@Override
		public @NonNull Class<BigDecimal> getValueClass() {
			return BigDecimal.class;
		}
	};

	private static final ValueConverter<LocalDate> LOCAL_DATE_CONVERTER = new ValueConverter<LocalDate>() {

		@Override
		public @NonNull LocalDate parse(final @NonNull String source) throws Exception {
			return LocalDate.parse(source, DateTimeFormatter.ISO_LOCAL_DATE);
		}

		@Override
		public @NonNull String format(final @NonNull LocalDate value) {
			return DateTimeFormatter.ISO_LOCAL_DATE.format(value);
		}

		@Override
		public @NonNull Class<LocalDate> getValueClass() {
			return LocalDate.class;
		}

	};

	private static final ValueConverter<LocalDateTime> LOCAL_DATE_TIME_CONVERTER = new ValueConverter<LocalDateTime>() {

		@Override
		public @NonNull LocalDateTime parse(final @NonNull String source) throws Exception {
			return LocalDateTime.parse(source, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		}

		@Override
		public @NonNull String format(final @NonNull LocalDateTime value) {
			return DateTimeFormatter.ISO_LOCAL_DATE.format(value);
		}

		@Override
		public @NonNull Class<LocalDateTime> getValueClass() {
			return LocalDateTime.class;
		}

	};

	private static final ValueConverter<LocalTime> LOCAL_TIME_CONVERTER = new ValueConverter<LocalTime>() {

		@Override
		public @NonNull LocalTime parse(final @NonNull String source) throws Exception {
			return LocalTime.parse(source, DateTimeFormatter.ISO_LOCAL_TIME);
		}

		@Override
		public @NonNull String format(final @NonNull LocalTime value) {
			return DateTimeFormatter.ISO_LOCAL_DATE.format(value);
		}

		@Override
		public @NonNull Class<LocalTime> getValueClass() {
			return LocalTime.class;
		}

	};

	private static final ValueConverter<Serializable> SERIALIZABLE_CONVERTER = new ValueConverter<Serializable>() {

		@Override
		public @NonNull Serializable parse(final @NonNull String source) throws Exception {
			final byte[] data = Base64.getDecoder().decode(source);
			return SerializeUtils.deserialize(data);
		}

		@Override
		public @NonNull String format(final @NonNull Serializable value) {
			try {
				final byte[] data = SerializeUtils.serialize(value);
				return Base64.getEncoder().encodeToString(data);
			} catch (final Exception ex) {
				ex.printStackTrace();
				// non dovrebbe accadere...
				return "";
			}
		}

		@Override
		public @NonNull Class<Serializable> getValueClass() {
			return Serializable.class;
		}
	};

	public static class EnumValueConverter<E extends Enum<E>> implements ValueConverter<E> {

		private final Class<E> enumClass;

		EnumValueConverter(final Class<E> enumClass) {
			this.enumClass = enumClass;
		}

		@Override
		public @NonNull E parse(final @NonNull String source) throws Exception {
			return EnumUtils.valueOf(this.enumClass, source);
		}

		@Override
		public @NonNull String format(final @NonNull E value) {
			return EnumUtils.name(value);
		}

		@Override
		public @NonNull Class<E> getValueClass() {
			return this.enumClass;
		}
	}

	private PropertyUtils() {
		throw new UnsupportedOperationException();
	}

	public interface ValueConverter<T> {

		@NonNull
		T parse(@NonNull String source) throws Exception;

		@NonNull
		String format(@NonNull T value);

		@NonNull
		Class<T> getValueClass();
	}

}
