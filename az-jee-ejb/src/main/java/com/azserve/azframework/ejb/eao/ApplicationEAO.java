package com.azserve.azframework.ejb.eao;

import java.io.Serializable;

import javax.ejb.SessionContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.query.ISingleQuery;
import com.azserve.azframework.query.Queries;

/**
 * Classe che permette l'accesso a {@linkplain EntityManager} e {@linkplain SessionContext}
 * oltre che fornire una facilitazione sulle query e sulle operazioni in scrittura delle entity.
 *
 * La classe va estesa iniettando le 2 risorse sopracitate.
 *
 * @author filippo
 * @since 22/12/2016
 */
public abstract class ApplicationEAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationEAO.class);

	/**
	 * Fornisce l'{@linkplain EntityManager} per le operazioni
	 * sulle entity.
	 */
	public abstract EntityManager getEntityManager();

	/**
	 * Fornisce l'{@linkplain SessionContext} per le informazioni
	 * sulla sessione.
	 */
	public abstract SessionContext getSessionContext();

	/**
	 * Restituisce una factory per generare query.
	 */
	public ISingleQuery<Void, Void> query() {
		return Queries.create(this.getEntityManager());
	}

	/**
	 * Restituisce una factory per generare query.
	 *
	 * @param entityClass tipo di entity su cui
	 *            effettuare la query.
	 */
	public <T> ISingleQuery<T, T> query(final @NonNull Class<T> entityClass) {
		return Queries.from(this.getEntityManager(), entityClass);
	}

	/**
	 * Cerca un'entity per chiave.
	 *
	 * @param entityClass classe dell'entity.
	 * @param id chiave dell'entity.
	 */
	public <T extends IEntity<K>, K extends Serializable> T find(final @NonNull Class<T> entityClass, final @Nullable K id) {
		LOGGER.debug("find({}, {})", entityClass, id);
		return id != null ? this.getEntityManager().find(entityClass, id) : null;
	}

	/**
	 * Restituisce un riferimento fittizio ad un'entity per chiave.
	 *
	 * @param entityClass classe dell'entity.
	 * @param id chiave dell'entity.
	 */
	public <T extends IEntity<K>, K extends Serializable> T getReference(final @NonNull Class<T> entityClass, final @Nullable K id) {
		LOGGER.debug("getReference({}, {})", entityClass, id);
		return id != null ? this.getEntityManager().getReference(entityClass, id) : null;
	}

	/**
	 * Effettua l'inserimento di un'entity.
	 *
	 * @param entity entity da inserire.
	 */
	public <T> void insert(final T entity) {
		LOGGER.debug("insert {}", entity);
		this.getEntityManager().persist(entity);
	}

	/**
	 * Effettua il merge di un'entity.
	 *
	 * @param entity entity da aggiornare.
	 */
	public <T> T update(final T entity) {
		LOGGER.debug("update {}", entity);
		return this.getEntityManager().merge(entity);
	}

	/**
	 * Effettua l'eliminazione di un'entity.
	 *
	 * @param entity entity da eliminare.
	 */
	public <T> void delete(final T entity) {
		LOGGER.debug("delete {}", entity);
		this.getEntityManager().remove(this.getEntityManager().merge(entity));
	}
}
