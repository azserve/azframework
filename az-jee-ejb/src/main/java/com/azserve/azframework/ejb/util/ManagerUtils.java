package com.azserve.azframework.ejb.util;

import static com.azserve.azframework.common.funct.Functions.identity;
import static java.util.stream.Collectors.toMap;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.persistence.EntityManager;

import com.azserve.azframework.common.funct.TriConsumer;
import com.azserve.azframework.dto.DtoWithKey;
import com.azserve.azframework.ejb.eao.ApplicationEAO;
import com.azserve.azframework.entity.IEntity;

public class ManagerUtils {

	private ManagerUtils() {}

	public static <E extends IEntity<K>, D extends DtoWithKey<D, K, ?>, K extends Serializable> void toEntityCollection(final Collection<E> entities, final Collection<D> dtos, final EntityManager em, final Supplier<E> newEntitySupplier, final BiConsumer<D, E> toEntity) {
		final Map<K, E> map = entities.stream().collect(toMap(E::getId, identity()));
		for (final D dto : dtos) {
			final K key = dto.getId();
			if (key == null) {
				final E e = newEntitySupplier.get();
				toEntity.accept(dto, e);
				entities.add(e);
				em.persist(e);
			} else {
				final E e = map.remove(key);
				if (e != null) {
					toEntity.accept(dto, e);
					em.merge(e);
				} else {
					// ERROR
				}
			}
		}
		for (final E e : map.values()) {
			entities.remove(e);
			em.remove(e);
		}
	}

	/**
	 * Given a collection of entities fetched from the persistence context
	 * and a collection of dto to save, merges the data inserting/updating
	 * the entities matching a common key and removing the unmatched entities.
	 *
	 * @param eao
	 * @param entities
	 * @param dtos
	 * @param entityKeyGetter
	 * @param dtoKeyGetter
	 * @param entitySupplier
	 * @param entityFiller
	 */
	public static <E, D, K> void merge(
			final ApplicationEAO eao,
			final Collection<E> entities,
			final Collection<D> dtos,
			final Function<E, K> entityKeyGetter,
			final Function<D, K> dtoKeyGetter,
			final Supplier<E> entitySupplier,
			final TriConsumer<K, D, E> entityFiller) {
		if (entities.isEmpty()) {
			for (final D dto : dtos) {
				final K key = dtoKeyGetter.apply(dto);
				final E entity = entitySupplier.get();
				entityFiller.accept(key, dto, entity);
				eao.insert(entity);
			}
			return;
		}
		final Map<K, E> entitiesMap = entities.stream().collect(toMap(entityKeyGetter, identity()));
		for (final D dto : dtos) {
			final K key = dtoKeyGetter.apply(dto);
			E entity = entitiesMap.remove(key);
			if (entity == null) {
				entity = entitySupplier.get();
				entityFiller.accept(key, dto, entity);
				eao.insert(entity);
			} else {
				entityFiller.accept(key, dto, entity);
				eao.update(entity);
			}
		}
		for (final E entity : entitiesMap.values()) {
			eao.delete(entity);
		}
	}
}
