package com.azserve.azframework.ejb.manager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.dto.Dto;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzInvalidDataException;
import com.azserve.azframework.exception.AzServiceException;

public class InheritableInsertHandler<E extends IEntity<?>, D extends Dto> implements InsertHandler<E, D> {

	private final InsertHandler<E, D> handler;

	public InheritableInsertHandler(final @NonNull InsertHandler<E, D> handler) {
		this.handler = handler;
	}

	@Override
	public void validateInsert(final @NonNull D dto) throws AzInvalidDataException {
		this.handler.validateInsert(dto);
	}

	@Override
	public void beforeInsert(final @NonNull D dto, final @NonNull E entity) throws AzServiceException {
		this.handler.beforeInsert(dto, entity);
	}

	@Override
	public void afterInsert(final @NonNull D dto, final @NonNull E entity) throws AzServiceException {
		this.handler.afterInsert(dto, entity);
	}

}
