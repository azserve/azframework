package com.azserve.azframework.eao;

import java.io.Serializable;

import javax.annotation.security.PermitAll;
import javax.persistence.EntityManager;

import com.azserve.azframework.ejb.eao.ApplicationEAO;
import com.azserve.azframework.entity.IEntity;

/**
 * Classe per la gestione grezza di una entity,
 * inserimento, modifica e cancellazione.
 *
 * L'entity manager viene fornito da un {@code EAO} che dovrà essere iniettato dalla classe figlia.
 *
 * @param <T> classe della entity.
 * @param <ID> classe della primary key.
 */
@PermitAll
public abstract class GenericJpaCudEAO<T extends IEntity<ID>, ID extends Serializable> {

	public void insert(final T entity) {
		this.getEao().insert(entity);
	}

	public T update(final T entity) {
		return this.getEao().update(entity);
	}

	public void delete(final T entity) {
		this.getEao().delete(entity);
	}

	public abstract ApplicationEAO getEao();

	public EntityManager getEntityManager() {
		return this.getEao().getEntityManager();
	}

	public boolean isCallerInRole(final String role) {
		return this.getEao().getSessionContext().isCallerInRole(role);
	}

	public String getLogin() {
		return this.getEao().getSessionContext().getCallerPrincipal().getName();
	}
}
