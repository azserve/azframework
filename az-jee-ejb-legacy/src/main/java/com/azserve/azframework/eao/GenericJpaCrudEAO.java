package com.azserve.azframework.eao;

import java.io.Serializable;

import javax.annotation.security.PermitAll;

import com.azserve.azframework.entity.IEntity;

@PermitAll
public abstract class GenericJpaCrudEAO<T extends IEntity<ID>, ID extends Serializable> extends GenericJpaCudEAO<T, ID> {

	/**
	 * Cerca una entity per chiave.
	 *
	 * @param id chiave di ricerca.
	 */
	public T findById(final ID id) {
		return this.getEao().find(this.getEntityClass(), id);
	}

	/**
	 * Restituisce la classe dell'entity gestita.
	 *
	 * @return classe dell'entity gestita.
	 */
	protected abstract Class<T> getEntityClass();
}
