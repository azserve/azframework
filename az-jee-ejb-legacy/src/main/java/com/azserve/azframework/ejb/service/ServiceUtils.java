package com.azserve.azframework.ejb.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.criteria.Predicate;

import com.azserve.azframework.dto.DtoReferenceWithInteger;
import com.azserve.azframework.dto.DtoWithInteger;
import com.azserve.azframework.dto.DtoWithKey;
import com.azserve.azframework.entity.EntityWithInteger;
import com.azserve.azframework.entity.IEntity;

public final class ServiceUtils {

	private ServiceUtils() {}

	public static <E extends EntityWithInteger, D extends DtoWithInteger<D>> void toEntitySet(final ServiceRW<E, ?, D, ?, ?> service, final Set<E> entities, final Set<D> dtos) throws Exception {
		final Map<DtoReferenceWithInteger<D>, E> map = entities.stream().collect(Collectors.toMap(e -> new DtoReferenceWithInteger<>(e.getId()), Function.identity()));
		for (final D dto : dtos) {
			final DtoReferenceWithInteger<D> reference = dto.getReference();
			if (reference == null) {
				final D d = service.insert(dto);
				entities.add(service.findEntity(d));
			} else {
				final E e = map.get(reference);
				if (e != null) {
					service.update(dto);
					map.remove(reference);
				} else {
					// ERROR
				}
			}
		}
		for (final E e : map.values()) {
			entities.remove(e);
			service.delete(e);
		}
	}

	/**
	 * Restituisce un array di oggetti {@code Predicate} a partire da una lista.
	 *
	 * @param predicates lista di predicati.
	 */
	public static Predicate[] predicateArray(final List<Predicate> predicates) {
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	public static <E extends IEntity<K>, D extends DtoWithKey<D, K, ?>, K extends Serializable> void toEntitySet(final Set<E> entities, final Set<D> dtos, final EntityManager em, final Supplier<E> newEntitySupplier, final BiConsumer<D, E> toEntity) {
		final Map<K, E> map = entities.stream().collect(Collectors.toMap(E::getId, Function.identity()));
		for (final D dto : dtos) {
			final K key = dto.getId();
			if (key == null) {
				final E e = newEntitySupplier.get();
				toEntity.accept(dto, e);
				entities.add(e);
				em.persist(e);
			} else {
				final E e = map.remove(key);
				if (e != null) {
					toEntity.accept(dto, e);
					em.merge(e);
				} else {
					// ERROR
				}
			}
		}
		for (final E e : map.values()) {
			entities.remove(e);
			em.remove(e);
		}
	}

	public static <E extends EntityWithInteger> void cleanEntitySet(final Set<E> entities, final EntityManager em) {
		for (final E e : entities) {
			em.remove(e);
		}
	}
}
