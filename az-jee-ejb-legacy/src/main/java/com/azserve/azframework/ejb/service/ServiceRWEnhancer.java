package com.azserve.azframework.ejb.service;

import com.azserve.azframework.dto.DtoWithKey;
import com.azserve.azframework.entity.IEntity;

public class ServiceRWEnhancer<T_Dto extends DtoWithKey<?, ?, ?>, T_Entity extends IEntity<?>>
		implements PreInsertHandler<T_Dto, T_Entity>,
		PostInsertHandler<T_Dto, T_Entity>,
		PreUpdateHandler<T_Dto, T_Entity>,
		PostUpdateHandler<T_Dto, T_Entity>,
		PreDeleteHandler<T_Entity>,
		PostDeleteHandler<T_Entity>,
		DtoValidator<T_Dto, T_Entity> {

	private final ServiceRW<T_Entity, ?, T_Dto, ?, ?> serviceRw;

	public ServiceRWEnhancer(final ServiceRW<T_Entity, ?, T_Dto, ?, ?> service) {
		this.serviceRw = service;
	}

	/**
	 * Effettua una validazione dei dati del dto contro
	 * quelli della entity appena prelevata dal database.
	 * <br/>
	 * Può tornare utile per campi soggetti a concorrenza.
	 *
	 * @param dto dato sorgente.
	 * @param entity entity prelevata dal database e non
	 *            ancora compilata.
	 */
	@Override
	public void validateDto(final T_Dto dto, final T_Entity entity) throws Exception {
		this.serviceRw.validateUpdate(dto, entity);
	}

	/**
	 * Metodo invocato prima di un inserimento.
	 *
	 * @param dto dato sorgente.
	 * @param entity entity compilata e pronta per il salvataggio.
	 */
	@Override
	public void preInsert(final T_Dto dto, final T_Entity entity) throws Exception {
		this.serviceRw.preInsert(dto, entity);
	}

	/**
	 * Metodo invocato prima di una modifica.
	 *
	 * @param dto dato sorgente.
	 * @param entity entity compilata e pronta per il salvataggio.
	 */
	@Override
	public void preUpdate(final T_Dto dto, final T_Entity entity) throws Exception {
		this.serviceRw.preUpdate(dto, entity);
	}

	/**
	 * Metodo invocato prima di una cancellazione.
	 *
	 * @param entity entity pronta per la cancellazione.
	 */
	@Override
	public void preDelete(final T_Entity entity) throws Exception {
		this.serviceRw.preDelete(entity);
	}

	/**
	 * Metodo invocato dopo di un inserimento.
	 *
	 * @param dto dato sorgente.
	 * @param entity entity inserita.
	 */
	@Override
	public void postInsert(final T_Dto dto, final T_Entity entity) throws Exception {
		this.serviceRw.postInsert(dto, entity);
	}

	/**
	 * Metodo invocato dopo di una modifica.
	 *
	 * @param dto dato sorgente.
	 * @param entity entity passata per l'aggiornamento.
	 */
	@Override
	public void postUpdate(final T_Dto dto, final T_Entity entity) throws Exception {

		this.serviceRw.postUpdate(dto, entity);

	}

	/**
	 * Metodo invocato dopo di una cancellazione.
	 *
	 * @param entity entity cancellata.
	 */
	@Override
	public void postDelete(final T_Entity entity) throws Exception {
		this.serviceRw.postDelete(entity);
	}

}
