package com.azserve.azframework.ejb.util;

import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.ejb.ApplicationException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.ex.Checked;
import com.azserve.azframework.common.ex.CheckedConsumer;
import com.azserve.azframework.common.ex.CheckedFunction;
import com.azserve.azframework.common.util.Pair;
import com.azserve.azframework.exception.AzServiceException;

public final class RemoteService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RemoteService.class);

	private RemoteService() {}

	public static <C> void invoke(final Parameters parameters, final boolean requiredTransaction, final Class<C> remoteInterface, final CheckedConsumer<C, Exception> consumer)
			throws AzServiceException, com.azserve.azframework.interfaces.service.exception.AzServiceException, RemoteServiceException {
		invokeGet(parameters, requiredTransaction, remoteInterface, Checked.consumerToFunction(consumer));
	}

	public static <C> void invoke(final Parameters parameters, final Class<C> remoteInterface, final CheckedConsumer<C, Exception> function)
			throws AzServiceException, com.azserve.azframework.interfaces.service.exception.AzServiceException, RemoteServiceException {
		invoke(parameters, true, remoteInterface, function);
	}

	public static <T, C> T invokeGet(final Parameters parameters, final Class<C> remoteInterface, final CheckedFunction<C, T, Exception> function)
			throws AzServiceException, com.azserve.azframework.interfaces.service.exception.AzServiceException, RemoteServiceException {
		return invokeGet(parameters, true, remoteInterface, function);
	}

	public static Pair<Context, Context> createContext(final String id, final String host, final int port, final String username, final String password, final Integer connectTimeout) throws NamingException {
		final Properties properties = new Properties();

		properties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		properties.put("org.jboss.ejb.client.scoped.context", "true");
		properties.put("endpoint.name", id);
		properties.put("remote.connections", id);

		properties.put("remote.connection." + id + ".host", host);
		properties.put("remote.connection." + id + ".port", "" + port);
		if (username != null) {
			properties.put("remote.connection." + id + ".username", username);
			if (password != null) {
				properties.put("remote.connection." + id + ".password", password);
				properties.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
			}
		} else {
			properties.put("remote.connection." + id + ".connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS", "false");
		}
		properties.put("remote.connection." + id + ".connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
		if (connectTimeout != null) {
			properties.put("remote.connection." + id + ".connect.timeout", connectTimeout);
		}

		final Context initialContext = new InitialContext(properties);
		final Context ejbContext = (Context) initialContext.lookup("ejb:");

		return Pair.of(initialContext, ejbContext);
	}

	public static String buildLookup(final String appName, final String moduleName, final Function<Class<?>, String> nameResolver, final Class<?> remoteInterface, final boolean stateful) {
		final String lookup = appName + "/" + moduleName + "//" + nameResolver.apply(remoteInterface) + "!" + remoteInterface.getCanonicalName();
		if (stateful) {
			return lookup + "?stateful";
		}
		return lookup;
	}

	public static <T, C> T invokeGet(final Parameters parameters, final boolean requiredTransaction, final Class<C> remoteInterface, final CheckedFunction<C, T, Exception> function)
			throws AzServiceException, com.azserve.azframework.interfaces.service.exception.AzServiceException, RemoteServiceException {

		try {
			final Pair<Context, Context> contexts = createContext(parameters.idProvider.get(), parameters.host, parameters.port, parameters.username, parameters.password, parameters.connectTimeout);
			final Context initialContext = contexts.getValue1();
			final Context ejbContext = contexts.getValue2();
			try {
				@SuppressWarnings("unchecked")
				final C service = (C) ejbContext.lookup(buildLookup(parameters.appName, parameters.moduleName, parameters.serviceNameResolver, remoteInterface, false));
				return function.apply(service);
			} finally {
				if (requiredTransaction) {
					// FIXME per ora schedulo la chiusura del contesto dopo alcuni minuti, altrimenti salta la transazione
					Executors.newSingleThreadScheduledExecutor().schedule(() -> close(ejbContext, initialContext), 10, TimeUnit.MINUTES);
				} else {
					close(ejbContext, initialContext);
				}
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore chiamata remota", ex);

			if (ex instanceof AzServiceException) {
				throw (AzServiceException) ex;
			}
			if (ex.getCause() instanceof AzServiceException) {
				throw (AzServiceException) ex.getCause();
			}
			if (ex instanceof com.azserve.azframework.interfaces.service.exception.AzServiceException) {
				throw (com.azserve.azframework.interfaces.service.exception.AzServiceException) ex;
			}
			if (ex.getCause() instanceof com.azserve.azframework.interfaces.service.exception.AzServiceException) {
				throw (com.azserve.azframework.interfaces.service.exception.AzServiceException) ex.getCause();
			}

			// IllegalStateException
			// EJBCLIENT000025: No EJB receiver available for handling [appName:Np, moduleName:NpEjb, distinctName:]
			throw new RemoteServiceException();
		}
	}

	public static void close(final Context ejbContext, final Context initialContext) {
		try {
			ejbContext.close();
		} catch (final Throwable th) {
			LOGGER.warn(th.getLocalizedMessage(), th);
		}

		try {
			initialContext.close();
		} catch (final Throwable th) {
			LOGGER.warn(th.getLocalizedMessage(), th);
		}
	}

	// 0:Module name cannot be null or empty
	// 1:Bean name cannot be null or empty
	// 2:Bean interface type cannot be null
	// 3:Incorrect max-allowed-connected-nodes value %s specified for cluster named %s. Defaulting to %s
	// 4:Incorrect connection timeout value %s specified for cluster named %s. Defaulting to %s
	// 5:Incorrect connection timeout value %s specified for node %s in cluster named %s. Defaulting to %s
	// 6:No host/port configured for connection named %s. Skipping connection creation
	// 7:Incorrect port value %s specified for connection named %s. Skipping connection creation
	// 8:Incorrect connection timeout value %s specified for connection named %s. Defaulting to %s
	// 9:Incorrect invocation timeout value %s specified. Defaulting to %s
	// 10:Incorrect reconnect tasks timeout value %s specified. Defaulting to %s
	// 11:Discarding result for invocation id %s since no waiting context found
	// 12:Cannot create a EJB receiver for %s since there was no match for a target destination
	// 13:Successful version handshake completed for receiver context %s on channel %s
	// 14:Version handshake not completed for receiver context %s. Closing receiver context
	// 15:Initial module availability report for %s wasn't received during the receiver context association
	// 16:Channel %s can no longer process messages
	// 17:Received server version %d and marshalling strategies %s
	// 18:%s cannot be null
	// 19:Node name cannot be null or empty string, while adding a node to cluster named %s
	// 20:%s cannot be null or empty string
	// 21:EJB client context selector may not be changed
	// 22:No EJB client context is available
	// 23:EJB client interceptor %s is already registered
	// 24:No EJB receiver available for handling [appName:%s, moduleName:%s, distinctName:%s] combination
	// 25:No EJB receiver available for handling [appName:%s, moduleName:%s, distinctName:%s] combination for invocation context %s
	// 26:%s has not been associated with %s
	// 27:No EJBReceiver available for node name %s
	// 28:No EJB receiver contexts available in cluster %s
	// 29:No cluster context available for cluster named %s
	// 30:sendRequest() called during wrong phase
	// 31:No receiver associated with invocation
	// 32:Cannot retry a request which hasn't previously been completed
	// 33:getResult() called during wrong phase
	// 34:discardResult() called during wrong phase
	// 35:Not supported
	// 36:Read only naming context, operation not supported
	// 37:Could not load ejb proxy class %s
	// 38:Transaction enlistment did not yield a transaction ID
	// 39:Cannot enlist transaction
	// 40:EJB communication channel %s is not yet ready to receive invocations (perhaps version handshake hasn't been completed), for receiver context %s
	// 41:A session bean does not have a primary key class
	// 42:Failed to find EJB client configuration file specified in %s system property
	// 43:Error reading EJB client properties file %s
	// 44:No transaction context available
	// 45:User transactions not supported by this context
	// 46:A transaction is already associated with this thread
	// 47:A transaction is not associated with this thread
	// 48:Transaction for this thread is not active
	// 49:Cannot proceed with invocation since transaction is pinned to node %s which has been excluded from handling invocation for the current invocation context %s
	// 50:Node of the current transaction %s does not accept %s
	// 51:Cannot proceed with invocation since the locator %s has an affinity on node %s which has been excluded from current invocation context %s
	// 52:%s for cluster %s is not of type org.jboss.ejb.client.ClusterNodeSelector
	// 53:Could not create the cluster node selector for cluster %s
	// 54:Cannot specify both a callback handler and a username/password
	// 55:Could not decode base64 encoded password
	// 56:Cannot specify both a plain text and base64 encoded password
	// 57:%s not of type org.jboss.ejb.client.DeploymentNodeSelector
	// 58:Could not create the deployment node selector
	// 59:Could not send a message over remoting channel, to cancel invocation for invocation id %s
	// 60:Failed to create scoped EJB client context
	// 61:Cannot send a transaction recovery message to the server since the protocol version of EJBReceiver %s doesn't support it
	// 100:Object '%s' is not a valid proxy object
	// 101:Proxy object '%s' was not generated by %s
	// 102:No asynchronous operation in progress
	// 400:Remote invocation failed due to an exception
	// 401:Result was discarded (one-way invocation)
	// 402:Remote invocation request was cancelled
	// 403:Timed out
	// 404:Operation not allowed since this EJB client context %s has been closed
	// 405:An EJB client context is already registered for EJB client context identifier %s
	// 406:Unexpected exception when discarding invocation result

	/**
	 * Eccezione propagata in caso di errori di comunicazione
	 * con il server remoto.
	 */
	@ApplicationException(inherited = true, rollback = true)
	public static final class RemoteServiceException extends Exception {

		private static final long serialVersionUID = -8943759939512390083L;

		public RemoteServiceException() {
			super();
		}
	}

	/**
	 * Oggetto contenente i parametri di connessione al server remoto.
	 */
	public static final class Parameters {

		private final String host;
		private final int port;
		private final String username;
		private final String password;
		private final String appName;
		private final String moduleName;
		private final Integer connectTimeout;
		private final Supplier<String> idProvider;
		private final Function<Class<?>, String> serviceNameResolver;

		public Parameters(final @NonNull String host, final int port, final @NonNull String appName,
				final @NonNull String moduleName) {
			this(host, port, null, null, appName, moduleName);
		}

		public Parameters(final @NonNull String host, final int port, final String username, final String password,
				final @NonNull String appName, final @NonNull String moduleName) {
			this(host, port, username, password, appName, moduleName, null);
		}

		public Parameters(final @NonNull String host, final int port, final String username, final String password,
				final @NonNull String appName, final @NonNull String moduleName, final @Nullable Integer connectTimeout) {
			this(host, port, username, password, appName, moduleName, connectTimeout,
					() -> UUID.randomUUID().toString().replace("-", ""), c -> c.getSimpleName().substring(1));
		}

		public Parameters(final @NonNull String host, final int port, final String username, final String password,
				final @NonNull String appName, final @NonNull String moduleName,
				final @Nullable Integer connectTimeout,
				final @NonNull Supplier<String> idProvider,
				final @NonNull Function<Class<?>, String> serviceNameResolver) {
			this.host = host;
			this.port = port;
			this.username = username;
			this.password = password;
			this.appName = appName;
			this.moduleName = moduleName;
			this.connectTimeout = connectTimeout;
			this.idProvider = idProvider;
			this.serviceNameResolver = serviceNameResolver;
		}

		@Override
		public String toString() {
			return "Parameters [host=" + this.host + ", port=" + this.port + ", username=" + this.username + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (this.host == null ? 0 : this.host.hashCode());
			result = prime * result + this.port;
			result = prime * result + (this.username == null ? 0 : this.username.hashCode());
			return result;
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (this.getClass() != obj.getClass()) {
				return false;
			}
			final Parameters other = (Parameters) obj;
			if (this.host == null) {
				if (other.host != null) {
					return false;
				}
			} else if (!this.host.equals(other.host)) {
				return false;
			}
			if (this.port != other.port) {
				return false;
			}
			if (this.username == null) {
				if (other.username != null) {
					return false;
				}
			} else if (!this.username.equals(other.username)) {
				return false;
			}
			return true;
		}
	}
}