package com.azserve.azframework.ejb.service;

import com.azserve.azframework.dto.DtoReferenceWithLong;
import com.azserve.azframework.dto.DtoWithLong;
import com.azserve.azframework.dto.HasInsertId;
import com.azserve.azframework.entity.EntityWithLong;

public abstract class ServiceRWWithLong<T_Entity extends EntityWithLong, T_Dto extends DtoWithLong<T_Dto>>
		extends ServiceRW<T_Entity, Long, T_Dto, Long, DtoReferenceWithLong<T_Dto>> {

	@SuppressWarnings("unchecked")
	@Override
	protected void compilePrimaryKeyEntity(final T_Dto dto, final T_Entity entity) {
		final DtoReferenceWithLong<T_Dto> reference = dto.getReference();
		if (reference == null) {
			if (dto instanceof HasInsertId) {
				entity.setId(((HasInsertId<Long>) dto).getInsertId());
			} else {
				entity.setId(Long.valueOf(System.currentTimeMillis()));
			}
		} else {
			entity.setId(reference.getId());
		}
	}

	@Override
	public Long createPrimaryKey(final Long key) {
		return key;
	}

	@Override
	public DtoReferenceWithLong<T_Dto> createReference(final T_Entity entity) {
		return entity == null ? null : new DtoReferenceWithLong<>(entity.getId());
	}
}
