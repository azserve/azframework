package com.azserve.azframework.ejb.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.funct.Functions;
import com.azserve.azframework.dto.DtoReference;
import com.azserve.azframework.dto.DtoWithKey;
import com.azserve.azframework.entity.IEntity;

/**
 * Classe per il riempimento di dto a partire da entity.
 *
 * @param <T_Entity> entity da gestire.
 * @param <T_kEntity> chiave dell'entity da gestire.
 * @param <T_Dto> dto da gestire.
 * @param <T_DtoReferenceKey> chiave del dto da gestire.
 * @param <T_DtoReference> reference del dto da gestire.
 */
public abstract class EntityDTOFiller<T_Entity extends IEntity<T_kEntity>, T_kEntity extends Serializable, T_Dto extends DtoWithKey<?, T_DtoReferenceKey, T_DtoReference>, T_DtoReferenceKey extends Serializable, T_DtoReference extends DtoReference<? super T_Dto, T_DtoReferenceKey>> {

	/**
	 * Crea e restituisce il dto corrispondente alla entity.<br>
	 * Se l'entity è {@code null} restituisce {@code null}.
	 */
	public T_Dto getDto(final T_Entity entity) {
		return entity == null ? null : this.getDtoNoCheck(entity);
	}

	final T_Dto getDtoNoCheck(final @NonNull T_Entity entity) {
		final T_Dto dto = this.createDto(entity);
		this.compileDto(entity, dto);
		return dto;
	}

	/**
	 * Inserisce nella collezione di dto, i dto corrispondenti alle entity.<br>
	 * Se la collezione di dto non è vuota, i nuovi dto vengono aggiunti.
	 */
	protected void fillDtoCollection(final Collection<? extends T_Entity> entities, final Collection<T_Dto> dtos) {
		entities.stream().filter(this.getFilter()).map(this::getDtoNoCheck).forEach(dtos::add);
	}

	/**
	 * Crea una lista di dto corrispondente alla collezione di entity passata come parametro.
	 */
	public List<T_Dto> getDtoList(final List<? extends T_Entity> entities) {
		if (entities == null) {
			return null;
		}
		final List<T_Dto> result = new ArrayList<>(entities.size());
		this.fillDtoCollection(entities, result);
		return result;
	}

	public Collection<T_Dto> getDtoCollection(final Collection<? extends T_Entity> entities) {
		return this.getDtoCollection(entities, ArrayList::new);
	}

	public <C extends Collection<T_Dto>> C getDtoCollection(final Collection<? extends T_Entity> entities, final Supplier<C> supplier) {
		if (entities == null) {
			return null;
		}
		return entities.stream().map(e -> this.getDtoNoCheck(e)).collect(Collectors.toCollection(supplier));
	}

	public Predicate<T_Entity> getFilter() {
		return Functions.acceptAll();
	}

	/**
	 * Crea una chiave primaria di una entity a partire da una referenza.
	 *
	 * @param reference la referenza.
	 * @return la chiave primaria generata.
	 */
	public final T_kEntity createPrimaryKey(final T_DtoReference reference) {
		return reference == null ? (T_kEntity) null : this.createPrimaryKey(reference.getId());
	}

	protected abstract T_kEntity createPrimaryKey(T_DtoReferenceKey reference);

	/**
	 * Crea una referenza tipizzata corrispondente alla entity.
	 */
	public abstract T_DtoReference createReference(T_Entity entity);

	/**
	 * Copia i dati del dalla entity al dto.
	 */
	protected abstract void compileDto(@NonNull T_Entity entity, @NonNull T_Dto dto);

	/**
	 * Crea e restituisce un nuovo dto con la reference compilata.
	 */
	protected abstract T_Dto createDto(T_Entity entity);
}
