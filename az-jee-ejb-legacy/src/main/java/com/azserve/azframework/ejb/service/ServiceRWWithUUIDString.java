package com.azserve.azframework.ejb.service;

import java.util.UUID;

import com.azserve.azframework.dto.DtoWithString;
import com.azserve.azframework.entity.EntityWithString;

public abstract class ServiceRWWithUUIDString<T_Entity extends EntityWithString, T_Dto extends DtoWithString<T_Dto>>
		extends ServiceRWWithString<T_Entity, T_Dto> {

	@Override
	protected String getDefaultId() {
		return UUID.randomUUID().toString();
	}
}
