package com.azserve.azframework.ejb.service;

import java.util.UUID;

import com.azserve.azframework.dto.DtoReferenceWithUuid;
import com.azserve.azframework.dto.DtoWithUuid;
import com.azserve.azframework.entity.EntityWithUuid;
import com.azserve.azframework.entity.UuidPk;

/**
 * Service per la gestione di dati in sola lettura
 * che hanno per chiave primaria un UUID.
 *
 * @param <T_Entity> entity da gestire.
 * @param <T_Dto> dto da gestire.
 * @param <T_Eao> eao di riferimento.
 */
public abstract class ServiceROWithUUID<T_Entity extends EntityWithUuid<T_kEntity>, T_kEntity extends UuidPk, T_Dto extends DtoWithUuid<T_Dto>>
		extends ServiceRO<T_Entity, T_kEntity, T_Dto, UUID, DtoReferenceWithUuid<T_Dto>> {

	@Override
	public T_kEntity createPrimaryKey(final UUID key) {
		final T_kEntity uuidPk = this.newPrimaryKey();
		uuidPk.setUuid(key);
		return uuidPk;
	}

	protected abstract T_kEntity newPrimaryKey();

	@Override
	public DtoReferenceWithUuid<T_Dto> createReference(final T_Entity entity) {
		return entity == null ? null : new DtoReferenceWithUuid<>(entity.getGuid());
	}
}
