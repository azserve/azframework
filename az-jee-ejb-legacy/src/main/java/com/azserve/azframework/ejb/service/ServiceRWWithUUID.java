package com.azserve.azframework.ejb.service;

import java.util.UUID;

import com.azserve.azframework.dto.DtoReferenceWithUuid;
import com.azserve.azframework.dto.DtoWithUuid;
import com.azserve.azframework.dto.HasInsertId;
import com.azserve.azframework.entity.EntityWithUuid;
import com.azserve.azframework.entity.UuidPk;

public abstract class ServiceRWWithUUID<T_Entity extends EntityWithUuid<T_kEntity>, T_kEntity extends UuidPk, T_Dto extends DtoWithUuid<T_Dto>>
		extends ServiceRW<T_Entity, T_kEntity, T_Dto, UUID, DtoReferenceWithUuid<T_Dto>> {

	@SuppressWarnings("unchecked")
	@Override
	protected void compilePrimaryKeyEntity(final T_Dto dto, final T_Entity entity) {
		final DtoReferenceWithUuid<T_Dto> dtoReference = dto.getReference();
		if (dtoReference == null) {
			if (dto instanceof HasInsertId) {
				entity.setGuid(((HasInsertId<UUID>) dto).getInsertId());
			} else {
				entity.setGuid(UUID.randomUUID());
			}
		} else {
			entity.setGuid(dtoReference.getId());
		}
	}

	@Override
	public T_kEntity createPrimaryKey(final UUID key) {
		final T_kEntity uuidPk = this.newPrimaryKey();
		uuidPk.setUuid(key);
		return uuidPk;
	}

	protected abstract T_kEntity newPrimaryKey();

	@Override
	public DtoReferenceWithUuid<T_Dto> createReference(final T_Entity entity) {
		return entity == null ? null : new DtoReferenceWithUuid<>(entity.getGuid());
	}
}
