package com.azserve.azframework.ejb.service;

import com.azserve.azframework.entity.IEntity;

@FunctionalInterface
public interface PreDeleteHandler<T_Entity extends IEntity<?>> {

	void preDelete(T_Entity entity) throws Exception;
}
