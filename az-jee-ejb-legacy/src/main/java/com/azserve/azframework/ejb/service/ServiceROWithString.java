package com.azserve.azframework.ejb.service;

import com.azserve.azframework.dto.DtoReferenceWithString;
import com.azserve.azframework.dto.DtoWithString;
import com.azserve.azframework.entity.EntityWithString;

/**
 * Service per la gestione di dati in sola lettura
 * che hanno per chiave primaria una stringa.
 *
 * @param <T_Entity> entity da gestire.
 * @param <T_Dto> dto da gestire.
 * @param <T_Eao> eao di riferimento.
 */
public abstract class ServiceROWithString<T_Entity extends EntityWithString, T_Dto extends DtoWithString<T_Dto>>
		extends ServiceRO<T_Entity, String, T_Dto, String, DtoReferenceWithString<T_Dto>> {

	@Override
	public String createPrimaryKey(final String key) {
		return key;
	}

	@Override
	public DtoReferenceWithString<T_Dto> createReference(final T_Entity entity) {
		return entity == null ? null : new DtoReferenceWithString<>(entity.getId());
	}
}
