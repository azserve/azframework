package com.azserve.azframework.ejb.service;

import com.azserve.azframework.dto.DtoReferenceWithInteger;
import com.azserve.azframework.dto.DtoWithInteger;
import com.azserve.azframework.entity.EntityWithInteger;

public abstract class ServiceROWithInteger<T_Entity extends EntityWithInteger, T_Dto extends DtoWithInteger<T_Dto>>
		extends ServiceRO<T_Entity, Integer, T_Dto, Integer, DtoReferenceWithInteger<T_Dto>> {

	@Override
	public Integer createPrimaryKey(final Integer key) {
		return key;
	}

	@Override
	public DtoReferenceWithInteger<T_Dto> createReference(final T_Entity entity) {
		return entity == null ? null : new DtoReferenceWithInteger<>(entity.getId());
	}

}
