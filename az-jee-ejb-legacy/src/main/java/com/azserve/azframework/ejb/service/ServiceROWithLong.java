package com.azserve.azframework.ejb.service;

import com.azserve.azframework.dto.DtoReferenceWithLong;
import com.azserve.azframework.dto.DtoWithLong;
import com.azserve.azframework.entity.EntityWithLong;

/**
 * Service per la gestione di dati in sola lettura
 * che hanno per chiave primaria un long.
 *
 * @param <T_Entity> entity da gestire.
 * @param <T_Dto> dto da gestire.
 * @param <T_Eao> eao di riferimento.
 */
public abstract class ServiceROWithLong<T_Entity extends EntityWithLong, T_Dto extends DtoWithLong<T_Dto>>
		extends ServiceRO<T_Entity, Long, T_Dto, Long, DtoReferenceWithLong<T_Dto>> {

	@Override
	public Long createPrimaryKey(final Long key) {
		return key;
	}

	@Override
	public DtoReferenceWithLong<T_Dto> createReference(final T_Entity entity) {
		return entity == null ? null : new DtoReferenceWithLong<>(entity.getId());
	}
}
