package com.azserve.azframework.ejb.service;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.ex.CheckedConsumer;
import com.azserve.azframework.dto.DtoReference;
import com.azserve.azframework.dto.DtoUtils;
import com.azserve.azframework.dto.DtoWithKey;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.exception.AzInvalidDataException;
import com.azserve.azframework.exception.AzServiceSecurityException;
import com.azserve.azframework.interfaces.service.exception.AzSecurityServiceException;
import com.azserve.azframework.interfaces.service.exception.AzServiceValidationException;
import com.azserve.azframework.validation.ValidationError;

/**
 * Service base per la gestione di dati in lettura e scrittura.
 *
 * @param <T_Entity> entity da gestire.
 * @param <T_kEntity> chiave dell'entity da gestire.
 * @param <T_Dto> dto da gestire.
 * @param <T_DtoReferenceKey> chiave del dto da gestire.
 * @param <T_DtoReference> reference del dto da gestire.
 * @param <T_Eao> eao di riferimento.
 */
public abstract class ServiceRW<T_Entity extends IEntity<T_kEntity>, T_kEntity extends Serializable, T_Dto extends DtoWithKey<?, T_DtoReferenceKey, T_DtoReference>, T_DtoReferenceKey extends Serializable, T_DtoReference extends DtoReference<? super T_Dto, T_DtoReferenceKey>>
		extends ServiceRO<T_Entity, T_kEntity, T_Dto, T_DtoReferenceKey, T_DtoReference> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceRW.class);
	public static boolean useLegacyValidation = false;

	protected final ServiceRWEnhancer<T_Dto, T_Entity> defaultServiceRWEnhancer = new ServiceRWEnhancer<>(this);

	/**
	 * Imposta la chiave primaria della entity a partire dal dto.
	 */
	protected abstract void compilePrimaryKeyEntity(final T_Dto dto, final T_Entity entity);

	/**
	 * Copia i dati dal dto alla entity.
	 */
	protected void compileEntity(final T_Dto dto, final T_Entity entity) {
		this.compilePrimaryKeyEntity(dto, entity);
		this.compileEntityOtherFields(dto, entity);
	}

	/**
	 * Copia i dati dal dto alla entity, tranne la chiave primaria.<br>
	 * <br>
	 *
	 * <b>Attenzione!!!</b> Non copiare la chiave primaria, questa viene già gestita dal metodo {@link #compilePrimaryKeyEntity}. Se serve risovrascrivere quel metodo.
	 *
	 * @param dto
	 * @param entity
	 */
	protected abstract void compileEntityOtherFields(T_Dto dto, T_Entity entity);

	/**
	 * Crea e restituisce una nuova entity.
	 */
	protected abstract T_Entity newEntity();

	/**
	 * Inserisce un dato nel database a partire dal dto.
	 *
	 * @return la nuova entity creata ed inserita.
	 */
	@NonNull
	protected T_Entity insertEntity(final T_Dto dto, final CheckedConsumer<T_Dto, Exception> validator, final PreInsertHandler<T_Dto, T_Entity> preInsertHandler, final PostInsertHandler<T_Dto, T_Entity> postInsertHandler) throws Exception {
		if (dto == null) {
			throw new Exception("DTO == null");
		}
		validator.accept(dto);
		final T_Entity entity = this.newEntity();
		this.compileEntity(dto, entity);
		this.checkPermission(entity);
		preInsertHandler.preInsert(dto, entity);
		this.doInsert(entity);
		postInsertHandler.postInsert(dto, entity);
		return entity;
	}

	@NonNull
	protected T_Entity insertEntity(final T_Dto dto) throws Exception {
		return this.insertEntity(dto, this::validateInsertDto, this.defaultServiceRWEnhancer, this.defaultServiceRWEnhancer);
	}

	/**
	 * Inserisce un dato nel database a partire dal dto.
	 *
	 * @return un nuovo dto con reference compilata.
	 */
	@NonNull
	public T_Dto insert(final T_Dto dto, final CheckedConsumer<T_Dto, Exception> validator, final PreInsertHandler<T_Dto, T_Entity> preInsertHandler, final PostInsertHandler<T_Dto, T_Entity> postInsertHandler) throws Exception {
		return this.getDtoNoCheck(this.insertEntity(dto, validator, preInsertHandler, postInsertHandler));
	}

	/**
	 * Inserisce un dato nel database a partire dal dto.
	 *
	 * @return un nuovo dto con reference compilata.
	 */
	@NonNull
	public T_Dto insert(final T_Dto dto) throws Exception {
		return this.getDtoNoCheck(this.insertEntity(dto));
	}

	/**
	 * @see #doDelete(IEntity)
	 */
	protected void doInsert(final T_Entity entity) throws Exception {
		this.getApplicationEao().insert(entity);
	}

	/**
	 * <p>Metodo che aggiorna un dto e restituisce l'entity aggiornata.</p>
	 *
	 * <p>Non può essere esposto perché le interfacce ejb dei service <b>non devono</b> avere nella firma entity.</p>
	 *
	 * @param dto dato da aggiornare.
	 * @return l'entity aggiornata.
	 */
	@NonNull
	protected T_Entity updateEntity(final T_Dto dto, final CheckedConsumer<T_Dto, Exception> validator, final DtoValidator<T_Dto, T_Entity> dtoValidator, final PreUpdateHandler<T_Dto, T_Entity> preUpdateHandler, final PostUpdateHandler<T_Dto, T_Entity> postUpdateHandler) throws Exception {
		final T_Entity entity = this.findEntity(dto);
		if (entity == null) {
			throw new Exception("Entity non trovata, dto = " + dto);
		}
		validator.accept(dto);
		// controllo il dato prelevato dal database
		this.checkPermission(entity);
		// effettuo delle validazioni tra dto e entity corrente
		dtoValidator.validateDto(dto, entity);
		// compilo il dato
		this.compileEntityOtherFields(dto, entity);
		// controllo il dato appena compilato
		this.checkPermission(entity);
		preUpdateHandler.preUpdate(dto, entity);
		final T_Entity updatedEntity = this.doUpdate(entity);
		postUpdateHandler.postUpdate(dto, entity);
		return updatedEntity;
	}

	@NonNull
	protected T_Entity updateEntity(final T_Dto dto) throws Exception {
		return this.updateEntity(dto, this::validateUpdateDto, this.defaultServiceRWEnhancer, this.defaultServiceRWEnhancer, this.defaultServiceRWEnhancer);
	}

	/**
	 * Aggiorna un dato nel database a partire dal dto.
	 */
	public void update(final T_Dto dto, final CheckedConsumer<T_Dto, Exception> validator, final DtoValidator<T_Dto, T_Entity> dtoValidator, final PreUpdateHandler<T_Dto, T_Entity> preUpdateHandler, final PostUpdateHandler<T_Dto, T_Entity> postUpdateHandler) throws Exception {
		this.updateEntity(dto, validator, dtoValidator, preUpdateHandler, postUpdateHandler);
	}

	/**
	 * Aggiorna un dato nel database a partire dal dto.
	 */
	public void update(final T_Dto dto) throws Exception {
		this.updateEntity(dto);
	}

	/**
	 * @see #doDelete(IEntity)
	 */
	protected T_Entity doUpdate(final T_Entity entity) throws Exception {
		return this.getApplicationEao().update(entity);
	}

	/**
	 * Elimina un dato dal database a partire dal dto.
	 */
	public void delete(final T_Dto dto) throws Exception {
		this.delete(dto, this.defaultServiceRWEnhancer, this.defaultServiceRWEnhancer);
	}

	/**
	 * Elimina un dato dal database a partire dal dto.
	 */
	public void delete(final T_Dto dto, final PreDeleteHandler<T_Entity> preDeleteHandler, final PostDeleteHandler<T_Entity> postDeleteHandler) throws Exception {
		if (dto == null) {
			throw new Exception("DTO == null");
		}
		this.delete(dto.getReference(), preDeleteHandler, postDeleteHandler);
	}

	/**
	 * Elimina un dato dal database a partire dalla reference del dto.
	 */
	public void delete(final T_DtoReference reference, final PreDeleteHandler<T_Entity> preDeleteHandler, final PostDeleteHandler<T_Entity> postDeleteHandler) throws Exception {
		this.delete(this.findEntity(reference), preDeleteHandler, postDeleteHandler);
	}

	/**
	 * Elimina un dato dal database a partire dalla reference del dto.
	 */
	public void delete(final T_DtoReference reference) throws Exception {
		this.delete(reference, this.defaultServiceRWEnhancer, this.defaultServiceRWEnhancer);
	}

	public void delete(final T_Entity entity, final PreDeleteHandler<T_Entity> preDeleteHandler, final PostDeleteHandler<T_Entity> postDeleteHandler) throws Exception {
		if (entity == null) {
			// l'entity non esiste più, non importa
			return;
		}
		this.checkPermission(entity);
		preDeleteHandler.preDelete(entity);
		this.doDelete(entity);
		postDeleteHandler.postDelete(entity);
	}

	public void delete(final T_Entity entity) throws Exception {
		this.delete(entity, this.defaultServiceRWEnhancer, this.defaultServiceRWEnhancer);
	}

	/**
	 * Effettua l'effettiva cancellazione invocando l'{@code EntityManager}:
	 *
	 * <pre>
	 * this.getApplicationEao().delete(entity);
	 * </pre>
	 *
	 * Può essere reimplementato per effettuare una cancellazione logica invece:
	 *
	 * <pre>
	 * entity.setDeleted(true);
	 * </pre>
	 *
	 * Oppure per invocare il {@code GenericJpaCrudEAO} di riferimento:
	 *
	 * <pre>
	 * myEao.delete(entity);
	 * </pre>
	 */
	protected void doDelete(final T_Entity entity) throws Exception {
		this.getApplicationEao().delete(entity);
	}

	/**
	 * Verifica se l'utente corrente ha i permessi
	 * necessari per la scrittura dell'entity
	 * specificata.
	 *
	 * @param entity entity da cui effettuare la verifica.
	 * @return {@code true} se l'utente ha i permessi
	 *         necessari, altrimenti {@code false}.
	 * @see #checkPermission(IEntity)
	 */
	public boolean hasWritePermission(final T_Entity entity) {
		final String roleW = this.getRoleW(entity);
		final String roleWU = this.getRoleWU(entity);
		if (roleW != null) {
			if (this.isCallerInRole(roleW)) {
				return true;
			} else if (roleWU == null) {
				LOGGER.error("checkPermission entityClass = {}, entityId = {}, roleWU == null)", entity.getClass(), entity.getIdAsString());
				return false;
			}
		}
		if (roleWU != null) {
			if (!this.isCallerInRole(roleWU)) {
				LOGGER.error("checkPermission entityClass = {}, entityId = {}, !eao.isCallerInRole({})", entity.getClass(), entity.getIdAsString(), roleWU);
				return false;
			}
			final String currentUser = this.getLogin();
			final Set<String> owners = this.getOwners(entity);
			if (!this.checkOwnership(entity, currentUser, owners)) {
				LOGGER.error("checkPermission entityClass = {}, entityId = {}, user = {}, owners = {}", entity.getClass(), entity.getIdAsString(), currentUser, owners);
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifica se l'utente corrente ha il permesso
	 * di scrivere sull'entity specificata richiamando
	 * il metodo {@linkplain #hasWritePermission(IEntity)}.
	 *
	 * Se l'utente non ha il permesso viene propagata
	 * una {@linkplain AzSecurityServiceException}.
	 *
	 * @param entity entity da cui effettuare la verifica.
	 * @throws AzSecurityServiceException quando l'utente non ha
	 *             i permessi necessari.
	 * @see #hasWritePermission(IEntity)
	 */
	protected void checkPermission(final T_Entity entity) throws AzServiceSecurityException {
		if (!this.hasWritePermission(entity)) {
			throw new AzServiceSecurityException();
		}
	}

	/**
	 * @param entity dato interessato.
	 * @param currentUser utente corrente.
	 * @param owners utenti proprietari del dato.
	 * @return
	 */
	protected boolean checkOwnership(final T_Entity entity, final String currentUser, final Set<String> owners) {
		return owners.contains(currentUser);
	}

	/**
	 * Restituisce il detentore del dato.
	 *
	 * @param entity dato da verificare.
	 */
	protected Set<String> getOwners(final T_Entity entity) {
		assert false : "Questo metodo va reimplementato assieme a getRoleWU";
		throw new IllegalArgumentException("getRoleWU implementato ma getOwner non implementato");
	}

	/**
	 * Restituisce il nome del ruolo di scrittura su qualsiasi dato.
	 *
	 * @param entity entity che si sta scrivendo.
	 */
	protected String getRoleW(final T_Entity entity) {
		return null;
	}

	/**
	 * Restituisce il nome del ruolo di scrittura sui propri dati.
	 *
	 * @param entity entity che si sta scrivendo.
	 */
	protected String getRoleWU(final T_Entity entity) {
		return null;
	}

	/**
	 * <p>Salva un dato nel database.</p>
	 * <ul>
	 * <li>se esiste: viene aggiornato</li>
	 * <li>se non esiste: viene creato</li>
	 * </ul>
	 *
	 * @param dto dato da salvare.
	 * @return l'entity inserita/aggiornata.
	 */
	public T_Entity save(final T_Dto dto) throws Exception {
		final T_Entity entity = this.findEntity(dto);
		if (entity == null) {
			return this.insertEntity(dto);
		}
		return this.updateEntity(dto);
	}

	/**
	 * Validazione del dto prima dell'inserimento.
	 * <br/>
	 * Di default il dto viene validato con le annotazioni presenti sui campi.
	 *
	 * @param dto dto da validare.
	 * @throws AzServiceValidationException se fallisce la validazione.
	 */
	protected void validateInsertDto(final T_Dto dto) throws AzInvalidDataException, AzServiceValidationException {
		this.validateDto(dto);
	}

	/**
	 * Validazione del dto prima dell'aggiornamento.
	 * <br/>
	 * Di default il dto viene validato con le annotazioni presenti sui campi.
	 *
	 * @param dto dto da validare.
	 * @throws AzServiceValidationException se fallisce la validazione.
	 */
	protected void validateUpdateDto(final T_Dto dto) throws AzInvalidDataException, AzServiceValidationException {
		this.validateDto(dto);
	}

	private void validateDto(final T_Dto dto) throws AzInvalidDataException, AzServiceValidationException {
		if (useLegacyValidation) {
			final List<String> errors = DtoUtils.checkData(dto);
			if (!errors.isEmpty()) {
				throw new AzServiceValidationException(errors);
			}
		} else {
			final List<ValidationError> errors = DtoUtils.validate(dto);
			if (!errors.isEmpty()) {
				throw new AzInvalidDataException(errors);
			}
		}
	}

	/**
	 * Effettua una validazione dei dati del dto contro
	 * quelli della entity appena prelevata dal database.
	 * <br/>
	 * Può tornare utile per campi soggetti a concorrenza.
	 *
	 * @param dto dato sorgente.
	 * @param entity entity prelevata dal database e non
	 *            ancora compilata.
	 */
	protected void validateUpdate(final T_Dto dto, final T_Entity entity) throws Exception {}

	/**
	 * Metodo invocato prima di un inserimento.
	 *
	 * @param dto dato sorgente.
	 * @param entity entity compilata e pronta per il salvataggio.
	 */
	protected void preInsert(final T_Dto dto, final T_Entity entity) throws Exception {}

	/**
	 * Metodo invocato prima di una modifica.
	 *
	 * @param dto dato sorgente.
	 * @param entity entity compilata e pronta per il salvataggio.
	 */
	protected void preUpdate(final T_Dto dto, final T_Entity entity) throws Exception {}

	/**
	 * Metodo invocato prima di una cancellazione.
	 *
	 * @param entity entity pronta per la cancellazione.
	 */
	protected void preDelete(final T_Entity entity) throws Exception {}

	/**
	 * Metodo invocato dopo di un inserimento.
	 *
	 * @param dto dato sorgente.
	 * @param entity entity inserita.
	 */
	protected void postInsert(final T_Dto dto, final T_Entity entity) throws Exception {}

	/**
	 * Metodo invocato dopo di una modifica.
	 *
	 * @param dto dato sorgente.
	 * @param entity entity passata per l'aggiornamento.
	 */
	protected void postUpdate(final T_Dto dto, final T_Entity entity) throws Exception {}

	/**
	 * Metodo invocato dopo di una cancellazione.
	 *
	 * @param entity entity cancellata.
	 */
	protected void postDelete(final T_Entity entity) throws Exception {}
}
