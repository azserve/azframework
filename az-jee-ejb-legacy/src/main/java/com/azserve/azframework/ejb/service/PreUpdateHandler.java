package com.azserve.azframework.ejb.service;

import com.azserve.azframework.dto.DtoWithKey;
import com.azserve.azframework.entity.IEntity;

@FunctionalInterface
public interface PreUpdateHandler<T_Dto extends DtoWithKey<?, ?, ?>, T_Entity extends IEntity<?>> {

	void preUpdate(T_Dto dto, T_Entity entity) throws Exception;
}
