package com.azserve.azframework.ejb.service;

import com.azserve.azframework.dto.DtoReferenceWithString;
import com.azserve.azframework.dto.DtoWithString;
import com.azserve.azframework.dto.HasInsertId;
import com.azserve.azframework.entity.EntityWithString;

public abstract class ServiceRWWithString<T_Entity extends EntityWithString, T_Dto extends DtoWithString<T_Dto>>
		extends ServiceRW<T_Entity, String, T_Dto, String, DtoReferenceWithString<T_Dto>> {

	@SuppressWarnings("unchecked")
	@Override
	protected void compilePrimaryKeyEntity(final T_Dto dto, final T_Entity entity) {
		final DtoReferenceWithString<T_Dto> dtoReference = dto.getReference();
		if (dtoReference == null) {
			if (dto instanceof HasInsertId) {
				entity.setId(((HasInsertId<String>) dto).getInsertId());
			} else {
				entity.setId(this.getDefaultId());
			}
		} else {
			entity.setId(dtoReference.getId());
		}
	}

	@Override
	public String createPrimaryKey(final String key) {
		return key;
	}

	@Override
	public DtoReferenceWithString<T_Dto> createReference(final T_Entity entity) {
		return entity == null ? null : new DtoReferenceWithString<>(entity.getId());
	}

	protected String getDefaultId() {
		return null;
	}
}
