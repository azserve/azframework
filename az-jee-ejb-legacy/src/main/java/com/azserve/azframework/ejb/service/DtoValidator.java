package com.azserve.azframework.ejb.service;

import com.azserve.azframework.dto.DtoWithKey;
import com.azserve.azframework.entity.IEntity;

@FunctionalInterface
public interface DtoValidator<T_Dto extends DtoWithKey<?, ?, ?>, T_Entity extends IEntity<?>> {

	void validateDto(T_Dto dto, T_Entity entity) throws Exception;
}
