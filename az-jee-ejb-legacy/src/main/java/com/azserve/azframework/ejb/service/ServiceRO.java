package com.azserve.azframework.ejb.service;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.dto.DtoReference;
import com.azserve.azframework.dto.DtoWithKey;
import com.azserve.azframework.ejb.eao.ApplicationEAO;
import com.azserve.azframework.entity.IEntity;
import com.azserve.azframework.query.Expressions;
import com.azserve.azframework.query.ISingleQuery;
import com.azserve.azframework.query.Queries;
import com.azserve.azframework.query.criteria.IRoot;

/**
 * Service base per la gestione di dati in sola lettura.
 *
 * @param <T_Entity> entity da gestire.
 * @param <T_kEntity> chiave (id) dell'entity da gestire.
 * @param <T_Dto> dto da gestire.
 * @param <T_DtoReferenceKey> chiave (key) del dto da gestire.
 * @param <T_DtoReference> reference del dto da gestire.
 */
public abstract class ServiceRO<T_Entity extends IEntity<T_kEntity>, T_kEntity extends Serializable, T_Dto extends DtoWithKey<?, T_DtoReferenceKey, T_DtoReference>, T_DtoReferenceKey extends Serializable, T_DtoReference extends DtoReference<? super T_Dto, T_DtoReferenceKey>>
		extends EntityDTOFiller<T_Entity, T_kEntity, T_Dto, T_DtoReferenceKey, T_DtoReference> {

	/**
	 * Cerca la entity a partire dalla referenza specificata e restituisce il dto corrispondente.
	 * <br/>
	 * Se l'entity non viene trovata restituisce {@code null}.
	 */
	public T_Dto findDto(final T_DtoReference reference) {
		if (reference == null) {
			return null;
		}
		final T_kEntity pk = this.createPrimaryKey(reference);
		final T_Entity entity = this.findEntityById(pk);
		return this.getDto(entity);
	}

	/**
	 * Cerca la entity corrispondente al dto,
	 * restituisce {@code null} se non la trova.
	 */
	public T_Entity findEntity(final T_Dto dto) {
		if (dto == null) {
			return null;
		}
		final T_DtoReference dtoReference = dto.getReference();
		return this.findEntity(dtoReference);
	}

	/**
	 * Cerca la entity a partire dalla referenza specificata.
	 * <br/>
	 * Se l'entity non viene trovata restituisce {@code null}.
	 */
	public T_Entity findEntity(final T_DtoReference dtoReference) {
		if (dtoReference == null) {
			return null;
		}
		final T_kEntity pk = this.createPrimaryKey(dtoReference);
		return this.findEntityById(pk);
	}

	/**
	 * Restituisce il riferimento alla entity
	 *
	 * L'oggetto entity restituito avrà solo l'id valorizzato, tutti gli altri campi sono {@code null}
	 *
	 * @param dtoReference
	 * @return
	 */
	public T_Entity createEntityReference(final T_DtoReference dtoReference) {
		if (dtoReference == null) {
			return null;
		}
		final T_kEntity pk = this.createPrimaryKey(dtoReference);
		return this.getEntityManager().getReference(this.getEntityClass(), pk);
	}

	/**
	 * Restituisce una lista di dto corrispondenti a tutte le entity della tabella.
	 *
	 * @deprecated da implementare solo quando serve
	 * @see #findAllEntities()
	 */
	@Deprecated
	public List<T_Dto> findAll() {
		return this.getDtoList(this.findAllEntities());
	}

	/**
	 * Cerca un entity per id.
	 */
	public T_Entity findEntityById(final T_kEntity id) {
		return this.getApplicationEao().find(this.getEntityClass(), id);
	}

	/**
	 * Cerca un entity per id e restituisce il dto corrispondente.
	 */
	public T_Dto findDtoById(final T_kEntity id) {
		return this.getDto(this.findEntityById(id));
	}

	/**
	 * Cerca un dto per reference,
	 * il metodo non può essere final per compatibilità con wildfly 8.2,
	 * può essere reso final una volta risolto il problema
	 */
	public T_Dto findDtoByKey(final T_DtoReferenceKey key) {
		if (key == null) {
			return null;
		}
		final T_kEntity pk = this.createPrimaryKey(key);
		final T_Entity entity = this.findEntityById(pk);
		return this.getDto(entity);
	}

	// metodi accessori

	/**
	 * Restituisce la classe dell'entity gestita.
	 *
	 * @return classe dell'entity gestita.
	 */
	protected abstract Class<T_Entity> getEntityClass();

	/**
	 * Restituisce l'{@code EAO} di riferimento per
	 * le operazioni sulle entity.
	 */
	protected abstract ApplicationEAO getApplicationEao();

	/**
	 * Restituisce l'{@code EntityManager} per le
	 * operazioni sulle entity.
	 */
	protected final EntityManager getEntityManager() {
		return this.getApplicationEao().getEntityManager();
	}

	/**
	 * Verifica se l'utente corrente ha il permesso indicato.
	 *
	 * @param role permesso da verificare.
	 */
	protected final boolean isCallerInRole(final String role) {
		return this.getApplicationEao().getSessionContext().isCallerInRole(role);
	}

	/**
	 * Restituisce l'utente corrente.
	 */
	protected final String getLogin() {
		return this.getApplicationEao().getSessionContext().getCallerPrincipal().getName();
	}

	// metodi per query

	/**
	 * Esegue una query che restituisce tutte le entity.
	 *
	 * @deprecated da implementare solo quando serve
	 */
	@Deprecated
	public List<T_Entity> findAllEntities() {
		return this.buildQuery().fetch();
	}

	/**
	 * Restituisce una factory per generare query
	 * sull'entity gestita.
	 *
	 * <p>Per applicare dei filtri di default è preferibile estendere
	 * il metodo {@linkplain #buildQuery(IRoot)} utilizzando
	 * il parametro {@code root} per ottenere i campi</p>
	 */
	public ISingleQuery<T_Entity, T_Entity> buildQuery() {
		final IRoot<T_Entity> root = Expressions.from(this.getEntityClass());
		return this.buildQuery(root);
	}

	/**
	 * Restituisce una factory per generare query
	 * sull'entity gestita.
	 */
	public @NonNull ISingleQuery<T_Entity, T_Entity> buildQuery(final IRoot<T_Entity> root) {
		return Queries.create(this.getEntityManager())
				.from(root).select(this.getEntityClass(), root);
	}
}
