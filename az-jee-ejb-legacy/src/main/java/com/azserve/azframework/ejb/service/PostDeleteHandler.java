package com.azserve.azframework.ejb.service;

import com.azserve.azframework.entity.IEntity;

@FunctionalInterface
public interface PostDeleteHandler<T_Entity extends IEntity<?>> {

	void postDelete(T_Entity entity) throws Exception;
}
