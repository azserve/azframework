package com.azserve.azframework.dto;

import java.util.Objects;

/**
 * Data Transfer Object con chiave.
 */
public class DtoWithKey<D extends Dto, K, R extends DtoReference<D, K>> implements Dto {

	private static final long serialVersionUID = -3716820606644291269L;

	private R reference;

	protected DtoWithKey() {}

	protected DtoWithKey(final R id) {
		this.reference = id;
	}

	/**
	 * Restituisce la reference di questo {@code DTO}.
	 */
	public R getReference() {
		return this.reference;
	}

	/**
	 * Restituisce la chiave contenuta nella
	 * reference di questo {@code DTO}.
	 * Se la reference è {@code null} viene
	 * restituito {@code null}.
	 */
	public K getId() {
		return this.reference == null ? (K) null : this.reference.getId();
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		if (this.reference == null || ((DtoWithKey<?, ?, ?>) obj).reference == null) {
			return false;
		}
		return Objects.equals(this.reference, ((DtoWithKey<?, ?, ?>) obj).reference);
	}

	@Override
	public int hashCode() {
		return this.reference == null ? 0 : this.reference.hashCode();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ",ref=" + this.reference;
	}
}
