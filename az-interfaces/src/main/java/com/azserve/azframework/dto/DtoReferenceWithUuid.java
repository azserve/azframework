package com.azserve.azframework.dto;

import java.util.UUID;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Oggetto che serve a racchiudere la chiave di un {@code DtoWithUuid}.
 *
 * @param <D> tipo del dto.
 */
public class DtoReferenceWithUuid<D extends DtoWithUuid<D>> extends DtoReference<D, UUID> {

	private static final long serialVersionUID = 926635774339007814L;

	protected DtoReferenceWithUuid() {
		super();
	}

	public DtoReferenceWithUuid(final @NonNull UUID id) {
		super(id);
	}
}
