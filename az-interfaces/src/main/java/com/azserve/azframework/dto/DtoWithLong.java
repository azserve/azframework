package com.azserve.azframework.dto;

/**
 * Data Transfer Object con chiave di tipo long.
 */
public class DtoWithLong<D extends DtoWithLong<D>> extends DtoWithKey<D, Long, DtoReferenceWithLong<D>> {

	private static final long serialVersionUID = 951210959671517022L;

	protected DtoWithLong() {
		super(null);
	}

	protected DtoWithLong(final Long id) {
		super(new DtoReferenceWithLong<D>(id));
	}

	protected DtoWithLong(final DtoReferenceWithLong<D> reference) {
		super(reference);
	}
}
