package com.azserve.azframework.dto;

/**
 * Data Transfer Object con chiave di tipo stringa.
 */
public class DtoWithString<D extends DtoWithString<D>> extends DtoWithKey<D, String, DtoReferenceWithString<D>> {

	private static final long serialVersionUID = 951210959671517022L;

	protected DtoWithString() {
		super(null);
	}

	protected DtoWithString(final String id) {
		super(new DtoReferenceWithString<D>(id));
	}

	protected DtoWithString(final DtoReferenceWithString<D> reference) {
		super(reference);
	}
}
