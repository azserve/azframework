package com.azserve.azframework.dto;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Oggetto che serve a racchiudere la chiave di un {@code DtoWithString}.
 *
 * @param <D> tipo del dto.
 */
public class DtoReferenceWithString<D extends DtoWithString<D>> extends DtoReference<D, String> {

	private static final long serialVersionUID = -3652131357431256774L;

	protected DtoReferenceWithString() {
		super();
	}

	public DtoReferenceWithString(final @NonNull String id) {
		super(id);
	}

	@Override
	protected int hashCodeId() {
		return this.getId().toLowerCase().trim().hashCode();
	}

	@Override
	protected boolean equalsId(final Object other) {
		return other instanceof String && this.getId().toLowerCase().trim().equals(((String) other).toLowerCase().trim());
	}
}
