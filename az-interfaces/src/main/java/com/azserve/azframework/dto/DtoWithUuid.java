package com.azserve.azframework.dto;

import java.util.UUID;

/**
 * Data Transfer Object con chiave di tipo UUID.
 */
public abstract class DtoWithUuid<D extends DtoWithUuid<D>> extends DtoWithKey<D, UUID, DtoReferenceWithUuid<D>> {

	private static final long serialVersionUID = -555695435670136550L;

	protected DtoWithUuid() {
		super(null);
	}

	protected DtoWithUuid(final UUID id) {
		super(new DtoReferenceWithUuid<D>(id));
	}

	protected DtoWithUuid(final DtoReferenceWithUuid<D> reference) {
		super(reference);
	}
}
