package com.azserve.azframework.dto;

import java.io.Serializable;
import java.util.Objects;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Oggetto che serve a racchiudere la chiave di un {@code Dto}.
 *
 * @param <D> tipo del dto.
 * @param <K> tipo di chiave del dto.
 */
public class DtoReference<D extends Dto, K> implements Serializable {

	private static final long serialVersionUID = 2538378520796246528L;

	private K id;

	protected DtoReference() {}

	public DtoReference(final @NonNull K id) {
		this.id = Objects.requireNonNull(id, "Chiave obbligatoria");
	}

	/**
	 * Restituisce la chiave contenuta.
	 * Non è mai {@code null}.
	 */
	public @NonNull K getId() {
		return this.id;
	}

	@Override
	public String toString() {
		return this.id.toString();
	}

	@Override
	public int hashCode() {
		return this.hashCodeId();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		return this.equalsId(((DtoReference<?, ?>) obj).id);
	}

	protected int hashCodeId() {
		return this.id.hashCode();
	}

	protected boolean equalsId(final Object other) {
		return this.id.equals(other);
	}
}
