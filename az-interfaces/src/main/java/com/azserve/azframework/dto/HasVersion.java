package com.azserve.azframework.dto;

public interface HasVersion<V extends Comparable<? super V>> {

	V getVersion();

	void setVersion(V version);

}
