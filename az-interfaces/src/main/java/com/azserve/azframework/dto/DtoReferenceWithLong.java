package com.azserve.azframework.dto;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Oggetto che serve a racchiudere la chiave di un {@code DtoWithLong}.
 *
 * @param <D> tipo del dto.
 */
public class DtoReferenceWithLong<D extends DtoWithLong<D>> extends DtoReference<D, Long> {

	private static final long serialVersionUID = -3652131357431256774L;

	protected DtoReferenceWithLong() {
		super();
	}

	public DtoReferenceWithLong(final @NonNull Long id) {
		super(id);
	}
}
