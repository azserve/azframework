package com.azserve.azframework.dto;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Oggetto che serve a racchiudere la chiave di un {@code DtoWithInteger}.
 *
 * @param <D> tipo del dto.
 */
public class DtoReferenceWithInteger<D extends DtoWithInteger<D>> extends DtoReference<D, Integer> {

	private static final long serialVersionUID = -7191658974183485229L;

	protected DtoReferenceWithInteger() {
		super();
	}

	public DtoReferenceWithInteger(final @NonNull Integer id) {
		super(id);
	}
}
