package com.azserve.azframework.dto;

import java.io.Serializable;
import java.lang.reflect.Constructor;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Interfaccia implementata dai &quot;Data Transfer Objects&quot;.
 */
public interface Dto extends Serializable {

	/**
	 * Restituisce un nuovo {@code DTO} della
	 * classe specificata.
	 *
	 * @param dtoClass classe del {@code DTO} da creare.
	 * @throws RuntimeException se il {@code DTO} non ha
	 *             un costruttore pubblico senza parametri.
	 */
	@SuppressWarnings("unchecked")
	@NonNull
	static <D extends Dto> D of(final Class<D> dtoClass) {
		try {
			final Constructor<?> constr = dtoClass.getConstructor();
			return (D) constr.newInstance();
		} catch (final ReflectiveOperationException roex) {
			throw new RuntimeException("Cannot find an empty constructor for " + dtoClass, roex);
		}
	}
}
