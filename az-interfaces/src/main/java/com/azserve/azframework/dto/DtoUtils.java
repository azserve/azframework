package com.azserve.azframework.dto;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.lang.ObjectUtils;
import com.azserve.azframework.common.lang.StringUtils;
import com.azserve.azframework.common.util.BeanProperty;
import com.azserve.azframework.validation.ValidationError;

/**
 * Classe di utilità per i {@code DTO}.
 *
 * @author filippo
 */
public final class DtoUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(DtoUtils.class);

	private static final ValidatorFactory VALIDATION_FACTORY = Validation.buildDefaultValidatorFactory();

	private DtoUtils() {}

	/**
	 * Restituisce un {@code Set} di reference a partire da una {@code Collection} di {@code DTO}.
	 * Se il {@code Set} è {@code null} restituisce {@code null}.
	 */
	public static <T extends Dto, T_Dto extends DtoWithKey<T, K, R>, K, R extends DtoReference<T, K>> Set<R> getReferences(final @Nullable Collection<T_Dto> dtos) {
		return dtos != null ? dtos.stream().map(DtoUtils::getReference).collect(Collectors.toSet()) : null;
	}

	/**
	 * Restituisce un {@code Set} di {@code DTO} a partire da una {@code Collection} di reference.
	 * Se il {@code Set} è {@code null} restituisce {@code null}.
	 */
	public static <T extends Dto, T_Dto extends DtoWithKey<T, K, R>, K, R extends DtoReference<T, K>> Set<T_Dto> toDtos(final @Nullable Collection<R> references, final @NonNull Function<R, T_Dto> toDtoFunction) {
		return references != null ? references.stream().map(toDtoFunction).collect(Collectors.toSet()) : null;
	}

	/**
	 * Restituisce la reference di un {@code DTO}.
	 * Se il {@code DTO} è {@code null} restituisce {@code null}.
	 */
	public static <T extends Dto, T_Dto extends DtoWithKey<T, K, R>, K, R extends DtoReference<T, K>> R getReference(final @Nullable T_Dto dto) {
		return dto != null ? dto.getReference() : null;
	}

	/**
	 * Restituisce un {@code Set} di chiavi a partire da una {@code Collection} di reference.
	 * Se il {@code Set} è {@code null} restituisce {@code null}.
	 */
	public static <K> Set<K> getKeys(final @Nullable Collection<? extends DtoReference<?, K>> references) {
		return references != null ? references.stream().map(DtoUtils::getKey).collect(Collectors.toSet()) : null;
	}

	/**
	 * Restituisce un {@code Set} di chiavi a partire da una {@code Collection} di {@code DTP}.
	 * Se la {@code Collection} è {@code null} restituisce {@code null}.
	 */
	public static <T extends Dto, D extends DtoWithKey<T, K, ?>, K> Set<K> getDtoKeys(final @Nullable Collection<D> dtos) {
		return dtos != null ? dtos.stream().map(DtoUtils::getKey).collect(Collectors.toSet()) : null;
	}

	/**
	 * Restituisce la chiave di una reference.
	 * Se la reference è {@code null} restituisce {@code null}.
	 */
	public static <K> K getKey(final @Nullable DtoReference<?, K> reference) {
		return reference != null ? reference.getId() : null;
	}

	/**
	 * Restituisce la chiave di un {@code DTO}.
	 * Se il {@code DTO} è {@code null} restituisce {@code null}.
	 */
	public static <T extends Dto, T_Dto extends DtoWithKey<T, K, ?>, K> K getKey(final @Nullable T_Dto dto) {
		return dto != null ? dto.getId() : null;
	}

	/**
	 * Genera una reference di tipo {@code UUID}.
	 *
	 * @return una reference al valore specificato,
	 *         {@code null} se il valore specificato è {@code null}.
	 */
	public static <T_Dto extends DtoWithUuid<T_Dto>> DtoReferenceWithUuid<T_Dto> createReference(final @Nullable UUID key) {
		return key != null ? new DtoReferenceWithUuid<>(key) : null;
	}

	/**
	 * Genera una reference di tipo {@code String}.
	 *
	 * @return una reference al valore specificato,
	 *         {@code null} se il valore specificato è {@code null}.
	 */
	public static <T_Dto extends DtoWithString<T_Dto>> DtoReferenceWithString<T_Dto> createReference(final @Nullable String key) {
		return key != null ? new DtoReferenceWithString<>(key) : null;
	}

	/**
	 * Genera una reference di tipo {@code Integer}.
	 *
	 * @return una reference al valore specificato,
	 *         {@code null} se il valore specificato è {@code null}.
	 */
	public static <T_Dto extends DtoWithInteger<T_Dto>> DtoReferenceWithInteger<T_Dto> createReference(final @Nullable Integer key) {
		return key != null ? new DtoReferenceWithInteger<>(key) : null;
	}

	/**
	 * Genera una reference di tipo {@code Long}.
	 *
	 * @return una reference al valore specificato,
	 *         {@code null} se il valore specificato è {@code null}.
	 */
	public static <T_Dto extends DtoWithLong<T_Dto>> DtoReferenceWithLong<T_Dto> createReference(final @Nullable Long key) {
		return key != null ? new DtoReferenceWithLong<>(key) : null;
	}

	/**
	 * Effettua delle validazioni sui campi del oggetto specificato
	 * utilizzando le annotazioni {@code javax.validation}.
	 *
	 * @param data l'oggetto da validare.
	 * @param groups i gruppi oggetto di validazione.
	 * @return la lista degli errori in validazione.
	 */
	public static @NonNull <T> List<String> checkData(final @NonNull T data, final Class<?>... groups) {
		final Validator validator = VALIDATION_FACTORY.getValidator();
		final Set<ConstraintViolation<T>> violations = validator.validate(data, groups);

		if (violations.isEmpty()) {
			// non uso emptylist perché così posso fare add se serve
			return new ArrayList<>();
		}

		final List<String> result = new ArrayList<>(violations.size());
		for (final ConstraintViolation<T> constraintViolation : violations) {
			final String messageTemplate = constraintViolation.getMessageTemplate();
			final String message;
			if (messageTemplate.startsWith("{javax.validation.constraints.") && messageTemplate.endsWith(".message}")) {
				message = StringUtils.splitCamelCase(constraintViolation.getPropertyPath() + "") + " " + constraintViolation.getMessage();
			} else {
				message = constraintViolation.getMessage();
			}
			result.add(message);
		}
		return result;
	}

	public static @NonNull <T> List<ValidationError> validate(final @NonNull T data, final Class<?>... groups) {
		final Validator validator = VALIDATION_FACTORY.getValidator();
		final Set<ConstraintViolation<T>> violations = validator.validate(data, groups);

		if (violations.isEmpty()) {
			// non uso emptylist perché così posso fare add se serve
			return new ArrayList<>();
		}
		return violations.stream().map(ValidationError::ofConstraintViolation).collect(Collectors.toCollection(ArrayList::new));
	}

	public static @NonNull <T, V> List<ValidationError> validateValue(final @NonNull Class<T> dataClass, final @NonNull BeanProperty<?, ?, T, V> property, final V value, final Class<?>... groups) {
		final Validator validator = VALIDATION_FACTORY.getValidator();
		final String propertyName = property.getName();
		final Set<ConstraintViolation<T>> violations = validator.validateValue(dataClass, propertyName, value, groups);

		if (violations.isEmpty()) {
			// non uso emptylist perché così posso fare add se serve
			return new ArrayList<>();
		}
		return violations.stream().map(c -> ValidationError.ofConstraintViolation(c, dataClass, propertyName)).collect(Collectors.toCollection(ArrayList::new));
	}

	@Deprecated
	public static <T extends DtoWithKey<T, ?, R>, R extends DtoReference<T, ?>> T copyDto(final @Nullable T data) {
		if (data == null) {
			return null;
		}
		final R reference = data.getReference();
		@SuppressWarnings("rawtypes")
		final Class<? extends DtoWithKey> dataClass = data.getClass();

		try {
			@SuppressWarnings("unchecked")
			final T t2 = (T) (reference == null ? dataClass.getConstructor().newInstance() : dataClass.getConstructor(reference.getClass()).newInstance(reference));
			ObjectUtils.copyObject(DtoWithKey.class, data, t2);
			return t2;
		} catch (final InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
			LOGGER.error("copyDto()", ex);
			throw new RuntimeException(ex);
		}
	}

	@Deprecated
	public static <T extends DtoWithKey<T, ?, R>, R extends DtoReference<T, ?>> List<T> deepCopyDtoList(final @NonNull List<T> list) {
		final List<T> result = new ArrayList<>(list.size());
		for (final T t : list) {
			result.add(copyDto(t));
		}
		return result;
	}
}