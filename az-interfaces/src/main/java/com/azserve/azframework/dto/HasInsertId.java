package com.azserve.azframework.dto;

/**
 * Interfaccia implementata dai dto che necessitano
 * di una chiave definita in inserimento.
 *
 * @param <T> tipo di chiave.
 */
public interface HasInsertId<T> {

	/**
	 * Indica la chiave da impostare
	 * in inserimento.
	 */
	void setInsertId(T insertId);

	/**
	 * Restituisce la chiave indicata da impostare
	 * in inserimento.
	 */
	T getInsertId();

	// questo metodo serve per forzare il dto a tipizzare l'interfaccia HasInsertId come la DtoReference
	T getId();
}
