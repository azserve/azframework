package com.azserve.azframework.dto;

/**
 * Data Transfer Object con chiave di tipo intero.
 */
public class DtoWithInteger<D extends DtoWithInteger<D>> extends DtoWithKey<D, Integer, DtoReferenceWithInteger<D>> {

	private static final long serialVersionUID = 7459469513673286979L;

	protected DtoWithInteger() {
		super(null);
	}

	protected DtoWithInteger(final Integer id) {
		super(new DtoReferenceWithInteger<>(id));
	}

	protected DtoWithInteger(final DtoReferenceWithInteger<D> id) {
		super(id);
	}
}
