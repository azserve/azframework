package com.azserve.azframework.exception;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Eccezione generica per mostrare un messaggio d'errore all'utente.
 * <br>
 * I costruttori accettano un oggetto {@linkplain ExceptionKey} che
 * fornisce il metodo {@linkplain ExceptionKey#getMessageKey getMessageKey} il quale deve restituire il
 * nome della chiave per andare a leggere il messaggio corrispondente
 * nel file di proprietà.
 * <br>
 * Le chiavi dei messaggi generici delle eccezioni sono date
 * dall'enum {@linkplain FrameworkExceptionKey} ed è possibile sovrascriverle
 * con una propria implementazione di {@linkplain ExceptionKey}.
 * </p>
 *
 * @author filippo
 */
public class AzGenericException extends Exception {

	private static final long serialVersionUID = 3187406181903034804L;

	private final @NonNull ExceptionKey key;
	private final ExceptionParams params;

	public AzGenericException(final @NonNull ExceptionKey key) {
		this(key, null);
	}

	public AzGenericException(final @NonNull ExceptionKey key, final ExceptionParams params) {
		this.key = key;
		this.params = params;
	}

	public AzGenericException(final String loggingMessage, final @NonNull ExceptionKey key) {
		this(loggingMessage, key, null);
	}

	public AzGenericException(final String loggingMessage, final @NonNull ExceptionKey key, final ExceptionParams params) {
		super(loggingMessage);
		this.key = key;
		this.params = params;
	}

	/**
	 * Fornisce un oggetto che identifica
	 * il tipo di eccezione.
	 */
	public @NonNull ExceptionKey getKey() {
		return this.key;
	}

	/**
	 * Fornisce una mappa in sola lettura contenente
	 * eventuali parametri associati all'eccezione.
	 */
	public @NonNull Map<String, Serializable> getParams() {
		if (this.params == null) {
			return Collections.emptyMap();
		}
		return Collections.unmodifiableMap(this.params.getAll());
	}

	@Override
	public String getMessage() {
		final String message = super.getMessage();
		return message != null ? message : this.getKey().getMessageKey();
	}
}
