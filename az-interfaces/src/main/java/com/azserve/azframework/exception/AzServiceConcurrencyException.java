package com.azserve.azframework.exception;

import javax.ejb.ApplicationException;

/**
 * Eccezione utilizzata all'interno dei service in caso
 * di errori di concorrenza in fase di salvataggio.
 *
 * @author filippo
 */
@ApplicationException(inherited = true, rollback = true)
public class AzServiceConcurrencyException extends AzServiceException {

	private static final long serialVersionUID = 6544497851139194495L;

	public AzServiceConcurrencyException() {
		super(FrameworkExceptionKey.CONCURRENCY);
	}
}
