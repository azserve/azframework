package com.azserve.azframework.exception;

/**
 * Eccezione utilizzata all'interno dei service in caso
 * di errori di sicurezza (permessi insufficenti, ecc..)
 *
 * @author filippo
 */
public class AzServiceSecurityException extends AzServiceException {

	private static final long serialVersionUID = -8928639443478850028L;

	public AzServiceSecurityException() {
		super(FrameworkExceptionKey.SECURITY);
	}
}
