package com.azserve.azframework.exception;

import javax.ejb.ApplicationException;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Eccezione utilizzata all'interno dei service.
 * <br>
 * In caso di propagazione viene effettuato il rollback
 * della transazione in corso.
 *
 * @author filippo
 */
@ApplicationException(inherited = true, rollback = true)
public class AzServiceException extends AzGenericException {

	private static final long serialVersionUID = -1058031931621235218L;

	public AzServiceException(final @NonNull ExceptionKey key) {
		super(key);
	}

	public AzServiceException(final String logMessage, final @NonNull ExceptionKey key) {
		super(logMessage, key);
	}

	public AzServiceException(final ExceptionKey key, final ExceptionParams params) {
		super(key, params);
	}

	public AzServiceException(final String loggingMessage, final ExceptionKey key, final ExceptionParams params) {
		super(loggingMessage, key, params);
	}
}
