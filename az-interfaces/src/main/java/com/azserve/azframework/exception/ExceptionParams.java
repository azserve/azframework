package com.azserve.azframework.exception;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Objects;

public class ExceptionParams implements Serializable {

	private static final long serialVersionUID = 7558098029670260563L;

	private HashMap<String, Serializable> parameters;

	public static ExceptionParams of(final String key, final Serializable value) {
		return new ExceptionParams().put(key, value);
	}

	public static ExceptionParams of(final String key, final int value) {
		return of(key, Integer.valueOf(value));
	}

	public static ExceptionParams of(final String key, final boolean value) {
		return of(key, Boolean.valueOf(value));
	}

	//

	public ExceptionParams put(final String key, final Serializable value) {
		Objects.requireNonNull(key, "Key cannot be null");
		this.getAll().put(key, value);
		return this;
	}

	public ExceptionParams put(final String key, final int value) {
		return this.put(key, Integer.valueOf(value));
	}

	public ExceptionParams put(final String key, final boolean value) {
		return this.put(key, Boolean.valueOf(value));
	}

	public HashMap<String, Serializable> getAll() {
		if (this.parameters == null) {
			this.parameters = new HashMap<>();
		}
		return this.parameters;
	}
}
