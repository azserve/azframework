package com.azserve.azframework.exception;

import java.io.Serializable;

/**
 * Interfaccia che fornisce una chiave che
 * rappresenta il tipo di eccezione.
 *
 * @author filippo
 */
public interface ExceptionKey extends Serializable {

	String getMessageKey();

	static ExceptionKey of(String messageKey) {
		return () -> messageKey;
	}
}
