package com.azserve.azframework.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.interfaces.service.Communication;

/**
 * Eccezione utilizzata all'interno dei service che
 * fornisce una lista di dettagli dell'errore.
 *
 * @author filippo
 */
public class AzServiceDetailsException extends AzServiceException {

	private static final long serialVersionUID = 7181611810400169234L;

	private final @NonNull List<Communication> details;

	public static AzServiceDetailsException ofDetails(final @NonNull ExceptionKey key, final @NonNull List<String> details) {
		return new AzServiceDetailsException(key, details.stream().map(d -> new Communication(null, d))
				.collect(Collectors.toCollection(ArrayList::new)));
	}

	public AzServiceDetailsException(final @NonNull ExceptionKey exceptionKey, final @NonNull List<Communication> details) {
		super(exceptionKey);
		this.details = details;
	}

	public @NonNull List<Communication> getDetails() {
		return this.details;
	}
}
