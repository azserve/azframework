package com.azserve.azframework.exception;

import java.util.Collections;
import java.util.List;

import javax.ejb.ApplicationException;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.validation.ValidationError;

/**
 * Eccezione propagata quando ci sono errori di validazione di dati.
 *
 * @author filippo
 */
@ApplicationException(inherited = true, rollback = true)
public class AzInvalidDataException extends AzServiceException {

	private static final long serialVersionUID = 830466656698395111L;

	private final @NonNull List<ValidationError> details;

	public AzInvalidDataException(final @NonNull ValidationError detail) {
		this(Collections.singletonList(detail));
	}

	public AzInvalidDataException(final @NonNull List<ValidationError> details) {
		super(FrameworkExceptionKey.VALIDATION);
		this.details = Collections.unmodifiableList(details);
	}

	public AzInvalidDataException(final String message, final @NonNull List<ValidationError> details) {
		super(message, FrameworkExceptionKey.VALIDATION);
		this.details = Collections.unmodifiableList(details);
	}

	public @NonNull List<ValidationError> getDetails() {
		return this.details;
	}

	@Override
	public String getMessage() {
		if (this.details.isEmpty()) {
			return super.getMessage();
		}
		final StringBuilder message = new StringBuilder()
				.append(super.getMessage())
				.append("\n");
		for (final ValidationError error : this.details) {
			message.append(toLogString(error)).append(", ");
		}
		return message.substring(0, message.length() - 2).toString();
	}

	private static String toLogString(final ValidationError t) {
		final String message = t.getMessage();
		final String propertyPath = t.getPropertyPath();
		if (propertyPath != null) {
			return propertyPath
					.replace(".<list element>", "")
					.replace(".<iterable element>", "")
					.replace(".<map key>", "")
					.replace(".<map value>", "")
					+ " " + (message != null ? message : t.getMessageKey());
		}
		return message != null ? message : t.getMessageKey();
	}
}
