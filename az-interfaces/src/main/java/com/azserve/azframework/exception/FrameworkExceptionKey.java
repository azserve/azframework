package com.azserve.azframework.exception;

/**
 * Enumerazione dei tipi di eccezioni propagate dal framework.
 *
 * @author filippo
 */
public enum FrameworkExceptionKey implements ExceptionKey {
	SECURITY("com.azserve.azframework.exception.security"),
	VALIDATION("com.azserve.azframework.exception.validation"),
	CONCURRENCY("com.azserve.azframework.exception.version");

	private final String messageKey;

	FrameworkExceptionKey(final String messageKey) {
		this.messageKey = messageKey;
	}

	@Override
	public String getMessageKey() {
		return this.messageKey;
	}
}
