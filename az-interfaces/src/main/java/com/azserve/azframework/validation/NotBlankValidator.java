package com.azserve.azframework.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.azserve.azframework.common.lang.StringUtils;

public class NotBlankValidator implements ConstraintValidator<NotBlank, String> {

	@Override
	public void initialize(final NotBlank annotation) {}

	@Override
	public boolean isValid(final String value, final ConstraintValidatorContext constraintValidatorContext) {
		return !StringUtils.isNullOrBlank(value);
	}
}
