package com.azserve.azframework.validation;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.Path.Node;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.funct.Functions;
import com.azserve.azframework.common.reflect.ReflectionUtils;
import com.azserve.azframework.exception.ExceptionKey;

public class ValidationError implements Serializable {

	private static final long serialVersionUID = 6777883084213788889L;

	public static @NonNull ValidationError of(final String key) {
		return of(null, key);
	}

	public static @NonNull ValidationError of(final Serializable rootBean, final String key) {
		return new ValidationError(rootBean, () -> key);
	}

	private final Annotation annotation;
	private final com.azserve.azframework.validation.Field fieldAnnotation;
	private final boolean field;
	private final String fullFieldName;
	private final String message;
	private final String messageKey;
	private final String propertyPath;
	private final Serializable invalidValue;
	private final Serializable rootBean;
	private final Map<String, Serializable> messageParams;

	public static ValidationError ofConstraintViolation(final @NonNull ConstraintViolation<?> constraintViolation) {
		return ofConstraintViolation1(constraintViolation, null, null);
	}

	public static ValidationError ofConstraintViolation(final @NonNull ConstraintViolation<?> constraintViolation, final @NonNull Class<?> beanClass, final @NonNull String fieldName) {
		return ofConstraintViolation1(constraintViolation, beanClass, fieldName);
	}

	private static ValidationError ofConstraintViolation1(final @NonNull ConstraintViolation<?> constraintViolation, final Class<?> beanClass, final String fieldName) {
		com.azserve.azframework.validation.Field f = null;
		boolean isField = false;
		final Path propertyPath2 = constraintViolation.getPropertyPath();
		String fullFieldName = null;
		if (fieldName != null) {
			final Field field = ReflectionUtils.findField(beanClass, fieldName);
			isField = field != null;
			if (field != null) {
				f = field.getAnnotation(com.azserve.azframework.validation.Field.class);
				fullFieldName = beanClass.getCanonicalName() + "." + field.getName();
			}
		} else {
			final Object leafBean = constraintViolation.getLeafBean();
			if (leafBean != null) {
				Node last = null;
				for (final Node node : propertyPath2) {
					last = node;
				}
				if (last != null) {
					try {
						final Class<? extends Object> leafBeanClass = leafBean.getClass();
						final Field beanField = ReflectionUtils.findField(leafBeanClass, last.getName());
						f = beanField.getAnnotation(com.azserve.azframework.validation.Field.class);
						fullFieldName = leafBeanClass.getCanonicalName() + "." + beanField.getName();
						isField = true;
					} catch (final Exception e1) {}
				}
			}
		}

		final String messageTemplate = constraintViolation.getMessageTemplate();
		final Object invalidValue = constraintViolation.getInvalidValue();
		final Object rootBean = constraintViolation.getRootBean();

		return new ValidationError(constraintViolation.getConstraintDescriptor().getAnnotation(),
				f, isField, fullFieldName, constraintViolation.getMessage(),
				messageTemplate.startsWith("{") && messageTemplate.endsWith("}") ? messageTemplate.substring(1, messageTemplate.length() - 1) : null,
				Objects.toString(propertyPath2, null),
				invalidValue instanceof Serializable ? (Serializable) invalidValue : null,
				rootBean instanceof Serializable ? (Serializable) rootBean : null,
				constraintViolation.getConstraintDescriptor().getAttributes().entrySet().stream()
						.filter(e -> e.getValue() instanceof Serializable)
						.collect(Collectors.toMap(Entry::getKey, e -> (Serializable) e.getValue(), Functions.acceptFirst(), HashMap::new)));
	}

	public ValidationError(final Serializable rootBean, final @NonNull ExceptionKey messageKey) {
		this(rootBean, null, messageKey);
	}

	public ValidationError(final Serializable rootBean, final Serializable invalidValue, final @NonNull ExceptionKey messageKey) {
		this(rootBean, null, null, invalidValue, messageKey);
	}

	public ValidationError(final Serializable rootBean, final com.azserve.azframework.validation.Field field, final String fullFieldName, final Serializable invalidValue, final @NonNull ExceptionKey messageKey) {
		this(null, field, field != null, fullFieldName, null, messageKey.getMessageKey(), null, invalidValue, rootBean, new HashMap<>());
	}

	private ValidationError(final Annotation annotation, final com.azserve.azframework.validation.Field fieldAnnotation, final boolean field, final String fullFieldName, final String message, final String messageKey, final String propertyPath, final Serializable invalidValue, final Serializable rootBean, final Map<String, Serializable> messageParams) {
		this.annotation = annotation;
		this.fieldAnnotation = fieldAnnotation;
		this.field = field;
		this.fullFieldName = fullFieldName;
		this.message = message;
		this.messageKey = messageKey;
		this.propertyPath = propertyPath;
		this.invalidValue = invalidValue;
		this.rootBean = rootBean;
		this.messageParams = messageParams;
	}

	public Annotation getAnnotation() {
		return this.annotation;
	}

	public boolean isField() {
		return this.field;
	}

	public String getFullFieldName() {
		return this.fullFieldName;
	}

	public String getMessage() {
		return this.message;
	}

	public String getMessageKey() {
		return this.messageKey;
	}

	public String getPropertyPath() {
		return this.propertyPath;
	}

	public Serializable getInvalidValue() {
		return this.invalidValue;
	}

	public Serializable getRootBean() {
		return this.rootBean;
	}

	public Map<String, Serializable> getMessageParams() {
		return this.messageParams;
	}

	public com.azserve.azframework.validation.Field getFieldAnnotation() {
		return this.fieldAnnotation;
	}

	public ValidationError addMessageParam(final String key, final Serializable value) {
		this.messageParams.put(key, value);
		return this;
	}
}
