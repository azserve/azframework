package com.azserve.azframework.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.temporal.ChronoUnit;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {
		TemporalRangeValidatorForDate.class,
		TemporalRangeValidatorForLocalDate.class,
		TemporalRangeValidatorForLocalDateTime.class,
		TemporalRangeValidatorForLocalTime.class
})
@Documented
public @interface TemporalRange {

	String message() default "{com.azserve.azframework.validation.TemporalRange.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	/**
	 * Indica di quante unità temporali (inclusive) controllare
	 * nel passato. Un numero negativo significa infinito.
	 */
	int back() default -1;

	/**
	 * Indica di quante unità temporali (inclusive) controllare
	 * nel futuro. Un numero negativo significa infinito.
	 */
	int forward() default -1;

	/**
	 * L'unità temporale.
	 */
	ChronoUnit unit() default ChronoUnit.DAYS;
}
