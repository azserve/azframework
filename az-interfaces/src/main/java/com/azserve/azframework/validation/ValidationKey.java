package com.azserve.azframework.validation;

public interface ValidationKey {

	String getMessageKey();

}
