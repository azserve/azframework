package com.azserve.azframework.validation;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class TemporalRangeValidatorForLocalTime extends TemporalRangeValidator<LocalTime, LocalTime> {

	@Override
	public void initialize(final TemporalRange annotation) {
		if (!annotation.unit().isTimeBased()) {
			throw new IllegalArgumentException("Invalid unit " + annotation.unit());
		}
		super.initialize(annotation);
	}

	@Override
	protected LocalTime convert(final LocalTime value) {
		return value;
	}

	@Override
	protected LocalTime getCurrent(final ChronoUnit unit) {
		return LocalTime.now();
	}

	@Override
	protected LocalTime calculateMin(final LocalTime current, final int units, final ChronoUnit unit) {
		return current.minus(units, unit);
	}

	@Override
	protected LocalTime calculateMax(final LocalTime current, final int units, final ChronoUnit unit) {
		return current.plus(units, unit);
	}
}
