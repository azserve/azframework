package com.azserve.azframework.validation;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class TemporalRangeValidatorForLocalDate extends TemporalRangeValidator<LocalDate, LocalDate> {

	@Override
	public void initialize(final TemporalRange annotation) {
		if (!annotation.unit().isDateBased()) {
			throw new IllegalArgumentException("Invalid unit " + annotation.unit());
		}
		super.initialize(annotation);
	}

	@Override
	protected LocalDate convert(final LocalDate value) {
		return value;
	}

	@Override
	protected LocalDate getCurrent(final ChronoUnit unit) {
		return LocalDate.now();
	}

	@Override
	protected LocalDate calculateMin(final LocalDate current, final int units, final ChronoUnit unit) {
		return current.minus(units, unit);
	}

	@Override
	protected LocalDate calculateMax(final LocalDate current, final int units, final ChronoUnit unit) {
		return current.plus(units, unit);
	}
}
