package com.azserve.azframework.interfaces.service.util;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ManagerFactory {

	private ManagerFactory() {}

	private static final Logger LOGGER = LoggerFactory.getLogger(ManagerFactory.class);

	@SuppressWarnings("unchecked")
	public static <T> T getBeanReference(final String jndiGlobalName, final String moduleName, final String simpleName, final Class<T> interfaceName) {
		try {
			final InitialContext ic = new InitialContext();
			final String s = "java:global/" + jndiGlobalName + "/" + moduleName + "/" + simpleName + "!" + interfaceName.getCanonicalName();
			LOGGER.info(s);
			return (T) (ic.lookup(s));
		} catch (final NamingException e) {
			LOGGER.error("getBeanReference", e);
		}
		return (T)null;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getLocalBeanReference(final String moduleName, final String simpleName, final Class<T> interfaceName) {
		try {
			final InitialContext ic = new InitialContext();
			final String s = "java:app/" + moduleName + "/" + simpleName + "!" + interfaceName.getCanonicalName();
			LOGGER.info(s);
			return (T) (ic.lookup(s));
		} catch (final NamingException e) {
			LOGGER.error("getLocalBeanReference", e);
		}
		return (T)null;
	}

}
