package com.azserve.azframework.interfaces.service;

import java.io.Serializable;

/**
 * Oggetto contenente una comunicazione
 * con codice e messaggio.
 *
 * @author filippo
 * @since 11/04/2018
 */
public class Communication implements Serializable {

	private static final long serialVersionUID = -253125261944800673L;

	private String code;
	private String message;

	protected Communication() {}

	public Communication(final String code, final String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return this.code;
	}

	public String getMessage() {
		return this.message;
	}

	@Override
	public String toString() {
		return this.code == null ? this.message : this.code + " - " + this.message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.code == null ? 0 : this.code.hashCode());
		result = prime * result + (this.message == null ? 0 : this.message.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Communication other = (Communication) obj;
		if (this.code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!this.code.equals(other.code)) {
			return false;
		}
		if (this.message == null) {
			if (other.message != null) {
				return false;
			}
		} else if (!this.message.equals(other.message)) {
			return false;
		}
		return true;
	}

}