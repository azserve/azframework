package com.azserve.azframework.interfaces.service.property;

import com.azserve.azframework.common.enums.EnumWithKey;

public interface IPropertyKey extends EnumWithKey<String> {

	Class<?> getValueClass();

	Object getDefaultValue();
}