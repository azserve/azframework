package com.azserve.azframework.common.xml;

import static java.util.Objects.requireNonNull;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import com.azserve.azframework.common.annotation.NonNull;

public final class XmlUtils {

	private XmlUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	public static String sgmlEncode(final String s, final int limit) {
		if (s == null) {
			return null;
		}
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length();) {
			final int code = s.codePointAt(i);
			if (code < limit) {
				sb.append((char) code);
			} else {
				sb.append("&#" + code + ";");
			}
			i += Character.charCount(code);
		}
		return sb.toString();
	}

	public static String sgmlEncode(final String s) {
		return sgmlEncode(s, 0x0100);
	}

	private static final Map<Class<?>, JAXBContext> CONTEXTS = new HashMap<>();

	public static <T> XmlManager<T> of(final @NonNull Class<T> rootClass, final Class<?>... classesToBeBound) throws JAXBException {
		requireNonNull(rootClass, "Class required");
		synchronized (CONTEXTS) {
			JAXBContext jaxbContext = CONTEXTS.get(rootClass);
			if (jaxbContext == null) {
				if (classesToBeBound.length == 0) {
					jaxbContext = JAXBContext.newInstance(rootClass);
				} else {
					final Class<?>[] classes = new Class[1 + classesToBeBound.length];
					classes[0] = rootClass;
					for (int i = 0; i < classesToBeBound.length; i++) {
						classes[i + 1] = classesToBeBound[i];
					}
					jaxbContext = JAXBContext.newInstance(classes);
				}
				CONTEXTS.put(rootClass, jaxbContext);
			}
			return new XmlManager<>(jaxbContext);
		}
	}

	public static final class XmlManager<T> {

		private final JAXBContext context;
		private Map<String, Object> marshallerProperties;
		private Map<String, Object> unmarshallerProperties;
		private Schema schema;

		public XmlManager(final @NonNull JAXBContext context) {
			this.context = context;
		}

		public XmlManager<T> withformattedOutput() {
			return this.withMarshallerProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		}

		public XmlManager<T> withMarshallerProperty(final String key, final Object value) {
			if (this.marshallerProperties == null) {
				this.marshallerProperties = new HashMap<>();
			}
			this.marshallerProperties.put(key, value);
			return this;
		}

		public XmlManager<T> withUnmarshallerProperty(final String key, final Object value) {
			if (this.unmarshallerProperties == null) {
				this.unmarshallerProperties = new HashMap<>();
			}
			this.unmarshallerProperties.put(key, value);
			return this;
		}

		public XmlManager<T> withSchema(final @NonNull URL url) throws SAXException {
			requireNonNull(url, "Schema URL cannot be null");
			final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			this.schema = schemaFactory.newSchema(url);
			return this;
		}

		public String marshal(final @NonNull T obj) throws JAXBException, PropertyException {
			requireNonNull(obj, "Object required");
			final StringWriter sw = new StringWriter();
			final Marshaller marshaller = this.createMarshaller();
			marshaller.marshal(obj, sw);
			return sw.toString();
		}

		public void marshal(final @NonNull T obj, final @NonNull OutputStream os) throws JAXBException, PropertyException {
			requireNonNull(obj, "Object required");
			requireNonNull(os, "Output stream required");
			this.createMarshaller().marshal(obj, os);
		}

		public String marshal(final @NonNull JAXBElement<T> obj) throws JAXBException, PropertyException {
			requireNonNull(obj, "JAXB element required");
			final StringWriter sw = new StringWriter();
			final Marshaller marshaller = this.createMarshaller();
			marshaller.marshal(obj, sw);
			return sw.toString();
		}

		public void marshal(final @NonNull JAXBElement<T> obj, final @NonNull OutputStream os) throws JAXBException, PropertyException {
			requireNonNull(obj, "JAXB element required");
			requireNonNull(os, "Output stream required");
			this.createMarshaller().marshal(obj, os);
		}

		@SuppressWarnings("unchecked")
		public T unmarshal(final @NonNull InputStream is) throws JAXBException, PropertyException {
			requireNonNull(is, "Source xml required");
			final Unmarshaller unmarshaller = this.createUnmarshaller();
			return (T) JAXBIntrospector.getValue(unmarshaller.unmarshal(is));
		}

		public T unmarshal(final @NonNull String xml) throws JAXBException, PropertyException {
			requireNonNull(xml, "Source xml required");
			return this.unmarshal(new ByteArrayInputStream(xml.getBytes()));
		}

		public Marshaller createMarshaller() throws JAXBException, PropertyException {
			final Marshaller marshaller = this.context.createMarshaller();
			if (this.marshallerProperties != null) {
				for (final Entry<String, Object> entry : this.marshallerProperties.entrySet()) {
					marshaller.setProperty(entry.getKey(), entry.getValue());
				}
			}
			marshaller.setSchema(this.schema);
			return marshaller;
		}

		public Unmarshaller createUnmarshaller() throws JAXBException, PropertyException {
			final Unmarshaller unmarshaller = this.context.createUnmarshaller();
			if (this.unmarshallerProperties != null) {
				for (final Entry<String, Object> entry : this.unmarshallerProperties.entrySet()) {
					unmarshaller.setProperty(entry.getKey(), entry.getValue());
				}
			}
			unmarshaller.setSchema(this.schema);
			return unmarshaller;
		}
	}
}