package com.azserve.framework.presenter.display;

public interface DisplayMessage {

	enum Type {
		ERROR,
		WARN,
		INFO,
		SUCCESS
	}

	String key();

	Type type();
}
