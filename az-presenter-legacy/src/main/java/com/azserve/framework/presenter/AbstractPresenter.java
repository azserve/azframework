package com.azserve.framework.presenter;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.framework.presenter.display.Display;

public abstract class AbstractPresenter<D extends Display> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPresenter.class);

	private D display;

	/**
	 * Inizializza il display specificato.
	 *
	 * @param display display da gestire.
	 */
	public final void setDisplay(final @NonNull D display) {
		this.display = display;
		this.postInit();
	}

	/**
	 * Imposta dei parametri dall'interfaccia grafica.
	 *
	 * @param parameters parametri del display.
	 */
	public final void setParameters(final @NonNull Map<String, String> parameters) {
		if (LOGGER.isInfoEnabled() && !parameters.isEmpty()) {
			LOGGER.info("Presenter parameters : {}", parameters);
		}
		this.manageParameters(parameters);
	}

	/**
	 * Effettua delle operazioni di inizializzazione del display.
	 */
	protected abstract void postInit();

	/**
	 * Gestisce i parametri ricevuti in ingresso dall'interfaccia grafica.
	 *
	 * @param parameters parametri del display.
	 */
	protected void manageParameters(final @NonNull Map<String, String> parameters) {}

	protected final D getDisplay() {
		return this.display;
	}

	/**
	 * Effettua delle operazioni alla chiusura del display.
	 *
	 * @param closeCallback interfaccia da invocare per
	 *            la chiusura del display.
	 * @return {@code true} se il display può essere
	 *         chiuso subito, {@code false} se il display
	 *         non può essere chiuso per interagire con l'utente.
	 */
	public boolean closing(final @NonNull Runnable closeCallback) {
		return true;
	}
}
