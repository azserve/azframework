package com.azserve.framework.presenter.display;

import java.io.InputStream;
import java.io.Serializable;

public class UploadData implements Serializable {

	private static final long serialVersionUID = -2088617895043319378L;

	private final String fileName;
	private final String mimeType;
	private InputStream content;
	private long contentLength;

	public UploadData(final String fileName, final String mimeType) {
		super();
		this.fileName = fileName;
		this.mimeType = mimeType;
	}

	public InputStream getContent() {
		return this.content;
	}

	public void setContent(final InputStream content) {
		this.content = content;
	}

	public long getContentLength() {
		return this.contentLength;
	}

	public void setContentLength(final long contentLength) {
		this.contentLength = contentLength;
	}

	public String getFileName() {
		return this.fileName;
	}

	public String getMimeType() {
		return this.mimeType;
	}

}