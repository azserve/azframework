package com.azserve.framework.presenter.display;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.funct.BooleanConsumer;
import com.azserve.framework.presenter.DataPresenter.Action;

public interface DataDisplay<Q, D, DE extends DataEditDisplay<D>> extends Display {

	void setDataList(@NonNull List<Q> list);

	void setOnInsert(@NonNull Runnable callback);

	void setOnEdit(@NonNull Runnable callback);

	void setOnView(@NonNull Runnable callback);

	void setOnDelete(@NonNull Runnable callback);

	DE createEditDisplay(@NonNull D dto, boolean newData, boolean readOnly);

	void setOnClose(@NonNull Consumer<DE> callback);

	void closeEditDisplay(@NonNull DE editDisplay);

	void select(@NonNull DE newDataEditDisplay);

	void showValidationErrors(@NonNull D dto, @NonNull List<String> errors);

	Q getSelectedItem();

	Set<Q> getSelectedItems();

	void askDelete(@NonNull Q qdto, @NonNull BooleanConsumer callback);

	void showNotAllowedWarning(@NonNull Action action, @NonNull Q qdto);

	void showSaveError(@NonNull Exception ex);

	void showDeleteError(@NonNull Exception ex);
}
