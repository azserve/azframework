package com.azserve.framework.presenter;

import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.enums.EnumUtils;
import com.azserve.azframework.dto.DtoReference;
import com.azserve.azframework.dto.DtoWithKey;
import com.azserve.framework.presenter.display.DataDisplay;
import com.azserve.framework.presenter.display.DataEditDisplay;

public abstract class DataPresenter<Q extends DtoWithKey<D, ?, R>, D extends DtoWithKey<D, ?, R>, R extends DtoReference<D, ?>, E extends DataDisplay<Q, D, DE>, DE extends DataEditDisplay<D>>
		extends AbstractPresenter<E> {

	public static final String PARAM_ACTION = "action";
	public static final String PARAM_ID = "id";

	public enum Action {

		INSERT,
		EDIT,
		VIEW,
		DELETE;

		public boolean isWrite() {
			return Action.VIEW != this;
		}
	}

	private DE newDataEditDisplay = null;

	private final Map<Q, DE> listObjDisplayMap = new HashMap<>();
	private final Map<DE, D> displayDataObjMap = new IdentityHashMap<>();

	public void loadDataList() {
		this.getDisplay().setDataList(this.loadData());
	}

	protected abstract @NonNull List<Q> loadData();

	protected abstract D findDto(R reference);

	protected abstract @NonNull D prepareNewDto();

	protected abstract void insert(@NonNull D dto) throws Exception;

	protected abstract void update(@NonNull D dto) throws Exception;

	protected abstract void delete(@NonNull R reference) throws Exception;

	/**
	 * @param action
	 * @param qdto
	 * @return
	 */
	public boolean isAllowed(final @NonNull Action action, final @NonNull Q qdto) {
		return true;
	}

	@Override
	protected void manageParameters(final @NonNull Map<String, String> parameters) {
		super.manageParameters(parameters);
		final String actionName = parameters.get(PARAM_ACTION);
		if (actionName != null) {
			final Action action = EnumUtils.safeValueOf(Action.class, actionName.toUpperCase());
			if (action != null) {
				if (Action.INSERT == action) {
					this.insert();
				} else {
					final String dataId = parameters.get(PARAM_ID);
					if (dataId != null) {
						final Q qdto = this.parseDataId(dataId);
						if (qdto != null) {
							switch (action) {
								case EDIT:
									this.view(false, qdto);
									break;
								case VIEW:
									this.view(true, qdto);
									break;
								case INSERT:
									// l'inserimento è già gestito
								case DELETE:
								default:
									// non permetto cancellazioni da parametro
									break;
							}
						}
					}
				}
			}
		} else {
			this.loadDataList();
		}
	}

	@Override
	public void postInit() {
		if (this.isAllowed(Action.INSERT)) {
			this.getDisplay().setOnInsert(this::insert);
		}
		if (this.isAllowed(Action.EDIT)) {
			this.getDisplay().setOnEdit(() -> this.view(false));
		}
		if (this.isAllowed(Action.VIEW)) {
			this.getDisplay().setOnView(() -> this.view(true));
		}
		if (this.isAllowed(Action.DELETE)) {
			this.getDisplay().setOnDelete(this::delete);
		}
		this.getDisplay().setOnClose(this::close);

		this.postInitOther();
	}

	protected void postInitOther() {}

	/**
	 * @param dataId
	 * @return
	 */
	protected Q parseDataId(final @NonNull String dataId) {
		return null;
	}

	/**
	 * @param action
	 * @return
	 */
	protected boolean isAllowed(final @NonNull Action action) {
		return true;
	}

	/**
	 * @param editDisplay
	 * @param dto
	 */
	protected void preCompileFields(final @NonNull DE editDisplay, final @NonNull D dto) {}

	/**
	 * @param editDisplay
	 * @param dto
	 */
	protected void postCompileFields(final @NonNull DE editDisplay, final @NonNull D dto) {}

	protected void insert() {
		if (this.newDataEditDisplay != null) {
			this.getDisplay().select(this.newDataEditDisplay);
			return;
		}
		final D dto = this.prepareNewDto();
		final DE editDisplay = this.getDisplay().createEditDisplay(dto, true, false);
		editDisplay.setOnSave(() -> this.save(editDisplay));
		this.preCompileFields(editDisplay, dto);
		editDisplay.compileFields(dto, false);
		this.postCompileFields(editDisplay, dto);
		this.newDataEditDisplay = editDisplay;
		this.displayDataObjMap.put(this.newDataEditDisplay, dto);
	}

	protected void view(final boolean readOnly) {
		final Q qdto = this.getDisplay().getSelectedItem();
		if (qdto == null) {
			return;
		}
		this.view(readOnly, qdto);
	}

	protected void view(final boolean readOnly, final Q qdto) {
		if (readOnly) {
			if (!this.isAllowed(Action.VIEW, qdto)) {
				this.getDisplay().showNotAllowedWarning(Action.VIEW, qdto);
				return;
			}
		} else if (!this.isAllowed(Action.EDIT, qdto)) {
			this.getDisplay().showNotAllowedWarning(Action.EDIT, qdto);
			return;
		}

		DE editDisplay = this.listObjDisplayMap.get(qdto);
		if (editDisplay == null) {
			final D dto = this.findDto(qdto.getReference());
			final DE de = this.getDisplay().createEditDisplay(dto, false, readOnly);
			de.setOnSave(() -> this.save(de));
			editDisplay = de;
			this.preCompileFields(editDisplay, dto);
			editDisplay.compileFields(dto, readOnly);
			this.postCompileFields(editDisplay, dto);
			this.listObjDisplayMap.put(qdto, editDisplay);
			this.displayDataObjMap.put(editDisplay, dto);
		}
		this.getDisplay().select(editDisplay);
	}

	protected void save(final @NonNull DE editDisplay) {
		this.save(editDisplay, true);
	}

	protected void save(final @NonNull DE editDisplay, final boolean validate) {
		final D dto = this.displayDataObjMap.get(editDisplay);
		assert dto != null;
		try {
			if (validate) {
				// effettuo la validazione dell'oggetto
				if (!this.validate(dto)) {
					return;
				}
			}
			if (this.newDataEditDisplay == editDisplay) {
				this.insert(dto);
				this.newDataEditDisplay = null;
			} else {
				this.update(dto);
			}

			this.displayDataObjMap.remove(editDisplay);
			this.listObjDisplayMap.entrySet().removeIf(entry -> entry.getValue() == editDisplay);
			this.getDisplay().closeEditDisplay(editDisplay);
			this.loadDataList();
		} catch (final Exception ex) {
			this.getDisplay().showSaveError(ex);
		}
	}

	protected void delete() {
		final Q qdto = this.getDisplay().getSelectedItem();
		if (qdto == null) {
			return;
		}
		if (!this.isAllowed(Action.DELETE, qdto)) {
			this.getDisplay().showNotAllowedWarning(Action.DELETE, qdto);
			return;
		}
		final DE dataEditDisplay = this.listObjDisplayMap.get(qdto);
		if (dataEditDisplay != null) {
			return;
		}

		this.getDisplay().askDelete(qdto, response -> {
			if (response) {
				try {
					this.delete(qdto.getReference());
					this.loadDataList();
				} catch (final Exception e) {
					this.getDisplay().showDeleteError(e);
				}
			}
		});
	}

	protected void close(final @NonNull DE editDisplay) {
		// TODO, chiedere se dati non salvati
		this.getDisplay().closeEditDisplay(editDisplay);

		this.displayDataObjMap.remove(editDisplay);
		this.listObjDisplayMap.entrySet().removeIf(entry -> entry.getValue() == editDisplay);

		if (this.newDataEditDisplay == editDisplay) {
			this.newDataEditDisplay = null;
		}
	}

	/**
	 * @param dataObj
	 * @return
	 */
	protected boolean validate(final @NonNull D dataObj) {
		// final List<String> validationErrors = DtoUtils.checkData(dataObj);
		// if (!validationErrors.isEmpty()) {
		// this.getDisplay().showValidationErrors(dataObj, validationErrors);
		// return false;
		// }
		return true;
	}
}
