package com.azserve.framework.presenter.display;

import java.io.InputStream;
import java.util.concurrent.Future;
import java.util.function.Consumer;

import com.azserve.azframework.common.annotation.NonNull;

public interface Display {

	void showError(@NonNull Exception ex);

	void notifyError(@NonNull Exception ex);

	void showMessage(@NonNull DisplayMessage message, Object... args);

	void notifyMessage(@NonNull DisplayMessage message, Object... args);

	void open(InputStream source, String prefix, String extension, String mimeType);

	default void open(final InputStream source, final String prefix, final String extension) {
		this.open(source, prefix, extension, null);
	}

	/**
	 * Esegue un'operazione asincrona mostrando un testo se specificato.
	 *
	 * @param text testo da mostrare.
	 * @param operation operazione asincrona da eseguire.
	 * @param consumer operazioni da effettuare al termine
	 *            dell'operazione asincrona.
	 * @param errorHandler interfaccia di callback per la
	 *            gestione in caso di errore
	 *            dell'operazione asincrona.
	 */
	<T> void task(@NonNull String text, @NonNull Future<T> operation, @NonNull Consumer<T> consumer, @NonNull Consumer<Exception> errorHandler);

	/**
	 * Esegue un'operazione asincrona mostrando un testo se specificato.
	 *
	 * @param text testo da mostrare.
	 * @param operation operazione asincrona da eseguire.
	 * @param consumer operazioni da effettuare al termine
	 *            dell'operazione asincrona.
	 */
	default <T> void task(final @NonNull String text, final @NonNull Future<T> operation, final @NonNull Consumer<T> consumer) {
		task(text, operation, consumer, this::showError);
	}
}
