package com.azserve.framework.presenter.display;

public interface DataEditDisplay<T> {

	/**
	 * Compila i campi di input a partire dal dato.
	 *
	 * @param obj dato da visualizzare.
	 * @param readOnly indica se il dato è aperto
	 *            in sola lettura.
	 */
	void compileFields(T obj, boolean readOnly);

	void setOnSave(Runnable callback);
}
