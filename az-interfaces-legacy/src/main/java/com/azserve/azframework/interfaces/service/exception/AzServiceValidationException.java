package com.azserve.azframework.interfaces.service.exception;

import java.util.Collections;
import java.util.List;

/**
 * @deprecated utilizzare {@linkplain com.azserve.azframework.exception.AzInvalidDataException}
 */
@Deprecated
public class AzServiceValidationException extends AzServiceDetailsException {

	private static final long serialVersionUID = 1268187538523604588L;

	public AzServiceValidationException(final List<String> details) {
		super(DefaultExMessage.SERVICE_VALIDATION_FAILED, details);
	}

	public AzServiceValidationException(final ExceptionMessageKey msgKey, final Object... msgArgs) {
		super(msgKey, msgArgs);
	}

	public AzServiceValidationException(final String detail) {
		this(Collections.singletonList(detail));
	}
}
