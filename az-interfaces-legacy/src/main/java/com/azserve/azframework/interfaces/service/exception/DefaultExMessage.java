package com.azserve.azframework.interfaces.service.exception;

/**
 * Enum delle chiavi delle proprietà dei messaggi
 * di default.
 *
 * @since 18/10/2013
 */
public enum DefaultExMessage implements ExceptionMessageKey {

	SERVICE_NOT_AUTHORIZED,
	SERVICE_VALIDATION_FAILED;

	@Override
	public String getName() {
		return this.name();
	}
}
