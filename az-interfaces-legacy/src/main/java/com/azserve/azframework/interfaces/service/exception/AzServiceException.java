package com.azserve.azframework.interfaces.service.exception;

import javax.ejb.ApplicationException;

/**
 * Eccezione generata dai service per mostrare un messaggio d'errore all'utente.
 *
 * @deprecated utilizzare {@linkplain com.azserve.azframework.exception.AzServiceException}
 * @author filippo
 * @since 21/03/2013
 */
@Deprecated
@ApplicationException(inherited = true, rollback = true)
public class AzServiceException extends AzException {

	private static final long serialVersionUID = -3244551723408792392L;

	public static AzServiceException from(final com.azserve.azframework.exception.AzServiceException ex) {
		return new AzServiceException(ex.getKey()::getMessageKey, ex.getParams().values().toArray());
	}

	public AzServiceException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public AzServiceException(final String message) {
		super(message);
	}

	/**
	 * Costruisce un'eccezione con il messaggio
	 * e i parametri specificati.
	 *
	 * @param msgKey chiave da utilizzare per leggere
	 *            il messaggio da una risorsa.
	 * @param msgArgs argomenti da utilizzare
	 *            per compilare il messaggio.
	 */
	public AzServiceException(final ExceptionMessageKey msgKey, final Object... msgArgs) {
		super(msgKey, msgArgs);
	}

	/**
	 * Costruisce un'eccezione con il messaggio,
	 * i parametri e la causa specificati.
	 *
	 * @param msgKey chiave da utilizzare per leggere
	 *            il messaggio da una risorsa.
	 * @param cause la causa dell'eccezione.
	 * @param msgArgs argomenti da utilizzare
	 *            per compilare il messaggio.
	 */
	public AzServiceException(final ExceptionMessageKey msgKey, final Throwable cause, final Object... msgArgs) {
		super(msgKey, cause, msgArgs);
	}
}
