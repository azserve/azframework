package com.azserve.azframework.interfaces.service.exception;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

import com.azserve.azframework.common.annotation.Nullable;

/**
 * Eccezione generica per mostrare un messaggio d'errore all'utente.
 * <p>
 * I messaggi d'errore vengono letti da un file di proprietà che
 * deve chiamarsi "exceptionMessages.properties".
 * <br>
 * I costruttori accettano un oggetto {@code ExceptionMessageKey} che
 * fornisce il metodo {@code getName()} il quale deve restituire il
 * nome della chiave per andare a leggere il messaggio corrispondente
 * nel file di proprietà.
 * <br>
 * Le chiavi dei messaggi generici delle eccezioni sono date
 * dall'enum {@code DefaultExMessage} ed è possibile sovrascriverle
 * con una propria implementazione di {@code ExceptionMessageKey}.
 * </p>
 *
 * @author filippo
 * @since 07/05/2013
 */
public class AzException extends Exception {

	private static final long serialVersionUID = -2478957806002219501L;

	/** Nome del file contenente i messaggi delle eccezioni. */
	private static final String MESSAGES = "exceptionMessages";
	/** {@code ResourceBundle} dell'applicazione. */
	private static final ResourceBundle RESOURCE_BUNDLE;
	/** {@code ResourceBundle} del framework. */
	private static final ResourceBundle FRAMEWORK_RESOURCE_BUNDLE;

	static {
		final ClassLoader cl = Thread.currentThread().getContextClassLoader();
		RESOURCE_BUNDLE = ResourceBundle.getBundle(MESSAGES, Locale.getDefault(), cl);
		// FIXME viene caricata la stessa (per ora tutti i messaggi sono sul file di Aftcloud)
		FRAMEWORK_RESOURCE_BUNDLE = ResourceBundle.getBundle(MESSAGES);
	}

	private final ExceptionMessageKey msgKey;
	private final Object[] msgArgs;

	public AzException(final String message, final Throwable cause) {
		super(message, cause);
		this.msgArgs = null;
		this.msgKey = null;
	}

	public AzException(final String message) {
		super(message);
		this.msgArgs = null;
		this.msgKey = null;
	}

	/**
	 * Costruisce un'eccezione con il messaggio
	 * e i parametri specificati.
	 *
	 * @param msgKey chiave da utilizzare per leggere
	 *            il messaggio da una risorsa.
	 * @param msgArgs argomenti da utilizzare
	 *            per compilare il messaggio.
	 */
	public AzException(final ExceptionMessageKey msgKey, final Object... msgArgs) {
		this(msgKey, null, msgArgs);
	}

	/**
	 * Costruisce un'eccezione con il messaggio,
	 * i parametri e la causa specificati.
	 *
	 * @param msgKey chiave da utilizzare per leggere
	 *            il messaggio da una risorsa.
	 * @param cause la causa dell'eccezione.
	 * @param msgArgs argomenti da utilizzare
	 *            per compilare il messaggio.
	 */
	public AzException(final ExceptionMessageKey msgKey, final Throwable cause, final Object... msgArgs) {
		super(Objects.requireNonNull(msgKey).getName(), cause);
		this.msgKey = msgKey;
		this.msgArgs = msgArgs;
	}

	public @Nullable ExceptionMessageKey getMessageKey() {
		return this.msgKey;
	}

	public @Nullable Object[] getArgs() {
		return this.msgArgs;
	}

	/**
	 * Restituisce il messaggio a partire
	 * dai {@code ResourceBundle}.
	 * Se la proprietà viene trovata in quello
	 * dell'applicazione, viene restituito il
	 * messaggio corrispondente, altrimenti viene
	 * restituito il messaggio dal bundle del framework.
	 * Se la proprietà non esiste in entrambi i
	 * bundle viene generata una {@code MissingResourceException}.
	 */
	protected final String getLocalizedMessageFromResource() {
		final String key = this.msgKey.getName();
		if (RESOURCE_BUNDLE.containsKey(key)) {
			return RESOURCE_BUNDLE.getString(key);
		}
		return FRAMEWORK_RESOURCE_BUNDLE.getString(key);
	}

	@Override
	public String getLocalizedMessage() {
		if (this.msgKey == null) {
			return this.getMessage();
		}
		try {
			final String message = this.getLocalizedMessageFromResource();
			if (this.msgArgs.length == 0) {
				return message;
			}
			return MessageFormat.format(message, this.msgArgs);
		} catch (final @SuppressWarnings("unused") Exception ex) {
			return this.getMessage();
		}
	}
}
