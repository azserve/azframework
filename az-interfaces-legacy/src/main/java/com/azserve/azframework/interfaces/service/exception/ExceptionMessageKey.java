package com.azserve.azframework.interfaces.service.exception;

import java.io.Serializable;

public interface ExceptionMessageKey extends Serializable {

	String getName();

}
