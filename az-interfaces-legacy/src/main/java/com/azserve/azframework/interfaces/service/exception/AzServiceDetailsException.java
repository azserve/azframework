package com.azserve.azframework.interfaces.service.exception;

import java.util.Collections;
import java.util.List;

/**
 * Eccezione generata dai service per mostrare un messaggio d'errore all'utente
 * più altri messaggi di dettaglio.
 *
 * @deprecated utilizzare {@linkplain com.azserve.azframework.exception.AzServiceDetailsException}
 *
 * @author filippo
 * @since 26/07/2013
 */
@Deprecated
public class AzServiceDetailsException extends AzServiceException {

	private static final long serialVersionUID = 7181611810400169234L;

	private final List<String> details;

	public AzServiceDetailsException(final String message, final List<String> details) {
		super(message);
		this.details = details;
	}

	public AzServiceDetailsException(final ExceptionMessageKey msgKey, final List<String> details) {
		super(msgKey);
		this.details = details;
	}

	public AzServiceDetailsException(final ExceptionMessageKey msgKey, final Object... msgArgs) {
		super(msgKey, msgArgs);
		this.details = Collections.emptyList();
	}

	public List<String> getDetails() {
		return this.details;
	}

	@Override
	public String getLocalizedMessage() {
		if (this.details.isEmpty()) {
			return super.getLocalizedMessage();
		}
		return super.getLocalizedMessage() + "\n" + this.details;
	}
}
