package com.azserve.azframework.interfaces.service.exception;

/**
 * Eccezione generata quando l'utente non ha l'autorizzazione per
 * agire sui dati tramite le classi service.
 *
 * @deprecated utilizzare {@linkplain com.azserve.azframework.exception.AzServiceSecurityException}
 */
@Deprecated
public class AzSecurityServiceException extends AzServiceException {

	private static final long serialVersionUID = -2801678345171325890L;

	public AzSecurityServiceException(final String message) {
		super(message);
	}

	/**
	 * Costruisce un'eccezione con il messaggio
	 * e i parametri specificati.
	 *
	 * @param msgKey chiave da utilizzare per leggere
	 *            il messaggio da una risorsa.
	 * @param msgArgs argomenti da utilizzare
	 *            per compilare il messaggio.
	 */
	public AzSecurityServiceException(final ExceptionMessageKey msgKey, final Object... msgArgs) {
		super(msgKey, msgArgs);
	}

	/**
	 * Costruisce un'eccezione con il messaggio,
	 * i parametri e la causa specificati.
	 *
	 * @param msgKey chiave da utilizzare per leggere
	 *            il messaggio da una risorsa.
	 * @param cause la causa dell'eccezione.
	 * @param msgArgs argomenti da utilizzare
	 *            per compilare il messaggio.
	 */
	public AzSecurityServiceException(final ExceptionMessageKey msgKey, final Throwable cause, final Object... msgArgs) {
		super(msgKey, cause, msgArgs);
	}
}
