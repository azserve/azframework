package com.azserve.azframework.common.network;

import java.net.InetAddress;
import java.net.UnknownHostException;

public final class NetworkUtils {

	private static final int[] MASKS = {
			0b00000000_00000000_00000000_00000000,

			0b10000000_00000000_00000000_00000000,
			0b11000000_00000000_00000000_00000000,
			0b11100000_00000000_00000000_00000000,
			0b11110000_00000000_00000000_00000000,
			0b11111000_00000000_00000000_00000000,
			0b11111100_00000000_00000000_00000000,
			0b11111110_00000000_00000000_00000000,
			0b11111111_00000000_00000000_00000000,

			0b11111111_10000000_00000000_00000000,
			0b11111111_11000000_00000000_00000000,
			0b11111111_11100000_00000000_00000000,
			0b11111111_11110000_00000000_00000000,
			0b11111111_11111000_00000000_00000000,
			0b11111111_11111100_00000000_00000000,
			0b11111111_11111110_00000000_00000000,
			0b11111111_11111111_00000000_00000000,

			0b11111111_11111111_10000000_00000000,
			0b11111111_11111111_11000000_00000000,
			0b11111111_11111111_11100000_00000000,
			0b11111111_11111111_11110000_00000000,
			0b11111111_11111111_11111000_00000000,
			0b11111111_11111111_11111100_00000000,
			0b11111111_11111111_11111110_00000000,
			0b11111111_11111111_11111111_00000000,

			0b11111111_11111111_11111111_10000000,
			0b11111111_11111111_11111111_11000000,
			0b11111111_11111111_11111111_11100000,
			0b11111111_11111111_11111111_11110000,
			0b11111111_11111111_11111111_11111000,
			0b11111111_11111111_11111111_11111100,
			0b11111111_11111111_11111111_11111110,
			0b11111111_11111111_11111111_11111111,
	};

	private NetworkUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Verifica se l'indirizzo appartiene alla rete
	 *
	 * @param network rete
	 * @param mask maschera
	 * @param address indirizzo di riferimento
	 */
	public static boolean match(final String network, final short mask, final String address) throws UnknownHostException {
		return match(InetAddress.getByName(network), mask, InetAddress.getByName(address));
	}

	/**
	 * Verifica se l'indirizzo appartiene alla rete
	 *
	 * @param network rete
	 * @param mask maschera
	 * @param address indirizzo di riferimento
	 */
	public static boolean match(final InetAddress network, final short mask, final InetAddress address) {
		final int networkInt = asInt(network);
		final int addressInt = asInt(address);
		final int addressAndMask = addressInt & MASKS[mask];
		final int networkAndMask = networkInt & MASKS[mask];
		return addressAndMask == networkAndMask;
	}

	private static int asInt(final InetAddress ia) {
		final byte[] bs = ia.getAddress();
		int i = 0;
		for (final byte b : bs) {
			i <<= 8;
			i |= b & 255; // previene sign-extension
		}
		return i;
	}

}
