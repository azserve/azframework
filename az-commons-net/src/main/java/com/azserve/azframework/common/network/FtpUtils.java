package com.azserve.azframework.common.network;

import java.io.IOException;

import org.apache.commons.net.ftp.FTPClient;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Classe di utilità per la connessione ad un server FTP.
 *
 * @author filippo
 */
public final class FtpUtils {

	/**
	 * Porta TCP di default per il protocollo FTP.
	 */
	private static final int DEFAULT_PORT = 21;

	private FtpUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Apre la connessione verso un server ftp.
	 * <br>
	 * &Egrave possibile istanziare l'oggetto {@code FTPClientImpl} utilizzando il
	 * costrutto try-with-resources per la disconnessione "automatica".
	 *
	 * @param host server al quale connettersi.
	 * @param port porta.
	 * @param user nome utente.
	 * @param password password di accesso.
	 * @return l'oggetto client ftp.
	 * @throws FTPLoginException se la login non è corretta.
	 */
	@NonNull
	public static AutoCloseableFTPClient connect(final @NonNull String host, final Integer port, final @NonNull String user, final @NonNull String password) throws IOException, FTPLoginException {
		final AutoCloseableFTPClient ftpClient = new AutoCloseableFTPClient();
		ftpClient.connect(host, port == null ? DEFAULT_PORT : port.intValue());
		ftpClient.enterLocalPassiveMode();
		if (!ftpClient.login(user, password)) {
			throw new FTPLoginException("Invalid login");
		}
		return ftpClient;
	}

	/**
	 * Estensione della classe {@code FTPClient} che implementa l'interfaccia AutoCloseable in modo
	 * da utilizzare il costrutto try-with-resources per la disconnessione.
	 */
	public static final class AutoCloseableFTPClient extends FTPClient implements AutoCloseable {

		/**
		 * Costruttore privato: questa classe dev'essere istanziabile solo all'interno
		 * di questa classe di utilità.
		 */
		private AutoCloseableFTPClient() {
			super();
		}

		@Override
		public void close() throws Exception {
			this.disconnect();
		}
	}

	/**
	 * Eccezione che viene sollevata in caso di login errato.
	 */
	public static class FTPLoginException extends Exception {

		private static final long serialVersionUID = 6501732892522771545L;

		public FTPLoginException(final String message) {
			super(message);
		}
	}
}
