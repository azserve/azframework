package com.azserve.azframework.common.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.azserve.azframework.common.annotation.NonNull;

public final class HttpUtils {

	private HttpUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	@NonNull
	public static InputStream requestPost(final @NonNull byte[] request, final @NonNull URL url) throws IOException {
		final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

		connection.setRequestProperty("Content-Length", Integer.toString(request.length));
		connection.setRequestProperty("Content-Language", "en-US");

		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);

		try (final OutputStream os = connection.getOutputStream()) {
			os.write(request);
			os.flush();
		}
		return connection.getInputStream();
	}
}
