package com.azserve.framework.common.network;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Test;

import com.azserve.azframework.common.network.NetworkUtils;
import com.azserve.azframework.common.test.TestUtils;

public class NetworkUtilsTest {

	@Test
	public void testConstructor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.testUtilsClassDefinition(NetworkUtils.class);
	}

	@Test
	public void testMatchInet() throws UnknownHostException {
		final InetAddress net = InetAddress.getByAddress(new byte[] { 10, 0, 0, 0 });
		final InetAddress addr = InetAddress.getByAddress(new byte[] { 10, 0, 1, 0 });
		assertFalse(NetworkUtils.match(net, (short) 32, addr));
		assertFalse(NetworkUtils.match(net, (short) 24, addr));
		assertTrue(NetworkUtils.match(net, (short) 23, addr));
		assertTrue(NetworkUtils.match(net, (short) 0, addr));
	}

	@Test
	public void testMatchString() throws UnknownHostException {
		final String net = "10.0.0.0";
		final String addr = "10.0.1.0";
		assertFalse(NetworkUtils.match(net, (short) 32, addr));
		assertFalse(NetworkUtils.match(net, (short) 24, addr));
		assertTrue(NetworkUtils.match(net, (short) 23, addr));
		assertTrue(NetworkUtils.match(net, (short) 0, addr));
	}
}
