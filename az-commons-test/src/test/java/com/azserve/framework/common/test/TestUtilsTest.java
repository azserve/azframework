package com.azserve.framework.common.test;

import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.azserve.azframework.common.test.TestUtils;

public class TestUtilsTest {

	@Test
	public void testConstuctor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.testUtilsClassDefinition(TestUtils.class);
	}

	@Test
	public void testAssertThrows1() {
		TestUtils.assertThrows(NullPointerException.class, () -> {
			throw new NullPointerException();
		});
		TestUtils.assertThrows(Exception.class, true, () -> {
			throw new NullPointerException();
		});
		TestUtils.assertThrows(NullPointerException.class, true, () -> {
			throw new NullPointerException();
		});
	}

	@Test(expected = AssertionError.class)
	public void testAssertThrows2() {
		TestUtils.assertThrows(Exception.class, false, () -> {
			throw new NullPointerException();
		});
	}

	@Test(expected = AssertionError.class)
	public void testAssertThrows3() {
		TestUtils.assertThrows(Exception.class, () -> {

		});
	}

	@Test(expected = AssertionError.class)
	public void testAssertThrows4() {
		TestUtils.assertThrows(Exception.class, true, () -> {

		});
	}

	@Test(expected = AssertionError.class)
	public void testAssertThrows5() {
		TestUtils.assertThrows(Exception.class, false, () -> {

		});
	}

	@Test(expected = AssertionError.class)
	public void testAssertThrows6() {
		TestUtils.assertThrows(NullPointerException.class, true, () -> {
			throw new Exception();
		});
	}

	@Test
	public void testUtilsClassDefinition() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.assertThrows(AssertionError.class, () -> TestUtils.testUtilsClassDefinition(PublicConstructorClass.class));
		TestUtils.assertThrows(AssertionError.class, () -> TestUtils.testUtilsClassDefinition(PrivateConstructorClass.class));
		TestUtils.assertThrows(AssertionError.class, () -> TestUtils.testUtilsClassDefinition(NotFinalClass.class));
		TestUtils.assertThrows(AssertionError.class, () -> TestUtils.testUtilsClassDefinition(MultiConstructorClass.class));
		TestUtils.assertThrows(AssertionError.class, () -> TestUtils.testUtilsClassDefinition(BadExceptionClass.class));
		TestUtils.testUtilsClassDefinition(UtilityClass.class);
	}

	@Test
	public void testNull() {
		assertNull(TestUtils.getNull());
	}

	// sample classes
	private static class PublicConstructorClass {

		@SuppressWarnings("unused")
		public PublicConstructorClass() {}
	}

	private static class PrivateConstructorClass {

		private PrivateConstructorClass() {}
	}

	private static class NotFinalClass {

		private NotFinalClass() {
			throw new AssertionError("Not instantiable: " + this.getClass());
		}
	}

	private static class BadExceptionClass {

		private BadExceptionClass() {
			throw new NullPointerException("Not instantiable: " + this.getClass());
		}
	}

	private final static class MultiConstructorClass {

		private MultiConstructorClass() {
			throw new AssertionError("Not instantiable: " + this.getClass());
		}

		private MultiConstructorClass(final int i) {}
	}

	private final static class UtilityClass {

		private UtilityClass() {
			throw new AssertionError("Not instantiable: " + this.getClass());
		}
	}
}
