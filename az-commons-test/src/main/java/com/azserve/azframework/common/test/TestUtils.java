package com.azserve.azframework.common.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

public final class TestUtils {

	private TestUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	public static void assertThrows(final Class<? extends Throwable> ex, final RunnableWithException runnable) {
		assertThrows(ex, false, runnable);
	}

	public static void assertThrows(final Class<? extends Throwable> ex, final boolean acceptSuper, final RunnableWithException runnable) {
		try {
			runnable.run();
		} catch (final Throwable e) {
			if (acceptSuper) {
				if (ex.isAssignableFrom(e.getClass())) {
					return;
				}
			} else {
				if (e.getClass() == ex) {
					return;
				}
			}
			throw new AssertionError("Exception " + ex.getName() + " expected but " + e.getClass().getName() + " was thrown");
		}
		throw new AssertionError("Exception " + ex.getName() + " expected but none was thrown");
	}

	public static void testUtilsClassDefinition(final Class<?> clazz) throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		testPrivateUnaccessibleConstructor(clazz);
		if (!Modifier.isFinal(clazz.getModifiers())) {
			throw new AssertionError("Class " + clazz + " is not final");
		}
	}

	private static void testPrivateUnaccessibleConstructor(final Class<?> clazz) throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		final int constructorsCount = clazz.getDeclaredConstructors().length;
		if (constructorsCount != 1) {
			throw new AssertionError("Class " + clazz + " has " + constructorsCount + " constructors");
		}
		final Constructor<?> constructor = clazz.getDeclaredConstructor();
		if (!Modifier.isPrivate(constructor.getModifiers())) {
			throw new AssertionError("Constructor is not private");
		}
		constructor.setAccessible(true);
		try {
			constructor.newInstance();
			throw new AssertionError("Constructor does not throw AssertionError");
		} catch (final InvocationTargetException e) {
			if (e.getCause().getClass() != AssertionError.class) {
				throw new AssertionError("Constructor does not throw AssertionError");
			}
			return;
		}
	}

	/**
	 * Restituisce sempre null
	 * Utile per evitare errore in compilazione con l'annotazione {@linkplain com.azserve.azframework.common.annotation.NonNull}
	 *
	 * @return null
	 */
	public static <T> T getNull() {
		return null;
	}
}
