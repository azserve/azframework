package com.azserve.azframework.common.test;

@FunctionalInterface
public interface RunnableWithException {

	void run() throws Exception;
}
