package com.azserve.framework.common.lang;

import static com.azserve.azframework.common.lang.ClassUtils.getWrapper;
import static com.azserve.azframework.common.lang.ClassUtils.isPrimitiveNumber;
import static com.azserve.azframework.common.test.TestUtils.assertThrows;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.azserve.azframework.common.lang.ClassUtils;
import com.azserve.azframework.common.test.TestUtils;

public class ClassUtilsTest {

	public interface AnchestorInterface {}

	public interface FirstInterface extends AnchestorInterface {}

	public interface SecondInterface {}

	public static class GrandParentClass {}

	public static class ParentClass extends GrandParentClass {}

	public static class AClass extends ParentClass implements FirstInterface, SecondInterface {}

	@Test
	public void constructor() throws Throwable {
		TestUtils.testUtilsClassDefinition(ClassUtils.class);
	}

	@Test
	public void uncheckedCastTest() {
		this.getListStringClass(List.class);
	}

	private Class<List<String>> getListStringClass(final Class<?> c) {
		return ClassUtils.uncheckedCast(c);
	}

	@Test
	public void getObjectClassTest() {
		assertNull(ClassUtils.getObjectClass(TestUtils.getNull()));
		assertTrue(ClassUtils.getObjectClass("pippo") == String.class);
		final Number n = Integer.valueOf(0);
		assertTrue(ClassUtils.getObjectClass(n) == Integer.class);
	}

	@Test
	public void getSuperclassesTest() {
		final List<Class<?>> superclasses = ClassUtils.getSuperclasses(AClass.class);
		assertTrue(superclasses.get(0) == ParentClass.class);
		assertTrue(superclasses.get(1) == GrandParentClass.class);
	}

	@Test
	public void isPrimitiveNumberTest() {
		assertThrows(NullPointerException.class, () -> isPrimitiveNumber(TestUtils.getNull()));

		assertTrue(isPrimitiveNumber(byte.class));
		assertTrue(isPrimitiveNumber(short.class));
		assertTrue(isPrimitiveNumber(int.class));
		assertTrue(isPrimitiveNumber(long.class));
		assertTrue(isPrimitiveNumber(float.class));
		assertTrue(isPrimitiveNumber(double.class));

		assertFalse(isPrimitiveNumber(boolean.class));
		assertFalse(isPrimitiveNumber(char.class));
		assertFalse(isPrimitiveNumber(void.class));

		assertFalse(isPrimitiveNumber(Object.class));
		assertFalse(isPrimitiveNumber(Integer.class));
		assertFalse(isPrimitiveNumber(String.class));
		assertFalse(isPrimitiveNumber(Boolean.class));
	}

	@Test
	public void getPrimitiveTest() {
		assertTrue(ClassUtils.getPrimitive(Byte.class) == byte.class);
		assertTrue(ClassUtils.getPrimitive(Short.class) == short.class);
		assertTrue(ClassUtils.getPrimitive(Integer.class) == int.class);
		assertTrue(ClassUtils.getPrimitive(Long.class) == long.class);
		assertTrue(ClassUtils.getPrimitive(Float.class) == float.class);
		assertTrue(ClassUtils.getPrimitive(Double.class) == double.class);

		assertFalse(ClassUtils.getPrimitive(Byte.class) == Byte.class);
		assertFalse(ClassUtils.getPrimitive(Short.class) == Short.class);
		assertFalse(ClassUtils.getPrimitive(Integer.class) == Integer.class);
		assertFalse(ClassUtils.getPrimitive(Long.class) == Long.class);
		assertFalse(ClassUtils.getPrimitive(Float.class) == Float.class);
		assertFalse(ClassUtils.getPrimitive(Double.class) == Double.class);

		assertTrue(ClassUtils.getPrimitive(BigInteger.class) == null);
		assertTrue(ClassUtils.getPrimitive(BigDecimal.class) == null);
		assertTrue(ClassUtils.getPrimitive(int.class) == null);
	}

	@Test
	public void getWrapperTest() {
		assertTrue(getWrapper(byte.class) == Byte.class);
		assertTrue(getWrapper(short.class) == Short.class);
		assertTrue(getWrapper(int.class) == Integer.class);
		assertTrue(getWrapper(long.class) == Long.class);
		assertTrue(getWrapper(float.class) == Float.class);
		assertTrue(getWrapper(double.class) == Double.class);

		assertTrue(getWrapper(Byte.class) == null);
		assertTrue(getWrapper(Short.class) == null);
		assertTrue(getWrapper(Integer.class) == null);
		assertTrue(getWrapper(Long.class) == null);
		assertTrue(getWrapper(Float.class) == null);
		assertTrue(getWrapper(Double.class) == null);
		assertTrue(getWrapper(BigInteger.class) == null);
		assertTrue(getWrapper(BigDecimal.class) == null);
	}

	@Test
	public void getShortClassNameTest() {
		assertTrue(ClassUtils.getShortClassName(null) == null);
		assertEquals(ClassUtils.getShortClassName(null, "Prova"), "Prova");
		assertEquals(ClassUtils.getShortClassName(new Object(), "Prova"), Object.class.getSimpleName());
	}

	@Test
	public void getInterfacesTest() {
		final Set<Class<?>> interfaces = ClassUtils.getInterfaces(AClass.class);
		assertTrue(interfaces.contains(AnchestorInterface.class)
				&& interfaces.contains(FirstInterface.class)
				&& interfaces.contains(SecondInterface.class));
	}

}
