package com.azserve.framework.common.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.azserve.azframework.common.test.TestUtils;
import com.azserve.azframework.common.util.Regexp;

public class RegexpTest {

	@Test
	public void testConstructor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.testUtilsClassDefinition(Regexp.class);
	}

	@Test
	public void testRegex() {
		final String[] ok = { "pippo@gmail.com", "pippo%1@gmail.com", "pippo-2@g-mail.it", "PIPPO+@GMAIL.MAIL" };
		for (final String email : ok) {
			assertTrue(email.matches(Regexp.EMAIL));
			assertTrue(email.matches(Regexp.EMPTY_OR_EMAIL));
		}
		assertFalse("".matches(Regexp.EMAIL));
		assertTrue("".matches(Regexp.EMPTY_OR_EMAIL));

		final String[] ko = { "pippo @gmail.com", "pippo%1@gmail.c", "pipp*o-2@g-mail.it", "PIPPO@@GMAIL.MAIL" };
		for (final String email : ko) {
			assertFalse(email.matches(Regexp.EMAIL));
			assertFalse(email.matches(Regexp.EMPTY_OR_EMAIL));
		}

	}
}
