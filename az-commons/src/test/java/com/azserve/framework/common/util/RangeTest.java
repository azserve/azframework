package com.azserve.framework.common.util;

import static com.azserve.azframework.common.test.TestUtils.assertThrows;
import static java.lang.Integer.valueOf;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import com.azserve.azframework.common.test.TestUtils;
import com.azserve.azframework.common.util.Range;

public class RangeTest {

	private final Range<Integer> infinityRange = Range.infinite();
	private final Range<Integer> zeroToTenRange = range(0, 10);
	private final Range<Integer> tenRange = range(10);
	private final Range<Integer> infinityToZeroRange = rangeTo(0);
	private final Range<Integer> zeroToInfinityRange = rangeFrom(0);

	private static Range<Integer> range(final int min, final int max) {
		return Range.of(valueOf(min), valueOf(max));
	}

	private static Range<Integer> range(final int value) {
		return Range.of(valueOf(value));
	}

	private static Range<Integer> rangeTo(final int max) {
		return Range.to(valueOf(max));
	}

	private static Range<Integer> rangeFrom(final int min) {
		return Range.from(valueOf(min));
	}

	//

	@Test
	public void staticContructorsTest() {
		assertTrue(this.infinityRange.getMin() == null);
		assertTrue(this.infinityRange.getMax() == null);

		final Integer min = valueOf(0);
		final Integer max = valueOf(10);

		final Range<Integer> rangeMinMax = Range.of(min, max);
		assertTrue(rangeMinMax.getMin() == min && rangeMinMax.getMax() == max);

		final Range<Integer> rangeMinMaxInverse = Range.of(Comparator.reverseOrder(), max, min);
		assertTrue(rangeMinMaxInverse.getMin() == max && rangeMinMaxInverse.getMax() == min);

		final Range<Integer> rangeMin = Range.from(min);
		assertTrue(rangeMin.getMin() == min && rangeMin.getMax() == null);

		final Range<Integer> rangeMinInverse = Range.from(Comparator.reverseOrder(), min);
		assertTrue(rangeMinInverse.getMin() == min && rangeMinInverse.getMax() == null);

		final Range<Integer> rangeMax = Range.to(max);
		assertTrue(rangeMax.getMin() == null && rangeMax.getMax() == max);

		final Range<Integer> rangeMaxInverse = Range.to(Comparator.reverseOrder(), max);
		assertTrue(rangeMaxInverse.getMin() == null && rangeMaxInverse.getMax() == max);
	}

	@Test
	public void orderFailTest() {
		assertThrows(IllegalArgumentException.class, () -> Range.of(valueOf(Integer.MAX_VALUE), valueOf(Integer.MIN_VALUE)));
	}

	@Test
	public void orderSuccessTest() {
		Range.of(Comparator.reverseOrder(), valueOf(Integer.MAX_VALUE), valueOf(Integer.MIN_VALUE));
	}

	@Test
	public void testIsInfinite() {
		assertTrue(Range.infinite().isInfinite());
		assertTrue(Range.<String> of((String) null, null).isInfinite());
	}

	@Test
	public void testContains() {
		assertFalse(this.infinityRange.contains(null));
		assertTrue(this.infinityRange.contains(valueOf(0)));
		assertTrue(this.infinityRange.contains(valueOf(10)));
		assertTrue(this.infinityRange.contains(valueOf(Integer.MIN_VALUE)));
		assertTrue(this.infinityRange.contains(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.zeroToTenRange.contains(null));
		assertFalse(this.zeroToTenRange.contains(valueOf(-10)));
		assertTrue(this.zeroToTenRange.contains(valueOf(0)));
		assertTrue(this.zeroToTenRange.contains(valueOf(5)));
		assertFalse(this.zeroToTenRange.contains(valueOf(10)));
		assertFalse(this.zeroToTenRange.contains(valueOf(20)));
		assertFalse(this.zeroToTenRange.contains(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.tenRange.contains(null));
		assertFalse(this.tenRange.contains(valueOf(0)));
		assertFalse(this.tenRange.contains(valueOf(10)));

		assertFalse(this.infinityToZeroRange.contains(null));
		assertTrue(this.infinityToZeroRange.contains(valueOf(Integer.MIN_VALUE)));
		assertFalse(this.infinityToZeroRange.contains(valueOf(0)));
		assertFalse(this.infinityToZeroRange.contains(valueOf(10)));
		assertFalse(this.infinityToZeroRange.contains(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.zeroToInfinityRange.contains(null));
		assertFalse(this.zeroToInfinityRange.contains(valueOf(Integer.MIN_VALUE)));
		assertTrue(this.zeroToInfinityRange.contains(valueOf(0)));
		assertTrue(this.zeroToInfinityRange.contains(valueOf(10)));
		assertTrue(this.zeroToInfinityRange.contains(valueOf(Integer.MAX_VALUE)));
	}

	@Test
	public void testContainsExclusive() {
		assertFalse(this.infinityRange.containsExclusive(null));
		assertTrue(this.infinityRange.containsExclusive(valueOf(0)));
		assertTrue(this.infinityRange.containsExclusive(valueOf(10)));
		assertTrue(this.infinityRange.containsExclusive(valueOf(Integer.MIN_VALUE)));
		assertTrue(this.infinityRange.containsExclusive(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.zeroToTenRange.containsExclusive(null));
		assertFalse(this.zeroToTenRange.containsExclusive(valueOf(-10)));
		assertFalse(this.zeroToTenRange.containsExclusive(valueOf(0)));
		assertTrue(this.zeroToTenRange.containsExclusive(valueOf(5)));
		assertFalse(this.zeroToTenRange.containsExclusive(valueOf(10)));
		assertFalse(this.zeroToTenRange.containsExclusive(valueOf(20)));
		assertFalse(this.zeroToTenRange.containsExclusive(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.tenRange.containsExclusive(null));
		assertFalse(this.tenRange.containsExclusive(valueOf(0)));
		assertFalse(this.tenRange.containsExclusive(valueOf(10)));

		assertFalse(this.infinityToZeroRange.containsExclusive(null));
		assertTrue(this.infinityToZeroRange.containsExclusive(valueOf(Integer.MIN_VALUE)));
		assertFalse(this.infinityToZeroRange.containsExclusive(valueOf(0)));
		assertFalse(this.infinityToZeroRange.containsExclusive(valueOf(10)));
		assertFalse(this.infinityToZeroRange.containsExclusive(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.zeroToInfinityRange.containsExclusive(null));
		assertFalse(this.zeroToInfinityRange.containsExclusive(valueOf(Integer.MIN_VALUE)));
		assertFalse(this.zeroToInfinityRange.containsExclusive(valueOf(0)));
		assertTrue(this.zeroToInfinityRange.containsExclusive(valueOf(10)));
		assertTrue(this.zeroToInfinityRange.containsExclusive(valueOf(Integer.MAX_VALUE)));
	}

	@Test
	public void testContainsInclusive() {
		assertFalse(this.infinityRange.containsInclusive(null));
		assertTrue(this.infinityRange.containsInclusive(valueOf(0)));
		assertTrue(this.infinityRange.containsInclusive(valueOf(10)));
		assertTrue(this.infinityRange.containsInclusive(valueOf(Integer.MIN_VALUE)));
		assertTrue(this.infinityRange.containsInclusive(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.zeroToTenRange.containsInclusive(null));
		assertFalse(this.zeroToTenRange.containsInclusive(valueOf(-10)));
		assertTrue(this.zeroToTenRange.containsInclusive(valueOf(0)));
		assertTrue(this.zeroToTenRange.containsInclusive(valueOf(5)));
		assertTrue(this.zeroToTenRange.containsInclusive(valueOf(10)));
		assertFalse(this.zeroToTenRange.containsInclusive(valueOf(20)));
		assertFalse(this.zeroToTenRange.containsInclusive(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.tenRange.containsInclusive(null));
		assertFalse(this.tenRange.containsInclusive(valueOf(0)));
		assertTrue(this.tenRange.containsInclusive(valueOf(10)));

		assertFalse(this.infinityToZeroRange.containsInclusive(null));
		assertTrue(this.infinityToZeroRange.containsInclusive(valueOf(Integer.MIN_VALUE)));
		assertTrue(this.infinityToZeroRange.containsInclusive(valueOf(0)));
		assertFalse(this.infinityToZeroRange.containsInclusive(valueOf(10)));
		assertFalse(this.infinityToZeroRange.containsInclusive(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.zeroToInfinityRange.containsInclusive(null));
		assertFalse(this.zeroToInfinityRange.containsInclusive(valueOf(Integer.MIN_VALUE)));
		assertTrue(this.zeroToInfinityRange.containsInclusive(valueOf(0)));
		assertTrue(this.zeroToInfinityRange.containsInclusive(valueOf(10)));
		assertTrue(this.zeroToInfinityRange.containsInclusive(valueOf(Integer.MAX_VALUE)));
	}

	@Test
	public void testIsBefore() {
		assertFalse(this.infinityRange.isBefore(null));
		assertFalse(this.infinityRange.isBefore(valueOf(10)));

		assertFalse(this.zeroToTenRange.isBefore(null));
		assertFalse(this.zeroToTenRange.isBefore(valueOf(Integer.MIN_VALUE)));
		assertFalse(this.zeroToTenRange.isBefore(valueOf(0)));
		assertFalse(this.zeroToTenRange.isBefore(valueOf(10)));
		assertTrue(this.zeroToTenRange.isBefore(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.tenRange.isBefore(null));
		assertFalse(this.tenRange.isBefore(valueOf(Integer.MIN_VALUE)));
		assertFalse(this.tenRange.isBefore(valueOf(0)));
		assertFalse(this.tenRange.isBefore(valueOf(10)));
		assertTrue(this.tenRange.isBefore(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.infinityToZeroRange.isBefore(null));
		assertFalse(this.infinityToZeroRange.isBefore(valueOf(Integer.MIN_VALUE)));
		assertFalse(this.infinityToZeroRange.isBefore(valueOf(0)));
		assertTrue(this.infinityToZeroRange.isBefore(valueOf(10)));
		assertTrue(this.infinityToZeroRange.isBefore(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.zeroToInfinityRange.isBefore(null));
		assertFalse(this.zeroToInfinityRange.isBefore(valueOf(Integer.MIN_VALUE)));
		assertFalse(this.zeroToInfinityRange.isBefore(valueOf(0)));
		assertFalse(this.zeroToInfinityRange.isBefore(valueOf(10)));
		assertFalse(this.zeroToInfinityRange.isBefore(valueOf(Integer.MAX_VALUE)));
	}

	@Test
	public void testIsAfter() {
		assertFalse(this.infinityRange.isAfter(null));
		assertFalse(this.infinityRange.isAfter(valueOf(10)));

		assertFalse(this.zeroToTenRange.isAfter(null));
		assertTrue(this.zeroToTenRange.isAfter(valueOf(Integer.MIN_VALUE)));
		assertFalse(this.zeroToTenRange.isAfter(valueOf(10)));
		assertFalse(this.zeroToTenRange.isAfter(valueOf(20)));
		assertFalse(this.zeroToTenRange.isAfter(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.tenRange.isAfter(null));
		assertTrue(this.tenRange.isAfter(valueOf(Integer.MIN_VALUE)));
		assertFalse(this.tenRange.isAfter(valueOf(10)));
		assertFalse(this.tenRange.isAfter(valueOf(20)));
		assertFalse(this.tenRange.isAfter(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.infinityToZeroRange.isAfter(null));
		assertFalse(this.infinityToZeroRange.isAfter(valueOf(Integer.MIN_VALUE)));
		assertFalse(this.infinityToZeroRange.isAfter(valueOf(0)));
		assertFalse(this.infinityToZeroRange.isAfter(valueOf(10)));
		assertFalse(this.infinityToZeroRange.isAfter(valueOf(Integer.MAX_VALUE)));

		assertFalse(this.zeroToInfinityRange.isAfter(null));
		assertTrue(this.zeroToInfinityRange.isAfter(valueOf(Integer.MIN_VALUE)));
		assertFalse(this.zeroToInfinityRange.isAfter(valueOf(0)));
		assertFalse(this.zeroToInfinityRange.isAfter(valueOf(10)));
		assertFalse(this.zeroToInfinityRange.isAfter(valueOf(Integer.MAX_VALUE)));
	}

	@Test
	public void testStartsBefore() {
		TestUtils.assertThrows(NullPointerException.class, () -> this.zeroToTenRange.startsBefore(TestUtils.getNull()));

		assertFalse(this.infinityRange.startsBefore(this.infinityRange));
		assertTrue(this.infinityRange.startsBefore(this.zeroToTenRange));
		assertTrue(this.infinityRange.startsBefore(this.tenRange));
		assertFalse(this.infinityRange.startsBefore(this.infinityToZeroRange));
		assertTrue(this.infinityRange.startsBefore(this.zeroToInfinityRange));

		assertFalse(this.zeroToTenRange.startsBefore(this.infinityRange));
		assertFalse(this.zeroToTenRange.startsBefore(this.zeroToTenRange));
		assertTrue(this.zeroToTenRange.startsBefore(this.tenRange));
		assertFalse(this.zeroToTenRange.startsBefore(this.infinityToZeroRange));
		assertFalse(this.zeroToTenRange.startsBefore(this.zeroToInfinityRange));

		assertFalse(this.tenRange.startsBefore(this.infinityRange));
		assertFalse(this.tenRange.startsBefore(this.zeroToTenRange));
		assertFalse(this.tenRange.startsBefore(this.tenRange));
		assertFalse(this.tenRange.startsBefore(this.infinityToZeroRange));
		assertFalse(this.tenRange.startsBefore(this.zeroToInfinityRange));
		assertTrue(this.tenRange.startsBefore(range(11)));
		assertTrue(this.tenRange.startsBefore(rangeFrom(11)));

		assertFalse(this.infinityToZeroRange.startsBefore(this.infinityRange));
		assertTrue(this.infinityToZeroRange.startsBefore(this.zeroToTenRange));
		assertTrue(this.infinityToZeroRange.startsBefore(this.tenRange));
		assertFalse(this.infinityToZeroRange.startsBefore(this.infinityToZeroRange));
		assertTrue(this.infinityToZeroRange.startsBefore(this.zeroToInfinityRange));

		assertFalse(this.zeroToInfinityRange.startsBefore(this.infinityRange));
		assertFalse(this.zeroToInfinityRange.startsBefore(this.zeroToTenRange));
		assertTrue(this.zeroToInfinityRange.startsBefore(this.tenRange));
		assertFalse(this.zeroToInfinityRange.startsBefore(this.infinityToZeroRange));
		assertFalse(this.zeroToInfinityRange.startsBefore(this.zeroToInfinityRange));
	}

	@Test
	public void testEndsBefore() {
		TestUtils.assertThrows(NullPointerException.class, () -> this.zeroToTenRange.endsBefore(TestUtils.getNull()));

		assertFalse(this.infinityRange.endsBefore(this.infinityRange));
		assertFalse(this.infinityRange.endsBefore(this.zeroToTenRange));
		assertFalse(this.infinityRange.endsBefore(this.tenRange));
		assertFalse(this.infinityRange.endsBefore(this.infinityToZeroRange));
		assertFalse(this.infinityRange.endsBefore(this.zeroToInfinityRange));

		assertTrue(this.zeroToTenRange.endsBefore(this.infinityRange));
		assertFalse(this.zeroToTenRange.endsBefore(this.zeroToTenRange));
		assertFalse(this.zeroToTenRange.endsBefore(this.tenRange));
		assertFalse(this.zeroToTenRange.endsBefore(this.infinityToZeroRange));
		assertTrue(this.zeroToTenRange.endsBefore(this.zeroToInfinityRange));

		assertTrue(this.tenRange.endsBefore(this.infinityRange));
		assertFalse(this.tenRange.endsBefore(this.zeroToTenRange));
		assertFalse(this.tenRange.endsBefore(this.tenRange));
		assertFalse(this.tenRange.endsBefore(this.infinityToZeroRange));
		assertTrue(this.tenRange.endsBefore(this.zeroToInfinityRange));
		assertTrue(this.tenRange.endsBefore(range(11)));
		assertTrue(this.tenRange.endsBefore(rangeTo(11)));

		assertTrue(this.infinityToZeroRange.endsBefore(this.infinityRange));
		assertTrue(this.infinityToZeroRange.endsBefore(this.zeroToTenRange));
		assertTrue(this.infinityToZeroRange.endsBefore(this.tenRange));
		assertFalse(this.infinityToZeroRange.endsBefore(this.infinityToZeroRange));
		assertTrue(this.infinityToZeroRange.endsBefore(this.zeroToInfinityRange));

		assertFalse(this.zeroToInfinityRange.endsBefore(this.infinityRange));
		assertFalse(this.zeroToInfinityRange.endsBefore(this.zeroToTenRange));
		assertFalse(this.zeroToInfinityRange.endsBefore(this.tenRange));
		assertFalse(this.zeroToInfinityRange.endsBefore(this.infinityToZeroRange));
		assertFalse(this.zeroToInfinityRange.endsBefore(this.zeroToInfinityRange));
	}

	@Test
	public void testIsOverlapping() {
		TestUtils.assertThrows(NullPointerException.class, () -> this.infinityRange.isOverlapping(TestUtils.getNull()));

		assertTrue(this.infinityRange.isOverlapping(this.infinityRange));
		assertTrue(this.infinityRange.isOverlapping(this.zeroToTenRange));
		assertTrue(this.infinityRange.isOverlapping(this.tenRange));
		assertTrue(this.infinityRange.isOverlapping(this.infinityToZeroRange));
		assertTrue(this.infinityRange.isOverlapping(this.zeroToInfinityRange));

		assertFalse(this.zeroToTenRange.isOverlapping(rangeTo(-1)));
		assertTrue(this.zeroToTenRange.isOverlapping(rangeTo(10)));
		assertTrue(this.zeroToTenRange.isOverlapping(rangeFrom(10)));
		assertTrue(this.zeroToTenRange.isOverlapping(this.infinityRange));

		assertTrue(this.tenRange.isOverlapping(this.infinityRange));
		assertTrue(this.tenRange.isOverlapping(this.zeroToTenRange));
		assertTrue(this.tenRange.isOverlapping(this.tenRange));
		assertFalse(this.tenRange.isOverlapping(range(9)));
		assertFalse(this.tenRange.isOverlapping(range(11)));
		assertFalse(this.tenRange.isOverlapping(this.infinityToZeroRange));
		assertTrue(this.tenRange.isOverlapping(this.zeroToInfinityRange));

		assertTrue(this.infinityToZeroRange.isOverlapping(this.infinityRange));
		assertTrue(this.infinityToZeroRange.isOverlapping(this.zeroToTenRange));
		assertFalse(this.infinityToZeroRange.isOverlapping(range(1)));
		assertFalse(this.infinityToZeroRange.isOverlapping(this.tenRange));
		assertTrue(this.infinityToZeroRange.isOverlapping(this.infinityToZeroRange));
		assertTrue(this.infinityToZeroRange.isOverlapping(this.zeroToInfinityRange));

		assertTrue(this.zeroToInfinityRange.isOverlapping(this.infinityRange));
		assertTrue(this.zeroToInfinityRange.isOverlapping(this.zeroToTenRange));
		assertTrue(this.zeroToInfinityRange.isOverlapping(this.tenRange));
		assertFalse(this.zeroToInfinityRange.isOverlapping(range(-1)));
		assertTrue(this.zeroToInfinityRange.isOverlapping(this.infinityToZeroRange));
		assertTrue(this.zeroToInfinityRange.isOverlapping(this.zeroToInfinityRange));
	}

	@Test
	public void testIntersect() {
		final List<Range<Integer>> ranges = Arrays.asList(this.infinityRange, this.zeroToTenRange, this.tenRange, this.infinityToZeroRange, this.zeroToInfinityRange);
		for (final Range<Integer> range : ranges) {
			assertTrue(this.infinityRange.intersect(range).equals(range));
		}
		for (final Range<Integer> range : ranges) {
			assertTrue(range.intersect(this.infinityRange).equals(this.infinityRange.intersect(range)));
		}

		assertTrue(range(10, 20).intersect(range(0, 10)).equals(range(10)));
		assertTrue(range(0, 20).intersect(range(0, 10)).equals(range(0, 10)));
		assertTrue(range(10, 20).intersect(range(0, 20)).equals(range(10, 20)));
		assertTrue(range(0, 10).intersect(range(20, 30)) == null);
	}

	@Test
	public void testIsEmpty() {
		assertTrue(this.tenRange.isEmpty());
		assertFalse(this.infinityRange.isEmpty());
		assertFalse(this.zeroToTenRange.isEmpty());
		assertFalse(this.infinityToZeroRange.isEmpty());
		assertFalse(this.zeroToInfinityRange.isEmpty());
	}

	@Test
	public void testIsClosed() {
		assertTrue(this.tenRange.isClosed());
		assertFalse(this.infinityRange.isClosed());
		assertTrue(this.zeroToTenRange.isClosed());
		assertFalse(this.infinityToZeroRange.isClosed());
		assertFalse(this.zeroToInfinityRange.isClosed());
	}

	@Test
	public void testCompareTo() {
		final List<Range<Integer>> ranges = Arrays.asList(this.infinityRange, this.zeroToTenRange, this.tenRange, this.infinityToZeroRange, this.zeroToInfinityRange);

		// range che si equivalgono hanno stesso ordine
		for (final Range<Integer> range : ranges) {
			assertTrue(range.compareTo(range) == 0);
		}

		// meno infinito "inizia" prima di 0
		assertTrue(this.infinityRange.compareTo(this.zeroToTenRange) < 0);
		// meno infinito "inizia" prima di 10
		assertTrue(this.infinityRange.compareTo(this.tenRange) < 0);
		// più infinito "finisce" dopo di 0
		assertTrue(this.infinityRange.compareTo(this.infinityToZeroRange) > 0);
		// meno infinito "inizia" prima di 0
		assertTrue(this.infinityRange.compareTo(this.zeroToInfinityRange) < 0);
		// zero inizia dopo di meno infinito
		assertTrue(this.zeroToTenRange.compareTo(this.infinityRange) > 0);
		// dieci inizia dopo di meno infinito
		assertTrue(this.tenRange.compareTo(this.infinityRange) > 0);
		// 0 finisce prima di più infinito
		assertTrue(this.infinityToZeroRange.compareTo(this.infinityRange) < 0);
		// 0 inizia dopo di meno finito
		assertTrue(this.zeroToInfinityRange.compareTo(this.infinityRange) > 0);
		// 10 finisce prima di più infinito
		assertTrue(range(0, 10).compareTo(rangeFrom(0)) < 0);
		// 0 inizia prima di 20
		assertTrue(range(0, 10).compareTo(range(20, 30)) < 0);
		// 20 finisce dopo di 10
		assertTrue(range(0, 20).compareTo(range(0, 10)) > 0);
	}
}
