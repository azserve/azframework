package com.azserve.framework.common.funct;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.Test;

import com.azserve.azframework.common.funct.Functions;
import com.azserve.azframework.common.test.TestUtils;

public class FunctionsTest {

	private final String object1 = "object1";
	private final String object2 = "object2";
	private final String object3 = "object3";

	@Test
	public void constructor() throws Throwable {
		TestUtils.testUtilsClassDefinition(Functions.class);
	}

	@Test
	public void testNullSupplier() {
		assertNull(Functions.nullSupplier().get());
	}

	@Test
	public void testNullFunction() {
		assertNull(Functions.nullFunction().apply(this.object1));
		assertNull(Functions.nullFunction().apply(null));
	}

	@Test
	public void testNullBiFunction() {
		assertNull(Functions.nullBiFunction().apply(this.object1, this.object2));
		assertNull(Functions.nullBiFunction().apply(this.object1, null));
		assertNull(Functions.nullBiFunction().apply(null, this.object2));
		assertNull(Functions.nullBiFunction().apply(null, null));
	}

	@Test
	public void testNullTriFunction() {
		assertNull(Functions.nullTriFunction().apply(this.object1, this.object2, this.object3));
		assertNull(Functions.nullTriFunction().apply(this.object1, this.object2, null));
		assertNull(Functions.nullTriFunction().apply(this.object1, null, null));
		assertNull(Functions.nullTriFunction().apply(null, null, null));
	}

	@Test
	public void testNullIntFunction() {
		assertNull(Functions.nullIntFunction().apply(100));
		assertNull(Functions.nullIntFunction().apply(0));
		assertNull(Functions.nullIntFunction().apply(-100));
	}

	// @Test
	// public void testNullConsumer() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testNullBiConsumer() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testNullTriConsumer() {
	// fail("Not yet implemented");
	// }

	@Test
	public void testIdentity() {
		assertTrue(Functions.identity().apply(this.object1) == this.object1);
		assertNull(Functions.identity().apply(null));
	}

	@Test
	public void testAcceptAll() {
		assertTrue(Functions.acceptAll().test(this.object1));
		assertTrue(Functions.acceptAll().test(null));
	}

	@Test
	public void testDeclineAll() {
		assertFalse(Functions.declineAll().test(this.object1));
		assertFalse(Functions.declineAll().test(null));
	}

	@Test
	public void testBiAcceptAll() {
		assertTrue(Functions.biAcceptAll().test(this.object1, this.object2));
		assertTrue(Functions.biAcceptAll().test(this.object1, null));
		assertTrue(Functions.biAcceptAll().test(null, null));
	}

	@Test
	public void testBiDeclineAll() {
		assertFalse(Functions.biDeclineAll().test(this.object1, this.object2));
		assertFalse(Functions.biDeclineAll().test(this.object1, null));
		assertFalse(Functions.biDeclineAll().test(null, null));
	}

	@Test
	public void testAcceptFirst() {
		assertTrue(Functions.acceptFirst().apply(this.object1, this.object2) == this.object1);
	}

	@Test
	public void testAcceptSecond() {
		assertTrue(Functions.acceptSecond().apply(this.object1, this.object2) == this.object2);
	}

	@Test
	public void testAcceptFirstIf() {
		assertTrue(Functions.acceptFirstIf(Objects::nonNull).apply(this.object1, this.object2) == this.object1);
		assertTrue(Functions.acceptFirstIf(Objects::nonNull).apply(this.object1, null) == this.object1);
		assertTrue(Functions.acceptFirstIf(Objects::nonNull).apply(null, this.object2) == this.object2);
	}

	@Test
	public void testAcceptSecondIf() {
		assertTrue(Functions.acceptSecondIf(Objects::nonNull).apply(this.object1, this.object2) == this.object2);
		assertTrue(Functions.acceptSecondIf(Objects::nonNull).apply(this.object1, null) == this.object1);
		assertTrue(Functions.acceptSecondIf(Objects::nonNull).apply(null, this.object2) == this.object2);
	}

	@Test
	public void testToFunction() {
		final int[] array = new int[1];
		final Consumer<int[]> consumer = a -> a[0] = 10;
		final Function<int[], Void> function = Functions.consumerAsFunction(consumer);
		function.apply(array);
		assertEquals(array[0], 10);
	}

	// @Test
	// public void testMergeGreatestFunctionOfTD() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testMergeGreatestFunctionOfTDComparatorOfD() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testMergeLeastFunctionOfTD() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testMergeLeastFunctionOfTDComparatorOfD() {
	// fail("Not yet implemented");
	// }

	@Test
	public void testDistinctByKey() {
		final String[] array = new String[] { "Pippo", "Pluto", "Paperino", "Minnie", "Clarabella" };
		final Set<String> letters = Arrays.stream(array).filter(Functions.distinctByKey(s -> s.substring(0, 1))).collect(Collectors.toSet());
		assertTrue(letters.size() == 3 && letters.contains("Minnie") && letters.contains("Clarabella")
				&& (letters.contains("Pippo") || letters.contains("Pluto") || letters.contains("Paperino")));
	}

	@Test
	public void testNegate() {
		assertTrue(Functions.negate(Objects::nonNull).test(null));
		assertFalse(Functions.negate(Objects::nonNull).test(this.object1));
	}

	// @Test
	// public void testRethrowRunnable() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testRethrowConsumer() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testRethrowBiConsumer() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testRethrowSupplier() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testRethrowFunction() {
	// fail("Not yet implemented");
	// }
}
