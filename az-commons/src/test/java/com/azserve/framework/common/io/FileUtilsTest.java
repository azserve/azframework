package com.azserve.framework.common.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.azserve.azframework.common.io.FileUtils;
import com.azserve.azframework.common.test.TestUtils;

public class FileUtilsTest {

	@Rule
	public TemporaryFolder temp = new TemporaryFolder();

	@Test
	public void constructor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.testUtilsClassDefinition(FileUtils.class);
	}

	@Test
	public void testFileToString() throws IOException {

		final StringBuilder overBuffer = new StringBuilder(9000);
		for (int i = 0; i < 900; i++) {
			overBuffer.append("0123456789");
		}

		final String[] strings = { "", "foo1", "foo2\n", "foo3\nfoofoo", "foo4\tfoo", "àèìòù", "€", overBuffer.toString() };
		final File file = this.temp.newFile("stringFile");
		final Path path = this.temp.newFile("stringPath").toPath();
		for (final String s : strings) {
			final int length = s.getBytes("UTF-8").length;
			assertEquals(length, FileUtils.stringToFile(s, file));
			assertEquals(s, FileUtils.fileToString(file));
			assertEquals(s, FileUtils.fileToString(file.toPath()));
			assertTrue(file.delete());
			assertEquals(length, FileUtils.stringToFile(s, path));
			assertEquals(s, FileUtils.fileToString(path.toFile()));
			assertEquals(s, FileUtils.fileToString(path));
			assertTrue(Files.deleteIfExists(path));
		}

	}

	@Test
	public void testFileToBytesFile() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFileToBytesPath() {
		// fail("Not yet implemented");
	}

	@Test
	public void testInputToFileInputStreamFile() {
		// fail("Not yet implemented");
	}

	@Test
	public void testInputToFileInputStreamPath() {
		// fail("Not yet implemented");
	}

	@Test
	public void testBytesToFileByteArrayFile() {
		// fail("Not yet implemented");
	}

	@Test
	public void testBytesToFileByteArrayPath() {
		// fail("Not yet implemented");
	}

	@Test
	public void testStringToFileStringFile() {
		// fail("Not yet implemented");
	}

	@Test
	public void testStringToFileStringPath() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFileToObject() {
		// fail("Not yet implemented");
	}

	@Test
	public void testObjectToFile() {
		// fail("Not yet implemented");
	}

	@Test
	public void testCopy() {
		// fail("Not yet implemented");
	}

	@Test
	public void testCountLines() {
		// fail("Not yet implemented");
	}

	@Test
	public void testGetFile() {
		// fail("Not yet implemented");
	}

	@Test
	public void testRemoveExtension() {
		// fail("Not yet implemented");
	}

	@Test
	public void testGetExtensionPath() {
		// fail("Not yet implemented");
	}

	@Test
	public void testGetExtensionString() {
		// fail("Not yet implemented");
	}

	@Test
	public void testGetTempDir() {
		// fail("Not yet implemented");
	}

	@Test
	public void testGetUserDir() {
		// fail("Not yet implemented");
	}

	@Test
	public void testGetUserHome() {
		// fail("Not yet implemented");
	}

	@Test
	public void testListPathsPath() {
		// fail("Not yet implemented");
	}

	@Test
	public void testListPathsPathFilterOfPath() {
		// fail("Not yet implemented");
	}

	@Test
	public void testTotalSize() {
		// fail("Not yet implemented");
	}

	@Test
	public void testRecursiveDelete() {
		// fail("Not yet implemented");
	}

}
