package com.azserve.framework.common.reflect;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Set;

import org.junit.Test;

import com.azserve.azframework.common.reflect.ReflectionUtils;
import com.azserve.azframework.common.test.TestUtils;

public class ReflectionUtilsTest {

	@Test
	public void testConstructor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.testUtilsClassDefinition(ReflectionUtils.class);
	}

	@Test
	public void testNewObject() {
		assertEquals(C1.class, ReflectionUtils.newObject(C1.class).getClass());
		assertEquals(C2.class, ReflectionUtils.newObject(C2.class).getClass());
		TestUtils.assertThrows(RuntimeException.class, () -> ReflectionUtils.newObject(C3.class));
		TestUtils.assertThrows(RuntimeException.class, () -> ReflectionUtils.newObject(C4.class));
	}

	public static class C1 {}

	public static class C2 {

		public C2() {}

		public C2(final @SuppressWarnings("unused") String x) {}
	}

	public static class C3 {

		public C3(final @SuppressWarnings("unused") String x) {}
	}

	public static class C4 {

		private C4() {}
	}

	@Test
	public void findFieldTest() {
		assertNotNull(ReflectionUtils.findField(Bean1.class, "boolField"));
		assertNull(ReflectionUtils.findField(Bean1.class, "string"));

		assertNotNull(ReflectionUtils.findField(Bean2.class, "primitive"));
		assertNotNull(ReflectionUtils.findField(Bean2.class, "string"));
		assertNotNull(ReflectionUtils.findField(Bean2.class, "object"));
	}

	@Test
	public void findGetterMethodTest() {
		assertNotNull(ReflectionUtils.findGetterMethod(Bean1.class, "boolField"));
		assertNull(ReflectionUtils.findGetterMethod(Bean1.class, "string"));

		assertNotNull(ReflectionUtils.findGetterMethod(Bean2.class, "primitive"));
		assertNotNull(ReflectionUtils.findGetterMethod(Bean2.class, "string"));
		assertNull(ReflectionUtils.findGetterMethod(Bean2.class, "object"));
	}

	@Test
	public void findSetterMethodTest() {
		assertNotNull(ReflectionUtils.findSetterMethod(Bean1.class, "boolField", boolean.class));
		assertNull(ReflectionUtils.findSetterMethod(Bean1.class, "string", String.class));

		assertNotNull(ReflectionUtils.findSetterMethod(Bean2.class, "primitive", int.class));
		assertNotNull(ReflectionUtils.findSetterMethod(Bean2.class, "string", String.class));
		assertNull(ReflectionUtils.findSetterMethod(Bean2.class, "string", Object.class));
		assertNull(ReflectionUtils.findSetterMethod(Bean2.class, "string"));
		assertNull(ReflectionUtils.findSetterMethod(Bean2.class, "object", Object.class));
	}

	@Test
	public void findfMethodTest() {
		assertNotNull(ReflectionUtils.findMethod(Bean1.class, "isBoolField", new String[] {}));
		assertNotNull(ReflectionUtils.findMethod(Bean1.class, "BoolField", new String[] { "is" }));
		assertNotNull(ReflectionUtils.findMethod(Bean1.class, "boolField", new String[] { "is" }));
		assertNull(ReflectionUtils.findMethod(Bean1.class, "BoolField", new String[] {}));

		assertNotNull(ReflectionUtils.findMethod(Bean2.class, "isBoolField", new String[] {}));
		assertNotNull(ReflectionUtils.findMethod(Bean2.class, "BoolField", new String[] { "is" }));
		assertNotNull(ReflectionUtils.findMethod(Bean2.class, "boolField", new String[] { "is" }));
		assertNull(ReflectionUtils.findMethod(Bean2.class, "BoolField", new String[] {}));

		assertNotNull(ReflectionUtils.findMethod(Bean2.class, "setBoolField", new String[] {}, boolean.class));
		assertNotNull(ReflectionUtils.findMethod(Bean2.class, "BoolField", new String[] { "set" }, boolean.class));
		assertNotNull(ReflectionUtils.findMethod(Bean2.class, "boolField", new String[] { "set" }, boolean.class));
		assertNull(ReflectionUtils.findMethod(Bean2.class, "BoolField", new String[] {}, boolean.class));
	}

	@Test
	public void getMethodSignatureTest() {
		final Set<String> sObj = ReflectionUtils.getMethodSignatures(Object.class);
		// equals, getClass, hashCode, notify, notifyAll, toString, wait (3)
		assertEquals(9, sObj.size());
		final Set<String> s1 = ReflectionUtils.getMethodSignatures(Bean1.class);
		assertEquals(4 + sObj.size(), s1.size());
		final Set<String> s2 = ReflectionUtils.getMethodSignatures(Bean2.class);
		assertEquals(2 + s1.size(), s2.size());
	}

	@Test
	public void getDeclaredMethodSignatureTest() {
		final Set<String> s1 = ReflectionUtils.getDeclaredMethodSignatures(Bean1.class);
		// coverage di eclipse inserisce un metodo statico
		final int extraMethod = s1.stream().filter(s -> s.contains("jacocoInit")).findAny().isPresent() ? 1 : 0;
		assertEquals(4 + extraMethod, s1.size());
		final Set<String> s2 = ReflectionUtils.getDeclaredMethodSignatures(Bean2.class);
		assertEquals(2 + extraMethod, s2.size());
	}

	public static class Bean1 {

		private boolean boolField;
		private int primitive;

		public boolean isBoolField() {
			return this.boolField;
		}

		public void setBoolField(final boolean boolField) {
			this.boolField = boolField;
		}

		public int getPrimitive() {
			return this.primitive;
		}

		public void setPrimitive(final int primitive) {
			this.primitive = primitive;
		}

	}

	public static class Bean2 extends Bean1 {

		private String string;
		@SuppressWarnings("unused")
		private Object object;

		public String getString() {
			return this.string;
		}

		public void setString(final String string) {
			this.string = string;
		}

	}
}
