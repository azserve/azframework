package com.azserve.framework.common.lang;

import static com.azserve.azframework.common.lang.ArrayUtils.equalsAll;
import static com.azserve.azframework.common.lang.ArrayUtils.isNullOrEmpty;
import static com.azserve.azframework.common.lang.ArrayUtils.linearSearch;
import static com.azserve.azframework.common.test.TestUtils.assertThrows;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.stream.IntStream;

import org.junit.Test;

import com.azserve.azframework.common.lang.ArrayUtils;
import com.azserve.azframework.common.test.TestUtils;

public class ArrayUtilsTest {

	private final String[] emptyArray = new String[0];
	private final String[] nonEmptyArray = new String[] { "pippo", "pluto", "paperino", "pluto", "paperone" };
	private final boolean[] trueBooleanArray = new boolean[] { true, true };
	private final boolean[] mixedBooleanArray = new boolean[] { false, false, true, false, true, true };

	@Test
	public void test() {

		assertTrue(isNullOrEmpty(ArrayUtils.EMPTY_STRING));
		assertTrue(isNullOrEmpty(this.emptyArray));
		assertTrue(isNullOrEmpty(null));
		assertFalse(isNullOrEmpty(this.nonEmptyArray));

		assertTrue(linearSearch(this.nonEmptyArray, "pluto") == 1);
		assertFalse(linearSearch(this.nonEmptyArray, "pluto") == 3);
		assertTrue(linearSearch(this.nonEmptyArray, "paperina") == -1);

		assertFalse(equalsAll(this.trueBooleanArray, true, 0, 0));
		assertTrue(equalsAll(this.trueBooleanArray, true, 0, 1));
		assertTrue(equalsAll(this.trueBooleanArray, true, 0, this.trueBooleanArray.length));

		assertFalse(equalsAll(this.mixedBooleanArray, false, 0, 0));
		assertFalse(equalsAll(this.mixedBooleanArray, true, 0, 1));
		assertTrue(equalsAll(this.mixedBooleanArray, false, 0, 1));

		assertTrue(equalsAll(this.mixedBooleanArray, true, 2, 3));
		assertTrue(equalsAll(this.mixedBooleanArray, false, 0, 2));
		assertTrue(equalsAll(this.mixedBooleanArray, true, 4, 6));

		assertThrows(ArrayIndexOutOfBoundsException.class, () -> equalsAll(this.mixedBooleanArray, false, -1, 1));
		assertThrows(ArrayIndexOutOfBoundsException.class, () -> equalsAll(this.mixedBooleanArray, false, this.mixedBooleanArray.length, 1));
		assertThrows(ArrayIndexOutOfBoundsException.class, () -> equalsAll(this.mixedBooleanArray, false, 0, this.mixedBooleanArray.length + 1));
	}

	@Test
	public void concatTest() {
		final Integer[] array1 = IntStream.rangeClosed(-5, -1).mapToObj(Integer::valueOf).toArray(Integer[]::new);
		final Integer[] array2 = IntStream.rangeClosed(0, 5).mapToObj(Integer::valueOf).toArray(Integer[]::new);
		final Integer[] array3 = IntStream.rangeClosed(6, 10).mapToObj(Integer::valueOf).toArray(Integer[]::new);
		final Integer[] concatNonEmptyArrays = ArrayUtils.concatElements(Integer[]::new, array1, Integer.valueOf(10), Integer.valueOf(-100));
		assertFalse(array1 == concatNonEmptyArrays);
		assertTrue(concatNonEmptyArrays.length == array1.length + 2);

		final Integer[] emptyArray1 = new Integer[0];
		final Integer[] concatEmptyArrays = ArrayUtils.concatElements(Integer[]::new, emptyArray1);
		assertTrue(concatEmptyArrays == emptyArray1);

		final Integer[] concatEmptyWithNonEmptyArrays = ArrayUtils.concatElements(Integer[]::new, emptyArray1, array1);
		assertTrue(concatEmptyWithNonEmptyArrays == array1);

		final Integer[] concatThreeArrays = ArrayUtils.concat(Integer[]::new, array1, array2, array3);
		assertTrue(concatThreeArrays.length == array1.length + array2.length + array3.length);
	}

	@Test
	public void isNullOrEmptyTest() {
		assertTrue(isNullOrEmpty(null));
		assertTrue(isNullOrEmpty(new Object[] {}));
		assertTrue(isNullOrEmpty(new String[] {}));
		assertFalse(isNullOrEmpty(new String[] { "Pippo" }));
	}

	@Test
	public void requireNonEmptyTest() {
		assertThrows(IllegalArgumentException.class, () -> ArrayUtils.requireNonEmpty(null, "A message"));
		assertThrows(IllegalArgumentException.class, () -> ArrayUtils.requireNonEmpty(new Integer[] {}, "A message"));
		ArrayUtils.requireNonEmpty(new Object[] { BigDecimal.ZERO }, "A message");
	}

	@Test
	public void linearSearchTest() {
		final String[] array = new String[] { "pippo", null, "pluto", "paperino", "topolino", null, "minnie", "topolino" };
		assertTrue(ArrayUtils.linearSearch(array, "paperone") == -1);
		assertTrue(ArrayUtils.linearSearch(array, "topolino") == 4);
		assertTrue(ArrayUtils.linearSearch(array, null) == 1);
		assertThrows(NullPointerException.class, () -> ArrayUtils.linearSearch(TestUtils.getNull(), "pluto"));
	}

	@Test
	public void shuffleTest() {
		ArrayUtils.shuffle(new char[0]);
		ArrayUtils.shuffle(new int[0]);
		final char[] c = { 'a', 'b', 'c', 'd' };
		ArrayUtils.shuffle(c);
		final int[] i = { 0, 5, 10, 15 };
		ArrayUtils.shuffle(i);
	}
}
