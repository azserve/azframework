package com.azserve.framework.common.util;

import static com.azserve.azframework.common.util.Pair.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.azserve.azframework.common.util.Pair;

public class PairTest {

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void test() {
		final Pair<String, Integer> p1 = of("val1", Integer.valueOf(2));
		assertEquals("val1", p1.getValue1());
		assertEquals(Integer.valueOf(2), p1.getValue2());

		final Pair<String, Integer> p2 = Pair.of(null, Integer.valueOf(2));
		assertNull(p2.getValue1());
		assertEquals(Integer.valueOf(2), p2.getValue2());

		final Pair<String, Integer> p3 = Pair.of("val1", null);
		assertEquals("val1", p3.getValue1());
		assertNull(p3.getValue2());

		final Pair<String, Integer> p4 = Pair.of("val1", Integer.valueOf(4));

		final Pair<String, Integer> pNull = Pair.of(null, null);
		assertNull(pNull.getValue1());
		assertNull(pNull.getValue2());
		final Pair<String, Integer> pNullBis = Pair.of(null, null);

		final Pair<String, Integer> p1Bis = of("val1", Integer.valueOf(2));

		assertFalse(p1.equals(p2));
		assertTrue(p1.equals(p1));
		assertFalse(p1.equals(null));
		assertFalse(p1.equals(""));
		assertTrue(p1.equals(p1Bis));
		assertFalse(p1.equals(pNull));
		assertFalse(p1.equals(p4));
		assertFalse(pNull.equals(p1));
		assertFalse(pNull.equals(p2));
		assertFalse(pNull.equals(p3));
		assertTrue(pNull.equals(pNullBis));

		assertTrue(p1.equals("val1", Integer.valueOf(2)));
		assertFalse(p1.equals(null, Integer.valueOf(2)));
		assertFalse(p1.equals("val1", null));
		assertFalse(p1.equals(null, null));

		assertFalse(pNull.equals("val1", Integer.valueOf(2)));
		assertFalse(pNull.equals(null, Integer.valueOf(2)));
		assertFalse(pNull.equals("val1", null));
		assertTrue(pNull.equals(null, null));

		assertEquals("val1;2", p1.toString());
		assertEquals("null;null", pNull.toString());

		assertEquals(p1.hashCode(), p1Bis.hashCode());
		assertEquals(pNull.hashCode(), pNullBis.hashCode());
	}
}
