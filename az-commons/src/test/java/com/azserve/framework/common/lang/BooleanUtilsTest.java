package com.azserve.framework.common.lang;

import static com.azserve.azframework.common.lang.BooleanUtils.and;
import static com.azserve.azframework.common.lang.BooleanUtils.iif;
import static com.azserve.azframework.common.lang.BooleanUtils.isFalse;
import static com.azserve.azframework.common.lang.BooleanUtils.isTrue;
import static com.azserve.azframework.common.lang.BooleanUtils.not;
import static com.azserve.azframework.common.lang.BooleanUtils.or;
import static com.azserve.azframework.common.lang.BooleanUtils.removeNull;
import static com.azserve.azframework.common.test.TestUtils.assertThrows;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.azserve.azframework.common.lang.BooleanUtils;
import com.azserve.azframework.common.test.TestUtils;

public class BooleanUtilsTest {

	@Test
	public void constructor() throws Throwable {
		TestUtils.testUtilsClassDefinition(BooleanUtils.class);
	}

	@Test
	public void booleanOperatorsTest() {
		assertFalse(and(new boolean[] { false }));
		assertTrue(and(new boolean[] { true }));
		assertFalse(and(new boolean[] { false, false }));
		assertFalse(and(new boolean[] { false, true }));
		assertTrue(and(new boolean[] { true, true }));

		assertFalse(and(null, null));
		assertFalse(and(TRUE, null));
		assertFalse(and(FALSE, null));
		assertFalse(and(FALSE, FALSE));
		assertFalse(and(TRUE, FALSE));
		assertTrue(and(TRUE, TRUE));

		assertFalse(or(new boolean[] { false }));
		assertTrue(or(new boolean[] { true }));
		assertFalse(or(new boolean[] { false, false }));
		assertTrue(or(new boolean[] { false, true }));
		assertTrue(or(new boolean[] { true, true }));

		assertFalse(or(null, null));
		assertTrue(or(TRUE, null));
		assertFalse(or(FALSE, null));
		assertFalse(or(FALSE, FALSE));
		assertTrue(or(TRUE, FALSE));
		assertTrue(or(TRUE, TRUE));

		assertTrue(not(null) == null);
		assertFalse(not(TRUE).booleanValue());
		assertTrue(not(FALSE).booleanValue());
	}

	@Test
	public void emptyOr() {
		assertThrows(IllegalArgumentException.class, () -> or());
	}

	@Test
	public void emptyAnd() {
		assertThrows(IllegalArgumentException.class, () -> and());
	}

	@Test
	public void ifTest() {
		final Object obj1 = new Object();
		final Object obj2 = new Object();

		assertTrue(iif(null, obj1, obj2) == null);
		assertTrue(iif(TRUE, obj1, obj2) == obj1);
		assertTrue(iif(FALSE, obj1, obj2) == obj2);

		assertTrue(isTrue(TRUE));
		assertFalse(isTrue(FALSE));
		assertFalse(isTrue(null));

		assertFalse(isFalse(TRUE));
		assertTrue(isFalse(FALSE));
		assertFalse(isFalse(null));
	}

	@Test
	public void otherTest() {
		assertTrue(removeNull(null, true) == true);
		assertTrue(removeNull(FALSE, true) == false);
		assertTrue(removeNull(TRUE, true) == true);

		final String nullString = "N";
		final String trueString = "T";
		final String falseString = "F";

		assertTrue(BooleanUtils.toString(null, nullString, trueString, falseString) == nullString);
		assertTrue(BooleanUtils.toString(TRUE, nullString, trueString, falseString) == trueString);
		assertTrue(BooleanUtils.toString(FALSE, nullString, trueString, falseString) == falseString);

		assertFalse(BooleanUtils.fromInt(0));
		assertTrue(BooleanUtils.fromInt(1));
		assertTrue(BooleanUtils.fromInt(-1));

		assertTrue(BooleanUtils.toInt(true) == 1);
		assertTrue(BooleanUtils.toInt(false) == 0);

	}

}
