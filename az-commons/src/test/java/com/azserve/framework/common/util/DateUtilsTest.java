package com.azserve.framework.common.util;

import static com.azserve.azframework.common.util.DateUtils.year2Digits;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.Year;
import java.util.Date;

import org.junit.Test;

import com.azserve.azframework.common.test.TestUtils;
import com.azserve.azframework.common.util.DateUtils;

public class DateUtilsTest {

	@Test
	public void constructor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.testUtilsClassDefinition(DateUtils.class);
	}

	@Test
	public void nowTest() {
		final Date d1 = new Date();
		final Date d2 = DateUtils.now();
		final Date d3 = new Date();
		assertTrue(!d1.after(d2));
		assertTrue(!d3.before(d2));
	}

	@Test
	public void year2DigitTest() {
		final int currentYear = Year.now().getValue();
		assertEquals(currentYear, year2Digits(currentYear));
		assertEquals(currentYear, year2Digits(currentYear % 100));
		assertEquals(currentYear - 80, year2Digits((currentYear + 20) % 100));
		assertEquals(currentYear + 5, year2Digits((currentYear + 5) % 100));
		assertEquals(currentYear - 5, year2Digits((currentYear - 5) % 100));

		assertEquals(year2Digits(2015, 2027), 2015);
		assertEquals(year2Digits(3, 2027), 2003);
		assertEquals(year2Digits(15, 2027), 2015);
		assertEquals(year2Digits(18, 2027), 2018);
		assertEquals(year2Digits(27, 2027), 2027);
		assertEquals(year2Digits(28, 2027), 1928);
		assertEquals(year2Digits(99, 2027), 1999);

		assertEquals(year2Digits(2015, 2005), 2015);
		assertEquals(year2Digits(3, 2005), 2003);
		assertEquals(year2Digits(15, 2005), 1915);
		assertEquals(year2Digits(18, 2005), 1918);
		assertEquals(year2Digits(27, 2005), 1927);
		assertEquals(year2Digits(28, 2005), 1928);
		assertEquals(year2Digits(99, 2005), 1999);
	}

	@Test
	public void isBetweenTest() {
		final Date now = DateUtils.now();
		final Date today = DateUtils.getDate(now);
		final Date tomorrow = DateUtils.tomorrow();
		assertTrue(DateUtils.isBetween(now, today, tomorrow));
		assertTrue(DateUtils.isBetween(today, today, tomorrow));
		assertTrue(DateUtils.isBetween(tomorrow, today, tomorrow));
	}
}
