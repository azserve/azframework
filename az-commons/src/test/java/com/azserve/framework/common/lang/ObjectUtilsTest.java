package com.azserve.framework.common.lang;

import static com.azserve.azframework.common.lang.ObjectUtils.allNull;
import static com.azserve.azframework.common.lang.ObjectUtils.allNullOrEmpty;
import static com.azserve.azframework.common.lang.ObjectUtils.anyNull;
import static com.azserve.azframework.common.lang.ObjectUtils.checkEquality;
import static com.azserve.azframework.common.lang.ObjectUtils.compareList;
import static com.azserve.azframework.common.lang.ObjectUtils.compareObject;
import static com.azserve.azframework.common.lang.ObjectUtils.copyObject;
import static com.azserve.azframework.common.lang.ObjectUtils.ifNull;
import static com.azserve.azframework.common.lang.ObjectUtils.isNullOrEmpty;
import static com.azserve.azframework.common.lang.ObjectUtils.max;
import static com.azserve.azframework.common.lang.ObjectUtils.min;
import static com.azserve.azframework.common.lang.ObjectUtils.nullSafeCompare;
import static com.azserve.azframework.common.test.TestUtils.assertThrows;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import org.junit.Test;

import com.azserve.azframework.common.lang.ObjectUtils;
import com.azserve.azframework.common.test.TestUtils;

public class ObjectUtilsTest {

	@Test
	public void constructor() throws Throwable {
		TestUtils.testUtilsClassDefinition(ObjectUtils.class);
	}

	@Test
	public void allNullTest() {
		assertTrue(allNull());
		assertTrue(allNull((Object) null));
		assertTrue(allNull((Object) null, (Object) null));
		assertFalse(allNull((String) null, ""));
		assertFalse(allNull("", ""));

	}

	@Test
	public void allNullOrEmptyTest() {
		assertTrue(allNullOrEmpty());
		assertTrue(allNullOrEmpty((Object) null));
		assertTrue(allNullOrEmpty((Object) null, (Object) null));
		assertTrue(allNullOrEmpty((String) null, ""));
		assertTrue(allNullOrEmpty("", ""));
		assertFalse(allNullOrEmpty((String) null, "x"));
		assertFalse(allNullOrEmpty("x", "y"));
		assertFalse(allNullOrEmpty(Collections.singletonList("x"), Collections.emptyList()));
		assertTrue(allNullOrEmpty(Collections.emptyList(), null));
		assertTrue(allNullOrEmpty(Collections.emptyMap(), null));
		assertFalse(allNullOrEmpty(Collections.singletonMap("k", "v"), null));
		assertFalse(allNullOrEmpty(new Object(), null));

	}

	@Test
	public void anyNullTest() {
		assertFalse(anyNull());
		assertTrue(anyNull((Object) null));
		assertTrue(anyNull((Object) null, (Object) null));
		assertTrue(anyNull((String) null, ""));
		assertFalse(anyNull("", ""));

	}

	@Test
	public void checkEqualityTest() {
		assertTrue(checkEquality(null, null));
		assertTrue(checkEquality("", ""));
		assertTrue(checkEquality("a", "a"));
		assertFalse(checkEquality("a", "b"));
		assertFalse(checkEquality("a", null));
		assertFalse(checkEquality("a", (String) null));
		final Object o = new Object();
		assertTrue(checkEquality(o, o));
		assertFalse(checkEquality(o, new Object()));

	}

	@Test
	public void compareListTest() {
		// ???
		assertTrue(compareList(Collections.emptyList(), Collections.emptyList()));
		assertTrue(compareList(Collections.singletonList("a"), Collections.singletonList("a")));
		assertFalse(compareList(Collections.singletonList("a"), Collections.singletonList("b")));
		assertTrue(compareList(Arrays.asList("a", "b"), Arrays.asList("a", "b")));
		assertFalse(compareList(Arrays.asList("a", "b"), Arrays.asList("b", "a")));
		assertFalse(compareList(Arrays.asList("a", "b"), Arrays.asList("a", "b", "c")));

	}

	@Test
	public void compareObjectTest() {
		assertTrue(compareObject(null, null));
		assertTrue(compareObject(new Object(), new Object()));
		final TestBean b1 = new TestBean();
		b1.setField1("");
		b1.setField2(Integer.valueOf(0));
		b1.setField3(false);
		final TestBean b2 = new TestBean();
		b2.setField1("");
		b2.setField2(Integer.valueOf(0));
		b2.setField3(false);
		assertTrue(compareObject(b1, b2));
		b2.setField3(true);
		assertFalse(compareObject(b1, b2));
		assertFalse(compareObject(b1, null));
		assertFalse(compareObject(null, b1));
		assertFalse(compareObject(b1, ""));
	}

	@Test
	public void copyObjectTest() {
		final TestBean b1 = new TestBean();
		b1.setField1("");
		b1.setField2(Integer.valueOf(0));
		b1.setField3(false);
		final TestBean b2 = new TestBean();
		copyObject(TestBean.class, b1, b2);
		assertTrue(compareObject(b1, b2));
	}

	@Test
	public void equalsTest() {
		assertTrue(ObjectUtils.equals(new Object[] {}));
		assertTrue(ObjectUtils.equals(new Object[] { new Object() }));
		assertTrue(ObjectUtils.equals(new String[] { "", "" }));
		assertTrue(ObjectUtils.equals(new String[] { "a", "a" }));
		assertFalse(ObjectUtils.equals(new String[] { "a", null }));
		assertTrue(ObjectUtils.equals(new String[] { null, null }));
		assertFalse(ObjectUtils.equals(new String[] { null, "a" }));

		assertFalse(ObjectUtils.equals("", new Object[] {}));
		assertTrue(ObjectUtils.equals("a", new Object[] { "a" }));
		assertTrue(ObjectUtils.equals("a", new Object[] { null, "a" }));
		assertTrue(ObjectUtils.equals("a", new Object[] { "a", null }));
		assertFalse(ObjectUtils.equals("a", new Object[] { "b", null }));
		assertThrows(NullPointerException.class, () -> ObjectUtils.equals(TestUtils.getNull(), new Object[] { "a", null }));
	}

	@Test
	public void hasModifiedFieldsTest() {
		// hasModifiedFields(Object)

	}

	@Test
	public void ifNullTest() {
		assertNull(ifNull(null, (Object) null));
		assertEquals("a", ifNull(null, "a"));
		assertEquals("b", ifNull("b", "a"));
	}

	@Test
	public void isNullOrEmptyTest() {
		assertTrue(isNullOrEmpty(null));
		assertTrue(isNullOrEmpty(""));
		assertTrue(isNullOrEmpty(Collections.emptyList()));
		assertTrue(isNullOrEmpty(Collections.emptyMap()));
		assertFalse(isNullOrEmpty(new Object()));
		assertFalse(isNullOrEmpty("a"));
		assertFalse(isNullOrEmpty(Collections.singletonList("")));
		assertFalse(isNullOrEmpty(Collections.singletonMap("K", "V")));

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void maxTest() {
		assertNull(max());
		assertNull(max((Comparable) null));
		assertEquals(Integer.valueOf(3), max(Integer.valueOf(3), Integer.valueOf(3), Integer.valueOf(3)));
		assertEquals(Integer.valueOf(3), max(Integer.valueOf(1), Integer.valueOf(3), Integer.valueOf(2)));
		assertEquals(Integer.valueOf(3), max(Integer.valueOf(3), Integer.valueOf(1), Integer.valueOf(2)));
		assertEquals(Integer.valueOf(3), max(Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3)));
		assertEquals(Integer.valueOf(3), max(Integer.valueOf(1), null, Integer.valueOf(3)));

		assertEquals("bb3", max(Comparator.comparing(String::length), "", "1", "z2", "bb3"));
		assertEquals("bb3", max(Comparator.comparing(String::length), null, "1", "z2", "bb3"));
		assertNull(max(Comparator.comparing(String::length), (String) null));
		assertNull(max(Comparator.comparing(String::length), new String[0]));

		assertEquals(Integer.valueOf(3), max(Integer.valueOf(1), Integer.valueOf(3)));
		assertEquals(Integer.valueOf(3), max(Integer.valueOf(3), Integer.valueOf(1)));
		assertEquals(Integer.valueOf(3), max(null, Integer.valueOf(3)));
		// due stringhe con lo stesso valore ma diverse
		final String s1 = "a1";
		final String s2 = "a" + Integer.valueOf(1);
		assertFalse(s1 == s2);
		assertTrue(s1 == max(s1, s2));

		assertEquals("bb3", max(Comparator.comparing(String::length), "zz", "bb3"));
		assertEquals("zz", max(Comparator.comparing(String::length), "zz", "bb"));
		assertEquals("bb3", max(Comparator.comparing(String::length), null, "bb3"));

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void minTest() {
		assertNull(min());
		assertNull(min((Comparable) null));
		assertEquals(Integer.valueOf(3), min(Integer.valueOf(3), Integer.valueOf(3), Integer.valueOf(3)));
		assertEquals(Integer.valueOf(1), min(Integer.valueOf(1), Integer.valueOf(3), Integer.valueOf(2)));
		assertEquals(Integer.valueOf(1), min(Integer.valueOf(3), Integer.valueOf(1), Integer.valueOf(2)));
		assertEquals(Integer.valueOf(1), min(Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3)));
		assertEquals(Integer.valueOf(1), min(Integer.valueOf(1), null, Integer.valueOf(3)));

		assertEquals("", min(Comparator.comparing(String::length), "", "1", "z2", "bb3"));
		assertEquals("1", min(Comparator.comparing(String::length), null, "1", "z2", "bb3"));
		assertNull(min(Comparator.comparing(String::length), (String) null));
		assertNull(min(Comparator.comparing(String::length), new String[0]));

		assertEquals(Integer.valueOf(1), min(Integer.valueOf(1), Integer.valueOf(3)));
		assertEquals(Integer.valueOf(1), min(Integer.valueOf(3), Integer.valueOf(1)));
		assertEquals(Integer.valueOf(3), min(null, Integer.valueOf(3)));
		// due stringhe con lo stesso valore ma diverse
		final String s1 = "a1";
		final String s2 = "a" + Integer.valueOf(1);
		assertFalse(s1 == s2);
		assertTrue(s1 == min(s1, s2));

		assertEquals("zz", min(Comparator.comparing(String::length), "bb3", "zz"));
		assertEquals("zz", max(Comparator.comparing(String::length), "zz", "bb"));
		assertNull(min(Comparator.comparing(String::length), null, "bb3"));

	}

	@Test
	public void nullSafeCompareTest() {
		assertEquals(0, nullSafeCompare(null, null));
		assertEquals(0, nullSafeCompare("a", "a"));
		assertEquals(1, nullSafeCompare("", null));
		assertEquals(-1, nullSafeCompare(null, ""));
		assertEquals(-1, nullSafeCompare("a", "b"));
		assertEquals(1, nullSafeCompare("b", "a"));

		assertEquals(0, nullSafeCompare(Comparator.comparing(String::length), (String) null, (String) null));
		assertEquals(0, nullSafeCompare(Comparator.comparing(String::length), "a", "a"));
		assertEquals(1, nullSafeCompare(Comparator.comparing(String::length), "", null));
		assertEquals(-1, nullSafeCompare(Comparator.comparing(String::length), null, ""));
		assertEquals(1, nullSafeCompare(Comparator.comparing(String::length), "a2", "b"));
		assertEquals(-1, nullSafeCompare(Comparator.comparing(String::length), "b", "a2"));

	}

	public static class TestBean {

		String field1;
		Integer field2;
		boolean field3;

		public String getField1() {
			return this.field1;
		}

		public void setField1(final String field1) {
			this.field1 = field1;
		}

		public Integer getField2() {
			return this.field2;
		}

		public void setField2(final Integer field2) {
			this.field2 = field2;
		}

		public boolean isField3() {
			return this.field3;
		}

		public void setField3(final boolean field3) {
			this.field3 = field3;
		}

	}
}
