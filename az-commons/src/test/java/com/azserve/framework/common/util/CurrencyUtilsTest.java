package com.azserve.framework.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.Currency;

import org.junit.Test;

import com.azserve.azframework.common.test.TestUtils;
import com.azserve.azframework.common.util.CurrencyUtils;

public class CurrencyUtilsTest {

	private final CurrencyUtils EUR = new CurrencyUtils("EUR");
	private final CurrencyUtils EUR_EXTRA = new CurrencyUtils("EUR", 5);

	@Test
	public void testCurrencyUtilsCurrency() {
		final CurrencyUtils usd = new CurrencyUtils(Currency.getInstance("USD"));
		assertEquals(0, usd.round(new BigDecimal("0.123")).compareTo(new BigDecimal("0.12")));
	}

	@Test
	public void testCurrencyUtilsString() {
		// fonte https://it.wikipedia.org/wiki/ISO_4217
		// Dinaro del Bahrain "BHD" ha tre decimali
		final CurrencyUtils bhd = new CurrencyUtils("BHD");
		assertEquals(0, bhd.round(new BigDecimal("0.1234")).compareTo(new BigDecimal("0.123")));
	}

	@Test
	public void testCurrencyUtilsCurrencyInt() {
		// fonte https://it.wikipedia.org/wiki/ISO_4217
		// YEN "JPY" non ha decimali
		final CurrencyUtils jpy = new CurrencyUtils(Currency.getInstance("JPY"), 3);
		assertEquals(0, jpy.round(new BigDecimal("10.55")).compareTo(new BigDecimal("11")));
	}

	@Test
	public void testCurrencyUtilsStringInt() {
		final CurrencyUtils eur = new CurrencyUtils("EUR", 3);
		assertEquals(0, eur.round(new BigDecimal("0.123")).compareTo(new BigDecimal("0.12")));
	}

	@Test
	public void testRound() {
		assertEquals(0, this.EUR.round(new BigDecimal("0.123")).compareTo(new BigDecimal("0.12")));
		assertEquals(0, this.EUR.round(new BigDecimal("0.125")).compareTo(new BigDecimal("0.13")));
		assertEquals(0, this.EUR_EXTRA.round(new BigDecimal("0.123")).compareTo(new BigDecimal("0.12")));
		assertEquals(0, this.EUR_EXTRA.round(new BigDecimal("0.125")).compareTo(new BigDecimal("0.13")));
		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.round(TestUtils.getNull()));
	}

	@Test
	public void testGetTaxable() {
		assertEquals(0, this.EUR.getTaxable(new BigDecimal("1"), new BigDecimal("22")).compareTo(new BigDecimal("0.82")));
		assertEquals(0, this.EUR.getTaxable(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("0.80")));
		assertEquals(0, this.EUR_EXTRA.getTaxable(new BigDecimal("1"), new BigDecimal("22")).compareTo(new BigDecimal("0.82")));
		assertEquals(0, this.EUR_EXTRA.getTaxable(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("0.80")));

		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getTaxable(TestUtils.getNull(), BigDecimal.ONE));
		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getTaxable(BigDecimal.ONE, TestUtils.getNull()));
	}

	@Test
	public void testGetTaxableExtraPrecision() {
		assertEquals(0, this.EUR.getTaxableExtraPrecision(new BigDecimal("1"), new BigDecimal("22")).compareTo(new BigDecimal("0.81967")));
		assertEquals(0, this.EUR.getTaxableExtraPrecision(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("0.80328")));
		assertEquals(0, this.EUR_EXTRA.getTaxableExtraPrecision(new BigDecimal("1"), new BigDecimal("22")).compareTo(new BigDecimal("0.8196721")));
		assertEquals(0, this.EUR_EXTRA.getTaxableExtraPrecision(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("0.8032787")));

		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getTaxableExtraPrecision(TestUtils.getNull(), BigDecimal.ONE));
		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getTaxableExtraPrecision(BigDecimal.ONE, TestUtils.getNull()));
	}

	@Test
	public void testGetVat() {
		assertEquals(0, this.EUR.getVat(new BigDecimal("1"), new BigDecimal("22")).compareTo(new BigDecimal("0.22")));
		assertEquals(0, this.EUR.getVat(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("0.22")));
		assertEquals(0, this.EUR_EXTRA.getVat(new BigDecimal("1"), new BigDecimal("22")).compareTo(new BigDecimal("0.22")));
		assertEquals(0, this.EUR_EXTRA.getVat(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("0.22")));

		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getVat(TestUtils.getNull(), BigDecimal.ONE));
		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getVat(BigDecimal.ONE, TestUtils.getNull()));
	}

	@Test
	public void testGetVatExtraPrecision() {
		assertEquals(0, this.EUR.getVatExtraPrecision(new BigDecimal("1"), new BigDecimal("22")).compareTo(new BigDecimal("0.22")));
		assertEquals(0, this.EUR.getVatExtraPrecision(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("0.2156")));
		assertEquals(0, this.EUR_EXTRA.getVatExtraPrecision(new BigDecimal("1.01"), new BigDecimal("22")).compareTo(new BigDecimal("0.2222")));
		assertEquals(0, this.EUR_EXTRA.getVatExtraPrecision(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("0.2156")));

		assertEquals(0, this.EUR.getVatExtraPrecision(new BigDecimal("1.01"), new BigDecimal("22.22")).compareTo(new BigDecimal("0.22442")));
		assertEquals(0, this.EUR.getVatExtraPrecision(new BigDecimal("0.98"), new BigDecimal("22.22")).compareTo(new BigDecimal("0.21776")));
		assertEquals(0, this.EUR_EXTRA.getVatExtraPrecision(new BigDecimal("1.01"), new BigDecimal("22.22")).compareTo(new BigDecimal("0.224422")));
		assertEquals(0, this.EUR_EXTRA.getVatExtraPrecision(new BigDecimal("0.98"), new BigDecimal("22.22")).compareTo(new BigDecimal("0.217756")));

		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getVatExtraPrecision(TestUtils.getNull(), BigDecimal.ONE));
		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getVatExtraPrecision(BigDecimal.ONE, TestUtils.getNull()));
	}

	@Test
	public void testGetTotal() {
		assertEquals(0, this.EUR.getTotal(new BigDecimal("1"), new BigDecimal("22")).compareTo(new BigDecimal("1.22")));
		assertEquals(0, this.EUR.getTotal(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("1.20")));
		assertEquals(0, this.EUR_EXTRA.getTotal(new BigDecimal("1"), new BigDecimal("22")).compareTo(new BigDecimal("1.22")));
		assertEquals(0, this.EUR_EXTRA.getTotal(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("1.20")));

		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getTotal(TestUtils.getNull(), BigDecimal.ONE));
		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getTotal(BigDecimal.ONE, TestUtils.getNull()));
	}

	@Test
	public void testGetTotalExtraPrecision() {
		assertEquals(0, this.EUR.getTotalExtraPrecision(new BigDecimal("1"), new BigDecimal("22")).compareTo(new BigDecimal("1.22")));
		assertEquals(0, this.EUR.getTotalExtraPrecision(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("1.1956")));
		assertEquals(0, this.EUR_EXTRA.getTotalExtraPrecision(new BigDecimal("1"), new BigDecimal("22")).compareTo(new BigDecimal("1.22")));
		assertEquals(0, this.EUR_EXTRA.getTotalExtraPrecision(new BigDecimal("0.98"), new BigDecimal("22")).compareTo(new BigDecimal("1.1956")));

		assertEquals(0, this.EUR.getTotalExtraPrecision(new BigDecimal("1.01"), new BigDecimal("22.22")).compareTo(new BigDecimal("1.23442")));
		assertEquals(0, this.EUR.getTotalExtraPrecision(new BigDecimal("0.98"), new BigDecimal("22.22")).compareTo(new BigDecimal("1.19776")));
		assertEquals(0, this.EUR_EXTRA.getTotalExtraPrecision(new BigDecimal("1.01"), new BigDecimal("22.22")).compareTo(new BigDecimal("1.234422")));
		assertEquals(0, this.EUR_EXTRA.getTotalExtraPrecision(new BigDecimal("0.98"), new BigDecimal("22.22")).compareTo(new BigDecimal("1.197756")));

		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getTotalExtraPrecision(TestUtils.getNull(), BigDecimal.ONE));
		TestUtils.assertThrows(NullPointerException.class, () -> this.EUR.getTotalExtraPrecision(BigDecimal.ONE, TestUtils.getNull()));
	}

	@Test
	public void testDiscount() {
		assertNull(CurrencyUtils.getDiscount("a"));
		assertNull(CurrencyUtils.getDiscount("3+3a"));
		assertEquals(0, CurrencyUtils.getDiscount("").compareTo(BigDecimal.ZERO));
		assertEquals(0, CurrencyUtils.getDiscount(" ").compareTo(BigDecimal.ZERO));
		assertEquals(0, CurrencyUtils.getDiscount(null).compareTo(BigDecimal.ZERO));
		assertEquals(0, CurrencyUtils.getDiscount("0").compareTo(BigDecimal.ZERO));
		assertEquals(0, CurrencyUtils.getDiscount("0+0").compareTo(BigDecimal.ZERO));
		assertEquals(0, CurrencyUtils.getDiscount("3").compareTo(new BigDecimal("3")));
		assertEquals(0, CurrencyUtils.getDiscount("3,5").compareTo(new BigDecimal("3.5")));
		assertEquals(0, CurrencyUtils.getDiscount("3.5").compareTo(new BigDecimal("3.5")));
		assertEquals(0, CurrencyUtils.getDiscount("10+10").compareTo(new BigDecimal("19")));
		assertEquals(0, CurrencyUtils.getDiscount(" 10,0 + 10.0   ").compareTo(new BigDecimal("19")));
		assertEquals(0, CurrencyUtils.getDiscount(" 50,0 + 20.0  + 10 + 5").compareTo(new BigDecimal("65.8")));

	}
}
