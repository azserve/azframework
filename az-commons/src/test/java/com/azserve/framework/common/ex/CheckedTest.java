package com.azserve.framework.common.ex;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.azserve.azframework.common.ex.Checked;
import com.azserve.azframework.common.ex.CheckedConsumer;
import com.azserve.azframework.common.ex.CheckedFunction;

public class CheckedTest {

	@Test
	public void testConsumerToFunction() {
		final int[] array = new int[1];

		{
			final CheckedConsumer<int[], RuntimeException> consumer = a -> a[0] = 10;
			final CheckedFunction<int[], Void, RuntimeException> function = Checked.consumerToFunction(consumer);
			function.apply(array);
			assertEquals(array[0], 10);
		}

		{
			final CheckedConsumer<int[], Exception> consumerThrowingException = a -> {
				throw new Exception();
			};
			final CheckedFunction<int[], Void, Exception> functionThrowingException = Checked.consumerToFunction(consumerThrowingException);
			try {
				functionThrowingException.apply(array);
				fail();
			} catch (final @SuppressWarnings("unused") Exception ex) {
				assertTrue(true);
			}
		}
	}

}
