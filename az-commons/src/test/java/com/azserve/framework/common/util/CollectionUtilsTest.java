package com.azserve.framework.common.util;

import static com.azserve.azframework.common.util.CollectionUtils.binarySearch;
import static com.azserve.azframework.common.util.CollectionUtils.findAny;
import static com.azserve.azframework.common.util.CollectionUtils.findFirst;
import static com.azserve.azframework.common.util.CollectionUtils.get;
import static com.azserve.azframework.common.util.CollectionUtils.getFirstOrNull;
import static com.azserve.azframework.common.util.CollectionUtils.hasElements;
import static com.azserve.azframework.common.util.CollectionUtils.isNullOrEmpty;
import static com.azserve.azframework.common.util.CollectionUtils.isValidIndex;
import static com.azserve.azframework.common.util.CollectionUtils.singletonList;
import static com.azserve.azframework.common.util.CollectionUtils.singletonSet;
import static com.azserve.azframework.common.util.CollectionUtils.streamOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.azserve.azframework.common.test.TestUtils;
import com.azserve.azframework.common.util.CollectionUtils;

public class CollectionUtilsTest {

	private final Collection<String> emptyCollection = Collections.emptySet();
	private final Collection<String> oneElementCollection = Collections.singleton("ciao");
	private final Collection<String> moreElementsCollection = Arrays.asList("ciao", "pippo");

	private final List<String> emptyList = Collections.emptyList();
	private final List<String> twoElementsList = Arrays.asList("ciao", "pippo");

	@Test
	public void constructor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.testUtilsClassDefinition(CollectionUtils.class);
	}

	@Test
	public void testStreamOf() {
		assertEquals(3, streamOf(this.twoElementsList, this.oneElementCollection).count());
	}

	@Test
	public void testSingletonList() {
		final List<String> singletonList = singletonList("X");
		assertEquals("X", singletonList.get(0));
		assertEquals(1, singletonList.size());
		assertTrue(singletonList(null).isEmpty());
	}

	@Test
	public void testSingletonSet() {
		final Set<String> singletonSet = singletonSet("X");
		assertEquals("X", singletonSet.iterator().next());
		assertEquals(1, singletonSet.size());
		assertTrue(singletonList(null).isEmpty());
	}

	@Test
	public void testFindFirst() {
		assertFalse(findFirst(this.emptyList).isPresent());
		assertEquals("ciao", findFirst(this.twoElementsList).get());
	}

	@Test
	public void testFindAny() {
		assertFalse(findAny(this.emptyCollection).isPresent());
		assertTrue(findAny(this.moreElementsCollection).isPresent());
	}

	@Test
	public void testGetFirstOrNull() {
		assertEquals(null, getFirstOrNull(this.emptyCollection));
		assertEquals("ciao", getFirstOrNull(this.oneElementCollection));
		assertNotNull(getFirstOrNull(this.moreElementsCollection));
	}

	@Test
	public void testIsNullOrEmpty() {
		assertTrue(isNullOrEmpty(null));
		assertTrue(isNullOrEmpty(this.emptyCollection));
		assertFalse(isNullOrEmpty(this.moreElementsCollection));
	}

	@Test
	public void testHasElements() {
		assertFalse(hasElements(null));
		assertFalse(hasElements(this.emptyCollection));
		assertTrue(hasElements(this.moreElementsCollection));
	}

	@Test
	public void testIsValidIndex() {
		assertFalse(isValidIndex(this.emptyList, -1));
		assertFalse(isValidIndex(this.emptyList, 0));
		assertFalse(isValidIndex(this.twoElementsList, -1));
		assertTrue(isValidIndex(this.twoElementsList, 0));
		assertFalse(isValidIndex(this.twoElementsList, 2));
	}

	@Test
	public void testGet() {
		assertTrue(get(this.twoElementsList, 0).isPresent());
		assertFalse(get(this.emptyList, 0).isPresent());
	}

	@Test
	public void testFindLastEntry() {
		// fail("Not yet implemented");
	}

	@Test
	public void testBinarySearch() {
		final List<ObjectWithKey> list = Arrays.asList(
				new ObjectWithKey(new Key("C")),
				new ObjectWithKey(new Key("A")),
				new ObjectWithKey(new Key("D")),
				new ObjectWithKey(new Key("B")),
				new ObjectWithKey(new Key("E")));

		list.sort(Comparator.comparing(obj -> obj.getKey().getId()));

		/*
		 * la lista ora ha gli oggetti TestObject ordinati con chiave A,B,C,D,E
		 *
		 * mi aspetto che:
		 *
		 * la ricerca per chiave D restituisca indice in quarta posizione (ix = 3)
		 * la ricerca per chiave A restituisca indice in prima posizione (ix = 0)
		 * la ricerca per chiave non presente restituisca indice negativo
		 */

		assertEquals(3, binarySearch(list, Key.class, new Key("D"), ObjectWithKey::getKey, Comparator.comparing(Key::getId)));
		assertEquals(0, binarySearch(list, Key.class, new Key("A"), ObjectWithKey::getKey, Comparator.comparing(Key::getId)));
		assertTrue(binarySearch(list, Key.class, new Key("F"), ObjectWithKey::getKey, Comparator.comparing(Key::getId)) < 0);
	}

	@Test
	public void testRequireElements() {
		// fail("Not yet implemented");
	}

	@Test
	public void testRemove() {
		// fail("Not yet implemented");
	}

	@Test
	public void testMinCollectionOfT() {
		// fail("Not yet implemented");
	}

	@Test
	public void testMinCollectionOfTComparatorOfQsuperT() {
		// fail("Not yet implemented");
	}

	@Test
	public void testMaxCollectionOfT() {
		// fail("Not yet implemented");
	}

	@Test
	public void testMaxCollectionOfTComparatorOfQsuperT() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToSortedList() {
		// fail("Not yet implemented");
	}

	@Test
	public void testAddAll() {
		// fail("Not yet implemented");
	}

	@Test
	public void testSetOf() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToMap() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToArray() {
		// fail("Not yet implemented");
	}

	@Test
	public void testMatch() {
		// fail("Not yet implemented");
	}

	private static class Key {

		private final String id;

		Key(final String id) {
			this.id = id;
		}

		public String getId() {
			return this.id;
		}
	}

	private static final class ObjectWithKey {

		private final Key key;

		ObjectWithKey(final Key key) {
			this.key = key;
		}

		public Key getKey() {
			return this.key;
		}
	}
}
