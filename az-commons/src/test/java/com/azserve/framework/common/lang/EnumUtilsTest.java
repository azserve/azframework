package com.azserve.framework.common.lang;

import static com.azserve.azframework.common.enums.EnumUtils.description;
import static com.azserve.azframework.common.enums.EnumUtils.exists;
import static com.azserve.azframework.common.enums.EnumUtils.key;
import static com.azserve.azframework.common.enums.EnumUtils.name;
import static com.azserve.azframework.common.enums.EnumUtils.safeValueOf;
import static com.azserve.azframework.common.enums.EnumUtils.safeValueOfKey;
import static com.azserve.azframework.common.enums.EnumUtils.valueOf;
import static com.azserve.azframework.common.enums.EnumUtils.valueOfKey;
import static com.azserve.azframework.common.test.TestUtils.assertThrows;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.azserve.azframework.common.enums.EnumWithDescription;
import com.azserve.azframework.common.enums.EnumWithKey;

public class EnumUtilsTest {

	private static final String KEY_MOTORCYCLE = "M";
	private static final String KEY_CAR = "C";
	private static final String KEY_TRUCK = "T";
	private static final String INVALID_KEY = "R";

	private enum VehicleType implements EnumWithKey<String>, EnumWithDescription {

		MOTORCYCLE(KEY_MOTORCYCLE),
		CAR(KEY_CAR),
		TRUCK(KEY_TRUCK);

		private final String key;

		VehicleType(final String key) {
			this.key = key;
		}

		@Override
		public String key() {
			return this.key;
		}

		@Override
		public String description() {
			// CAR = Car
			return this.name().substring(0, 1) + this.name().substring(1).toLowerCase();
		}
	}

	@Test
	public void testExists() {
		assertTrue(exists(VehicleType.class, "CAR"));
		assertFalse(exists(VehicleType.class, "car"));
		assertFalse(exists(VehicleType.class, "TRAIN"));
		assertFalse(exists(VehicleType.class, ""));
		assertFalse(exists(VehicleType.class, null));
	}

	@Test
	public void testValueOf() {
		assertTrue(VehicleType.TRUCK == valueOf(VehicleType.class, "TRUCK"));
		assertFalse(VehicleType.TRUCK == valueOf(VehicleType.class, "CAR"));
		assertTrue(null == valueOf(VehicleType.class, null));
		assertThrows(IllegalArgumentException.class, () -> valueOf(VehicleType.class, ""));
		assertThrows(IllegalArgumentException.class, () -> valueOf(VehicleType.class, "TRAIN"));
	}

	@Test
	public void testSafeValueOfClassOfTString() {
		assertTrue(VehicleType.TRUCK == safeValueOf(VehicleType.class, "TRUCK"));
		assertFalse(VehicleType.TRUCK == safeValueOf(VehicleType.class, "CAR"));
		assertTrue(null == safeValueOf(VehicleType.class, null));
		assertTrue(null == safeValueOf(VehicleType.class, ""));
		assertTrue(null == safeValueOf(VehicleType.class, "TRAIN"));
	}

	@Test
	public void testSafeValueOfClassOfTStringT() {
		assertTrue(VehicleType.TRUCK == safeValueOf(VehicleType.class, "TRUCK", VehicleType.CAR));
		assertFalse(VehicleType.TRUCK == safeValueOf(VehicleType.class, "CAR", VehicleType.TRUCK));
		assertTrue(null == safeValueOf(VehicleType.class, null, null));
		assertTrue(VehicleType.TRUCK == safeValueOf(VehicleType.class, "", VehicleType.TRUCK));
		assertTrue(VehicleType.TRUCK == safeValueOf(VehicleType.class, "TRAIN", VehicleType.TRUCK));
	}

	@Test
	public void testValueOfKey() {
		assertTrue(VehicleType.TRUCK == valueOfKey(VehicleType.class, KEY_TRUCK));
		assertFalse(VehicleType.TRUCK == valueOfKey(VehicleType.class, KEY_CAR));
		assertTrue(null == valueOfKey(VehicleType.class, null));
		assertThrows(IllegalArgumentException.class, () -> valueOfKey(VehicleType.class, ""));
		assertThrows(IllegalArgumentException.class, () -> valueOfKey(VehicleType.class, INVALID_KEY));
	}

	@Test
	public void testSafeValueOfKeyClassOfET() {
		assertTrue(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_TRUCK));
		assertFalse(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_CAR));
		assertTrue(null == safeValueOfKey(VehicleType.class, null));
		assertTrue(null == safeValueOfKey(VehicleType.class, ""));
		assertTrue(null == safeValueOfKey(VehicleType.class, INVALID_KEY));
	}

	@Test
	public void testSafeValueOfKeyClassOfETE() {
		assertTrue(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_TRUCK, VehicleType.CAR));
		assertFalse(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_CAR, VehicleType.TRUCK));
		assertTrue(null == safeValueOfKey(VehicleType.class, null, null));
		assertTrue(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, "", VehicleType.TRUCK));
		assertTrue(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, INVALID_KEY, VehicleType.TRUCK));
	}

	@Test
	public void testValueOfKeyIgnoreCase() {
		assertTrue(VehicleType.TRUCK == valueOfKey(VehicleType.class, KEY_TRUCK));
		assertFalse(VehicleType.TRUCK == valueOfKey(VehicleType.class, KEY_CAR));
		assertTrue(null == valueOfKey(VehicleType.class, null));
		assertThrows(IllegalArgumentException.class, () -> valueOfKey(VehicleType.class, ""));
		assertThrows(IllegalArgumentException.class, () -> valueOfKey(VehicleType.class, KEY_TRUCK.toLowerCase()));
		assertThrows(IllegalArgumentException.class, () -> valueOfKey(VehicleType.class, INVALID_KEY));
	}

	@Test
	public void testSafeValueOfKeyIgnoreCaseClassOfEString() {
		assertTrue(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_TRUCK));
		assertFalse(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_TRUCK.toLowerCase()));
		assertFalse(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_CAR));
		assertFalse(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_CAR.toLowerCase()));
		assertTrue(null == safeValueOfKey(VehicleType.class, null));
		assertTrue(null == safeValueOfKey(VehicleType.class, ""));
		assertTrue(null == safeValueOfKey(VehicleType.class, INVALID_KEY));
	}

	@Test
	public void testSafeValueOfKeyIgnoreCaseClassOfEStringE() {
		assertTrue(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_TRUCK, VehicleType.CAR));
		assertFalse(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_TRUCK.toLowerCase(), VehicleType.CAR));
		assertFalse(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_CAR, VehicleType.TRUCK));
		assertTrue(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, KEY_CAR.toLowerCase(), VehicleType.TRUCK));
		assertTrue(null == safeValueOfKey(VehicleType.class, null, null));
		assertTrue(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, "", VehicleType.TRUCK));
		assertTrue(VehicleType.TRUCK == safeValueOfKey(VehicleType.class, INVALID_KEY, VehicleType.TRUCK));
	}

	// @Test
	public void testValueOfCodeClassOfEInt() {
		fail("Not yet implemented");
	}

	// @Test
	public void testSafeValueOfCodeClassOfEInt() {
		fail("Not yet implemented");
	}

	// @Test
	public void testSafeValueOfCodeClassOfEIntE() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValueOfCodeClassOfEChar() {
		fail("Not yet implemented");
	}

	// @Test
	public void testSafeValueOfCodeClassOfEChar() {
		fail("Not yet implemented");
	}

	// @Test
	public void testSafeValueOfCodeClassOfECharE() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValueOfCodeClassOfEInteger() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValueOfCodeClassOfECharacter() {
		fail("Not yet implemented");
	}

	@Test
	public void testNameE() {
		assertEquals("CAR", name(VehicleType.CAR));
		assertNotEquals("car", name(VehicleType.CAR));
		assertTrue(null == name(null));
		assertTrue(null != name(VehicleType.TRUCK));
	}

	@Test
	public void testNameEString() {
		assertEquals("CAR", name(VehicleType.CAR, "TRAIN"));
		assertNotEquals("car", name(VehicleType.CAR, "TRAIN"));
		assertEquals("TRAIN", name(null, "TRAIN"));
		assertNotEquals("truck", name(VehicleType.TRUCK, "truck"));
	}

	@Test
	public void testKeyE() {
		assertEquals(KEY_CAR, key(VehicleType.CAR));
		assertNotEquals(KEY_CAR.toLowerCase(), key(VehicleType.CAR));
		assertTrue(null == key(null));
		assertTrue(null != key(VehicleType.TRUCK));
	}

	@Test
	public void testKeyET() {
		assertEquals(KEY_CAR, key(VehicleType.CAR, "TRAIN"));
		assertNotEquals(KEY_CAR.toLowerCase(), key(VehicleType.CAR, "TRAIN"));
		assertEquals("TRAIN", key(null, "TRAIN"));
		assertNotEquals("truck", key(VehicleType.TRUCK, "truck"));
	}

	// @Test
	public void testCode() {
		fail("Not yet implemented");
	}

	// @Test
	public void testCharCode() {
		fail("Not yet implemented");
	}

	@Test
	public void testDescriptionEString() {
		assertEquals("Car", description(VehicleType.CAR, "Train"));
		assertEquals("Train", description(null, "Train"));
		assertTrue(null == description(null, null));
	}

	@Test
	public void testDescriptionE() {
		assertEquals("Car", description(VehicleType.CAR));
		assertNotEquals("CAR", description(VehicleType.CAR));
		assertTrue(null == description(null));
		assertTrue(null != description(VehicleType.TRUCK));
	}

	// @Test
	public void testToEnumSet() {
		fail("Not yet implemented");
	}
}
