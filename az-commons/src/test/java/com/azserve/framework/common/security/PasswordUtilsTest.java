package com.azserve.framework.common.security;

import static com.azserve.azframework.common.test.TestUtils.assertThrows;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.azserve.azframework.common.security.PasswordUtils;
import com.azserve.azframework.common.test.TestUtils;

public class PasswordUtilsTest {

	@Test
	public void constructorTest() throws Throwable {
		TestUtils.testUtilsClassDefinition(PasswordUtils.class);
	}

	@Test
	public void generateTest() {
		// generate(int, boolean, boolean, boolean)
		assertEquals(8, PasswordUtils.generate(8, false, false, false).length());
		assertThrows(IllegalArgumentException.class, () -> PasswordUtils.generate(2, true, true, true));
		for (int i = 0; i < 50; i++) {
			assertTrue(PasswordUtils.generate(8, true, false, false).matches(".*[A-Z].*"));
			assertTrue(PasswordUtils.generate(8, false, true, false).matches(".*[a-z].*"));
			assertTrue(PasswordUtils.generate(8, false, false, true).matches(".*[0-9].*"));

			assertTrue(PasswordUtils.generate(8, true, true, false).matches(".*[A-Z].*[a-z].*|.*[a-z].*[A-Z].*"));
			assertTrue(PasswordUtils.generate(8, true, false, true).matches(".*[A-Z].*[0-9].*|.*[0-9].*[A-Z].*"));
			assertTrue(PasswordUtils.generate(8, false, true, true).matches(".*[a-z].*[0-9].*|.*[0-9].*[a-z].*"));

			final String p3 = PasswordUtils.generate(8, true, true, true);
			assertTrue(p3.matches(".*[0-9].*"));
			assertTrue(p3.matches(".*[a-z].*"));
			assertTrue(p3.matches(".*[A-Z].*"));
		}
		// generate(int, boolean, boolean, boolean, boolean)
		for (int i = 0; i < 50; i++) {
			final String p3 = PasswordUtils.generate(8, true, true, true, true);
			assertFalse(p3.matches(".*[0].*"));
			assertFalse(p3.matches(".*[O].*"));
			assertFalse(p3.matches(".*[I].*"));
			assertFalse(p3.matches(".*[l].*"));
			assertFalse(p3.matches(".*[k].*"));
			assertFalse(p3.matches(".*[K].*"));
		}
	}

}
