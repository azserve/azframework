package com.azserve.framework.common.lang;

import static com.azserve.azframework.common.lang.NumberUtils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.UUID;

import org.junit.Test;

import com.azserve.azframework.common.lang.NumberUtils;
import com.azserve.azframework.common.test.TestUtils;

public class NumberUtilsTest {

	private static final BigDecimal POSITIVE_BIGDECIMAL = new BigDecimal("0.1");
	private static final BigDecimal ZERO_BIGDECIMAL = new BigDecimal("0");
	private static final BigDecimal NEGATIVE_BIGDECIMAL = new BigDecimal("-0.1");

	private static final Integer ONE_INTEGER = Integer.valueOf(1);
	private static final Integer TWO_INTEGER = Integer.valueOf(2);
	private static final Integer ZERO_INTEGER = Integer.valueOf(0);
	private static final Integer MINUS_ONE_INTEGER = Integer.valueOf(-1);

	private static final Short ONE_SHORT = Short.valueOf((short) 1);
	private static final Short TWO_SHORT = Short.valueOf((short) 2);

	private static final Long ONE_LONG = Long.valueOf(1);
	private static final Long TWO_LONG = Long.valueOf(2);

	private static final Double ONE_DOUBLE = Double.valueOf(1);
	private static final Double TWO_DOUBLE = Double.valueOf(2);

	@Test
	public void constructor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.testUtilsClassDefinition(NumberUtils.class);
	}

	@Test
	public void testToByteNumber() {
		assertNull(NumberUtils.toByte(null));
		assertEquals(Byte.valueOf((byte) 0), NumberUtils.toByte(Integer.valueOf(0)));
		assertEquals(Byte.valueOf((byte) -1), NumberUtils.toByte(Integer.valueOf(-1)));
		assertEquals(Byte.valueOf((byte) -128), NumberUtils.toByte(Integer.valueOf(128)));
		assertEquals(Byte.valueOf((byte) 127), NumberUtils.toByte(Integer.valueOf(-129)));
		assertEquals(Byte.valueOf((byte) 3), NumberUtils.toByte(Double.valueOf(3.2)));
	}

	@Test
	public void testToByteNumberByte() {
		assertEquals(Byte.valueOf((byte) 5), NumberUtils.toByte(null, (byte) 5));
		assertEquals(Byte.valueOf((byte) 0), NumberUtils.toByte(Integer.valueOf(0), (byte) 5));
		assertEquals(Byte.valueOf((byte) -1), NumberUtils.toByte(Integer.valueOf(-1), (byte) 5));
		assertEquals(Byte.valueOf((byte) -128), NumberUtils.toByte(Integer.valueOf(128), (byte) 5));
		assertEquals(Byte.valueOf((byte) 127), NumberUtils.toByte(Integer.valueOf(-129), (byte) 5));
		assertEquals(Byte.valueOf((byte) 3), NumberUtils.toByte(Double.valueOf(3.2), (byte) 5));
	}

	@Test
	public void testToShortNumber() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToShortNumberShort() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToIntegerNumber() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToIntegerNumberInt() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToLongNumber() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToLongNumberLong() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToFloatNumber() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToFloatNumberFloat() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToDoubleNumber() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToDoubleNumberDouble() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToBigDecimalNumber() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToBigDecimalNumberBigDecimal() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToUUID() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToBigIntegerInteger() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToBigIntegerIntegerBigInteger() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToBigIntegerLong() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToBigIntegerDouble() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToBigDecimalIntegerBigDecimal() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToBigDecimalInteger() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToBigDecimalDoubleBigDecimal() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToBigDecimalDouble() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToIntegerDouble() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToLongDouble() {
		// fail("Not yet implemented");
	}

	@Test
	public void testParseInteger() {
		assertTrue(parseInteger("2").equals(TWO_INTEGER));
		assertTrue(parseInteger("a") == null);
		assertTrue(parseInteger("") == null);
		assertTrue(parseInteger(null) == null);
		assertTrue(parseInteger("2", ONE_INTEGER).equals(TWO_INTEGER));
		assertTrue(parseInteger("a", ONE_INTEGER).equals(ONE_INTEGER));
		assertTrue(parseInteger("", ONE_INTEGER).equals(ONE_INTEGER));
		assertTrue(parseInteger(null, ONE_INTEGER).equals(ONE_INTEGER));
		assertTrue(parseInteger("2", 1).equals(TWO_INTEGER));
		assertTrue(parseInteger("a", 1).equals(ONE_INTEGER));
		assertTrue(parseInteger("", 1).equals(ONE_INTEGER));
		assertTrue(parseInteger(null, 1).equals(ONE_INTEGER));

	}

	@Test
	public void testParseInt() {
		assertTrue(parseInt("2", 3) == 2);
		assertTrue(parseInt("a", 2) == 2);
		assertTrue(parseInt("", 2) == 2);
		assertTrue(parseInt(null, 2) == 2);
	}

	@Test
	public void testParseIntAsOptional() {
		assertTrue(parseIntAsOptional("2").isPresent());
		assertFalse(parseIntAsOptional("a").isPresent());
		assertFalse(parseIntAsOptional(null).isPresent());
		assertEquals(parseIntAsOptional("2").getAsInt(), 2);
	}

	@Test
	public void testParseShort() {
		assertTrue(parseShort("2").equals(TWO_SHORT));
		assertTrue(parseShort("a") == null);
		assertTrue(parseShort("") == null);
		assertTrue(parseShort(null) == null);
		assertTrue(parseShort("2", ONE_SHORT).equals(TWO_SHORT));
		assertTrue(parseShort("a", ONE_SHORT).equals(ONE_SHORT));
		assertTrue(parseShort("", ONE_SHORT).equals(ONE_SHORT));
		assertTrue(parseShort(null, ONE_SHORT).equals(ONE_SHORT));
		assertTrue(parseShort("2", (short) 1).equals(TWO_SHORT));
		assertTrue(parseShort("a", (short) 1).equals(ONE_SHORT));
		assertTrue(parseShort("", (short) 1).equals(ONE_SHORT));
		assertTrue(parseShort(null, (short) 1).equals(ONE_SHORT));

	}

	@Test
	public void testParseLong() {
		assertTrue(parseLong("2").equals(TWO_LONG));
		assertTrue(parseLong("a") == null);
		assertTrue(parseLong("") == null);
		assertTrue(parseLong(null) == null);
		assertTrue(parseLong("2", ONE_LONG).equals(TWO_LONG));
		assertTrue(parseLong("a", ONE_LONG).equals(ONE_LONG));
		assertTrue(parseLong("", ONE_LONG).equals(ONE_LONG));
		assertTrue(parseLong(null, ONE_LONG).equals(ONE_LONG));
		assertTrue(parseLong("2", 1).equals(TWO_LONG));
		assertTrue(parseLong("a", 1).equals(ONE_LONG));
		assertTrue(parseLong("", 1).equals(ONE_LONG));
		assertTrue(parseLong(null, 1).equals(ONE_LONG));
	}

	@Test
	public void testParseDouble() {
		assertTrue(parseDouble("2").equals(TWO_DOUBLE));
		assertTrue(parseDouble("a") == null);
		assertTrue(parseDouble("") == null);
		assertTrue(parseDouble(null) == null);
		assertTrue(parseDouble("2", ONE_DOUBLE).equals(TWO_DOUBLE));
		assertTrue(parseDouble("a", ONE_DOUBLE).equals(ONE_DOUBLE));
		assertTrue(parseDouble("", ONE_DOUBLE).equals(ONE_DOUBLE));
		assertTrue(parseDouble(null, ONE_DOUBLE).equals(ONE_DOUBLE));
		assertTrue(parseDouble("2", 1).equals(TWO_DOUBLE));
		assertTrue(parseDouble("a", 1).equals(ONE_DOUBLE));
		assertTrue(parseDouble("", 1).equals(ONE_DOUBLE));
		assertTrue(parseDouble(null, 1).equals(ONE_DOUBLE));
	}

	@Test
	public void testParseUUID() {
		assertNull(parseUUID("a"));
		assertNull(parseUUID(""));
		final UUID randomUUID = UUID.randomUUID();
		assertEquals(randomUUID, parseUUID(randomUUID.toString()));
	}

	@Test
	public void testIfNullRemoveNullByte() {
		final byte one = (byte) 1;
		final byte two = (byte) 2;
		final Byte nullObject = null;
		final Byte oneObject = Byte.valueOf(one);

		assertTrue(ifNull(oneObject, two) == one);
		assertTrue(ifNull(nullObject, two) == two);
		assertTrue(removeNull(nullObject) == (byte) 0);
		assertTrue(removeNull(oneObject) == one);
	}

	@Test
	public void testIfNullRemoveNullShort() {
		final short one = (short) 1;
		final short two = (short) 2;
		final Short nullObject = null;
		final Short oneObject = Short.valueOf(one);

		assertTrue(ifNull(oneObject, two) == one);
		assertTrue(ifNull(nullObject, two) == two);
		assertTrue(removeNull(nullObject) == (short) 0);
		assertTrue(removeNull(oneObject) == one);
	}

	@Test
	public void testIfNullRemoveNullInteger() {
		final int one = 1;
		final int two = 2;
		final Integer nullObject = null;
		final Integer oneObject = Integer.valueOf(one);

		assertTrue(ifNull(oneObject, two) == one);
		assertTrue(ifNull(nullObject, two) == two);
		assertTrue(removeNull(nullObject) == 0);
		assertTrue(removeNull(oneObject) == one);
	}

	@Test
	public void testIfNullRemoveNullLong() {
		final long one = 1;
		final long two = 2;
		final Long nullObject = null;
		final Long oneObject = Long.valueOf(one);

		assertTrue(ifNull(oneObject, two) == one);
		assertTrue(ifNull(nullObject, two) == two);
		assertTrue(removeNull(nullObject) == 0);
		assertTrue(removeNull(oneObject) == one);
	}

	@Test
	public void testIfNullRemoveNullFloat() {
		final float one = 1;
		final float two = 2;
		final Float nullObject = null;
		final Float oneObject = Float.valueOf(one);

		assertTrue(ifNull(oneObject, two) == one);
		assertTrue(ifNull(nullObject, two) == two);
		assertTrue(removeNull(nullObject) == 0);
		assertTrue(removeNull(oneObject) == one);
	}

	@Test
	public void testIfNullRemoveNullDouble() {
		final double one = 1;
		final double two = 2;
		final Double nullObject = null;
		final Double oneObject = Double.valueOf(one);

		assertTrue(ifNull(oneObject, two) == one);
		assertTrue(ifNull(nullObject, two) == two);
		assertTrue(removeNull(nullObject) == 0);
		assertTrue(removeNull(oneObject) == one);
	}

	@Test
	public void testIfNullRemoveNullBigDecimal() {
		// fail("Not yet implemented");
	}

	@Test
	public void testZeroToNull() {
		assertNull(zeroToNull(BigDecimal.ZERO));
		assertTrue(zeroToNull(BigDecimal.ONE).equals(BigDecimal.ONE));
		final BigDecimal zeroDecimal = new BigDecimal("0.01");
		assertTrue(zeroToNull(zeroDecimal).equals(zeroDecimal));
		assertNull(zeroToNull(Integer.valueOf(0)));
		assertTrue(zeroToNull(Integer.valueOf(1)).intValue() == 1);
	}

	@Test
	public void testNegateBigDecimal() {
		// fail("Not yet implemented");
	}

	@Test
	public void testNegateInteger() {
		// fail("Not yet implemented");
	}

	@Test
	public void testNegateLong() {
		// fail("Not yet implemented");
	}

	@Test
	public void testSumIntegerInteger() {
		// fail("Not yet implemented");
	}

	@Test
	public void testSumLongLong() {
		// fail("Not yet implemented");
	}

	@Test
	public void testDiffIntegerInteger() {
		// fail("Not yet implemented");
	}

	@Test
	public void testDiffLongLong() {
		// fail("Not yet implemented");
	}

	@Test
	public void testEqualsIntegerInt() {
		// fail("Not yet implemented");
	}

	@Test
	public void testNotEquals() {
		// fail("Not yet implemented");
	}

	@Test
	public void testIsPositiveOrZero() {
		assertFalse(isPositiveOrZero(null));

		assertTrue(isPositiveOrZero(Byte.valueOf((byte) 0)));
		assertTrue(isPositiveOrZero(Float.valueOf(0F)));
		assertTrue(isPositiveOrZero(Short.valueOf((short) 0)));
		assertTrue(isPositiveOrZero(Integer.valueOf(0)));
		assertTrue(isPositiveOrZero(Double.valueOf(0)));
		assertTrue(isPositiveOrZero(Long.valueOf(0)));
		assertTrue(isPositiveOrZero(BigDecimal.ZERO));
		assertTrue(isPositiveOrZero(BigInteger.ZERO));

		assertTrue(isPositiveOrZero(NumberUtilsTest.POSITIVE_BIGDECIMAL));
		assertTrue(isPositiveOrZero(NumberUtilsTest.ZERO_BIGDECIMAL));
		assertFalse(isPositiveOrZero(NumberUtilsTest.NEGATIVE_BIGDECIMAL));

		assertTrue(isPositiveOrZero(ONE_INTEGER));
		assertTrue(isPositiveOrZero(ZERO_INTEGER));
		assertFalse(isPositiveOrZero(MINUS_ONE_INTEGER));
	}

	@Test
	public void testIsPositive() {
		assertFalse(isPositive(null));

		assertTrue(isPositive(Byte.valueOf((byte) 1)));
		assertTrue(isPositive(Float.valueOf(1F)));
		assertTrue(isPositive(Short.valueOf((short) 1)));
		assertTrue(isPositive(Integer.valueOf(1)));
		assertTrue(isPositive(Double.valueOf(1)));
		assertTrue(isPositive(Long.valueOf(1)));
		assertTrue(isPositive(BigDecimal.ONE));
		assertTrue(isPositive(BigInteger.ONE));

		assertTrue(isPositive(Float.valueOf(0.000001F)));
		assertTrue(isPositive(Double.valueOf(0.000001)));
		assertTrue(isPositive(NumberUtilsTest.POSITIVE_BIGDECIMAL));
		assertFalse(isPositive(NumberUtilsTest.ZERO_BIGDECIMAL));
		assertFalse(isPositive(NumberUtilsTest.NEGATIVE_BIGDECIMAL));

		assertTrue(isPositive(ONE_INTEGER));
		assertFalse(isPositive(ZERO_INTEGER));
		assertFalse(isPositive(MINUS_ONE_INTEGER));
	}

	@Test
	public void testIsNegative() {
		assertFalse(isNegative(null));

		assertTrue(isNegative(Byte.valueOf((byte) -1)));
		assertTrue(isNegative(Float.valueOf(-1F)));
		assertTrue(isNegative(Short.valueOf((short) -1)));
		assertTrue(isNegative(Integer.valueOf(-1)));
		assertTrue(isNegative(Double.valueOf(-1)));
		assertTrue(isNegative(Long.valueOf(-1)));
		assertTrue(isNegative(BigDecimal.ONE.negate()));
		assertTrue(isNegative(BigInteger.ONE.negate()));

		assertFalse(isNegative(NumberUtilsTest.POSITIVE_BIGDECIMAL));
		assertFalse(isNegative(NumberUtilsTest.ZERO_BIGDECIMAL));
		assertTrue(isNegative(NumberUtilsTest.NEGATIVE_BIGDECIMAL));

		assertFalse(isNegative(ONE_INTEGER));
		assertFalse(isNegative(ZERO_INTEGER));
		assertTrue(isNegative(MINUS_ONE_INTEGER));
	}

	@Test
	public void testSumIntArray() {
		// fail("Not yet implemented");
	}

	@Test
	public void testMinMaxIntIntInt() {
		// fail("Not yet implemented");
	}

	@Test
	public void testMinMaxLongLongLong() {
		// fail("Not yet implemented");
	}

	@Test
	public void testMinMaxDoubleDoubleDouble() {
		// fail("Not yet implemented");
	}

	@Test
	public void testMinIntArray() {
		assertTrue(min(1) == 1);
		TestUtils.assertThrows(IllegalArgumentException.class, () -> min());
		assertTrue(min(5, 4, 3, 2, 1, 2, 3, 4, 5) == 1);
	}

	@Test
	public void testMaxIntArray() {
		TestUtils.assertThrows(IllegalArgumentException.class, () -> max());
		assertTrue(max(1) == 1);
		assertTrue(max(5, 4, 3, 2, 1, 2, 3, 4, 5) == 5);
	}

	@Test
	public void testMinLongArray() {
		// fail("Not yet implemented");
	}

	@Test
	public void testMaxLongArray() {
		// fail("Not yet implemented");
	}

	@Test
	public void testIntRange() {
		// fail("Not yet implemented");
	}

	@Test
	public void testIntegerRange() {
		// fail("Not yet implemented");
	}

	@Test
	public void testIsDigits() {
		// fail("Not yet implemented");
	}

	@Test
	public void testIsIntegerNumber() {
		// fail("Not yet implemented");
	}

	@Test
	public void testLengthLong() {
		// fail("Not yet implemented");
	}

	@Test
	public void testLengthInt() {
		// fail("Not yet implemented");
	}

	@Test
	public void testRoundDoubleInt() {
		// fail("Not yet implemented");
	}

	@Test
	public void testRoundDoubleIntRoundingMode() {
		// fail("Not yet implemented");
	}

	@Test
	public void testRoundDoubleIntRoundingMode1() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFormatDoubleIntString() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFormatDoubleInt() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFormatDoubleInt1() {
		// fail("Not yet implemented");
	}

	@Test
	public void testCreatePattern() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFormatIntNumberPattern() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFormatLongNumberPattern() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFormatFloatNumberPattern() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFormatDoubleNumberPattern() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFormatNumberNumberPattern() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFormatNumberString() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFormatNumberNumberFormat() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFormatNumberInt() {
		// fail("Not yet implemented");
	}

	@Test
	public void testParseBigDecimal() {
		// fail("Not yet implemented");
	}

	@Test
	public void testHashCodeLong() {
		// fail("Not yet implemented");
	}

	@Test
	public void testHashCodeLong1() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToStringSafe() {
		// fail("Not yet implemented");
	}

}
