package com.azserve.framework.common.awt;

import static com.azserve.azframework.common.awt.ColorUtils.isDark;
import static com.azserve.azframework.common.awt.ColorUtils.rgb;
import static com.azserve.azframework.common.awt.ColorUtils.tryDecode;
import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import com.azserve.azframework.common.awt.ColorUtils;
import com.azserve.azframework.common.test.TestUtils;

public class ColorUtilsTest {

	private final static int _FF0000 = 255;
	private final static int _00FF00 = 255 * 256;
	private final static int _0000FF = 255 * 256 * 256;
	private final static int _FFFFFF = 255 * 256 * 256 + 255 * 256 + 255;

	@Test
	public void constructor() throws Throwable {
		TestUtils.testUtilsClassDefinition(ColorUtils.class);
	}

	@Test
	public void testRgb() {
		assertArrayEquals(new int[] { 0, 0, 0 }, rgb(0));
		assertArrayEquals(new int[] { 255, 0, 0 }, rgb(_FF0000));
		assertArrayEquals(new int[] { 0, 255, 0 }, rgb(_00FF00));
		assertArrayEquals(new int[] { 0, 0, 255 }, rgb(_0000FF));
		assertArrayEquals(new int[] { 255, 255, 255 }, rgb(_FFFFFF));
		assertArrayEquals(new int[] { 10, 20, 30 }, rgb(10 + 20 * 256 + 30 * 256 * 256));
	}

	@Test
	public void testIsDarkColor() {
		assertTrue(isDark(Color.BLACK));
		assertFalse(isDark(Color.WHITE));
	}

	@Test
	public void testIsDarkInt() {
		assertTrue(isDark(0));
		assertFalse(isDark(_FFFFFF));
	}

	@Test
	public void testIsDarkIntIntInt() {
		assertTrue(isDark(0, 0, 0));
		assertTrue(isDark(255, 0, 0));
		assertFalse(isDark(0, 255, 0));
		assertTrue(isDark(0, 0, 255));
		assertFalse(isDark(255, 255, 255));

		final Color[] a = new Color[64];
		int i = 0;
		for (int r = 0; r < 4; r++) {
			for (int g = 0; g < 4; g++) {
				for (int b = 0; b < 4; b++) {
					a[i] = new Color(r * 64, g * 64, b * 64);
					i++;
				}
			}
		}
		assertTrue(isDark(a[0]));
		assertTrue(isDark(a[1]));
		assertTrue(isDark(a[2]));
		assertTrue(isDark(a[3]));
		assertTrue(isDark(a[4]));
		assertTrue(isDark(a[5]));
		assertTrue(isDark(a[6]));
		assertTrue(isDark(a[7]));
		assertTrue(isDark(a[8]));
		assertTrue(isDark(a[9]));
		assertTrue(isDark(a[10]));
		assertTrue(isDark(a[11]));
		assertTrue(isDark(a[12]));
		assertTrue(isDark(a[13]));
		assertTrue(isDark(a[14]));
		assertFalse(isDark(a[15]));
		assertTrue(isDark(a[16]));
		assertTrue(isDark(a[17]));
		assertTrue(isDark(a[18]));
		assertTrue(isDark(a[19]));
		assertTrue(isDark(a[20]));
		assertTrue(isDark(a[21]));
		assertTrue(isDark(a[22]));
		assertTrue(isDark(a[23]));
		assertTrue(isDark(a[24]));
		assertTrue(isDark(a[25]));
		assertTrue(isDark(a[26]));
		assertTrue(isDark(a[27]));
		assertFalse(isDark(a[28]));
		assertFalse(isDark(a[29]));
		assertFalse(isDark(a[30]));
		assertFalse(isDark(a[31]));
		assertTrue(isDark(a[32]));
		assertTrue(isDark(a[33]));
		assertTrue(isDark(a[34]));
		assertTrue(isDark(a[35]));
		assertTrue(isDark(a[36]));
		assertTrue(isDark(a[37]));
		assertTrue(isDark(a[38]));
		assertTrue(isDark(a[39]));
		assertTrue(isDark(a[40]));
		assertTrue(isDark(a[41]));
		assertFalse(isDark(a[42]));
		assertFalse(isDark(a[43]));
		assertFalse(isDark(a[44]));
		assertFalse(isDark(a[45]));
		assertFalse(isDark(a[46]));
		assertFalse(isDark(a[47]));
		assertTrue(isDark(a[48]));
		assertTrue(isDark(a[49]));
		assertTrue(isDark(a[50]));
		assertTrue(isDark(a[51]));
		assertTrue(isDark(a[52]));
		assertTrue(isDark(a[53]));
		assertTrue(isDark(a[54]));
		assertTrue(isDark(a[55]));
		assertFalse(isDark(a[56]));
		assertFalse(isDark(a[57]));
		assertFalse(isDark(a[58]));
		assertFalse(isDark(a[59]));
		assertFalse(isDark(a[60]));
		assertFalse(isDark(a[61]));
		assertFalse(isDark(a[62]));
		assertFalse(isDark(a[63]));

	}

	@Test
	public void testComputeColor() {
		// fail("Not yet implemented");
	}

	@Test
	public void testGenerateColorsInt() {
		// fail("Not yet implemented");
	}

	@Test
	public void testGenerateColorsIntFloatFloat() {
		// fail("Not yet implemented");
	}

	@Test
	public void testGenerateCssColorsIntFloatFloat() {
		// fail("Not yet implemented");
	}

	@Test
	public void testGenerateCssColorsInt() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToHexRGBHtml() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToRGBACss() {
		// fail("Not yet implemented");
	}

	@Test
	public void testTryDecode() {
		assertEquals(Color.WHITE, tryDecode("#FFFFFF"));
		assertNull(tryDecode(null));
		assertNull(tryDecode("ZZZZZZ"));
	}

}
