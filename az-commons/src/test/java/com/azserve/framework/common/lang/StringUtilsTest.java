package com.azserve.framework.common.lang;

import static com.azserve.azframework.common.lang.StringUtils.*;
import static com.azserve.azframework.common.test.TestUtils.assertThrows;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.junit.Test;

import com.azserve.azframework.common.lang.StringUtils;
import com.azserve.azframework.common.lang.StringUtils.StringNotValorizedException;
import com.azserve.azframework.common.test.TestUtils;

public class StringUtilsTest {

	final String emptyString = "";
	final String blankString = "   ";
	final String string1 = "ciao";
	final String string2 = "pippo";
	final String string3 = "pluto";

	@SuppressWarnings("cast")
	@Test
	public void constructor() throws Throwable {
		TestUtils.testUtilsClassDefinition(StringUtils.class);
		assertTrue(new StringNotValorizedException() instanceof RuntimeException);
		assertEquals("Message", new StringNotValorizedException("Message").getMessage());
	}

	@Test
	public void equalsTest() {
		// equals(String, char)
		assertTrue(StringUtils.equals("a", 'a'));
		assertFalse(StringUtils.equals("aa", 'a'));
		assertFalse(StringUtils.equals("b", 'a'));
		assertFalse(StringUtils.equals("", 'a'));
		assertFalse(StringUtils.equals(null, 'a'));
		// equals(String, String ...)
		assertTrue(StringUtils.equals("a", "a"));
		assertTrue(StringUtils.equals("a", "a", "b", "c"));
		assertTrue(StringUtils.equals("b", "a", "b", "c"));
		assertTrue(StringUtils.equals("c", "a", "b", "c"));
		assertTrue(StringUtils.equals(null, "a", null, "c"));
		assertFalse(StringUtils.equals("d", "a", "b", "c"));
	}

	@Test
	public void equalsIgnoreNullCaseTest() {
		assertTrue(equalsIgnoreNullCase(null, null));
		assertTrue(equalsIgnoreNullCase(this.emptyString, this.emptyString));
		assertTrue(equalsIgnoreNullCase(null, this.emptyString));
		assertTrue(equalsIgnoreNullCase(this.string1.toLowerCase(), this.string1.toUpperCase()));
	}

	@Test
	public void equalsIgnoreNullTest() {
		assertTrue(equalsIgnoreNull(null, null));
		assertTrue(equalsIgnoreNull(this.emptyString, this.emptyString));
		assertTrue(equalsIgnoreNull(null, this.emptyString));
		assertTrue(equalsIgnoreNull(this.string1, this.string1));
		assertTrue(equalsIgnoreNull(this.string1, this.string1));
	}

	@Test
	public void ifNullOrBlankTest() {
		assertEquals(this.string1, ifNullOrBlank(null, this.string1));
		assertEquals(this.string1, ifNullOrBlank(this.emptyString, this.string1));
		assertEquals(this.string1, ifNullOrBlank(this.blankString, this.string1));
		assertEquals(this.string2, ifNullOrBlank(this.string2, this.string1));
	}

	@Test
	public void ifNullOrEmptyTest() {
		assertEquals(this.string1, ifNullOrEmpty(null, this.string1));
		assertEquals(this.string1, ifNullOrEmpty(this.emptyString, this.string1));
		assertEquals(this.blankString, ifNullOrEmpty(this.blankString, this.string1));
		assertEquals(this.string2, ifNullOrEmpty(this.string2, this.string1));
	}

	@Test
	public void ifNullTest() {
		assertEquals(this.string1, ifNull(null, this.string1));
		assertEquals(this.string1, ifNull(this.string1, this.string2));
		assertEquals(this.string1, ifNull(null, null, this.string1));
		assertEquals(this.string1, ifNull(null, this.string2, this.string1));
		assertEquals(this.string1, ifNull(this.string2, null, this.string1));
		assertEquals(this.string1 + this.string2, ifNull(this.string1, this.string2, this.string3));
	}

	@Test
	public void nullToEmptyTest() {
		assertEquals(this.emptyString, nullToEmpty(null));
		assertEquals(this.emptyString, nullToEmpty(this.emptyString));
		assertEquals(this.blankString, nullToEmpty(this.blankString));
		assertEquals(this.string1, nullToEmpty(this.string1));
	}

	@Test
	public void blankToNullTest() {
		assertNull(blankToNull(null));
		assertNull(blankToNull(this.emptyString));
		assertNull(blankToNull(this.blankString));
		assertEquals(this.string1, blankToNull(this.string1));
	}

	@Test
	public void isNotNullNorBlankTest() {
		assertFalse(isNotNullNorBlank(null));
		assertFalse(isNotNullNorBlank(this.emptyString));
		assertFalse(isNotNullNorBlank(this.blankString));
		assertTrue(isNotNullNorBlank(this.string1));
	}

	@Test
	public void isNullOrBlankTest() {
		assertTrue(isNullOrBlank(null));
		assertTrue(isNullOrBlank(this.emptyString));
		assertTrue(isNullOrBlank(this.blankString));
		assertFalse(isNullOrBlank(this.string1));
	}

	@Test
	public void emptyToNullTest() {
		assertNull(emptyToNull(null));
		assertNull(emptyToNull(this.emptyString));
		assertEquals(this.blankString, emptyToNull(this.blankString));
		assertEquals(this.string1, emptyToNull(this.string1));
	}

	@Test
	public void isNotNullNorEmptyTest() {
		assertFalse(StringUtils.isNotNullNorEmpty(null));
		assertFalse(isNotNullNorEmpty(this.emptyString));
		assertTrue(isNotNullNorEmpty(this.blankString));
		assertTrue(isNotNullNorEmpty(this.string1));
	}

	@Test
	public void isNullOrEmptyTest() {
		assertTrue(isNullOrEmpty(null));
		assertTrue(isNullOrEmpty(this.emptyString));
		assertFalse(isNullOrEmpty(this.blankString));
		assertFalse(isNullOrEmpty(this.string1));
	}

	@Test
	public void requireNonEmptyTest() {
		assertEquals(this.string1, requireNonEmpty(this.string1, "message"));
		assertThrows(StringNotValorizedException.class, () -> requireNonEmpty(null, "just a message"));
		assertThrows(StringNotValorizedException.class, () -> requireNonEmpty("", "just a message"));
	}

	@Test
	public void trimSafeTest() {
		assertNull(trimSafe(null));
		assertEquals("", trimSafe(""));
		assertEquals("", trimSafe("   "));
		assertEquals("ciao", trimSafe(" ciao "));
	}

	@Test
	public void trimToEmptyTest() {
		assertEquals("", trimToEmpty(null));
		assertEquals("", trimToEmpty(""));
		assertEquals("", trimToEmpty("   "));
		assertEquals("ciao", trimToEmpty(" ciao "));
	}

	@Test
	public void trimToNullTest() {
		assertNull(trimToNull(null));
		assertNull(trimToNull(""));
		assertNull(trimToNull("   "));
		assertEquals("ciao", trimToNull(" ciao "));
	}

	@Test
	public void trimLeftTest() {
		assertEquals("", trimLeft(""));
		assertEquals("", trimLeft("   "));
		assertEquals("ciao", trimLeft("ciao"));
		assertEquals("ciao ", trimLeft("\nciao "));
	}

	@Test
	public void trimRightTest() {
		assertEquals("", trimRight(""));
		assertEquals("", trimRight("   "));
		assertEquals(" ciao", trimRight(" ciao "));
		assertEquals(" ciao", trimRight(" ciao\n"));
	}

	@Test
	public void clearWhitespacesTest() {
		assertNull(clearWhitespaces(null));
		assertEquals("", clearWhitespaces(""));
		assertEquals("", clearWhitespaces("   "));
		assertEquals("ciao", clearWhitespaces(" ciao "));
		assertEquals("ciaoatutti", clearWhitespaces(" ciao a tutti "));
		assertEquals("ciaoatutti", clearWhitespaces(" ciao a\ntutti "));
		assertEquals("ciao", clearWhitespaces(StringUtils.NBSP + "ciao"));
		assertEquals("ciao", clearWhitespaces("ciao"));
	}

	@Test
	public void findCommonPrefixTest() {
		assertEquals("prova", findCommonPrefix("prova", "prova"));
		assertEquals("prova", findCommonPrefix("prova1", "prova2"));
		assertEquals("prova", findCommonPrefix("prova1", "prova"));
		assertEquals("prova", findCommonPrefix("provaxxxx", "provayyyy"));
		assertEquals(null, findCommonPrefix("", ""));
		assertEquals(null, findCommonPrefix("ciao", "prova"));
		assertEquals(null, findCommonPrefix("ciao", null));
		assertEquals(null, findCommonPrefix(null, "ciao"));
	}

	@Test
	public void removeLeadingCharsTest() {
		assertEquals("", removeLeadingChars("", '0'));
		assertEquals("", removeLeadingChars("0", '0'));
		assertEquals("12345", removeLeadingChars("00012345", '0'));
		assertEquals("12345000", removeLeadingChars("00012345000", '0'));
	}

	@Test
	public void removeTrailingCharsTest() {
		assertEquals("", removeTrailingChars("", '0'));
		assertEquals("", removeTrailingChars("0", '0'));
		assertEquals("12345", removeTrailingChars("12345000", '0'));
		assertEquals("00012345", removeTrailingChars("00012345000", '0'));
	}

	@Test
	public void leftTest() {
		assertEquals("", left(null, 5));
		assertEquals("", left("", 5));
		assertEquals("ciao", left("ciao", 5));
		assertEquals("paper", left("paperino", 5));
	}

	@Test
	public void rightTest() {
		assertEquals("", right(null, 5));
		assertEquals("", right("", 5));
		assertEquals("ciao", right("ciao", 5));
		assertEquals("erino", right("paperino", 5));
	}

	@Test
	public void substringIndexTest() {
		final String string_1 = "ciao a tutti";
		final String string_2 = "abababa";

		assertEquals("tutti", substringIndex(string_1, " a ", 0));
		assertEquals("", substringIndex(string_1, " a ", 1));
		assertEquals("", substringIndex(string_1, "xx", 0));
		assertEquals("ababa", substringIndex(string_2, "ab", 0));
		assertEquals("aba", substringIndex(string_2, "ab", 1));
		assertEquals("a", substringIndex(string_2, "ab", 2));
		assertEquals("", substringIndex(string_2, "ab", 3));
		assertThrows(IllegalArgumentException.class, () -> substringIndex("", "", -1));
	}

	@Test
	public void substringDelimitedTest() {
		assertNull(substringDelimited("array[]", "(", ")"));
		assertEquals("", substringDelimited("array[]", "[", "]"));
		assertEquals("index", substringDelimited("array[index]", "[", "]"));
		assertNull(substringDelimited("array[index]", "[", "}"));
	}

	@Test
	public void capitalizeTest() {
		assertEquals("Ciao", capitalize("ciao"));
		assertEquals("Pinco Pallino", capitalize("pinco pallino"));
		assertEquals("Pinco o Pallino", capitalize("pinco o pallino"));
		assertEquals("PincoPallino", capitalize("pincoPallino"));
		assertEquals("", capitalize(""));
		assertEquals(" ", capitalize(" "));
		assertEquals("A", capitalize("A"));
		assertEquals("A", capitalize("a"));
		assertEquals("X[Y]", capitalize("x[y]"));
	}

	@Test
	public void toUpperCaseTest() {
		assertEquals("ABCDEFGHI", toUpperCase("abcdefghi", 0, 9));
		assertEquals("abcdefGHI", toUpperCase("abcdefghi", 6, 9));
		assertEquals("ABCdefghi", toUpperCase("abcdefghi", 0, 3));
		assertEquals("abcDEFghi", toUpperCase("abcdefghi", 3, 6));
		assertEquals("abcdefghi", toUpperCase("abcdefghi", 0, 0));
		assertEquals("ABCdefghi", toUpperCase("ABCdefghi", 0, 3));
		assertThrows(IllegalArgumentException.class, () -> toUpperCase("ABC", 2, 1));
	}

	@Test
	public void toLowerCaseTest() {

		assertEquals("abcdefghi", toLowerCase("ABCDEFGHI", 0, 9));
		assertEquals("ABCDEFghi", toLowerCase("ABCDEFGHI", 6, 9));
		assertEquals("abcDEFGHI", toLowerCase("ABCDEFGHI", 0, 3));
		assertEquals("ABCdefGHI", toLowerCase("ABCDEFGHI", 3, 6));
		assertEquals("ABCDEFGHI", toLowerCase("ABCDEFGHI", 0, 0));
		assertEquals("abcDEFGHI", toLowerCase("abcDEFGHI", 0, 3));
		assertThrows(IllegalArgumentException.class, () -> toLowerCase("ABC", 2, 1));
	}

	@Test
	public void abbreviateTest() {
		assertEquals(null, abbreviate(null, 10));
		assertEquals("", abbreviate("", 10));
		assertEquals("ciao", abbreviate("ciao", 10));
		assertEquals("ciao a ...", abbreviate("ciao a tutti", 10));
		assertEquals(null, abbreviate(null, 10, "*"));
		assertEquals("", abbreviate("", 10, "*"));
		assertEquals("ciao", abbreviate("ciao", 10, "*"));
		assertEquals("ciao a tu*", abbreviate("ciao a tutti", 10, "*"));
		assertEquals("***", abbreviate("ciao a tutti", 3, "***"));
		assertThrows(IndexOutOfBoundsException.class, () -> abbreviate("123456789", 3, "****"));
	}

	@Test
	public void insertTest() {
		assertEquals("AAA123456789", insert("123456789", "AAA", 0));
		assertEquals("123AAA456789", insert("123456789", "AAA", 3));
		assertEquals("123456789AAA", insert("123456789", "AAA", 9));
		assertThrows(StringIndexOutOfBoundsException.class, () -> insert("123456789", "AAA", 10));
	}

	@Test
	public void removeTest() {
		assertEquals("", remove("123456789", 0, 9));
		assertEquals("123456789", remove("123456789", 0, 0));
		assertEquals("123456", remove("123456789", 6, 9));
		assertEquals("456789", remove("123456789", 0, 3));
		assertEquals("123789", remove("123456789", 3, 6));
	}

	@Test
	public void replaceTest() {
		assertEquals("**", replace("123456789", "**", 0, 9));
		assertEquals("123456**", replace("123456789", "**", 6, 9));
		assertEquals("**456789", replace("123456789", "**", 0, 3));
		assertEquals("123**789", replace("123456789", "**", 3, 6));
		assertEquals("123456789", replace("123456789", "**", 0, 0));
		assertThrows(IllegalArgumentException.class, () -> replace("1234567890", "AAA", 5, 4));
	}

	@Test
	public void replaceLambdaTest() {
		assertEquals(StringUtils.replace("ABCABCABCABC", "A", (s, ix) -> s + ix), "A0BCA1BCA2BCA3BC");
		assertEquals(StringUtils.replace("ABCABCABCABC", "A", (s, ix) -> "" + ix), "0BC1BC2BC3BC");
		assertEquals(StringUtils.replace("ABCABCABCABC", "ABC", (s, ix) -> null), "nullnullnullnull");

		assertEquals(StringUtils.replace("0123456789ABCDEFGHI0", Pattern.compile("[0-9]"), (s, ix) -> "" + Integer.parseInt(s) * ix), "0149162536496481ABCDEFGHI0");
	}

	@Test
	public void padLeftTest() {
		assertEquals("000000ciao", padLeft("ciao", 10, '0'));
		assertEquals("123456789X", padLeft("123456789X", 10, '0'));
		assertEquals("ao a tutti", padLeft("ciao a tutti", 10, '0'));
	}

	@Test
	public void padRightTest() {
		assertEquals("ciao000000", padRight("ciao", 10, '0'));
		assertEquals("123456789X", padRight("123456789X", 10, '0'));
		assertEquals("ciao a tut", padRight("ciao a tutti", 10, '0'));
	}

	@Test
	public void generateTest() {
		// generate (String, int)
		assertEquals("", generate("", 2));
		assertEquals("", generate("abc", 0));
		assertEquals("abcabc", generate("abc", 2));
		assertEquals("abc", generate("abc", 1));
		assertThrows(IllegalArgumentException.class, () -> generate("a", -1));
		// generate (char, int)
		assertEquals("", generate('a', 0));
		assertEquals("a", generate('a', 1));
		assertEquals("aaaaa", generate('a', 5));
		assertThrows(NegativeArraySizeException.class, () -> generate('a', -1));
	}

	@Test
	public void maxLengthTest() {
		assertEquals(1, maxLength(Arrays.asList("A")));
		assertEquals(2, maxLength(Arrays.asList("A", "AA", "")));
		assertThrows(NullPointerException.class, () -> maxLength(Arrays.asList("A", null, "")));
	}

	@Test
	public void checkRegexTest() {
		assertTrue(checkRegex(null, null));
		assertTrue(checkRegex("uno", "(uno|due|tre)"));
		assertFalse(checkRegex(null, "uno"));
		assertFalse(checkRegex(" ", "uno"));
	}

	@Test
	public void countMatchesTest() {
		assertEquals(0, countMatches("", "a"));
		assertEquals(0, countMatches("a", ""));
		assertEquals(0, countMatches("", ""));
		assertEquals(3, countMatches("abc abc abc", "a"));
		assertEquals(0, countMatches("abc abc abc", "d"));
		assertEquals(2, countMatches("aaaa", "aa"));

		assertEquals(6, countMatches("abc abc abc", Pattern.compile("(a|b)")));
	}

	@Test
	public void charAtSafeTest() {
		assertEquals('c', charAtSafe("ciao", 0, '*'));
		assertEquals('a', charAtSafe("ciao", 2, '*'));
		assertEquals('*', charAtSafe("ciao", -2, '*'));
		assertEquals('*', charAtSafe("ciao", 7, '*'));
		assertEquals('*', charAtSafe(null, 4, '*'));
	}

	@Test
	public void concatOrNullTest() {
		assertEquals("uno - due", concatOrNull("uno", "due", " - "));
		assertEquals("uno", concatOrNull("uno", null, " - "));
		assertEquals("due", concatOrNull(null, "due", " - "));
		assertNull(concatOrNull(null, null, " - "));
	}

	@Test
	public void parseMapTest() throws Exception {
		assertEquals(Collections.emptyMap(), parseMap("", "\\|"));
		final Map<String, String> m = new HashMap<>();
		m.put("nome", "Pinco");
		m.put("cognome", "Pallino");
		assertEquals(m, parseMap("nome=Pinco|cognome=Pallino", "\\|"));
		m.put("cognome", null);
		assertEquals(m, parseMap("nome=Pinco|cognome", "\\|"));
	}

	@Test
	public void splitInRowsTest() {
		assertArrayEquals(new String[] { "" }, splitInRows("", 2));
		assertArrayEquals(new String[] { "foo" }, splitInRows("foo", 3));
		assertArrayEquals(new String[] { "foo", "fo1", "fo2" }, splitInRows("foo fo1 fo2", 3));
		assertArrayEquals(new String[] { "foo", "fo", "fo2" }, splitInRows("foofo fo2", 3));
		assertArrayEquals(new String[] { "foo", "fof", "o2" }, splitInRows("foo fofo2", 3));
		assertArrayEquals(new String[] { "foo", "fof", "o2" }, splitInRows("foo     fofo2", 3));
		assertArrayEquals(new String[] { "fo", "of", "o1", "fo2" }, splitInRows("fo of o1 fo2", 3));
		assertArrayEquals(new String[] { "foo", "foo", "foo" }, splitInRows("foofoofoo", 3));
	}

	@Test
	public void maxRowLengthTest() {
		assertEquals(0, maxRowLength(null, ' '));
		assertEquals(0, maxRowLength("", ' '));
		assertEquals(5, maxRowLength("Ciao a tutti", ' '));
		assertEquals(4, maxRowLength("Ciao a tu", ' '));
		assertEquals(5, maxRowLength("Ciao tutti a", ' '));
		assertEquals(0, maxRowLength(null, " "));
		assertEquals(0, maxRowLength("", " "));
		assertEquals(5, maxRowLength("Ciao a tutti", " "));
		assertEquals(12, maxRowLength("Ciao a tutti", null));
	}

	@Test
	public void containsIgnoreCaseTest() {
		assertFalse(StringUtils.containsIgnoreCase(null, ""));
		assertFalse(StringUtils.containsIgnoreCase(null, null));
		assertFalse(StringUtils.containsIgnoreCase("", null));
		assertFalse(StringUtils.containsIgnoreCase("", null));
		assertFalse(StringUtils.containsIgnoreCase("HELLO", "HELLO WORLD"));
		assertTrue(StringUtils.containsIgnoreCase("HELLO WORLD", "HELLO"));
		assertTrue(StringUtils.containsIgnoreCase("HELLO WORLD", "hello"));
		assertTrue(StringUtils.containsIgnoreCase("HELLO WORLD", "Hello "));
		assertFalse(StringUtils.containsIgnoreCase("HELLO WORLD", "Hell "));

	}

	@Test
	public void splitCamelCaseTest() {
		assertThrows(NullPointerException.class, () -> splitCamelCase(TestUtils.getNull()));
		assertEquals("", splitCamelCase(""));
		assertEquals("a", splitCamelCase("a"));
		assertEquals("a", splitCamelCase("A"));
		assertEquals("hello world", splitCamelCase("HelloWorld"));
		assertEquals("hello world", splitCamelCase("helloWorld"));
		assertEquals("hello world method", splitCamelCase("helloWorldMethod"));
		assertEquals("hello1 world", splitCamelCase("hello1World"));
	}

	@Test
	public void toAsciiTest() {
		assertNull(toAscii(null));
		assertEquals("", toAscii(""));
		assertEquals("Hello world", toAscii("Hello world"));
		assertEquals("San Dona", toAscii("San Donà"));
		assertEquals("baeeiou@#??#[]#?{?", toAscii("bàéèìòù@#¢¢#[]#Þ{¿"));
	}

	@Test
	public void deleteFromBeginTest() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Hello");
		deleteFromBegin(sb, 2);
		assertEquals("llo", sb.toString());
	}

	@Test
	public void deleteFromEndTest() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Hello");
		deleteFromEnd(sb, 2);
		assertEquals("Hel", sb.toString());
	}

	@Test
	public void nullSafeCompareTest() {
		// tests with null values
		assertTrue(nullSafeCompare(null, null, true, true) == 0);
		assertTrue(nullSafeCompare(null, null, false, true) == 0);
		assertTrue(nullSafeCompare(null, null, true, false) == 0);
		assertTrue(nullSafeCompare(null, null, false, false) == 0);
		assertTrue(nullSafeCompare(null, "", true, true) < 0);
		assertTrue(nullSafeCompare(null, "A", true, true) < 0);
		assertTrue(nullSafeCompare(null, "", false, true) < 0);
		assertTrue(nullSafeCompare(null, "A", false, true) < 0);
		assertTrue(nullSafeCompare(null, "", true, false) > 0);
		assertTrue(nullSafeCompare(null, "A", true, false) > 0);
		assertTrue(nullSafeCompare(null, "", false, false) > 0);
		assertTrue(nullSafeCompare(null, "A", false, false) > 0);
		assertTrue(nullSafeCompare("", null, true, true) > 0);
		assertTrue(nullSafeCompare("A", null, true, true) > 0);
		assertTrue(nullSafeCompare("", null, false, true) > 0);
		assertTrue(nullSafeCompare("A", null, false, true) > 0);
		assertTrue(nullSafeCompare("", null, true, false) < 0);
		assertTrue(nullSafeCompare("A", null, true, false) < 0);
		assertTrue(nullSafeCompare("", null, false, false) < 0);
		assertTrue(nullSafeCompare("A", null, false, false) < 0);
		// tests with not null values
		final String[] s1 = { "", "A", "a", "Hello", "ZZ", "null", "NuLl", "zz" };
		for (final String element : s1) {
			for (final String element2 : s1) {
				assertEquals(element.compareTo(element2), nullSafeCompare(element, element2, true, false));
				assertEquals(element.compareTo(element2), nullSafeCompare(element, element2, true, true));
				assertEquals(element.compareTo(element2), nullSafeCompare(element2, element, false, false));
				assertEquals(element.compareTo(element2), nullSafeCompare(element2, element, false, true));
			}
		}
	}

	@Test
	public void nullCaseSafeCompareTest() {
		// tests with null values
		assertTrue(nullCaseSafeCompare(null, null, true, true) == 0);
		assertTrue(nullCaseSafeCompare(null, null, false, true) == 0);
		assertTrue(nullCaseSafeCompare(null, null, true, false) == 0);
		assertTrue(nullCaseSafeCompare(null, null, false, false) == 0);
		assertTrue(nullCaseSafeCompare(null, "", true, true) < 0);
		assertTrue(nullCaseSafeCompare(null, "A", true, true) < 0);
		assertTrue(nullCaseSafeCompare(null, "", false, true) < 0);
		assertTrue(nullCaseSafeCompare(null, "A", false, true) < 0);
		assertTrue(nullCaseSafeCompare(null, "", true, false) > 0);
		assertTrue(nullCaseSafeCompare(null, "A", true, false) > 0);
		assertTrue(nullCaseSafeCompare(null, "", false, false) > 0);
		assertTrue(nullCaseSafeCompare(null, "A", false, false) > 0);
		assertTrue(nullCaseSafeCompare("", null, true, true) > 0);
		assertTrue(nullCaseSafeCompare("A", null, true, true) > 0);
		assertTrue(nullCaseSafeCompare("", null, false, true) > 0);
		assertTrue(nullCaseSafeCompare("A", null, false, true) > 0);
		assertTrue(nullCaseSafeCompare("", null, true, false) < 0);
		assertTrue(nullCaseSafeCompare("A", null, true, false) < 0);
		assertTrue(nullCaseSafeCompare("", null, false, false) < 0);
		assertTrue(nullCaseSafeCompare("A", null, false, false) < 0);
		// tests with not null values
		final String[] s1 = { "", "A", "a", "Hello", "ZZ", "null", "NuLl", "zz" };
		for (final String element : s1) {
			for (final String element2 : s1) {
				assertEquals(element.compareToIgnoreCase(element2), nullCaseSafeCompare(element, element2, true, false));
				assertEquals(element.compareToIgnoreCase(element2), nullCaseSafeCompare(element, element2, true, true));
				assertEquals(element.compareToIgnoreCase(element2), nullCaseSafeCompare(element2, element, false, false));
				assertEquals(element.compareToIgnoreCase(element2), nullCaseSafeCompare(element2, element, false, true));
			}
		}

	}

	@Test
	public void concatTest() {
		// concat (String, String ...)
		assertThrows(NullPointerException.class, () -> concat(TestUtils.getNull(), "a", "b", "c"));
		assertEquals("a|b|c", concat("|", "a", "b", "c"));
		assertEquals("a||c", concat("|", "a", "", "c"));
		assertEquals("a||c", concat("|", "a", null, "c"));
		assertEquals("", concat("", null, ""));
		assertEquals("", concat("|"));
		// concat (Collection<String>, String, String, String)
		assertThrows(NullPointerException.class, () -> concat(Collections.emptyList(), TestUtils.getNull(), "-", "*"));
		assertEquals("", concat(Collections.emptyList(), "|", "-", "*"));
		final List<String> stringList = Arrays.asList("a", null, "b", "c");
		assertEquals("abc", concat(stringList, "", "", ""));
		assertEquals("a||b|c", concat(stringList, "|", "", ""));
		assertEquals("a||b|c", concat(stringList, "|", "", null));
		assertEquals("a||b|c", concat(stringList, "|", null, null));
		assertEquals("a||b|c", concat(stringList, "|", null, ""));
		assertEquals("*a-|*-|*b-|*c-", concat(stringList, "|", "*", "-"));
		assertEquals("", concat(Collections.singleton(""), "", null, null));
	}

	@Test
	public void containsTest() {
		assertThrows(NullPointerException.class, () -> contains(TestUtils.getNull(), "X"));
		assertThrows(NullPointerException.class, () -> contains("X", TestUtils.getNull()));
		// ?assertThrows(NullPointerException.class, () -> contains("X", "X", TestUtils.getNull()));
		assertTrue(contains("Hello world", "Hello", "!"));
		assertTrue(contains("Hello world", "z", "o"));
		assertFalse(contains("Hello world", "a", "b"));
		assertTrue(contains("Hello world", ""));
		assertFalse(contains("Hello world"));
	}

	@Test
	public void iteratorTest() {
		// a cosa serve?
		iterator("");
	}

	@Test
	public void containsAllTest() {
		assertThrows(NullPointerException.class, () -> containsAll(TestUtils.getNull(), "X"));
		assertThrows(NullPointerException.class, () -> containsAll("X", TestUtils.getNull()));
		// ?assertThrows(NullPointerException.class, () -> contains("X", "X", TestUtils.getNull()));
		assertTrue(containsAll("Hello world!", "Hello", "!"));
		assertFalse(containsAll("Hello world", "z", "o"));
		assertFalse(containsAll("Hello world", "a", "b"));
		assertTrue(containsAll("Hello world", ""));
		assertTrue(containsAll("Hello world"));
	}
}
