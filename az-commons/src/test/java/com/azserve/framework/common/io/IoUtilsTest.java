package com.azserve.framework.common.io;

import static com.azserve.azframework.common.io.IOUtils.copy;
import static com.azserve.azframework.common.io.IOUtils.toBytes;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;

import org.junit.Test;

import com.azserve.azframework.common.io.IOUtils;
import com.azserve.azframework.common.test.TestUtils;

public class IoUtilsTest {

	@Test
	public void testConstructor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.testUtilsClassDefinition(IOUtils.class);
	}

	@Test
	public void testToBytes() throws IOException {
		final byte[] bytes = "hello".getBytes();
		try (InputStream is = new ByteArrayInputStream(bytes)) {
			assertArrayEquals(bytes, toBytes(is));
		}
		try (InputStream is = new ByteArrayInputStream(new byte[0])) {
			assertArrayEquals(new byte[0], toBytes(is));
		}
	}

	@Test
	public void testToStringReader() throws IOException {
		try (Reader reader = new StringReader("hello")) {
			assertEquals("hello", IOUtils.toString(reader));
		}
	}

	@Test
	public void testToStringBufferedReader() throws IOException {
		try (Reader reader = new StringReader("hello");
				BufferedReader br = new BufferedReader(reader)) {
			assertEquals("hello", IOUtils.toString(br));
		}
	}

	@Test
	public void testToStringInputStreamInt() throws IOException {
		try (InputStream is = new ByteArrayInputStream("hello".getBytes())) {
			assertEquals("hello", IOUtils.toString(is, 2));
		}
		try (InputStream is = new ByteArrayInputStream("hello".getBytes())) {
			assertEquals("hello", IOUtils.toString(is, 20));
		}
	}

	@Test
	public void testToStringInputStream() throws IOException {
		try (InputStream is = new ByteArrayInputStream("hello".getBytes())) {
			assertEquals("hello", IOUtils.toString(is));
		}
	}

	@Test
	public void testToInputStream() throws IOException {
		try (InputStream is = IOUtils.toInputStream("hello")) {
			assertEquals("hello", IOUtils.toString(is));
		}
	}

	@Test
	public void testCopyInputStreamOutputStreamInt() throws IOException {
		final byte[] bytes = "hello".getBytes();
		try (InputStream is = new ByteArrayInputStream(bytes);
				ByteArrayOutputStream os = new ByteArrayOutputStream()) {
			final long len = copy(is, os, 2);
			assertEquals(bytes.length, len);
			assertArrayEquals(bytes, os.toByteArray());
		}
	}

	@Test
	public void testCopyInputStreamOutputStreamIntLong() throws IOException {
		final byte[] bytes = "hello".getBytes();
		try (InputStream is = new ByteArrayInputStream(bytes);
				ByteArrayOutputStream os = new ByteArrayOutputStream()) {
			final long len = copy(is, os, 2, -1);
			assertEquals(bytes.length, len);
			assertArrayEquals(bytes, os.toByteArray());
		}
		try (InputStream is = new ByteArrayInputStream(bytes);
				ByteArrayOutputStream os = new ByteArrayOutputStream()) {
			final long len = copy(is, os, 2, 6);
			assertEquals(bytes.length, len);
			assertArrayEquals(bytes, os.toByteArray());
		}
		try (InputStream is = new ByteArrayInputStream(bytes);
				ByteArrayOutputStream os = new ByteArrayOutputStream()) {
			TestUtils.assertThrows(IOException.class, () -> copy(is, os, 2, 4));
		}
	}

	@Test
	public void testCopyInputStreamOutputStream() throws IOException {
		final byte[] bytes = "hello".getBytes();
		try (InputStream is = new ByteArrayInputStream(bytes);
				ByteArrayOutputStream os = new ByteArrayOutputStream()) {
			final long len = copy(is, os);
			assertEquals(bytes.length, len);
			assertArrayEquals(bytes, os.toByteArray());
		}
	}

	@Test
	public void testGuessContentTypeByteArray() {
		// fail("Not yet implemented");
	}

	@Test
	public void testGuessContentTypeInputStream() {
		// fail("Not yet implemented");
	}

	@Test
	public void testDownloadFromWebString() {
		// fail("Not yet implemented");
	}

	@Test
	public void testDownloadFromWebStringLong() {
		// fail("Not yet implemented");
	}

}
