package com.azserve.azframework.common.enums;

import java.util.EnumSet;
import java.util.Optional;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.NonNullByDefault;

public final class EnumUtils {

	private EnumUtils() {}

	@NonNullByDefault
	public static <E extends Enum<E>> Optional<E> findByName(final Class<E> enumClass, final String constantName) {
		try {
			return Optional.of(Enum.valueOf(enumClass, constantName));
		} catch (final @SuppressWarnings("unused") IllegalArgumentException ex) {
			return Optional.empty();
		}
	}

	@NonNullByDefault
	public static <E extends Enum<E>> Optional<E> findByNameCaseInsensitive(final Class<E> enumClass, final String constantName) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (constantName.equalsIgnoreCase(constant.name())) {
				return Optional.of(constant);
			}
		}
		return Optional.empty();
	}

	@NonNullByDefault
	public static <E extends Enum<E> & EnumWithKey<K>, K> Optional<E> findByKey(final Class<E> enumClass, final K key) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (key.equals(constant.key())) {
				return Optional.of(constant);
			}
		}
		return Optional.empty();
	}

	@NonNullByDefault
	public static <E extends Enum<E> & EnumWithCode> Optional<E> findByCode(final Class<E> enumClass, final int code) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (constant.code() == code) {
				return Optional.of(constant);
			}
		}
		return Optional.empty();
	}

	@NonNullByDefault
	public static <E extends Enum<E> & EnumWithChar> Optional<E> findByCharCode(final Class<E> enumClass, final int charCode) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (constant.code() == charCode) {
				return Optional.of(constant);
			}
		}
		return Optional.empty();
	}

	@Deprecated
	public static <T extends Enum<T>> boolean exists(final @NonNull Class<T> enumClass, final String name) {
		if (name != null) {
			try {
				Enum.valueOf(enumClass, name);
				return true;
			} catch (final @SuppressWarnings("unused") IllegalArgumentException ex) {
				// ignored
			}
		}
		return false;
	}

	/**
	 * Restituisce la costante di una enum corrispondente al nome dato.
	 * Se il nome è {@code null} restituisce {@code null}. Se il nome
	 * non esiste viene propagata una {@code IllegalArgumentException}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param name nome della costante dell'enum.
	 * @return la costante corrispondente al nome dato.
	 */
	public static <T extends Enum<T>> T valueOf(final @NonNull Class<T> enumClass, final String name) {
		return name == null ? (T) null : Enum.valueOf(enumClass, name);
	}

	/**
	 * Restituisce la costante di una enum corrispondente al nome dato.
	 * Se il nome è {@code null} restituisce {@code null}. Se il nome
	 * non esiste restituisce {@code null}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param name nome della costante dell'enum.
	 * @return la costante corrispondente al nome dato.
	 */
	public static <T extends Enum<T>> T safeValueOf(final @NonNull Class<T> enumClass, final String name) {
		return safeValueOf(enumClass, name, (T) null);
	}

	/**
	 * Restituisce la costante di una enum corrispondente al nome dato.
	 * Se il nome è {@code null} oppure non esiste restituisce
	 * un valore di default.
	 *
	 * @param enumClass classe dell'enum.
	 * @param name nome della costante dell'enum.
	 * @param defaultValue valore di default.
	 * @return la costante corrispondente al nome dato.
	 */
	public static <T extends Enum<T>> T safeValueOf(final @NonNull Class<T> enumClass, final String name, final T defaultValue) {
		if (name != null) {
			try {
				return Enum.valueOf(enumClass, name);
			} catch (final @SuppressWarnings("unused") IllegalArgumentException ex) {
				// ignored
			}
		}
		return defaultValue;
	}

	/**
	 * Restituisce la costante di una enum corrispondente alla chiave specificata.
	 * Se la chiave specificata è {@code null} restituisce {@code null}. Se
	 * non esiste una costante con la chiave specificata viene
	 * propagata una {@code IllegalArgumentException}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param key nome della chiave.
	 * @return la costante corrispondente alla chiave data.
	 */
	public static <E extends Enum<E> & EnumWithKey<T>, T> E valueOfKey(final @NonNull Class<E> enumClass, final T key) {
		if (key == null) {
			return null;
		}
		for (final E constant : enumClass.getEnumConstants()) {
			if (key.equals(constant.key())) {
				return constant;
			}
		}
		throw new IllegalArgumentException("No enum constant found with key = " + key);
	}

	/**
	 * Restituisce la costante di una enum corrispondente alla chiave specificata.
	 * Se la chiave specificata è {@code null} restituisce {@code null}. Se
	 * non esiste una costante con la chiave specificata
	 * restituisce {@code null}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param key nome della chiave.
	 * @return la costante corrispondente alla chiave data.
	 */
	public static <E extends Enum<E> & EnumWithKey<T>, T> E safeValueOfKey(final @NonNull Class<E> enumClass, final T key) {
		return safeValueOfKey(enumClass, key, (E) null);
	}

	/**
	 * Restituisce la costante di una enum corrispondente alla chiave specificata.
	 * Se la chiave specificata è {@code null} oppure non esiste una costante
	 * con la chiave specificata restituisce un valore di default.
	 *
	 * @param enumClass classe dell'enum.
	 * @param key nome della chiave.
	 * @param defaultValue valore di default.
	 * @return la costante corrispondente alla chiave data.
	 */
	public static <E extends Enum<E> & EnumWithKey<T>, T> E safeValueOfKey(final @NonNull Class<E> enumClass, final T key, final E defaultValue) {
		if (key != null) {
			for (final E constant : enumClass.getEnumConstants()) {
				if (key.equals(constant.key())) {
					return constant;
				}
			}
		}
		return defaultValue;
	}

	/**
	 * Restituisce la costante di una enum corrispondente alla chiave
	 * di tipo stringa specificata ignorando maiuscole/minuscole.
	 * Se la chiave specificata è {@code null} restituisce {@code null}. Se
	 * non esiste una costante con la chiave specificata viene
	 * propagata una {@code IllegalArgumentException}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param key nome della chiave.
	 * @return la costante corrispondente alla chiave data.
	 */
	public static <E extends Enum<E> & EnumWithKey<String>> E valueOfKeyIgnoreCase(final @NonNull Class<E> enumClass, final String key) {
		if (key == null) {
			return null;
		}
		for (final E constant : enumClass.getEnumConstants()) {
			if (key.equalsIgnoreCase(constant.key())) {
				return constant;
			}
		}
		throw new IllegalArgumentException("No enum constant found with key = " + key);
	}

	/**
	 * Restituisce la costante di una enum corrispondente alla chiave
	 * di tipo stringa specificata ignorando maiuscole/minuscole.
	 * Se la chiave specificata è {@code null} restituisce {@code null}. Se
	 * non esiste una costante con la chiave specificata
	 * restituisce {@code null}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param key nome della chiave.
	 * @return la costante corrispondente alla chiave data.
	 */
	public static <E extends Enum<E> & EnumWithKey<String>> E safeValueOfKeyIgnoreCase(final @NonNull Class<E> enumClass, final String key) {
		return safeValueOfKeyIgnoreCase(enumClass, key, (E) null);
	}

	/**
	 * Restituisce la costante di una enum corrispondente alla chiave
	 * di tipo stringa specificata ignorando maiuscole/minuscole.
	 * Se la chiave specificata è {@code null} oppure non esiste una
	 * costante con la chiave specificata restituisce un valore
	 * di default.
	 *
	 * @param enumClass classe dell'enum.
	 * @param key nome della chiave.
	 * @param defaultValue valore di default.
	 * @return la costante corrispondente alla chiave data.
	 */
	public static <E extends Enum<E> & EnumWithKey<String>> E safeValueOfKeyIgnoreCase(final @NonNull Class<E> enumClass, final String key, final E defaultValue) {
		if (key != null) {
			for (final E constant : enumClass.getEnumConstants()) {
				if (key.equalsIgnoreCase(constant.key())) {
					return constant;
				}
			}
		}
		return defaultValue;
	}

	/**
	 * Restituisce la costante di una enum corrispondente al codice specificato.
	 * Se non esiste una costante con il codice specificato viene
	 * propagata una {@code IllegalArgumentException}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param code nome del codice.
	 * @return la costante corrispondente al codice dato.
	 */
	public static <E extends Enum<E> & EnumWithCode> E valueOfCode(final @NonNull Class<E> enumClass, final int code) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (code == constant.code()) {
				return constant;
			}
		}
		throw new IllegalArgumentException("No enum constant found with code = " + code);
	}

	/**
	 * Restituisce la costante di una enum corrispondente al codice specificato.
	 * Se non esiste una costante con il codice specificato
	 * restituisce {@code null}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param code nome del codice.
	 * @return la costante corrispondente al codice dato.
	 */
	public static <E extends Enum<E> & EnumWithCode> E safeValueOfCode(final @NonNull Class<E> enumClass, final int code) {
		return safeValueOfCode(enumClass, code, (E) null);
	}

	/**
	 * Restituisce la costante di una enum corrispondente al codice specificato.
	 * Se non esiste una costante con il codice specificato
	 * restituisce un valore di default..
	 *
	 * @param enumClass classe dell'enum.
	 * @param code nome del codice.
	 * @param defaultValue valore di default.
	 * @return la costante corrispondente al codice dato.
	 */
	public static <E extends Enum<E> & EnumWithCode> E safeValueOfCode(final @NonNull Class<E> enumClass, final int code, final E defaultValue) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (code == constant.code()) {
				return constant;
			}
		}
		return defaultValue;
	}

	/**
	 * Restituisce la costante di una enum corrispondente al codice specificato.
	 * Se non esiste una costante con il codice specificato viene
	 * propagata una {@code IllegalArgumentException}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param code nome del codice.
	 * @return la costante corrispondente al codice dato.
	 */
	public static <E extends Enum<E> & EnumWithChar> E valueOfCode(final @NonNull Class<E> enumClass, final char code) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (code == constant.code()) {
				return constant;
			}
		}
		throw new IllegalArgumentException("No enum constant found with code = " + code);
	}

	/**
	 * Restituisce la costante di una enum corrispondente al codice specificato.
	 * Se non esiste una costante con il codice specificato
	 * restituisce {@code null}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param code nome del codice.
	 * @return la costante corrispondente al codice dato.
	 */
	public static <E extends Enum<E> & EnumWithChar> E safeValueOfCode(final @NonNull Class<E> enumClass, final char code) {
		return safeValueOfCode(enumClass, code, (E) null);
	}

	/**
	 * Restituisce la costante di una enum corrispondente al codice specificato.
	 * Se non esiste una costante con il codice specificato
	 * restituisce un valore di default..
	 *
	 * @param enumClass classe dell'enum.
	 * @param code nome del codice.
	 * @param defaultValue valore di default.
	 * @return la costante corrispondente al codice dato.
	 */
	public static <E extends Enum<E> & EnumWithChar> E safeValueOfCode(final @NonNull Class<E> enumClass, final char code, final E defaultValue) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (code == constant.code()) {
				return constant;
			}
		}
		return defaultValue;
	}

	/**
	 * Restituisce la costante di una enum corrispondente al codice specificato.
	 * Se il codice specificato è {@code null} restituisce {@code null}.
	 * Se non esiste una costante con il codice specificato viene
	 * propagata una {@code IllegalArgumentException}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param code nome del codice.
	 * @return la costante corrispondente al codice dato.
	 */
	public static <E extends Enum<E> & EnumWithCode> E valueOfCode(final @NonNull Class<E> enumClass, final Integer code) {
		if (code == null) {
			return null;
		}
		return valueOfCode(enumClass, code.intValue());
	}

	/**
	 * Restituisce la costante di una enum corrispondente al codice specificato.
	 * Se il codice specificato è {@code null} restituisce {@code null}.
	 * Se non esiste una costante con il codice specificato viene
	 * propagata una {@code IllegalArgumentException}.
	 *
	 * @param enumClass classe dell'enum.
	 * @param code nome del codice.
	 * @return la costante corrispondente al codice dato.
	 */
	public static <E extends Enum<E> & EnumWithChar> E valueOfCode(final @NonNull Class<E> enumClass, final Character code) {
		if (code == null) {
			return null;
		}
		return valueOfCode(enumClass, code.charValue());
	}

	/**
	 * Restituisce il nome di una costante.
	 * Se la costante è {@code null} restituisce {@code null}.
	 *
	 * @param constant costante di enum.
	 * @return il nome della costante.
	 */
	public static <E extends Enum<E>> String name(final E constant) {
		return name(constant, null);
	}

	/**
	 * Restituisce il nome di una costante.
	 * Se la costante è {@code null} restituisce un valore
	 * di default.
	 *
	 * @param constant costante di enum.
	 * @param defaultValue valore di default.
	 * @return il nome della costante o il valore
	 *         di default.
	 */
	public static <E extends Enum<E>> String name(final E constant, final String defaultValue) {
		return constant == null ? defaultValue : constant.name();
	}

	/**
	 * Restituisce la chiave di una costante.
	 * Se la costante è {@code null} restituisce {@code null}.
	 *
	 * @param constant costante di enum.
	 * @return la chiave corrispondente.
	 */
	public static <T, E extends Enum<E> & EnumWithKey<T>> T key(final E constant) {
		return key(constant, (T) null);
	}

	/**
	 * Restituisce la chiave di una costante.
	 * Se la costante è {@code null} restituisce un
	 * valore di default.
	 *
	 * @param constant costante di enum.
	 * @param defaultValue valore di default.
	 * @return la chiave corrispondente o il valore di
	 *         default.
	 */
	public static <T, E extends Enum<E> & EnumWithKey<T>> T key(final E constant, final T defaultValue) {
		return constant == null ? defaultValue : constant.key();
	}

	/**
	 * Restituisce il codice di una costante.
	 * Se la costante è {@code null} restituisce {@code null}.
	 *
	 * @param constant costante di enum.
	 * @return il codice corrispondente
	 *         come oggetto {@code Integer}.
	 */
	public static <E extends Enum<E> & EnumWithCode> Integer code(final E constant) {
		return constant == null ? null : Integer.valueOf(constant.code());
	}

	/**
	 * Restituisce il codice di tipo Character di una costante.
	 * Se la costante è {@code null} restituisce {@code null}.
	 *
	 * @param constant costante di enum.
	 * @return il codice corrispondente
	 *         come oggetto {@code Character}.
	 */
	public static <E extends Enum<E> & EnumWithChar> Character charCode(final E constant) {
		return constant == null ? null : Character.valueOf(constant.code());
	}

	/**
	 * Restituisce la descrizione di una costante.
	 * Se la costante è {@code null} restituisce un
	 * valore di default.
	 *
	 * @param constant costante di enum.
	 * @param defaultValue il valore di default.
	 * @return la descrizione corrispondente.
	 */
	public static <E extends Enum<E> & EnumWithDescription> String description(final E constant, final String defaultValue) {
		return constant == null ? defaultValue : constant.description();
	}

	/**
	 * Restituisce la descrizione di una costante.
	 * Se la costante è {@code null} restituisce {@code null}.
	 *
	 * @param constant costante di enum.
	 * @return la descrizione corrispondente.
	 */
	public static <E extends Enum<E> & EnumWithDescription> String description(final E constant) {
		return description(constant, null);
	}

	/**
	 * Restituisce una collezione {@code EnumSet} a partire
	 * da un array di costanti.
	 *
	 * @param enumClass classe della enum.
	 * @param constants costanti di enum.
	 */
	public static @SafeVarargs <E extends Enum<E>> EnumSet<E> toEnumSet(final Class<E> enumClass, final E... constants) {
		return constants.length > 0 ? EnumSet.of(constants[0], constants) : EnumSet.noneOf(enumClass);
	}
}
