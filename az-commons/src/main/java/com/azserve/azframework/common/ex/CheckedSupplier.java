package com.azserve.azframework.common.ex;

import static java.util.Objects.requireNonNull;

import java.util.function.Function;
import java.util.function.Supplier;

public interface CheckedSupplier<T, E extends Exception> {

	T get() throws E;

	/**
	 * Unwraps this checked supplier to an unchecked one.
	 */
	default Supplier<T> unchecked() {
		return () -> {
			try {
				return this.get();
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return null;
			}
		};
	}

	/**
	 * Provides a way to handle the exception and to return an alternative value.
	 *
	 * <pre>
	 * CheckedSupplier.of(this::throwException).onError(ex -> defaultValue);
	 * </pre>
	 *
	 * @param predicate the exception handler
	 */
	default Supplier<T> onError(final Function<Exception, T> function) {
		return () -> {
			try {
				return this.get();
			} catch (final Exception ex) {
				return function.apply(ex);
			}
		};
	}

	/**
	 * Creates a {@link CheckedSupplier} by using a method reference.
	 *
	 * @param supplier the supplier as method reference
	 * @return the supplier
	 */
	static <T, E extends Exception> CheckedSupplier<T, E> of(final CheckedSupplier<T, E> supplier) {
		return supplier;
	}

	/**
	 * Wraps an unchecked supplier in an unchecked one.
	 *
	 * @param supplier the unchecked supplier
	 */
	static <T, E extends Exception> CheckedSupplier<T, E> wrap(final Supplier<T> supplier) {
		return supplier::get;
	}

	/**
	 * Unwraps a checked supplier to have an unchecked one.
	 * The exception is then propagated to this method caller.
	 *
	 * @throws E the propagated exception
	 */
	static <T, E extends Exception> Supplier<T> unwrap(final CheckedSupplier<T, E> supplier) throws E {
		requireNonNull(supplier);
		return () -> {
			try {
				return supplier.get();
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return null;
			}
		};
	}

	/**
	 * Unwraps a checked supplier to have an unchecked one.
	 */
	static <T, E extends Exception> Supplier<T> unchecked(final CheckedSupplier<T, E> supplier) {
		return () -> {
			try {
				return supplier.get();
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return null;
			}
		};
	}
}
