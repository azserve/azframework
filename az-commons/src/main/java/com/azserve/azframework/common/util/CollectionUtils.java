package com.azserve.azframework.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Stream;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;

/**
 * Classe di utilità per le {@linkplain Collection}.
 *
 * @author filippo
 * @since 30/12/2012
 */
public final class CollectionUtils {

	private CollectionUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Restituisce uno {@code Stream} sugli elementi
	 * delle collezioni specificate.
	 *
	 * @param collections collezioni interessate.
	 *
	 * @throws NullPointerException se l'array di collezioni
	 *             è {@code null}
	 *             oppure una delle collezioni
	 *             è {@code null}.
	 */
	public static @SafeVarargs <T> Stream<T> streamOf(final @NonNull Collection<? extends T>... collections) {
		return Stream.of(collections).flatMap(Collection::stream);
	}

	/**
	 * Fornisce una lista in sola lettura composta dal solo oggetto dato.<br>
	 * Se l'oggetto è {@code null}, restituisce una lista vuota.
	 *
	 * @param item oggetto da inserire nella lista.
	 */
	public static @NonNull <T> List<T> singletonList(final @Nullable T item) {
		if (item == null) {
			return Collections.emptyList();
		}
		return Collections.singletonList(item);
	}

	/**
	 * Fornisce un set in sola lettura composta dal solo oggetto dato.<br>
	 * Se l'oggetto è {@code null}, restituisce un set vuoto.
	 *
	 * @param item oggetto da inserire nel set.
	 */
	public static @NonNull <T> Set<T> singletonSet(final @Nullable T item) {
		if (item == null) {
			return Collections.emptySet();
		}
		return Collections.singleton(item);
	}

	/**
	 * Restituisce il primo elemento di una lista, se presente
	 * sotto forma di {@code Optional}, altrimenti un
	 * oggetto {@code Optional} vuoto.
	 *
	 * @param list lista da analizzare.
	 * @throws NullPointerException se la lista specificata
	 *             o il primo elemento della lista è {@code null}.
	 */
	public static <T> Optional<T> findFirst(final @NonNull List<T> list) {
		return list.isEmpty() ? Optional.empty() : Optional.of(list.get(0));
	}

	/**
	 * Restituisce un qualsiasi elemento di una collezione,
	 * se presente sotto forma di {@code Optional}, altrimenti un
	 * oggetto {@code Optional} vuoto.
	 *
	 * @param collection collezione da analizzare.
	 * @throws NullPointerException se la collezione specificata
	 *             o l'elemento prelevato dalla collezione è {@code null}.
	 */
	public static <T> Optional<T> findAny(final @NonNull Collection<T> collection) {
		return collection.isEmpty() ? Optional.empty() : Optional.of(collection.iterator().next());
	}

	/**
	 * Restituisce il prossimo elemento tramite l'iteratore
	 * specificato sotto forma di {@code Optional}, altrimenti un
	 * oggetto {@code Optional} vuoto.
	 *
	 * @param list lista da analizzare.
	 * @throws NullPointerException se l'iteratore specificato
	 *             o l'elemento restituito è {@code null}.
	 */
	public static <T> Optional<T> next(final @NonNull Iterator<T> iterator) {
		return iterator.hasNext() ? Optional.of(iterator.next()) : Optional.empty();
	}

	public static <T> Iterator<T> iterator() {
		return Collections.emptyIterator();
	}

	public static <T> Iterator<T> iterator(final T element) {
		return new SingleElementIterator<>(element);
	}

	/**
	 * Restituisce il primo elemento di una collezione.
	 * <br>
	 * Se la collezione è {@code null} o non ha elementi restituisce {@code null}.
	 * <br>
	 * Se la collezione non è ordinata restituisce un elemento a caso.
	 *
	 * <pre>
	 * Collection&lt;String&gt; c1 = Arrays.asList("1","2");
	 * getFirstOrNull(c1) -> "1"
	 *
	 * Collection&lt;String&gt; c2 = Collections.emptyList();
	 * getFirstOrNull(c2) -> null
	 * </pre>
	 *
	 * @param c la collezione oppure {@code null}.
	 * @return il primo elemento della {@linkplain Collection} oppure {@code null}.
	 */
	public static <T> T getFirstOrNull(final Collection<T> c) {
		return c == null || c.isEmpty() ? (T) null : c.iterator().next();
	}

	/**
	 * Restituisce il primo elemento e unico elemento di una collezione.
	 * <br>
	 * Se la collezione è {@code null}, non ha elementi o ha più di un elemento restituisce {@code null}.
	 * <br>
	 * Se la collezione non è ordinata restituisce un elemento a caso.
	 *
	 * <pre>
	 * Collection&lt;String&gt; c1 = Collections.singleton("1");
	 * getSingleOrNull(c1) -> "1"
	 *
	 * Collection&lt;String&gt; c2 = Arrays.asList("1","2");
	 * getSingleOrNull(c2) -> null
	 *
	 * Collection&lt;String&gt; c3 = Collections.emptyList();
	 * getSingleOrNull(c3) -> null
	 * </pre>
	 *
	 * @param c la collezione oppure {@code null}.
	 * @return il primo elemento della {@linkplain Collection} oppure {@code null}.
	 */
	public static <T> T getSingleOrNull(final Collection<T> c) {
		return c == null || c.size() != 1 ? (T) null : c.iterator().next();
	}

	/**
	 * Controlla se una collezione è {@code null} oppure vuota.
	 *
	 * @param c la collezione oppure {@code null}.
	 */
	public static boolean isNullOrEmpty(final Collection<?> c) {
		return c == null || c.isEmpty();
	}

	/**
	 * Controlla se una collezione non è {@code null} e contiene almeno
	 * un elemento.
	 *
	 * @param c la collezione.
	 */
	public static boolean hasElements(final Collection<?> c) {
		return c != null && !c.isEmpty();
	}

	/**
	 * Verifica se un indice è valido all'interno di una lista.
	 *
	 * @param list lista da verificare.
	 * @param ix indice.
	 */
	public static boolean isValidIndex(final @NonNull List<?> list, final int ix) {
		return ix >= 0 && ix < list.size();
	}

	/**
	 * Restituisce l'elemento di una lista all'indice
	 * specificato contenuto in un {@code Optional}.
	 * Se l'indice specificato non è valido (negativo o
	 * oltre la dimensione della lista) restituisce un
	 * oggetto {@code Optional} vuoto.
	 *
	 * @param list lista da analizzare (obbligatoria).
	 * @param ix indice dell'elemento.
	 *
	 * @throws NullPointerException se la lista specificata
	 *             o il l'elemento della lista all'indice
	 *             specificato è {@code null}.
	 */
	public static <T> Optional<T> get(final @NonNull List<T> list, final int ix) {
		if (isValidIndex(list, ix)) {
			return Optional.of(list.get(ix));
		}
		return Optional.empty();
	}

	/**
	 * Restituisce l'ultima coppia chiave-valore di una {@code LinkedHashMap}.
	 *
	 * @param map mappa da analizzare.
	 * @return l'ultima entry oppure {@code null} se la mappa è vuota.
	 * @throws NullPointerException se la mappa specificata è {@code null}.
	 */
	public static <K, V> Map.Entry<K, V> findLastEntry(final @NonNull LinkedHashMap<K, V> map) {
		final Iterator<Map.Entry<K, V>> iterator = map.entrySet().iterator();
		Map.Entry<K, V> last = null;
		while (iterator.hasNext()) {
			last = iterator.next();
		}
		return last;
	}

	/**
	 * Effettua una ricerca binaria per chiave, anzichè per oggetto dello
	 * stesso tipo.
	 *
	 * @param list lista ordinata su cui effettuare la ricerca.
	 * @param key chiave da cercare.
	 * @param keyClass classe della chiave da cercare.
	 * @param keyExtractor funzione di estrazione della chiave
	 *            dagli oggetti della lista per effettuare il confronto.
	 *
	 * @since 2.5.6
	 *
	 * @see Collections#binarySearch(List, Object, Comparator)
	 */
	public static <T, K extends Comparable<? super K>> int binarySearch(final @NonNull List<T> list, final @NonNull Class<K> keyClass, final K key, final @NonNull Function<T, K> keyExtractor) {
		return binarySearch(list, keyClass, key, keyExtractor, Comparator.naturalOrder());
	}

	/**
	 * Effettua una ricerca binaria per chiave, anzichè per oggetto dello
	 * stesso tipo.
	 *
	 * @param list lista ordinata su cui effettuare la ricerca.
	 * @param key chiave da cercare.
	 * @param keyClass classe della chiave da cercare.
	 * @param keyExtractor funzione di estrazione della chiave
	 *            dagli oggetti della lista per effettuare il confronto.
	 * @param comparator operatore di confronto tra chiavi.
	 *
	 * @since 2.5.6
	 *
	 * @see Collections#binarySearch(List, Object)
	 */
	@SuppressWarnings("unchecked")
	public static <T, K> int binarySearch(final @NonNull List<T> list, final @NonNull Class<K> keyClass, final K key, final @NonNull Function<T, K> keyExtractor, final @NonNull Comparator<K> comparator) {
		final Comparator<Object> c = (o1, o2) -> {
			final K key1 = keyClass.isInstance(o1) ? (K) o1 : keyExtractor.apply((T) o1);
			final K key2 = keyClass.isInstance(o2) ? (K) o2 : keyExtractor.apply((T) o2);
			return comparator.compare(key1, key2);
		};

		return Collections.<Object> binarySearch(list, key, c);
	}

	/**
	 * Verifica se una collezione è {@code null} o vuota. In tal caso
	 * viene generata un'eccezione, altrimenti viene restituita
	 * la collezione stessa.
	 * <p>
	 * Questo è un metodo di utilità per la verifica dei parametri
	 * di un metodo basato su {@link Objects#requireNonNull(Object, String)}.
	 * </p>
	 *
	 * @param c collezione da verificare.
	 * @param message messaggio dell'eccezione.
	 * @param <T> qualsiasi tipo di collezione.
	 *
	 * @return la collezione stessa.
	 * @throws IllegalArgumentException se la collezione è {@code null} oppure vuota (nessun elemento).
	 */
	@NonNull
	public static <T extends Collection<?>> T requireElements(final @Nullable T c, final @NonNull String message) {
		if (c == null || c.isEmpty()) {
			throw new IllegalArgumentException(message);
		}
		return c;
	}

	/**
	 * Rimuove gli elementi di una {@code ArrayList} posizionati
	 * tra due indici specificati.
	 * &Egrave; consigliato utilizzare questa soluzione
	 * anzichè ciclare chiamando il
	 * metodo {@code remove(index)} più volte.
	 *
	 * @param l la lista da modificare.
	 * @param from indice iniziale.
	 * @param to indice finale.
	 *
	 * @throws NullPointerException se la lista specificata è {@code null}.
	 * @throws IndexOutOfBoundsException se l'indice iniziale
	 *             è negativo o se l'indice finale supera la
	 *             lunghezza della lista.
	 * @throws IllegalArgumentException se l'indice iniziale
	 *             è maggiore di quello finale.
	 */
	public static void remove(final @NonNull ArrayList<?> l, final int from, final int to) {
		l.subList(from, to).clear();
	}

	/**
	 * Restituisce l'oggetto con valore più basso in una
	 * collezione dettato dal
	 * metodo {@linkplain Comparable#compareTo(Object)}.
	 *
	 * @param c collezione di oggetti da confrontare.
	 * @return l'oggetto con valore più basso.
	 * @throws NullPointerException se {@code c} è {@code null}.
	 */
	public static <T extends Comparable<T>> T min(final @NonNull Collection<T> c) {
		T min = null;
		final Iterator<T> it = c.iterator();
		while (it.hasNext()) {
			final T current = it.next();
			if (min == null || current.compareTo(min) < 0) {
				min = current;
			}
		}
		return min;
	}

	/**
	 * Restituisce l'oggetto con valore più basso in una
	 * collezione dettato dal
	 * metodo {@linkplain Comparator#compare(Object, Object)}.
	 *
	 * @param c collezione di oggetti da confrontare.
	 * @param comparator oggetto utilizzato per il confronto.
	 * @return l'oggetto con valore più basso.
	 * @throws NullPointerException se {@code c} e/o {@code comparator} sono {@code null}.
	 */
	public static <T> T min(final @NonNull Collection<T> c, final @NonNull Comparator<? super T> comparator) {
		// JAVA8 : c.stream().min(comparator).orElse(null)
		T min = null;
		final Iterator<T> it = c.iterator();
		while (it.hasNext()) {
			final T current = it.next();
			if (min == null || comparator.compare(current, min) < 0) {
				min = current;
			}
		}
		return min;
	}

	/**
	 * Restituisce l'oggetto con valore più alto in una
	 * collezione dettato dal
	 * metodo {@linkplain Comparable#compareTo(Object)}.
	 *
	 * @param c collezione di oggetti da confrontare.
	 * @return l'oggetto con valore più alto.
	 * @throws NullPointerException se {@code c} è {@code null}.
	 */
	public static <T extends Comparable<T>> T max(final @NonNull Collection<T> c) {
		T max = null;
		final Iterator<T> it = c.iterator();
		while (it.hasNext()) {
			final T current = it.next();
			if (max == null || current.compareTo(max) > 0) {
				max = current;
			}
		}
		return max;
	}

	/**
	 * Restituisce l'oggetto con valore più alto in una
	 * collezione dettato dal
	 * metodo {@linkplain Comparator#compare(Object, Object)}.
	 *
	 * @param c collezione di oggetti da confrontare.
	 * @param comparator oggetto utilizzato per il confronto.
	 * @return l'oggetto con valore più alto.
	 * @throws NullPointerException se {@code c} e/o {@code comparator} sono {@code null}.
	 */
	public static <T> T max(final @NonNull Collection<T> c, final @NonNull Comparator<? super T> comparator) {
		// JAVA8 : c.stream().max(comparator).orElse(null)
		T max = null;
		final Iterator<T> it = c.iterator();
		while (it.hasNext()) {
			final T current = it.next();
			if (max == null || comparator.compare(current, max) > 0) {
				max = current;
			}
		}
		return max;
	}

	/**
	 * Restituisce una nuova lista ordinata a partire da una {@code Collection}.
	 *
	 * @param c la collezione sorgente (obbligatora).
	 * @param comparator il comparatore per ordinare gli elementi (obbligatorio).
	 * @return la lista ordinata.
	 */
	public static @NonNull <T> List<T> toSortedList(final @NonNull Collection<T> c, final @NonNull Comparator<T> comparator) {
		// JAVA8 : c.stream().sorted(comparator).collect(Collectors.toCollection(ArrayList::new))
		final List<T> list = new ArrayList<>(c);
		Collections.sort(list, comparator);
		return list;
	}

	/**
	 * Aggiunge gli elementi specificati ad una collezione
	 * e restituisce la collezione stessa.
	 *
	 * @param objs array di oggetti da inserire.
	 *
	 * @return la collezione stessa.
	 */
	public static @SafeVarargs @NonNull <C extends Collection<T>, T> C addAll(final @NonNull C collection, final @NonNull T... objs) {
		Collections.addAll(collection, objs);
		return collection;
	}

	/**
	 * Restituisce un nuovo {@link Set} in sola lettura composto
	 * dagli elementi dell'array dato.
	 * (l'ordine di inserimento degli elementi non è garantito)
	 * <br>
	 * Non accetta un array {@code null}.
	 *
	 * @param objs array di oggetti da inserire.
	 *
	 * @return un set composto dagli oggetti specificati.
	 */
	public static @SafeVarargs @NonNull <T> Set<T> setOf(final @NonNull T... objs) {
		if (objs.length == 1) {
			return Collections.singleton(objs[0]);
		}
		// JAVA8 : Stream.of(objs).collect(Collectors.collectingAndThen(Collectors.toSet(), Collections::unmodifiableSet))
		final Set<T> set = new HashSet<>(objs.length);
		Collections.addAll(set, objs);
		return Collections.unmodifiableSet(set);
	}

	/**
	 * Restituisce una mappa modificabile che ha per chiave gli elementi
	 * della collezione specificata e per valore {@code null}.
	 *
	 * @param items collezione contenente gli elementi.
	 */
	public static <K, V> Map<K, V> toMap(final @NonNull Collection<K> items) {
		final Map<K, V> map = new HashMap<>();
		for (final K item : items) {
			map.put(item, null);
		}
		return map;
	}

	/**
	 * Restituisce un array contenente gli elementi della collezione specificata.
	 *
	 * @param collection collezione contenente gli elementi.
	 * @param arraySupplier funzione che restituisce un array
	 *            della dimensione necessaria.
	 */
	public static <T> T[] toArray(final @NonNull Collection<T> collection, final IntFunction<T[]> arraySupplier) {
		return collection.toArray(arraySupplier.apply(collection.size()));
	}

	/**
	 *
	 * Applica operazioni diverse agli elementi di due collezioni a seconda che siano presenti nella prima, nella seconda o in entrambe.
	 *
	 * L'uguaglianza tra elementi viene stabilita dalla funzione test.
	 *
	 * La ricerca non è efficiente come potrebbe essere tra {@code Set} ma è quadratica (|c1| * |c2|).
	 *
	 * @param c1 collezione 1
	 * @param c2 collezione 2
	 * @param equalTest test che indica se un elemento di c1 è uguale ad uno di c2
	 * @param onlyFirst consumer applicato a tutti gli elementi presenti in c1 ma non in c2 secondo quanto stabilito da equalTest
	 * @param onlySecond consumer applicato a tutti gli elementi presenti in c2 ma non in c1 secondo quanto stabilito da equalTest
	 * @param intersection consumer applicato a tutti gli elementi presenti sia in c1 che in c2 secondo quanto stabilito da equalTest
	 */
	public static <T1, T2> void match(
			final @NonNull Collection<T1> c1,
			final @NonNull Collection<T2> c2,
			final @NonNull BiPredicate<T1, T2> equalTest,
			final @NonNull Consumer<T1> onlyFirst,
			final @NonNull Consumer<T2> onlySecond,
			final @NonNull BiConsumer<T1, T2> intersection) {

		final List<T1> l1 = new ArrayList<>();
		for (final T1 t1 : c1) {
			boolean present = false;
			for (final T2 t2 : c2) {
				if (equalTest.test(t1, t2)) {
					intersection.accept(t1, t2);
					present = true;
				}
			}
			if (!present) {
				onlyFirst.accept(t1);
			} else {
				l1.add(t1);
			}
		}
		for (final T2 t2 : c2) {
			boolean present = false;
			for (final T1 t1 : l1) {
				if (equalTest.test(t1, t2)) {
					present = true;
					break;
				}
			}
			if (!present) {
				onlySecond.accept(t2);
			}
		}
	}

	private static final class SingleElementIterator<E> implements Iterator<E> {

		private final E element;
		private boolean hasNext = true;

		SingleElementIterator(final E element) {
			this.element = element;
		}

		@Override
		public boolean hasNext() {
			return this.hasNext;
		}

		@Override
		public E next() {
			this.hasNext = false;
			return this.element;
		}
	}
}
