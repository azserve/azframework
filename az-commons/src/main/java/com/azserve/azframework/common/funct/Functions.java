package com.azserve.azframework.common.funct;

import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Fornisce delle funzioni comuni.
 *
 * @author filippo
 * @since 12/11/2014
 */
public final class Functions {

	private Functions() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Implementazione di {@link Runnable} che non fa nulla.
	 */
	public static final @NonNull Runnable NULL_RUNNABLE = () -> {};

	/**
	 * Fornisce una funzione {@link Supplier} che restituisce {@code null}.
	 */
	public static @NonNull <T> Supplier<T> nullSupplier() {
		return () -> null;
	}

	/**
	 * Fornisce una funzione {@link Function} che restituisce {@code null}.
	 */
	public static @NonNull <T, R> Function<T, R> nullFunction() {
		return t -> null;
	}

	/**
	 * Fornisce una funzione {@link BiFunction} che restituisce {@code null}.
	 */
	public static @NonNull <T, U, R> BiFunction<T, U, R> nullBiFunction() {
		return (t, u) -> null;
	}

	/**
	 * Fornisce una funzione {@link TriFunction} che restituisce {@code null}.
	 */
	public static @NonNull <T, U, V, R> TriFunction<T, U, V, R> nullTriFunction() {
		return (t, u, v) -> null;
	}

	/**
	 * Fornisce una funzione {@link IntFunction} che restituisce {@code null}.
	 */
	public static @NonNull <R> IntFunction<R> nullIntFunction() {
		return i -> null;
	}

	/**
	 * Fornisce una funzione {@link Consumer} che non fa nulla.
	 */
	public static @NonNull <T> Consumer<T> nullConsumer() {
		return t -> {};
	}

	/**
	 * Fornisce una funzione {@link BiConsumer} che non fa nulla.
	 */
	public static @NonNull <T, U> BiConsumer<T, U> nullBiConsumer() {
		return (t, u) -> {};
	}

	/**
	 * Fornisce una funzione {@link TriConsumer} che non fa nulla.
	 */
	public static @NonNull <T, U, V> TriConsumer<T, U, V> nullTriConsumer() {
		return (t, u, v) -> {};
	}

	/**
	 * Fornisce una funzione {@link Function} che
	 * restituisce sempre lo stesso oggetto in ingresso.
	 */
	public static @NonNull <T> Function<T, T> identity() {
		return t -> t;
	}

	/**
	 * Fornisce una funzione {@link Predicate} che
	 * da sempre risultato positivo.
	 */
	public static @NonNull <T> Predicate<T> acceptAll() {
		return t -> true;
	}

	/**
	 * Fornisce una funzione {@link Predicate} che
	 * da sempre risultato negativo.
	 */
	public static @NonNull <T> Predicate<T> declineAll() {
		return t -> false;
	}

	/**
	 * Fornisce una funzione {@link BiPredicate} che
	 * da sempre risultato positivo.
	 */
	public static @NonNull <T, U> BiPredicate<T, U> biAcceptAll() {
		return (t, u) -> true;
	}

	/**
	 * Fornisce una funzione {@link BiPredicate} che
	 * da sempre risultato negativo.
	 */
	public static @NonNull <T, U> BiPredicate<T, U> biDeclineAll() {
		return (t, u) -> false;
	}

	/**
	 * Fornisce una funzione {@link BinaryOperator} che
	 * restituisce sempre il primo argomento.
	 */
	public static @NonNull <T> BinaryOperator<T> acceptFirst() {
		return (o1, o2) -> o1;
	}

	/**
	 * Fornisce una funzione {@link BinaryOperator} che
	 * restituisce sempre il secondo argomento.
	 */
	public static @NonNull <T> BinaryOperator<T> acceptSecond() {
		return (o1, o2) -> o2;
	}

	/**
	 * Fornisce una funzione {@link BinaryOperator} che
	 * restituisce il primo argomento se passa
	 * la funzione {@link Predicate} specificata, altrimenti
	 * restituisce il secondo argomento.
	 */
	public static @NonNull <T> BinaryOperator<T> acceptFirstIf(final Predicate<T> predicate) {
		return (o1, o2) -> predicate.test(o1) ? o1 : o2;
	}

	/**
	 * Fornisce una funzione {@link BinaryOperator} che
	 * restituisce il secondo argomento se passa
	 * la funzione {@link Predicate} specificata, altrimenti
	 * restituisce il primo argomento.
	 */
	public static @NonNull <T> BinaryOperator<T> acceptSecondIf(final Predicate<T> predicate) {
		return (o1, o2) -> predicate.test(o2) ? o2 : o1;
	}

	/**
	 * Restituisce una funzione {@link Function} che invoca
	 * il {@link Consumer} specificato e restituisce {@code null}.
	 */
	public static @NonNull <T> Function<T, Void> consumerAsFunction(final @NonNull Consumer<T> consumer) {
		return t -> {
			consumer.accept(t);
			return null;
		};
	}

	/**
	 * Restituisce una funzione {@link Function} che restituisce
	 * il risultato fornitore dal {@link Supplier} specificato.
	 */
	public static @NonNull <T, X> Function<T, X> supplierAsFunction(final @NonNull Supplier<X> supplier) {
		return t -> supplier.get();
	}

	/**
	 * Restituisce una funzione {@link BooleanConsumer} che invoca
	 * il {@link Runnable} specificato se l'argomento in input è {@code true}.
	 */
	public static @NonNull BooleanConsumer runIfTrue(final @NonNull Runnable runnable) {
		return b -> {
			if (b) {
				runnable.run();
			}
		};
	}

	// other

	/**
	 * Restituisce una funzione {@link BinaryOperator} da utilizzare
	 * nel metodo {@linkplain java.util.Map#merge(Object, Object, java.util.function.BiFunction)} per
	 * mantenere il valore maggiore in caso di chiave duplicata nella mappa.
	 */
	public static @NonNull <T extends Comparable<? super T>> BinaryOperator<T> mergeGreatest() {
		return mergeGreatest(Functions.<T> identity());
	}

	/**
	 * Restituisce una funzione {@link BinaryOperator} da utilizzare
	 * nel metodo {@linkplain java.util.Map#merge(Object, Object, java.util.function.BiFunction)} per
	 * mantenere il valore maggiore in caso di chiave duplicata nella mappa.
	 *
	 * @param function funzione che restituisce il valore da comparare.
	 * @since 11/06/2015
	 */
	public static @NonNull <T, D extends Comparable<? super D>> BinaryOperator<T> mergeGreatest(final @NonNull Function<T, D> function) {
		return mergeGreatest(function, Comparator.naturalOrder());
	}

	/**
	 * Restituisce una funzione {@link BinaryOperator} da utilizzare
	 * nel metodo {@linkplain java.util.Map#merge(Object, Object, java.util.function.BiFunction)} per
	 * mantenere il valore maggiore in caso di chiave duplicata nella mappa.
	 *
	 * @param function funzione che restituisce il valore da comparare.
	 * @param comparator comparatore per analizzare i valori.
	 * @since 23/12/2015
	 */
	public static @NonNull <T, D> BinaryOperator<T> mergeGreatest(final @NonNull Function<T, D> function, final @NonNull Comparator<D> comparator) {
		return (oldVal, newVal) -> comparator.compare(function.apply(oldVal), function.apply(newVal)) > 0 ? oldVal : newVal;
	}

	/**
	 * Restituisce una funzione {@link BinaryOperator} da utilizzare
	 * nel metodo {@linkplain java.util.Map#merge(Object, Object, java.util.function.BiFunction)} per
	 * mantenere il valore minore in caso di chiave duplicata nella mappa.
	 */
	public static @NonNull <T extends Comparable<? super T>> BinaryOperator<T> mergeLeast() {
		return mergeLeast(Functions.<T> identity());
	}

	/**
	 * Restituisce una funzione {@link BinaryOperator} da utilizzare
	 * nel metodo {@linkplain java.util.Map#merge(Object, Object, java.util.function.BiFunction)} per
	 * mantenere il valore minore in caso di chiave duplicata nella mappa.
	 *
	 * @param function funzione che restituisce il valore da comparare.
	 * @since 11/06/2015
	 */
	public static @NonNull <T, D extends Comparable<? super D>> BinaryOperator<T> mergeLeast(final @NonNull Function<T, D> function) {
		return mergeLeast(function, Comparator.naturalOrder());
	}

	/**
	 * Restituisce una funzione {@link BinaryOperator} da utilizzare
	 * nel metodo {@linkplain java.util.Map#merge(Object, Object, java.util.function.BiFunction)} per
	 * mantenere il valore minore in caso di chiave duplicata nella mappa.
	 *
	 * @param function funzione che restituisce il valore da comparare.
	 * @param comparator comparatore per analizzare i valori.
	 * @since 11/06/2015
	 */
	public static @NonNull <T, D> BinaryOperator<T> mergeLeast(final @NonNull Function<T, D> function, final @NonNull Comparator<D> comparator) {
		return (oldVal, newVal) -> comparator.compare(function.apply(oldVal), function.apply(newVal)) < 0 ? oldVal : newVal;
	}

	/**
	 * Funzione che filtra elementi duplicati basandosi su una proprietà.
	 * Esempio:
	 * rimuovere le operazioni con la stessa data
	 *
	 * <pre>
	 * operazioni.stream().filter(Functions.distinctByKey(op -> op.getData()))...
	 * </pre>
	 *
	 * @param keyExtractor funzione che restituisce il valore su cui filtrare.
	 */
	public static @NonNull <T> Predicate<T> distinctByKey(final @NonNull Function<? super T, Object> keyExtractor) {
		final Set<Object> seen = Collections.newSetFromMap(new ConcurrentHashMap<>());
		return t -> seen.add(keyExtractor.apply(t));
	}

	/**
	 * Restituisce una funzione {@link Predicate} che
	 * nega quella data in parametro.
	 *
	 * <pre>
	 * ..stream().filter(Functions.negate(Test::isValid))..
	 * </pre>
	 *
	 * Il metodo è utile per negare un {@code Predicate} dichiato con
	 * una method reference come da esempio.
	 *
	 */
	@SuppressWarnings("unchecked")
	public static @NonNull <T> Predicate<T> negate(final @NonNull Predicate<? super T> predicate) {
		return (Predicate<T>) predicate.negate();
	}
}
