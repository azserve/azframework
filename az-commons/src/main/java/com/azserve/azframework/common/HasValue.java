package com.azserve.azframework.common;

import java.util.Optional;

/**
 * Interfaccia comune per le classi con valore.
 *
 * @author filippo
 * @since 09/02/2017
 */
@FunctionalInterface
public interface HasValue<T> {

	/**
	 * Restituisce il valore.
	 */
	T getValue();

	/**
	 * Restituisce il valore tramite {@linkplain Optional}.
	 */
	default Optional<T> getOptionalValue() {
		return Optional.ofNullable(getValue());
	}
}
