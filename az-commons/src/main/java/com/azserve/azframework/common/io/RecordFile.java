package com.azserve.azframework.common.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.lang.StringUtils;

/**
 * Classe che si occupa di scrivere un tracciato record composto da campi
 * di lunghezza fissa.
 *
 * @author filippo
 * @since 30/05/2013
 */
public class RecordFile {

	/** Contenuto del tracciato record. */
	@NonNull
	private StringBuilder strBuilder = new StringBuilder();

	/** Filler di dati di tipo stringa. */
	@NonNull
	private Filler<String> defaultStringFiller = new DefaultStringFiller();

	/** Filler di dati di tipo intero. */
	@NonNull
	private Filler<Integer> defaultIntegerFiller = new DefaultIntegerFiller();

	/** Filler di dati di tipo data. */
	@NonNull
	private Filler<Date> defaultDateFiller = new DateFiller("yyyyMMdd");

	/** Array delle lunghezze dei campi della riga corrente */
	private int[] currentRowFieldsLength = null;

	/** Indice del campo corrente */
	private int fieldIndex = 0;

	/** Indice della riga corrente */
	private int rowIndex = -1;

	/** Carattere di riempimento degli spazi vuoti */
	private final char emptyChar;

	/** Istanzia un nuovo RecordFile. */
	public RecordFile() {
		this(' ');
	}

	/**
	 * Istanzia un nuovo RecordFile specificando il carattere di riempimento.
	 *
	 * @param emptyChar carattere di riempimento.
	 */
	public RecordFile(final char emptyChar) {
		this.emptyChar = emptyChar;
	}

	/**
	 * Aggiunge un campo nel tracciato record.
	 *
	 * @param value valore del campo.
	 * @param filler classe che si occupa di scrivere nel tracciato.
	 */
	@NonNull
	public final <T> RecordFile add(final T value, final @NonNull Filler<T> filler) {
		this.check();
		filler.fill(value, this.strBuilder, this.getCurrentFieldLength());
		this.fieldIndex++;
		return this;
	}

	/**
	 * Aggiunge un campo di un solo carattere nel tracciato record.
	 *
	 * @param value carattere.
	 */
	@NonNull
	public final RecordFile addChar(final char value) {
		this.check();
		this.strBuilder.append(value);
		this.fieldIndex++;
		return this;
	}

	/**
	 * Aggiunge un campo di tipo stringa al tracciato record.
	 *
	 * @param value valore del campo.
	 */
	@NonNull
	public final RecordFile addString(final String value) {
		return this.add(value, this.defaultStringFiller);
	}

	/**
	 * Aggiunge un campo di tipo intero al tracciato record.
	 *
	 * @param value valore del campo.
	 */
	@NonNull
	public final RecordFile addInteger(final Integer value) {
		return this.add(value, this.defaultIntegerFiller);
	}

	/**
	 * Aggiunge un campo di tipo intero al tracciato record.
	 *
	 * @param value valore del campo.
	 */
	@NonNull
	public final RecordFile addInt(final int value) {
		return this.add(Integer.valueOf(value), this.defaultIntegerFiller);
	}

	/**
	 * Aggiunge un campo di tipo data al tracciato record.
	 *
	 * @param value valore del campo.
	 */
	@NonNull
	public final RecordFile addDate(final Date value) {
		return this.add(value, this.defaultDateFiller);
	}

	/**
	 * Salta il numero di campi specificato.
	 *
	 * @param count numero di campi da saltare.
	 * @throws IllegalArgumentException se si è giunti alla fine della riga corrente.
	 */
	@NonNull
	public final RecordFile skip(final int count) {
		for (int j = 0; j < count; j++) {
			this.skip();
		}
		return this;
	}

	/**
	 * Salta il campo corrente.
	 *
	 * @throws IllegalArgumentException se si è giunti alla fine della riga corrente.
	 */
	@NonNull
	public final RecordFile skip() {
		this.check();
		final char[] filler = new char[this.getCurrentFieldLength()];
		Arrays.fill(filler, this.emptyChar);
		this.strBuilder.append(filler);
		this.fieldIndex++;
		return this;
	}

	/**
	 * Salta tutti i campi fino a raggiungere la fine della riga corrente.
	 */
	@NonNull
	public final RecordFile skipToEnd() {
		if (this.rowIndex >= 0) {
			this.skipToEndInternal();
		}
		return this;
	}

	private void skipToEndInternal() {
		while (this.fieldIndex < this.currentRowFieldsLength.length - 1) {
			this.skip();
		}
	}

	/**
	 * Aggiunge una riga al tracciato specificando un array di lunghezze dei campi.
	 *
	 * @param fieldLengths array delle lunghezze dei campi della riga da aggiungere.
	 */
	@NonNull
	public final RecordFile nextRow(final @NonNull int[] fieldLengths) {
		Objects.requireNonNull(fieldLengths, "Length of the fiels are required");
		if (fieldLengths.length < 1) {
			throw new IllegalArgumentException("No fields defined");
		}
		if (this.rowIndex >= 0) {
			this.skipToEndInternal();
			this.strBuilder.append("\r\n");
		}
		this.currentRowFieldsLength = fieldLengths;
		this.rowIndex++;
		this.fieldIndex = 0;
		return this;
	}

	/**
	 * Svuota il tracciato record.
	 */
	public final void clear() {
		if (this.strBuilder.length() > 0) {
			this.strBuilder = new StringBuilder();
		}
	}

	/**
	 * Esporta il tracciato record.
	 *
	 * @param file file di destinazione.
	 * @throws RuntimeException se il tracciato è vuoto.
	 * @throws IOException se ci sono errori in scrittura del file.
	 */
	public final void export(final @NonNull File file) throws IOException {
		if (this.strBuilder.length() == 0) {
			throw new RuntimeException("No content to export");
		}
		try (FileWriter fw = new FileWriter(file); BufferedWriter bw = new BufferedWriter(fw)) {
			bw.append(this.strBuilder);
			bw.flush();
		}
	}

	/** Restituisce la lunghezza del campo corrente */
	private int getCurrentFieldLength() {
		return this.currentRowFieldsLength[this.fieldIndex];
	}

	/**
	 * Effettua dei controlli prima di scrivere un dato:
	 * <br>
	 * deve esserci almeno una riga e non si deve
	 * aver raggiunto la fine della riga corrente.
	 */
	private void check() {
		if (this.rowIndex < 0) {
			throw new IllegalArgumentException("Invalid row");
		}
		if (this.fieldIndex >= this.currentRowFieldsLength.length) {
			throw new IllegalArgumentException("Cannot add more fields: line is complete");
		}
	}

	/**
	 * Restituisce il contenuto del tracciato record.
	 */
	@Override
	@NonNull
	public String toString() {
		return this.strBuilder.toString();
	}

	/**
	 * Restituisce l'oggetto che si occupa di scrivere un campo di
	 * tipo stringa nel tracciato record.
	 */
	@NonNull
	public final Filler<String> getDefaultStringFiller() {
		return this.defaultStringFiller;
	}

	/**
	 * Imposta l'oggetto che si occupa di scrivere un campo di
	 * tipo stringa nel tracciato record.
	 */
	public final void setDefaultStringFiller(final @NonNull Filler<String> defaultStringFiller) {
		this.defaultStringFiller = Objects.requireNonNull(defaultStringFiller, "Filler required");
	}

	/**
	 * Restituisce l'oggetto che si occupa di scrivere un campo di
	 * tipo intero nel tracciato record.
	 */
	@NonNull
	public final Filler<Integer> getDefaultIntegerFiller() {
		return this.defaultIntegerFiller;
	}

	/**
	 * Imposta l'oggetto che si occupa di scrivere un campo di
	 * tipo intero nel tracciato record.
	 */
	public final void setDefaultIntegerFiller(final @NonNull Filler<Integer> defaultIntegerFiller) {
		this.defaultIntegerFiller = Objects.requireNonNull(defaultIntegerFiller, "Filler required");
	}

	/**
	 * Restituisce l'oggetto che si occupa di scrivere un campo di
	 * tipo data nel tracciato record.
	 */
	@NonNull
	public final Filler<Date> getDefaultDateFiller() {
		return this.defaultDateFiller;
	}

	/**
	 * Imposta l'oggetto che si occupa di scrivere un campo di
	 * tipo data nel tracciato record.
	 */
	public final void setDefaultDateFiller(final @NonNull Filler<Date> defaultDateFiller) {
		this.defaultDateFiller = Objects.requireNonNull(defaultDateFiller, "Filler required");
	}

	/**
	 * Interfaccia implementata dalle classi che si occupano di scrivere
	 * dati nel tracciato record.
	 *
	 * @param <T> tipo del dato da scrivere.
	 */
	public interface Filler<T> {

		/**
		 * Scrive il dato nel campo corrente del tracciato record.
		 *
		 * @param value dato da scrivere.
		 * @param target set di caratteri che compone il tracciato record sul quale scrivere.
		 * @param length lunghezza massima consentita nel campo corrente.
		 */
		void fill(T value, @NonNull StringBuilder target, int length);
	}

	/**
	 * Implementazione base del filler.
	 *
	 * @param <T> tipo di dato da scrivere.
	 */
	protected abstract static class AbstractFiller<T> implements Filler<T> {

		@Override
		public void fill(final T value, final @NonNull StringBuilder target, final int length) {
			final String nullSafeValue = this.convertToString(value);
			final int diff = length - nullSafeValue.length();
			if (diff < 0) {
				// stringa più lunga del consentito
				throw new IllegalArgumentException("Data length (" + nullSafeValue.length() + ") exceeds the limit (" + length + ")");
			} else if (diff == 0) {
				target.append(nullSafeValue);
			} else {
				final char fillerChar = this.getFiller();
				// stringa più corta della lunghezza del campo
				final char[] filler = new char[diff];
				Arrays.fill(filler, fillerChar);
				this.fillEmpty(nullSafeValue, filler, target);
			}
		}

		/**
		 * Converte il dato passato in stringa per poter essere scritto.
		 *
		 * @param value dato da scrivere.
		 */
		@NonNull
		protected abstract String convertToString(final T value);

		/**
		 * Riempie il campo se il dato passato è più corto.
		 *
		 * @param value dato da scrivere.
		 * @param filler array di caratteri usato per riempire gli spazi.
		 * @param target set di caratteri che compone il tracciato record sul quale scrivere.
		 */
		protected void fillEmpty(final String value, final @NonNull char[] filler, final @NonNull StringBuilder target) {
			target.append(value).append(filler);
		}

		/**
		 * Restituisce il carattere di riempimento per gli spazi.
		 */
		protected abstract char getFiller();
	}

	protected static class DefaultStringFiller extends AbstractFiller<String> {

		@Override
		@NonNull
		protected String convertToString(final String value) {
			return StringUtils.nullToEmpty(value);
		}

		@Override
		protected char getFiller() {
			return ' ';
		}
	}

	/**
	 * Filler di default per dati di tipo stringa.
	 */
	protected static class DefaultIntegerFiller extends AbstractFiller<Integer> {

		@Override
		@NonNull
		protected String convertToString(final Integer value) {
			return value == null ? "" : value.toString();
		}

		@Override
		protected char getFiller() {
			return '0';
		}

		@Override
		protected void fillEmpty(final String value, final @NonNull char[] filler, final @NonNull StringBuilder target) {
			// allinea a dx
			target.append(filler).append(value);
		}
	}

	/**
	 * Filler di default per dati di tipo data.
	 */
	protected static class DateFiller extends AbstractFiller<Date> {

		private final DateFormat df;

		protected DateFiller(final String format) {
			this.df = new SimpleDateFormat(format);
		}

		@Override
		@NonNull
		protected final String convertToString(final Date value) {
			return value == null ? "" : this.df.format(value);
		}

		@Override
		protected char getFiller() {
			return ' ';
		}
	}
}
