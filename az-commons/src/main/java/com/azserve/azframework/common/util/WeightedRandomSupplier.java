package com.azserve.azframework.common.util;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

/**
 * Funzione che restituisce un elemento a partire da
 * una lista in maniera casuale considerando il
 * peso degli elementi.
 *
 * @author filippo
 * @since 04/05/2015
 *
 * @param <T> tipo degli elementi.
 */
public class WeightedRandomSupplier<T> implements Supplier<T> {

	private final List<T> elements;
	private final ToIntFunction<T> weightFunction;
	private final int totalSum;

	/**
	 * Istanzia un nuovo supplier indicando gli elementi
	 * e la funzione che decide il peso di un elemento.
	 *
	 * @param elements lista degli elementi.
	 * @param weightFunction funzione che restituisce il peso
	 *            dato un elemento della lista.
	 */
	public WeightedRandomSupplier(final List<T> elements, final ToIntFunction<T> weightFunction) {
		this.elements = elements;
		this.weightFunction = weightFunction;
		this.totalSum = elements.stream().mapToInt(weightFunction).sum();
	}

	@Override
	public T get() {
		final int rnd = new Random().nextInt(this.totalSum) + 1;
		int cnt = 0;
		for (final T element : this.elements) {
			cnt += this.weightFunction.applyAsInt(element);
			if (cnt >= rnd) {
				return element;
			}
		}
		return (T) null;
	}
}
