package com.azserve.azframework.common.io;

import java.util.UUID;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;

/**
 * Enum che rappresenta estensioni di file.
 */
public enum FileExtension {

	OCTET_STREAM(null, "application/octet-stream"),
	UNKNOWN("", "application/unknown"),
	BMP("bmp", "image/bmp"),
	CSS("css", "text/css"),
	CSV("csv", "text/csv"),
	GIF("gif", "image/gif"),
	HTML("html", "text/html"),
	JPG("jpg", "image/jpeg"),
	PDF("pdf", "application/pdf"),
	PKCS_7("p7m", "application/pkcs7-mime"),
	PNG("png", "image/png"),
	RTF("rtf", "application/rtf"),
	TXT("txt", "text/plain"),
	XLS("xls", "application/vnd.ms-excel"),
	XML("xml", "application/xml"),
	XML_CDA2("xml", "text/x-cda-r2+xml"),
	ZIP("zip", "application/zip");

	/**
	 * Carattere di separazione tra il nome e l'estensione di un file.
	 */
	public static final char EXT_SEPARATOR = '.';

	private final String extension;
	private final String mimeType;

	FileExtension(final String extension, final String mimeType) {
		this.extension = extension;
		this.mimeType = mimeType;
	}

	/**
	 * Restituisce l'estensione senza carattere separatore.
	 */
	public String getExtension() {
		return this.extension;
	}

	/**
	 * Restituisce il tipo &quot;Multipart Internet Mail Extension&quot;.
	 */
	@NonNull
	public String getMimeType() {
		return this.mimeType;
	}

	/**
	 * Genera un nome file casuale con questo formato:<br>
	 * <i>{@literal <UUID casuale>.<estensione>}</i>
	 */
	@NonNull
	public String createRandomFilename() {
		return UUID.randomUUID().toString() + EXT_SEPARATOR + this.extension;
	}

	/**
	 * Restituisce la rappresentazione in stringa della costante:<br>
	 * l'estensione comprensiva del punto separatore.
	 *
	 * <pre>
	 * FileExtension.HTML.toString() = ".html"
	 * </pre>
	 */
	@Override
	@NonNull
	public String toString() {
		return EXT_SEPARATOR + this.extension;
	}

	/**
	 * Restituisce la costante corrispondente a partire dall'estensione.
	 *
	 * <pre>
	 * byExtension("html") = FileExtension.HTML
	 * byExtension(".PnG") = FileExtension.PNG
	 * </pre>
	 *
	 * @param ext estensione.
	 */
	@Nullable
	public static FileExtension byExtension(final String ext) {
		if (ext != null) {
			final String ext1 = (ext.startsWith(".") ? ext.substring(1, ext.length()) : ext).toLowerCase();
			for (final FileExtension extension : values()) {
				if (ext1.equals(extension.getExtension())) {
					return extension;
				}
			}
		}
		return null;
	}

	/**
	 * Restituisce la costante corrispondente a partire dal tipo mime.
	 *
	 * <pre>
	 * byExtension("text/html") = FileExtension.HTML
	 * </pre>
	 *
	 */
	@Nullable
	public static FileExtension byMimeType(final String mimeType) {
		if (mimeType != null) {
			for (final FileExtension extension : values()) {
				if (mimeType.equals(extension.getMimeType())) {
					return extension;
				}
			}
		}
		return null;
	}
}
