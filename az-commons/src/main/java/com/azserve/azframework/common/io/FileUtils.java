package com.azserve.azframework.common.io;

import static java.util.Objects.requireNonNull;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;

/**
 * Utilità per la lettura/scrittura su file.
 *
 * @author filippo
 */
public final class FileUtils {

	/**
	 * Nome del set di caratteri che si desidera utilizzare quando
	 * si vuole avere il controllo su di esso.
	 */
	public static final String CHARSET = "UTF-8";

	private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

	private FileUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Legge da un file fornendo una stringa con il
	 * relativo contenuto.
	 *
	 * @param file file dal quale leggere.
	 * @return il contenuto letto.
	 *
	 * @since 18/07/2013
	 */
	@NonNull
	public static String fileToString(final @NonNull File file) throws IOException {
		try (final FileReader fr = new FileReader(file)) {
			return IOUtils.toString(fr);
		}
	}

	/**
	 * Legge da un file fornendo una stringa con il
	 * relativo contenuto.
	 *
	 * @param path file dal quale leggere.
	 * @return il contenuto letto.
	 *
	 * @since 03/11/2014
	 */
	public static String fileToString(final @NonNull Path path) throws IOException {
		try (BufferedReader bufferedReader = Files.newBufferedReader(path, Charset.defaultCharset())) {
			return IOUtils.toString(bufferedReader);
		}
	}

	/**
	 * Restituisce un file sottoforma di array di byte.
	 *
	 * <p>&Egrave; possibile utilizzare anche {@link Files#readAllBytes(java.nio.file.Path)}.</p>
	 *
	 * @param file file sorgente.
	 */
	@NonNull
	public static byte[] fileToBytes(final @NonNull File file) throws IOException {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream(); FileInputStream fis = new FileInputStream(file)) {
			IOUtils.copy(fis, baos, 1024);
			return baos.toByteArray();
		}
	}

	/**
	 * Restituisce un file sottoforma di array di byte.
	 *
	 * <p>&Egrave; possibile utilizzare anche {@link Files#readAllBytes(java.nio.file.Path)}.</p>
	 *
	 * @param file file sorgente.
	 */
	public static @NonNull byte[] fileToBytes(final @NonNull Path file) throws IOException {
		requireNonNull(file, "source file required");
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream(); InputStream is = Files.newInputStream(file)) {
			IOUtils.copy(is, baos, 1024);
			return baos.toByteArray();
		}
	}

	/**
	 * Scrive il flusso specificato su un file.
	 *
	 * @param source il flusso di dati (obbligatorio).
	 * @param file il file di destinazione (obbligatorio).
	 *
	 * @return il numero di byte copiati.
	 *
	 * @since 15/07/2016
	 */
	public static long inputToFile(final @NonNull InputStream source, final @NonNull File file) throws IOException {
		requireNonNull(source, "source stream required");
		requireNonNull(file, "destination file required");
		try (FileOutputStream fos = new FileOutputStream(file)) {
			return IOUtils.copy(source, fos, 1024);
		}
	}

	/**
	 * Scrive il flusso specificato su un file.
	 *
	 * @param source il flusso di dati (obbligatorio).
	 * @param file il file di destinazione (obbligatorio).
	 *
	 * @return il numero di byte copiati.
	 *
	 * @since 15/07/2016
	 */
	public static long inputToFile(final @NonNull InputStream source, final @NonNull Path file) throws IOException {
		requireNonNull(source, "source stream required");
		requireNonNull(file, "destination file required");
		try (OutputStream os = Files.newOutputStream(file)) {
			final long length = IOUtils.copy(source, os, 1024);
			os.flush();
			return length;
		}
	}

	/**
	 * Scrive il contenuto di un array di byte su un file.
	 *
	 * @param source il contenuto (obbligatorio).
	 * @param file il file di destinazione (obbligatorio).
	 *
	 * @return il numero di byte copiati.
	 *
	 * @since 17/09/2013
	 */
	public static long bytesToFile(final @NonNull byte[] source, final @NonNull File file) throws IOException {
		requireNonNull(source, "source content required");
		requireNonNull(file, "destination file required");
		try (FileOutputStream fos = new FileOutputStream(file); ByteArrayInputStream bais = new ByteArrayInputStream(source)) {
			return IOUtils.copy(bais, fos, 1024);
		}
	}

	/**
	 * Scrive il contenuto di un array di byte su un file.
	 *
	 * @param source il contenuto (obbligatorio).
	 * @param file il file di destinazione (obbligatorio).
	 *
	 * @return il numero di byte copiati.
	 *
	 * @since 23/06/2014
	 */
	public static long bytesToFile(final byte[] source, final Path file) throws IOException {
		requireNonNull(source, "source content required");
		requireNonNull(file, "destination file required");
		try (OutputStream os = Files.newOutputStream(file); ByteArrayInputStream bais = new ByteArrayInputStream(source)) {
			final long length = IOUtils.copy(bais, os, 1024);
			os.flush();
			return length;
		}
	}

	/**
	 * Scrive il contenuto di una stringa su un file.
	 *
	 * @param source la stringa da salvare (obbligatoria).
	 * @param file il file di destinazione (obbligatorio).
	 *
	 * @return il numero di byte copiati.
	 *
	 * @since 17/09/2013
	 */
	public static long stringToFile(final @NonNull String source, final @NonNull File file) throws IOException {
		return bytesToFile(requireNonNull(source, "source string required").getBytes(), file);
	}

	/**
	 * Scrive il contenuto di una stringa su un file.
	 *
	 * @param source la stringa da salvare (obbligatoria).
	 * @param file il file di destinazione (obbligatorio).
	 *
	 * @return il numero di byte copiati.
	 *
	 * @since 23/06/2014
	 */
	public static long stringToFile(final @NonNull String source, final @NonNull Path file) throws IOException {
		return bytesToFile(requireNonNull(source, "source string required").getBytes(), file);
	}

	/**
	 * Legge un oggetto serializzato da file.
	 *
	 * @param file file sorgente.
	 *
	 * @since 26/09/2013
	 */
	public static @NonNull <T extends Serializable> T fileToObject(final @NonNull File file) throws IOException, ClassNotFoundException {
		final byte[] fileToBytes = fileToBytes(requireNonNull(file, "source file required"));
		return SerializeUtils.deserialize(fileToBytes);
	}

	/**
	 * Salva un oggetto su file serializzandolo.
	 *
	 * @param object l'oggetto (obbligatorio).
	 * @param file il file di destinazione (obbligatorio).
	 *
	 * @return il numero di byte copiati.
	 *
	 * @since 26/09/2013
	 */
	public static <T extends Serializable> long objectToFile(final @NonNull T object, final @NonNull File file) throws IOException {
		requireNonNull(object, "source object required");
		requireNonNull(file, "destination file required");
		final byte[] objectToBytes = SerializeUtils.serialize(object);
		return bytesToFile(objectToBytes, file);
	}

	/**
	 * Copia un file.
	 * Se il file di destinazione esiste già,
	 * verrà sovrascritto.
	 *
	 * <p>
	 * &Egrave; possibile utilizzare anche il
	 * metodo {@link Files#copy(java.nio.file.Path source, java.io.OutputStream out)}.
	 * </p>
	 *
	 * @param source file da copiare.
	 * @param destination file di destinazione.
	 * @return numero di byte copiati.
	 *
	 * @since 18/10/2013
	 */
	public static long copy(final @NonNull File source, final @NonNull File destination) throws IOException {
		try (FileInputStream fis = new FileInputStream(requireNonNull(source, "source file required"));
				FileOutputStream fos = new FileOutputStream(requireNonNull(destination, "destination file required"))) {
			return IOUtils.copy(fis, fos);
		}
	}

	/**
	 * Conta il numero di righe di un file.
	 *
	 * @param source file da analizzare.
	 * @return il numero di righe.
	 */
	public static long countLines(final @NonNull File source) throws IOException {
		requireNonNull(source, "source file required");
		long count = 0;
		try (FileReader fr = new FileReader(source); BufferedReader br = new BufferedReader(fr)) {
			while (br.readLine() != null) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Restituisce il file aperto.
	 *
	 * @param is stream.
	 */
	public static File getFile(final @Nullable InputStream is) {
		if (is != null) {
			if (is instanceof FileInputStream) {
				final FileInputStream fis = (FileInputStream) is;
				try {
					final Field pathField = FileInputStream.class.getDeclaredField("path");
					pathField.setAccessible(true);
					final Object path = pathField.get(fis);
					if (path != null) {
						return new File((String) path);
					}
				} catch (final Exception ex) {
					LOGGER.warn("getFile(InputStream)", ex);
				}
			}
		}
		return null;
	}

	/**
	 * Restituisce il nome del file specificato senza estensione.
	 *
	 * @param fileName il nome del file.
	 */
	public static @NonNull String removeExtension(final @NonNull String fileName) {
		final int lastDot = fileName.lastIndexOf('.');
		if (lastDot > 0) {
			return fileName.substring(0, lastDot);
		}
		return fileName;
	}

	/**
	 * Restituisce l'estensione di un oggetto {@code Path}.
	 * Se il nome non ha estensione restituisce {@code null}.
	 *
	 * @param path l'oggetto che rappresenta un percorso di file.
	 * @return l'estensione del file.
	 */
	public static String getExtension(final @NonNull Path path) {
		final String pathname = path.getFileName().toString();
		final int index = pathname.lastIndexOf('.');
		if (index == -1) {
			return null;
		}
		return pathname.substring(index + 1);
	}

	/**
	 * Restituisce l'estensione a partire dal nome
	 * di un file.
	 * Se il nome non ha estensione restituisce {@code null}.
	 *
	 * @param fileName nome del file.
	 * @return l'estensione del file.
	 */
	public static String getExtension(final @NonNull String fileName) {
		final int index = new File(fileName).getName().lastIndexOf('.');
		if (index == -1) {
			return null;
		}
		return fileName.substring(index + 1);
	}

	/**
	 * Restituisce la directory temporanea a partire
	 * dalla proprietà di sistema {@code java.io.tmpdir}.
	 */
	public static Path getTempDir() {
		return Paths.get(System.getProperty("java.io.tmpdir"));
	}

	/**
	 * Restituisce la directory corrente a partire dalla proprietà
	 * di sistema {@code user.dir}.
	 */
	public static Path getUserDir() {
		return Paths.get(System.getProperty("user.dir"));
	}

	/**
	 * Restituisce la directory home dell'utente corrente
	 * a partire dalla proprietà di sistema {@code user.home}.
	 */
	public static Path getUserHome() {
		return Paths.get(System.getProperty("user.home"));
	}

	/**
	 * Restituisce una lista di {@code Path} rappresentanti
	 * i file contenuti in un {@code Path} specificato
	 * (che dev'essere una cartella).
	 *
	 * @param rootPath directory da esplorare.
	 * @return la lista dei file contenuti.
	 * @throws IOException se {@code rootPath} non
	 *             rappresenta una cartella o per un
	 *             errore di I/O.
	 */
	public static List<Path> listPaths(final @NonNull Path rootPath) throws IOException {
		return listPaths(requireNonNull(rootPath, "root path required"), path -> true);
	}

	/**
	 * Restituisce una lista di {@code Path} rappresentanti
	 * i file contenuti in un {@code Path} specificato
	 * (che dev'essere una cartella).
	 *
	 * @param rootPath directory da esplorare.
	 * @param filter filtro sui file contenuti.
	 * @return la lista dei file contenuti.
	 * @throws IOException se {@code rootPath} non
	 *             rappresenta una cartella o per un
	 *             errore di I/O.
	 */
	public static List<Path> listPaths(final @NonNull Path rootPath, final @NonNull Filter<Path> filter) throws IOException {
		requireNonNull(rootPath, "root path required");
		requireNonNull(filter, "filter required");
		// JAVA8 : Files.walk(rootPath,1).filter(filter).collect(Collectors.toCollection(ArrayList::new))
		final List<Path> paths = new ArrayList<>();
		try (DirectoryStream<Path> ds = Files.newDirectoryStream(rootPath, filter)) {
			for (final Path path : ds) {
				paths.add(path);
			}
			return paths;
		}
	}

	public static long totalSize(final @NonNull Path path) throws IOException {
		requireNonNull(path, "root path required");
		final AtomicLong size = new AtomicLong(0);
		Files.walkFileTree(path, new FileVisitor<Path>() {

			@Override
			public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
				size.addAndGet(Files.size(file));
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(final Path file, final IOException exc) throws IOException {
				throw exc;
			}

			@Override
			public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}
		});
		return size.longValue();
	}

	public static void recursiveDelete(final Path path) throws IOException {
		if (!Files.exists(requireNonNull(path, "path required"))) {
			return;
		}
		Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) throws IOException {
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}
}
