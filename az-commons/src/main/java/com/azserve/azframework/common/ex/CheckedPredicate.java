package com.azserve.azframework.common.ex;

import static java.util.Objects.requireNonNull;

import java.util.function.Predicate;

@FunctionalInterface
public interface CheckedPredicate<T, E extends Exception> {

	boolean test(T t) throws E;

	/**
	 * Unwraps this checked predicate to an unchecked one.
	 */
	default Predicate<T> unchecked() {
		return t -> {
			try {
				return this.test(t);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return false;
			}
		};
	}

	/**
	 * Provides a way to handle the exception and to return an alternative value.
	 *
	 * <pre>
	 * CheckedPredicate.of(this::throwException).onError(ex -> false);
	 * </pre>
	 *
	 * @param predicate the exception handler
	 */
	default Predicate<T> onError(final Predicate<Exception> predicate) {
		return t -> {
			try {
				return this.test(t);
			} catch (final Exception ex) {
				return predicate.test(ex);
			}
		};
	}

	default CheckedPredicate<T, E> negate() {
		return t -> !this.test(t);
	}

	/**
	 * Creates a {@link CheckedPredicate} by using a method reference.
	 *
	 * @param predicate the predicate as method reference
	 * @return the predicate
	 */
	static <T, E extends Exception> CheckedPredicate<T, E> of(final CheckedPredicate<T, E> predicate) {
		return requireNonNull(predicate);
	}

	/**
	 * Wraps an unchecked predicate in an unchecked one.
	 *
	 * @param predicate the unchecked predicate
	 */
	static <T, E extends Exception> CheckedPredicate<T, E> wrap(final Predicate<T> predicate) {
		return requireNonNull(predicate)::test;
	}

	/**
	 * Unwraps a checked predicate to have an unchecked one.
	 * The exception is then propagated to this method caller.
	 *
	 * @throws E the propagated exception
	 */
	static <T, E extends Exception> Predicate<T> unwrap(final CheckedPredicate<T, E> predicate) throws E {
		requireNonNull(predicate);
		return t -> {
			try {
				return predicate.test(t);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return false;
			}
		};
	}

	/**
	 * Unwraps a checked predicate to have an unchecked one.
	 */
	static <T, E extends Exception> Predicate<T> unchecked(final CheckedPredicate<T, E> predicate) {
		return t -> {
			try {
				return predicate.test(t);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return false;
			}
		};
	}
}
