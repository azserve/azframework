package com.azserve.azframework.common.awt;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Utilità per la gestione di immagini.
 * 
 * @author filippo
 * @since 08/08/2013
 */
public final class ImageUtils {

	private ImageUtils() {}

	/**
	 * Crea un immagine a partire da un array di byte.<br>
	 * Se l'array è {@code null} o vuoto restituisce {@code null}.
	 * 
	 * @since 18/03/2013
	 */
	@NonNull
	public static BufferedImage imageFromBytes(final @NonNull byte[] b) throws Exception {
		try (final ByteArrayInputStream bais = new ByteArrayInputStream(b)) {
			return ImageIO.read(bais);
		}
	}

	/**
	 * Restituisce un immagine sotto forma di array di byte.
	 * 
	 * @param image immagine sorgente.
	 * @since 18/03/2013
	 */
	@NonNull
	public static byte[] imageToBytes(final @NonNull RenderedImage image) throws IOException {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			ImageIO.write(image, "jpg", baos);
			baos.flush();
			return baos.toByteArray();
		}
	}

	/**
	 * Crea un oggetto {@linkplain BufferedImage} a partire da un immagine.
	 * 
	 * @param image immagine sorgente.
	 * @param imageType tipo di immagine.
	 */
	@NonNull
	public static BufferedImage convertBufferedImage(final @NonNull Image image, final int imageType) {
		final BufferedImage bi = new BufferedImage(image.getWidth(null), image.getHeight(null), imageType);
		final Graphics2D g2d = bi.createGraphics();
		g2d.drawImage(image, 0, 0, null);
		g2d.dispose();
		return bi;
	}
}
