/**
 * <p>
 * Fornisce varie classi di utilità per le operazioni più comuni su
 * oggetti del package {@code java.util} ed altre utilità.
 * </p>
 */
package com.azserve.azframework.common.util;