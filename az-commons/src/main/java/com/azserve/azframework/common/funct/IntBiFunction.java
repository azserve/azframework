package com.azserve.azframework.common.funct;

@FunctionalInterface
public interface IntBiFunction<R> {

	R apply(int value1, int value2);

}
