package com.azserve.azframework.common.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;

/**
 * Utilità per la gestione di flussi di dati e operazioni di I/O.
 *
 * @author filippo
 * @since 14/03/2013
 */
public final class IOUtils {

	private static final int DEFAULT_BUFFER_LENGTH = 1024 * 4;

	private IOUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Legge da un {@code InputStream} e restituisce il contenuto
	 * sotto forma di array di {@code byte}.
	 *
	 * @since 14/02/2017
	 */
	public static @NonNull byte[] toBytes(final @NonNull InputStream is) throws IOException {
		try (ByteArrayOutputStream baos = new NonSyncByteArrayOutputStream()) {
			copy(is, baos);
			return baos.toByteArray();
		}
	}

	/**
	 * Legge da un oggetto {@code Reader} fornendo una stringa con il
	 * relativo contenuto.
	 *
	 * @param reader sorgente.
	 * @return il contenuto letto.
	 */
	public static @NonNull String toString(final @NonNull Reader reader) throws IOException {
		try (final BufferedReader bufferedReader = new BufferedReader(reader)) {
			return toString(bufferedReader);
		}
	}

	/**
	 * Legge da un oggetto {@code BufferedReader} fornendo una stringa con il
	 * relativo contenuto.
	 *
	 * @param bufferedReader sorgente.
	 * @return il contenuto letto.
	 */
	public static @NonNull String toString(final @NonNull BufferedReader bufferedReader) throws IOException {
		final StringBuilder sb = new StringBuilder();

		final char[] buf = new char[DEFAULT_BUFFER_LENGTH];
		int i = 0;
		while ((i = bufferedReader.read(buf)) > 0) {
			sb.append(new String(buf, 0, i));
		}
		return sb.toString();
	}

	/**
	 * Legge da un {@code InputStream} e restituisce il contenuto
	 * sotto forma di stringa sfruttando un buffer di
	 * dimensioni specificate.
	 *
	 * @since 27/03/2013
	 */
	public static @NonNull String toString(final @NonNull InputStream is, final int bufferSize) throws IOException {
		final StringWriter sw = new StringWriter();
		final char[] buffer = new char[bufferSize];
		try (final BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
			int i;
			while ((i = reader.read(buffer)) != -1) {
				sw.write(buffer, 0, i);
			}
		}
		return sw.toString();
	}

	/**
	 * Legge da un {@code InputStream} e restituisce il contenuto
	 * sotto forma di stringa.
	 *
	 * @since 14/03/2013
	 */
	public static @NonNull String toString(final @NonNull InputStream is) throws IOException {
		return toString(is, DEFAULT_BUFFER_LENGTH);
	}

	/**
	 * Restituisce un {@code InputStream} a partire da una stringa.
	 *
	 * @param source la stringa (obbligatoria).
	 */
	public static @NonNull InputStream toInputStream(final @NonNull String source) {
		return new ByteArrayInputStream(source.getBytes());
	}

	/**
	 * Legge i byte da un {@code InputStream} e li copia
	 * su un {@code OutputStream} utilizzando un buffer
	 * di dimensioni specificate.
	 *
	 * @param is sorgente di dati.
	 * @param os destinazione dei dati.
	 * @param bufferSize dimensione del buffer.
	 * @return il numero di byte copiati.
	 * @throws IOException se ci sono errori in lettura/scrittura
	 *             oppure almeno uno dei due flussi è chiuso.
	 * @since 27/05/2013
	 */
	public static long copy(final @NonNull InputStream is, final @NonNull OutputStream os, final int bufferSize) throws IOException {
		return copy(is, os, bufferSize, -1);
	}

	/**
	 * Legge i byte da un {@code InputStream} e li copia
	 * su un {@code OutputStream} utilizzando un buffer
	 * di dimensioni specificate.
	 *
	 * @param is sorgente di dati.
	 * @param os destinazione dei dati.
	 * @param bufferSize dimensione del buffer.
	 * @param sizeLimit il limite massimo da copiare.
	 *            Un numero negativo indica nessun limite.
	 * @return il numero di byte copiati.
	 * @throws IOException se ci sono errori in lettura/scrittura,
	 *             se si supera il limite imposto da {@code sizeLimit}
	 *             oppure almeno uno dei due flussi è chiuso.
	 * @since 27/05/2013
	 */
	public static long copy(final @NonNull InputStream is, final @NonNull OutputStream os, final int bufferSize, final long sizeLimit) throws IOException {
		long bytesRead = 0L;
		final byte[] buf = new byte[bufferSize];
		int n;
		while ((n = is.read(buf)) > 0) {
			os.write(buf, 0, n);
			bytesRead += n;
			if (sizeLimit >= 0 && bytesRead > sizeLimit) {
				throw new IOException("Size limit excedeed");
			}
		}
		return bytesRead;
	}

	/**
	 * Legge i byte da un {@code InputStream} e li copia
	 * su un {@code OutputStream}.
	 *
	 * @param is sorgente di dati.
	 * @param os destinazione dei dati.
	 * @return il numero di byte copiati.
	 * @throws IOException se ci sono errori in lettura/scrittura
	 *             oppure almeno uno dei due flussi è chiuso.
	 * @since 18/10/2013
	 */
	public static long copy(final @NonNull InputStream is, final @NonNull OutputStream os) throws IOException {
		return copy(is, os, DEFAULT_BUFFER_LENGTH);
	}

	/**
	 * Prova a determinare il tipo di contenuto da un array di byte.
	 *
	 * @param bArray contenuto da analizzare.
	 * @return la costante {@code FileExtension} corrispondente
	 *         al tipo di contenuto, se trovata.
	 * @throws IOException in caso di errore in lettura del flusso.
	 *
	 * @see URLConnection#guessContentTypeFromStream(InputStream)
	 */
	@Nullable
	public static FileExtension guessContentType(final @NonNull byte[] bArray) throws IOException {
		FileExtension guessed;
		try (ByteArrayInputStream bais = new NonSyncByteArrayInputStream(bArray)) {
			guessed = guessContentType(bais);
		}
		if (guessed == null && isPdfContent(bArray)) {
			// tratto a parte il PDF
			return FileExtension.PDF;
		}
		return guessed;
	}

	/**
	 * Prova a determinare il tipo di contenuto da un input stream.
	 *
	 * @param is flusso da esaminare.
	 * @return la costante {@code FileExtension} corrispondente
	 *         al tipo di contenuto, se trovata.
	 * @throws IOException in caso di errore in lettura del flusso.
	 *
	 * @see URLConnection#guessContentTypeFromStream(InputStream)
	 */
	@Nullable
	public static FileExtension guessContentType(final @NonNull InputStream is) throws IOException {
		final String contentType = URLConnection.guessContentTypeFromStream(is);
		return FileExtension.byMimeType(contentType);
	}

	private static boolean isPdfContent(final byte[] data) {

		// http://stackoverflow.com/questions/941813/how-can-i-determine-if-a-file-is-a-pdf-file/2228540#2228540

		if (data != null && data.length > 4 &&
				data[0] == 0x25 && // %
				data[1] == 0x50 && // P
				data[2] == 0x44 && // D
				data[3] == 0x46 && // F
				data[4] == 0x2D) { // -

			// file terminator
			if (data[5] == 0x31 && // 1
					data[6] == 0x2E && // .
					// (data[7] == 0x33 || data[7] == 0x34) && // 3 || 4
					data[data.length - 6] == 0x25 && // %
					data[data.length - 5] == 0x25 && // %
					data[data.length - 4] == 0x45 && // E
					data[data.length - 3] == 0x4F && // O
					data[data.length - 2] == 0x46 && // F
					data[data.length - 1] == 0x0A) { // EOL
				return true;
			}
		}
		return false;
	}

	/**
	 * Recupera il contenuto dall'url specificato.
	 *
	 * @param url sorgente.
	 * @return il contenuto come array di byte.
	 */
	public static @NonNull byte[] downloadFromWeb(final @NonNull String url) throws IOException {
		return downloadFromWeb(url, -1);
	}

	/**
	 * Recupera il contenuto dall'url specificato.
	 *
	 * @param url sorgente.
	 * @param sizeLimit la dimensione massima del contenuto ammessa.
	 *            Un numero negativo indica nessun limite di dimensione.
	 * @return il contenuto come array di byte.
	 * @see #copy(InputStream, OutputStream, int, long)
	 */
	public static @NonNull byte[] downloadFromWeb(final @NonNull String url, final long sizeLimit) throws IOException {
		final URL u = new URL(url);
		try (final InputStream input = u.openStream(); final ByteArrayOutputStream output = new ByteArrayOutputStream()) {
			copy(input, output, 16 * 1024, sizeLimit);
			return output.toByteArray();
		}
	}

	/**
	 * Classe {@code ByteArrayInputStream} non sincronizzata.
	 */
	@SuppressWarnings("sync-override")
	public static class NonSyncByteArrayInputStream extends ByteArrayInputStream {

		public NonSyncByteArrayInputStream(final byte[] buf) {
			super(buf);
		}

		public NonSyncByteArrayInputStream(final byte[] buf, final int offset, final int length) {
			super(buf, offset, length);
		}

		@Override
		public int read() {
			return this.pos < this.count ? this.buf[this.pos++] & 0xff : -1;
		}

		@Override
		public int read(final byte[] b, final int off, final int len) {
			if (b == null) {
				throw new NullPointerException();
			} else if (off < 0 || len < 0 || len > b.length - off) {
				throw new IndexOutOfBoundsException();
			}

			if (this.pos >= this.count) {
				return -1;
			}
			int length = len;
			final int avail = this.count - this.pos;
			if (length > avail) {
				length = avail;
			}
			if (length <= 0) {
				return 0;
			}
			System.arraycopy(this.buf, this.pos, b, off, length);
			this.pos += length;
			return length;
		}

		@Override
		public long skip(final long n) {
			long k = this.count - this.pos;
			if (n < k) {
				k = n < 0 ? 0 : n;
			}

			this.pos += k;
			return k;
		}

		@Override
		public void mark(final int readAheadLimit) {
			this.mark = this.pos;
		}

		@Override
		public void reset() {
			this.pos = this.mark;
		}
	}

	/**
	 * Classe {@code ByteArrayOutputStream} non sincronizzata.
	 */
	@SuppressWarnings("sync-override")
	public static class NonSyncByteArrayOutputStream extends ByteArrayOutputStream {

		public NonSyncByteArrayOutputStream() {
			super(32);
		}

		public NonSyncByteArrayOutputStream(final int size) {
			super(size);
		}

		private void ensureCapacity(final int minCapacity) {
			if (minCapacity - this.buf.length > 0) {
				this.grow(minCapacity);
			}
		}

		private void grow(final int minCapacity) {
			final int oldCapacity = this.buf.length;
			int newCapacity = oldCapacity << 1;
			if (newCapacity - minCapacity < 0) {
				newCapacity = minCapacity;
			}
			if (newCapacity < 0) {
				if (minCapacity < 0) {
					throw new OutOfMemoryError();
				}
				newCapacity = Integer.MAX_VALUE;
			}
			this.buf = Arrays.copyOf(this.buf, newCapacity);
		}

		@Override
		public void write(final int b) {
			this.ensureCapacity(this.count + 1);
			this.buf[this.count] = (byte) b;
			this.count += 1;
		}

		@Override
		public void write(final byte[] b, final int off, final int len) {
			if (off < 0 || off > b.length || len < 0 || off + len - b.length > 0) {
				throw new IndexOutOfBoundsException();
			}
			this.ensureCapacity(this.count + len);
			System.arraycopy(b, off, this.buf, this.count, len);
			this.count += len;
		}

		@Override
		public void writeTo(final OutputStream out) throws IOException {
			out.write(this.buf, 0, this.count);
		}

		@Override
		public void reset() {
			this.count = 0;
		}

		@Override
		public byte[] toByteArray() {
			return Arrays.copyOf(this.buf, this.count);
		}

		@Override
		public int size() {
			return this.count;
		}

		@Override
		public String toString() {
			return new String(this.buf, 0, this.count);
		}

		@Override
		public String toString(final String charsetName) throws UnsupportedEncodingException {
			return new String(this.buf, 0, this.count, charsetName);
		}

		@Override
		@Deprecated
		public String toString(final int hibyte) {
			return new String(this.buf, hibyte, 0, this.count);
		}
	}
}
