package com.azserve.azframework.common.web;

import java.awt.Color;
import java.util.Base64;
import java.util.Collection;
import java.util.List;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.lang.StringUtils;

/**
 * Classe di utilità per la scrittura di HTML.
 *
 * @author filippo
 * @since 08/08/2013
 */
public final class Html {

	private static final String[] HTML_CODES = new String[256];

	static {
		for (int i = 0; i < 10; i++) {
			HTML_CODES[i] = "&#00" + i + ";";
		}
		for (int i = 10; i < 32; i++) {
			HTML_CODES[i] = "&#0" + i + ";";
		}
		for (int i = 32; i < 128; i++) {
			HTML_CODES[i] = String.valueOf((char) i);
		}
		HTML_CODES['\t'] = "\t";
		HTML_CODES['\n'] = "<br />\n";
		HTML_CODES['\"'] = "&quot;";
		HTML_CODES['&'] = "&amp;";
		HTML_CODES['<'] = "&lt;";
		HTML_CODES['>'] = "&gt;";

		for (int i = 128; i < 256; i++) {
			HTML_CODES[i] = "&#" + i + ";";
		}
	}

	public enum OlTagType {

		NUMERIC('1'),
		ALPHABETIC_UCASE('A'),
		ALPHABETIC_LCASE('a'),
		ROMAN_UCASE('I'),
		ROMAN_LCASE('i');

		private final char code;

		OlTagType(final char code) {
			this.code = code;
		}

		@Override
		public String toString() {
			return String.valueOf(this.code);
		}
	}

	private Html() {}

	/**
	 * Converte una stringa nel formato Html.
	 *
	 * <p>
	 * Filippo: sorgenti presi da {@code
	 * com.lowagie.text.html.HtmlEncoder
	 * } in modo da non dover importare librerie
	 * con classi inutili.
	 * </p>
	 *
	 * @param string stringa da analizzare.
	 * @return la stringa convertita in Html.
	 */
	@NonNull
	public static String encode(final @NonNull String string) {
		final StringBuilder result = new StringBuilder();
		// ciclo i caratteri di una stringa
		for (final char character : string.toCharArray()) {
			if (character < 256) {
				result.append(HTML_CODES[character]);
			} else {
				result.append("&#").append((int) character).append(";");
			}
		}
		return result.toString().trim();
	}

	/**
	 * Inserisce il contenuto specificato tra i due tag
	 * del grasseto {@literal <b></b>}.
	 * <br/>
	 * Un contenuto {@code null} viene riportato con la
	 * stringa "null".
	 *
	 * @param content testo o html da inserire tra i tag.
	 */
	@NonNull
	public static String b(final String content) {
		return "<b>" + content + "</b>";
	}

	/**
	 * Inserisce il contenuto specificato tra i due tag
	 * del corsivo {@literal <i></i>}.
	 * <br/>
	 * Un contenuto {@code null} viene riportato con la
	 * stringa "null".
	 *
	 * @param content testo o html da inserire tra i tag.
	 */
	@NonNull
	public static String i(final String content) {
		return "<i>" + content + "</i>";
	}

	/**
	 * Inserisce il contenuto specificato tra i due tag
	 * del testo sottolineato {@literal <u></u>}.
	 * <br/>
	 * Un contenuto {@code null} viene riportato con la
	 * stringa "null".
	 *
	 * @param content testo o html da inserire tra i tag.
	 */
	@NonNull
	public static String u(final String content) {
		return "<u>" + content + "</u>";
	}

	/**
	 * Inserisce il contenuto specificato nel
	 * tag {@literal <a></a>} specificando
	 * l'url dell'attributo href.
	 * <br/>
	 * Contenuto o url {@code null} vengono riportati
	 * con la stringa "null".
	 *
	 * <pre>
	 * a("AZserve", "http://azserve.com")
	 *
	 * {@literal <a href="http://azserve.com">AZserve</a>}"
	 * </pre>
	 *
	 * @param content testo o html da inserire tra i tag.
	 * @param url url dell'attributo href.
	 */
	@NonNull
	public static String a(final @NonNull String content, final @NonNull String url, final String target, final String styles) {
		final StringBuilder sb = new StringBuilder();
		sb.append("<a href=\"");
		sb.append(url);
		sb.append("\"");
		if (target != null) {
			sb.append(" target=\"").append(target).append("\"");
		}
		if (styles != null) {
			sb.append(" style=\"").append(styles).append("\"");
		}
		sb.append("\">").append(content).append("</a>");
		return sb.toString();
	}

	/**
	 * Inserisce il contenuto specificato tra i due tag
	 * del testo sottolineato {@literal <li></li>}.
	 * <br/>
	 * Un contenuto {@code null} viene riportato con la
	 * stringa "null".
	 *
	 * @param content testo o html da inserire tra i tag.
	 */
	@NonNull
	public static String li(final String content) {
		return "<li>" + content + "</li>";
	}

	/**
	 * Crea un elenco numerato a partire da una lista
	 * di stringhe.
	 * <br/>
	 * Stringhe {@code null} vengono sostituite con la
	 * stringa vuota.
	 *
	 * @param details voci dell'elenco (obbligatorio).
	 * @param start numero da cui partire.
	 * @param type tipo di marcatore dell'elenco.
	 */
	@NonNull
	public static String ol(final @NonNull List<?> details, final Integer start, final OlTagType type) {
		final StringBuilder message = new StringBuilder();
		message.append("<ol");
		if (start != null) {
			message.append(" start=").append(start.intValue());
		}
		if (type != null) {
			message.append(" type=\"").append(type).append("\"");
		}
		message.append(">");
		for (final Object detail : details) {
			message.append("<li>").append(detail == null ? "" : detail.toString().replace("<", "&lt;").replace(">", "&gt")).append("</li>");
		}
		message.append("</ol>");
		return message.toString();
	}

	/**
	 * Crea un elenco puntato a partire da una collezione
	 * di stringhe.
	 * <br/>
	 * Stringhe {@code null} vengono sostituite con la
	 * stringa vuota.
	 *
	 * @param details voci dell'elenco.
	 */
	@NonNull
	public static String ul(final @NonNull Collection<?> details) {
		final StringBuilder message = new StringBuilder();
		message.append("<ul>");
		for (final Object detail : details) {
			message.append("<li>").append(detail == null ? "" : detail.toString().replace("<", "&lt;").replace(">", "&gt")).append("</li>");
		}
		message.append("</ul>");
		return message.toString();
	}

	/**
	 * Scrive il colore nel formato rgb.
	 *
	 * <pre>
	 * rgb(Color.GREEN) = "rgb(0,255,0)"
	 * </pre>
	 *
	 * @param color colore da utilizzare.
	 */
	@NonNull
	public static String rgb(final @NonNull Color color) {
		final int r = color.getRed();
		final int g = color.getGreen();
		final int b = color.getBlue();
		return "rgb(" + r + "," + g + "," + b + ")";
	}

	/**
	 * Inserisce un immagine (byte array) nel
	 * tag {@literal <img>} specificando il
	 * testo con l'attributo alt e la dimensione.
	 *
	 * @param imageBytes array di byte dell'immagine da inserire.
	 * @param altText testo dell'immagine.
	 * @param width larghezza dell'immagine.
	 * @param height altezza dell'immagine.
	 */
	@NonNull
	public static String img(final @NonNull byte[] imageBytes, final String altText, final int width, final int height) {
		return "<img alt=\"" + encode(altText) + "\" src=\"" + dataUrl(imageBytes) + "\" width=\"" + width + "\" height=\"" + height + "\"/>";
	}

	/**
	 * Fornisce la stringa di un URL di tipo "data".
	 *
	 * @param bytes dati.
	 *
	 * @see <a href="http://www.faqs.org/rfcs/rfc2397.html">RFC 2397</a>
	 */
	@NonNull
	public static String dataUrl(final @NonNull byte[] bytes) {
		return dataUrl(bytes, null);
	}

	/**
	 * Fornisce la stringa di un URL di tipo "data".
	 *
	 * @param bytes dati.
	 * @param mediatype tipo di dati.
	 *
	 * @see <a href="http://www.faqs.org/rfcs/rfc2397.html">RFC 2397</a>
	 */
	@NonNull
	public static String dataUrl(final @NonNull byte[] bytes, final String mediatype) {
		return "data:" + StringUtils.nullToEmpty(mediatype) + ";base64," + Base64.getEncoder().encodeToString(bytes);
	}
}
