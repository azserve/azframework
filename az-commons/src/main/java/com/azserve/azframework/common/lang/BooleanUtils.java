package com.azserve.azframework.common.lang;

import static java.util.Objects.requireNonNull;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Classe di utilità per la gestione di oggetti {@link Boolean}.
 *
 * @since 13/08/2012
 */
public final class BooleanUtils {

	private BooleanUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Restituisce un boolean a partire dall'oggetto {@link Boolean}.
	 * <br>
	 * Se l'oggetto è {@code null} restituisce un valore di default.
	 *
	 * @param value oggetto {@link Boolean} da esaminare.
	 * @param defaultValue valore di default.
	 */
	public static boolean removeNull(final Boolean value, final boolean defaultValue) {
		return value == null ? defaultValue : value.booleanValue();
	}

	/**
	 * Restituisce l'oggetto {@code Boolean} negato.
	 * <br>
	 * Se l'oggetto è {@code null} restituisce {@code null}.
	 */
	public static Boolean not(final Boolean value) {
		return value == null ? null : value.booleanValue() ? Boolean.FALSE : Boolean.TRUE;
	}

	/**
	 * Restituisce {@code true} se l'oggetto {@code Boolean} da verificare
	 * non è {@code null} e il primitivo boolean rappresentato è {@code true}.
	 *
	 * <pre>
	 * isTrue(null) -> false
	 * isTrue(Boolean.FALSE) -> false
	 * isTrue(Boolean.TRUE) -> true
	 * </pre>
	 */
	public static boolean isTrue(final Boolean value) {
		return value != null && value.booleanValue();
	}

	/**
	 * Restituisce {@code true} se l'oggetto {@code Boolean} da verificare
	 * non è {@code null} e il primitivo boolean rappresentato è {@code false}.
	 *
	 * <pre>
	 * isFalse(null) -> false
	 * isFalse(Boolean.FALSE) -> true
	 * isFalse(Boolean.TRUE) -> false
	 * </pre>
	 */
	public static boolean isFalse(final Boolean value) {
		return value != null && !value.booleanValue();
	}

	/**
	 * Effettua l'and logico tra i valori boolean rappresentati
	 * dai due oggetti {@code Boolean} specificati.
	 * Restituisce {@code true} se entrambi gli oggetti
	 * rappresentano il valore boolean {@code true}.
	 *
	 * <pre>
	 * and(Boolean.TRUE, Boolean.TRUE) -> true
	 * and(Boolean.TRUE, Boolean.FALSE) -> false
	 * and(Boolean.FALSE, Boolean.FALSE) -> false
	 * and(Boolean.TRUE, null) -> false
	 * and(null, null) -> false
	 * </pre>
	 *
	 * @param b1 primo oggetto.
	 * @param b2 secondo oggetto.
	 */
	public static boolean and(final Boolean b1, final Boolean b2) {
		return isTrue(b1) && isTrue(b2);
	}

	/**
	 * Effettua l'or logico tra i valori boolean rappresentati
	 * dai due oggetti {@code Boolean} specificati.
	 * Restituisce {@code true} se almeno uno dei due oggetti
	 * rappresenta il valore boolean {@code true}.
	 *
	 * <pre>
	 * or(Boolean.TRUE, Boolean.TRUE) -> true
	 * or(Boolean.TRUE, Boolean.FALSE) -> true
	 * or(Boolean.FALSE, Boolean.FALSE) -> false
	 * or(Boolean.TRUE, null) -> true
	 * or(null, null) -> false
	 * </pre>
	 *
	 * @param b1 primo oggetto.
	 * @param b2 secondo oggetto.
	 */
	public static boolean or(final Boolean b1, final Boolean b2) {
		return isTrue(b1) || isTrue(b2);
	}

	/**
	 * Restituisce un valore {@code boolean} a partire
	 * da un intero.
	 * Se l'intero è diverso da 0 restituisce {@code true}.
	 *
	 * @param value intero da verificare.
	 * @return {@code true} se l'intero non è 0, {@code false} altrimenti.
	 */
	public static boolean fromInt(final int value) {
		return value != 0;
	}

	/**
	 * Restituisce la rappresentazione in intero di un boolean (0 - 1).
	 *
	 * @param value boolean da verificare.
	 * @return 1 se la condizione è {@code true}, 0 altrimenti.
	 */
	public static int toInt(final boolean value) {
		return value ? 1 : 0;
	}

	/**
	 * A partire da un oggetto {@code Boolean} restituisce:
	 * <ul>
	 * <li>{@code null} se l'oggetto è {@code null}.</li>
	 * <li>un valore definito a seconda che l'oggetto
	 * rappresenti {@code true} oppure {@code false}.</li>
	 * </ul>
	 *
	 * @param value l'oggetto {@code Boolean} da verificare.
	 * @param ifTrue l'oggetto da restituire se {@code value.booleanValue()}.
	 * @param ifFalse l'oggetto da restituire se {@code !value.booleanValue()}.
	 */
	public static <T> T iif(final Boolean value, final T ifTrue, final T ifFalse) {
		return value == null ? (T) null : value.booleanValue() ? ifTrue : ifFalse;
	}

	/**
	 * Restituisce una rappresentazione in stringa
	 * di un oggetto {@code Boolean}.
	 *
	 * @param value oggetto da rappresentare.
	 * @param nullString stringa nel caso di oggetto {@code null}.
	 * @param trueString stringa nel caso di valore {@code true}.
	 * @param falseString stringa nel caso di valore {@code false}.
	 */
	public static String toString(final Boolean value, final String nullString, final String trueString, final String falseString) {
		if (value == null) {
			return nullString;
		}
		return value.booleanValue() ? trueString : falseString;
	}

	/**
	 * Effettua l'AND logico tra i valori boolean specificati.
	 */
	public static boolean and(final @NonNull boolean... booleans) {
		requireNonNull(booleans);
		if (booleans.length == 0) {
			throw new IllegalArgumentException("array is empty");
		}
		for (final boolean b : booleans) {
			if (!b) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Effettua l'OR logico tra i valori boolean specificati.
	 */
	public static boolean or(final @NonNull boolean... booleans) {
		requireNonNull(booleans);
		if (booleans.length == 0) {
			throw new IllegalArgumentException("array is empty");
		}
		for (final boolean b : booleans) {
			if (b) {
				return true;
			}
		}
		return false;
	}
}