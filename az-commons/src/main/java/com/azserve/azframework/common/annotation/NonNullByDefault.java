package com.azserve.azframework.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Annotazione che indica che un campo,
 * un parametro o un metodo non è o non
 * ritorna mai {@code null}.
 *
 * @author filippo
 */
@Target({ ElementType.METHOD, ElementType.TYPE, ElementType.PACKAGE })
public @interface NonNullByDefault {}