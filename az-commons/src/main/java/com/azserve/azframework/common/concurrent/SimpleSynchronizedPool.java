package com.azserve.azframework.common.concurrent;

import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.function.Function;
import java.util.function.Supplier;

import com.azserve.azframework.common.ex.CheckedFunction;

/**
 * Experimental
 *
 * Classe che permette di gestire un pool di oggetti ed eseguire operazioni su questi oggetti.
 *
 * Il nome Simple è perché, a differenza di altri ObjectPool,
 * non permette di ottenere l'oggetto ma solo di operare con esso tramite il metodo apply
 *
 * La classe è thread safe
 *
 * Possibili migliorie presenti su librerie o altri esempi:
 * - svuotare forzatamente la lista per liberare memoria (in ogni caso sono tutte weak reference)
 * - eseguire un metodo "postActivate" appena dopo il lock e un "prePassivate" prima dell'unlock
 * - eseguire una metodo distruttore (es. nel caso di Closable) quando su libera forzatamente la lista
 *
 */
public class SimpleSynchronizedPool<T> {

	/** Numero massimo di oggetti instanziabili */
	private final int max;
	/** Supplier che istanzia un nuovo oggetto */
	private final Supplier<T> builder;

	/** Coda con gli oggetti utilizzabili */
	private final Queue<WeakReference<T>> freeQueue;
	/** Numero di oggetti impegnati */
	private int lockedCount = 0;

	public SimpleSynchronizedPool(final int max, final Supplier<T> builder) {
		if (max <= 0) {
			throw new IllegalArgumentException("max <= 0 (" + max + ")");
		}
		if (builder == null) {
			throw new NullPointerException("builder == null");
		}
		this.max = max;
		this.freeQueue = new ArrayDeque<>();
		this.builder = builder;
	}

	public final <R> R apply(final Function<T, R> f) throws InterruptedException {
		final T t = this.lock();
		try {
			return f.apply(t);
		} finally {
			this.unlock(t);
		}

	}

	public final <R, E extends Exception> R applyWithException(final CheckedFunction<T, R, E> f) throws E, InterruptedException {
		final T t = this.lock();
		try {
			final R r = f.apply(t);
			return r;
		} finally {
			this.unlock(t);
		}
	}

	private synchronized void unlock(final T t) {
		this.lockedCount--;
		this.freeQueue.add(new WeakReference<>(t));
		this.notify();
	}

	private synchronized T lock() throws InterruptedException {

		while (this.lockedCount >= this.max) {
			this.wait();
		}

		this.lockedCount++;
		final WeakReference<T> wt = this.freeQueue.poll();
		final T t;
		if (wt == null || (t = wt.get()) == null) {
			return this.builder.get();
		}
		return t;
	}

	public final int getLockedCount() {
		return this.lockedCount;
	}

	public final int getFreeCount() {
		return this.freeQueue.size();
	}

}
