package com.azserve.azframework.common.util;

import java.io.Serializable;

import com.azserve.azframework.common.annotation.NonNull;

public class ObjBooleanPair<T> implements Serializable {

	private static final long serialVersionUID = -7297997935814176961L;

	public static @NonNull <V> ObjBooleanPair<V> of(final V val, final boolean booleanValue) {
		return new ObjBooleanPair<>(val, booleanValue);
	}

	private final T value;
	private final boolean booleanValue;

	public ObjBooleanPair(final T value, final boolean booleanValue) {
		this.value = value;
		this.booleanValue = booleanValue;
	}

	public T getValue() {
		return this.value;
	}

	public boolean getBooleanValue() {
		return this.booleanValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.booleanValue ? 1231 : 1237);
		result = prime * result + (this.value == null ? 0 : this.value.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final ObjBooleanPair<?> other = (ObjBooleanPair<?>) obj;
		if (this.booleanValue != other.booleanValue) {
			return false;
		}
		if (this.value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!this.value.equals(other.value)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return this.value + ";" + this.booleanValue;
	}
}