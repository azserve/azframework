package com.azserve.azframework.common.enums;

public interface EnumWithChar {

	char code();
}
