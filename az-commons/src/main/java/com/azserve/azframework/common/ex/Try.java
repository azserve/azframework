package com.azserve.azframework.common.ex;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.azserve.azframework.common.util.Either;

/**
 * Imported and adapted from VAVR 0.10.4 https://github.com/vavr-io/vavr.
 *
 * It does not handle throwables but it is limited to exceptions.
 */
public abstract class Try<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	public static class FailureException extends RuntimeException {

		private static final long serialVersionUID = 1L;

		public FailureException(final String message, final Throwable cause) {
			super(message, cause);
		}
	}

	private Try() {}

	public static <T> Try<T> of(final CheckedSupplier<? extends T, ?> supplier) {
		requireNonNull(supplier, "supplier is null");
		try {
			return new Success<>(supplier.get());
		} catch (final Exception ex) {
			return new Failure<>(ex);
		}
	}

	public static <T> Try<T> ofSupplier(final Supplier<? extends T> supplier) {
		requireNonNull(supplier, "supplier is null");
		return of(supplier::get);
	}

	public static <T> Try<T> ofCallable(final Callable<? extends T> callable) {
		requireNonNull(callable, "callable is null");
		return of(callable::call);
	}

	public static Try<Void> run(final CheckedRunnable<?> runnable) {
		requireNonNull(runnable, "runnable is null");
		try {
			runnable.run();
			return new Success<>(null); // null represents the absence of an value, i.e. Void
		} catch (final Exception ex) {
			return new Failure<>(ex);
		}
	}

	public static Try<Void> runRunnable(final Runnable runnable) {
		requireNonNull(runnable, "runnable is null");
		return run(runnable::run);
	}

	public static <T> Try<T> success(final T value) {
		return new Success<>(value);
	}

	public static <T> Try<T> failure(final Exception exception) {
		return new Failure<>(exception);
	}

	@SuppressWarnings("unchecked")
	public static <T> Try<T> narrow(final Try<? extends T> t) {
		return (Try<T>) t;
	}

	public final Try<T> andThen(final Consumer<? super T> consumer) {
		requireNonNull(consumer, "consumer is null");
		return this.andThenTry(consumer::accept);
	}

	public final Try<T> andThenTry(final CheckedConsumer<? super T, ?> consumer) {
		requireNonNull(consumer, "consumer is null");
		if (this.isFailure()) {
			return this;
		}
		try {
			consumer.accept(this.get());
			return this;
		} catch (final Exception ex) {
			return new Failure<>(ex);
		}
	}

	public final Try<T> andThen(final Runnable runnable) {
		requireNonNull(runnable, "runnable is null");
		return this.andThenTry(runnable::run);
	}

	public final Try<T> andThenTry(final CheckedRunnable<?> runnable) {
		requireNonNull(runnable, "runnable is null");
		if (this.isFailure()) {
			return this;
		}
		try {
			runnable.run();
			return this;
		} catch (final Exception ex) {
			return new Failure<>(ex);
		}
	}

	public final Try<Exception> failed() {
		if (this.isFailure()) {
			return new Success<>(this.getCause());
		}
		return new Failure<>(new NoSuchElementException("Success.failed()"));
	}

	public final Try<T> filter(final Predicate<? super T> predicate, final Supplier<? extends Exception> exceptionSupplier) {
		requireNonNull(predicate, "predicate is null");
		requireNonNull(exceptionSupplier, "exceptionSupplier is null");
		return this.filterTry(predicate::test, exceptionSupplier);
	}

	public final Try<T> filter(final Predicate<? super T> predicate, final Function<? super T, ? extends Exception> errorProvider) {
		requireNonNull(predicate, "predicate is null");
		requireNonNull(errorProvider, "errorProvider is null");
		return this.filterTry(predicate::test, errorProvider::apply);
	}

	public final Try<T> filter(final Predicate<? super T> predicate) {
		requireNonNull(predicate, "predicate is null");
		return this.filterTry(predicate::test);
	}

	public final Try<T> filterNot(final Predicate<? super T> predicate, final Supplier<? extends Exception> exceptionSupplier) {
		requireNonNull(predicate, "predicate is null");
		return this.filter(predicate.negate(), exceptionSupplier);
	}

	public final Try<T> filterNot(final Predicate<? super T> predicate, final Function<? super T, ? extends Exception> errorProvider) {
		requireNonNull(predicate, "predicate is null");
		return this.filter(predicate.negate(), errorProvider);
	}

	public final Try<T> filterNot(final Predicate<? super T> predicate) {
		requireNonNull(predicate, "predicate is null");
		return this.filter(predicate.negate());
	}

	public final Try<T> filterTry(final CheckedPredicate<? super T, ?> predicate, final Supplier<? extends Exception> exceptionSupplier) {
		requireNonNull(predicate, "predicate is null");
		requireNonNull(exceptionSupplier, "exceptionSupplier is null");

		if (this.isFailure()) {
			return this;
		}
		try {
			if (predicate.test(this.get())) {
				return this;
			}
			return new Failure<>(exceptionSupplier.get());
		} catch (final Exception ex) {
			return new Failure<>(ex);
		}
	}

	public final Try<T> filterTry(final CheckedPredicate<? super T, ?> predicate, final CheckedFunction<? super T, ? extends Exception, ?> errorProvider) {
		requireNonNull(predicate, "predicate is null");
		requireNonNull(errorProvider, "errorProvider is null");
		return this.flatMapTry(t -> predicate.test(t) ? this : failure(errorProvider.apply(t)));
	}

	public final Try<T> filterTry(final CheckedPredicate<? super T, ?> predicate) {
		requireNonNull(predicate, "predicate is null");
		return this.filterTry(predicate, () -> new NoSuchElementException("Predicate does not hold for " + this.get()));
	}

	public final <U> Try<U> flatMap(final Function<? super T, ? extends Try<? extends U>> mapper) {
		requireNonNull(mapper, "mapper is null");
		return this.flatMapTry((CheckedFunction<T, Try<? extends U>, ?>) mapper::apply);
	}

	@SuppressWarnings("unchecked")
	public final <U> Try<U> flatMapTry(final CheckedFunction<? super T, ? extends Try<? extends U>, ?> mapper) {
		requireNonNull(mapper, "mapper is null");
		if (this.isFailure()) {
			return (Failure<U>) this;
		}
		try {
			return (Try<U>) mapper.apply(this.get());
		} catch (final Exception ex) {
			return new Failure<>(ex);
		}
	}

	protected abstract T get();

	public abstract Exception getCause();

	public abstract boolean isFailure();

	public abstract boolean isSuccess();

	public final <U> Try<U> map(final Function<? super T, ? extends U> mapper) {
		requireNonNull(mapper, "mapper is null");
		return this.mapTry(mapper::apply);
	}

	@SuppressWarnings("unchecked")
	public final <U> Try<U> mapTry(final CheckedFunction<? super T, ? extends U, ?> mapper) {
		requireNonNull(mapper, "mapper is null");
		if (this.isFailure()) {
			return (Failure<U>) this;
		}
		try {
			return new Success<>(mapper.apply(this.get()));
		} catch (final Exception ex) {
			return new Failure<>(ex);
		}
	}

	@SuppressWarnings("unchecked")
	public final <X extends Exception> Try<T> mapFailure(final Class<X> exceptionType, final Function<? super X, ? extends Exception> mapper) {
		requireNonNull(exceptionType, "exceptionType is null");
		requireNonNull(mapper, "mapper is null");
		if (this.isFailure()) {
			final Exception cause = this.getCause();
			if (exceptionType.isAssignableFrom(cause.getClass())) {
				return Try.failure(mapper.apply((X) cause));
			}
		}
		return this;
	}

	public final Try<T> onFailure(final Consumer<? super Exception> action) {
		requireNonNull(action, "action is null");
		if (this.isFailure()) {
			action.accept(this.getCause());
		}
		return this;
	}

	@SuppressWarnings("unchecked")
	public final <X extends Exception> Try<T> onFailure(final Class<X> exceptionType, final Consumer<? super X> action) {
		requireNonNull(exceptionType, "exceptionType is null");
		requireNonNull(action, "action is null");
		if (this.isFailure() && exceptionType.isAssignableFrom(this.getCause().getClass())) {
			action.accept((X) this.getCause());
		}
		return this;
	}

	public final Try<T> onSuccess(final Consumer<? super T> action) {
		requireNonNull(action, "action is null");
		if (this.isSuccess()) {
			action.accept(this.get());
		}
		return this;
	}

	@SuppressWarnings("unchecked")
	public final Try<T> or(final Try<? extends T> other) {
		requireNonNull(other, "other is null");
		return this.isSuccess() ? this : (Try<T>) other;
	}

	@SuppressWarnings("unchecked")
	public final Try<T> or(final Supplier<? extends Try<? extends T>> supplier) {
		requireNonNull(supplier, "supplier is null");
		return this.isSuccess() ? this : (Try<T>) supplier.get();
	}

	public final T orElse(final T value) {
		return this.isSuccess() ? this.get() : value;
	}

	public final T orElseThrow() {
		return this.get();
	}

	public final T orElseGet(final Supplier<? extends T> supplier) {
		requireNonNull(supplier, "supplier is null");
		return this.isSuccess() ? this.get() : supplier.get();
	}

	public final T orElseGet(final Function<? super Exception, ? extends T> other) {
		requireNonNull(other, "other is null");
		if (this.isFailure()) {
			return other.apply(this.getCause());
		}
		return this.get();
	}

	public final void orElseRun(final Consumer<? super Exception> action) {
		requireNonNull(action, "action is null");
		if (this.isFailure()) {
			action.accept(this.getCause());
		}
	}

	public final <X extends Exception> T orElseThrow(final Function<? super Exception, X> exceptionProvider) throws X {
		requireNonNull(exceptionProvider, "exceptionProvider is null");
		if (this.isFailure()) {
			throw exceptionProvider.apply(this.getCause());
		}
		return this.get();
	}

	public final Optional<T> toOptional() {
		return this.isSuccess() ? Optional.ofNullable(this.get()) : Optional.empty();
	}

	public final Either<Exception, T> toEither() {
		return this.isSuccess() ? Either.right(this.get()) : Either.left(this.getCause());
	}

	public final <X> X fold(final Function<? super Exception, ? extends X> ifFail, final Function<? super T, ? extends X> f) {
		if (this.isFailure()) {
			return ifFail.apply(this.getCause());
		}
		return f.apply(this.get());
	}

	public final Try<T> peek(final Consumer<? super Exception> failureAction, final Consumer<? super T> successAction) {
		requireNonNull(failureAction, "failureAction is null");
		requireNonNull(successAction, "successAction is null");

		if (this.isFailure()) {
			failureAction.accept(this.getCause());
		} else {
			successAction.accept(this.get());
		}

		return this;
	}

	public final Try<T> peek(final Consumer<? super T> successAction) {
		requireNonNull(successAction, "successAction is null");
		if (this.isSuccess()) {
			successAction.accept(this.get());
		}
		return this;
	}

	@SuppressWarnings("unchecked")
	public final <X extends Exception> Try<T> recover(final Class<X> exceptionType, final Function<? super X, ? extends T> f) {
		requireNonNull(exceptionType, "exceptionType is null");
		requireNonNull(f, "f is null");
		if (this.isFailure()) {
			final Exception cause = this.getCause();
			if (exceptionType.isAssignableFrom(cause.getClass())) {
				return Try.of(() -> f.apply((X) cause));
			}
		}
		return this;
	}

	@SuppressWarnings("unchecked")
	public final <X extends Exception> Try<T> recoverWith(final Class<X> exceptionType, final Function<? super X, Try<? extends T>> f) {
		requireNonNull(exceptionType, "exceptionType is null");
		requireNonNull(f, "f is null");
		if (this.isFailure()) {
			final Exception cause = this.getCause();
			if (exceptionType.isAssignableFrom(cause.getClass())) {
				try {
					return narrow(f.apply((X) cause));
				} catch (final Exception ex) {
					return new Failure<>(ex);
				}
			}
		}
		return this;
	}

	public final <X extends Exception> Try<T> recoverWith(final Class<X> exceptionType, final Try<? extends T> recovered) {
		requireNonNull(exceptionType, "exeptionType is null");
		requireNonNull(recovered, "recovered is null");
		return this.isFailure() && exceptionType.isAssignableFrom(this.getCause().getClass())
				? narrow(recovered)
				: this;
	}

	public final <X extends Exception> Try<T> recover(final Class<X> exceptionType, final T value) {
		requireNonNull(exceptionType, "exceptionType is null");
		return this.isFailure() && exceptionType.isAssignableFrom(this.getCause().getClass())
				? Try.success(value)
				: this;
	}

	public final Try<T> recover(final Function<? super Exception, ? extends T> f) {
		requireNonNull(f, "f is null");
		if (this.isFailure()) {
			return Try.of(() -> f.apply(this.getCause()));
		}
		return this;
	}

	@SuppressWarnings("unchecked")
	public final Try<T> recoverWith(final Function<? super Exception, ? extends Try<? extends T>> f) {
		requireNonNull(f, "f is null");
		if (this.isFailure()) {
			try {
				return (Try<T>) f.apply(this.getCause());
			} catch (final Exception ex) {
				return new Failure<>(ex);
			}
		}
		return this;
	}

	public final <U> U transform(final Function<? super Try<T>, ? extends U> f) {
		requireNonNull(f, "f is null");
		return f.apply(this);
	}

	public final Try<T> andFinally(final Runnable runnable) {
		requireNonNull(runnable, "runnable is null");
		return this.andFinallyTry(runnable::run);
	}

	public final Try<T> andFinallyTry(final CheckedRunnable<?> runnable) {
		requireNonNull(runnable, "runnable is null");
		try {
			runnable.run();
			return this;
		} catch (final Exception ex) {
			return new Failure<>(ex);
		}
	}

	private static final class Success<T> extends Try<T> {

		private static final long serialVersionUID = 1L;

		private final T value;

		Success(final T value) {
			this.value = value;
		}

		@Override
		protected T get() {
			return this.value;
		}

		@Override
		public Exception getCause() {
			throw new UnsupportedOperationException("getCause on Success");
		}

		@Override
		public boolean isFailure() {
			return false;
		}

		@Override
		public boolean isSuccess() {
			return true;
		}

		@Override
		public boolean equals(final Object obj) {
			return obj == this || obj instanceof Success && Objects.equals(this.value, ((Success<?>) obj).value);
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(this.value);
		}

		@Override
		public String toString() {
			return "Success (" + this.value + ")";
		}
	}

	private static final class Failure<T> extends Try<T> {

		private static final long serialVersionUID = 1L;

		private final Exception cause;

		Failure(final Exception cause) {
			requireNonNull(cause, "cause is null");
			this.cause = cause;
		}

		@Override
		protected T get() {
			if (this.cause instanceof RuntimeException) {
				throw (RuntimeException) this.cause;
			}
			throw new FailureException("Cannot retrieve value, execution has failed", this.cause);
		}

		@Override
		public Exception getCause() {
			return this.cause;
		}

		@Override
		public boolean isFailure() {
			return true;
		}

		@Override
		public boolean isSuccess() {
			return false;
		}

		@Override
		public boolean equals(final Object obj) {
			return obj == this || obj instanceof Failure && Arrays.deepEquals(this.cause.getStackTrace(), ((Failure<?>) obj).cause.getStackTrace());
		}

		@Override
		public int hashCode() {
			return Arrays.hashCode(this.cause.getStackTrace());
		}

		@Override
		public String toString() {
			return "Failure (" + this.cause + ")";
		}
	}
}
