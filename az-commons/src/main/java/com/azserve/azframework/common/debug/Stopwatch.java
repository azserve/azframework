package com.azserve.azframework.common.debug;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.ObjLongConsumer;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;

/**
 * Classe di utilità per calcolare il tempo di esecuzione
 * tra più istruzioni.
 * <br>
 * Utilizzo:
 * <pre>
 * StopWatch sw = new {@linkplain #Stopwatch(String) StopWatch}("test");
 * sw.{@link #start()};
 * firstOperation();
 * sw.{@link #step(String) step}("first");
 * secondOperation();
 * sw.{@link #step(String) step}("second");
 * ...
 * lastOperation();
 * sw.{@link #step(String) step}("last");
 * String result = {@link #done()};
 * </pre>
 */
public final class Stopwatch {

	/**
	 * Crea un cronometro che scrive una riga direttamente su
	 * standard output con il tempo appena salvato nel formato:
	 * <pre>
	 * descrizione - tempo di esecuzione
	 * </pre>
	 *
	 * @param name nome del cronometro.
	 */
	public static Stopwatch stdout(final String name) {
		return new Stopwatch(name, (s, v) -> System.out.println(s + " - " + v));
	}

	@NonNull
	private final String name;

	@NonNull
	private final ObjLongConsumer<String> csm;

	@NonNull
	private final List<TimerEntry> times = new ArrayList<>();

	private boolean active = false;
	private long startTime;
	private long lastTime;

	/**
	 * Inizializza il cronometro con il nome specificato.
	 *
	 * @param name nome del cronometro.
	 */
	public Stopwatch(final @NonNull String name) {
		this(name, (s, v) -> {});
	}

	/**
	 * Inizializza il cronometro con il nome specificato e una funzione
	 * per la gestione dell'ultimo tempo salvato.
	 *
	 * @param name nome del cronometro.
	 * @param csm funzione che "consuma" due parametri:
	 *            nome del passo e tempo impiegato per eseguirlo.
	 */
	public Stopwatch(final @NonNull String name, final @NonNull ObjLongConsumer<String> csm) {
		this.name = Objects.requireNonNull(name, "Stopwatch name required");
		this.csm = Objects.requireNonNull(csm, "Consumer required");
	}

	/**
	 * Avvia il cronometro.
	 *
	 * @throws IllegalStateException se il cronometro è già
	 *             stato avviato.
	 */
	public void start() {
		if (this.active) {
			throw new IllegalStateException("Stopwatch already started");
		}
		this.active = true;
		this.startTime = System.currentTimeMillis();
		this.lastTime = this.startTime;
	}

	/**
	 * Salva un tempo.
	 *
	 * @param description descrizione.
	 * @throws IllegalStateException se il cronometro non è stato attivato.
	 */
	public void step(final String description) {
		if (!this.active) {
			throw new IllegalStateException("Stopwatch must be running");
		}
		final long currentTime = System.currentTimeMillis();
		this.times.add(new TimerEntry(description, currentTime));
		this.csm.accept(description, currentTime - this.lastTime);
		this.lastTime = currentTime;
	}

	/**
	 * Ferma il cronometro e restituisce i tempi
	 * memorizzati come testo nel formato seguente:
	 * <pre>
	 * TIMER: nome_timer
	 * tempo[0] : ms trascorsi dall'avvio
	 * tempo[1] : ms trascorsi dall'avvio, ms trascorsi dal tempo[0]
	 * tempo[n] : ms trascorsi dall'avvio, ms trascorsi dal tempo[n-1]
	 * ...
	 * </pre>
	 * Tutti i tempi memorizzati vengono rimossi.
	 */
	@Nullable
	public String done() {
		if (this.times.isEmpty()) {
			return null;
		}
		final StringBuilder result = new StringBuilder();
		result.append("TIMER: ").append(this.name).append("\n");

		TimerEntry lastEntry = this.times.get(0);
		result.append(lastEntry.description).append(" : ");
		result.append(lastEntry.time - this.startTime).append(" ms\n");
		for (int i = 1; i < this.times.size(); i++) {
			final TimerEntry entry = this.times.get(i);
			this.appendDescription(result, entry, lastEntry.time);
			lastEntry = entry;
		}
		this.times.clear();
		this.active = false;
		return result.substring(0, result.length() - 1);
	}

	private void appendDescription(final @NonNull StringBuilder sb, final @NonNull TimerEntry entry, final long previousTime) {
		sb.append(entry.description).append(" : ");
		sb.append(entry.time - this.startTime).append(" ms, ");
		sb.append(entry.time - previousTime).append(" ms\n");
	}

	private static final class TimerEntry {

		private final String description;
		private final long time;

		TimerEntry(final String description, final long time) {
			this.description = description;
			this.time = time;
		}
	}
}
