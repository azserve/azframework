package com.azserve.azframework.common.ex;

import static java.util.Objects.requireNonNull;

import java.util.function.BiConsumer;

public interface CheckedBiConsumer<T, U, E extends Exception> {

	void accept(T t, U u) throws E;

	/**
	 * Unwraps thid checked consumer to an unchecked one.
	 */
	default BiConsumer<T, U> unchecked() {
		return (t, u) -> {
			try {
				this.accept(t, u);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
			}
		};
	}

	default CheckedBiConsumer<T, U, E> andThen(final CheckedBiConsumer<? super T, ? super U, ? extends E> consumer) {
		requireNonNull(consumer);
		return (t, u) -> {
			this.accept(t, u);
			consumer.accept(t, u);
		};
	}

	/**
	 * Creates a {@link CheckedBiConsumer} by using a method reference.
	 *
	 * @param function the function as method reference
	 * @return the function
	 */
	static <T, U, E extends Exception> CheckedBiConsumer<T, U, E> of(final CheckedBiConsumer<T, U, E> consumer) {
		return requireNonNull(consumer);
	}

	/**
	 * Wraps an unchecked consumer in an unchecked one.
	 *
	 * @param consumer the unchecked consumer
	 */
	static <T, U, E extends Exception> CheckedBiConsumer<T, U, E> wrap(final BiConsumer<T, U> consumer) {
		return requireNonNull(consumer)::accept;
	}

	/**
	 * Unwraps a checked consumer to have an unchecked one.
	 * The exception is then propagated to this method caller.
	 *
	 * @throws E the propagated exception
	 */
	static <T, U, E extends Exception> BiConsumer<T, U> unwrap(final CheckedBiConsumer<T, U, E> consumer) throws E {
		requireNonNull(consumer);
		return (t, u) -> {
			try {
				consumer.accept(t, u);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
			}
		};
	}

	/**
	 * Unwraps a checked consumer to have an unchecked one.
	 */
	static <T, U, E extends Exception> BiConsumer<T, U> unchecked(final CheckedBiConsumer<T, U, E> consumer) {
		return (t, u) -> {
			try {
				consumer.accept(t, u);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
			}
		};
	}
}
