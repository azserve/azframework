package com.azserve.azframework.common.util;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Imported and adapted from VAVR 0.10.4 https://github.com/vavr-io/vavr.
 */
public abstract class Either<L, R> implements Serializable {

	private static final long serialVersionUID = 1L;

	private Either() {}

	/**
	 * Constructs a right instance.
	 *
	 * @param right The value.
	 * @param <L> type of left value.
	 * @param <R> type of right value.
	 * @return a new Right instance.
	 */
	public static <L, R> Either<L, R> right(final R right) {
		return new Right<>(right);
	}

	/**
	 * Constructs a left instance.
	 *
	 * @param left The value.
	 * @param <L> type of left value.
	 * @param <R> type of right value.
	 * @return a new Left instance.
	 */
	public static <L, R> Either<L, R> left(final L left) {
		return new Left<>(left);
	}

	/**
	 * Narrows a widened {@code Either<? extends L, ? extends R>} to {@code Either<L, R>}
	 * by performing a type-safe cast. This is eligible because immutable/read-only
	 * collections are covariant.
	 *
	 * @param either a {@code Either}.
	 * @param <L> type of left value.
	 * @param <R> type of right value.
	 * @return the given {@code either} instance as narrowed type {@code Either<L, R>}.
	 */
	@SuppressWarnings("unchecked")
	public static <L, R> Either<L, R> narrow(final Either<? extends L, ? extends R> either) {
		return (Either<L, R>) either;
	}

	/**
	 * Returns the left value.
	 *
	 * @return the left value.
	 * @throws NoSuchElementException if this is a Right.
	 */
	public abstract L getLeft();

	/**
	 * Returns whether this Either is a left.
	 *
	 * @return true, if this is a left, false otherwise
	 */
	public abstract boolean isLeft();

	/**
	 * Returns whether this Either is a right.
	 *
	 * @return true, if this is a right, false otherwise
	 */
	public abstract boolean isRight();

	public final LeftProjection<L, R> left() {
		return new LeftProjection<>(this);
	}

	public final RightProjection<L, R> right() {
		return new RightProjection<>(this);
	}

	/**
	 * Maps either the left or the right side of this disjunction.
	 *
	 * @param leftMapper maps the left value if this is a left
	 * @param rightMapper maps the right value if this is a right
	 * @param <X> The new left type of the resulting Either
	 * @param <Y> The new right type of the resulting Either
	 * @return a new Either instance
	 */
	public final <X, Y> Either<X, Y> bimap(final Function<? super L, ? extends X> leftMapper, final Function<? super R, ? extends Y> rightMapper) {
		requireNonNull(leftMapper, "leftMapper is null");
		requireNonNull(rightMapper, "rightMapper is null");
		if (this.isRight()) {
			return new Right<>(rightMapper.apply(this.get()));
		}
		return new Left<>(leftMapper.apply(this.getLeft()));
	}

	/**
	 * Folds either the left or the right side of this disjunction.
	 *
	 * @param leftMapper maps the left value if this is a left
	 * @param rightMapper maps the right value if this is a right
	 * @param <U> type of the folded value
	 * @return a value of type U
	 */
	public final <U> U fold(final Function<? super L, ? extends U> leftMapper, final Function<? super R, ? extends U> rightMapper) {
		requireNonNull(leftMapper, "leftMapper is null");
		requireNonNull(rightMapper, "rightMapper is null");
		if (this.isRight()) {
			return rightMapper.apply(this.get());
		}
		return leftMapper.apply(this.getLeft());
	}

	/**
	 * Transforms this {@code Either}.
	 *
	 * @param f a transformation
	 * @param <U> type of transformation result
	 * @return an instance of type {@code U}
	 * @throws NullPointerException if {@code f} is null
	 */
	public final <U> U transform(final Function<? super Either<L, R>, ? extends U> f) {
		requireNonNull(f, "f is null");
		return f.apply(this);
	}

	/**
	 * Gets the Right value or an alternate value, if the projected Either is a left.
	 *
	 * @param other a function which converts a Left value to an alternative Right value
	 * @return the right value, if the underlying Either is a right or else the alternative Right value provided by
	 *         {@code other} by applying the Left value.
	 */
	public final R orElseGet(final Function<? super L, ? extends R> other) {
		requireNonNull(other, "other is null");
		if (this.isRight()) {
			return this.get();
		}
		return other.apply(this.getLeft());
	}

	/**
	 * Runs an action in the case this is a projection on a Left value.
	 *
	 * @param action an action which consumes a Left value
	 */
	public final void orElseRun(final Consumer<? super L> action) {
		requireNonNull(action, "action is null");
		if (this.isLeft()) {
			action.accept(this.getLeft());
		}
	}

	/**
	 * Gets the Right value or throws, if the projected Either is a left.
	 *
	 * @param <X> a throwable type
	 * @param exceptionFunction a function which creates an exception based on a Left value
	 * @return the right value, if the underlying Either is a right or else throws the exception provided by
	 *         {@code exceptionFunction} by applying the Left value.
	 * @throws X if the projected Either is a left
	 */
	public final <X extends Throwable> R orElseThrow(final Function<? super L, X> exceptionFunction) throws X {
		requireNonNull(exceptionFunction, "exceptionFunction is null");
		if (this.isRight()) {
			return this.get();
		}
		throw exceptionFunction.apply(this.getLeft());
	}

	/**
	 * Converts a Left to a Right vice versa by wrapping the value in a new type.
	 *
	 * @return a new {@code Either}
	 */
	public final Either<R, L> swap() {
		if (this.isRight()) {
			return new Left<>(this.get());
		}
		return new Right<>(this.getLeft());
	}

	/**
	 * Calls recoveryFunction if the projected Either is a left, performs no operation if this is a right. This is
	 * similar to {@code orElseGet}, but where the fallback method also returns an Either.
	 *
	 * @param recoveryFunction a function which accepts a Left value and returns an Either
	 * @return an {@code Either<L, R>} instance
	 * @throws NullPointerException if the given {@code recoveryFunction} is null
	 */
	@SuppressWarnings("unchecked")
	public final Either<L, R> recoverWith(final Function<? super L, ? extends Either<? extends L, ? extends R>> recoveryFunction) {
		requireNonNull(recoveryFunction, "recoveryFunction is null");
		if (this.isLeft()) {
			return (Either<L, R>) recoveryFunction.apply(this.getLeft());
		}
		return this;
	}

	/**
	 * Calls {@code recoveryFunction} if the projected Either is a left, or returns {@code this} if Right. The result
	 * of {@code recoveryFunction} will be projected as a Right.
	 *
	 * @param recoveryFunction a function which accepts a Left value and returns a Right value
	 * @return an {@code Either<L, R>} instance
	 * @throws NullPointerException if the given {@code recoveryFunction} is null
	 */
	public final Either<L, R> recover(final Function<? super L, ? extends R> recoveryFunction) {
		requireNonNull(recoveryFunction, "recoveryFunction is null");
		if (this.isLeft()) {
			return right(recoveryFunction.apply(this.getLeft()));
		}
		return this;
	}

	/**
	 * FlatMaps this right-biased Either.
	 *
	 * @param mapper a mapper
	 * @param <U> component type of the mapped right value
	 * @return this as {@code Either<L, U>} if this is a left, otherwise the right mapping result
	 * @throws NullPointerException if {@code mapper} is null
	 */
	@SuppressWarnings("unchecked")
	public final <U> Either<L, U> flatMap(final Function<? super R, ? extends Either<L, ? extends U>> mapper) {
		requireNonNull(mapper, "mapper is null");
		if (this.isRight()) {
			return (Either<L, U>) mapper.apply(this.get());
		}
		return (Either<L, U>) this;
	}

	/**
	 * Maps the value of this Either if it is a right, performs no operation if this is a left.
	 *
	 * @param mapper a mapper
	 * @param <U> component type of the mapped right value
	 * @return a mapped {@code Either}
	 * @throws NullPointerException if {@code mapper} is null
	 */
	@SuppressWarnings("unchecked")
	public final <U> Either<L, U> map(final Function<? super R, ? extends U> mapper) {
		requireNonNull(mapper, "mapper is null");
		if (this.isRight()) {
			return right(mapper.apply(this.get()));
		}
		return (Either<L, U>) this;
	}

	/**
	 * Maps the value of this Either if it is a left, performs no operation if this is a right.
	 *
	 * @param leftMapper a mapper
	 * @param <U> component type of the mapped right value
	 * @return a mapped {@code Either}
	 * @throws NullPointerException if {@code mapper} is null
	 */
	@SuppressWarnings("unchecked")
	public final <U> Either<U, R> mapLeft(final Function<? super L, ? extends U> leftMapper) {
		requireNonNull(leftMapper, "leftMapper is null");
		if (this.isLeft()) {
			return left(leftMapper.apply(this.getLeft()));
		}
		return (Either<U, R>) this;
	}

	/**
	 * Filters this right-biased {@code Either} by testing a predicate.
	 *
	 * @param predicate a predicate
	 * @return a new {@code Optional} instance
	 * @throws NullPointerException if {@code predicate} is null
	 */
	public final Optional<Either<L, R>> filter(final Predicate<? super R> predicate) {
		requireNonNull(predicate, "predicate is null");
		return this.isLeft() || predicate.test(this.get()) ? Optional.of(this) : Optional.empty();
	}

	/**
	 * Filters this right-biased {@code Either} by testing a predicate.
	 *
	 * @param predicate a predicate
	 * @return a new {@code Either}
	 * @throws NullPointerException if {@code predicate} is null
	 *
	 */
	public final Optional<Either<L, R>> filterNot(final Predicate<? super R> predicate) {
		requireNonNull(predicate, "predicate is null");
		return this.filter(predicate.negate());
	}

	/**
	 * Filters this right-biased {@code Either} by testing a predicate.
	 * If the {@code Either} is a Right and the predicate doesn't match, the
	 * {@code Either} will be turned into a Left with contents computed by applying
	 * the zero function to the {@code Either} value.
	 *
	 * @param predicate a predicate
	 * @param zero a function that turns a right value into a left value if the right value does not make it through the filter.
	 * @return an {@code Either} instance
	 * @throws NullPointerException if {@code predicate} is null
	 */
	public final Either<L, R> filterOrElse(final Predicate<? super R> predicate, final Function<? super R, ? extends L> zero) {
		requireNonNull(predicate, "predicate is null");
		requireNonNull(zero, "zero is null");
		if (this.isLeft() || predicate.test(this.get())) {
			return this;
		}
		return left(zero.apply(this.get()));
	}

	/**
	 * Gets the right value if this is a Right or throws if this is a Left.
	 *
	 * @return the right value
	 * @throws NoSuchElementException if this is a Left.
	 */
	public R orElseThrow() {
		return this.get();
	}

	protected abstract R get();

	public R orElse(final R other) {
		return this.isLeft() ? other : this.get();
	}

	@SuppressWarnings("unchecked")
	public final Either<L, R> orElse(final Either<? extends L, ? extends R> other) {
		requireNonNull(other, "other is null");
		return this.isRight() ? this : (Either<L, R>) other;
	}

	@SuppressWarnings("unchecked")
	public final Either<L, R> orElse(final Supplier<? extends Either<? extends L, ? extends R>> supplier) {
		requireNonNull(supplier, "supplier is null");
		return this.isRight() ? this : (Either<L, R>) supplier.get();
	}

	/**
	 * Performs the given {@code leftAction} on the left element if this is Left.
	 * Performs the given {@code rightAction} on the right element if this is Right.
	 *
	 * @param leftAction The action that will be performed on the left element
	 * @param rightAction The action that will be performed on the right element
	 * @return this instance
	 */
	public final Either<L, R> peek(final Consumer<? super L> leftAction, final Consumer<? super R> rightAction) {
		requireNonNull(leftAction, "leftAction is null");
		requireNonNull(rightAction, "rightAction is null");

		if (this.isLeft()) {
			leftAction.accept(this.getLeft());
		} else { // this isRight() by definition
			rightAction.accept(this.get());
		}

		return this;
	}

	public final Either<L, R> peek(final Consumer<? super R> action) {
		requireNonNull(action, "action is null");
		if (this.isRight()) {
			action.accept(this.get());
		}
		return this;
	}

	public final Either<L, R> peekLeft(final Consumer<? super L> action) {
		requireNonNull(action, "action is null");
		if (this.isLeft()) {
			action.accept(this.getLeft());
		}
		return this;
	}

	public static final class LeftProjection<L, R> {

		private final Either<L, R> either;

		private LeftProjection(final Either<L, R> either) {
			this.either = either;
		}

		public <L2, R2> LeftProjection<L2, R2> bimap(final Function<? super L, ? extends L2> leftMapper, final Function<? super R, ? extends R2> rightMapper) {
			return this.either.<L2, R2> bimap(leftMapper, rightMapper).left();
		}

		/**
		 * Gets the Left value or throws.
		 *
		 * @return the left value, if the underlying {@code Either} is a Left
		 * @throws NoSuchElementException if the underlying {@code Either} of this {@code LeftProjection} is a Right
		 */
		public L get() {
			if (this.either.isLeft()) {
				return this.either.getLeft();
			}
			throw new NoSuchElementException("LeftProjection.get() on Right");
		}

		@SuppressWarnings("unchecked")
		public LeftProjection<L, R> orElse(final LeftProjection<? extends L, ? extends R> other) {
			requireNonNull(other, "other is null");
			return this.either.isLeft() ? this : (LeftProjection<L, R>) other;
		}

		@SuppressWarnings("unchecked")
		public LeftProjection<L, R> orElse(final Supplier<? extends LeftProjection<? extends L, ? extends R>> supplier) {
			requireNonNull(supplier, "supplier is null");
			return this.either.isLeft() ? this : (LeftProjection<L, R>) supplier.get();
		}

		/**
		 * Gets the Left value or an alternate value, if the projected Either is a right.
		 *
		 * @param other an alternative value
		 * @return the left value, if the underlying Either is a left or else {@code other}
		 * @throws NoSuchElementException if the underlying either of this LeftProjection is a right
		 */
		public L orElse(final L other) {
			return this.either.isLeft() ? this.either.getLeft() : other;
		}

		/**
		 * Gets the Left value or an alternate value, if the projected Either is a right.
		 *
		 * @param other a function which converts a Right value to an alternative Left value
		 * @return the left value, if the underlying Either is a left or else the alternative Left value provided by
		 *         {@code other} by applying the Right value.
		 */
		public L orElseGet(final Function<? super R, ? extends L> other) {
			requireNonNull(other, "other is null");
			if (this.either.isLeft()) {
				return this.either.getLeft();
			}
			return other.apply(this.either.get());
		}

		/**
		 * Runs an action in the case this is a projection on a Right value.
		 *
		 * @param action an action which consumes a Right value
		 */
		public void orElseRun(final Consumer<? super R> action) {
			requireNonNull(action, "action is null");
			if (this.either.isRight()) {
				action.accept(this.either.get());
			}
		}

		/**
		 * Gets the Left value or throws, if the projected Either is a right.
		 *
		 * @param <X> a throwable type
		 * @param exceptionFunction a function which creates an exception based on a Right value
		 * @return the left value, if the underlying Either is a left or else throws the exception provided by
		 *         {@code exceptionFunction} by applying the Right value.
		 * @throws X if the projected Either is a right
		 */
		public <X extends Throwable> L orElseThrow(final Function<? super R, X> exceptionFunction) throws X {
			requireNonNull(exceptionFunction, "exceptionFunction is null");
			if (this.either.isLeft()) {
				return this.either.getLeft();
			}
			throw exceptionFunction.apply(this.either.get());
		}

		/**
		 * Returns the underlying either of this projection.
		 *
		 * @return the underlying either
		 */
		public Either<L, R> toEither() {
			return this.either;
		}

		/**
		 * Returns {@code Some} value of type L if this is a left projection of a Left value and the predicate
		 * applies to the underlying value.
		 *
		 * @param predicate a predicate
		 * @return a new Option
		 */
		public Optional<LeftProjection<L, R>> filter(final Predicate<? super L> predicate) {
			requireNonNull(predicate, "predicate is null");
			return this.either.isRight() || predicate.test(this.either.getLeft()) ? Optional.of(this) : Optional.empty();
		}

		/**
		 * FlatMaps this LeftProjection.
		 *
		 * @param mapper a mapper
		 * @param <U> component type of the mapped left value
		 * @return this as {@code LeftProjection<L, U>} if a Right is underlying, otherwise a the mapping result of the left value.
		 * @throws NullPointerException if {@code mapper} is null
		 */
		@SuppressWarnings("unchecked")
		public <U> LeftProjection<U, R> flatMap(final Function<? super L, ? extends LeftProjection<? extends U, R>> mapper) {
			requireNonNull(mapper, "mapper is null");
			if (this.either.isLeft()) {
				return (LeftProjection<U, R>) mapper.apply(this.either.getLeft());
			}
			return (LeftProjection<U, R>) this;
		}

		/**
		 * Maps the left value if the projected Either is a left.
		 *
		 * @param mapper a mapper which takes a left value and returns a value of type U
		 * @param <U> The new type of a Left value
		 * @return a new LeftProjection
		 */
		@SuppressWarnings("unchecked")
		public <U> LeftProjection<U, R> map(final Function<? super L, ? extends U> mapper) {
			requireNonNull(mapper, "mapper is null");
			if (this.either.isLeft()) {
				return this.either.mapLeft((Function<L, U>) mapper).left();
			}
			return (LeftProjection<U, R>) this;
		}

		/**
		 * Applies the given action to the value if the projected either is a left. Otherwise nothing happens.
		 *
		 * @param action An action which takes a left value
		 * @return this LeftProjection
		 */
		public LeftProjection<L, R> peek(final Consumer<? super L> action) {
			requireNonNull(action, "action is null");
			if (this.either.isLeft()) {
				action.accept(this.either.getLeft());
			}
			return this;
		}

		/**
		 * Transforms this {@code LeftProjection}.
		 *
		 * @param f a transformation
		 * @param <U> type of transformation result
		 * @return an instance of type {@code U}
		 * @throws NullPointerException if {@code f} is null
		 */
		public <U> U transform(final Function<? super LeftProjection<L, R>, ? extends U> f) {
			requireNonNull(f, "f is null");
			return f.apply(this);
		}

		@Override
		public boolean equals(final Object obj) {
			return obj == this || obj instanceof LeftProjection && Objects.equals(this.either, ((LeftProjection<?, ?>) obj).either);
		}

		@Override
		public int hashCode() {
			return this.either.hashCode();
		}

		@Override
		public String toString() {
			return "LeftProjection(" + this.either + ")";
		}
	}

	public static final class RightProjection<L, R> {

		private final Either<L, R> either;

		private RightProjection(final Either<L, R> either) {
			this.either = either;
		}

		public <L2, R2> RightProjection<L2, R2> bimap(final Function<? super L, ? extends L2> leftMapper, final Function<? super R, ? extends R2> rightMapper) {
			return this.either.<L2, R2> bimap(leftMapper, rightMapper).right();
		}

		/**
		 * Gets the Right value or throws.
		 *
		 * @return the right value, if the underlying {@code Either} is a Right
		 * @throws NoSuchElementException if the underlying {@code Either} of this {@code RightProjection} is a Left
		 */
		public R get() {
			if (this.either.isRight()) {
				return this.either.get();
			}
			throw new NoSuchElementException("RightProjection.get() on Left");
		}

		@SuppressWarnings("unchecked")
		public RightProjection<L, R> orElse(final RightProjection<? extends L, ? extends R> other) {
			requireNonNull(other, "other is null");
			return this.either.isRight() ? this : (RightProjection<L, R>) other;
		}

		@SuppressWarnings("unchecked")
		public RightProjection<L, R> orElse(final Supplier<? extends RightProjection<? extends L, ? extends R>> supplier) {
			requireNonNull(supplier, "supplier is null");
			return this.either.isRight() ? this : (RightProjection<L, R>) supplier.get();
		}

		/**
		 * Gets the Right value or an alternate value, if the projected Either is a left.
		 *
		 * @param other an alternative value
		 * @return the right value, if the underlying Either is a right or else {@code other}
		 * @throws NoSuchElementException if the underlying either of this RightProjection is a left
		 */
		public R orElse(final R other) {
			return this.either.orElse(other);
		}

		/**
		 * Gets the Right value or an alternate value, if the projected Either is a left.
		 *
		 * @param other a function which converts a Left value to an alternative Right value
		 * @return the right value, if the underlying Either is a right or else the alternative Right value provided by
		 *         {@code other} by applying the Left value.
		 */
		public R orElseGet(final Function<? super L, ? extends R> other) {
			requireNonNull(other, "other is null");
			return this.either.orElseGet(other);
		}

		/**
		 * Runs an action in the case this is a projection on a Left value.
		 *
		 * @param action an action which consumes a Left value
		 */
		public void orElseRun(final Consumer<? super L> action) {
			requireNonNull(action, "action is null");
			this.either.orElseRun(action);
		}

		/**
		 * Gets the Right value or throws, if the projected Either is a left.
		 *
		 * @param <X> a throwable type
		 * @param exceptionFunction a function which creates an exception based on a Left value
		 * @return the right value, if the underlying Either is a right or else throws the exception provided by
		 *         {@code exceptionFunction} by applying the Left value.
		 * @throws X if the projected Either is a left
		 */
		public <X extends Throwable> R getOrElseThrow(final Function<? super L, X> exceptionFunction) throws X {
			requireNonNull(exceptionFunction, "exceptionFunction is null");
			return this.either.orElseThrow(exceptionFunction);
		}

		/**
		 * Returns the underlying either of this projection.
		 *
		 * @return the underlying either
		 */
		public Either<L, R> toEither() {
			return this.either;
		}

		/**
		 * Returns {@code Some} value of type R if this is a right projection of a Right value and the predicate
		 * applies to the underlying value.
		 *
		 * @param predicate a predicate
		 * @return a new Option
		 */
		public Optional<RightProjection<L, R>> filter(final Predicate<? super R> predicate) {
			requireNonNull(predicate, "predicate is null");
			return this.either.isLeft() || predicate.test(this.either.get()) ? Optional.of(this) : Optional.empty();
		}

		/**
		 * FlatMaps this RightProjection.
		 *
		 * @param mapper a mapper
		 * @param <U> component type of the mapped right value
		 * @return this as {@code RightProjection<L, U>} if a Left is underlying, otherwise a the mapping result of the right value.
		 * @throws NullPointerException if {@code mapper} is null
		 */
		@SuppressWarnings("unchecked")
		public <U> RightProjection<L, U> flatMap(final Function<? super R, ? extends RightProjection<L, ? extends U>> mapper) {
			requireNonNull(mapper, "mapper is null");
			if (this.either.isRight()) {
				return (RightProjection<L, U>) mapper.apply(this.either.get());
			}
			return (RightProjection<L, U>) this;
		}

		/**
		 * Maps the right value if the projected Either is a right.
		 *
		 * @param mapper a mapper which takes a right value and returns a value of type U
		 * @param <U> The new type of a Right value
		 * @return a new RightProjection
		 */
		@SuppressWarnings("unchecked")
		public <U> RightProjection<L, U> map(final Function<? super R, ? extends U> mapper) {
			requireNonNull(mapper, "mapper is null");
			if (this.either.isRight()) {
				return this.either.map((Function<R, U>) mapper).right();
			}
			return (RightProjection<L, U>) this;
		}

		/**
		 * Applies the given action to the value if the projected either is a right. Otherwise nothing happens.
		 *
		 * @param action An action which takes a right value
		 * @return this {@code Either} instance
		 */
		public RightProjection<L, R> peek(final Consumer<? super R> action) {
			requireNonNull(action, "action is null");
			if (this.either.isRight()) {
				action.accept(this.either.get());
			}
			return this;
		}

		/**
		 * Transforms this {@code RightProjection}.
		 *
		 * @param f a transformation
		 * @param <U> type of transformation result
		 * @return an instance of type {@code U}
		 * @throws NullPointerException if {@code f} is null
		 */
		public <U> U transform(final Function<? super RightProjection<L, R>, ? extends U> f) {
			requireNonNull(f, "f is null");
			return f.apply(this);
		}

		@Override
		public boolean equals(final Object obj) {
			return obj == this || obj instanceof RightProjection && Objects.equals(this.either, ((RightProjection<?, ?>) obj).either);
		}

		@Override
		public int hashCode() {
			return this.either.hashCode();
		}

		@Override
		public String toString() {
			return "RightProjection(" + this.either + ")";
		}
	}

	private static final class Left<L, R> extends Either<L, R> {

		private static final long serialVersionUID = 1L;

		private final L value;

		Left(final L value) {
			this.value = value;
		}

		@Override
		protected R get() {
			throw new NoSuchElementException("get() on Left");
		}

		@Override
		public L getLeft() {
			return this.value;
		}

		@Override
		public boolean isLeft() {
			return true;
		}

		@Override
		public boolean isRight() {
			return false;
		}

		@Override
		public boolean equals(final Object obj) {
			return obj == this || obj instanceof Left && Objects.equals(this.value, ((Left<?, ?>) obj).value);
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(this.value);
		}

		@Override
		public String toString() {
			return "Left(" + this.value + ")";
		}
	}

	private static final class Right<L, R> extends Either<L, R> {

		private static final long serialVersionUID = 1L;

		private final R value;

		Right(final R value) {
			this.value = value;
		}

		@Override
		protected R get() {
			return this.value;
		}

		@Override
		public L getLeft() {
			throw new NoSuchElementException("getLeft() on Right");
		}

		@Override
		public boolean isLeft() {
			return false;
		}

		@Override
		public boolean isRight() {
			return true;
		}

		@Override
		public boolean equals(final Object obj) {
			return obj == this || obj instanceof Right && Objects.equals(this.value, ((Right<?, ?>) obj).value);
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(this.value);
		}

		@Override
		public String toString() {
			return "Right(" + this.value + ")";
		}
	}
}