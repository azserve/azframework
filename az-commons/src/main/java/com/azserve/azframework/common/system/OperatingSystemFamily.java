package com.azserve.azframework.common.system;

public enum OperatingSystemFamily {
	LINUX,
	WINDOWS,
	MAC_OS,
	OTHER
}