package com.azserve.azframework.common.lang;

import static java.util.Objects.requireNonNull;

import java.io.UnsupportedEncodingException;
import java.text.CharacterIterator;
import java.text.Normalizer;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.funct.ObjIntFunction;

/**
 * Serie di utilità per gestire oggetti di tipo {@code String}.
 * <p>
 * I metodi più comuni sono quelli di "conversione" tra stringhe {@code null} e
 * stringhe vuote. Per stringhe vuote s'intende stringhe di lunghezza
 * zero (empty strings) oppure stringhe che contengono solo spazi (blank strings).
 * </p>
 * Ad esempio:
 *
 * <pre>
 * isNullOrEmpty("") -> true
 * isNullOrBlank("") -> true
 * isNullOrEmpty("  ") -> false
 * isNullOrBlank("  ") -> true
 * </pre>
 *
 * <p>
 * Altri metodi permettono di eseguire le operazioni più comuni sulle
 * stringhe evitando che vengano scatenati {@code NullPointerException}.
 * </p>
 *
 * @since 15/06/2012
 */
public final class StringUtils {

	/**
	 * Nome del set di caratteri ASCII.
	 */
	@NonNull
	public static final String ASCII_CHARSET = "ascii";
	/**
	 * Carattere &amp;nbsp; (non breaking space).
	 */
	public static final char NBSP = 0x00A0;

	private StringUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Restituisce una stringa valorizzata oppure {@code null}; elimina lo stato di stringa vuota.
	 * <br>
	 * La stringa viene considerata vuota solo se la sua lunghezza è 0.
	 *
	 * <pre>
	 * emptyToNull(null) -> null
	 * emptyToNull("") -> null
	 * emptyToNull("  ") -> "  "
	 * emptyToNull("ciao") -> "ciao"
	 * </pre>
	 *
	 * @param s la stringa da analizzare.
	 * @return {@code null} se la stringa è {@code null} o
	 *         vuota, altrimenti la stringa stessa.
	 *
	 * @since 15/06/2012
	 */
	public static String emptyToNull(final @Nullable String s) {
		return "".equals(s) ? null : s;
	}

	/**
	 * Verifica se una stringa è {@code null} oppure vuota.
	 * <br>
	 * La stringa viene considerata vuota solo se la sua lunghezza è 0.
	 *
	 * <pre>
	 * isNullOrEmpty(null) -> true
	 * isNullOrEmpty("") -> true
	 * isNullOrEmpty("  ") -> false
	 * isNullOrEmpty("ciao") -> false
	 * </pre>
	 *
	 * @param cs stringa da analizzare.
	 * @return {@code true} se la stringa è {@code null} o
	 *         vuota, {@code false} se la stringa è valorizzata.
	 *
	 * @see #isNullOrBlank(String)
	 * @since 15/06/2012
	 */
	public static boolean isNullOrEmpty(final @Nullable CharSequence cs) {
		return cs == null || cs.length() == 0;
	}

	/**
	 * Verifica se una stringa non è {@code null} e nemmeno vuota.
	 * <br>
	 * La stringa viene considerata vuota solo se la sua lunghezza è 0.
	 *
	 * <pre>
	 * isNotNullNorEmpty(null) -> false
	 * isNotNullNorEmpty("") -> false
	 * isNotNullNorEmpty("  ") -> true
	 * isNotNullNorEmpty("ciao") -> true
	 * </pre>
	 *
	 * Questo metodo negato torna utile con le espressioni lambda:
	 *
	 * <pre>
	 * stream.filter(StringUtils::isNotNullNorEmpty)
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @return {@code false} se la stringa è {@code null} o
	 *         vuota, {@code true} se la stringa è valorizzata.
	 *
	 * @see #isNotNullNorBlank(String)
	 * @since 12/10/2016
	 */
	public static boolean isNotNullNorEmpty(final @Nullable String s) {
		return s != null && !s.isEmpty();
	}

	/**
	 * Se una stringa è {@code null} restituisce una stringa vuota, altrimenti la stringa stessa.
	 *
	 * <pre>
	 * nullToEmpty(null) -> ""
	 * nullToEmpty("") -> ""
	 * nullToEmpty("ciao") -> "ciao"
	 * </pre>
	 *
	 * java 8:
	 *
	 * <pre>
	 * Optional.ofNullable(s).orElse("");
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @return {@code ""} se la stringa analizzata è {@code null}.
	 *
	 * @since 15/06/2012
	 */
	public static @NonNull String nullToEmpty(final @Nullable String s) {
		return s == null ? "" : s;
	}

	/**
	 * Verifica se una stringa è {@code null} oppure vuota.
	 * <br>
	 * Una stringa di lunghezza 0 oppure composta da soli spazi viene considerata vuota.
	 *
	 * <pre>
	 * isNullOrBlank(null) -> true
	 * isNullOrBlank("") -> true
	 * isNullOrBlank("  ") -> true
	 * isNullOrBlank("ciao") -> false
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @return {@code true} se la stringa è {@code null} o
	 *         vuota, {@code false} se la stringa è valorizzata.
	 *
	 * @see #isNullOrEmpty(CharSequence)
	 * @since 15/06/2012
	 */
	public static boolean isNullOrBlank(final @Nullable String s) {
		return s == null || s.trim().isEmpty();
	}

	/**
	 * Verifica se una stringa non è {@code null} e nemmeno vuota.
	 * <br>
	 * Una stringa di lunghezza 0 oppure composta da soli spazi viene considerata vuota.
	 *
	 * <pre>
	 * isNotNullNorBlank(null) -> false
	 * isNotNullNorBlank("") -> false
	 * isNotNullNorBlank("  ") -> false
	 * isNotNullNorBlank("ciao") -> true
	 * </pre>
	 *
	 * Questo metodo negato torna utile con le espressioni lambda:
	 *
	 * <pre>
	 * stream.filter(StringUtils::isNotNullNorBlank)
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @return {@code false} se la stringa è {@code null} o
	 *         vuota, {@code true} se la stringa è valorizzata.
	 *
	 * @see #isNotNullNorEmpty(String)
	 * @since 12/10/2016
	 */
	public static boolean isNotNullNorBlank(final @Nullable String s) {
		return s != null && !s.trim().isEmpty();
	}

	/**
	 * Restituisce una stringa valorizzata oppure {@code null};
	 * elimina lo stato di stringa vuota.
	 * <br>
	 * Una stringa di lunghezza 0 oppure composta da soli spazi
	 * viene considerata vuota.
	 * <br/><br/>
	 * <b>Attenzione</b> a differenza del metodo {@linkplain #trimToNull(String)} questo
	 * restituisce la stringa iniziale se non è vuota dopo aver effettuato il trim
	 * (vedi ultimo esempio di chiamata).
	 *
	 * <pre>
	 * blankToNull(null) -> null
	 * blankToNull("") -> null
	 * blankToNull("  ") -> null
	 * blankToNull("ciao") -> "ciao"
	 * blankToNull("ciao  ") -> "ciao  "
	 * </pre>
	 *
	 * @param s la stringa da analizzare.
	 * @return la stringa analizzata.
	 *
	 * @since 15/06/2012
	 * @see #trimToNull(String)
	 */
	public static String blankToNull(final @Nullable String s) {
		return s == null || s.trim().isEmpty() ? null : s;
	}

	/**
	 * Verifica se una stringa è {@code null} o vuota. In tal caso
	 * viene generata un'eccezione, altrimenti viene restituita
	 * la stringa stessa.
	 * <p>
	 * Questo è un metodo di utilità per la verifica dei parametri
	 * di un metodo basato su {@link Objects#requireNonNull(Object, String)}.
	 * </p>
	 *
	 * @param s stringa da verificare.
	 * @param message messaggio dell'eccezione.
	 *
	 * @return la stringa stessa.
	 * @throws StringNotValorizedException se la stringa
	 *             è {@code null} oppure vuota (lunghezza zero).
	 *
	 * @since 17/09/2013
	 */
	public static @NonNull String requireNonEmpty(final @Nullable String s, final @NonNull String message) {
		if (s == null || s.isEmpty()) {
			throw new StringNotValorizedException(message);
		}
		return s;
	}

	/**
	 * Restituisce la stringa senza spazi vuoti all'inizio e alla fine.
	 * <br/>
	 * Se la stringa è {@code null} restituisce {@code null}.
	 *
	 * <pre>
	 * trimSafe(null) -> null
	 * trimSafe(" ") -> ""
	 * trimSafe("ciao") -> "ciao"
	 * trimSafe("ciao   ") -> "ciao"
	 * </pre>
	 *
	 * <p>
	 * Per evitare che venga restituita una stringa {@code null} utilizzare
	 * il metodo {@link #trimToEmpty(String)}.
	 * </p>
	 *
	 * @param s stringa da analizzare.
	 *
	 * @since 15/06/2012
	 */
	public static String trimSafe(final @Nullable String s) {
		return s == null ? null : s.trim();
	}

	/**
	 * Restituisce la stringa senza spazi vuoti all'inizio e alla fine.
	 * <br/>
	 * Se la stringa è {@code null} o vuota restituisce {@code null}.
	 * <br/><br/>
	 * <b>Attenzione</b> a differenza del metodo {@linkplain #blankToNull(String)} questo
	 * restituisce sempre una stringa senza spazi iniziali e finali
	 * (vedi ultimo esempio di chiamata).
	 *
	 * <pre>
	 * trimToNull(null) -> null
	 * trimToNull(" ") -> null
	 * trimToNull("ciao") -> "ciao"
	 * trimToNull("ciao   ") -> "ciao"
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 *
	 * @since 26/05/2015
	 * @see #blankToNull(String)
	 */
	public static String trimToNull(final @Nullable String s) {
		if (s == null) {
			return null;
		}
		final String trimmed = s.trim();
		return trimmed.isEmpty() ? null : trimmed;
	}

	/**
	 * Restituisce la stringa senza spazi vuoti all'inizio e alla fine.
	 * <br/>
	 * Se la stringa è {@code null} restituisce una stringa vuota.
	 *
	 * <pre>
	 * trimToEmpty(null) -> ""
	 * trimToEmpty(" ") -> ""
	 * trimToEmpty("ciao") -> "ciao"
	 * trimToEmpty("ciao   ") -> "ciao"
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 *
	 * @since 17/09/2013
	 */
	public static @NonNull String trimToEmpty(final @Nullable String s) {
		return s == null ? "" : s.trim();
	}

	/**
	 * Restituisce la stringa senza spazi vuoti all'inizio.
	 * <br/>
	 * Vengono presi in considerazione i caratteri
	 * con codice minore o uguale a <code>'&#92;u0020'</code>
	 * come nel metodo {@link String#trim()}.
	 *
	 * <pre>
	 * trimLeft(" ") -> ""
	 * trimLeft("   ciao") -> "ciao"
	 * trimLeft("ciao   ") -> "ciao   "
	 * </pre>
	 *
	 * @param s stringa da analizzare (obbligatoria).
	 *
	 * @since 03/07/2014
	 */
	public static @NonNull String trimLeft(final @NonNull String s) {
		final int len = s.length();
		if (len > 0) {
			int i = 0;
			while (true) {
				if (s.charAt(i) > ' ') {
					return s.substring(i);
				} else if (++i == len) {
					return "";
				}
			}
		}
		return s;
	}

	/**
	 * Restituisce la stringa senza spazi vuoti alla fine.
	 * <br/>
	 * Vengono presi in considerazione i caratteri
	 * con codice minore o uguale a <code>'&#92;u0020'</code>
	 * come nel metodo {@link String#trim()}.
	 *
	 * <pre>
	 * trimRight(" ") -> ""
	 * trimRight("   ciao") -> "   ciao"
	 * trimRight("ciao   ") -> "ciao"
	 * </pre>
	 *
	 * @param s stringa da analizzare (obbligatoria).
	 *
	 * @since 03/07/2014
	 */
	public static @NonNull String trimRight(final @NonNull String s) {
		final int len = s.length();
		if (len > 0) {
			int i = len - 1;
			while (true) {
				if (s.charAt(i) > ' ') {
					return s.substring(0, i + 1);
				} else if (--i == 0) {
					return "";
				}
			}
		}
		return s;
	}

	/**
	 * Restituisce la stringa rimuovendo gli spazi vuoti.
	 * Se la stringa è {@code null} o vuota restituisce
	 * la stringa stessa.
	 * <br/>
	 * I caratteri considerati spazi sono quelli
	 * definiti dal metodo {@linkplain Character#isWhitespace(char)} più
	 * il carattere {@code &nbsp;}.
	 *
	 * @param s stringa da analizzare.
	 *
	 * @since 20/09/2013
	 */
	public static String clearWhitespaces(final @Nullable String s) {
		if (s == null || s.isEmpty()) {
			return s;
		}

		final int length = s.length();

		boolean whitespaces = false;
		for (int i = 0; i < length; i++) {
			final char c = s.charAt(i);
			if (Character.isWhitespace(c) || c == NBSP) {
				whitespaces = true;
				break;
			}
		}

		if (!whitespaces) {
			return s;
		}

		final char[] tmp = new char[length];
		int j = 0;
		for (int i = 0; i < length; i++) {
			final char c = s.charAt(i);
			if (!Character.isWhitespace(c) && c != NBSP) {
				tmp[j++] = c;
			}
		}
		return new String(tmp, 0, j);
	}

	/**
	 * Se una stringa è {@code null} restituisce una stringa
	 * sostituiva, altrimenti la stringa stessa.
	 *
	 * <pre>
	 * ifNull(null, "ciao") -> "ciao"
	 * ifNull("", "ciao") -> ""
	 * ifNull("ciao", null) -> "ciao"
	 * </pre>
	 *
	 * java 8:
	 *
	 * <pre>
	 * Optional.ofNullable(s).orElse(replacement);
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @param replacement stringa sostitutiva.
	 * @return se {@code s == null} la stringa sostituiva,
	 *         altrimenti la stringa stessa.
	 *
	 * @since 27/09/2013
	 */
	public static String ifNull(final @Nullable String s, final @Nullable String replacement) {
		return s == null ? replacement : s;
	}

	/**
	 * Date due stringhe s1 ed s2:
	 * <ul>
	 * <li>Se s1 o s2 è {@code null} restituisce una
	 * stringa sostitutiva.</li>
	 * <li>Se entrambi le stringhe non sono {@code null} restituisce
	 * la concatenazione di s1 con s2.</li>
	 * </ul>
	 *
	 * <pre>
	 * ifNull("Nome: ", "Filippo", "") -> "Nome: Filippo"
	 * ifNull("Nome: ", null, "") -> ""
	 * ifNull(null, "Filippo", "") -> ""
	 * </pre>
	 *
	 * @param s1 prima stringa da analizzare.
	 * @param s2 seconda stringa da analizzare.
	 * @param replacement stringa sostitutiva.
	 * @return se {@code s1 == null || s2 == null } : {@code replacement},
	 *         altrimenti : {@code s1 + s2}.
	 *
	 * @since 16/12/2013
	 */
	public static String ifNull(final @Nullable String s1, final @Nullable String s2, final @Nullable String replacement) {
		if (s1 == null || s2 == null) {
			return replacement;
		}
		return s1 + s2;
	}

	/**
	 * Se una stringa è {@code null} o vuota restituisce una stringa
	 * sostituiva, altrimenti la stringa stessa.
	 * <br>
	 * Una stringa di lunghezza 0 viene considerata vuota.
	 *
	 * <pre>
	 * ifNullOrEmpty(null, "ciao") -> "ciao"
	 * ifNullOrEmpty("", "ciao") -> "ciao"
	 * ifNullOrEmpty("pippo", "ciao") -> "pippo"
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @param replacement stringa sostitutiva.
	 * @return se {@code s == null} oppure vuota
	 *         la stringa sostituiva,
	 *         altrimenti la stringa stessa.
	 *
	 * @since 18/12/2013
	 */
	public static String ifNullOrEmpty(final @Nullable String s, final @Nullable String replacement) {
		return isNullOrEmpty(s) ? replacement : s;
	}

	/**
	 * Se una stringa è {@code null} o vuota restituisce una stringa
	 * sostituiva, altrimenti la stringa stessa.
	 * <br>
	 * Una stringa di lunghezza 0 oppure composta da soli
	 * spazi viene considerata vuota.
	 *
	 * <pre>
	 * ifNullOrBlank(null, "ciao") -> "ciao"
	 * ifNullOrBlank("  ", "ciao") -> "ciao"
	 * ifNullOrBlank("pippo", "ciao") -> "pippo"
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @param replacement stringa sostitutiva.pa
	 * @return se {@code s == null} oppure vuota
	 *         la stringa sostituiva,
	 *         altrimenti la stringa stessa.
	 *
	 * @since 18/12/2013
	 */
	public static String ifNullOrBlank(final @Nullable String s, final @Nullable String replacement) {
		return isNullOrBlank(s) ? replacement : s;
	}

	/**
	 * Restituisce una stringa risultato dell'inserimento
	 * di una stringa all'interno di un'altra.
	 *
	 * <pre>
	 * insert("123456789", "AAA", 0) -> "AAA123456789"
	 * insert("123456789", "AAA", 3) -> "123AAA456789"
	 * insert("123456789", "AAA", 9) -> "123456789AAA"
	 * </pre>
	 *
	 * @param s stringa sorgente.
	 * @param strToInsert stringa da inserire.
	 * @param pos posizione di inserimento.
	 */
	public static @NonNull String insert(final @NonNull String s, final @NonNull String strToInsert, final int pos) {
		Objects.requireNonNull(strToInsert);
		final int sLen = s.length();
		if (pos > sLen) {
			throw new StringIndexOutOfBoundsException(pos + " > " + s.length());
		}
		if (pos == 0) {
			return strToInsert + s;
		}
		if (pos == sLen) {
			return s.substring(0, pos) + strToInsert;
		}
		return s.substring(0, pos) + strToInsert + s.substring(pos, sLen);
	}

	/**
	 * Verifica se una stringa è composta dal solo
	 * carattere indicato.
	 *
	 * @param s stringa da verificare (accetta {@code null}.
	 * @param c carattere contenuto.
	 */
	public static boolean equals(final @Nullable String s, final char c) {
		return s != null && s.length() == 1 && s.charAt(0) == c;
	}

	/**
	 * Confronta due stringhe ignorando {@code null}.<br>
	 * Le stringhe {@code null} vengono considerate vuote.
	 *
	 * <pre>
	 * equalsIgnoreNull(null, null) -> true
	 * equalsIgnoreNull(null, "") -> true
	 * equalsIgnoreNull("", null) -> true
	 * equalsIgnoreNull(null, "ciao") -> false
	 * equalsIgnoreNull("ciao", "ciao") -> true
	 * equalsIgnoreNull("Ciao", "ciao") -> false
	 * equalsIgnoreNull("ciao", "ciao ") -> false
	 * </pre>
	 *
	 * @param s1 prima stringa.
	 * @param s2 seconda stringa.
	 *
	 * @since 10/07/2012
	 */
	public static boolean equalsIgnoreNull(final @Nullable String s1, final @Nullable String s2) {
		return s1 == s2 || nullToEmpty(s1).equals(nullToEmpty(s2));
	}

	/**
	 * Confronta due stringhe ignorando {@code null} e lettere maiuscole/minuscole.
	 * <br>
	 * Le stringhe {@code null} vengono considerate vuote.
	 *
	 * <pre>
	 * equalsIgnoreNullCase(null, null) -> true
	 * equalsIgnoreNullCase(null, "") -> true
	 * equalsIgnoreNullCase("", null) -> true
	 * equalsIgnoreNullCase(null, "ciao") -> false
	 * equalsIgnoreNullCase("ciao", "ciao") -> true
	 * equalsIgnoreNullCase("Ciao", "ciao") -> true
	 * equalsIgnoreNullCase("ciao", "ciao ") -> false
	 * </pre>
	 *
	 * @param s1 prima stringa.
	 * @param s2 seconda stringa.
	 * @return {@code true} se entrambi le stringhe sono vuote
	 *         (considerando {@code null} come stringa vuota)
	 *         oppure hanno la stessa lunghezza e i caratteri
	 *         corrispondenti sono uguali ignorando la
	 *         differenza tra maiuscole e minuscole.
	 */
	public static boolean equalsIgnoreNullCase(final @Nullable String s1, final @Nullable String s2) {
		return s1 == s2 || nullToEmpty(s1).equalsIgnoreCase(nullToEmpty(s2));
	}

	/**
	 * Verifica se una stringa supera una certa lunghezza.
	 * In tal caso viene restituita la stringa troncata più
	 * uno spazio e i 3 punti di sospensione.
	 * <p>
	 * La stringa restituita sarà lunga al massimo
	 * come la lunghezza massima specificata.
	 * </p>
	 * <p>
	 * Se la stringa è {@code null}, vuota o meno lunga
	 * del previsto viene restituita essa stessa.
	 * </p>
	 *
	 * <pre>
	 * abbreviate(null, 20) -> null
	 * abbreviate("", 20) -> ""
	 * abbreviate("ciao", 20) -> "ciao"
	 * abbreviate("Ascesso amebico cerebrale", 20) -> "Ascesso amebico cereb ..."
	 * </pre>
	 *
	 * @param source stringa da analizzare.
	 * @param maxLength lunghezza massima.
	 *
	 * @since 23/09/2013
	 */
	public static String abbreviate(final @Nullable String source, final int maxLength) {
		return abbreviate(source, maxLength, " ...");
	}

	/**
	 * Verifica se una stringa supera una certa lunghezza.
	 * In tal caso viene restituita la stringa troncata più
	 * un suffisso che può essere composto ad esempio dai
	 * 3 punti di sospensione.
	 * <p>
	 * La stringa restituita sarà lunga al massimo
	 * come la lunghezza massima specificata.
	 * </p>
	 * <p>
	 * Se la stringa è {@code null}, vuota o meno lunga
	 * del previsto viene restituita essa stessa.
	 * </p>
	 *
	 * <pre>
	 * abbreviate(null, 20, "..") -> null
	 * abbreviate("", 3, "..") -> ""
	 * abbreviate("ciao", 3, "..") -> "c.."
	 * abbreviate("Ascesso amebico cerebrale", 20, "..") -> "Ascesso amebico cerebra.."
	 * abbreviate("ciao", 1, "..") -> IndexOutOfBoundsException
	 * </pre>
	 *
	 * @param source stringa da analizzare.
	 * @param maxLength lunghezza massima.
	 * @param suffix suffiso da appendere in coda se la
	 *            stringa supera la lunghezza specificata.
	 * @throws IndexOutOfBoundsException se la lunghezza del suffisso
	 *             supera quella massima specificata.
	 *
	 * @since 23/09/2013
	 */
	public static String abbreviate(final @Nullable String source, final int maxLength, final @NonNull String suffix) {
		final int suffixLength = suffix.length();
		if (maxLength < suffixLength) {
			throw new IndexOutOfBoundsException("Suffix length greather than max length: " + suffixLength + " > " + maxLength);
		}
		if (source == null || source.isEmpty() || source.length() <= maxLength) {
			return source;
		}
		return source.substring(0, maxLength - suffixLength) + suffix;
	}

	/**
	 * Restituisce una stringa risultato della "rimozione"
	 * di una porzione di una stringa data.
	 * Effettua l'operazione inversa di {@code substring}.
	 *
	 * <pre>
	 * remove("123456789", 0, 9) -> ""
	 * remove("123456789", 6, 9) -> "123456"
	 * remove("123456789", 0, 3) -> "456789"
	 * remove("123456789", 3, 6) -> "123789"
	 * remove("123456789", 0, 0) -> "123456789"
	 * </pre>
	 *
	 * @param s stringa sorgente.
	 * @param from posizione iniziale.
	 * @param to posizione finale.
	 *
	 * @since 22/11/2013
	 */
	public static @NonNull String remove(final @NonNull String s, final int from, final int to) {
		final int sLen = s.length();
		if (from == 0) {
			return sLen == to ? "" : s.substring(to, sLen);
		}
		if (sLen == to) {
			return s.substring(0, from);
		}
		return s.substring(0, from) + s.substring(to, sLen);
	}

	/**
	 * Restituisce una stringa risultato della sostituzione
	 * di una porzione di una stringa data con un'altra stringa.
	 *
	 * <pre>
	 * replace("123456789", "**", 0, 9) -> "**"
	 * replace("123456789", "**", 6, 9) -> "123456**"
	 * replace("123456789", "**", 0, 3) -> "**456789"
	 * replace("123456789", "**", 3, 6) -> "123**789"
	 * replace("123456789", "**", 0, 0) -> "123456789"
	 * </pre>
	 *
	 * @param s stringa sorgente.
	 * @param replacement stringa sostitutiva.
	 * @param from posizione iniziale.
	 * @param to posizione finale.
	 *
	 * @throws IllegalArgumentException se {@code from > to}.
	 * @throws StringIndexOutOfBoundsException
	 *             se {@code from < 0 || to > s.length()}.
	 *
	 * @see #toLowerCase(String, int, int)
	 * @see #toUpperCase(String, int, int)
	 *
	 * @since 21/01/2014
	 */
	public static @NonNull String replace(final @NonNull String s, final @NonNull String replacement, final int from, final int to) {
		if (from == to) {
			return s;
		}
		final int sLen = s.length();
		if (from > to) {
			throw new IllegalArgumentException("Invalid range: " + from + " > " + to);
		}
		// check not null
		Objects.requireNonNull(replacement, "Replacement string required");
		if (from == 0) {
			return sLen == to ? replacement : replacement + s.substring(to, sLen);
		}
		if (sLen == to) {
			return s.substring(0, from) + replacement;
		}
		return s.substring(0, from) + replacement + s.substring(to, sLen);
	}

	public static String replace(final @NonNull String source, final @NonNull Pattern pattern, final @NonNull ObjIntFunction<String, String> occurrenceIxToString) {
		requireNonNull(occurrenceIxToString);
		final Matcher matcher = pattern.matcher(requireNonNull(source));
		final StringBuilder sb = new StringBuilder();
		int i = 0;
		int occ = 0;
		while (matcher.find()) {
			sb.append(source.substring(i, matcher.start()));
			sb.append(occurrenceIxToString.apply(matcher.group(0), occ++));
			i = matcher.end();
		}
		sb.append(source.substring(i, source.length()));
		return sb.toString();
	}

	public static String replace(final @NonNull CharSequence source, final @NonNull CharSequence target, final @NonNull ObjIntFunction<String, String> occurrenceIxToString) {
		return replace(source.toString(), Pattern.compile(target.toString(), Pattern.LITERAL), occurrenceIxToString);
	}

	/**
	 * Rimuove un determinato carattere a inizio stringa.
	 *
	 * @param s stringa da analizzare.
	 * @param c carattere da rimuovere.
	 * @return la stringa stessa se non contiene
	 *         nessun carattere {@code c} in testa,
	 *         altrimenti una nuova stringa senza
	 *         il suddetto carattere in testa.
	 */
	public static String removeLeadingChars(final @NonNull String s, final char c) {
		if (s.isEmpty()) {
			return s;
		}
		int from = 0;
		int i = 0;
		while (i < s.length() && s.charAt(i++) == c) {
			from++;
		}
		return s.substring(from);
	}

	/**
	 * Rimuove un determinato carattere a fine stringa.
	 *
	 * @param s stringa da analizzare.
	 * @param c carattere da rimuovere.
	 * @return la stringa stessa se non contiene
	 *         nessun carattere {@code c} in coda,
	 *         altrimenti una nuova stringa senza
	 *         il suddetto carattere in coda.
	 */
	public static String removeTrailingChars(final @NonNull String s, final char c) {
		if (s.isEmpty()) {
			return s;
		}
		int to = s.length();
		int i = to - 1;
		while (i >= 0 && s.charAt(i--) == c) {
			to--;
		}
		return s.substring(0, to);
	}

	/**
	 * Restituisce una stringa risultato della "minuscolizzazione"
	 * di una porzione di una stringa data.
	 *
	 * <pre>
	 * toLowerCase("ABCDEFGHI", 0, 9) -> "abcdefghi"
	 * toLowerCase("ABCDEFGHI", 6, 9) -> "ABCDEFghi"
	 * toLowerCase("ABCDEFGHI", 0, 3) -> "abcDEFGHI"
	 * toLowerCase("ABCDEFGHI", 3, 6) -> "ABCdefGHI"
	 * toLowerCase("ABCDEFGHI", 0, 0) -> "ABCDEFGHI"
	 * toLowerCase("abcDEFGHI", 0, 3) -> "abcDEFGHI"
	 * </pre>
	 *
	 * @param s stringa sorgente.
	 * @param from posizione iniziale.
	 * @param to posizione finale.
	 *
	 * @throws IllegalArgumentException se {@code from > to}.
	 * @throws StringIndexOutOfBoundsException
	 *             se {@code from < 0 || to > s.length()}.
	 *
	 * @see #replace(String, String, int, int)
	 * @see #toUpperCase(String, int, int)
	 *
	 * @since 21/01/2014
	 */
	public static @NonNull String toLowerCase(final @NonNull String s, final int from, final int to) {
		if (from == to) {
			return s;
		}
		final int sLen = s.length();
		if (from > to) {
			throw new IllegalArgumentException("Invalid range: " + from + " > " + to);
		}
		if (from == 0) {
			return sLen == to ? s.toLowerCase() : s.substring(from, to).toLowerCase() + s.substring(to, sLen);
		}
		if (sLen == to) {
			return s.substring(0, from) + s.substring(from, to).toLowerCase();
		}
		return s.substring(0, from) + s.substring(from, to).toLowerCase() + s.substring(to, sLen);
	}

	/**
	 * Restituisce una stringa risultato della "maiuscolizzazione"
	 * di una porzione di una stringa data.
	 *
	 * <pre>
	 * toUpperCase("abcdefghi", 0, 9) -> "ABCDEFGHI"
	 * toUpperCase("abcdefghi", 6, 9) -> "abcdefGHI"
	 * toUpperCase("abcdefghi", 0, 3) -> "ABCdefghi"
	 * toUpperCase("abcdefghi", 3, 6) -> "abcDEFghi"
	 * toUpperCase("abcdefghi", 0, 0) -> "abcdefghi"
	 * toUpperCase("ABCdefghi", 0, 3) -> "ABCdefghi"
	 * </pre>
	 *
	 * @param s stringa sorgente.
	 * @param from posizione iniziale.
	 * @param to posizione finale.
	 *
	 * @throws IllegalArgumentException se {@code from > to}.
	 * @throws StringIndexOutOfBoundsException
	 *             se {@code from < 0 || to > s.length()}.
	 *
	 * @see #replace(String, String, int, int)
	 * @see #toLowerCase(String, int, int)
	 *
	 * @since 21/01/2014
	 */
	public static @NonNull String toUpperCase(final @NonNull String s, final int from, final int to) {
		if (from == to) {
			return s;
		}
		final int sLen = s.length();
		if (from > to) {
			throw new IllegalArgumentException("Invalid range: " + from + " > " + to);
		}
		if (from == 0) {
			return sLen == to ? s.toUpperCase() : s.substring(from, to).toUpperCase() + s.substring(to, sLen);
		}
		if (sLen == to) {
			return s.substring(0, from) + s.substring(from, to).toUpperCase();
		}
		return s.substring(0, from) + s.substring(from, to).toUpperCase() + s.substring(to, sLen);
	}

	/**
	 * Restituisce una porzione di stringa a partire
	 * dall'indice dell'n-esima occorrenza della
	 * sottostringa data (a partire da 0).
	 * <br/>
	 * Se non vengono trovate abbastanza occorrenze
	 * all'interno della stringa viene restituita
	 * una stringa vuota.
	 *
	 * <pre>
	 * substringIndex("ciao a tutti", " a ", 0) -> "tutti"
	 * substringIndex("ciao a tutti", " a ", 1) -> ""
	 * substringIndex("ciao a tutti", "xx", 0) -> ""
	 * substringIndex("abababa", "ab", 0) -> "ababa"
	 * substringIndex("abababa", "ab", 1) -> "aba"
	 * substringIndex("abababa", "ab", 2) -> "a"
	 * substringIndex("abababa", "ab", 3) -> ""
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @param substr sottostringa.
	 * @param ix indice dell'occorrenza della
	 *            sottostringa.
	 *
	 * @throws IllegalArgumentException se {@code ix} è negativo.
	 */
	public static String substringIndex(final @NonNull String s, final @NonNull String substr, final int ix) {
		if (ix < 0) {
			throw new IllegalArgumentException("Index must be positive, ix = " + ix);
		}
		int c = -1;
		int tmpIx = 0;
		while (ix > c++) {
			tmpIx = s.indexOf(substr, tmpIx);
			if (tmpIx < 0) {
				return "";
			}
			tmpIx += substr.length();
		}
		return s.substring(tmpIx);
	}

	/**
	 * Cerca e restituisce la sottostringa della stringa data
	 * delimitata da 2 stringhe.
	 * Se la sottostringa non viene trovata restituisce {@code null}.
	 *
	 * @param s stringa da cui estrarre la sottostringa.
	 * @param startingWith primo delimitatore.
	 * @param endingWith secondo delimitatore.
	 *
	 * @since 07/11/2016
	 */
	public static String substringDelimited(final @NonNull String s, final @NonNull String startingWith, final @NonNull String endingWith) {
		int startIx = s.indexOf(startingWith);
		if (startIx < 0) {
			return null;
		}
		startIx += startingWith.length();
		final int endIx = s.indexOf(endingWith, startIx);
		if (endIx < 0 || startIx > endIx) {
			return null;
		}
		return s.substring(startIx, endIx);
	}

	/**
	 * Restituisce i primi n caratteri di una stringa. Se n supera
	 * la lunghezza della stringa restituisce la stringa stessa.
	 * <br>
	 * Le stringhe {@code null} vengono considerate vuote.
	 *
	 * <pre>
	 * left("ciao", 2) -> "ci"
	 * left("ciao", 7) -> "ciao"
	 * left(null, 4) -> ""
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @param n numero di caratteri da estrarre.
	 * @return una sottostringa di {@code s} di lunghezza {@code n} oppure {@code s}.
	 */
	public static @NonNull String left(final @Nullable String s, final int n) {
		final String strNotNull = nullToEmpty(s);
		if (strNotNull.length() > n) {
			return strNotNull.substring(0, n);
		}
		return strNotNull;
	}

	/**
	 * Restituisce gli ultimi n caratteri di una stringa. Se n supera
	 * la lunghezza della stringa restituisce la stringa stessa.
	 * <br>
	 * Le stringhe {@code null} vengono considerate vuote.
	 *
	 * <pre>
	 * right("ciao", 2) -> "ao"
	 * right("ciao", 7) -> "ciao"
	 * right(null, 4) -> ""
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @param n numero di caratteri da estrarre.
	 * @return una sottostringa di {@code s} di lunghezza {@code n} oppure {@code s}.
	 */
	public static @NonNull String right(final @Nullable String s, final int n) {
		final String strNotNull = nullToEmpty(s);
		if (strNotNull.length() > n) {
			return strNotNull.substring(strNotNull.length() - n);
		}
		return strNotNull;
	}

	/**
	 * Allinea un stringa a destra effettuando il padding a sinistra con una
	 * serie di caratteri in modo che raggiunga la lunghezza impostata.
	 * <br>
	 * Se la lunghezza della stringa supera la lunghezza n desiderata
	 * vengono restituiti gli ultimi n caratteri della stringa stessa.
	 *
	 * <pre>
	 * padLeft("ciao", 6, '*') -> '**ciao'
	 * padLeft("ciao", 2, '*') -> 'ciao'
	 * padLeft("", 3, '*') -> '***'
	 * </pre>
	 *
	 * @param s stringa da analizzare (obbligatoria).
	 * @param length lunghezza della stringa restituita.
	 * @param paddingChar carattere di riempimento.
	 * @return una nuova stringa di lunghezza {@code length}.
	 */
	public static @NonNull String padLeft(final @NonNull String s, final int length, final char paddingChar) {
		final int sLength = s.length();

		final int diff = length - sLength;
		if (diff == 0) {
			return s;
		}
		if (diff < 0) {
			return s.substring(-diff, sLength);
		}
		return generate(paddingChar, diff) + s;
	}

	/**
	 * Allinea un stringa a sinistra effettuando il padding a destra con una
	 * serie di caratteri in modo che raggiunga la lunghezza impostata.
	 * <br>
	 * Se la lunghezza della stringa supera la lunghezza n desiderata
	 * vengono restituiti i primi n caratteri della stringa stessa.
	 *
	 * <pre>
	 * padRight("ciao", 6, '*') -> 'ciao**'
	 * padRight("ciao", 2, '*') -> 'ciao'
	 * padRight("", 3, '*') -> '***'
	 * </pre>
	 *
	 * @param s stringa da analizzare (obbligatoria).
	 * @param length lunghezza della stringa restituita.
	 * @param paddingChar carattere di riempimento.
	 * @return una nuova stringa di lunghezza {@code length}.
	 */
	public static @NonNull String padRight(final @NonNull String s, final int length, final char paddingChar) {
		final int diff = length - s.length();
		if (diff == 0) {
			return s;
		}
		if (diff < 0) {
			return s.substring(0, length);
		}
		return s + generate(paddingChar, diff);
	}

	/**
	 * Controlla se una stringa segue un'espressione
	 * regolare accettando stringhe {@code null}.
	 *
	 * <pre>
	 * checkRegex(null, null) -> true
	 * checkRegex("uno", "(uno|due|tre)") -> true
	 * checkRegex(null, "uno") -> false
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @param expr espressione regolare.
	 *
	 * @since 25/07/2012
	 */
	public static boolean checkRegex(final @Nullable CharSequence s, final @Nullable String expr) {
		return s == null && expr == null || s != null && expr != null && Pattern.matches(expr, s);
	}

	/**
	 * Conta il numero di corrispondenze di una espressione
	 * regolare in una stringa.
	 *
	 * @param source stringa da analizzare (obbligatoria).
	 * @param regexPattern pattern di un'espressione regolare
	 *            (obbligatorio).
	 *
	 * @since 17/09/2013
	 */
	public static int countMatches(final @NonNull CharSequence source, final @NonNull Pattern regexPattern) {
		final Matcher matcher = regexPattern.matcher(source);
		int count = 0;
		while (matcher.find()) {
			count++;
		}
		return count;
	}

	/**
	 * Conta il numero di corrispondenze di una sottostringa
	 * all'interno di una stringa.
	 *
	 * @param source stringa da analizzare (obbligatoria).
	 * @param substring la sottostringa da cercare (obbligatoria).
	 *
	 * @since 21/02/2014
	 */
	public static int countMatches(final @NonNull String source, final @NonNull String substring) {
		if (source.isEmpty() || substring.isEmpty()) {
			return 0;
		}
		final int subStrLength = substring.length();
		int start = 0;
		int count = 0;
		int lastIndex;
		while ((lastIndex = source.indexOf(substring, start)) > -1) {
			count++;
			start = lastIndex + subStrLength;
		}
		return count;
	}

	/**
	 * Restituisce il carattere di una stringa
	 * nella posizione indicata.
	 * Se la stringa è {@code null} o la posizione
	 * non è valida viene restituito un carattere di default.
	 *
	 * <pre>
	 * charAtSafe("ciao", 0, '*') -> 'c'
	 * charAtSafe("ciao", 2, '*') -> 'a'
	 * charAtSafe("ciao", -2, '*') -> '*'
	 * charAtSafe("ciao", 7, '*') -> '*'
	 * charAtSafe(null, 4, '*') -> '*'
	 * </pre>
	 *
	 * @param s stringa da esaminare.
	 * @param pos posizione del carattere da estrarre.
	 * @param defValue carattere di default
	 * @return il carattere nella posizione indicata
	 *         oppure {@code defValue}.
	 *
	 * @since 24/08/2012
	 */
	public static char charAtSafe(final @Nullable CharSequence s, final int pos, final char defValue) {
		if (s == null || pos < 0 || pos >= s.length()) {
			return defValue;
		}
		return s.charAt(pos);
	}

	/**
	 * Concatena due stringhe separandole con una stringa.
	 * <br/>
	 * Se una delle due stringhe è {@code null} viene restituita
	 * l'altra stringa senza separatore.
	 * <br/>
	 * Se entrambe le stringhe sono {@code null} viene
	 * restituito {@code null}.
	 *
	 * <pre>
	 * concat("uno", "due", " - ") -> "uno - due"
	 * concat("uno", null, " - ") -> "uno"
	 * concat(null, "due", " - ") -> "due"
	 * concat(null, null, " - ") -> null
	 * </pre>
	 *
	 * @param s1 prima stringa.
	 * @param s2 seconda stringa.
	 * @param separator il separatore delle stringhe (obbligatorio).
	 *
	 * @since 14/05/2014
	 */
	public static String concatOrNull(final @Nullable String s1, final @Nullable String s2, final @NonNull String separator) {
		if (s1 == null && s2 == null) {
			return null;
		}
		if (s1 != null) {
			if (s2 != null) {
				return s1 + separator + s2;
			}
			return s1;
		}
		return s2;
	}

	/**
	 * Concatena una collezione di stringhe separandole con una stringa,
	 * aggiungendo un prefisso e un suffisso per ogni stringa.
	 * Stringhe {@code null} contenute nella collezione vengono
	 * considerate stringhe di lunghezza 0.
	 *
	 * <pre>
	 * concat(Arrays.asList("uno", "due", "tre"), "\n", " - ", ";")
	 *
	 * - uno;
	 * - due;
	 * - tre;
	 * </pre>
	 *
	 * java 8:
	 *
	 * <pre>
	 * StringJoiner
	 * </pre>
	 *
	 * @param cs collezione di stringhe da concatenare (obbligatorio).
	 * @param separator il separatore delle stringhe (obbligatorio).
	 * @param prefix il prefisso.
	 * @param suffix il suffisso.
	 * @return la stringa risultato della concatenazione
	 *         oppure una stringa vuota se la collezione
	 *         di stringhe specificata è vuota.
	 *
	 * @since 17/09/2012
	 */
	public static @NonNull String concat(final @NonNull Collection<String> cs, final @NonNull String separator, final String prefix, final String suffix) {
		Objects.requireNonNull(separator, "Strings separator required");
		if (cs.isEmpty()) {
			return "";
		}
		final String pre = nullToEmpty(prefix);
		final String suf = nullToEmpty(suffix);
		final StringBuilder result = new StringBuilder();
		for (final String s : cs) {
			result.append(pre).append(nullToEmpty(s)).append(suf).append(separator);
		}

		if (result.length() == 0) {
			return "";
		}
		// devo rimuovere l'ultimo separatore
		return result.substring(0, result.length() - separator.length());
	}

	/**
	 * Concatena una serie di stringhe separandole con una stringa
	 * separatrice.
	 * Stringhe {@code null} vengono considerate stringhe di
	 * lunghezza 0.
	 *
	 * <pre>
	 * concat("; ", "uno", "due", "tre")
	 *
	 * uno; due; tre
	 * </pre>
	 *
	 * @param separator il separatore delle stringhe (obbligatorio).
	 * @param strings le stringhe da concatenare.
	 * @return la stringa risultato della concatenazione
	 *         oppure una stringa vuota se non è
	 *         specificata alcuna stringa.
	 *
	 * @since 21/02/2014
	 */
	public static @NonNull String concat(final @NonNull CharSequence separator, final @NonNull CharSequence... strings) {
		Objects.requireNonNull(separator, "Strings separator required");
		if (strings.length == 0) {
			return "";
		}
		final StringBuilder result = new StringBuilder();
		for (final CharSequence string : strings) {
			result.append(string == null ? "" : string).append(separator);
		}
		// devo rimuovere l'ultimo separatore
		return result.substring(0, result.length() - separator.length());
	}

	/**
	 * Effettua il parsing di una stringa in modo
	 * da inserire dei valori in una mappa (non ordinata).
	 *
	 * <pre>
	 * parse("nome=Pinco|cognome=Pallino", "\\|") ->
	 * new HashMap: {"nome"="Pinco"},{"cognome"="Pallino"}
	 * </pre>
	 *
	 * @param source stringa da analizzare.
	 * @param separator separatore delle coppie.
	 * @throws Exception il caso di formato non
	 *             valido della stringa.
	 */
	public static Map<String, String> parseMap(final @NonNull String source, final @NonNull String separator) throws Exception {
		requireNonEmpty(separator, "Empty separator");
		if (source.isEmpty()) {
			return Collections.emptyMap();
		}
		final Map<String, String> map = new HashMap<>();
		final String[] tokens = source.split(separator);
		for (final String token : tokens) {
			final int ix = token.indexOf('=');
			if (ix < 0) {
				map.put(token, null);
			} else {
				map.put(token.substring(0, ix), token.substring(ix + 1, token.length()));
			}
		}
		return map;
	}

	/**
	 * Divide una stringa in righe di lunghezza massima specificata
	 * effettuando uno split sugli spazi.
	 *
	 * <pre>
	 * splitInRows("Questa è una stringa molto molto lunga", 15)
	 *
	 * Questa è una
	 * stringa molto
	 * molto lunga
	 * </pre>
	 *
	 * @param source stringa da dividere.
	 * @param maxLength lunghezza massima delle righe.
	 *
	 * @since 02/09/2014
	 */
	public static String[] splitInRows(final @NonNull String source, final int maxLength) {
		if (source.length() <= maxLength) {
			return new String[] { source };
		}

		final List<String> result = new ArrayList<>();

		String current = "";
		for (final String split : source.split("\\s{1,}")) {
			String tmp;
			if ((tmp = (current + " " + split).trim()).length() <= maxLength) {
				current = tmp;
			} else {
				if (!current.equals("")) {
					result.add(current);
				}

				tmp = split;
				while (tmp.length() > maxLength) {
					result.add(tmp.substring(0, maxLength));
					tmp = tmp.substring(maxLength);
				}

				current = tmp;
			}
		}

		if (!current.equals("")) {
			result.add(current);
		}

		return result.toArray(new String[result.size()]);
	}

	/**
	 * Divide una stringa con il carattere separatore specificato
	 * e restituisce la lunghezza della sottostringa più lunga.
	 *
	 * <pre>
	 * maxRowLength(null, ' ') -> 0
	 * maxRowLength("", ' ') -> 0
	 * maxRowLength("Ciao a tutti", ' ') -> 5
	 * </pre>
	 *
	 * @param source stringa da analizzare.
	 * @param separator il carattere separatore.
	 * @return la lunghezza massima oppure 0 se la stringa
	 *         da analizzare è {@code null} o vuota.
	 *
	 * @see #maxRowLength(String, String)
	 *
	 * @since 21/01/2014
	 */
	public static int maxRowLength(final @Nullable String source, final char separator) {
		if (source == null || source.isEmpty()) {
			return 0;
		}
		int max = 0;
		final int length = source.length();
		int current = 0;
		for (int i = 0; i < length; i++) {
			if (source.charAt(i) == separator) {
				max = Math.max(max, current);
				current = 0;
			} else {
				current++;
			}
		}
		return Math.max(max, current);
	}

	/**
	 * Divide una stringa con il separatore regex specificato
	 * e restituisce la lunghezza della sottostringa più lunga.
	 *
	 * <pre>
	 * maxRowLength(null, " ") -> 0
	 * maxRowLength("", " ") -> 0
	 * maxRowLength("Ciao a tutti", " ") -> 5
	 * maxRowLength("Ciao a tutti", null) -> 12
	 * </pre>
	 *
	 * @param source stringa da analizzare.
	 * @param separator separatore.
	 * @return la lunghezza massima oppure 0 se la stringa
	 *         da analizzare è {@code null} o vuota.
	 *         Se il separatore è {@code null} o una stringa
	 *         vuota restituisce la lunghezza della stringa
	 *         analizzata stessa.
	 *
	 * @see #maxRowLength(String, char)
	 *
	 * @since 06/09/2013
	 */
	public static int maxRowLength(final @Nullable String source, final @Nullable String separator) {
		if (source == null || source.isEmpty()) {
			return 0;
		}
		if (separator == null || separator.isEmpty()) {
			return source.length();
		}
		return maxLength(Arrays.asList(source.split(separator)));
	}

	/**
	 * Restituisce la lunghezza della sequenza di caratteri
	 * più lunga in un iterabile.
	 * Se l'iterabile è {@code null} restituisce 0.
	 *
	 * @param charSequences iterabile di sequenze di caratteri.
	 * @return la lunghezza della sequenza di caratteri maggiore.
	 * @throws NullPointerException se l'iterabile è {@code null} o
	 *             uno degli elementi restituito dal suo iteratore
	 *             è {@code null}.
	 *
	 * @since 17/09/2013
	 */
	public static int maxLength(final @NonNull Iterable<? extends CharSequence> charSequences) {
		int max = 0;
		final Iterator<? extends CharSequence> it = charSequences.iterator();
		while (it.hasNext()) {
			max = Math.max(max, it.next().length());
		}
		return max;
	}

	/**
	 * Restituisce un iteratore sui caratteri della stringa
	 * specificata.
	 *
	 * @param s la stringa su cui iterare.
	 *
	 * @since 21/01/2014
	 */
	public static @NonNull CharacterIterator iterator(final @NonNull String s) {
		return new StringCharacterIterator(s);
	}

	/**
	 * Controlla se una stringa contiene almeno una delle sottostringhe passate.
	 * (Case sensitive)
	 *
	 * <pre>
	 * contains("ciao Pinco Pallino", "ciao") -> true
	 * contains("ciao Pinco Pallino", "ciao", "Pinco", " ", "xxx") -> true
	 * contains("ciao Pinco Pallino", "pinco") -> false
	 * contains("ciao Pinco Pallino") -> false
	 * </pre>
	 *
	 * @param source stringa da analizzare.
	 * @param strings sottostringhe.
	 *
	 * @throws NullPointerException se la stringa da analizzare
	 *             oppure una delle sottostringhe è {@code null}.
	 *
	 * @since 08/04/2013
	 */
	public static boolean contains(final @NonNull String source, final @NonNull String... strings) {
		for (final String string : strings) {
			if (source.contains(string)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Controlla se una stringa contiene tutte le sottostringhe passate.
	 * (Case sensitive)
	 *
	 * <pre>
	 * contains("ciao Pinco Pallino", "ciao") -> true
	 * contains("ciao Pinco Pallino", "ciao", "Pal", " ") -> true
	 * contains("ciao Pinco Pallino", "pinco") -> false
	 * contains("ciao Pinco Pallino") -> true
	 * </pre>
	 *
	 * @param source stringa da analizzare.
	 * @param strings sottostringhe.
	 *
	 * @throws NullPointerException se la stringa da analizzare
	 *             oppure una delle sottostringhe è {@code null}.
	 *
	 * @since 08/04/2013
	 */
	public static boolean containsAll(final @NonNull String source, final @NonNull String... strings) {
		for (final String string : strings) {
			if (!source.contains(string)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifica se una stringa contiene una sottostringa
	 * specificata ignorando maiuscole/minuscole.
	 * (Case insensitive)
	 *
	 * <pre>
	 * containsIgnoreCase(null, null) -> false
	 * containsIgnoreCase("ciao", null) -> false
	 * containsIgnoreCase("ciao a tutti", "TUTTI") -> true
	 * containsIgnoreCase("prova 1", " 1") -> true
	 * </pre>
	 *
	 * @param source stringa da analizzare.
	 * @param search sottostringa da cercare.
	 * @return {@code true} se la stringa analizzata
	 *         contiene la sottostringa.
	 *
	 * @since 23/10/2013
	 */
	public static boolean containsIgnoreCase(final @Nullable String source, final @Nullable String search) {
		if (source == null || search == null) {
			return false;
		}
		final int searchLength = search.length();
		for (int i = 0; i <= source.length() - searchLength; i++) {
			if (source.regionMatches(true, i, search, 0, searchLength)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Controlla se una stringa è "uguale" ad una delle stringhe passate.
	 *
	 * <pre>
	 * equals("due") -> false
	 * equals("due", "due") -> true
	 * equals("due", "uno", "due", "tre) -> true
	 * equals("due", "tre") -> false
	 * equals("due", "uno", "tre") -> false
	 * </pre>
	 *
	 * @param source stringa da analizzare.
	 * @param strings altre stringhe.
	 *
	 * @throws NullPointerException se l'array delle stringhe da analizzare è {@code null}.
	 */
	public static boolean equals(final @Nullable String source, final @NonNull String... strings) {
		for (final String string : strings) {
			if (Objects.equals(source, string)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Restituisce una stringa rappresentante il prefisso
	 * in comune tra due stringhe date.
	 * <br/>
	 * Se almeno una delle due stringhe è {@code null} o
	 * non esiste un prefisso in comune restituisce {@code null}.
	 *
	 * @param s1 una stringa.
	 * @param s2 un'altra stringa.
	 *
	 * @since 26/09/2014
	 */
	public static String findCommonPrefix(final @Nullable String s1, final @Nullable String s2) {
		if (s1 == null || s2 == null) {
			return null;
		}
		int i;
		for (i = 0; i < s1.length() && i < s2.length(); i++) {
			if (s1.charAt(i) != s2.charAt(i)) {
				break;
			}
		}
		return i > 0 ? s1.substring(0, i) : null;
	}

	/**
	 * Semplice "capitalizzazione" delle iniziali in una stringa.
	 *
	 * <p>Se un carattere <b>c</b> è una lettera, quello precedente non lo è
	 * ed il successivo non è uno spazio: <b>c</b> non cambia.</p>
	 *
	 * <pre>
	 * capitalize("ciao") -> "Ciao"
	 * capitalize("pinco pallino") -> "Pinco Pallino"
	 * capitalize("pinco o pallino") -> "Pinco o Pallino"
	 * capitalize("pincoPallino") -> "PincoPallino"
	 * capitalize("x[y]") -> "X[Y]"
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @throws NullPointerException se la stringa da analizzare è {@code null}.
	 */
	public static @NonNull String capitalize(final @NonNull String s) {
		final int length = s.length();
		if (length == 0) {
			return s;
		}
		if (length == 1) {
			return String.valueOf(Character.toUpperCase(s.charAt(0)));
		}
		final char[] chars = s.toCharArray();
		final char[] result = new char[length];
		boolean upper = true;
		int i = 0;
		for (; i < length - 1; i++) {
			final char c = chars[i];
			if (!Character.isLetter(c)) {
				upper = true;
				result[i] = c;
			} else {
				result[i] = upper && !Character.isWhitespace(chars[i + 1]) ? Character.toUpperCase(c) : c;
				upper = false;
			}
		}
		result[i] = chars[i];
		return new String(result);
	}

	/**
	 * Inserisce uno spazio prima di ogni lettera maiuscola
	 * e la converte in minuscola.
	 * <br/>
	 * Se la prima lettera è maiuscola, viene convertita in minuscola
	 * ma senza mattere un spazio.
	 *
	 * <pre>
	 * splitCamelCase("ciao") -> "ciao"
	 * splitCamelCase("splitCamelCase") -> "split camel case"
	 * splitCamelCase("Stringa Con spazi") -> "stringa  Con spazi"
	 * </pre>
	 *
	 * @param s stringa da analizzare.
	 * @throws NullPointerException se la stringa da analizzare è {@code null}.
	 */
	public static @NonNull String splitCamelCase(final @NonNull String s) {
		if (s.isEmpty()) {
			return s;
		}
		if (s.length() == 1) {
			final char c = s.charAt(0);
			return c >= 'A' && c <= 'Z' ? Character.toString(Character.toLowerCase(c)) : s;
		}
		final char[] chars = s.toCharArray();
		final char[] result = new char[chars.length * 2];
		int offset = 0;
		result[0] = Character.toLowerCase(chars[0]);
		for (int i = 1; i < chars.length; i++) {
			final char c = chars[i];
			if (c >= 'A' && c <= 'Z') {
				result[i + offset] = ' ';
				offset++;
				result[i + offset] = Character.toLowerCase(c);
			} else {
				result[i + offset] = c;
			}
		}
		return new String(result, 0, chars.length + offset);
	}

	/**
	 * Genera una stringa composta da n caratteri uguali.
	 *
	 * <pre>
	 * generate('b', 3) -> "bbb"
	 * generate('x', 0) -> ""
	 * </pre>
	 *
	 * @param character carattere.
	 * @param length numero di ripetizioni.
	 * @throws NegativeArraySizeException se {@code length < 0}.
	 */
	public static @NonNull String generate(final char character, final int length) {
		if (length == 0) {
			return "";
		}
		if (length == 1) {
			return String.valueOf(character);
		}
		final char[] array = new char[length];
		Arrays.fill(array, character);
		return new String(array);
	}

	/**
	 * Genera una stringa composta da una stringa ripetuta
	 * n volte.
	 *
	 * <pre>
	 * generate("ciao", 3) -> "ciaociaociao"
	 * generate("ciao", 0) -> ""
	 * </pre>
	 *
	 * @param string la stringa da ripetere.
	 * @param n numero di ripetizioni.
	 * @throws IllegalArgumentException se {@code length < 0}.
	 *
	 * @since 21/11/2013
	 */
	public static @NonNull String generate(final @NonNull String string, final int n) {
		if (n < 0) {
			throw new IllegalArgumentException("Negative number of repetitions");
		}
		if (n == 0 || string.isEmpty()) {
			return "";
		}
		if (n == 1) {
			return string;
		}
		final StringBuilder result = new StringBuilder(string.length() * n);
		for (int i = 0; i < n; i++) {
			result.append(string);
		}
		return result.toString();
	}

	/**
	 * Converte una stringa in ascii.
	 * <p>
	 * La stringa viene prima "normalizzata" per non perdere le lettere accentate
	 * e poi convertita in ascii mantenendo la stessa lunghezza.
	 *
	 * <pre>
	 * toAscii("bàéèìòù@#¢¢#[]#Þ{¿") -> "baeeiou@#??#[]#?{?"
	 * </pre>
	 *
	 * </p>
	 * Se la stringa è {@code null} restituisce {@code null}.
	 *
	 * @param s stringa da convertire.
	 * @see <a href="http://www.unicode.org/reports/tr15/tr15-23.html#Decomposition">Unicode Normalization Forms - Decomposition</a>
	 */
	public static String toAscii(final @Nullable String s) {
		if (isNullOrEmpty(s)) {
			return s;
		}
		final String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
		final Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		try {
			return new String(pattern.matcher(temp).replaceAll("").getBytes(ASCII_CHARSET), ASCII_CHARSET);
		} catch (final UnsupportedEncodingException ex) {
			assert false : ex;
			return s;
		}
	}

	/**
	 * Rimuove n caratteri da uno {@code StringBuilder} partendo
	 * dall'inizio della stringa.
	 *
	 * @param sb l'oggetto interessato.
	 * @param n numero di caratteri da rimuovere.
	 *
	 * @since 14/11/2013
	 */
	public static void deleteFromBegin(final @NonNull StringBuilder sb, final int n) {
		sb.delete(0, n);
	}

	/**
	 * Rimuove n caratteri da uno {@code StringBuilder} partendo
	 * dalla fine della stringa.
	 *
	 * @param sb l'oggetto interessato.
	 * @param n numero di caratteri da rimuovere.
	 *
	 * @since 14/11/2013
	 */
	public static void deleteFromEnd(final @NonNull StringBuilder sb, final int n) {
		final int sbLength = sb.length();
		sb.delete(sbLength - n, sbLength);
	}

	/**
	 * Confronta due stringhe, eventualmente nulle.
	 *
	 * @param s1 prima stringa (opzionale).
	 * @param s2 seconda stringa (opzionale).
	 * @param ascending se {@code true}, la stringa minore è quella che alfabeticamente viene prima, altrimenti viceversa.
	 * @param nullFirst indica se le stringhe {@code null} vengono prima di quelle non {@code null}, indipendentemente da {@code asceding}.
	 */
	public static int nullSafeCompare(final String s1, final String s2, final boolean ascending, final boolean nullFirst) {
		if (s1 == null) {
			if (s2 != null) {
				return nullFirst ? -1 : 1;
			}
			return 0;
		}
		if (s2 == null) {
			return nullFirst ? 1 : -1;
		}
		return ascending ? s1.compareTo(s2) : s2.compareTo(s1);
	}

	/**
	 * Confronta due stringhe, eventualmente nulle.
	 *
	 * @param s1 prima stringa (opzionale).
	 * @param s2 seconda stringa (opzionale).
	 * @param ascending se {@code true}, la stringa minore è quella che alfabeticamente viene prima senza contare maiuscole e miniscole, altrimenti viceversa.
	 * @param nullFirst indica se le stringhe {@code null} vengono prima di quelle non {@code null}, indipendentemente da {@code asceding}.
	 */
	public static int nullCaseSafeCompare(final String s1, final String s2, final boolean ascending, final boolean nullFirst) {
		if (s1 == null) {
			if (s2 != null) {
				return nullFirst ? -1 : 1;
			}
			return 0;
		}
		if (s2 == null) {
			return nullFirst ? 1 : -1;
		}
		return ascending ? s1.compareToIgnoreCase(s2) : s2.compareToIgnoreCase(s1);
	}

	/**
	 * Eccezione non gestita propagata quando una stringa richiesta non è
	 * valorizzata (in genere quando è {@code null} oppure vuota).
	 *
	 * @author filippo
	 * @since 17/09/2013
	 */
	public static class StringNotValorizedException extends RuntimeException {

		private static final long serialVersionUID = 7921753315363018868L;

		public StringNotValorizedException() {
			super();
		}

		public StringNotValorizedException(final String message) {
			super(message);
		}
	}
}
