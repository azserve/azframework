package com.azserve.azframework.common.lang;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Objects;
import java.util.OptionalInt;
import java.util.UUID;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;

/**
 * Classe di utilità per la gestione di oggetti {@link Number}.
 *
 * @author filippo
 * @since 10/06/2013
 */
public final class NumberUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(NumberUtils.class);

	public enum NumberPattern {

		INT("0"),
		INT_THOUSANDS_SEPARATOR("#,##0"),
		/** Formatta il valore 0.3 in "0 %" */
		INT_PERCENTAGE("0 '%'"),
		/** Formatta il valore 0.3 in "30 %" */
		INT_PERCENTAGE_100("0 %"),
		TWO_DIGITS("0.00"),
		TWO_DIGITS_THOUSANDS_SEPARATOR("#,##0.00"),
		/** Formatta il valore 0.3 in "0.30 %" */
		TWO_DIGITS_PERCENTAGE("0.00 '%'"),
		/** Formatta il valore 0.3 in "30.0 %" */
		TWO_DIGITS_PERCENTAGE_100("0.00 %"),
		TWO_DIGITS_CURRENCY("0.00 \u00A4"),
		TWO_DIGITS_CURRENCY_THOUSANDS_SEPARATOR("#,##0.00 \u00A4");

		private final DecimalFormat formatter;
		private final String pattern;

		NumberPattern(final String pattern) {
			this.pattern = pattern;
			this.formatter = new DecimalFormat(pattern);
		}

		public DecimalFormat getFormatter() {
			return this.formatter;
		}

		public String getPattern() {
			return this.pattern;
		}
	}

	/** Costante che rappresenta il valore {@value} . */
	private static final int MAX_LENGTH = 19;

	/** Costante che rappresenta il valore {@value} . */
	private static final String DOUBLE_FORMAT = "#,##0";

	private static final Pattern DIGITS_REGEX_PATTERN = Pattern.compile("[0-9]*");

	/** Oggetto {@code Integer} rappresentante il numero 0. */
	public static final Integer INTEGER_ZERO = Integer.valueOf(0);
	/** Oggetto {@code Short} rappresentante il numero 0. */
	public static final Short SHORT_ZERO = Short.valueOf((short) 0);
	/** Oggetto {@code Long} rappresentante il numero 0. */
	public static final Long LONG_ZERO = Long.valueOf(0);
	/** Oggetto {@code Float} rappresentante il numero 0. */
	public static final Float FLOAT_ZERO = Float.valueOf(0);
	/** Oggetto {@code Double} rappresentante il numero 0. */
	public static final Double DOUBLE_ZERO = Double.valueOf(0);

	public static final BigDecimal HUNDRED = BigDecimal.valueOf(100);
	public static final BigInteger HUNDRED_BI = BigInteger.valueOf(100);

	public static final BigDecimal THOUSAND = BigDecimal.valueOf(1000);
	public static final BigInteger THOUSAND_BI = BigInteger.valueOf(1000);

	private NumberUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	// conversion obj to obj

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Byte}.
	 */
	public static Byte toByte(final @Nullable Number n) {
		return n == null ? null : Byte.valueOf(n.byteValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Byte}.
	 *
	 * @param defaultValue valore rappresentato dall'oggetto {@code Byte} se
	 *            il numero in ingresso è {@code null}.
	 */
	public static @NonNull Byte toByte(final @Nullable Number n, final byte defaultValue) {
		return Byte.valueOf(n == null ? defaultValue : n.byteValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Short}.
	 */
	public static Short toShort(final @Nullable Number n) {
		return n == null ? null : Short.valueOf(n.shortValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Short}.
	 *
	 * @param defaultValue valore rappresentato dall'oggetto {@code Short} se
	 *            il numero in ingresso è {@code null}.
	 */
	public static @NonNull Short toShort(final @Nullable Number n, final short defaultValue) {
		return Short.valueOf(n == null ? defaultValue : n.shortValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Integer}.
	 */
	public static Integer toInteger(final @Nullable Number n) {
		return n == null ? null : Integer.valueOf(n.intValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Integer}.
	 *
	 * @param defaultValue valore rappresentato dall'oggetto {@code Integer} se
	 *            il numero in ingresso è {@code null}.
	 */
	public static @NonNull Integer toInteger(final @Nullable Number n, final int defaultValue) {
		return Integer.valueOf(n == null ? defaultValue : n.intValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Long}.
	 */
	public static Long toLong(final @Nullable Number n) {
		return n == null ? null : Long.valueOf(n.longValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Long}.
	 *
	 * @param defaultValue valore rappresentato dall'oggetto {@code Long} se
	 *            il numero in ingresso è {@code null}.
	 */
	public static @NonNull Long toLong(final @Nullable Number n, final long defaultValue) {
		return Long.valueOf(n == null ? defaultValue : n.longValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Float}.
	 */
	public static Float toFloat(final @Nullable Number n) {
		return n == null ? null : Float.valueOf(n.floatValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Float}.
	 *
	 * @param defaultValue valore rappresentato dall'oggetto {@code Float} se
	 *            il numero in ingresso è {@code null}.
	 */
	public static @NonNull Float toFloat(final @Nullable Number n, final float defaultValue) {
		return Float.valueOf(n == null ? defaultValue : n.floatValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Double}.
	 */
	public static Double toDouble(final @Nullable Number n) {
		return n == null ? null : Double.valueOf(n.doubleValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code Double}.
	 *
	 * @param defaultValue valore rappresentato dall'oggetto {@code Double} se
	 *            il numero in ingresso è {@code null}.
	 */
	public static @NonNull Double toDouble(final @Nullable Number n, final double defaultValue) {
		return Double.valueOf(n == null ? defaultValue : n.doubleValue());
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code BigDecimal}.
	 */
	public static BigDecimal toBigDecimal(final @Nullable Number n) {
		return toBigDecimal(n, null);
	}

	/**
	 * Converte un oggetto {@code Number} in un oggetto {@code BigDecimal}.
	 *
	 * @param defaultValue l'oggetto {@code BigDecimal} restituito
	 *            se il numero in ingresso è {@code null}.
	 */
	public static BigDecimal toBigDecimal(final @Nullable Number n, final BigDecimal defaultValue) {
		if (n == null) {
			return defaultValue;
		}
		if (n instanceof BigDecimal) {
			return (BigDecimal) n;
		}
		return new BigDecimal(n.toString());
	}

	/**
	 * Restituisce un oggetto {@code UUID} a partire da due oggetti {@code Long}.
	 * Se uno degli oggetti {@code Long} è {@code null} restituisce {@code null}.
	 */
	public static UUID toUUID(final @Nullable Long msbId, final @Nullable Long lsbId) {
		return msbId == null || lsbId == null ? null : new UUID(msbId.longValue(), lsbId.longValue());
	}

	/**
	 * Converts an universally unique identifier to a byte array.
	 *
	 * @param uuid unique-identifier
	 * @return the bytes in {@linkplain ByteOrder#BIG_ENDIAN} order
	 */
	public static byte[] uuidToBytes(final @NonNull UUID uuid) {
		Objects.requireNonNull(uuid, "UUID is null");
		final byte[] uuidBytes = new byte[16];
		ByteBuffer.wrap(uuidBytes)
				.order(ByteOrder.BIG_ENDIAN)
				.putLong(uuid.getMostSignificantBits())
				.putLong(uuid.getLeastSignificantBits());
		return uuidBytes;
	}

	/**
	 * Restituisce un oggetto {@code BigInteger} a partire da un oggetto {@code Integer}.
	 * Se l'oggetto passato è {@code null} restituisce {@code null}.
	 *
	 * @param n oggetto sorgente.
	 */
	public static BigInteger toBigInteger(final @Nullable Integer n) {
		return n == null ? null : BigInteger.valueOf(n.intValue());
	}

	/**
	 * Restituisce un oggetto {@code BigInteger} a partire da un oggetto {@code Integer}.
	 * Se l'oggetto passato è {@code null} restituisce il valore di default specificato.
	 *
	 * @param n oggetto sorgente.
	 * @param defaultValue l'oggetto {@code BigInteger} restituito
	 *            se il numero in ingresso è {@code null}.
	 */
	public static BigInteger toBigInteger(final @Nullable Integer n, final BigInteger defaultValue) {
		return n == null ? defaultValue : BigInteger.valueOf(n.intValue());
	}

	/**
	 * Restituisce un oggetto {@code BigInteger} a partire da un oggetto {@code Long}.
	 * Se l'oggetto passato è {@code null} restituisce {@code null}.
	 *
	 * @param n oggetto sorgente.
	 */
	public static BigInteger toBigInteger(final @Nullable Long n) {
		return n == null ? null : BigInteger.valueOf(n.longValue());
	}

	/**
	 * Restituisce un oggetto {@code BigInteger} a partire da un oggetto {@code Double}.
	 * Se l'oggetto passato è {@code null} restituisce {@code null}.
	 *
	 * @param n oggetto sorgente.
	 */
	public static BigInteger toBigInteger(final @Nullable Double n) {
		return n == null ? null : BigInteger.valueOf(n.longValue());
	}

	/**
	 * Restituisce un oggetto {@code BigDecimal} a partire da un oggetto {@code Integer}.
	 * Se l'oggetto passato è {@code null} restituisce un valore specificato.
	 *
	 * @param n oggetto sorgente.
	 * @param nullValue oggetto da restituire se l'oggetto sorgente è {@code null}.
	 */
	public static BigDecimal toBigDecimal(final @Nullable Integer n, final @Nullable BigDecimal nullValue) {
		return n == null ? nullValue : BigDecimal.valueOf(n.intValue());
	}

	/**
	 * Restituisce un oggetto {@code BigDecimal} a partire da un oggetto {@code Integer}.
	 * Se l'oggetto passato è {@code null} restituisce {@code null}.
	 *
	 * @param n oggetto sorgente.
	 */
	public static BigDecimal toBigDecimal(final @Nullable Integer n) {
		return toBigDecimal(n, null);
	}

	/**
	 * Restituisce un oggetto {@code BigDecimal} a partire da un oggetto {@code Double}.
	 * Se l'oggetto passato è {@code null} restituisce un valore specificato.
	 *
	 * @param n oggetto sorgente.
	 * @param nullValue oggetto da restituire se l'oggetto sorgente è {@code null}.
	 */
	public static BigDecimal toBigDecimal(final @Nullable Double n, final @Nullable BigDecimal nullValue) {
		return n == null ? nullValue : BigDecimal.valueOf(n.doubleValue());
	}

	/**
	 * Restituisce un oggetto {@code BigDecimal} a partire da un oggetto {@code Double}.
	 * Se l'oggetto passato è {@code null} restituisce {@code null}.
	 *
	 * @param n oggetto sorgente.
	 */
	public static BigDecimal toBigDecimal(final @Nullable Double n) {
		return toBigDecimal(n, null);
	}

	// conversion primitive to obj

	/**
	 * Restituisce un oggetto {@link Integer} a partire da un numero double.
	 */
	public static @NonNull Integer toInteger(final double value) {
		return Integer.valueOf((int) value);
	}

	/**
	 * Restituisce un oggetto {@link Long} a partire da un numero double.
	 */
	public static @NonNull Long toLong(final double value) {
		return Long.valueOf((long) value);
	}

	// parsing

	/**
	 * Restituisce un oggetto {@code Integer} a partire da una stringa.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce {@code null}.
	 *
	 * @param s stringa da analizzare.
	 */
	public static Integer parseInteger(final @Nullable String s) {
		return parseInteger(s, null);
	}

	/**
	 * @see #parseInteger(String, Integer)
	 */
	public static Integer parseInteger(final String s, final int defaultValue) {
		return parseInteger(s, Integer.valueOf(defaultValue));
	}

	/**
	 * Restituisce un oggetto {@code Integer} a partire da una stringa.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce
	 * il valore di default indicato.
	 *
	 * @param s stringa da analizzare.
	 * @param defaultValue valore di default.
	 */
	public static Integer parseInteger(final @Nullable String s, final @Nullable Integer defaultValue) {
		if (s != null && !s.isEmpty()) {
			try {
				return Integer.valueOf(s);
			} catch (final @SuppressWarnings("unused") NumberFormatException nfex) {
				//
			}
		}
		return defaultValue;
	}

	/**
	 * Restituisce un intero a partire da una stringa.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce
	 * il valore di default indicato.
	 *
	 * @param s stringa da analizzare.
	 * @param defaultValue valore di default.
	 */
	public static int parseInt(final @Nullable String s, final int defaultValue) {
		if (s != null && !s.isEmpty()) {
			try {
				return Integer.parseInt(s);
			} catch (final @SuppressWarnings("unused") NumberFormatException nfex) {
				//
			}
		}
		return defaultValue;
	}

	/**
	 * Restituisce un intero in base 10 a partire da una stringa
	 * sotto forma di {@code OptionalInt}.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce
	 * {@linkplain OptionalInt#empty()}.
	 *
	 * @param s stringa da analizzare.
	 */
	public static OptionalInt parseIntAsOptional(final String s) {
		return parseIntAsOptional(s, 10);
	}

	/**
	 * Restituisce un intero a partire da una stringa sotto forma di {@code OptionalInt}.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce
	 * {@linkplain OptionalInt#empty()}.
	 *
	 * @param s stringa da analizzare.
	 * @param radix la base numerica.
	 */
	public static OptionalInt parseIntAsOptional(final String s, final int radix) {
		if (s == null) {
			return OptionalInt.empty();
		}
		if (radix < Character.MIN_RADIX) {
			throw new IllegalArgumentException("radix " + radix + " less than Character.MIN_RADIX");
		}
		if (radix > Character.MAX_RADIX) {
			throw new IllegalArgumentException("radix " + radix + " greater than Character.MAX_RADIX");
		}
		int result = 0;
		boolean negative = false;
		int i = 0;
		final int len = s.length();
		int limit = -Integer.MAX_VALUE;
		int multmin;
		int digit;

		if (len > 0) {
			final char firstChar = s.charAt(0);
			if (firstChar < '0') {
				if (firstChar == '-') {
					negative = true;
					limit = Integer.MIN_VALUE;
				} else if (firstChar != '+') {
					return OptionalInt.empty();
				}

				if (len == 1) {
					return OptionalInt.empty();
				}
				i++;
			}
			multmin = limit / radix;
			while (i < len) {
				digit = Character.digit(s.charAt(i++), radix);
				if (digit < 0 || result < multmin) {
					return OptionalInt.empty();
				}
				result *= radix;
				if (result < limit + digit) {
					return OptionalInt.empty();
				}
				result -= digit;
			}
			return OptionalInt.of(negative ? result : -result);
		}
		return OptionalInt.empty();
	}

	/**
	 * Restituisce un oggetto {@code Short} a partire da una stringa.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce {@code null}.
	 *
	 * @param s stringa da analizzare.
	 */
	public static Short parseShort(final @Nullable String s) {
		return parseShort(s, null);
	}

	/**
	 * @see #parseShort(String, Short)
	 */
	public static Short parseShort(final String s, final short defaultValue) {
		return parseShort(s, Short.valueOf(defaultValue));
	}

	/**
	 * Restituisce un oggetto {@code Short} a partire da una stringa.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce
	 * il valore di default indicato.
	 *
	 * @param s stringa da analizzare.
	 * @param defaultValue valore di default.
	 */
	public static Short parseShort(final @Nullable String s, final @Nullable Short defaultValue) {
		if (s != null && !s.isEmpty()) {
			try {
				return Short.valueOf(s);
			} catch (final @SuppressWarnings("unused") NumberFormatException nfex) {
				//
			}
		}
		return defaultValue;
	}

	/**
	 * Restituisce un oggetto {@code Long} a partire da una stringa.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce {@code null}.
	 *
	 * @param s stringa da analizzare.
	 */
	public static Long parseLong(final @Nullable String s) {
		return parseLong(s, null);
	}

	/**
	 * @see #parseLong(String, Long)
	 */
	public static Long parseLong(final String s, final long defaultValue) {
		return parseLong(s, Long.valueOf(defaultValue));
	}

	/**
	 * Restituisce un oggetto {@code Long} a partire da una stringa.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce
	 * il valore di default indicato.
	 *
	 * @param s stringa da analizzare.
	 * @param defaultValue valore di default.
	 */
	public static Long parseLong(final @Nullable String s, final @Nullable Long defaultValue) {
		if (s != null && !s.isEmpty()) {
			try {
				return Long.valueOf(s);
			} catch (final @SuppressWarnings("unused") NumberFormatException nfex) {
				//
			}
		}
		return defaultValue;
	}

	/**
	 * Restituisce un oggetto {@code Double} a partire da una stringa.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce {@code null}.
	 *
	 * @param s stringa da analizzare.
	 */
	public static Double parseDouble(final String s) {
		return parseDouble(s, null);
	}

	/**
	 * @see #parseDouble(String, Double)
	 */
	public static Double parseDouble(final String s, final double defaultValue) {
		return parseDouble(s, Double.valueOf(defaultValue));
	}

	/**
	 * Restituisce un oggetto {@code Double} a partire da una stringa.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce
	 * il valore di default indicato.
	 *
	 * @param s stringa da analizzare.
	 * @param defaultValue valore di default.
	 */
	public static Double parseDouble(final String s, final Double defaultValue) {
		if (s != null && !s.isEmpty()) {
			try {
				return Double.valueOf(s);
			} catch (final @SuppressWarnings("unused") NumberFormatException nfex) {
				//
			}
		}
		return defaultValue;
	}

	/**
	 * Restituisce un oggetto {@code UUID} a partire da una stringa.
	 * Se la stringa è {@code null}, vuota o non convertibile restituisce {@code null}.
	 *
	 * @param s stringa da analizzare.
	 */
	public static UUID parseUUID(final String s) {
		if (s != null && !s.isEmpty()) {
			try {
				return UUID.fromString(s);
			} catch (final @SuppressWarnings("unused") IllegalArgumentException iaex) {
				//
			}
		}
		return null;
	}

	// primitive values

	/**
	 * Restituisce il valore numerico di tipo {@code byte} rappresentato dall'oggetto {@code Byte}.
	 * Se l'oggetto passato è {@code null} restituisce un valore di default.
	 *
	 * @param value oggetto da analizzare.
	 * @param defaultValue valore da restituire se l'oggetto passato è {@code null}.
	 */
	public static byte ifNull(final Byte value, final byte defaultValue) {
		return value == null ? defaultValue : value.byteValue();
	}

	/**
	 * Restituisce il valore numerico di tipo {@code short} rappresentato dall'oggetto {@code Short}.
	 * Se l'oggetto passato è {@code null} restituisce un valore di default.
	 *
	 * @param value oggetto da analizzare.
	 * @param defaultValue valore da restituire se l'oggetto passato è {@code null}.
	 */
	public static short ifNull(final Short value, final short defaultValue) {
		return value == null ? defaultValue : value.shortValue();
	}

	/**
	 * Restituisce il valore numerico di tipo {@code int} rappresentato dall'oggetto {@code Integer}.
	 * Se l'oggetto passato è {@code null} restituisce un valore di default.
	 *
	 * @param value oggetto da analizzare.
	 * @param defaultValue valore da restituire se l'oggetto passato è {@code null}.
	 */
	public static int ifNull(final Integer value, final int defaultValue) {
		return value == null ? defaultValue : value.intValue();
	}

	/**
	 * Restituisce il valore numerico di tipo {@code long} rappresentato dall'oggetto {@code Long}.
	 * Se l'oggetto passato è {@code null} restituisce un valore di default.
	 *
	 * @param value oggetto da analizzare.
	 * @param defaultValue valore da restituire se l'oggetto passato è {@code null}.
	 */
	public static long ifNull(final Long value, final long defaultValue) {
		return value == null ? defaultValue : value.longValue();
	}

	/**
	 * Restituisce il valore numerico di tipo {@code float} rappresentato dall'oggetto {@code Float}.
	 * Se l'oggetto passato è {@code null} restituisce un valore di default.
	 *
	 * @param value oggetto da analizzare.
	 * @param defaultValue valore da restituire se l'oggetto passato è {@code null}.
	 */
	public static float ifNull(final Float value, final float defaultValue) {
		return value == null ? defaultValue : value.floatValue();
	}

	/**
	 * Restituisce il valore numerico di tipo {@code double} rappresentato dall'oggetto {@code Double}.
	 * Se l'oggetto passato è {@code null} restituisce un valore di default.
	 *
	 * @param value oggetto da analizzare.
	 * @param defaultValue valore da restituire se l'oggetto passato è {@code null}.
	 */
	public static double ifNull(final Double value, final double defaultValue) {
		return value == null ? defaultValue : value.doubleValue();
	}

	/**
	 * Restituisce il valore numerico di tipo {@code byte} rappresentato dall'oggetto {@code Byte}.
	 * Se l'oggetto passato è {@code null} restituisce 0.
	 *
	 * @param value oggetto da analizzare.
	 */
	public static byte removeNull(final Byte value) {
		return ifNull(value, (byte) 0);
	}

	/**
	 * Restituisce il valore numerico di tipo {@code short} rappresentato dall'oggetto {@code Short}.
	 * Se l'oggetto passato è {@code null} restituisce 0.
	 *
	 * @param value oggetto da analizzare.
	 */
	public static short removeNull(final Short value) {
		return ifNull(value, (short) 0);
	}

	/**
	 * Restituisce il valore numerico di tipo {@code int} rappresentato dall'oggetto {@code Integer}.
	 * Se l'oggetto passato è {@code null} restituisce 0.
	 *
	 * @param value oggetto da analizzare.
	 */
	public static int removeNull(final Integer value) {
		return ifNull(value, 0);
	}

	/**
	 * Restituisce il valore numerico di tipo {@code long} rappresentato dall'oggetto {@code Long}.
	 * Se l'oggetto passato è {@code null} restituisce 0.
	 *
	 * @param value oggetto da analizzare.
	 */
	public static long removeNull(final Long value) {
		return ifNull(value, 0);
	}

	/**
	 * Restituisce il valore numerico di tipo {@code float} rappresentato dall'oggetto {@code Float}.
	 * Se l'oggetto passato è {@code null} restituisce 0.
	 *
	 * @param value oggetto da analizzare.
	 */
	public static float removeNull(final Float value) {
		return ifNull(value, 0F);
	}

	/**
	 * Restituisce il valore numerico di tipo {@code double} rappresentato dall'oggetto {@code Double}.
	 * Se l'oggetto passato è {@code null} restituisce 0.
	 *
	 * @param value oggetto da analizzare.
	 */
	public static double removeNull(final Double value) {
		return ifNull(value, 0D);
	}

	public static BigDecimal removeNull(final BigDecimal value) {
		return value != null ? value : BigDecimal.ZERO;
	}

	/**
	 * Se l'oggetto {@code Integer} passato è 0 allora ritorna {@code null}, altrimenti l'oggetto stesso
	 *
	 * @param value oggetto da analizzare
	 * @return {@code null} se {@code value} è 0, {@code value} altrimenti (potrebbe essere {@code null})
	 */
	public static Integer zeroToNull(final @Nullable Integer value) {
		return INTEGER_ZERO.equals(value) ? null : value;
	}

	/**
	 * Se l'oggetto {@code BigDecimal} passato è 0 allora ritorna {@code null}, altrimenti l'oggetto stesso
	 *
	 * @param value oggetto da analizzare
	 * @return {@code null} se {@code value} è 0, {@code value} altrimenti (potrebbe essere {@code null}).
	 */
	public static BigDecimal zeroToNull(final @Nullable BigDecimal value) {
		return BigDecimal.ZERO.compareTo(removeNull(value)) == 0 ? null : value;
	}

	/**
	 * Se l'oggetto {@code BigDecimal} passato {@code null} restituisce {@code null},
	 * altrimenti un nuovo oggetto {@code BigDecimal} negato.
	 *
	 * @param value oggetto da analizzare.
	 */
	public static BigDecimal negate(final @Nullable BigDecimal value) {
		return value != null ? value.negate() : null;
	}

	/**
	 * Se l'oggetto {@code Integer} passato {@code null} restituisce {@code null},
	 * altrimenti un oggetto {@code Integer} negato.
	 *
	 * @param value oggetto da analizzare.
	 */
	public static Integer negate(final @Nullable Integer value) {
		return value != null ? Integer.valueOf(-value.intValue()) : null;
	}

	/**
	 * Se l'oggetto {@code Long} passato {@code null} restituisce {@code null},
	 * altrimenti un oggetto {@code Long} negato.
	 *
	 * @param value oggetto da analizzare.
	 */
	public static Long negate(final @Nullable Long value) {
		return value != null ? Long.valueOf(-value.longValue()) : null;
	}

	/**
	 * Restituisce un oggetto {@code Integer} corrispondente
	 * all'intero successivo a quello specificato (obbligatorio).
	 *
	 * <pre>
	 * Optional.ofNullable(n).map(NumberUtils::increment).orElse(Integer.valueOf(1));
	 * </pre>
	 *
	 * @param n numero intero.
	 */
	public static @NonNull Integer increment(final @NonNull Integer n) {
		return Integer.valueOf(n.intValue() + 1);
	}

	/**
	 * Restituisce un oggetto {@code Integer} corrispondente
	 * all'intero precedente a quello specificato (obbligatorio).
	 *
	 * <pre>
	 * Optional.ofNullable(n).map(NumberUtils::decrement).orElse(Integer.valueOf(0));
	 * </pre>
	 *
	 * @param n numero intero.
	 */
	public static @NonNull Integer decrement(final @NonNull Integer n) {
		return Integer.valueOf(n.intValue() - 1);
	}

	/**
	 * Restituisce un oggetto {@code Integer} corrispondente
	 * alla somma dei 2 valori int rappresentati dagli
	 * oggetti {@code Integer} specificati (obbligatori).
	 *
	 * @param a primo addendo.
	 * @param b secondo addendo.
	 */
	public static @NonNull Integer sum(final @NonNull Integer a, final @NonNull Integer b) {
		return Integer.valueOf(a.intValue() + b.intValue());
	}

	/**
	 * Restituisce un oggetto {@code Long} corrispondente
	 * alla somma dei 2 valori long rappresentati dagli
	 * oggetti {@code Long} specificati (obbligatori).
	 *
	 * @param a primo addendo.
	 * @param b secondo addendo.
	 */
	public static @NonNull Long sum(final @NonNull Long a, final @NonNull Long b) {
		return Long.valueOf(a.longValue() + b.longValue());
	}

	/**
	 * Restituisce un oggetto {@code Integer} corrispondente
	 * alla differenza dei 2 valori int rappresentati dagli
	 * oggetti {@code Integer} specificati (obbligatori).
	 *
	 * @param a minuendo.
	 * @param b sottraendo.
	 */
	public static @NonNull Integer diff(final @NonNull Integer a, final @NonNull Integer b) {
		return Integer.valueOf(a.intValue() - b.intValue());
	}

	/**
	 * Restituisce un oggetto {@code Long} corrispondente
	 * alla differenza dei 2 valori long rappresentati dagli
	 * oggetti {@code Long} specificati (obbligatori).
	 *
	 * @param a minuendo.
	 * @param b sottraendo.
	 */
	public static @NonNull Long diff(final @NonNull Long a, final @NonNull Long b) {
		return Long.valueOf(a.longValue() - b.longValue());
	}

	//

	/**
	 * Verifica se un oggetto {@code Integer} rappresenta il primitivo
	 * numerico indicato;
	 *
	 * @param n l'oggetto da verificare.
	 * @return {@code true} se l'oggetto non è {@code null} e
	 *         il suo valore è uguale a {@code value}.
	 */
	public static boolean equals(final Integer n, final int value) {
		return n != null && value == n.intValue();
	}

	/**
	 * Verifica se un oggetto {@code Integer} non rappresenta il primitivo
	 * numerico indicato;
	 *
	 * @param n l'oggetto da verificare.
	 * @return {@code true} se l'oggetto non è {@code null} e
	 *         il suo valore è diverso da {@code value}.
	 */
	public static boolean notEquals(final Integer n, final int value) {
		return n != null && value != n.intValue();
	}

	/**
	 * Verifica se un oggetto rappresenta un primitivo
	 * numerico positivo o uguale a zero.
	 *
	 * @param n l'oggetto da verificare.
	 * @return {@code true} se l'oggetto non è {@code null} e
	 *         il suo valore è positivo o uguale a zero.
	 */
	public static boolean isPositiveOrZero(final @Nullable Number n) {
		if (n == null) {
			return false;
		}
		if (n instanceof BigDecimal) {
			return ((BigDecimal) n).signum() >= 0;
		}
		return n.doubleValue() >= 0;
	}

	/**
	 * Verifica se un oggetto rappresenta un primitivo
	 * numerico positivo;
	 *
	 * @param n l'oggetto da verificare.
	 * @return {@code true} se l'oggetto non è {@code null} e
	 *         il suo valore è positivo.
	 */
	public static boolean isPositive(final Number n) {
		if (n == null) {
			return false;
		}
		if (n instanceof BigDecimal) {
			return ((BigDecimal) n).signum() > 0;
		}
		return n.doubleValue() > 0;
	}

	/**
	 * Verifica se un oggetto rappresenta un primitivo
	 * numerico negativo;
	 *
	 * @param n l'oggetto da verificare.
	 * @return {@code true} se l'oggetto non è {@code null} e
	 *         il suo valore è negativo.
	 */
	public static boolean isNegative(final Number n) {
		if (n == null) {
			return false;
		}
		if (n instanceof BigDecimal) {
			return ((BigDecimal) n).signum() < 0;
		}
		return n.doubleValue() < 0;
	}

	//

	/**
	 * Effettua la somma degli interi specificati.
	 *
	 * @param values numeri interi da sommare.
	 * @return la somma dei numeri interi specificati.
	 */
	public static int sum(final @NonNull int... values) {
		// JAVA8: IntStream.of(values).sum();
		int total = 0;
		for (final int value : values) {
			total += value;
		}
		return total;
	}

	/**
	 * Se il valore supera min restituisce min, se il valore
	 * supera max restituisce max, altrimenti restituisce il valore stesso.
	 *
	 * @param value valore da controllare.
	 * @param min valore minimo.
	 * @param max valore massimo.
	 */
	public static int minMax(final int value, final int min, final int max) {
		if (value < min) {
			return min;
		}
		if (value > max) {
			return max;
		}
		return value;
	}

	/**
	 * Se il valore supera min restituisce min, se il valore
	 * supera max restituisce max, altrimenti restituisce il valore stesso.
	 *
	 * @param value valore da controllare.
	 * @param min valore minimo.
	 * @param max valore massimo.
	 */
	public static long minMax(final long value, final long min, final long max) {
		if (value < min) {
			return min;
		}
		if (value > max) {
			return max;
		}
		return value;
	}

	/**
	 * Se il valore supera min restituisce min, se il valore
	 * supera max restituisce max, altrimenti restituisce il valore stesso.
	 *
	 * @param value valore da controllare.
	 * @param min valore minimo.
	 * @param max valore massimo.
	 */
	public static double minMax(final double value, final double min, final double max) {
		if (value < min) {
			return min;
		}
		if (value > max) {
			return max;
		}
		return value;
	}

	/**
	 * Restituisce il valore intero minimo tra
	 * quelli indicati.
	 *
	 * @param values array di interi.
	 */
	public static int min(final @NonNull int... values) {
		if (values.length < 1) {
			throw new IllegalArgumentException("Array must have at least one element");
		}
		// JAVA8: IntStream.of(values).min();
		int minValue = Integer.MAX_VALUE;
		for (final int value : values) {
			if (value < minValue) {
				minValue = value;
			}
		}
		return minValue;
	}

	/**
	 * Restituisce il valore intero massimo tra
	 * quelli indicati.
	 *
	 * @param values array di interi.
	 */
	public static int max(final @NonNull int... values) {
		if (values.length < 1) {
			throw new IllegalArgumentException("Array must have at least one element");
		}
		// JAVA8: IntStream.of(values).max();
		int maxValue = Integer.MIN_VALUE;
		for (final int value : values) {
			if (value > maxValue) {
				maxValue = value;
			}
		}
		return maxValue;
	}

	/**
	 * Restituisce il valore intero minimo tra
	 * quelli indicati.
	 *
	 * @param values array di long.
	 */
	public static long min(final @NonNull long... values) {
		// JAVA8: LongStream.of(values).min();
		long minValue = Long.MAX_VALUE;
		for (final long value : values) {
			if (value < minValue) {
				minValue = value;
			}
		}
		return minValue;
	}

	/**
	 * Restituisce il valore intero massimo tra
	 * quelli indicati.
	 *
	 * @param values array di long.
	 */
	public static long max(final @NonNull long... values) {
		// JAVA8: LongStream.of(values).max();
		long maxValue = Long.MIN_VALUE;
		for (final long value : values) {
			if (value > maxValue) {
				maxValue = value;
			}
		}
		return maxValue;
	}

	/**
	 * Restituisce un array di interi che include il
	 * range di numeri in un intervallo definito.
	 *
	 * @param from numero iniziale.
	 * @param to numero finale.
	 * @throws IndexOutOfBoundsException se {@code from > to}.
	 */
	public static int[] intRange(final int from, final int to) {
		// JAVA8: IntStream.range(from, to + 1).toArray();
		final int length = to - from + 1;
		if (length < 0) {
			throw new IndexOutOfBoundsException(from + " > " + to);
		}
		final int[] res = new int[length];
		for (int i = 0; i < length; i++) {
			res[i] = i + from;
		}
		return res;
	}

	/**
	 * Restituisce un array di oggetti {@code Integer} che
	 * include il range di numeri in un intervallo definito.
	 *
	 * @param from numero iniziale.
	 * @param to numero finale.
	 * @throws IndexOutOfBoundsException se {@code from > to}.
	 */
	public static Integer[] integerRange(final int from, final int to) {
		// JAVA8 : IntStream.range(from, to + 1).mapToObj(Integer::valueOf).toArray(Integer[]::new);
		final int length = to - from + 1;
		if (length < 0) {
			throw new IndexOutOfBoundsException(from + " > " + to);
		}
		final Integer[] res = new Integer[length];
		if (from > 127) {
			/*
			 * XXX piccola ottimizzazione:
			 * di default i valori maggiori di 127 non sono
			 * tenuti in cache dalla classe Integer.IntegerCache
			 * percui in tal caso non ha senso usare valueOf(int)
			 */
			for (int i = 0; i < length; i++) {
				res[i] = new Integer(i + from);
			}
		} else {
			for (int i = 0; i < length; i++) {
				res[i] = Integer.valueOf(i + from);
			}
		}
		return res;
	}

	// test if string is number

	/**
	 * Controlla con un espressione regolare se la stringa indicata
	 * è composta da sole cifre.
	 *
	 * @param s stringa da analizzare.
	 */
	public static boolean isDigits(final String s) {
		return s == null || s.isEmpty() ? false : DIGITS_REGEX_PATTERN.matcher(s).matches();
	}

	/**
	 * Controlla se una stringa può essere considerata
	 * un numero valido.
	 *
	 * @param s stringa da analizzare.
	 * @return {@code true} se la stringa non
	 *         è {@code null} o di lunghezza 0.
	 *         Se non supera 19 caratteri (lunghezza
	 *         di {@code Long.MAX_VALUE} ed composta
	 *         da sole cifre ad eccezione del primo
	 *         carattare il quale può essere il
	 *         segno '+' o '-'.
	 */
	public static boolean isIntegerNumber(final String s) {
		if (s == null || s.isEmpty()) {
			return false;
		}
		int i = 0;
		final char first = s.charAt(0);
		if (first == '-' || first == '+') {
			if (s.length() == 1) {
				return false;
			}
			i++;
		}
		if (s.length() > i + MAX_LENGTH) {
			return false;
		}
		for (; i < s.length(); i++) {
			if (!Character.isDigit(s.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Restituisce il numero di cifre di un numero intero lungo.
	 *
	 * @param num il numero da analizzare.
	 * @return il numero di cifre.
	 */
	public static int length(final long num) {
		if (num == 0) {
			return 1;
		}
		if (num < 0) {
			if (num == Long.MIN_VALUE) {
				return (int) Math.log10(-(Long.MIN_VALUE + 1)) + 1;
			}
			return (int) Math.log10(-num) + 1;
		}
		return (int) Math.log10(num) + 1;
	}

	/**
	 * Restituisce il numero di cifre di un numero intero.
	 *
	 * @param num il numero da analizzare.
	 * @return il numero di cifre.
	 */
	public static int length(final int num) {
		if (num == 0) {
			return 1;
		}
		if (num < 0) {
			if (num == Integer.MIN_VALUE) {
				return (int) Math.log10(-(Integer.MIN_VALUE + 1)) + 1;
			}
			return (int) Math.log10(-num) + 1;
		}
		return (int) Math.log10(num) + 1;
	}

	// rounding

	/**
	 * Arrotonda per eccesso ai decimali specificati.
	 *
	 * @param num numero da arrotondare.
	 * @param decimali numero di decimali.
	 * @return il numero arrotondato.
	 */
	public static double round(final double num, final int decimali) {
		return round(num, decimali, RoundingMode.HALF_UP);
	}

	/**
	 * Arrotonda ai decimali specificati con il metodo di arrotondamento specificato.
	 *
	 * @param num numero da arrotondare.
	 * @param decimali numero di decimali.
	 * @param roundingMode metodo di arrotondamento.
	 * @return il numero arrotondato.
	 */
	public static double round(final double num, final int decimali, final @NonNull RoundingMode roundingMode) {
		if (Double.isInfinite(num) || Double.isNaN(num)) {
			/*
			 * devo controllare se il numero è infinito o "not a number"
			 * per non far saltare il costruttore BigDecimal
			 */
			return num;
		}
		return new BigDecimal(num).setScale(decimali, roundingMode).doubleValue();
	}

	/**
	 * Arrotonda ai decimali specificati.
	 * Se l'oggetto {@code Double} passato è {@code null},
	 * infinito o {@code NaN} restituisce l'oggetto stesso.
	 *
	 * @param num numero da arrotondare.
	 * @param decimali numero di decimali.
	 * @param tipo tipo di arrotondamento.
	 * @return il numero arrotondato.
	 * @throws IllegalArgumentException se il numero di decimali specificato è negativo.
	 */
	public static Double round(final Double num, final int decimali, final @NonNull RoundingMode tipo) {
		if (num == null || num.isInfinite() || num.isNaN()) {
			return num;
		}
		return Double.valueOf(round(num.doubleValue(), decimali, tipo));
	}

	// format

	/**
	 * Formatta un numero con i decimali specificati.
	 * Se il numero è {@code null} restituisce la stringa
	 * indicata.
	 *
	 * @param value numero da formattare.
	 * @param decimali numero di decimali.
	 * @param ifNull stringa da restituire in caso di valore {@code null}.
	 * @return una stringa rappresentante il numero formattato oppure una
	 *         la stringa specificata in caso di valore {@code null}.
	 * @throws IllegalArgumentException se il numero di decimali specificato è negativo.
	 */
	public static String format(final @Nullable Double value, final int decimali, final String ifNull) {
		if (value == null) {
			return ifNull;
		}
		return format(value.doubleValue(), decimali);
	}

	/**
	 * Formatta un numero con i decimali specificati.
	 * Se il numero è {@code null} restituisce {@code null}.
	 *
	 * @param value numero da formattare.
	 * @param decimali numero di decimali.
	 * @return una stringa rappresentante il numero formattato oppure {@code null}.
	 * @throws IllegalArgumentException se il numero di decimali specificato è negativo.
	 */
	public static String format(final @Nullable Double value, final int decimali) {
		return format(value, decimali, null);
	}

	/**
	 * Formatta un numero con i decimali specificati.
	 *
	 * @param value numero da formattare.
	 * @param decimali numero di decimali.
	 * @return una stringa rappresentante il numero formattato.
	 * @throws IllegalArgumentException se il numero di decimali specificato è negativo.
	 */
	@NonNull
	public static String format(final double value, final int decimali) {
		if (decimali < 0) {
			throw new IllegalArgumentException("Number of decimals must be positive");
		}
		final DecimalFormat decimalFormat = new DecimalFormat();
		if (decimali == 0) {
			decimalFormat.applyPattern(DOUBLE_FORMAT);
		} else if (decimali == 1) {
			decimalFormat.applyPattern(DOUBLE_FORMAT + "." + '0');
		} else {
			final char[] filler = new char[decimali];
			Arrays.fill(filler, '0');
			decimalFormat.applyPattern(DOUBLE_FORMAT + "." + new String(filler));
		}
		return decimalFormat.format(value);
	}

	public static DecimalFormat createPattern(final int decimals) {
		if (decimals < 0) {
			throw new IllegalArgumentException("Number of decimals must be positive");
		}
		final DecimalFormat decimalFormat = new DecimalFormat();
		if (decimals == 0) {
			decimalFormat.applyPattern(DOUBLE_FORMAT);
		} else if (decimals == 1) {
			decimalFormat.applyPattern(DOUBLE_FORMAT + "." + '0');
		} else {
			final char[] filler = new char[decimals];
			Arrays.fill(filler, '0');
			decimalFormat.applyPattern(DOUBLE_FORMAT + "." + new String(filler));
		}
		return decimalFormat;
	}

	/**
	 * Formatta un intero secondo il pattern specificato.
	 *
	 * @param value numero da formattare.
	 * @param pattern costante rappresentante il pattern
	 *            da utilizzare.
	 * @return una stringa rappresentante il numero formattato.
	 *
	 * @since 27/10/2014
	 */
	public static String format(final int value, final NumberPattern pattern) {
		synchronized (pattern.formatter) {
			return pattern.formatter.format(value);
		}
	}

	/**
	 * Formatta un intero long secondo il pattern specificato.
	 *
	 * @param value numero da formattare.
	 * @param pattern costante rappresentante il pattern
	 *            da utilizzare.
	 * @return una stringa rappresentante il numero formattato.
	 *
	 * @since 27/10/2014
	 */
	public static String format(final long value, final NumberPattern pattern) {
		synchronized (pattern.formatter) {
			return pattern.formatter.format(value);
		}
	}

	/**
	 * Formatta un numero {@code float} secondo il pattern specificato.
	 *
	 * @param value numero da formattare.
	 * @param pattern costante rappresentante il pattern
	 *            da utilizzare.
	 * @return una stringa rappresentante il numero formattato.
	 *
	 * @since 27/10/2014
	 */
	public static String format(final float value, final NumberPattern pattern) {
		synchronized (pattern.formatter) {
			return pattern.formatter.format(value);
		}
	}

	/**
	 * Formatta un numero {@code double} secondo il pattern specificato.
	 *
	 * @param value numero da formattare.
	 * @param pattern costante rappresentante il pattern
	 *            da utilizzare.
	 * @return una stringa rappresentante il numero formattato.
	 *
	 * @since 27/10/2014
	 */
	public static String format(final double value, final NumberPattern pattern) {
		synchronized (pattern.formatter) {
			return pattern.formatter.format(value);
		}
	}

	/**
	 * Formatta un numero secondo il pattern specificato.
	 *
	 * @param value numero da formattare.
	 * @param pattern costante rappresentante il pattern
	 *            da utilizzare.
	 * @return una stringa rappresentante il numero formattato.
	 *
	 * @since 11/09/2014
	 */
	public static String format(final Number value, final NumberPattern pattern) {
		if (value == null) {
			return "";
		}
		synchronized (pattern.formatter) {
			return pattern.formatter.format(value);
		}
	}

	/**
	 * Formatta un numero secondo il pattern specificato.
	 *
	 * @param value numero da formattare.
	 * @param pattern il pattern da utilizzare.
	 * @return una stringa rappresentante il numero formattato.
	 *
	 * @since 11/09/2014
	 */
	public static String format(final Number value, final String pattern) {
		return value == null ? "" : format(value, new DecimalFormat(pattern));
	}

	/**
	 * Formatta un numero con il formatter specificato.
	 *
	 * @param value numero da formattare.
	 * @param formatter l'oggetto formatter.
	 * @return una stringa rappresentante il numero formattato.
	 *
	 * @since 11/09/2014
	 */
	public static String format(final Number value, final NumberFormat formatter) {
		if (value == null) {
			return "";
		}
		return formatter.format(value);
	}

	/**
	 * Formatta un numero con il numero di decimali indicato.
	 *
	 * @param value numero da formattare.
	 * @param decimals numero di decimali (positivo).
	 * @return l'oggetto formattato con numero di decimali indicato.
	 *         Se è {@code null} allora stringa vuota.
	 *
	 * @since 08/10/2015
	 */
	public static String format(final Number value, final int decimals) {
		if (value == null) {
			return "";
		}
		final NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(decimals);
		nf.setMinimumFractionDigits(decimals);
		return nf.format(value);
	}

	public static BigDecimal parseBigDecimal(final String value) {
		if (StringUtils.isNullOrBlank(value)) {
			return null;
		}
		final NumberFormat nf = NumberFormat.getInstance();
		if (nf instanceof DecimalFormat) {
			((DecimalFormat) nf).setParseBigDecimal(true);
		} else {
			// non dovrebbe mai accadere
			LOGGER.warn("NumberFormat is not a DecimalFormat but {}", nf.getClass());
		}
		try {
			return toBigDecimal(nf.parse(value));
		} catch (final @SuppressWarnings("unused") ParseException ex) {
			return null;
		}
	}

	// hashCode

	/**
	 * Genera l'hashcode a partire da una variabile di tipo long.
	 */
	public static int hashCode(final long value) {
		return (int) (value ^ value >>> 32);
	}

	/**
	 * Genera l'hashcode a partire da un oggetto {@code Long}.
	 */
	public static int hashCode(final Long value) {
		return value == null ? 0 : hashCode(value.longValue());
	}

	//

	/**
	 * Restituisce la rappresentazione in stringa dell'oggetto {@code Number} passato.
	 * Se l'oggetto in questione è {@code null} restituisce una stringa vuota.
	 *
	 * @see Objects#toString(Object, String)
	 */
	@NonNull
	public static String toStringSafe(final Number value) {
		return Objects.toString(value, "");
	}
}
