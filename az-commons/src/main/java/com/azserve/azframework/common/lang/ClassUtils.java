package com.azserve.azframework.common.lang;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Classe di utilità per operare su oggetti {@code Class}.
 *
 * @author filippo
 * @since 29/10/2013
 */
public final class ClassUtils {

	private static final Map<Class<?>, Class<?>> PRIMITIVES;
	private static final Map<Class<?>, Class<?>> WRAPPERS;

	static {
		PRIMITIVES = new HashMap<>();
		PRIMITIVES.put(Boolean.class, Boolean.TYPE);
		PRIMITIVES.put(Byte.class, Byte.TYPE);
		PRIMITIVES.put(Character.class, Character.TYPE);
		PRIMITIVES.put(Double.class, Double.TYPE);
		PRIMITIVES.put(Float.class, Float.TYPE);
		PRIMITIVES.put(Integer.class, Integer.TYPE);
		PRIMITIVES.put(Long.class, Long.TYPE);
		PRIMITIVES.put(Short.class, Short.TYPE);
		PRIMITIVES.put(Void.class, Void.TYPE);

		WRAPPERS = new HashMap<>();
		WRAPPERS.put(Boolean.TYPE, Boolean.class);
		WRAPPERS.put(Byte.TYPE, Byte.class);
		WRAPPERS.put(Character.TYPE, Character.class);
		WRAPPERS.put(Double.TYPE, Double.class);
		WRAPPERS.put(Float.TYPE, Float.class);
		WRAPPERS.put(Integer.TYPE, Integer.class);
		WRAPPERS.put(Long.TYPE, Long.class);
		WRAPPERS.put(Short.TYPE, Short.class);
		WRAPPERS.put(Void.TYPE, Void.class);
	}

	private ClassUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Effettua un cast.
	 *
	 * <code>
	 * Class<DtoReferenceWithString<D>> class = (Class<DtoReferenceWithString<D>>) (Class<?>) DtoReferenceWithString.class;
	 * </code>
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> uncheckedCast(final Class<?> c) {
		return (Class<T>) c;
	}

	/**
	 * Restituisce il nome della classe dell'oggetto specificato e
	 * in caso di reference {@code null} restituisce una stringa
	 * di default.
	 *
	 * @param obj oggetto da verificare.
	 * @param defaultName nome di default.
	 *
	 * @since 14/08/2015
	 */
	public static String getShortClassName(final Object obj, final String defaultName) {
		return obj != null ? obj.getClass().getSimpleName() : defaultName;
	}

	/**
	 * Restituisce il nome della classe dell'oggetto specificato e
	 * in caso di reference {@code null} restituisce {@code null}.
	 *
	 * @param obj oggetto da verificare.
	 *
	 * @since 14/08/2015
	 */
	public static String getShortClassName(final Object obj) {
		return getShortClassName(obj, null);
	}

	/**
	 * Restituisce la classe dell'oggetto specificato e
	 * in caso di reference {@code null} restituisce {@code null}.
	 *
	 * @param obj oggetto da verificare.
	 *
	 * @since 27/06/2014
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<? extends T> getObjectClass(final T obj) {
		return obj == null ? null : (Class<? extends T>) obj.getClass();
	}

	/**
	 * Restituisce tutta la sua gerarchia di superclassi
	 * della classe specificata, dal padre agli antenati.
	 *
	 * @param clazz classe da analizzare.
	 *
	 * @since 14/08/2015
	 */
	public static List<Class<?>> getSuperclasses(final @NonNull Class<?> clazz) {
		Class<?> cls = requireNonNull(clazz).getSuperclass();
		final List<Class<?>> result = new ArrayList<>();
		while (cls != null) {
			result.add(cls);
			cls = cls.getSuperclass();
		}
		return result;
	}

	/**
	 * Restituisce tutte le interfacce implementate dalla classe
	 * specificata e da tutta la sua gerarchia di superclassi.
	 *
	 * @param clazz classe da analizzare.
	 *
	 * @since 14/08/2015
	 */
	public static Set<Class<?>> getInterfaces(final Class<?> clazz) {
		final Set<Class<?>> result = new HashSet<>();
		getInterfaces(clazz, result);
		return result;
	}

	private static void getInterfaces(final Class<?> clazz, final Set<Class<?>> result) {
		Class<?> cls = clazz;
		while (cls != null) {
			for (final Class<?> iface : cls.getInterfaces()) {
				if (!result.contains(iface)) {
					result.add(iface);
					getInterfaces(iface, result);
				}
			}
			cls = cls.getSuperclass();
		}
	}

	/**
	 * Verifica se una classe rappresenta un tipo
	 * primitivo numerico.
	 *
	 * <pre>
	 * isPrimitiveNumber(int.class) -> true
	 * isPrimitiveNumber(float.class) -> true
	 * isPrimitiveNumber(char.class) -> false
	 * isPrimitiveNumber(Integer.class) -> false
	 * isPrimitiveNumber(Object.class) -> false
	 * </pre>
	 *
	 * @param cls classe da verificare.
	 * @throws NullPointerException se la classe è {@code null}.
	 *
	 * @since 29/10/2013
	 */
	public static boolean isPrimitiveNumber(final @NonNull Class<?> cls) {
		return requireNonNull(cls).isPrimitive()
				&& cls != char.class
				&& cls != boolean.class
				&& cls != void.class;
	}

	/**
	 * Restituisce la classe del tipo primitivo
	 * corrispondente alla classe involucro
	 * specificata.
	 */
	public static Class<?> getPrimitive(final Class<?> cls) {
		return PRIMITIVES.get(cls);
	}

	/**
	 * Restituisce la classe involucro corrispondente
	 * a quella del tipo primitivo specificato.
	 */
	public static Class<?> getWrapper(final Class<?> cls) {
		return WRAPPERS.get(cls);
	}
}
