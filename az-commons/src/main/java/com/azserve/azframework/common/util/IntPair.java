package com.azserve.azframework.common.util;

import java.io.Serializable;

import com.azserve.azframework.common.annotation.NonNull;

public class IntPair implements Serializable {

	private static final long serialVersionUID = -206481018509927551L;

	private final int value1;
	private final int value2;

	public static @NonNull IntPair of(final int value1, final int value2) {
		return new IntPair(value1, value2);
	}

	private IntPair(final int value1, final int value2) {
		this.value1 = value1;
		this.value2 = value2;
	}

	public int getValue1() {
		return this.value1;
	}

	public int getValue2() {
		return this.value2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.value1;
		result = prime * result + this.value2;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final IntPair other = (IntPair) obj;
		if (this.value1 != other.value1) {
			return false;
		}
		if (this.value2 != other.value2) {
			return false;
		}
		return true;
	}
}
