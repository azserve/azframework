package com.azserve.azframework.common.ex;

import static java.util.Objects.requireNonNull;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * This class consists of {@code static} utility methods for operating
 * on exceptions.
 */
public final class ExceptionUtils {

	private ExceptionUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Provides the {@code Throwable}'s stacktrace as string.
	 */
	public static @NonNull String createStackTrace(final @NonNull Throwable exception) {
		final StringWriter stringWriter = new StringWriter();
		try (final PrintWriter printWriter = new PrintWriter(stringWriter)) {
			exception.printStackTrace(printWriter);
			return stringWriter.toString();
		}
	}

	/**
	 * Reads the stacktrace and checks if an element
	 * starting with "java.lang.reflect" is found.
	 */
	public static boolean checkReflection() {
		final Exception ex = new Exception();
		for (final StackTraceElement s : ex.getStackTrace()) {
			if (s.getClassName().startsWith("java.lang.reflect")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Retrieves the first cause of the {@code Throwable}, if present, or else
	 * the {@code Throwable} itself.
	 */
	public static @NonNull Throwable getFirstCause(final @NonNull Throwable t) {
		final Throwable cause = requireNonNull(t).getCause();
		if (cause == null) {
			return t;
		}
		return getFirstCause(cause);
	}
}
