package com.azserve.azframework.common.debug;

import java.util.function.Supplier;

import org.slf4j.Logger;

public final class DebugUtils {

	/**
	 * Verifica se è impostato il parametro {@code -ea} della jvm.
	 */
	public static boolean assertionEnabled() {
		boolean assertionEnabled = false;
		assert assertionEnabled = true;
		return assertionEnabled;
	}

	public static void log(final Logger logger, final Supplier<String> messageSupplier) {
		if (logger.isDebugEnabled()) {
			logger.debug(messageSupplier.get());
		}
	}

	public static void sleep(final long ms) {
		try {
			Thread.sleep(ms);
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
	}

	public static <T> T print(final T obj) {
		System.out.print(obj);
		return obj;
	}

	public static <T> T println(final T obj) {
		System.out.println(obj);
		return obj;
	}

	private DebugUtils() {}
}
