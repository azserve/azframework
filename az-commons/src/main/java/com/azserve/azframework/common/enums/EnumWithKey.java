package com.azserve.azframework.common.enums;

/**
 * Interfaccia implementata dalle enum
 * che hanno una chiave.
 * 
 * @author filippo
 * @since 09/09/2013
 * 
 * @param <T> tipo della chiave.
 */
public interface EnumWithKey<T> {

	T key();
}