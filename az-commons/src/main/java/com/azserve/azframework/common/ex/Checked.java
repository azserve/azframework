package com.azserve.azframework.common.ex;

import static java.util.Objects.requireNonNull;

import java.util.function.Function;
import java.util.function.Supplier;

import com.azserve.azframework.common.annotation.NonNull;

public final class Checked {

	/**
	 * Returns a composed function that tries to apply
	 * the specified function to its input and, in
	 * case of failure, returns {@code null}.
	 *
	 * <p>Be careful, any exception will be suppressed.</p>
	 *
	 * @param <T> input type
	 * @param <R> output type
	 * @param function the input function
	 * @return a composed function that tries to apply
	 *         the specified function to its input and, in
	 *         case of failure, returns {@code null}.
	 */
	public static <T, R> Function<T, R> applyOrNull(final @NonNull CheckedFunction<T, R, Exception> function) {
		return t -> {
			try {
				return function.apply(t);
			} catch (@SuppressWarnings("unused") final Exception ex) {
				return null;
			}
		};
	}

	/**
	 * Returns a composed function that tries to get
	 * the value from the specified supplier and, in
	 * case of failure, returns {@code null}.
	 *
	 * <p>Be careful, any exception will be suppressed.</p>
	 *
	 * @param <T> output type
	 * @param supplier the input supplier
	 * @return a composed function that tries to get
	 *         the value from the specified supplier and, in
	 *         case of failure, returns {@code null}.
	 */
	public static <T> Supplier<T> getOrNull(final @NonNull CheckedSupplier<T, Exception> supplier) {
		return () -> {
			try {
				return supplier.get();
			} catch (@SuppressWarnings("unused") final Exception ex) {
				return null;
			}
		};
	}

	/**
	 * @throws E
	 */
	public static <T, E extends Exception> CheckedFunction<T, Void, E> consumerToFunction(final @NonNull CheckedConsumer<T, E> consumer) {
		return t -> {
			consumer.accept(t);
			return null;
		};
	}

	/**
	 * @throws E
	 */
	public static <T, U, E extends Exception> CheckedFunction<T, U, E> supplierToFunction(final @NonNull CheckedSupplier<U, E> supplier) {
		return t -> supplier.get();
	}

	/**
	 * Throws a checked {@code Exception}/{@code Throwable}
	 * without the throws declaration.
	 *
	 * @param t
	 * @return
	 * @throws T
	 */
	@SuppressWarnings("unchecked")
	public static <R, T extends Throwable> R sneakyThrow(final @NonNull Throwable t) throws T {
		throw (T) requireNonNull(t);
	}

	private Checked() {}
}
