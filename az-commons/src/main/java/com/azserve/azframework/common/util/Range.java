package com.azserve.azframework.common.util;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Classe immutabile che rappresenta un intervallo immutabile di valori
 * finito o infinito.
 *
 * <p>Il valore {@code null} rappresenta l'infinito.</p>
 *
 * @author filippo
 * @since 24/03/2015
 *
 * @param <C> tipo di valore.
 */
public class Range<C> implements Comparable<Range<C>>, Serializable {

	private static final long serialVersionUID = -7554577129972955934L;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static final Range INFINITE = new Range((o1, o2) -> 0, null, null);

	private final Comparator<C> comparator;
	private final C min;
	private final C max;

	/**
	 * Crea un intervallo tra i due valori specificati.
	 *
	 * Il confronto di valori avviene tramite l'oggetto {@code Comparator} specificato.
	 *
	 * @param min valore minimo dell'intervallo.
	 * @param max valore massimo dell'intervallo.
	 * @throws NullPointerException se il {@code Comparator} è {@code null}.
	 */
	public static @NonNull <C> Range<C> of(final @NonNull Comparator<C> comparator, final C min, final C max) {
		return new Range<>(comparator, min, max);
	}

	/**
	 * Crea un intervallo tra i due valori specificati.
	 *
	 * @param min valore minimo dell'intervallo.
	 * @param max valore massimo dell'intervallo.
	 */
	public static @NonNull <C extends Comparable<? super C>> Range<C> of(final C min, final C max) {
		return of(Comparator.naturalOrder(), min, max);
	}

	/**
	 * Crea un intervallo a partire dal valore specificato.
	 *
	 * Il confronto di valori avviene tramite l'oggetto {@code Comparator} specificato.
	 *
	 * @param min valore minimo dell'intervallo.
	 * @throws NullPointerException se il {@code Comparator} è {@code null}.
	 */
	public static @NonNull <C> Range<C> from(final @NonNull Comparator<C> comparator, final C min) {
		return new Range<>(comparator, min, null);
	}

	/**
	 * Crea un intervallo a partire dal valore specificato.
	 *
	 * @param min valore minimo dell'intervallo.
	 */
	public static @NonNull <C extends Comparable<? super C>> Range<C> from(final C min) {
		return from(Comparator.naturalOrder(), min);
	}

	/**
	 * Crea un intervallo fino dal valore specificato.
	 *
	 * Il confronto di valori avviene tramite l'oggetto {@code Comparator} specificato.
	 *
	 * @param max valore massimo dell'intervallo.
	 * @throws NullPointerException se il {@code Comparator} è {@code null}.
	 */
	public static @NonNull <C> Range<C> to(final @NonNull Comparator<C> comparator, final C max) {
		return new Range<>(comparator, null, max);
	}

	/**
	 * Crea un intervallo fino al valore specificato.
	 *
	 * @param max valore massimo dell'intervallo.
	 */
	public static @NonNull <C extends Comparable<? super C>> Range<C> to(final C max) {
		return to(Comparator.naturalOrder(), max);
	}

	/**
	 * Crea un intervallo composto da un solo valore.
	 *
	 * Il confronto di valori avviene tramite l'oggetto {@code Comparator} specificato.
	 *
	 * @param obj valore dell'intervallo.
	 * @throws NullPointerException se il {@code Comparator} è {@code null}.
	 */
	public static @NonNull <C> Range<C> of(final @NonNull Comparator<C> comparator, final C obj) {
		return new Range<>(comparator, obj, obj);
	}

	/**
	 * Crea un intervallo composto da un solo valore.
	 *
	 * @param obj valore dell'intervallo.
	 */
	public static @NonNull <C extends Comparable<? super C>> Range<C> of(final C obj) {
		return of(Comparator.naturalOrder(), obj);
	}

	/**
	 * Crea un intervallo infinito.
	 */
	@SuppressWarnings("unchecked")
	public static @NonNull <C> Range<C> infinite() {
		return INFINITE;
	}

	/**
	 * Verifica se l'intervallo specificato è valido.
	 *
	 * @param min valore minimo dell'intervallo.
	 * @param max valore massimo dell'intervallo.
	 */
	public static <C extends Comparable<? super C>> boolean isValid(final C min, final C max) {
		return isValid(min, max, Comparator.naturalOrder());
	}

	/**
	 * Verifica se l'intervallo specificato è valido.
	 *
	 * Il confronto di valori avviene tramite l'oggetto {@code Comparator} specificato.
	 *
	 * @param min valore minimo dell'intervallo.
	 * @param max valore massimo dell'intervallo.
	 * @throws NullPointerException se il {@code Comparator} è {@code null}.
	 */
	public static <C> boolean isValid(final C min, final C max, final @NonNull Comparator<C> comparator) {
		Objects.requireNonNull(comparator, "Comparator cannot be null");
		return min == null || max == null || comparator.compare(min, max) <= 0;
	}

	private Range(final @NonNull Comparator<C> comparator, final C min, final C max) {
		this.comparator = Objects.requireNonNull(comparator, "Comparator cannot be null");

		if (!isValid(min, max, comparator)) {
			throw new IllegalArgumentException("Min value cannot be greater than Max value");
		}
		this.min = min;
		this.max = max;
	}

	/**
	 * Verifica se il valore specificato è compreso nell'intervallo
	 * escludendo gli estremi, {@code null} non è mai compreso.
	 *
	 * <p>Un range con stesso valore agli estremi restituirà sempre {@code false}.</p>
	 *
	 * @param obj valore da verificare.
	 */
	public boolean containsExclusive(final C obj) {
		return this.contains(obj, true, true);
	}

	/**
	 * Verifica se il valore specificato è compreso nell'intervallo
	 * includendo gli estremi, {@code null} non è mai compreso.
	 *
	 * @param obj valore da verificare.
	 */
	public boolean containsInclusive(final C obj) {
		return this.contains(obj, false, false);
	}

	/**
	 * Verifica se il valore specificato è compreso nell'intervallo
	 * escludendo l'estremo superiore, {@code null} non è mai compreso.
	 *
	 * <p>Un range con stesso valore agli estremi restituirà sempre {@code false}.</p>
	 *
	 * <pre>
	 * Range range = Range.of(0,10);
	 * range.contains(0) -> true
	 * range.contains(5) -> true
	 * range.contains(10) -> false
	 * Range.of(10).contains(10) -> false
	 * </pre>
	 *
	 * @param obj valore da verificare.
	 */
	public boolean contains(final C obj) {
		return this.contains(obj, false, true);
	}

	/**
	 * Verifica se il valore specificato è compreso nell'intervallo,
	 * {@code null} non è mai compreso.
	 *
	 * @param obj valore da verificare.
	 * @param excludeMin indica se escludere il valore minimo dell'intervallo.
	 * @param excludeMax indica se escludere il valore massimo dell'intervallo.
	 */
	public boolean contains(final C obj, final boolean excludeMin, final boolean excludeMax) {
		if (obj == null) {
			return false;
		}

		final int minFactor = excludeMin ? -1 : 0;
		final int maxFactor = excludeMax ? 1 : 0;

		if (this.min == null && this.max == null) {
			return true;
		}
		if (this.min == null) {
			return this.comparator.compare(this.max, obj) >= maxFactor;
		}
		if (this.max == null) {
			return this.comparator.compare(this.min, obj) <= minFactor;
		}
		return this.comparator.compare(this.min, obj) <= minFactor
				&& this.comparator.compare(this.max, obj) >= maxFactor;
	}

	/**
	 * Verifica se l'intero intervallo si trova prima
	 * del valore specificato.
	 * <br/>
	 * La condizione non si verifica mai in caso di
	 * valore massimo dell'intervallo infinito
	 * o valore specificato {@code null}.
	 *
	 * @param obj valore da verificare.
	 */
	public boolean isBefore(final C obj) {
		if (obj == null || this.max == null) {
			return false;
		}
		return this.comparator.compare(this.max, obj) < 0;
	}

	/**
	 * Verifica se l'intero intervallo si trova dopo
	 * del valore specificato.
	 * <br/>
	 * La condizione non si verifica mai in caso di
	 * valore minimo dell'intervallo infinito
	 * o valore specificato {@code null}.
	 *
	 * @param obj valore da verificare.
	 */
	public boolean isAfter(final C obj) {
		if (obj == null || this.min == null) {
			return false;
		}
		return this.comparator.compare(this.min, obj) > 0;
	}

	/**
	 * Verifica se l'intervallo inizia prima di
	 * quello specificato.
	 *
	 * @param range intervallo da verificare.
	 * @throws NullPointerException se l'intervallo specificato
	 *             è {@code null}.
	 */
	public boolean startsBefore(final @NonNull Range<C> range) {
		Objects.requireNonNull(range, "Range cannot be null");
		if (this.min == null) {
			return range.min != null;
		}
		if (range.min == null) {
			return false;
		}
		return this.comparator.compare(this.min, range.min) < 0;
	}

	/**
	 * Verifica se l'intervallo finisce prima di
	 * quello specificato.
	 *
	 * @param range intervallo da verificare.
	 * @throws NullPointerException se l'intervallo specificato
	 *             è {@code null}.
	 */
	public boolean endsBefore(final @NonNull Range<C> range) {
		Objects.requireNonNull(range, "Range cannot be null");
		if (this.max == null) {
			return false;
		}
		if (range.max == null) {
			return this.max != null;
		}
		return this.comparator.compare(this.max, range.max) < 0;
	}

	/**
	 * Verifica se un intervallo tra i due valori specificati si
	 * interseca con questo.
	 *
	 * @param otherMin valore minimo.
	 * @param otherMax valore massimo.
	 */
	public boolean isOverlapping(final C otherMin, final C otherMax) {
		return (this.max == null || otherMin == null || this.comparator.compare(this.max, otherMin) >= 0)
				&& (this.min == null || otherMax == null || this.comparator.compare(this.min, otherMax) <= 0);
	}

	/**
	 * Verifica se l'intervallo specificato si interseca con questo.
	 * <br/>
	 * Per i confronti viene utilizzato l'oggetto {@code Comparator} di
	 * questo intervallo (se i due intervalli ne utilizzano diversi
	 * il risultato potrebbe essere imprevedibile).
	 *
	 * @param range intervallo da verificare.
	 */
	public boolean isOverlapping(final @NonNull Range<C> range) {
		Objects.requireNonNull(range, "Range cannot be null");
		return this.isOverlapping(range.min, range.max);
	}

	/**
	 * Restituisce un nuovo intervallo risultato dell'intersezione
	 * tra questo intervallo e quello specificato, solo se
	 * l'intersezione si verifica.
	 * <br/>
	 * Per i confronti viene utilizzato l'oggetto {@code Comparator} di
	 * questo intervallo (se i due intervalli ne utilizzano diversi
	 * il risultato potrebbe essere imprevedibile).
	 *
	 * @param range intervallo da verificare.
	 * @return un nuovo intervallo risultato dell'intersezione tra
	 *         i 2 oppure {@code null} se non intersecano.
	 */
	public Range<C> intersect(final @NonNull Range<C> range) {
		Objects.requireNonNull(range, "Range cannot be null");

		final C iMin = Comparator.nullsFirst(this.comparator).compare(this.min, range.min) >= 0 ? this.min : range.min;
		final C iMax = Comparator.nullsLast(this.comparator).compare(this.max, range.max) <= 0 ? this.max : range.max;

		if (iMin != null && iMax != null && this.comparator.compare(iMin, iMax) > 0) {
			return null;
		}
		return new Range<>(this.comparator, iMin, iMax);
	}

	/**
	 * Indica se l'intervallo è vuoto ossia il valore minimo
	 * equivale al valore massimo.
	 */
	public boolean isEmpty() {
		return this.min != null && this.max != null
				&& this.comparator.compare(this.min, this.max) == 0;
	}

	/**
	 * Indica se l'intervallo è chiuso ossia il valore minimo
	 * e il valore massimo non sono {@code null}.
	 */
	public boolean isClosed() {
		return this.min != null && this.max != null;
	}

	/**
	 * Indica se l'intervallo è infinito ossia il valore minimo
	 * e il valore massimo sono {@code null}.
	 */
	public boolean isInfinite() {
		return this.min == null && this.max == null;
	}

	/**
	 * Restituisce il valore minimo dell'intervallo, {@code null} se infinito.
	 */
	public C getMin() {
		return this.min;
	}

	/**
	 * Restituisce il valore massimo dell'intervallo, {@code null} se infinito.
	 */
	public C getMax() {
		return this.max;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.max == null ? 0 : this.max.hashCode());
		result = prime * result + (this.min == null ? 0 : this.min.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		@SuppressWarnings("unchecked")
		final Range<C> other = (Range<C>) obj;
		if (this.max == null) {
			if (other.max != null) {
				return false;
			}
		} else if (!this.max.equals(other.max)) {
			return false;
		}
		if (this.min == null) {
			if (other.min != null) {
				return false;
			}
		} else if (!this.min.equals(other.min)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "{" + this.min + " - " + this.max + "}";
	}

	@Override
	public int compareTo(final Range<C> o) {
		if (this.equals(o)) {
			return 0;
		}
		int c = Comparator.nullsFirst(this.comparator).compare(this.min, o.min);
		if (c == 0) {
			c = Comparator.nullsLast(this.comparator).compare(this.max, o.max);
		}
		return c;
	}
}