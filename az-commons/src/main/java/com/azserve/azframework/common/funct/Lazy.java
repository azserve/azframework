package com.azserve.azframework.common.funct;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.azserve.azframework.common.annotation.NonNull;

public final class Lazy<T> implements Supplier<T>, Serializable {

	private static final long serialVersionUID = 4689762335535569551L;

	private Supplier<? extends T> supplier;
	private T value;

	private Lazy(final Supplier<? extends T> supplier) {
		this.supplier = supplier;
	}

	@SuppressWarnings("unchecked")
	public static <T> Lazy<T> of(final @NonNull Supplier<? extends T> supplier) {
		requireNonNull(supplier);
		if (supplier instanceof Lazy) {
			return (Lazy<T>) supplier;
		}
		return new Lazy<>(supplier);
	}

	@Override
	public T get() {
		return this.supplier == null ? this.value : this.computeValue();
	}

	@SuppressWarnings("unchecked")
	public static <T> T proxyInterface(final Supplier<? extends T> supplier, final Class<T> interfaceType) {
		requireNonNull(supplier);
		requireNonNull(interfaceType);
		if (!interfaceType.isInterface()) {
			throw new IllegalArgumentException("Type must be an interface");
		}
		final Lazy<T> lazy = Lazy.of(supplier);
		final InvocationHandler handler = (proxy, method, args) -> method.invoke(lazy.get(), args);
		return (T) Proxy.newProxyInstance(interfaceType.getClassLoader(), new Class<?>[] { interfaceType }, handler);
	}

	public Optional<T> filter(final Predicate<? super T> predicate) {
		requireNonNull(predicate);
		final T val = this.get();
		return predicate.test(val) ? Optional.ofNullable(val) : Optional.empty();
	}

	public <U> Lazy<U> map(final Function<? super T, ? extends U> mapper) {
		requireNonNull(mapper);
		return of(() -> mapper.apply(this.get()));
	}

	public boolean isComputed() {
		return this.supplier == null;
	}

	private T computeValue() {
		final Supplier<? extends T> s = this.supplier;
		if (s != null) {
			this.value = s.get();
			this.supplier = null;
		}
		return this.value;
	}

	private void writeObject(final ObjectOutputStream s) throws IOException {
		this.get();
		s.defaultWriteObject();
	}
}
