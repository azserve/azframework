package com.azserve.azframework.common.ex;

import static java.util.Objects.requireNonNull;

import java.util.function.Consumer;

public interface CheckedConsumer<T, E extends Exception> {

	void accept(T t) throws E;

	/**
	 * Unwraps this checked consumer to an unchecked one.
	 */
	default Consumer<T> unchecked() {
		return t -> {
			try {
				this.accept(t);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
			}
		};
	}

	/**
	 * Provides a way to handle the exception.
	 *
	 * <pre>
	 * CheckedConsumer.of(this::throwException).onError(ex -> logger.logError(ex));
	 * </pre>
	 *
	 * @param consumer the exception handler
	 */
	default Consumer<T> onError(final Consumer<Exception> consumer) {
		return t -> {
			try {
				this.accept(t);
			} catch (final Exception ex) {
				consumer.accept(ex);
			}
		};
	}

	default CheckedConsumer<T, E> andThen(final CheckedConsumer<? super T, ? extends E> consumer) {
		requireNonNull(consumer);
		return t -> {
			this.accept(t);
			consumer.accept(t);
		};
	}

	/**
	 * Creates a {@link CheckedConsumer} by using a method reference.
	 *
	 * @param function the function as method reference
	 * @return the function
	 */
	static <T, E extends Exception> CheckedConsumer<T, E> of(final CheckedConsumer<T, E> consumer) {
		return requireNonNull(consumer);
	}

	/**
	 * Wraps an unchecked consumer in an unchecked one.
	 *
	 * @param consumer the unchecked consumer
	 */
	static <T, E extends Exception> CheckedConsumer<T, E> wrap(final Consumer<T> consumer) {
		return requireNonNull(consumer)::accept;
	}

	/**
	 * Unwraps a checked consumer to have an unchecked one.
	 * The exception is then propagated to this method caller.
	 *
	 * @throws E the propagated exception
	 */
	static <T, E extends Exception> Consumer<T> unwrap(final CheckedConsumer<T, E> consumer) throws E {
		requireNonNull(consumer);
		return t -> {
			try {
				consumer.accept(t);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
			}
		};
	}

	/**
	 * Unwraps a checked consumer to an unchecked one.
	 */
	static <T, E extends Exception> Consumer<T> unchecked(final CheckedConsumer<T, E> consumer) {
		return t -> {
			try {
				consumer.accept(t);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
			}
		};
	}
}
