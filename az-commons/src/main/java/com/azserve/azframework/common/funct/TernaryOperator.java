package com.azserve.azframework.common.funct;

@FunctionalInterface
public interface TernaryOperator<T, R> extends TriFunction<T, T, T, R> {

}
