package com.azserve.azframework.common.util;

/**
 * Classe che gestisce un array di elementi in round robin.
 * Questa classe è thread-safe e non richiede sincronizzazione.
 * 
 * @author luca
 * 
 * @param <T> tipo degli elementi.
 */
public class Ring<T> {

	/** Elementi su cui ciclare. */
	private final T[] array;
	/** Elemento appena letto. */
	private int i = 0;
	/** Numero di elementi. */
	private final int count;
	/** Utilizzato per stabilire se è la prima lettura in next(). */
	private int offset = 0;

	public Ring(final T[] data) {
		this.array = data;
		this.count = data.length;
	}

	/**
	 * Restituisce l'elemento successivo a quello appena letto,
	 * il primo elemento se è la prima lettura.
	 */
	public synchronized T next() {
		return this.getElementAt((this.i + this.offset) % this.count);
	}

	/**
	 * Restituisce l'elemento appena letto,
	 * il primo elemento se è la prima lettura.
	 */
	public synchronized T current() {
		return this.getElementAt(this.i);
	}

	/**
	 * Restituisce l'elemento precedente a quello appena letto,
	 * l'ultimo elemento se è la prima lettura.
	 */
	public synchronized T previous() {
		return this.getElementAt((this.i - 1 + this.count) % this.count);
	}

	private T getElementAt(final int pos) {
		this.offset = 1;
		this.i = pos;
		return this.array[this.i];
	}
}
