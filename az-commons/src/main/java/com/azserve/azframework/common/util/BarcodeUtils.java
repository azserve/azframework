package com.azserve.azframework.common.util;

import java.util.regex.Pattern;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.lang.NumberUtils;

public class BarcodeUtils {

	private BarcodeUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	private static final String BARCODE_REGEX = "^([0-9]{8}|[0-9]{12}|[0-9]{13})$";

	private static final char START = 58;
	private static final char MIDDLE = 42;
	private static final char END = 43;

	private static final char[] A = new char[] { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74 };
	private static final char[] B = new char[] { 75, 76, 77, 78, 79, 80, 81, 82, 83, 84 };
	private static final char[] C = new char[] { 97, 98, 99, 100, 101, 102, 103, 104, 105, 106 };
	private static final char[] D = new char[] { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57 };
	private static final char[] E = new char[] { 107, 108, 109, 110, 111, 112, 113, 114, 115, 116 };
	private static final char[] F = new char[] { 85, 86, 87, 88, 89, 90, 91, 92, 93, 94 };
	private static final char[] G = new char[] { 117, 118, 119, 120, 121, 122, 123, 124, 125, 126 };

	// @formatter:off
	private static final char[][][] EAN13_UPC = new char[][][] {
			new char[][] { A, A, A, A, A, A },
			new char[][] { A, A, B, A, B, B },
			new char[][] { A, A, B, B, A, B },
			new char[][] { A, A, B, B, B, A },
			new char[][] { A, B, A, A, B, B },
			new char[][] { A, B, B, A, A, B },
			new char[][] { A, B, B, B, A, A },
			new char[][] { A, B, A, B, A, B },
			new char[][] { A, B, A, B, B, A },
			new char[][] { A, B, B, A, B, A },
	};
	// @formatter:on

	private static int value(final char c) {
		return c - '0';
	}

	private static char translate(final char c) {
		return (char) (c | 0xF000);
	}

	public static String createBarcode(final @NonNull String barcode) {
		return createBarcode(barcode, BarcodeType.get(barcode));
	}

	public static String createBarcode(final @NonNull String barcode, final @NonNull BarcodeType type) {
		return BarcodeType.EAN8 == type ? createEan8(barcode) : BarcodeType.EAN13 == type ? createEan13(barcode) : createUpc(barcode);
	}

	public static String createEan13(final String barcode) {
		final StringBuilder sb = new StringBuilder();
		final char first = barcode.charAt(0);

		sb.append(translate(D[value(first)]));

		final int digit = first - '0';
		for (int i = 1; i <= 6; i++) {
			sb.append(translate(EAN13_UPC[digit][i - 1][value(barcode.charAt(i))]));
		}

		sb.append(translate(MIDDLE));

		for (int i = 7; i <= 12; i++) {
			sb.append(translate(C[value(barcode.charAt(i))]));
		}

		sb.append(translate(END));

		return sb.toString();
	}

	public static String createEan8(final String barcode) {
		final StringBuilder sb = new StringBuilder();

		sb.append(translate(START));

		for (int i = 1; i <= 4; i++) {
			sb.append(translate(A[value(barcode.charAt(i - 1))]));
		}

		sb.append(translate(MIDDLE));

		for (int i = 5; i <= 8; i++) {
			sb.append(translate(C[value(barcode.charAt(i - 1))]));
		}

		sb.append(translate(END));

		return sb.toString();
	}

	public static String createUpc(final String barcode) {
		final StringBuilder sb = new StringBuilder();

		final int first = value(barcode.charAt(0));
		sb.append(translate(E[first]));
		sb.append(translate(START));
		sb.append(translate(F[first]));

		for (int i = 1; i <= 5; i++) {
			sb.append(translate(A[value(barcode.charAt(i))]));
		}

		sb.append(translate(MIDDLE));

		for (int i = 6; i <= 10; i++) {
			sb.append(translate(C[value(barcode.charAt(i))]));
		}

		final int last = value(barcode.charAt(11));
		sb.append(translate(G[last]));
		sb.append(translate(START));
		sb.append(translate(E[last]));

		return sb.toString();
	}

	public static char calculateEAN13CheckDigit(final @NonNull String s) {
		return calculateCheckDigit(s, BarcodeType.EAN13);
	}

	public static char calculateEAN8CheckDigit(final @NonNull String s) {
		return calculateCheckDigit(s, BarcodeType.EAN8);
	}

	public static char calculateUPCCheckDigit(final @NonNull String s) {
		return calculateCheckDigit(s, BarcodeType.UPC);
	}

	/**
	 * Effettua il calcolo del carattere di controllo.
	 *
	 * @param s codice a barre incompleto.
	 * @param tipo tipo di codice a barre.
	 *
	 * @return il carattere di controllo.
	 * @throws IllegalArgumentException se il tipo di codice
	 *             a barre non è {@code EAN13}, {@code EAN8} o {@code UPC}.
	 */
	public static char calculateCheckDigit(final @NonNull String s, final @NonNull BarcodeType tipo) {
		int evenSum = 0; // somma pari
		int oddSum = 0; // somma dispari

		for (int i = 0; i < s.trim().length(); i++) {
			if (i % 2 == 0) {
				evenSum += Integer.parseInt(s.substring(i, i + 1));
			} else {
				oddSum += Integer.parseInt(s.substring(i, i + 1));
			}
		}

		final int digit;
		switch (tipo) {
			case EAN13:
				digit = (oddSum * 3 + evenSum) % 10;
				break;
			case EAN8:
			case UPC:
				digit = (evenSum * 3 + oddSum) % 10;
				break;
			case CODE128:
			default:
				throw new IllegalArgumentException("Unsupported type \"" + tipo + "\"");
		}

		return Character.forDigit(digit == 0 ? 0 : 10 - digit, 10);
	}

	/**
	 * Questo metodo attualmente non ritorna la codifica 128 della stringa passata per sopperire ad un non ancora compreso problema:
	 * <ul>
	 * <li>Se utilizziamo il font code128 il codice a barre viene correttamente mostrato <b>solo</b> nell'anteprima di stampa. In stampa diretta questo non viene generato</li>
	 * </ul>
	 * Per il code128 utilizziamo la libreria Barbecue-1.5-beta1.jar ed il componente classico di JasperStudio per disegnare i codici a barre.
	 * E' opportuno trovare la soluzione al problema e riuscire ad ottenere anche in stampa diretta un codice corretto tramite il font!
	 *
	 * @param barcode {@link String}
	 * @return barcode {@link String}
	 * @author matteo
	 * @since 20/05/2016
	 */
	public static String createCode128(final String barcode) {
		// if (StringUtils.isNullOrBlank(barcode)) {
		// return null;
		// }
		//
		// final int longueur = barcode.length();
		//
		// for (int i = 0; i < longueur; i++) {
		// if ((barcode.charAt(i) < 32) || (barcode.charAt(i)) > 126) {
		// return null;
		// }
		// }
		//
		// String code128 = "";
		// int dummy;
		// boolean tableB = true;
		// int ind = 0;
		// int mini;
		// while (ind < longueur) {
		// if (tableB) {
		// mini = (ind == 0 || ind + 3 == longueur - 1) ? 4 : 6;
		// mini = mini - 1;
		// if ((ind + mini) <= longueur - 1) {
		// while (mini >= 0) {
		// if ((barcode.charAt(ind + mini) < 48) || (barcode.charAt(ind + mini) > 57)) {
		// break;
		// }
		// mini = mini - 1;
		// }
		// }
		//
		// if (mini < 0) {
		// code128 = ind == 0 ? String.valueOf((char) 210) : code128 + String.valueOf((char) 204);
		// tableB = false;
		// } else {
		// if (ind == 0) {
		// code128 = String.valueOf((char) 209);
		// }
		// }
		// }
		//
		// if (!tableB) {
		// mini = 2;
		// mini = mini - 1;
		// if (ind + mini < longueur) {
		// while (mini >= 0) {
		// if (((barcode.charAt(ind + mini)) < 48) || ((barcode.charAt(ind)) > 57)) {
		// break;
		// }
		// mini = mini - 1;
		// }
		// }
		// if (mini < 0) {
		// dummy = Integer.parseInt(barcode.substring(ind, ind + 2));
		//
		// dummy = dummy < 95 ? dummy + 32 : dummy + 105;
		//
		// code128 = code128 + (char) (dummy);
		//
		// ind = ind + 2;
		// } else {
		// code128 = code128 + String.valueOf((char) 205);
		// tableB = true;
		// }
		// }
		// if (tableB) {
		// code128 = code128 + barcode.charAt(ind);
		// ind = ind + 1;
		// }
		// }
		//
		// int checksum = 0;
		// for (ind = 0; ind <= code128.length() - 1; ind++) {
		// dummy = code128.charAt(ind);
		//
		// dummy = dummy < 127 ? dummy - 32 : dummy - 105;
		//
		// if (ind == 0) {
		// checksum = dummy;
		// }
		// checksum = (checksum + (ind) * dummy) % 103;
		// }
		//
		// checksum = checksum < 95 ? checksum + 32 : checksum + 105;
		//
		// return code128 + String.valueOf((char) checksum) + String.valueOf((char) 211);
		// FIXME
		return barcode;
	}

	/**
	 * Effettua la validazione del codice a barre specificato
	 * e se manca il carattere di controllo lo aggiunge.
	 *
	 * @param value codice a barre da verificare.
	 * @param type tipo di codice a barre da verificare.
	 *
	 * @return il codice a barre stesso se valido e intero,
	 *         il codice a barre con carattere di controllo se valido
	 *         e non intero, oppure {@code null} se il codice a barre
	 *         è invalido.
	 */
	public static String validateComplete(final @NonNull String value, final @NonNull BarcodeType type) {
		if (!value.isEmpty()) {
			if (value.length() == type.length) {
				if (isValid(value)) {
					return value;
				}
			} else if (value.length() == type.length - 1) {
				if (NumberUtils.isDigits(value)) {
					return value + calculateCheckDigit(value, type);
				}
			}
		}
		// codice non valido
		return null;
	}

	/**
	 * Verifica se il valore passato rappresente un codice a barre valido.
	 * <br>Un codice a barre deve essere:
	 * <ul>
	 * <li>lungo 8,12 o 13 caratteri</li>
	 * <li>del tipo xxxxxxxx, xxxxxxxxxxxx, xxxxxxxxxxxxx (dove x è una cifra inclusa tra 0 e 9)</li>
	 * </ul>
	 *
	 * @param value {@link String} da verificare
	 * @return <code>true</code> se il valore è un codice a barre valido. <code>false</code> altrimenti
	 * @author matteo
	 * @since 08/10/2015
	 */
	public static boolean isValid(final String value) {
		if (!Pattern.compile(BARCODE_REGEX).matcher(value).matches()) {
			return false;
		}

		final char digit;
		switch (BarcodeType.get(value)) {
			case EAN8:
				digit = calculateEAN8CheckDigit(value.substring(0, 7));
				break;
			case UPC:
				digit = calculateUPCCheckDigit(value.substring(0, 11));
				break;
			case EAN13:
				digit = calculateEAN13CheckDigit(value.substring(0, 12));
				break;
			case CODE128:
			default:
				throw new IllegalArgumentException("Invalid barcode \"" + value + "\"");
		}

		return digit == value.charAt(value.length() - 1);
	}

	public static enum BarcodeType {

		CODE128(-1),
		EAN8(8),
		EAN13(13),
		UPC(12);

		private final int length;

		BarcodeType(final int length) {
			this.length = length;
		}

		public static BarcodeType get(final @NonNull String barcode) {
			final int len = barcode.length();
			for (final BarcodeType type : values()) {
				if (type.length == len) {
					return type;
				}
			}
			throw new IllegalArgumentException("Invalid barcode type \"" + barcode + "\"");
		}
	}
}
