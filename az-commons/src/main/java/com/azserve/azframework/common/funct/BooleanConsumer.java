package com.azserve.azframework.common.funct;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * Rappresenta un'operazione che accetta un singolo argomento {@code boolean} e
 * non restituisce un risultato. Questa è la specializzazione della classe
 * {@link Consumer} per i valori primitivi {@code boolean}.
 */
@FunctionalInterface
public interface BooleanConsumer {

	/**
	 * Effettua l'operazione sul valore specificato.
	 *
	 * @param value il valore in ingresso.
	 */
	void accept(boolean value);

	/**
	 * Restituisce un {@code BooleanConsumer} composto che esegue, in sequenza, questa
	 * operazione seguita da quella specificata come parametro.
	 */
	default BooleanConsumer andThen(final BooleanConsumer after) {
		Objects.requireNonNull(after);
		return t -> {
			this.accept(t);
			after.accept(t);
		};
	}
}
