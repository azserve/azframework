package com.azserve.azframework.common.util;

import java.io.Serializable;

import com.azserve.azframework.common.annotation.NonNull;

public class ObjIntPair<T> implements Serializable {

	private static final long serialVersionUID = -7297997935814176961L;

	public @NonNull static <V> ObjIntPair<V> of(final V val, final int intValue) {
		return new ObjIntPair<>(val, intValue);
	}

	private final T value;
	private final int intValue;

	public ObjIntPair(final T value, final int intValue) {
		this.value = value;
		this.intValue = intValue;
	}

	public T getValue() {
		return this.value;
	}

	public int getIntValue() {
		return this.intValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.value == null ? 0 : this.value.hashCode());
		result = prime * result + this.intValue;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final ObjIntPair<?> other = (ObjIntPair<?>) obj;
		if (this.value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!this.value.equals(other.value)) {
			return false;
		}
		if (this.intValue != other.intValue) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return this.value + ";" + this.intValue;
	}
}
