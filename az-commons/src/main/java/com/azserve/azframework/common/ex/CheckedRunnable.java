package com.azserve.azframework.common.ex;

import static java.util.Objects.requireNonNull;

import java.util.function.Consumer;

public interface CheckedRunnable<E extends Exception> {

	void run() throws E;

	/**
	 * Unwraps this checked runnable to an unchecked one.
	 */
	default Runnable unchecked() {
		return () -> {
			try {
				this.run();
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
			}
		};
	}

	/**
	 * Provides a way to handle the exception.
	 *
	 * <pre>
	 * CheckedRunnable.of(this::throwException).onError(ex -> logger.logError(ex));
	 * </pre>
	 *
	 * @param consumer the exception handler
	 */
	default Runnable onError(final Consumer<Exception> consumer) {
		return () -> {
			try {
				this.run();
			} catch (final Exception ex) {
				consumer.accept(ex);
			}
		};
	}

	/**
	 * Creates a {@link CheckedRunnable} by using a method reference.
	 *
	 * @param runnable the runnable as method reference
	 * @return the runnable
	 */
	static <E extends Exception> CheckedRunnable<E> of(final CheckedRunnable<E> runnable) {
		return runnable;
	}

	/**
	 * Wraps an unchecked runnable in an unchecked one.
	 *
	 * @param runnable the unchecked runnable
	 */
	static <E extends Exception> CheckedRunnable<E> wrap(final Runnable runnable) {
		return runnable::run;
	}

	/**
	 * Unwraps a checked runnable to have an unchecked one.
	 * The exception is then propagated to this method caller.
	 *
	 * @throws E the propagated exception
	 */
	static <E extends Exception> Runnable unwrap(final CheckedRunnable<E> runnable) throws E {
		requireNonNull(runnable);
		return () -> {
			try {
				runnable.run();
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
			}
		};
	}

	/**
	 * Unwraps a checked runnable to have an unchecked one.
	 */
	static <E extends Exception> Runnable unchecked(final CheckedRunnable<E> runnable) {
		return () -> {
			try {
				runnable.run();
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
			}
		};
	}

	default CheckedRunnable<E> andThen(final CheckedRunnable<? extends E> runnable) {
		return () -> {
			this.run();
			runnable.run();
		};
	}
}
