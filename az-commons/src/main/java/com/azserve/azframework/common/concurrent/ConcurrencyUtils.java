package com.azserve.azframework.common.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * Serie di utilità per l'utilizzo di thread e per
 * la gestione della concorrenza.
 *
 * @author filippo
 * @since 12/11/2014
 */
public final class ConcurrencyUtils {

	private ConcurrencyUtils() {}

	/**
	 * Restituisce un oggetto {@code Future} che esegue una operazione
	 * definita dall'oggetto {@code Callable} specificato.
	 *
	 * @param callable operazione da eseguire.
	 */
	public static <V> Future<V> runTask(final Callable<V> callable) {
		final FutureTask<V> task = new FutureTask<>(callable);
		task.run();
		return task;
	}
}
