package com.azserve.azframework.common.enums;

/**
 * Interfaccia implementata dalle enum
 * che hanno un codice (chiave di tipo int).
 * 
 * @author filippo
 * @since 09/09/2013
 */
public interface EnumWithCode {

	int code();
}