/**
 * <p>
 * Fornisce varie classi di utilità per le operazioni più comuni sui
 * tipi primitivi e gli oggetti involucro corrispondenti.
 * </p>
 */
package com.azserve.azframework.common.lang;