package com.azserve.azframework.common.util;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.lang.StringUtils;

/**
 * Classe di utilità per la gestione di date.
 *
 * @since 31/05/2012
 */
public final class DateUtils {

	/**
	 * Enum dei pattern più comuni utilizzati per lavorare con le date.
	 */
	public enum DatePattern {

		D_M_YY("d/M/yy"),
		D_M_YYYY("d/M/yyyy"),
		DD_MM_YY("dd/MM/yy"),
		DD_MM_YYYY("dd/MM/yyyy"),
		D_MMMM_YYYY("d MMMM yyyy"),
		DDMMYY("ddMMyy"),
		TIME("HH:mm:ss"),
		HOUR("HH:mm"),
		HHMMSS("HHmmss"),
		YYMMDD("yyMMdd"),
		YYYYMMDD("yyyyMMdd"),
		YYYYMMDDHHMMSS("yyyyMMddHHmmss"),
		DD_MM_YYYY_HH_MM("dd/MM/yyyy HH:mm"),
		DD_MM_YYYY_HH_MM_SS("dd/MM/yyyy HH:mm:ss"),
		DESCRIPTION("EEEE d MMMM yyyy"),
		MMMM("MMMM"),
		MMMM_YYYY("MMMM yyyy"),
		SQL_DATE("yyyy-MM-dd"),
		SQL_DATETIME("yyyy-MM-dd HH:mm:ss"),
		XML_TIMESTAMP("yyyy-MM-dd'T'HH:mm:ss'Z'");

		private static final Map<DatePattern, WeakReference<DateFormat>> FORMATTERS = new HashMap<>();

		private final String pattern;

		DatePattern(final String pattern) {
			this.pattern = pattern;
		}

		/**
		 * Restituisce il pattern da utilizzare
		 * con gli oggetti {@code DateFormat}.
		 */
		public String getPattern() {
			return this.pattern;
		}

		/**
		 * Crea un nuovo oggetto {@code DateFormat} a partire
		 * dal pattern.
		 */
		public DateFormat build() {
			final DateFormat df = new SimpleDateFormat(this.pattern);
			df.setLenient(false);
			return df;
		}

		public synchronized DateFormat get() {
			final WeakReference<DateFormat> format = FORMATTERS.get(this);
			DateFormat df = null;
			if (format == null || (df = format.get()) == null) {
				df = this.build();
				FORMATTERS.put(this, new WeakReference<>(df));
			}
			return df;
		}
	}

	// istanze statiche per format e parse
	// il loro utilizzo DEVE essere sincronizzato
	private static final SimpleDateFormat SHARED_FORMATTER;
	private static final SimpleDateFormat SHARED_PARSER;
	private static final DateTimeFormatter SHARED_FORMATTER_LOCALDATETIME = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	private static DatatypeFactory dtf;

	static {
		SHARED_FORMATTER = new SimpleDateFormat(DatePattern.DD_MM_YYYY.getPattern());
		SHARED_FORMATTER.setLenient(false);

		SHARED_PARSER = new SimpleDateFormat(DatePattern.D_M_YY.getPattern());
		SHARED_PARSER.setLenient(false);
	}

	/**
	 * Comparatore per confrontare due date ignorandone ore, minuti e secondi.
	 */
	public static final Comparator<Date> DATE_WITHOUT_TIME_COMPARATOR = (o1, o2) -> DateUtils.getDate(o1).compareTo(DateUtils.getDate(o2));

	/** Numero di ore in un giorno */
	public static final int HOURS_IN_DAY = 24;

	/** Numero di minuti in un'ora */
	public static final int MINUTES_IN_HOUR = 60;

	/** Numero di secondi in un minuto */
	public static final int SECONDS_IN_MINUTE = 60;

	/** Numero di millisecondi che compongono un secondo. */
	public static final long SECOND_MS = 1000;

	/** Numero di millisecondi che compongono un minuto. */
	public static final long MINUTE_MS = SECOND_MS * SECONDS_IN_MINUTE;

	/** Numero di millisecondi che compongono un ora. */
	public static final long HOUR_MS = MINUTE_MS * MINUTES_IN_HOUR;

	/** Numero di millisecondi che compongono un giorno. */
	public static final long DAY_MS = HOUR_MS * HOURS_IN_DAY;

	private DateUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Restituisce la data odierna, inclusa l'ora.
	 *
	 * @return la data odierna, inclusa l'ora.
	 */
	@NonNull
	public static Date now() {
		return new Date();
	}

	/**
	 * Restituisce la data odierna senza ora.
	 *
	 * @return la data odierna senza ora.
	 */
	@NonNull
	public static Date today() {
		return getDateInternal(now());
	}

	/**
	 * Restituisce la data di ieri senza ora.
	 *
	 * @return la data di ieri senza ora.
	 */
	@NonNull
	public static Date yesterday() {
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(now());
		gc.add(Calendar.DATE, -1);
		return new GregorianCalendar(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DAY_OF_MONTH)).getTime();
	}

	/**
	 * Restituisce la data di domani senza ora.
	 *
	 * @return la data di domani senza ora.
	 */
	@NonNull
	public static Date tomorrow() {
		return nextDay(now());
	}

	/**
	 * Restituisce la data corrispondente al giorno
	 * successivo della data indicata alla mezzanotte.
	 *
	 * <p>
	 * 2014-01-01 17:44:17 -> 2014-01-02 00:00:00
	 * </p>
	 *
	 * @param d la data di riferimento.
	 * @return la data corrispondente al giorno successivo.
	 */
	public static Date nextDay(final @Nullable Date d) {
		if (d == null) {
			return null;
		}
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d);
		gc.add(Calendar.DATE, 1);
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		return gc.getTime();
	}

	/**
	 * Restituisce la data corrispondente al primo giorno del mese
	 * successivo della data indicata alla mezzanotte.
	 *
	 * <p>
	 * 2014-01-17 12:52:44 -> 2014-02-01 00:00:00
	 * <br>
	 * 2012-12-01 00:00:00 -> 2013-01-01 00:00:00
	 * </p>
	 *
	 * @param d la data di riferimento.
	 * @return la data corrispondente al primo giorno
	 *         del mese successivo.
	 */
	@NonNull
	public static Date nextMonth(final @NonNull Date d) {
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d);
		gc.add(Calendar.MONTH, 1);
		gc.set(Calendar.DAY_OF_MONTH, 1);
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		return gc.getTime();
	}

	/**
	 * Verifica se la data specificata è odierna.
	 *
	 * @param d data da verificare.
	 * @throws NullPointerException se la data specificata è {@code null}.
	 */
	public static boolean isToday(final @NonNull Date d) {
		final Calendar c = createCalendar(d);
		final Calendar now = Calendar.getInstance();
		return c.get(Calendar.YEAR) == now.get(Calendar.YEAR)
				&& c.get(Calendar.MONTH) == now.get(Calendar.MONTH)
				&& c.get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Verifica se due oggetti date rappresentano
	 * lo stesso giorno confrontando i campi
	 * YEAR, MONTH e DATE.
	 * Se almeno una delle due date è {@code null} restituisce {@code false}.
	 *
	 * @param d1 una data (obbligatoria).
	 * @param d2 un'altra data (obbligatoria).
	 */
	public static boolean isSameDay(final @Nullable Date d1, final @Nullable Date d2) {
		if (d1 == null || d2 == null) {
			return false;
		}

		final Calendar c1 = Calendar.getInstance();
		c1.setTime(d1);

		final Calendar c2 = Calendar.getInstance();
		c2.setTime(d2);

		return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
				&& c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)
				&& c1.get(Calendar.DATE) == c2.get(Calendar.DATE);
	}

	/**
	 * Verifica se una data ha i campi relativi all'ora (ora, minuti, secondi, millisecondi) valorizzati
	 *
	 * Se data è {@code null} restituisce {@code false}.
	 *
	 * @param date
	 */
	public static boolean hasTime(final @Nullable Date date) {
		if (date == null) {
			return false;
		}
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return !(calendar.get(Calendar.HOUR_OF_DAY) == 0 &&
				calendar.get(Calendar.MINUTE) == 0 &&
				calendar.get(Calendar.SECOND) == 0 &&
				calendar.get(Calendar.MILLISECOND) == 0);
	}

	/**
	 * Crea una data a partire dall'anno, mese e giorno.
	 *
	 * @param year anno da impostare.
	 * @param month mese da impostare (0-11).
	 * @param day giorno del mese da impostare (1-x).
	 */
	@NonNull
	public static Date create(final int year, final int month, final int day) {
		final GregorianCalendar gc = new GregorianCalendar(year, month, day);
		return gc.getTime();
	}

	/**
	 * Crea e restituisce un'instanza di {@code DateFormat} con
	 * il pattern specificato.
	 *
	 * @param pattern pattern da utilizzare.
	 */
	@NonNull
	public static DateFormat createDateFormat(final @NonNull DatePattern pattern) {
		return Objects.requireNonNull(pattern, "Pattern required").build();
	}

	/**
	 * Crea e restituisce un'instanza di {@code DateFormat} con
	 * il pattern {@code dd/MM/yyyy}.
	 */
	@NonNull
	public static DateFormat createDateFormat() {
		return DatePattern.DD_MM_YYYY.build();
	}

	/**
	 * Restituisce la rappresentazione in stringa di una data utilizzando il formato data normale <i>dd/MM/yyyy</i>.
	 * Questo metodo utilizza un'istanza statica di {@code SimpleDateFormat} <u>sincronizzata</u>.
	 *
	 * @param d la data da formattare.
	 * @return la data formattata o {@code ""} se la data è {@code null}.
	 */
	@NonNull
	public static String syncFormat(final Date d) {
		return syncFormatOr(d, "");
	}

	/**
	 * Restituisce la rappresentazione in stringa di una data utilizzando il formato data normale <i>dd/MM/yyyy</i>.
	 * Questo metodo utilizza un'istanza statica di {@code SimpleDateFormat} <u>sincronizzata</u>.
	 *
	 * @param d la data da formattare.
	 * @param nullDefault stringa da restituire in caso di data {@code null}.
	 * @return la data formattata o {@code nullDefault} se la data è {@code null}.
	 */
	@NonNull
	public static String syncFormatOr(final Date d, final @NonNull String nullDefault) {
		if (d == null) {
			return nullDefault;
		}
		synchronized (SHARED_FORMATTER) {
			return SHARED_FORMATTER.format(d);
		}
	}

	/**
	 * Restituisce la rappresentazione in stringa della data corrente
	 * utilizzando il formato data normale {@code dd/MM/yyyy}.
	 * Viene utilizzato un formatter statico sincronizzato.
	 *
	 * @return la data corrente formattata.
	 */
	@NonNull
	public static String formatNow() {
		return syncFormat(now());
	}

	/**
	 * Restituisce la rappresentazione in stringa di una data
	 * utilizzando il formato data normale {@code dd/MM/yyyy}.
	 * Viene utilizzato un formatter statico sincronizzato.
	 *
	 * @param d la data da formattare.
	 * @return la data formattata o {@code ""} se la data è {@code null}.
	 */
	@NonNull
	public static String format(final Date d) {
		return syncFormat(d);
	}

	/**
	 * Restituisce la rappresentazione in stringa di una data
	 * utilizzando il formato data normale {@code dd/MM/yyyy}.
	 * Viene utilizzato un formatter statico sincronizzato.
	 *
	 * @param d la data da formattare.
	 * @param nullDefault stringa da restituire in caso di data {@code null}.
	 * @return la data formattata o {@code nullDefault} se la data è {@code null}.
	 */
	@NonNull
	public static String formatOr(final Date d, final @NonNull String nullDefault) {
		return syncFormatOr(d, nullDefault);
	}

	/**
	 * Restituisce la rappresentazione in stringa della data corrente
	 * utilizzando il formato specificato.
	 *
	 * @param format formato da utilizzare.
	 * @return la data formattata .
	 */
	@NonNull
	public static String format(final @NonNull DatePattern format) {
		return format(now(), format.getPattern());
	}

	/**
	 * Restituisce la rappresentazione in stringa di una data
	 * utilizzando il formato specificato.
	 * <p>
	 * Se si deve formattare una serie di date in successione
	 * è consigliabile utilizzare il metodo {@link #format(Date, DateFormat)}.
	 * In questo modo verrà istanziato l'oggetto {@code DateFormat} una
	 * sola volta:
	 *
	 * <pre>
	 * DateFormat dateFormat = createDateFormat(DatePattern.SQL_DATETIME);
	 * DateUtils.format(date1, dateFormat);
	 * DateUtils.format(date2, dateFormat);
	 * DateUtils.format(date3, dateFormat);
	 * </pre>
	 *
	 * </p>
	 *
	 * @param d data da formattare.
	 * @param format formato da utilizzare.
	 * @return la data formattata o {@code ""} se la data è {@code null}.
	 */
	@NonNull
	public static String format(final Date d, final @NonNull DatePattern format) {
		return format(d, format.getPattern());
	}

	/**
	 * Restituisce la rappresentazione in stringa di una data
	 * utilizzando il formato specificato.
	 * <p>
	 * Se si deve formattare una serie di date in successione
	 * è consigliabile utilizzare il metodo {@link #format(Date, DateFormat)}.
	 * In questo modo verrà istanziato l'oggetto {@code DateFormat} una
	 * sola volta:
	 *
	 * <pre>
	 * DateFormat dateFormat = createDateFormat(DatePattern.SQL_DATETIME);
	 * DateUtils.formatOr(date1, dateFormat, "");
	 * DateUtils.formatOr(date2, dateFormat, "");
	 * DateUtils.formatOr(date3, dateFormat, "");
	 * </pre>
	 *
	 * </p>
	 *
	 * @param d data da formattare.
	 * @param format formato da utilizzare.
	 * @param nullDefault stringa da restituire in caso di data {@code null}.
	 * @return la data formattata o {@code nullDefault} se la data è {@code null}.
	 */
	@NonNull
	public static String formatOr(final Date d, final @NonNull DatePattern format, final @NonNull String nullDefault) {
		return formatOr(d, format.getPattern(), nullDefault);
	}

	/**
	 * Restituisce la rappresentazione in stringa di una data
	 * utilizzando il formato specificato.
	 * <p>
	 * Se si deve formattare una serie di date in successione
	 * è consigliabile utilizzare il metodo {@link #format(Date, DateFormat)}.
	 * In questo modo verrà istanziato l'oggetto {@code DateFormat} una
	 * sola volta:
	 *
	 * <pre>
	 * DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	 * DateUtils.format(date1, dateFormat);
	 * DateUtils.format(date2, dateFormat);
	 * DateUtils.format(date3, dateFormat);
	 * </pre>
	 *
	 * </p>
	 *
	 * @param d data da formattare.
	 * @param format formato da utilizzare.
	 * @return la data formattata o {@code ""} se la data è {@code null}.
	 * @see #formatOr(Date, String, String)
	 */
	@NonNull
	public static String format(final Date d, final @NonNull String format) {
		return formatOr(d, format, "");
	}

	/**
	 * Restituisce la rappresentazione in stringa di una data
	 * utilizzando il formato specificato.
	 * <p>
	 * Se si deve formattare una serie di date in successione
	 * è consigliabile utilizzare il metodo {@link #format(Date, DateFormat)}.
	 * In questo modo verrà istanziato l'oggetto {@code DateFormat} una
	 * sola volta:
	 *
	 * <pre>
	 * DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	 * DateUtils.formatOr(date1, dateFormat, "");
	 * DateUtils.formatOr(date2, dateFormat, "");
	 * DateUtils.formatOr(date3, dateFormat, "");
	 * </pre>
	 *
	 * </p>
	 *
	 * @param d data da formattare.
	 * @param format formato da utilizzare.
	 * @param nullDefault stringa da restituire in caso di data {@code null}.
	 * @return la data formattata o {@code nullDefault} se la data è {@code null}.
	 */
	@NonNull
	public static String formatOr(final Date d, final @NonNull String format, final @NonNull String nullDefault) {
		if (d == null) {
			return nullDefault;
		}
		final DateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.setLenient(false);
		return dateFormat.format(d);
	}

	/**
	 * Restituisce la rappresentazione in stringa di una data utilizzando
	 * l'oggetto {@link DateFormat} specificato.
	 *
	 * <pre>
	 * DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	 * DateUtils.format(date1, dateFormat);
	 * DateUtils.format(date2, dateFormat);
	 * DateUtils.format(date3, dateFormat);
	 * </pre>
	 *
	 * @param d data da formattare.
	 * @param formatter oggetto che si occupa della formattazione.
	 * @return la data formattata o {@code ""} se la data è {@code null}.
	 */
	@NonNull
	public static String format(final Date d, final @NonNull DateFormat formatter) {
		return formatOr(d, formatter, "");
	}

	/**
	 * Restituisce la rappresentazione in stringa di una data utilizzando
	 * l'oggetto {@link DateFormat} specificato.
	 *
	 * <pre>
	 * DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	 * DateUtils.formatOr(date1, dateFormat, "");
	 * DateUtils.formatOr(date2, dateFormat, "");
	 * DateUtils.formatOr(date3, dateFormat, "");
	 * </pre>
	 *
	 * @param d data da formattare.
	 * @param formatter oggetto che si occupa della formattazione.
	 * @param nullDefault stringa da restituire in caso di data {@code null}.
	 * @return la data formattata o {@code nullDefault} se la data è {@code null}.
	 */
	@NonNull
	public static String formatOr(final Date d, final @NonNull DateFormat formatter, final @NonNull String nullDefault) {
		return d == null ? nullDefault : formatter.format(d);
	}

	/**
	 * Ricava una data a partire da una stringa con il formato <i>d/M/y</i>.
	 * Questo metodo utilizza un'istanza statica di {@code SimpleDateFormat} <u>sincronizzata</u>.
	 *
	 * @param s la stringa da esaminare.
	 * @return la data convertita o {@code null} se la stringa è {@code ""}.
	 * @throws ParseException se la stringa non rispetta il formato.
	 */
	public static Date syncParse(final String s) throws ParseException {
		if (StringUtils.isNullOrEmpty(s)) {
			return null;
		}
		synchronized (SHARED_PARSER) {
			return SHARED_PARSER.parse(s);
		}
	}

	/**
	 * Ricava una data a partire da una stringa con il formato <i>d/M/y</i>.
	 *
	 * @param s la stringa da esaminare.
	 * @return la data convertita o {@code null} se la stringa è {@code ""}.
	 * @throws ParseException se la stringa non rispetta il formato.
	 */
	public static Date parse(final String s) throws ParseException {
		return parse(s, DatePattern.D_M_YY);
	}

	/**
	 * Ricava una data a partire da una stringa con il formato specificato.
	 *
	 * @param s la stringa da esaminare.
	 * @param format formato che deve avere la stringa.
	 * @return la data convertita o {@code null} se la stringa è {@code ""}.
	 * @throws ParseException se la stringa non rispetta il formato.
	 */
	public static Date parse(final String s, final @NonNull DatePattern format) throws ParseException {
		if (StringUtils.isNullOrEmpty(s)) {
			return null;
		}
		final DateFormat dateFormat = new SimpleDateFormat(format.getPattern());
		dateFormat.setLenient(false);
		return dateFormat.parse(s);
	}

	/**
	 * Ricava una data a partire da una stringa utilizzando
	 * l'oggetto {@link DateFormat} specificato.
	 *
	 * @param s la stringa da esaminare.
	 * @param parser oggetto che si occupa di ricavare la data.
	 * @return la data convertita o {@code null} se la stringa è {@code ""}.
	 * @throws ParseException se la stringa non rispetta il formato.
	 */
	public static Date parse(final String s, final @NonNull DateFormat parser) throws ParseException {
		return StringUtils.isNullOrEmpty(s) ? null : parser.parse(s);
	}

	/**
	 * Ricava una data a partire da una stringa utilizzando
	 * il formato specificato.
	 *
	 * @param s la stringa da esaminare.
	 * @param pattern formato della data che ci si aspetta.
	 * @return la data convertita o {@code null} se la stringa è {@code null} o {@code ""}.
	 * @throws ParseException se la stringa non rispetta il pattern.
	 * @throws NullPointerException se il pattern è {@code null}.
	 * @throws IllegalArgumentException se il pattern non è valido.
	 */
	public static Date parse(final String s, final @NonNull String pattern) throws ParseException {
		Objects.requireNonNull(pattern, "Pattern required");
		if (StringUtils.isNullOrEmpty(s)) {
			return null;
		}
		final DateFormat dateFormat = new SimpleDateFormat(pattern);
		dateFormat.setLenient(false);
		return dateFormat.parse(s);
	}

	/**
	 * Ricava un {@code LocalDate} a partire da una stringa con il formato ISO yyyy-MM-dd.
	 * <br/>
	 * Se la stringa è vuota o non convertibile restituisce {@code null}.
	 *
	 * @param s la stringa da esaminare.
	 * @return la data convertita o {@code null} se la stringa è vuota o
	 *         non convertibile.
	 */
	public static LocalDate parseIsoLocalDate(final String s) {
		return parseLocalDate(s, DateTimeFormatter.ISO_DATE);
	}

	/**
	 * Ricava un {@code LocalDate} a partire da una stringa con il formato specificato.
	 * <br/>
	 * Se la stringa è vuota o non convertibile restituisce {@code null}.
	 *
	 * @param s la stringa da esaminare.
	 * @param formatter formato che deve avere la stringa.
	 * @return la data convertita o {@code null} se la stringa è vuota o
	 *         non convertibile.
	 */
	public static LocalDate parseLocalDate(final String s, final @NonNull DateTimeFormatter formatter) {
		if (StringUtils.isNullOrBlank(s)) {
			return null;
		}
		try {
			return LocalDate.parse(s, formatter);
		} catch (final @SuppressWarnings("unused") DateTimeParseException ex) {
			return null;
		}
	}

	/**
	 * Ricava un {@code LocalTime} a partire da una stringa con il formato specificato.
	 * <br/>
	 * Se la stringa è vuota o non convertibile restituisce {@code null}.
	 *
	 * @param s la stringa da esaminare.
	 * @param formatter formato che deve avere la stringa.
	 * @return l'ora convertita o {@code null} se la stringa è vuota o
	 *         non convertibile.
	 */
	public static LocalTime parseLocalTime(final String s, final @NonNull DateTimeFormatter formatter) {
		if (StringUtils.isNullOrBlank(s)) {
			return null;
		}
		try {
			return LocalTime.parse(s, formatter);
		} catch (final @SuppressWarnings("unused") DateTimeParseException ex) {
			return null;
		}
	}

	/**
	 * Ricava un {@code LocalDateTime} a partire da una stringa con il formato specificato.
	 * <br/>
	 * Se la stringa è vuota o non convertibile restituisce {@code null}.
	 *
	 * @param s la stringa da esaminare.
	 * @param formatter formato che deve avere la stringa.
	 * @return la data convertita o {@code null} se la stringa è vuota o
	 *         non convertibile.
	 */
	public static LocalDateTime parseLocalDateTime(final String s, final @NonNull DateTimeFormatter formatter) {
		if (StringUtils.isNullOrBlank(s)) {
			return null;
		}
		try {
			return LocalDateTime.parse(s, formatter);
		} catch (final @SuppressWarnings("unused") DateTimeParseException ex) {
			return null;
		}
	}

	/**
	 * Ricava una data a partire da una stringa con il formato specificato.
	 * <br/>
	 * Se la stringa è vuota o non convertibile restituisce {@code null}.
	 *
	 * @param s la stringa da esaminare.
	 * @param format formato che deve avere la stringa.
	 * @return la data convertita o {@code null} se la stringa è vuota o
	 *         non convertibile.
	 */
	public static Date safeParse(final String s, final @NonNull DatePattern format) {
		if (StringUtils.isNullOrBlank(s)) {
			return null;
		}
		final DateFormat dateFormat = new SimpleDateFormat(format.getPattern());
		dateFormat.setLenient(false);
		try {
			return dateFormat.parse(s);
		} catch (final @SuppressWarnings("unused") ParseException ex) {
			return null;
		}
	}

	/**
	 * Converte un anno di due cifre in uno di 4
	 * Se il parametro è maggiore di 99 o minore di zero viene restituito senza modifiche
	 * Se è compreso tra zero e 99 viene calcolato l'anno a quattro cifre in fuzione dell'anno corrente + 10
	 *
	 * @see #DateUtils.year2Digit(int,int)
	 *
	 * @param year
	 * @return
	 */
	public static int year2Digits(final int year) {
		return year2Digits(year, Year.now().getValue() + 10);
	}

	/**
	 * Converte un anno di due cifre in uno di 4
	 * Se il parametro è maggiore di 99 o minore di zero viene restituito senza modifiche
	 * Se è compreso tra zero e 99 viene calcolato l'anno a quattro cifre in fuzione dell'anno soglia
	 *
	 * <pre>
	 * year2Digits(2015, 2027) = 2015
	 * year2Digits(3, 2027) = 2003
	 * year2Digits(15, 2027) = 2015
	 * year2Digits(18, 2027) = 2018
	 * year2Digits(27, 2027) = 2027
	 * year2Digits(28, 2027) = 1928
	 * year2Digits(99, 2027) = 1999
	 *
	 * year2Digits(2015, 2005) = 2015
	 * year2Digits(3, 2005) = 2003
	 * year2Digits(15, 2005) = 1915
	 * year2Digits(18, 2005) = 1918
	 * year2Digits(27, 2005) = 1927
	 * year2Digits(28, 2005) = 1928
	 * year2Digits(99, 2005) = 1999
	 * </pre>
	 *
	 * @param year
	 * @param thresholdYear
	 * @return
	 */
	public static int year2Digits(final int year, final int thresholdYear) {
		if (year >= 0 && year < 100) {
			// devo sistemare le date quando scrivo solo le ultime 2 cifre
			final int c = thresholdYear % 100;
			if (year <= c) {
				// 16 + 2017 - 17 = 2016
				return year + thresholdYear - c;
			}
			return year + thresholdYear - c - 100;
		}
		return year;
	}

	/**
	 * Calcola il numero di anni passati tra due date eseguendo la differenza
	 * degli anni delle rispettive date.
	 * <p>Se: d1 = 16/08/1992, d2 = 22/01/2011</p>
	 *
	 * <pre>
	 * ageByYear(d1, d2) = 19
	 * </pre>
	 *
	 * <p>Per le {@code LocalDate}:
	 *
	 * <pre>
	 * when.getYear() - d.getYear();
	 * </pre>
	 *
	 * </p>
	 *
	 * Se si vuole tener conto anche dei mesi e giorni utilizzare il metodo {@link #age(Date, Date)}.
	 *
	 * @param d data iniziale.
	 * @param when data finale.
	 * @return l'anno di {@code when} meno l'anno di {@code d}.
	 * @throws NullPointerException se almeno una delle due date è {@code null}.
	 */
	public static int ageByYear(final @NonNull Date d, final @NonNull Date when) {
		final Calendar cNow = Calendar.getInstance();
		cNow.setTime(when);
		final Calendar cThen = Calendar.getInstance();
		cThen.setTime(d);
		return cNow.get(Calendar.YEAR) - cThen.get(Calendar.YEAR);
	}

	/**
	 * Calcola la differenza tra l'anno corrente e l'anno della data specificata.
	 * <p>Se si vuole tener conto anche dei mesi e giorni utilizzare il metodo {@link #age(Date)}.</p>
	 *
	 * <p>Per le {@code LocalDate}:
	 *
	 * <pre>
	 * LocalDate.now().getYear() - d.getYear();
	 * </pre>
	 *
	 * </p>
	 *
	 * @param d data iniziale.
	 * @return l'anno corrente meno l'anno di {@code d}.
	 * @throws IllegalArgumentException se la data è {@code null}.
	 */
	public static int ageByYear(final @NonNull Date d) {
		return ageByYear(d, now());
	}

	/**
	 * Calcola il numero di anni passati tra due date.
	 * <p>Se: d1 = 16/08/1992, d2 = 22/01/2011</p>
	 *
	 * <pre>
	 * ageByYear(d1, d2) -> 18
	 * </pre>
	 *
	 * <p>Per le {@code LocalDate}:
	 *
	 * <pre>
	 * d.until(when, ChronoUnit.YEARS);
	 * </pre>
	 *
	 * </p>
	 *
	 * @param d data iniziale.
	 * @param when data finale.
	 * @throws NullPointerException se almeno una delle due date è {@code null}.
	 */
	public static int age(final @NonNull Date d, final @NonNull Date when) {
		final GregorianCalendar birthCalendar = new GregorianCalendar();
		birthCalendar.setTime(d);

		final GregorianCalendar whenCalendar = new GregorianCalendar();
		whenCalendar.setTime(when);

		final int age = whenCalendar.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR);
		birthCalendar.add(Calendar.YEAR, age);
		return birthCalendar.after(whenCalendar) ? age - 1 : age;
	}

	/**
	 * Calcola il numero di anni passati dalla data specificata.
	 * <p>Per le {@code LocalDate}:
	 *
	 * <pre>
	 * d.until(LocalDate.now(), ChronoUnit.YEARS);
	 * </pre>
	 *
	 * </p>
	 *
	 * @param d data iniziale.
	 * @throws NullPointerException se la data è {@code null}.
	 */
	public static int age(final @NonNull Date d) {
		return age(d, now());
	}

	/**
	 * Verifica se oggi è l'anniversario di una data.
	 *
	 * @param d data dell'evento.
	 */
	public static boolean isBirthday(final @NonNull Date d) {
		final GregorianCalendar now = new GregorianCalendar();
		final GregorianCalendar date = new GregorianCalendar();
		date.setTime(d);

		final int thisLeapYear = now.isLeapYear(now.get(Calendar.YEAR)) && now.get(Calendar.MONTH) > Calendar.FEBRUARY ? 1 : 0;
		final int dateLeapYear = date.isLeapYear(date.get(Calendar.YEAR)) && date.get(Calendar.MONTH) > Calendar.FEBRUARY ? 1 : 0;

		return now.get(Calendar.DAY_OF_YEAR) - thisLeapYear == date.get(Calendar.DAY_OF_YEAR) - dateLeapYear;
	}

	/**
	 * Restituisce la data senza l'ora.
	 */
	public static Date getDate(final Date d) {
		if (d == null) {
			return null;
		}
		return getDateInternal(d);
	}

	/**
	 * Restituisce la data senza l'ora.
	 * Questo metodo interno non ammette date in
	 * ingresso {@code null}.
	 *
	 * @param d la data.
	 */
	@NonNull
	private static Date getDateInternal(final @NonNull Date d) {
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d);
		return new GregorianCalendar(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DAY_OF_MONTH)).getTime();
	}

	/**
	 * Restituisce la data azzerando anno, mese e giorno.
	 *
	 * @return un nuovo oggetto {@code Date} con data 01/01/1970 e stesse ore, minuti,
	 *         secondi e millisecondi della data passata.
	 */
	public static Date getTime(final Date d) {
		if (d == null) {
			return null;
		}
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d);
		gc.clear(Calendar.YEAR);
		gc.clear(Calendar.MONTH);
		gc.clear(Calendar.DAY_OF_MONTH);
		return gc.getTime();
	}

	/**
	 * Crea un nuovo oggetto {@code Date} effettuando la "copia" di alcuni campi
	 * da una data sorgente ad una di destinazione.
	 *
	 * <br>Se:<ul>
	 * <li>d1 = 04/06/2013 17:00:00</li>
	 * <li>d2 = 04/06/2012 18:22:15</li>
	 * </ul>
	 *
	 * <pre>
	 * copy(d1, d2, Calendar.YEAR, Calendar.HOUR) = 04/06/2013 17:22:15
	 * </pre>
	 *
	 * @param calendarFields array di costanti della classe {@linkplain Calendar}.
	 */
	@NonNull
	public static Date copy(final @NonNull Date source, final @NonNull Date destination, final @NonNull int... calendarFields) {
		Objects.requireNonNull(source, "Source date required");
		Objects.requireNonNull(destination, "Destination date required");
		Objects.requireNonNull(calendarFields, "Calendar fields required");
		if (calendarFields.length == 0) {
			return destination;
		}
		final Calendar sourceCal = createCalendar(source);
		final Calendar destinationCal = createCalendar(destination);
		for (final int calendarField : calendarFields) {
			destinationCal.set(calendarField, sourceCal.get(calendarField));
		}
		return destinationCal.getTime();
	}

	/**
	 * Verifica se una data viene dopo o allo stesso
	 * tempo di un'altra.
	 * Le date non possono essere {@code null}.
	 *
	 * @param grDate data da verificare.
	 * @param date data con cui confrontare.
	 */
	public static boolean greatherThanEqualTo(final @NonNull Date grDate, final @NonNull Date date) {
		return grDate.getTime() >= date.getTime();
	}

	/**
	 * Verifica se una data viene prima o allo stesso
	 * tempo di un'altra.
	 * Le date non possono essere {@code null}.
	 *
	 * @param lsDate data da verificare.
	 * @param date data con cui confrontare.
	 */
	public static boolean lessThanEqualTo(final @NonNull Date lsDate, final @NonNull Date date) {
		return lsDate.getTime() <= date.getTime();
	}

	/**
	 * <p>Calcola la differenza in giorni trascorsi tra due date.</p>
	 * Non si tiene conto dell'ora:
	 * ad esempio, tra le ore 15.00 del 1/1/2000 e le ore 8.00
	 * del del 2/1/2000 risulta che è trascorso un giorno intero.
	 * <br>
	 * Il metodo tiene conto del cambio dell'ora (Daylight Saving Time) che in Italia
	 * (Timezone Europe/Rome con orario che passa da <em>CET &ndash;
	 * Central European Time</em> a <em>CEST &ndash; Central
	 * European Summer Time</em>) avviene negli ultimi weekend di Marzo e
	 * Ottobre.
	 * <br>
	 * Per esempio, dalle 00:00 del 2005-03-27 CET alle 00:00 del
	 * 2005-03-28 CEST sono trascorse solo 23 ore, ma la funzione indica
	 * ugualmente che è trascorso un giorno.
	 * <br>
	 * Analogamente dalle 00:00 del
	 * 2005-10-30 CEST alle ore 23:30 del 2005-10-30 CET saranno trascorse 24
	 * ore e mezza, ma la funzione ritornerà ugualmente 0.
	 *
	 * @param start la data iniziale.
	 * @param end la data finale.
	 * @throws NullPointerException se qualche parametro è {@code null}.
	 */
	public static int daysBetween(final @NonNull Date start, final @NonNull Date end) {
		final long millisecBetween = getDateInternal(end).getTime() - getDateInternal(start).getTime();
		return (int) Math.round((double) millisecBetween / (double) DAY_MS);
	}

	/**
	 * <p>Aggiunge o sottrae dalla data in parametro il numero di unità del tipo specificato.</p>
	 *
	 * @param d la data iniziale.
	 * @param field una delle costanti di {@code Calendar}.
	 * @param amount il numero di unità da sommare o sottrarre.
	 * @return la data finale.
	 * @throws IllegalArgumentException se il tipo non è valido..
	 */
	@NonNull
	public static Date add(final @NonNull Date d, final int field, final int amount) {
		if (amount == 0) {
			return d;
		}
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d);
		gc.add(field, amount);
		return gc.getTime();
	}

	/**
	 * <p>Aggiunge o sottrae dalla data in parametro il numero di unità del tipo specificato.</p>
	 * <p>Se la data è {@code null} restituisce {@code null}.</p>
	 *
	 * @param d la data iniziale.
	 * @param field una delle costanti di {@code Calendar}.
	 * @param amount il numero di unità da sommare o sottrarre.
	 * @return la data finale.
	 * @throws IllegalArgumentException se il tipo non è valido..
	 */
	public static Date addOrNull(final Date d, final int field, final int amount) {
		if (d == null) {
			return null;
		}
		return add(d, field, amount);
	}

	/**
	 * <p>Aggiunge o sottrae dalla data in parametro il numero di giorni specificati.</p>
	 *
	 * @param d la data iniziale.
	 * @param amount il numero di giorni da sommare o sottrarre.
	 * @return la data finale.
	 */
	@NonNull
	public static Date addDays(final @NonNull Date d, final int amount) {
		return add(d, Calendar.DATE, amount);
	}

	/**
	 * <p>Aggiunge o sottrae dalla data in parametro il numero di giorni specificati.</p>
	 * <p>Se la data è {@code null} restituisce {@code null}.</p>
	 *
	 * @param d la data iniziale.
	 * @param amount il numero di giorni da sommare o sottrarre.
	 * @return la data finale.
	 */
	public static Date addDaysOrNull(final Date d, final int amount) {
		if (d == null) {
			return null;
		}
		return add(d, Calendar.DATE, amount);
	}

	/**
	 * Confronta due date, eventualmente nulle.
	 *
	 * @param d1 prima data (opzionale).
	 * @param d2 seconda data (opzionale).
	 * @param ascending se {@code true}, la data minore è quella che temporalmente viene prima, altrimenti viceversa.
	 * @param nullFirst indica se le date {@code null} vengono prima di quelle non {@code null}, indipendentemente da {@code asceding}.
	 */
	public static int nullSafeCompare(final Date d1, final Date d2, final boolean ascending, final boolean nullFirst) {
		if (d1 == null) {
			if (d2 != null) {
				return nullFirst ? -1 : 1;
			}
			return 0;
		}
		if (d2 == null) {
			return nullFirst ? 1 : -1;
		}
		return ascending ? d1.compareTo(d2) : d2.compareTo(d1);
	}

	/**
	 * Verifica se una data si trova in un intervallo di date specificato.
	 *
	 * @param d data da verificare.
	 * @param d1 data inziale.
	 * @param d2 data finale.
	 * @return {@code true} se la data è compresa nell'intervallo.
	 */
	public static boolean isBetween(final Date d, final Date d1, final Date d2) {
		return d != null && (d1 == null || !d1.after(d)) && (d2 == null || !d.after(d2));
	}

	/**
	 * Crea e restituisce un instanza di {@link GregorianCalendar} a partire da una data.
	 */
	@NonNull
	public static GregorianCalendar createCalendar(final @NonNull Date date) {
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		return gc;
	}

	/** Restituisce un campo specifico di una data. */
	public static int getField(final @NonNull Date date, final int calendarConst) {
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		return gc.get(calendarConst);
	}

	/** Restituisce il valore minimo di un campo specifico di una data. */
	public static int getActualMinimum(final @NonNull Date date, final int calendarConst) {
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		return gc.getActualMinimum(calendarConst);
	}

	/** Restituisce il valore massimo di un campo specifico di una data. */
	public static int getActualMaximum(final @NonNull Date date, final int calendarConst) {
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		return gc.getActualMaximum(calendarConst);
	}

	/**
	 * Se la data specificata è del giorno indicato, viene restituita
	 * una nuova istanza della stessa data senza ore, minuti e secondi.
	 * Altrimenti viene restituta la prima data precedente con giorno indicato.
	 *
	 * <p>Esempio:</p>
	 * se la data è mercoledì 18 agosto e il giorno della settimana è 2 (lunedì)
	 * verrà restituito lunedì 16 agosto.
	 *
	 * @param date data di riferimento.
	 * @param dayOfWeek giorno della settimana.
	 *
	 * @since 27/06/2014
	 */
	public static Date getFirstWeekDayBeforeDate(final Date date, final int dayOfWeek) {
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		final int day = gc.get(Calendar.DAY_OF_WEEK);
		gc.add(Calendar.DAY_OF_WEEK, -((7 + day - dayOfWeek) % 7));
		return getDate(gc.getTime());
	}

	/**
	 * Converte un oggetto di tipo {@link XMLGregorianCalendar} in un oggetto {@link Date}.
	 *
	 * @param includeTime indica se includere anche ore, minuti e secondi.
	 * @param xmlGC l'oggetto da convertire.
	 * @return la data corrispondente oppure {@code null} se l'oggetto passato è {@code null}.
	 */
	public static Date fromXMLGregorianCalendar(final boolean includeTime, final XMLGregorianCalendar xmlGC) {
		if (xmlGC == null) {
			return null;
		}
		return (includeTime ? new GregorianCalendar(xmlGC.getYear(), xmlGC.getMonth() - 1, xmlGC.getDay(), xmlGC.getHour(), xmlGC.getMinute(), xmlGC.getSecond()) : new GregorianCalendar(xmlGC.getYear(), xmlGC.getMonth() - 1, xmlGC.getDay())).getTime();
	}

	/**
	 * Crea e restituisce un oggetto {@link XMLGregorianCalendar} a partire da una data.
	 *
	 * @param date data da convertire.
	 * @param includeTime indica se includere ore, minuti e secondi.
	 * @return l'oggetto {@link XMLGregorianCalendar} corrispondente alla data oppure {@code null} se la data passata è {@code null}.
	 */
	public static XMLGregorianCalendar createXMLGregorianCalendar(final Date date, final boolean includeTime) {
		if (date == null) {
			return null;
		}
		final GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeInMillis(date.getTime());
		if (includeTime) {
			return getDatatypeFactory().newXMLGregorianCalendar(
					gregorianCalendar.get(Calendar.YEAR),
					gregorianCalendar.get(Calendar.MONTH) + 1,
					gregorianCalendar.get(Calendar.DATE),
					gregorianCalendar.get(Calendar.HOUR_OF_DAY),
					gregorianCalendar.get(Calendar.MINUTE),
					gregorianCalendar.get(Calendar.SECOND),
					DatatypeConstants.FIELD_UNDEFINED,
					DatatypeConstants.FIELD_UNDEFINED);
		}
		return getDatatypeFactory().newXMLGregorianCalendarDate(
				gregorianCalendar.get(Calendar.YEAR),
				gregorianCalendar.get(Calendar.MONTH) + 1,
				gregorianCalendar.get(Calendar.DATE),
				DatatypeConstants.FIELD_UNDEFINED);
	}

	public static XMLGregorianCalendar createXMLGregorianCalendar(final @Nullable LocalDate date, final @Nullable ZoneOffset offset) {
		if (date == null) {
			return null;
		}
		return getDatatypeFactory().newXMLGregorianCalendarDate(
				date.getYear(),
				date.getMonthValue(),
				date.getDayOfMonth(),
				offset == null ? DatatypeConstants.FIELD_UNDEFINED : offset.getTotalSeconds() / SECONDS_IN_MINUTE);

	}

	public static XMLGregorianCalendar createXMLGregorianCalendar(final @Nullable LocalDate date) {
		return createXMLGregorianCalendar(date, null);
	}

	public static XMLGregorianCalendar createXMLGregorianCalendar(final @Nullable LocalDateTime date, final @Nullable ZoneOffset offset) {
		if (date == null) {
			return null;
		}
		return getDatatypeFactory().newXMLGregorianCalendar(
				date.getYear(),
				date.getMonthValue(),
				date.getDayOfMonth(),
				date.getHour(),
				date.getMinute(),
				date.getSecond(),
				DatatypeConstants.FIELD_UNDEFINED,
				offset == null ? DatatypeConstants.FIELD_UNDEFINED : offset.getTotalSeconds() / SECONDS_IN_MINUTE);
	}

	public static XMLGregorianCalendar createXMLGregorianCalendar(final @Nullable LocalDateTime date) {
		return createXMLGregorianCalendar(date, null);
	}

	// utilità java.time

	public static String format(final TemporalAccessor ta) {
		if (ta == null) {
			return "";
		}
		return SHARED_FORMATTER_LOCALDATETIME.format(ta);
	}

	public static String format(final TemporalAccessor ta, final @NonNull DatePattern pattern) {
		if (ta == null) {
			return "";
		}
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern.pattern);
		return formatter.format(ta);
	}

	public static String format(final TemporalAccessor ta, final @NonNull String pattern) {
		if (ta == null) {
			return "";
		}
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		return formatter.format(ta);
	}

	/**
	 * Restituisce un oggetto {@code java.util.Date} a
	 * partire da un oggetto {@code java.time.LocalTime}.
	 * <br/>
	 * L'oggetto restituito avrà come data 01/01/1970 e come
	 * ora la stessa rappresentata dall'input.
	 */
	public static Date toDate(final LocalTime lt) {
		if (lt == null) {
			return null;
		}
		// TODO non so se c'è un modo migliore
		final Instant instant = lt.atDate(LocalDate.ofEpochDay(0)).atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(instant);
	}

	/**
	 * Restituisce un oggetto {@code java.util.Date} a
	 * partire da un oggetto {@code java.time.LocalDate}.
	 */
	public static Date toDate(final LocalDate ld) {
		if (ld == null) {
			return null;
		}
		final Instant instant = ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(instant);
	}

	/**
	 * Restituisce un oggetto {@code java.util.Date} a
	 * partire da un oggetto {@code java.time.LocalDateTime}.
	 */
	public static Date toDate(final LocalDateTime ldt) {
		if (ldt == null) {
			return null;
		}
		final Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(instant);
	}

	/**
	 * Restituisce un oggetto {@code java.util.Date} a
	 * partire da un oggetto {@code java.time.YearMonth}.
	 * La data sarà impostata con il primo giorno del mese.
	 */
	public static Date toDateStart(final YearMonth ym) {
		if (ym == null) {
			return null;
		}
		return Date.from(ym.atDay(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * Restituisce un oggetto {@code java.util.Date} a
	 * partire da un oggetto {@code java.time.YearMonth}.
	 * La data sarà impostata con l'ultimo giorno del mese.
	 */
	public static Date toDateEnd(final YearMonth ym) {
		if (ym == null) {
			return null;
		}
		return Date.from(ym.atEndOfMonth().atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * Restituisce un oggetto {@code java.time.LocalTime} a
	 * partire da un oggetto {@code java.util.Date}.
	 */
	public static LocalTime toLocalTime(final Date date) {
		return toLocalTime(date, ZoneId.systemDefault());
	}

	public static LocalTime toLocalTime(final Date date, final ZoneId zoneId) {
		if (date == null) {
			return null;
		}
		return toLocalDateTime(date, zoneId).toLocalTime();
	}

	/**
	 * Restituisce un oggetto {@code java.time.LocalDate} a
	 * partire da un oggetto {@code java.util.Date}.
	 */
	public static LocalDate toLocalDate(final Date date) {
		return toLocalDate(date, ZoneId.systemDefault());
	}

	/**
	 * Restituisce un oggetto {@code java.time.LocalDate} a
	 * partire da un oggetto {@code java.util.Date}.
	 */
	public static LocalDate toLocalDate(final Date date, final ZoneId zoneId) {
		if (date == null) {
			return null;
		}
		final Instant instant = Instant.ofEpochMilli(date.getTime());
		return LocalDateTime.ofInstant(instant, zoneId).toLocalDate();
	}

	/**
	 * Restituisce un oggetto {@code java.time.LocalDate} a
	 * partire dai millisecondi.
	 */
	public static LocalDate toLocalDate(final long millis, final @NonNull ZoneId zoneId) {
		final Instant instant = Instant.ofEpochMilli(millis);
		return instant.atZone(zoneId).toLocalDate();
	}

	/**
	 * Restituisce un oggetto {@code java.time.LocalDateTime} a
	 * partire da un oggetto {@code java.util.Date}.
	 */
	public static LocalDateTime toLocalDateTime(final Date date) {
		return toLocalDateTime(date, ZoneId.systemDefault());
	}

	/**
	 * Restituisce un oggetto {@code java.time.LocalDateTime} a
	 * partire da un oggetto {@code java.util.Date}.
	 */
	public static LocalDateTime toLocalDateTime(final Date date, final @NonNull ZoneId zoneId) {
		if (date == null) {
			return null;
		}
		return toLocalDateTime(date.getTime(), zoneId);
	}

	/**
	 * Restituisce un oggetto {@code java.time.LocalDateTime} a
	 * partire dai millisecondi.
	 */
	public static LocalDateTime toLocalDateTime(final long millis, final @NonNull ZoneId zoneId) {
		final Instant instant = Instant.ofEpochMilli(millis);
		return LocalDateTime.ofInstant(instant, zoneId);
	}

	/**
	 * Restituisce un oggetto {@code java.time.LocalDateTime} a
	 * partire da un oggetto {@code javax.xml.datatype.XMLGregorianCalendar}.
	 */
	public static LocalDateTime toLocalDateTime(final XMLGregorianCalendar xmlgc) {
		return toLocalDateTime(xmlgc, ZoneId.systemDefault());
	}

	/**
	 * Restituisce un oggetto {@code java.time.LocalDateTime} a
	 * partire da un oggetto {@code javax.xml.datatype.XMLGregorianCalendar}.
	 */
	public static LocalDateTime toLocalDateTime(final XMLGregorianCalendar xmlgc, final @NonNull ZoneId zoneId) {
		if (xmlgc == null) {
			return null;
		}
		return xmlgc.toGregorianCalendar().toZonedDateTime().withZoneSameInstant(zoneId).toLocalDateTime();
	}

	/**
	 * Restituisce un oggetto {@code java.time.YearMonth} a
	 * partire da un oggetto {@code java.util.Date}.
	 */
	public static YearMonth toYearMonth(final Date date) {
		if (date == null) {
			return null;
		}
		if (date instanceof java.sql.Date) {
			return YearMonth.from(((java.sql.Date) date).toLocalDate());
		}
		return YearMonth.from(date.toInstant().atZone(ZoneId.systemDefault()));
	}

	/**
	 * Restituisce un oggetto {@link LocalDate} che rappresenta l'ultimo giorno del mese della data passata come parametro
	 */
	public static LocalDate lastDayOfMonth(final LocalDate date) {
		return date == null ? null : date.withDayOfMonth(date.lengthOfMonth());
	}

	/**
	 * Restituisce un oggetto {@link LocalDateTime} a partire dai millisecondi indicati
	 * utilizzando il fuso orario di sistema.
	 *
	 * @param ms i millisecondi dal 01/01/1970.
	 */
	public static LocalDateTime fromMillis(final long ms) {
		return LocalDateTime.ofInstant(Instant.ofEpochMilli(ms), TimeZone.getDefault().toZoneId());
	}

	/**
	 * Restituisce il numero di millisecondi dal 01/01/1970
	 * della data indicata utilizzando il fuso orario di sistema.
	 *
	 * @param ldt data/ora da analizzare.
	 */
	public static long getMillis(final @NonNull LocalDateTime ldt) {
		return ldt.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
	}

	// utilità java.sql

	/**
	 * Restituisce un oggetto {@code java.sql.Date} a partire
	 * da un oggetto {@code java.util.Date}.
	 *
	 * @param date data sorgente.
	 */
	public static java.sql.Date toSqlDate(final @Nullable Date date) {
		return date != null ? new java.sql.Date(date.getTime()) : null;
	}

	/**
	 * Restituisce un oggetto {@code java.sql.Date} a partire
	 * da un oggetto {@code java.time.LocalDateTime}.
	 *
	 * @param ldt data sorgente.
	 */
	public static java.sql.Date toSqlDate(final @Nullable LocalDateTime ldt) {
		return ldt != null ? new java.sql.Date(getMillis(ldt)) : null;
	}

	/**
	 * Restituisce un oggetto {@code java.sql.Date} a partire
	 * da un oggetto {@code java.time.LocalDate}.
	 *
	 * @param ld data sorgente.
	 */
	public static java.sql.Date toSqlDate(final @Nullable LocalDate ld) {
		return ld != null ? java.sql.Date.valueOf(ld) : null;
	}

	/**
	 * Restituisce un oggetto {@code java.sql.Timestamp} a partire
	 * da un oggetto {@code java.util.Date}.
	 *
	 * @param date data sorgente.
	 */
	public static java.sql.Timestamp toSqlTimestamp(final @Nullable Date date) {
		return date != null ? new java.sql.Timestamp(date.getTime()) : null;
	}

	/**
	 * Restituisce un oggetto {@code java.sql.Timestamp} a partire
	 * da un oggetto {@code java.time.LocalDateTime}.
	 *
	 * @param ldt data sorgente.
	 */
	public static java.sql.Timestamp toSqlTimestamp(final @Nullable LocalDateTime ldt) {
		return ldt != null ? java.sql.Timestamp.valueOf(ldt) : null;
	}

	// altro

	/**
	 * Restituisce una sequenza ordinata di date sottoforma di {@code Stream}.
	 * La sequenza parte dalla data iniziale (inclusa) a quella finale (esclusa)
	 * incrementando di un giorno ogni step.
	 *
	 * @param start data iniziale (inclusa).
	 * @param end data finale (esclusa).
	 */
	public static Stream<Date> range(final Date start, final Date end) {
		final Date start1 = getDateInternal(start);
		final Date end1 = getDateInternal(end);
		if (start1.after(end1)) {
			throw new IllegalArgumentException("Start date is after end date");
		}

		final Calendar cal = Calendar.getInstance();
		cal.setTime(start1);

		final long millisecBetween = end1.getTime() - start1.getTime();

		return Stream.iterate(start1, d -> {
			cal.add(Calendar.DATE, 1);
			return cal.getTime();
		}).limit(Math.round((double) millisecBetween / (double) DateUtils.DAY_MS));
	}

	/**
	 * Restituisce una sequenza ordinata di date sottoforma di {@code Stream}.
	 * La sequenza parte dalla data iniziale (inclusa) a quella finale (esclusa)
	 * incrementando di un giorno ogni step.
	 *
	 * @param start data iniziale (inclusa).
	 * @param end data finale (esclusa).
	 */
	public static Stream<LocalDate> range(final LocalDate start, final LocalDate end) {
		return LongStream.range(start.toEpochDay(), end.toEpochDay()).mapToObj(LocalDate::ofEpochDay);
	}

	private static synchronized DatatypeFactory getDatatypeFactory() {
		if (dtf == null) {
			try {
				dtf = DatatypeFactory.newInstance();
			} catch (final DatatypeConfigurationException e) {
				throw new RuntimeException(e);
			}
		}
		return dtf;
	}
}
