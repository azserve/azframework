package com.azserve.azframework.common.awt;

import java.awt.Color;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Classe di utilità per la gestione di colori.
 *
 * @author filippo
 * @since 08/08/2013
 */
public final class ColorUtils {

	private static final float HSB_BRIGHTNESS_FACTOR = 0.9F;
	private static final float HSB_SATURATION_FACTOR = 0.9F;

	private ColorUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Restituisce le componenti R,G,B a partire
	 * dal colore in rgb.
	 *
	 * @param rgbColor colore in rgb.
	 */
	@NonNull
	public static int[] rgb(final int rgbColor) {
		return new int[] { rgbColor & 0xFF, rgbColor >> 8 & 0xFF, rgbColor >> 16 & 0xFF };
	}

	/**
	 * Indica se un colore può essere considerato scuro.
	 *
	 * @param color il colore.
	 */
	public static boolean isDark(final @NonNull Color color) {
		return isDark(color.getRed(), color.getGreen(), color.getBlue());
	}

	/**
	 * Indica se un colore può essere considerato scuro.
	 *
	 * @param rgbColor colore in rgb.
	 */
	public static boolean isDark(final int rgbColor) {
		final int[] rgb = rgb(rgbColor);
		return isDark(rgb[0], rgb[1], rgb[2]);
	}

	/**
	 * Indica se un colore può essere considerato scuro.
	 *
	 * @param red componente rossa.
	 * @param green componente verde.
	 * @param blue componente blue.
	 */
	public static boolean isDark(final int red, final int green, final int blue) {
		return 1 - (0.299 * red + 0.587 * green + 0.114 * blue) / 0xFF >= 0.5D;
	}

	public static Color computeColor(final int ix, final int total, final float saturation) {
		final double hueValue = (double) ix / (double) total;
		return new Color(Color.HSBtoRGB((float) hueValue, saturation, HSB_BRIGHTNESS_FACTOR));
	}

	/**
	 * Restituisce un array di valori rgb composti creando
	 * una scala di colori con metodo HSB (o HSV).
	 *
	 * @param n numero di colori da generare.
	 * @return l'array di colori in rgb.
	 */
	@NonNull
	public static int[] generateColors(final int n) {
		return generateColors(n, 1F, HSB_SATURATION_FACTOR);
	}

	/**
	 * Restituisce un array di valori rgb composti creando
	 * una scala di colori con metodo HSB (o HSV).
	 *
	 * @param n numero di colori da generare.
	 * @return l'array di colori in rgb.
	 */
	@NonNull
	public static int[] generateColors(final int n, final float hue, final float saturation) {
		final int[] colors = new int[n];
		for (int i = 0; i < n; i++) {
			final double hueValue = (double) i / (double) n * hue;
			colors[i] = Color.HSBtoRGB((float) hueValue, saturation, HSB_BRIGHTNESS_FACTOR);
		}
		return colors;
	}

	public static String[] generateCssColors(final int n, final float hue, final float saturation) {
		final String[] colors = new String[n];
		for (int i = 0; i < n; i++) {
			final double hueValue = (double) i / (double) n * hue;
			final Color value = new Color(Color.HSBtoRGB((float) hueValue, saturation, HSB_BRIGHTNESS_FACTOR));
			colors[i] = "rgb(" + value.getRed() + "," + value.getGreen() + "," + value.getBlue() + ")";
		}
		return colors;
	}

	public static String[] generateCssColors(final int n) {
		return generateCssColors(n, 1F, HSB_SATURATION_FACTOR);
	}

	/**
	 * Restituisce la rappresentazione esadecimale del colore
	 * specificato senza canale alpha per l'html.
	 *
	 * <pre>
	 * toRGBHtml(new Color(255, 255, 0)) = "#FFFF00"
	 * </pre>
	 *
	 * @param color colore da rappresentare.
	 * @return la stringa del colore.
	 */
	@NonNull
	public static String toHexRGBHtml(final @NonNull Color color) {
		return "#" + Integer.toHexString(color.getRGB());
	}

	@NonNull
	public static String toRGBACss(final @NonNull Color color) {
		return "rgba(" + color.getRed() + "," + color.getGreen() + "," + color.getBlue() + "," + color.getAlpha() + ")";
	}

	public static Color tryDecode(final String colorHex) {
		if (colorHex != null) {
			try {
				return Color.decode(colorHex);
			} catch (final @SuppressWarnings("unused") NumberFormatException nfex) {
				// ignoro l'eccezione
			}
		}
		return null;
	}
}
