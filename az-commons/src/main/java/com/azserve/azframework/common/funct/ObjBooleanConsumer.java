package com.azserve.azframework.common.funct;

import java.util.function.BiConsumer;

/**
 * Rappresenta un'operazione che accetta un argomento {@code boolean} assieme
 * ad un oggetto e non restituisce un risultato.
 * Questa è la specializzazione della classe {@link BiConsumer} per
 * i valori primitivi {@code boolean}.
 */
@FunctionalInterface
public interface ObjBooleanConsumer<T> {

	/**
	 * Effettua l'operazione sui valori specificati.
	 *
	 * @param t l'oggetto in ingresso.
	 * @param value il valore primitivo in ingresso.
	 */
	void accept(T t, boolean value);
}
