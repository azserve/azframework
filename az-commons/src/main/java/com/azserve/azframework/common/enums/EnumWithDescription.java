package com.azserve.azframework.common.enums;

/**
 * Interfaccia implementata dalle enum
 * che hanno una descrizione.
 *
 * @author filippo
 * @since 18/05/2016
 */
public interface EnumWithDescription {

	String description();
}
