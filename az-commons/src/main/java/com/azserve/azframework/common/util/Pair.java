package com.azserve.azframework.common.util;

import java.io.Serializable;
import java.util.Objects;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Classe wrapper per una coppia di oggetti.
 *
 * La classe supporta riferimenti {@code null}.
 *
 * @param <T1> tipo del primo oggetto.
 * @param <T2> tipo del secondo oggetto.
 */
public final class Pair<T1, T2> implements Serializable {

	private static final long serialVersionUID = -7297997935814176961L;

	public static @NonNull <V1, V2> Pair<V1, V2> of(final V1 val1, final V2 val2) {
		return new Pair<>(val1, val2);
	}

	private final T1 value1;
	private final T2 value2;

	private Pair(final T1 value1, final T2 value2) {
		this.value1 = value1;
		this.value2 = value2;
	}

	public T1 getValue1() {
		return this.value1;
	}

	public T2 getValue2() {
		return this.value2;
	}

	public boolean equals(final T1 val1, final T2 val2) {
		return Objects.equals(val1, this.value1)
				&& Objects.equals(val2, this.value2);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.value1 == null ? 0 : this.value1.hashCode());
		result = prime * result + (this.value2 == null ? 0 : this.value2.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Pair<?, ?> other = (Pair<?, ?>) obj;
		if (this.value1 == null) {
			if (other.value1 != null) {
				return false;
			}
		} else if (!this.value1.equals(other.value1)) {
			return false;
		}
		if (this.value2 == null) {
			if (other.value2 != null) {
				return false;
			}
		} else if (!this.value2.equals(other.value2)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return this.value1 + ";" + this.value2;
	}

}
