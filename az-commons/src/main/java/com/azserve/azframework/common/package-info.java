/**
 * Fornisce classi comuni ed utilità per operare su tipi primitivi, oggetti,
 * collezioni, enum, eccezioni ed altro.
 * <p>
 * I metodi sono stati scritti in modo da essere il più possibile
 * ottimizzati.
 * </p>
 * <p>
 * <b>Qualsiasi classe o metodo aggiunto dovrà essere documentato a dovere.</b>
 * </p>
 * <table><tr><td width="90px">get</td> <td width="600px">Gets something from an object, such as <code>bean.getBar()</code>. This is also used with a key to lookup a list by index or a map by key, such as <code>list.get(index)</code> or <code>map.get(key)</code>.</td></tr><tr><td>is</td><td>Checks if something is true or false about an object or a property of an object. Example <code>foo.isValid()</code>.</td></tr>
 * <tr><td>check</td><td>Checks if something is true, throwing an exception if it is not true. Example <code>foo.checkValid()</code>.</td></tr><tr><td>contains</td><td>Checks if a collection contains the queried object, such as <code>coll.contains(bar)</code>. This can be used on classes that wrap, or otherwise act as, a collection.</td></tr><tr><td>remove</td><td>Removes an element from a collection, such as <code>coll.remove(bar)</code>. This can be used on classes that wrap, or otherwise act as, a collection.</td></tr><tr><td>clear</td><td>Clears the object, typically but not necessarily a collection, so that it is "empty".</td></tr><tr><td>put</td><td>Mutable putter. This mutates the target object replacing or adding some form of key-value pair. Examples are <code>map.put(key,bar)</code> and <code>bean.putFoo(key,bar)</code></td></tr><tr><td>set</td><td>Mutable setter. This mutates the target object setting a property, such as <code>bean.getBar(bar)</code>.</td></tr><tr><td>with</td><td>Immutable "setter". Returns a copy of the original with one or more properties changed, such as <code>result = original.withBar(bar)</code>.</tr><tr><td>to</td><td>Converts this object to an independent object, generally of another type. This generally takes no arguments, but might if it is appropriate.</td></tr><tr><td>as</td><td>Converts this object to another object where changes to the original are exposed in the result, such as <code>Arrays.asList()</code>.</td></tr><tr><td>build</td><td>Builds another object based on either the specified arguments, the state of the target object, or both.</td></tr><tr><td>add/subtract</td><td>Adds/subracts a value to the quantity. This mutates the target quantity (a number, date, time, distance...) adding/subracting the "foo" property. This name is also separately used for adding elements to a collection.</td></tr><br />
 * <tr><td>plus/minus</td><td>Immutable version of add/subract for a quantity. Returns a copy of the original with the value added/subtracted. This name does not seem to work as well for adding elements to an immutable collection.</td></tr><tr><td>reset</td><td>Resets the object back to a suitable initial state ready to be re-used.</td></tr></table><p>And here are some static method names:<br />
 * </p><table class="tbl"><tr> <td width="90px">of</td> <td width="600px">Static factory method. Typically used with immutable classes where constructors are private (permitting caching). This is used by <code>EnumSet</code> and ThreeTen/JSR-310.</td> </tr>
 * <tr> <td>valueOf</td> <td>Longer form of <code>of</code> used by the JDK.</td> </tr>
 * <tr> <td>from</td> <td>Longer form of <code>of</code>.</td> </tr>
 * <tr> <td>parse</td> <td>Static factory method that creates an instance of the class by parsing a string. This could just be another <code>of</code> method, but I think the semantics are clearer with a dedicated name.</td> </tr>
 * </table>
 */
package com.azserve.azframework.common;