package com.azserve.azframework.common.util;

import java.util.Arrays;
import java.util.Objects;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Classe che funge da chiave per mappe
 * sfruttando l'hash di una serie di oggetti.
 *
 * @since 25/10/2013
 */
public class ArrayKey {

	@NonNull
	private final Object[] objects;

	public ArrayKey(final @NonNull Object... objects) {
		// mi proteggo da un eventuale array null
		this.objects = Objects.requireNonNull(objects, "Objects required");
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null && obj instanceof ArrayKey) {
			return Arrays.equals(this.objects, ((ArrayKey) obj).objects);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(this.objects);
	}

	@Override
	public String toString() {
		return Arrays.toString(this.objects);
	}
}
