package com.azserve.azframework.common.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

public final class DateTimeUtils {

	public static OffsetDateTime toOffsetDateTimeFromUTC(final LocalDateTime dateTime) {
		return toOffsetDateTime(dateTime, ZoneOffset.UTC, ZoneOffset.UTC);
	}

	public static OffsetDateTime toOffsetDateTime(final LocalDateTime dateTime, final ZoneOffset sourceZoneOffset, final ZoneOffset targetZoneOffset) {
		return dateTime != null ? OffsetDateTime.ofInstant(dateTime.toInstant(sourceZoneOffset), targetZoneOffset) : null;
	}

	public static LocalDateTime toLocalDateTimeUTC(final OffsetDateTime offsetDateTime) {
		return toLocalDateTime(offsetDateTime, ZoneOffset.UTC);
	}

	public static LocalDateTime toLocalDateTime(final OffsetDateTime offsetDateTime, final ZoneOffset targetZoneOffset) {
		return offsetDateTime != null ? offsetDateTime.atZoneSameInstant(targetZoneOffset).toLocalDateTime() : null;
	}

	public static LocalDate toLastDayOfMonth(final LocalDate date) {
		return date == null ? null : date.withDayOfMonth(date.lengthOfMonth());
	}

	public static int age(final LocalDate birthDay, final LocalDate when) {
		return (int) birthDay.until(when, ChronoUnit.YEARS);
	}

	public static int ageToday(final LocalDate birthDay) {
		return age(birthDay, LocalDate.now());
	}

	public static boolean isBirthday(final LocalDate birthday, final LocalDate when) {
		final int whenLeapYear = when.isLeapYear() && when.getMonthValue() > Month.FEBRUARY.getValue() ? 1 : 0;
		final int birthdayLeapYear = birthday.isLeapYear() && birthday.getMonthValue() > Month.FEBRUARY.getValue() ? 1 : 0;
		return when.getDayOfYear() - whenLeapYear == birthday.getDayOfYear() - birthdayLeapYear;
	}

	public static boolean isBirthdayToday(final LocalDate birthday) {
		return isBirthday(birthday, LocalDate.now());
	}

	private DateTimeUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}
