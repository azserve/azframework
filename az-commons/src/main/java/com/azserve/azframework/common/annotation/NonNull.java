package com.azserve.azframework.common.annotation;

/**
 * Annotazione che indica che un campo,
 * un parametro o un metodo non è o non
 * ritorna mai {@code null}.
 *
 * @author filippo
 */
// @Target({ FIELD, METHOD, PARAMETER, LOCAL_VARIABLE, TYPE_PARAMETER, TYPE_USE })
/*
 * 28/02/2019: abbiamo lasciato commentato l'annotazione @Target.
 * Se togliamo il commento dobbiamo modificare alcuni metodi, ad esempio:
 *
 * -- DA
 * public static boolean equals(final @NonNull Object[] objs) {}
 * -- L'annotazione in questo caso indica che l'array objs non può essere null
 *
 * -- A
 * public static boolean equals(final @NonNull Object @NonNull[] objs) {}
 * -- In questo caso invce laa prima annotazione @NonNull indica che gli elementi
 * -- dell'array objs non possono essere null mentre la seconda indica
 * -- che l'array objs non può essere null
 *
 * Fin qui tutto bene. Il problema si è presentato con il metodo EntityDtoManager.findDto(R)
 * che da un errore qui:
 * this.findEntity(reference).map(this::toDto).orElse(null);
 * Indicando che il null finale non è valido perchè il tipo D del metodo .map non può
 * essere null
 */
public @interface NonNull {}