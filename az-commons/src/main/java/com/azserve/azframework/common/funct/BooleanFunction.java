package com.azserve.azframework.common.funct;

@FunctionalInterface
public interface BooleanFunction<R> {

	R apply(boolean value);
}
