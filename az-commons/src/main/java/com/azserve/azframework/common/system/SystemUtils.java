package com.azserve.azframework.common.system;

public final class SystemUtils {

	private SystemUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	public static OperatingSystemFamily getOperatingSystemFamily() {
		final String os1 = System.getProperty("os.name");
		if (os1 == null) {
			return OperatingSystemFamily.OTHER;
		}
		final String os = os1.toLowerCase();
		if ((os.indexOf("mac") >= 0) || (os.indexOf("darwin") >= 0)) {
			return OperatingSystemFamily.MAC_OS;
		} else if (os.indexOf("win") >= 0) {
			return OperatingSystemFamily.WINDOWS;
		} else if (os.indexOf("nux") >= 0) {
			return OperatingSystemFamily.LINUX;
		} else {
			return OperatingSystemFamily.OTHER;
		}
	}
}
