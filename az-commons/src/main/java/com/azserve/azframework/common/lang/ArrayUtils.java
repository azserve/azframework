package com.azserve.azframework.common.lang;

import static java.util.Objects.requireNonNull;

import java.util.Objects;
import java.util.Random;
import java.util.function.Function;
import java.util.function.IntFunction;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;

/**
 * Classe di utilità per gli array.
 *
 * @author filippo
 *
 * @since 13/12/2013
 */
public final class ArrayUtils {

	private ArrayUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/** Static empty {@code Object[]} instance */
	public static final Object[] EMPTY = new Object[0];

	/** Static empty {@code String[]} instance */
	public static final String[] EMPTY_STRING = new String[0];

	/** Static empty {@code byte[]} instance */
	public static final byte[] EMPTY_BYTE = new byte[0];

	/** Static empty {@code int[]} instance */
	public static final int[] EMPTY_INT = new int[0];

	/** Static empty {@code long[]} instance */
	public static final long[] EMPTY_LONG = new long[0];

	/** Static empty {@code double[]} instance */
	public static final double[] EMPTY_DOUBLE = new double[0];

	/** Static empty {@code boolean[]} instance */
	public static final boolean[] EMPTY_BOOLEAN = new boolean[0];

	/**
	 * Checks if the given array is empty or is a {@code null} reference.
	 *
	 * @param arr the array to be checked.
	 */
	public static boolean isNullOrEmpty(final Object[] arr) {
		return arr == null || arr.length == 0;
	}

	/**
	 * Checks if the given array is empty or is a {@code null} reference.
	 * If so an {@code IllegalArgumentException} is thrown.
	 *
	 * @param arr the array to be checked.
	 * @throws IllegalArgumentException if the given array is {@code null}
	 *             or empty.
	 */
	public static <T> T[] requireNonEmpty(final T[] array, final String message) {
		if (array == null || array.length == 0) {
			throw new IllegalArgumentException(message);
		}
		return array;
	}

	/**
	 * Creates a new array containing the same number
	 * of elements of the given array mapping the
	 * source element with the given mapping function.
	 *
	 * @param array the source array.
	 * @param arraySupplier a supplier for the new array.
	 * @param mapper the mapping function.
	 * @return new new array with mapped elements.
	 * @throws ArrayIndexOutOfBoundsException if {@code arraySupplier}
	 *             supplies a smaller array than the given one.
	 * @throws NullPointerException if any argument is {@code null}.
	 *
	 * @param <S> source array element type.
	 * @param <D> destination array element type.
	 */
	public static <S, D> D[] map(final @NonNull S[] array, final @NonNull IntFunction<D[]> arraySupplier, final @NonNull Function<S, D> mapper) {
		final int length = requireNonNull(array).length;
		final D[] destArray = requireNonNull(arraySupplier).apply(length);
		requireNonNull(mapper);
		for (int i = 0; i < length; i++) {
			destArray[i] = mapper.apply(array[i]);
		}
		return destArray;
	}

	/**
	 * Searches the specified array of longs for the specified value using
	 * a linear search with the {@linkplain Objects#equals(Object, Object)} method.
	 *
	 *
	 * @param array the source array.
	 * @param element the element to find.
	 *
	 * @return the first found element index or -1.
	 * @throws NullPointerException if the source array is {@code null}.
	 */
	public static <T> int linearSearch(final @NonNull T[] array, final @Nullable T element) {
		for (int i = 0; i < requireNonNull(array).length; i++) {
			if (Objects.equals(array[i], element)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Verifica se i valori di una porzione di array
	 * sono uguali (==) al valore specificato.
	 *
	 * @param array array di {@code boolean}.
	 * @param value valore da verificare.
	 * @param startIndex indice iniziale dell'array (inclusivo).
	 * @param endIndex indice finale dell'array (esclusivo).
	 * @return {@code true} se tutti i valori all'interno
	 *         del range specificato solo uguali a {@code value}.
	 *         Se gli indici del range corrispondono (range vuoto)
	 *         restituisce {@code false}.
	 * @throws ArrayIndexOutOfBoundsException se gli indici
	 *             specificati non sono validi.
	 */
	public static boolean equalsAll(final @NonNull boolean[] array, final boolean value, final int startIndex, final int endIndex) {
		requireNonNull(array);
		if (startIndex < 0 || startIndex >= array.length || endIndex > array.length) {
			throw new ArrayIndexOutOfBoundsException();
		}
		if (startIndex == endIndex) {
			return false;
		}
		for (int i = startIndex; i < endIndex; i++) {
			if (array[i] != value) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Restituisce un array contenente gli elementi degli array
	 * specificati.
	 *
	 * Se entrambi gli array sono vuoti o il secondo array
	 * è vuoto restituisce il primo array, altrimenti
	 * se solo il secondo array è vuoto restituisce il primo.
	 *
	 * @param supplier fornitore dell'array risultato
	 *            a partire dalla lunghezza necessaria.
	 * @param array primo array contenente degli oggetti.
	 * @param elements secondo array contenente altri oggetti.
	 */
	public static @SafeVarargs @NonNull <T> T[] concatElements(final @NonNull IntFunction<T[]> supplier, final @NonNull T[] array, final @NonNull T... elements) {
		if (requireNonNull(elements).length == 0) {
			return array;
		}
		if (requireNonNull(array).length == 0) {
			return elements;
		}
		requireNonNull(supplier);
		final int length = array.length + elements.length;
		final T[] result = supplier.apply(length);
		int i = 0;
		for (final T element : array) {
			result[i++] = element;
		}
		for (final T element : elements) {
			result[i++] = element;
		}
		return result;
	}

	/**
	 * Crea un nuovo array contenente gli elementi degli array
	 * specificati.
	 *
	 * @param supplier fornitore dell'array risultato
	 *            a partire dalla lunghezza necessaria.
	 * @param arrays array da concatenare.
	 */
	public static @SafeVarargs @NonNull <T> T[] concat(final @NonNull IntFunction<T[]> supplier, final @NonNull T[]... arrays) {
		requireNonNull(supplier);
		requireNonNull(arrays);
		int length = 0;
		for (final T[] array : arrays) {
			length += array.length;
		}
		final T[] result = supplier.apply(length);
		int i = 0;
		for (final T[] array : arrays) {
			for (final T element : array) {
				result[i++] = element;
			}
		}
		return result;
	}

	/**
	 * Permuta casualmente gli elementi dell'array.
	 *
	 * @param array l'array interessato.
	 *
	 * @since 27/06/2014
	 */
	public static void shuffle(final @NonNull char[] array) {
		requireNonNull(array);
		int index;
		char temp;
		final Random random = new Random();
		for (int i = array.length - 1; i > 0; i--) {
			index = random.nextInt(i + 1);
			temp = array[index];
			array[index] = array[i];
			array[i] = temp;
		}
	}

	/**
	 * Permuta casualmente gli elementi dell'array.
	 *
	 * @param array l'array interessato.
	 *
	 * @since 27/06/2014
	 */
	public static void shuffle(final @NonNull int[] array) {
		requireNonNull(array);
		int index;
		final Random random = new Random();
		for (int i = array.length - 1; i > 0; i--) {
			index = random.nextInt(i + 1);
			if (index != i) {
				// chicca per evitare di utilizzare una variabile
				array[index] ^= array[i];
				array[i] ^= array[index];
				array[index] ^= array[i];
			}
		}
	}
}
