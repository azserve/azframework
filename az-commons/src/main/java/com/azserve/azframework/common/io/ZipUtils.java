package com.azserve.azframework.common.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Classe di utilità per la compressione/decompressione di dati
 * tramite il formato zip.
 *
 * @author matteo
 */
public final class ZipUtils {

	/** Dimensione predefinita del buffer (1 Mb) */
	private static final int BUFFER_SIZE = 1024 * 1024;

	private ZipUtils() {}

	/**
	 * Comprime tramite {@link ZipEntry}.
	 *
	 * @param data dati da comprimere.
	 * @return i dati compressi sotto forma di array di byte.
	 * @since 09/07/2012
	 */
	public static byte[] zip(final byte[] data, final String entryName) throws IOException {
		if (data == null) {
			return null;
		}
		try (final ByteArrayOutputStream dest = new ByteArrayOutputStream()) {
			try (final ZipOutputStream out = new ZipOutputStream(dest); final InputStream origin = new ByteArrayInputStream(data)) {
				final byte[] ba = new byte[BUFFER_SIZE];
				int count;
				final ZipEntry zipEntry = new ZipEntry(entryName);
				out.putNextEntry(zipEntry);
				while ((count = origin.read(ba, 0, BUFFER_SIZE)) != -1) {
					out.write(ba, 0, count);
				}
				/*
				 * IMPORTANTE: Termina la scrittura nel contenuto del file .zip
				 * Se lo si omette in compilazione e run non viene generato nessun errore
				 * ma una volta salvato il file nel pc questo risulterà errato e
				 * non apribile come archivio.
				 */
				out.finish();

				out.closeEntry();
			}
			return dest.toByteArray();
		}
	}

	/**
	 * Decomprime tramite {@link ZipEntry}.
	 *
	 * @param zipData dati da decomprimere.
	 * @return {@link String}
	 * @since 31/05/2012
	 */
	public static String unzip(final byte[] zipData) throws IOException {
		return unzip(zipData, BUFFER_SIZE);
	}

	/**
	 * Decomprime tramite {@link ZipEntry}.
	 *
	 * @param zipData dati da decomprimere.
	 * @param bufferSize dimensione del buffer.
	 * @return dati decompressi sotto forma di stringa.
	 * @since 31/05/2012
	 */
	public static String unzip(final byte[] zipData, final int bufferSize) throws IOException {
		final ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(zipData));
		String result = "";
		while (zipInputStream.getNextEntry() != null) {
			final byte[] buffer = new byte[bufferSize];
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			int size = 0;
			while ((size = zipInputStream.read(buffer)) > 0) {
				baos.write(buffer, 0, size);
			}
			result += new String(baos.toByteArray());
		}
		return result;
	}

	/**
	 * Comprime un array di byte impostando la dimensione del buffer a 1Mb (1024 * 1024)
	 * tramite {@link Deflater} e {@link Inflater}.
	 *
	 * @param data dati da comprimere.
	 * @return dati compressi.
	 * @since 16/05/2012
	 */
	public static byte[] compress(final byte[] data) throws IOException {
		return compress(data, BUFFER_SIZE);
	}

	/**
	 * Comprime un array di byte tramite {@link Deflater} e {@link Inflater}.
	 *
	 * @param data dati da comprimere.
	 * @param bufferSize dimensione del buffer.
	 * @return dati compressi.
	 * @since 16/05/2012
	 */
	public static byte[] compress(final byte[] data, final int bufferSize) throws IOException {
		final Deflater compressor = new Deflater();
		compressor.setLevel(Deflater.BEST_COMPRESSION);
		compressor.setInput(data);
		compressor.finish();
		try (final ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length)) {
			final byte[] buf = new byte[bufferSize];
			while (!compressor.finished()) {
				final int count = compressor.deflate(buf);
				bos.write(buf, 0, count);
			}
			return bos.toByteArray();
		}
	}

	/**
	 * Decomprime un array di byte impostando la dimensione del buffer
	 * a 1Mb (1024 * 1024) tramite {@link Deflater} e {@link Inflater}.
	 *
	 * @param compressedData dati compressi.
	 * @return dati decompressi.
	 * @since 16/05/2012
	 */
	public static byte[] decompress(final byte[] compressedData) throws Exception {
		return decompress(compressedData, BUFFER_SIZE);
	}

	/**
	 * Decomprime un byte[] tramite {@link Deflater} e {@link Inflater}.
	 *
	 * @param compressedData dati compressi.
	 * @param bufferSize dimensione del buffer.
	 * @return dati decompressi.
	 * @since 16/05/2012
	 */
	public static byte[] decompress(final byte[] compressedData, final int bufferSize) throws Exception {
		final Inflater decompressor = new Inflater();
		decompressor.setInput(compressedData);
		try (final ByteArrayOutputStream bos = new ByteArrayOutputStream(compressedData.length)) {
			final byte[] buf = new byte[bufferSize];
			while (!decompressor.finished()) {
				final int count = decompressor.inflate(buf);
				bos.write(buf, 0, count);
			}
			return bos.toByteArray();
		}
	}

	/**
	 * Estrae i file contenuti nell'archivio specificato.
	 *
	 * @param zipFile file zip contenente i file.
	 * @param destDir directory di destinazione (viene creata
	 *            se non esiste).
	 * @throws IOException in caso di errori di accesso/scrittura ai
	 *             vari file coinvolti.
	 * @since 18/09/2014
	 */
	public static void unzip(final File zipFile, final File destDir) throws IOException {
		try (BufferedInputStream zipInputStream = new BufferedInputStream(new FileInputStream(zipFile))) {
			unzip(zipInputStream, destDir);
		}
	}

	/**
	 * Estrae i file contenuti nell'archivio specificato.
	 *
	 * @param zipFile file zip contenente i file.
	 * @param destDir directory di destinazione (viene creata
	 *            se non esiste).
	 * @throws IOException in caso di errori di accesso/scrittura ai
	 *             vari file coinvolti.
	 */
	public static void unzip(final InputStream zipInputStream, final File destDir) throws IOException {
		if (!destDir.exists()) {
			destDir.mkdirs();
		}
		try (final ZipInputStream zipIn = new ZipInputStream(zipInputStream)) {
			ZipEntry entry = null;
			// ciclo le entry del file zip
			while ((entry = zipIn.getNextEntry()) != null) {
				final String filePath = destDir.getAbsolutePath() + File.separator + entry.getName();
				if (!entry.isDirectory()) {
					// se la entry è un file lo estraggo
					try (final BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath))) {
						final byte[] bytesIn = new byte[BUFFER_SIZE];
						int read = 0;
						while ((read = zipIn.read(bytesIn)) != -1) {
							bos.write(bytesIn, 0, read);
						}
					}
				} else {
					// se la entry è una cartella, la creo
					final File dir = new File(filePath);
					dir.mkdir();
				}
				zipIn.closeEntry();
			}
		}
	}

	public static void unzip(final InputStream zipInputStream, final Path destDir) throws IOException {
		if (!Files.exists(destDir)) {
			Files.createDirectories(destDir);
		}
		try (final ZipInputStream zipIn = new ZipInputStream(zipInputStream)) {
			ZipEntry entry = null;
			// ciclo le entry del file zip
			while ((entry = zipIn.getNextEntry()) != null) {
				final Path filePath = destDir.resolve(entry.getName());
				if (!entry.isDirectory()) {
					// se la entry è un file lo estraggo
					try (final OutputStream os = Files.newOutputStream(filePath);
							final BufferedOutputStream bos = new BufferedOutputStream(os)) {
						final byte[] bytesIn = new byte[BUFFER_SIZE];
						int read = 0;
						while ((read = zipIn.read(bytesIn)) != -1) {
							bos.write(bytesIn, 0, read);
						}
					}
				} else {
					// se la entry è una cartella, la creo
					Files.createDirectories(filePath);
				}
				zipIn.closeEntry();
			}
		}
	}

	/**
	 * Estrae un file specifico da un archivio.
	 *
	 * @param zipFile file zip contenente i file.
	 * @param dest file di destinazione.
	 * @param fileName nome del file da estrarre.
	 * @throws IOException in caso di errori di accesso/scrittura ai
	 *             vari file coinvolti.
	 * @since 18/09/2014
	 */
	public static void unzip(final File zipFile, final File dest, final String fileName) throws IOException {
		try (final ZipFile zf = new ZipFile(zipFile)) {
			final ZipEntry entry = zf.getEntry(fileName);
			if (entry != null) {
				final File destination = dest.isDirectory() ? new File(dest.getAbsolutePath(), entry.getName()) : dest;
				if (!entry.isDirectory()) {
					try (final InputStream is = zf.getInputStream(entry); final BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destination))) {
						IOUtils.copy(is, bos);
					}
				}
			}
		}
	}
}
