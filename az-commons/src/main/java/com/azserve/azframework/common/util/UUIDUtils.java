package com.azserve.azframework.common.util;

import java.nio.ByteBuffer;
import java.util.UUID;

public final class UUIDUtils {

	private UUIDUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	public static byte[] getBytesFromUUID(final UUID uuid) {
		if (uuid == null) {
			return null;
		}
		final ByteBuffer buffer = ByteBuffer.wrap(new byte[16]);
		buffer.putLong(uuid.getMostSignificantBits());
		buffer.putLong(uuid.getLeastSignificantBits());
		return buffer.array();
	}

	public static UUID getUUIDFromBytes(final byte[] bytes) {
		if (bytes == null) {
			return null;
		}
		if (bytes.length != 16) {
			throw new IllegalArgumentException("Bytes length = " + bytes.length);
		}
		final ByteBuffer buffer = ByteBuffer.wrap(bytes);
		return new UUID(buffer.getLong(), buffer.getLong());
	}

}
