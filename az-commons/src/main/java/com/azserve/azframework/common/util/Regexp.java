package com.azserve.azframework.common.util;

public final class Regexp {

	/**
	 * Accetta solo una stringa contenente un indirizzo email
	 */
	public static final String EMAIL = "^[A-Za-z0-9._%+-]+@(?:[A-Za-z0-9-]+\\.)+[A-Za-z]{2,}$";

	/**
	 * Accetta una stringa vuota o un indirizzo email
	 */
	public static final String EMPTY_OR_EMAIL = "^$|" + EMAIL;

	private Regexp() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}