package com.azserve.azframework.common.lang;

import static java.util.Objects.requireNonNull;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.reflect.ReflectionUtils;

/**
 * Classe di utilità generiche per la gestione di oggetti.
 *
 * @since 12/06/2012
 */
public final class ObjectUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(ObjectUtils.class);

	/**
	 * Oggetto condiviso da utilizzare per rappresentare
	 * un valore {@code null}.
	 */
	public static final Object NULL = new Object();

	private ObjectUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Verifica se tutti gli oggetti specificati sono "uguali".
	 * Ammette riferimenti {@code null}.
	 *
	 * @param objs array di oggetti.
	 */
	public static boolean equals(final @NonNull Object[] objs) {
		if (objs.length < 2) {
			return true;
		}
		final Object first = objs[0];
		for (int i = 1; i < objs.length; i++) {
			if (!Objects.equals(first, objs[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Controlla se un oggetto è "uguale" ad uno degli oggetti passati.
	 *
	 * <pre>
	 * equals("due") -> false
	 * equals("due", "due") -> true
	 * equals(Integer.valueOf(5), "tre", Integer.valueOf(10/2)) -> true
	 * equals("due", "tre") -> false
	 * equals("due", "uno", "tre") -> false
	 * </pre>
	 *
	 * @param source oggetto da analizzare.
	 *
	 * @throws NullPointerException se l'oggetto da analizzare
	 *             oppure uno degli oggetti è {@code null}.
	 */
	public static boolean equals(final @NonNull Object source, final @NonNull Object... objs) {
		requireNonNull(source);
		// Arrays.stream(objs).anyMatch(obj -> source.equals(obj));
		for (final Object obj : requireNonNull(objs)) {
			if (source.equals(obj)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Restituisce un valore di default se un oggetto è {@code null} altrimenti l'oggetto stesso.
	 *
	 * <p>Utilizzo:</p>
	 * <blockquote>
	 *
	 * <pre>
	 * Integer calculated = obj.longTask();
	 * doSomething(calculated == null ? Integer.valueOf(0) : calculated);
	 * </pre>
	 *
	 * </blockquote>
	 * <blockquote>
	 *
	 * <pre>
	 * doSomething(ifNull(obj.longTask(), Integer.valueOf(0)));
	 * </pre>
	 *
	 * </blockquote>
	 *
	 * @param obj oggetto da esaminare.
	 * @param defaultValue valore di default.
	 */
	public static <T> T ifNull(final @Nullable T obj, final @Nullable T defaultValue) {
		return obj == null ? defaultValue : obj;
	}

	/**
	 * Restituisce una funzione {@code Supplier} che fornisce
	 * un valore di default se un oggetto è {@code null} altrimenti l'oggetto stesso.
	 *
	 * <p>Utilizzo:</p>
	 *
	 * <pre>
	 * Integer calculated = obj.longTask();
	 * doSomething(calculated == null ? obj.otherLongTask() : calculated);
	 * </pre>
	 *
	 * Diventa:
	 *
	 * <pre>
	 * doSomething(ifNull(obj.longTask(), obj::otherLongTask));
	 * </pre>
	 *
	 * In alternativa:
	 *
	 * <pre>
	 * doSomething(Optional.ofNullable(obj.longTask()).orElseGet(obj::otherLongTask));
	 * </pre>
	 *
	 * </blockquote>
	 *
	 * @param obj oggetto da esaminare.
	 * @param defaultValueSupplier funzione che fornisce il valore di default.
	 *
	 * @throws NullPointerException se sia obj che la funzione sono {@code null}.
	 */
	public static <T> T ifNull(final @Nullable T obj, final Supplier<? extends T> defaultValueSupplier) {
		return obj != null ? obj : defaultValueSupplier.get();
	}

	public static <T> T requireNonNullElse(final @Nullable T obj, final T defaultValue) {
		return obj != null ? obj : requireNonNull(defaultValue, "defaultValue");
	}

	public static <T> T requireNonNullElseGet(final @Nullable T obj, final Supplier<? extends T> supplier) {
		return obj != null ? obj : requireNonNull(requireNonNull(supplier, "supplier").get(), "supplier.get()");
	}

	/**
	 * <p>
	 * Confronta due oggetti {@linkplain Comparable} tramite
	 * il metodo {@linkplain Comparable#compareTo(Object)}.
	 * </p>
	 * Entrambi gli oggetti possono essere {@code null}.
	 *
	 * @param obj1 primo oggetto.
	 * @param obj2 secondo oggetto.
	 */
	public static <T extends Comparable<? super T>> int nullSafeCompare(final @Nullable T obj1, final @Nullable T obj2) {
		return obj1 == obj2 ? 0 : obj1 == null ? -1 : obj2 == null ? 1 : obj1.compareTo(obj2);
	}

	/**
	 * <p>
	 * Confronta due oggetti tramite comparatore.
	 * </p>
	 * Entrambi gli oggetti possono essere {@code null}.
	 *
	 * @param comparator oggetto utilizzato per il confronto.
	 * @param obj1 primo oggetto.
	 * @param obj2 secondo oggetto.
	 */
	public static <T> int nullSafeCompare(final @NonNull Comparator<? super T> comparator, final @Nullable T obj1, final @Nullable T obj2) {
		return obj1 == obj2 ? 0 : obj1 == null ? -1 : obj2 == null ? 1 : comparator.compare(obj1, obj2);
	}

	/**
	 * Restituisce l'oggetto con valore più basso dettato
	 * dal metodo {@linkplain Comparable#compareTo(Object)}.
	 * Se il confronto restituisce 0, verrà restituito il primo valore
	 *
	 * <pre>
	 * Integer value1 = Integer.valueOf(5);
	 * Integer value2 = Integer.valueOf(2);
	 *
	 * min(null, null)     -> null
	 * min(null, value1)   -> value1
	 * min(value1, value2) -> value2
	 * </pre>
	 *
	 * @param obj1 primo oggetto da confrontare.
	 * @param obj2 secondo oggetto da confrontare.
	 * @return l'oggetto con valore più basso tra i due.
	 */
	public static <T extends Comparable<? super T>> T min(final T obj1, final T obj2) {
		return obj1 == null ? obj2 : nullSafeCompare(obj1, obj2) <= 0 ? obj1 : obj2;
	}

	/**
	 * Restituisce l'oggetto con valore più basso dettato
	 * dal metodo {@linkplain Comparator#compare(Object, Object)}.
	 * Se il comparatore restituisce 0, verrà restituito il primo valore
	 *
	 * @param comparator oggetto utilizzato per il confronto.
	 * @param obj1 primo oggetto da confrontare.
	 * @param obj2 secondo oggetto da confrontare.
	 * @return l'oggetto con valore più basso tra i due.
	 * @throws NullPointerException se {@code objs} è {@code null}.
	 */
	public static <T> T min(final @NonNull Comparator<? super T> comparator, final T obj1, final T obj2) {
		return nullSafeCompare(requireNonNull(comparator), obj1, obj2) <= 0 ? obj1 : obj2;
	}

	/**
	 * Restituisce l'oggetto con valore più basso dettato
	 * dal metodo {@linkplain Comparable#compareTo(Object)}.
	 *
	 * <pre>
	 * min() -> null
	 *
	 * Integer nullValue = null;
	 * Integer value1 = Integer.valueOf(5);
	 * Integer value2 = Integer.valueOf(2);
	 *
	 * min(nullValue) -> null
	 * min(value1, value2) -> value2
	 * min(null, value1, value2) -> value2
	 * </pre>
	 *
	 * @param objs array di oggetti da confrontare.
	 * @return l'oggetto con valore più basso.
	 * @throws NullPointerException se {@code objs} è {@code null}.
	 */
	@SafeVarargs
	public static <T extends Comparable<? super T>> T min(final @NonNull T... objs) {
		// Arrays.stream(objs).min(Comparator.naturalOrder()).orElse(null);
		T min = null;
		for (final T obj : objs) {
			if (obj != null && (min == null || obj.compareTo(min) < 0)) {
				min = obj;
			}
		}
		return min;
	}

	/**
	 * Restituisce l'oggetto con valore più basso dettato
	 * dal metodo {@linkplain Comparator#compare(Object, Object)}.
	 *
	 * @param comparator oggetto utilizzato per il confronto.
	 * @param objs array di oggetti da confrontare.
	 * @return l'oggetto con valore più basso.
	 * @throws NullPointerException se {@code objs} è {@code null}.
	 */
	public static @SafeVarargs <T> T min(final @NonNull Comparator<? super T> comparator, final @NonNull T... objs) {
		// Arrays.stream(objs).min(comparator).orElse(null);
		requireNonNull(comparator);
		requireNonNull(objs);
		T min = null;
		for (final T obj : objs) {
			if (obj != null && (min == null || comparator.compare(obj, min) < 0)) {
				min = obj;
			}
		}
		return min;
	}

	/**
	 * Restituisce l'oggetto con valore più alto dettato
	 * dal metodo {@linkplain Comparable#compareTo(Object)}.
	 * Se il confronto restituisce 0, verrà restituito il primo valore
	 *
	 * <pre>
	 * Integer value1 = Integer.valueOf(5);
	 * Integer value2 = Integer.valueOf(2);
	 *
	 * max(null, null)     -> null
	 * max(null, value1)   -> value1
	 * max(value1, value2) -> value1
	 * </pre>
	 *
	 * @param obj1 primo oggetto da confrontare.
	 * @param obj2 secondo oggetto da confrontare.
	 * @return l'oggetto con valore più alto tra i due.
	 */
	public static <T extends Comparable<? super T>> T max(final T obj1, final T obj2) {
		return nullSafeCompare(obj1, obj2) >= 0 ? obj1 : obj2;
	}

	/**
	 * Restituisce l'oggetto con valore più alto dettato
	 * dal metodo {@linkplain Comparator#compare(Object, Object)}.
	 * Se il comparatore restituisce 0, verrà restituito il primo valore
	 *
	 * @param comparator oggetto utilizzato per il confronto.
	 * @param obj1 primo oggetto da confrontare.
	 * @param obj2 secondo oggetto da confrontare.
	 * @return l'oggetto con valore più alto tra i due.
	 * @throws NullPointerException se {@code objs} è {@code null}.
	 */
	public static <T> T max(final @NonNull Comparator<? super T> comparator, final T obj1, final T obj2) {
		return nullSafeCompare(requireNonNull(comparator), obj1, obj2) >= 0 ? obj1 : obj2;
	}

	/**
	 * Restituisce l'oggetto con valore più alto dettato
	 * dal metodo {@linkplain Comparable#compareTo(Object)}.
	 *
	 * <pre>
	 * max() -> null
	 *
	 * Integer nullValue = null;
	 * Integer value1 = Integer.valueOf(5);
	 * Integer value2 = Integer.valueOf(2);
	 *
	 * max(nullValue) -> null
	 * max(value1, value2) -> value1
	 * max(null, value1, value2) -> value1
	 * </pre>
	 *
	 * @param objs array di oggetti da confrontare.
	 * @return l'oggetto con valore più alto.
	 * @throws NullPointerException se {@code objs} è {@code null}.
	 */
	public static @SafeVarargs <T extends Comparable<? super T>> T max(final @NonNull T... objs) {
		// Arrays.stream(objs).max(Comparator.naturalOrder()).orElse(null);
		requireNonNull(objs);
		T max = null;
		for (final T obj : objs) {
			if (obj != null && (max == null || obj.compareTo(max) > 0)) {
				max = obj;
			}
		}
		return max;
	}

	/**
	 * Restituisce l'oggetto con valore più alto dettato
	 * dal metodo {@linkplain Comparator#compare(Object, Object)}.
	 *
	 * @param comparator oggetto utilizzato per il confronto.
	 * @param objs array di oggetti da confrontare.
	 * @return l'oggetto con valore più alto.
	 * @throws NullPointerException se {@code objs} è {@code null}.
	 */
	public static @SafeVarargs <T> T max(final @NonNull Comparator<? super T> comparator, final @NonNull T... objs) {
		// Arrays.stream(objs).max(comparator).orElse(null);
		requireNonNull(comparator);
		T max = null;
		for (final T obj : objs) {
			if (obj != null && (max == null || comparator.compare(obj, max) > 0)) {
				max = obj;
			}
		}
		return max;
	}

	/**
	 * Copia i valori dei campi di un oggetto su un altro oggetto
	 * attraverso la reflection sfruttando i metodi get/is/set.
	 *
	 * FIXME Filippo : considerare campi Collection/Map ??
	 *
	 * @param orig oggetto originale (obbligatorio).
	 * @param copy oggetto da modificare (obbligatorio).
	 * @return l'oggetto copy
	 */
	public static <T> T copyObject(final @NonNull T orig, final @NonNull T copy) {
		// non-null checks done by .getClass() methods
		final Class<?> origClass = orig.getClass();
		final Method[] methods = copy.getClass().getMethods();
		for (final Method method : methods) {
			final String methodName = method.getName();
			if (Modifier.isPublic(method.getModifiers()) && methodName.startsWith("set")) {
				final String fieldName = methodName.substring(3);
				final Method m = ReflectionUtils.findGetterMethod(origClass, fieldName);
				if (m != null && Modifier.isPublic(m.getModifiers())) {
					try {
						method.invoke(copy, m.invoke(orig));
					} catch (final ReflectiveOperationException e) {
						LOGGER.error("copyObject(T, T1)", e);
					}
				}
			}
		}
		return copy;
	}

	/**
	 * Copia i valori dei campi di un oggetto su un altro oggetto
	 * attraverso la reflection sfruttando i metodi get/is/set.
	 *
	 * FIXME Filippo : considerare campi Collection/Map ??
	 *
	 * @param orig oggetto originale (obbligatorio).
	 * @param copy oggetto da modificare (obbligatorio).
	 * @return l'oggetto copy
	 */
	public static <T, T1 extends T> T1 copyObject(final @NonNull Class<T> beanClass, final @NonNull T1 orig, final @NonNull T1 copy) {
		requireNonNull(beanClass);
		requireNonNull(orig);
		final Method[] methods = copy.getClass().getMethods();
		for (final Method method : methods) {
			final String methodName = method.getName();
			if (Modifier.isPublic(method.getModifiers()) && methodName.startsWith("set")) {
				final String fieldName = methodName.substring(3);
				final Method m = ReflectionUtils.findGetterMethod(beanClass, fieldName);
				if (m != null && Modifier.isPublic(m.getModifiers())) {
					try {
						method.invoke(copy, m.invoke(orig));
					} catch (final ReflectiveOperationException e) {
						LOGGER.error("copyObject(T, T1)", e);
					}
				}
			}
		}
		return copy;
	}

	/**
	 * Confronta due oggetti.
	 *
	 * Se sono entrambi {@code Comparable} utilizza il metodo
	 * {@linkplain Comparable#compareTo(Object)}, altrimenti
	 * il metodo {@linkplain Object#equals(Object)}.
	 *
	 * Sono ammessi valori {@code null}.
	 *
	 * @param o1 primo oggetto.
	 * @param o2 secondo oggetto.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T, T1 extends T> boolean checkEquality(final @Nullable T o1, final @Nullable T1 o2) {
		if (o1 instanceof Comparable && o2 instanceof Comparable) {
			if (((Comparable) o1).compareTo(o2) != 0) {
				return false;
			}
		} else if (!Objects.equals(o1, o2)) {
			return false;
		}
		return true;
	}

	/**
	 * Confronta due oggetti attraverso la reflection sfruttando i metodi get/is/set.
	 * <p>
	 * Se un campo implementa {@code Comparable} l'uguaglianza viene
	 * valutata con il metodo {@linkplain Comparable#compareTo}.
	 * </p>
	 *
	 * Il metodo non è commutativo
	 *
	 * @param obj1 primo oggetto.
	 * @param obj2 secondo oggetto.
	 * @return {@code true} se tutti i campi dei due oggetti hanno
	 *         stesso valore (attraverso il metodo equals.
	 */
	public static boolean compareObject(final @Nullable Object obj1, final @Nullable Object obj2) {
		if (obj1 == obj2) {
			return true;
		}
		if (obj1 == null || obj2 == null) {
			return false;
		}
		final Class<?> clazz = obj1.getClass();
		final Method[] methods = clazz.getMethods();
		for (final Method method : methods) {
			final String methodName = method.getName();
			if (Modifier.isPublic(method.getModifiers()) && method.getParameterTypes().length == 0) {
				if (methodName.startsWith("get") || methodName.startsWith("is")) {
					try {
						if (!checkEquality(method.invoke(obj1), method.invoke(obj2))) {
							return false;
						}
					} catch (final Exception ex) {
						LOGGER.error("Cannot retrieve reflective information", ex);
						return false;
					}

				}
			}
		}
		return true;
	}

	public static <T> boolean compareList(final @NonNull List<T> orig, final @NonNull List<T> copy) {
		return requireNonNull(orig).equals(requireNonNull(copy))
				&& orig.stream().allMatch(t -> ObjectUtils.compareObject(t, copy.get(copy.indexOf(t))));
	}

	/**
	 * Verifica se tutti gli oggetti sono {@code null}.
	 *
	 * @param objs gli oggetti da analizzare.
	 * @return {@code true} se tutti gli elementi sono {@code null},
	 *         {@code false} altrimenti.
	 */
	public static boolean allNull(final @NonNull Object... objs) {
		// Arrays.stream(objs).allMatch(Objects::isNull)
		for (final Object obj : objs) {
			if (obj != null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifica se almeno uno degli oggetti è {@code null}.
	 *
	 * @param objs gli oggetti da analizzare.
	 * @return {@code true} se almeno un elemento è {@code null},
	 *         {@code false} altrimenti.
	 */
	public static boolean anyNull(final @NonNull Object... objs) {
		// Arrays.stream(objs).anyMatch(Objects::isNull)
		for (final Object obj : objs) {
			if (obj == null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Verifica se tutti gli oggetti sono {@code null} o vuoti.
	 * Se un oggetto è di tipo {@code Collection} verifica il metodo {@link Collection#isEmpty()}.
	 * Se un oggetto è di tipo {@code Map} verifica il metodo {@link Map#isEmpty()}.
	 * Se un oggetto è di tipo {@code String} verifica {@code trim.equals("")}.
	 *
	 * @param objs gli oggetti da analizzare.
	 * @return {@code true} se tutti gli elementi sono {@code null} o vuoti,
	 *         {@code false} altrimenti.
	 */
	public static boolean allNullOrEmpty(final @NonNull Object... objs) {
		for (final Object obj : objs) {
			if (!isNullOrEmpty(obj)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifica se l'oggetto specificato è {@code null} o vuoto.<ul>
	 * <li>se un oggetto è di tipo {@code Collection} verifica il metodo {@link Collection#isEmpty()};</li>
	 * <li>se un oggetto è di tipo {@code Map} verifica il metodo {@link Map#isEmpty()};</li>
	 * <li>se un oggetto è di tipo {@code String} verifica {@link String#isEmpty() String.trim.isEmpty()};</li>
	 * </ul>
	 *
	 * @param obj l'oggetto da analizzare.
	 * @return {@code true} se l'elemento è {@code null} o vuoto, {@code false} altrimenti.
	 */
	public static boolean isNullOrEmpty(final Object obj) {
		if (obj == null) {
			return true;
		}
		if (obj instanceof String) {
			if (((String) obj).trim().isEmpty()) {
				return true;
			}
		} else if (obj instanceof Collection) {
			if (((Collection<?>) obj).isEmpty()) {
				return true;
			}
		} else if (obj instanceof Map) {
			if (((Map<?, ?>) obj).isEmpty()) {
				return true;
			}
		}
		return false;
	}
}
