/**
 * <p>
 * Fornisce varie classi di utilità per le operazioni più comuni su
 * oggetti dei package {@code java.io} e {@code java.nio}. 
 * </p>
 */
package com.azserve.azframework.common.io;