package com.azserve.azframework.common.security;

import java.util.Random;

import com.azserve.azframework.common.lang.ArrayUtils;

/**
 * Classe di utilità per la generazione di password.
 *
 * @author luca
 * @since 27/06/2014
 */
public final class PasswordUtils {

	private static final char[] CHARS = { '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', 'I', 'O', 'l', 'K', 'k' };
	private static final char[] DIGITS = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
	private static final char[] UPPER_CHARS = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'I', 'O', 'K' };
	private static final char[] LOWER_CHARS = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'l', 'k' };

	private static final int AMBIGUOUS_DIGITS = 1;
	private static final int AMBIGUOUS_UPPER_CHARS = 3;
	private static final int AMBIGUOUS_LOWER_CHARS = 2;
	private static final int AMBIGUOUS_CHARS = AMBIGUOUS_DIGITS + AMBIGUOUS_UPPER_CHARS + AMBIGUOUS_LOWER_CHARS;

	private PasswordUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Genera una password casuale specificando alcuni
	 * parametri.
	 *
	 * @param length lunghezza della password.
	 * @param upperCase indica se includere lettere maiuscole.
	 * @param lowerCase indica se includere lettere minuscole.
	 * @param digits indica se includere cifre.
	 *
	 * @since 27/06/2014
	 */
	public static String generate(final int length, final boolean upperCase, final boolean lowerCase, final boolean digits) {
		return generate(length, upperCase, lowerCase, digits, false);
	}

	/**
	 * Genera una password casuale specificando alcuni
	 * parametri.
	 *
	 * @param length lunghezza della password.
	 * @param upperCase indica se includere lettere maiuscole.
	 * @param lowerCase indica se includere lettere minuscole.
	 * @param digits indica se includere cifre.
	 * @param avoidAmbiguous indica se escludere i caratteri con aspetto simile (0, O, I, l, k, K)
	 *
	 */
	public static String generate(final int length, final boolean upperCase, final boolean lowerCase, final boolean digits, final boolean avoidAmbiguous) {

		int minLen = 0;
		if (upperCase) {
			minLen++;
		}
		if (lowerCase) {
			minLen++;
		}
		if (digits) {
			minLen++;
		}

		if (minLen > length) {
			throw new IllegalArgumentException("Minimum password length is " + minLen);
		}

		final char[] password = new char[length];
		final Random r = new Random();
		int index = 0;
		if (upperCase) {
			password[index++] = UPPER_CHARS[r.nextInt(UPPER_CHARS.length - (avoidAmbiguous ? AMBIGUOUS_UPPER_CHARS : 0))];
		}
		if (lowerCase) {
			password[index++] = LOWER_CHARS[r.nextInt(LOWER_CHARS.length - (avoidAmbiguous ? AMBIGUOUS_LOWER_CHARS : 0))];
		}
		if (digits) {
			password[index++] = DIGITS[r.nextInt(DIGITS.length - (avoidAmbiguous ? AMBIGUOUS_DIGITS : 0))];
		}
		for (int i = index; i < length; i++) {
			// imposto caratteri a caso
			password[i] = CHARS[r.nextInt(CHARS.length - (avoidAmbiguous ? AMBIGUOUS_CHARS : 0))];
		}
		// mescolo l'array per spostare i primi caratteri
		ArrayUtils.shuffle(password);

		return new String(password);
	}
}
