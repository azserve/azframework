package com.azserve.azframework.common.ex;

import static java.util.Objects.requireNonNull;

import java.util.function.BiFunction;

public interface CheckedBiFunction<T, U, R, E extends Exception> {

	R apply(T t, U u) throws E;

	/**
	 * Unwraps this checked function to an unchecked one.
	 */
	default BiFunction<T, U, R> unchecked() {
		return (t, u) -> {
			try {
				return this.apply(t, u);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return null;
			}
		};
	}

	/**
	 * Creates a {@link CheckedBiFunction} by using a method reference.
	 *
	 * @param function the function as method reference
	 * @return the function
	 */
	static <T, U, R, E extends Exception> CheckedBiFunction<T, U, R, E> of(final CheckedBiFunction<T, U, R, E> function) {
		return requireNonNull(function);
	}

	/**
	 * Wraps an unchecked function in an unchecked one.
	 *
	 * @param function the unchecked function
	 */
	static <T, U, R, E extends Exception> CheckedBiFunction<T, U, R, E> wrap(final BiFunction<T, U, R> function) {
		return requireNonNull(function)::apply;
	}

	/**
	 * Unwraps a checked function to have an unchecked one.
	 * The exception is then propagated to this method caller.
	 *
	 * @throws E the propagated exception
	 */
	static <T, U, R, E extends Exception> BiFunction<T, U, R> unwrap(final CheckedBiFunction<T, U, R, E> function) throws E {
		requireNonNull(function);
		return (t, u) -> {
			try {
				return function.apply(t, u);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return null;
			}
		};
	}

	/**
	 * Unwraps a checked function to have an unchecked one.
	 */
	static <T, U, R, E extends Exception> BiFunction<T, U, R> unchecked(final CheckedBiFunction<T, U, R, E> function) {
		return (t, u) -> {
			try {
				return function.apply(t, u);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return null;
			}
		};
	}
}
