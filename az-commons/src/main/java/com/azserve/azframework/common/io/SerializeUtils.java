package com.azserve.azframework.common.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Classe di utilità per la serializzazione e la deserializzazione di oggetti.
 */
public final class SerializeUtils {

	private SerializeUtils() {}

	/**
	 * Deserializza un array di byte.
	 *
	 * @param serializedObject array di byte.
	 * @return l'oggetto deserializzato.
	 * @throws ClassNotFoundException se l'oggetto letto appartiene ad una classe non esistente.
	 * @throws ClassCastException se l'oggetto deserializzato non è del tipo voluto.
	 */
	@SuppressWarnings("unchecked")
	@NonNull
	public static <T extends Serializable> T deserialize(final @NonNull byte[] serializedObject) throws IOException, ClassNotFoundException {
		try (final ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(serializedObject))) {
			return (T) ois.readObject();
		}
	}

	/**
	 * Serializza un oggetto e restituisce l'array di byte.
	 *
	 * @param object oggetto da serializzare.
	 * @return l'array di byte.
	 */
	@NonNull
	public static byte[] serialize(final @NonNull Serializable object) throws IOException {
		try (final ByteArrayOutputStream baos = new ByteArrayOutputStream(); final ObjectOutputStream oos = new ObjectOutputStream(baos)) {
			oos.writeObject(object);
			return baos.toByteArray();
		}
	}
}
