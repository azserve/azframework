package com.azserve.azframework.common.comparator;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

import com.azserve.azframework.common.annotation.NonNull;

public final class Comparators {

	@SuppressWarnings("unchecked")
	public static <T> Comparator<T> comparingBoolean(final @NonNull Predicate<T> keyExtractor) {
		Objects.requireNonNull(keyExtractor);
		return (Comparator<T> & Serializable) (c1, c2) -> Boolean.compare(keyExtractor.test(c1), keyExtractor.test(c2));
	}

	@SuppressWarnings("unchecked")
	public static <T> Comparator<T> comparingTrueFirst(final @NonNull Predicate<T> keyExtractor) {
		Objects.requireNonNull(keyExtractor);
		return (Comparator<T> & Serializable) (c1, c2) -> -Boolean.compare(keyExtractor.test(c1), keyExtractor.test(c2));
	}

	public static <T> Comparator<T> comparingIndexOf(final @NonNull String string, final @NonNull Function<T, String> stringExtractor) {
		final String ucaseString = string.toUpperCase();
		return (o1, o2) -> {
			final String descr1 = stringExtractor.apply(o1).toUpperCase();
			final String descr2 = stringExtractor.apply(o2).toUpperCase();
			final int ix1 = descr1.indexOf(ucaseString);
			final int ix2 = descr2.indexOf(ucaseString);
			int i = Integer.compare(ix1 < 0 ? Integer.MAX_VALUE : ix1, ix2 < 0 ? Integer.MAX_VALUE : ix2);
			if (i == 0) {
				i = descr1.compareTo(descr2);
			}
			return i;
		};
	}

	private Comparators() {}
}
