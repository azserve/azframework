package com.azserve.azframework.common.util;

/**
 * Classe per la rappresentazione di una proprietà di un bean.
 *
 * Viene considerata proprietà un campo del bean accompagnato dal suo
 * metodo getter e dal suo metodo setter.
 *
 * @author filippo
 *
 * @param <B> tipo del bean.
 * @param <T> tipo della proprietà.
 */
public class BeanProperty<A, P, B, T> {

	public static class RootBeanProperty<B, T> extends BeanProperty<B, Void, B, T> {

		public RootBeanProperty(final Class<B> beanType, final String name, final Class<T> type) {
			super(null, beanType, name, type);
		}
	}

	final BeanProperty<A, ?, P, B> parent;
	final Class<B> beanType;
	final String name;
	final Class<T> type;

	protected BeanProperty(final BeanProperty<A, ?, P, B> parent, final Class<B> beanType, final String name, final Class<T> type) {
		this.parent = parent;
		this.beanType = beanType;
		this.name = name;
		this.type = type;
	}

	public Class<B> getBeanType() {
		return this.beanType;
	}

	public String getName() {
		return this.name;
	}

	public Class<T> getType() {
		return this.type;
	}

	public BeanProperty<A, ?, P, B> getParent() {
		return this.parent;
	}

	/**
	 * Costruisce il riferimento ad una proprietà innestata.
	 *
	 * @param property proprietà innestata.
	 * @return il riferimento composto tra la proprietà principale
	 *         e quella innestata.
	 */
	public <X> BeanProperty<A, B, T, X> then(final BeanProperty<?, ?, T, X> property) {
		return new BeanProperty<>(this, property.beanType, property.name, property.type);
	}

	public String getNestedName() {
		if (this.parent == null) {
			return this.name;
		}
		return this.parent.getNestedName() + "." + this.name;
	}

	@Override
	public String toString() {
		return this.getNestedName();
	}
}
