package com.azserve.azframework.common.util;

import java.io.Serializable;

import com.azserve.azframework.common.annotation.NonNull;

/**
 * Classe wrapper per una terna di oggetti.
 *
 * La classe supporta riferimenti {@code null}.
 *
 * @param <T1> tipo del primo oggetto.
 * @param <T2> tipo del secondo oggetto.
 * @param <T3> tipo del terzo oggetto.
 */
public final class Triad<T1, T2, T3> implements Serializable {

	private static final long serialVersionUID = 417874994946658840L;

	public static @NonNull <V1, V2, V3> Triad<V1, V2, V3> of(final V1 val1, final V2 val2, final V3 val3) {
		return new Triad<>(val1, val2, val3);
	}

	private final T1 value1;
	private final T2 value2;
	private final T3 value3;

	private Triad(final T1 value1, final T2 value2, final T3 value3) {
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
	}

	public T1 getValue1() {
		return this.value1;
	}

	public T2 getValue2() {
		return this.value2;
	}

	public T3 getValue3() {
		return this.value3;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.value1 == null ? 0 : this.value1.hashCode());
		result = prime * result + (this.value2 == null ? 0 : this.value2.hashCode());
		result = prime * result + (this.value3 == null ? 0 : this.value3.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Triad<?, ?, ?> other = (Triad<?, ?, ?>) obj;
		if (this.value1 == null) {
			if (other.value1 != null) {
				return false;
			}
		} else if (!this.value1.equals(other.value1)) {
			return false;
		}
		if (this.value2 == null) {
			if (other.value2 != null) {
				return false;
			}
		} else if (!this.value2.equals(other.value2)) {
			return false;
		}
		if (this.value3 == null) {
			if (other.value3 != null) {
				return false;
			}
		} else if (!this.value3.equals(other.value3)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return this.value1 + ";" + this.value2 + ";" + this.value3;
	}
}
