package com.azserve.azframework.common.ex;

import static java.util.Objects.requireNonNull;

import java.util.function.BiPredicate;

@FunctionalInterface
public interface CheckedBiPredicate<T, U, E extends Exception> {

	boolean test(T t, U u) throws E;

	/**
	 * Unwraps this checked predicate to an unchecked one.
	 */
	default BiPredicate<T, U> unchecked() {
		return (t, u) -> {
			try {
				return this.test(t, u);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return false;
			}
		};
	}

	/**
	 * Creates a {@link CheckedBiPredicate} by using a method reference.
	 *
	 * @param predicate the predicate as method reference
	 * @return the predicate
	 */
	static <T, U, E extends Exception> CheckedBiPredicate<T, U, E> of(final CheckedBiPredicate<T, U, E> predicate) {
		return requireNonNull(predicate);
	}

	/**
	 * Wraps an unchecked predicate in an unchecked one.
	 *
	 * @param predicate the unchecked predicate
	 */
	static <T, U, E extends Exception> CheckedBiPredicate<T, U, E> wrap(final BiPredicate<T, U> predicate) {
		return requireNonNull(predicate)::test;
	}

	default CheckedBiPredicate<T, U, E> negate() {
		return (t, u) -> !this.test(t, u);
	}

	/**
	 * Unwraps a checked predicate to have an unchecked one.
	 * The exception is then propagated to this method caller.
	 *
	 * @throws E the propagated exception
	 */
	static <T, U, E extends Exception> BiPredicate<T, U> unwrap(final CheckedBiPredicate<T, U, E> predicate) throws E {
		requireNonNull(predicate);
		return (t, u) -> {
			try {
				return predicate.test(t, u);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return false;
			}
		};
	}

	/**
	 * Unwraps a checked predicate to have an unchecked one.
	 */
	static <T, U, E extends Exception> BiPredicate<T, U> unchecked(final CheckedBiPredicate<T, U, E> predicate) {
		return (t, u) -> {
			try {
				return predicate.test(t, u);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return false;
			}
		};
	}
}
