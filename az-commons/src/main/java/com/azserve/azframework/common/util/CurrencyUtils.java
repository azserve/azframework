package com.azserve.azframework.common.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Locale;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.lang.NumberUtils;

public class CurrencyUtils {

	public static final CurrencyUtils DEFAULT;

	static {
		final Locale defaultLocale = Locale.getDefault();
		final String defaultCountry = defaultLocale.getCountry();
		if (defaultCountry.length() != 2) {
			// test environment in gitlab haven't defaultCountry
			DEFAULT = new CurrencyUtils(Currency.getInstance(Locale.ITALY));
		} else {
			DEFAULT = new CurrencyUtils(Currency.getInstance(defaultLocale));
		}
	}

	private static final int EXTRA_PRECISION = 3;

	private final int defaultFractionDigits;
	private final int extraFractionDigits;

	public CurrencyUtils(final Currency currency) {
		this(currency, EXTRA_PRECISION);
	}

	public CurrencyUtils(final String currencyCode) {
		this(Currency.getInstance(currencyCode));
	}

	public CurrencyUtils(final Currency currency, final int extraPrecision) {
		this.defaultFractionDigits = currency.getDefaultFractionDigits();
		this.extraFractionDigits = this.defaultFractionDigits + extraPrecision;
	}

	public CurrencyUtils(final String currencyCode, final int extraPrecision) {
		this(Currency.getInstance(currencyCode), extraPrecision);
	}

	public BigDecimal round(final @NonNull BigDecimal amount) {
		return amount.setScale(this.defaultFractionDigits, RoundingMode.HALF_UP);
	}

	@NonNull
	public BigDecimal getTaxable(final @NonNull BigDecimal total, final @NonNull BigDecimal vatPerc) {
		return getTaxable(total, vatPerc, this.defaultFractionDigits);
	}

	@NonNull
	public BigDecimal getTaxableExtraPrecision(final @NonNull BigDecimal total, final @NonNull BigDecimal vatPerc) {
		return getTaxable(total, vatPerc, this.extraFractionDigits);
	}

	@NonNull
	public BigDecimal getVat(final @NonNull BigDecimal taxable, final @NonNull BigDecimal vatPerc) {
		return getVatFullPrecision(taxable, vatPerc).setScale(this.defaultFractionDigits, RoundingMode.HALF_UP);
	}

	@NonNull
	public BigDecimal getVatExtraPrecision(final @NonNull BigDecimal taxable, final @NonNull BigDecimal vatPerc) {
		return getVatFullPrecision(taxable, vatPerc).setScale(this.extraFractionDigits, RoundingMode.HALF_UP);
	}

	@NonNull
	public BigDecimal getTotal(final @NonNull BigDecimal taxable, final @NonNull BigDecimal vatPerc) {
		return getTotalFullPrecision(taxable, vatPerc).setScale(this.defaultFractionDigits, RoundingMode.HALF_UP);
	}

	@NonNull
	public BigDecimal getTotalExtraPrecision(final @NonNull BigDecimal taxable, final @NonNull BigDecimal vatPerc) {
		return getTotalFullPrecision(taxable, vatPerc).setScale(this.extraFractionDigits, RoundingMode.HALF_UP);
	}

	@NonNull
	public static BigDecimal getTaxable(final @NonNull BigDecimal total, final @NonNull BigDecimal vatPerc, final int fractionDigits) {
		return total.divide(BigDecimal.ONE.add(vatPerc.divide(NumberUtils.HUNDRED)), fractionDigits, RoundingMode.HALF_UP);
	}

	@NonNull
	public static BigDecimal getVatFullPrecision(final @NonNull BigDecimal taxable, final @NonNull BigDecimal vatPerc) {
		return taxable.multiply(vatPerc).divide(NumberUtils.HUNDRED);
	}

	@NonNull
	public static BigDecimal getTotalFullPrecision(final @NonNull BigDecimal taxable, final @NonNull BigDecimal vatPerc) {
		return taxable.multiply(BigDecimal.ONE.add(vatPerc.divide(NumberUtils.HUNDRED)));
	}

	/**
	 * Calcola il valore di uno sconto testuale, ad esempio "50+50" = 75 %
	 * Se il parametro in ingresso è null o vuoto restituisce ZERO, accetta spazi, virgole, punti
	 * Se il parametro non è valido restituisce null
	 *
	 * @param discount
	 * @return
	 */
	public static BigDecimal getDiscount(final @Nullable String discount) {
		if (discount == null || discount.trim().isEmpty()) {
			return BigDecimal.ZERO;
		}
		final String[] values = discount.replace(',', '.').split("\\+");
		BigDecimal total = BigDecimal.ONE;
		for (final String value : values) {
			try {
				final BigDecimal perc = new BigDecimal(value.trim());
				if (perc.compareTo(NumberUtils.HUNDRED) > 0 || perc.signum() < 0) {
					return null;
				}
				total = total.multiply(BigDecimal.ONE.subtract(perc.divide(NumberUtils.HUNDRED)));
			} catch (final @SuppressWarnings("unused") Exception e) {
				return null;
			}

		}
		return BigDecimal.ONE.subtract(total).multiply(NumberUtils.HUNDRED);
	}
}
