package com.azserve.azframework.common.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import com.azserve.azframework.common.annotation.NonNull;

public final class ReflectionUtils {

	private ReflectionUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	public static <T> T newObject(final Class<T> c) {
		try {
			final Constructor<T> constructor = c.getConstructor();
			return constructor.newInstance();
		} catch (final ReflectiveOperationException roex) {
			throw new RuntimeException(roex);
		}
	}

	public static Set<String> getDeclaredMethodSignatures(final Class<?> c) {
		try {
			final Set<String> result = new HashSet<>();
			for (final Method m : c.getDeclaredMethods()) {
				result.add(m.toGenericString());
			}
			return result;
		} catch (final SecurityException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static Set<String> getMethodSignatures(final Class<?> c) {
		try {
			final Set<String> result = new HashSet<>();
			for (final Method m : c.getMethods()) {
				result.add(m.toGenericString());
			}
			return result;
		} catch (final SecurityException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static Method findGetterMethod(final @NonNull Class<?> clazz, final @NonNull String propertyName) {
		return findMethod(clazz, propertyName, new String[] { "get", "is" });
	}

	public static Method findSetterMethod(final @NonNull Class<?> clazz, final @NonNull String propertyName, final @NonNull Class<?>... paramTypes) {
		return findMethod(clazz, propertyName, new String[] { "set" }, paramTypes);
	}

	/**
	 * Effettua la ricerca di un metodo di una classe
	 * a partire dal nome di una proprietà, specificando
	 * i prefissi possibili.
	 * <br/>
	 * Sono inclusi i metodi delle superclassi e
	 * delle interfacce che implementano.
	 *
	 * <pre>
	 * findMethod(String.class, "bytes", new String[] {"get"}, Charset.class);
	 * findMethod(String.class, "Bytes", new String[] {"get"}, Charset.class);
	 * <blockquote>entrambi restituiscono il metodo String.getBytes(Charset)</blockquote>
	 * </pre>
	 *
	 * @param clazz classe su cui cercare (obbligatoria).
	 * @param propertyName nome della proprietà (obbligatorio).
	 * @param methodPrefixes prefissi dei metodi.
	 * @param paramTypes tipi dei parametri del metodo.
	 */
	public static Method findMethod(final @NonNull Class<?> clazz, final @NonNull String propertyName, final @NonNull String[] methodPrefixes, final @NonNull Class<?>... paramTypes) {
		if (methodPrefixes.length == 0) {
			try {
				return clazz.getMethod(propertyName, paramTypes);
			} catch (final @SuppressWarnings("unused") NoSuchMethodException nsmex) {
				return null;
			}
		}
		// metto la prima lettera sempre in maiuscolo
		final String capPropertyName;
		if (Character.isLowerCase(propertyName.charAt(0))) {
			final char[] buf = propertyName.toCharArray();
			buf[0] = Character.toUpperCase(buf[0]);
			capPropertyName = new String(buf);
		} else {
			capPropertyName = propertyName;
		}
		for (final String methodPrefix : methodPrefixes) {
			try {
				return clazz.getMethod(methodPrefix + capPropertyName, paramTypes);
			} catch (final @SuppressWarnings("unused") NoSuchMethodException nsmex) {
				// ignoro l'errore e continuo a provare
			}
		}
		return null;
	}

	public static Field findField(final @NonNull Class<?> clazz, final @NonNull String name) {
		try {
			return clazz.getDeclaredField(name);
		} catch (final @SuppressWarnings("unused") NoSuchFieldException ex) {
			final Class<?> superclass = clazz.getSuperclass();
			return superclass == null ? null : findField(superclass, name);
		} catch (final @SuppressWarnings("unused") SecurityException ex) {
			return null;
		}
	}
}
