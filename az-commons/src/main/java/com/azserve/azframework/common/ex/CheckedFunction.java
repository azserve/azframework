package com.azserve.azframework.common.ex;

import static java.util.Objects.requireNonNull;

import java.util.function.BiFunction;
import java.util.function.Function;

public interface CheckedFunction<T, R, E extends Exception> {

	R apply(T t) throws E;

	/**
	 * Unwraps this checked function to an unchecked one.
	 */
	default Function<T, R> unchecked() {
		return t -> {
			try {
				return this.apply(t);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return null;
			}
		};
	}

	/**
	 * Provides a way to handle the exception and to return an alternative value.
	 *
	 * <pre>
	 * CheckedFunction.of(this::throwException).onError((input, ex) -> defaultValue);
	 * </pre>
	 *
	 * @param function the exception handler
	 */
	default Function<T, R> onError(final BiFunction<T, Exception, R> function) {
		return t -> {
			try {
				return this.apply(t);
			} catch (final Exception ex) {
				return function.apply(t, ex);
			}
		};
	}

	default <V> CheckedFunction<V, R, E> compose(final CheckedFunction<? super V, ? extends T, ? extends E> before) {
		requireNonNull(before);
		return t -> this.apply(before.apply(t));
	}

	default <V> CheckedFunction<T, V, E> andThen(final CheckedFunction<? super R, ? extends V, ? extends E> after) {
		requireNonNull(after);
		return t -> after.apply(this.apply(t));
	}

	/**
	 * Creates a {@link CheckedFunction} by using a method reference.
	 *
	 * @param function the function as method reference
	 * @return the function
	 */
	static <T, R, E extends Exception> CheckedFunction<T, R, E> of(final CheckedFunction<T, R, E> function) {
		return requireNonNull(function);
	}

	/**
	 * Wraps an unchecked function in an unchecked one.
	 *
	 * @param function the unchecked function
	 */
	static <T, R, E extends Exception> CheckedFunction<T, R, E> wrap(final Function<T, R> function) {
		return requireNonNull(function)::apply;
	}

	/**
	 * Unwraps a checked function to have an unchecked one.
	 * The exception is then propagated to this method caller.
	 *
	 * @throws E the propagated exception
	 */
	static <T, R, E extends Exception> Function<T, R> unwrap(final CheckedFunction<T, R, E> function) throws E {
		requireNonNull(function);
		return t -> {
			try {
				return function.apply(t);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return null;
			}
		};
	}

	/**
	 * Unwraps a checked function to have an unchecked one.
	 */
	static <T, R, E extends Exception> Function<T, R> unchecked(final CheckedFunction<T, R, E> function) {
		return t -> {
			try {
				return function.apply(t);
			} catch (final Exception ex) {
				Checked.sneakyThrow(ex);
				return null;
			}
		};
	}
}
