package com.azserve.azframework.poi;

import org.apache.poi.ss.usermodel.Sheet;

@FunctionalInterface
public interface GroupFooterBuilder<D, V> {

	GroupFooterBuilder<?, ?> DO_NOTHING = (w, d, a) -> {};

	@SuppressWarnings("unchecked")
	static <D, A> GroupFooterBuilder<D, A> getDoNothing() {
		return (GroupFooterBuilder<D, A>) DO_NOTHING;
	}

	void buildEdge(Sheet workbook, D dto, V accumulatorValue);
}