package com.azserve.azframework.poi;

import java.util.Date;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Workbook;

public class CellFiller<D, K> {

	public static <D> CellFiller<D, Date> forDate(final Function<D, Date> valueProvider) {
		return new DateCellFiller<>("dd/MM/yyyy", valueProvider);
	}

	public static CellFiller<Date, Date> forDate() {
		return forDate(e -> e);
	}

	public static <D> CellFiller<D, Boolean> forBoolean(final Function<D, Boolean> valueProvider) {
		return new BooleanCellFiller<>(valueProvider);
	}

	public static <D> CellFiller<D, Number> forInteger(final Function<D, Number> valueProvider) {
		return new NumericCellFiller<>("0", valueProvider);
	}

	public static CellFiller<Number, Number> forInteger() {
		return forInteger(e -> e);
	}

	public static <D> CellFiller<D, Number> forDecimal(final Function<D, Number> valueProvider) {
		return new NumericCellFiller<>("#0.00", valueProvider);
	}

	public static CellFiller<Number, Number> forDecimal() {
		return forDecimal(e -> e);
	}

	public static <D> CellFiller<D, String> forString(final Function<D, String> valueProvider) {
		return new CellFiller<>("", valueProvider);
	}

	public static CellFiller<String, String> forString() {
		return forString(e -> e);
	}

	public static <D> CellFiller<D, String> forFormula(final BiFunction<Cell, D, String> valueProvider) {
		return new FormulaCellFiller<>("", valueProvider);
	}

	//

	private CellStyle style;

	private final String pattern;
	private final BiFunction<Cell, D, K> valueProvider;

	public CellFiller(final String pattern, final Function<D, K> valueProvider) {
		this(pattern, (c, d) -> valueProvider.apply(d));
	}

	public CellFiller(final String pattern, final BiFunction<Cell, D, K> valueProvider) {
		this.pattern = pattern;
		this.valueProvider = valueProvider;
	}

	public void compileCell(final Workbook wb, final Cell cell, final D bean) {
		if (this.style == null) {
			this.style = wb.createCellStyle();
			this.style.setDataFormat(wb.createDataFormat().getFormat(this.pattern));
		}
		this.compileStyle(this.style);
		cell.setCellStyle(this.style);
		this.setCellValue(cell, this.valueProvider.apply(cell, bean));
	}

	public void compileCell(final Cell cell, final D bean) {
		this.compileCell(cell.getSheet().getWorkbook(), cell, bean);
	}

	/**
	 *
	 * @param cellStyle
	 */
	protected void compileStyle(final CellStyle cellStyle) {}

	protected void setCellValue(final Cell cell, final K value) {
		cell.setCellValue(Objects.toString(value, ""));
	}

	//

	public static class BooleanCellFiller<D> extends CellFiller<D, Boolean> {

		public BooleanCellFiller(final Function<D, Boolean> valueProvider) {
			super("", valueProvider);
		}

		@Override
		protected void compileStyle(final CellStyle style) {
			style.setAlignment(HorizontalAlignment.CENTER);
		}

		@Override
		protected void setCellValue(final Cell cell, final Boolean value) {
			cell.setCellValue(value != null && value.booleanValue() ? "X" : "");
		}
	}

	public static class DateCellFiller<D, K extends Date> extends CellFiller<D, K> {

		public DateCellFiller(final String pattern, final Function<D, K> valueProvider) {
			super(pattern, valueProvider);
		}

		@Override
		protected void compileStyle(final CellStyle style) {
			style.setAlignment(HorizontalAlignment.CENTER);
		}

		@Override
		protected void setCellValue(final Cell cell, final K value) {
			if (value == null) {
				cell.setCellValue("");
			} else {
				cell.setCellValue(value);
			}
		}
	}

	public static class NumericCellFiller<D, K extends Number> extends CellFiller<D, K> {

		public NumericCellFiller(final String pattern, final Function<D, K> valueProvider) {
			super(pattern, valueProvider);
		}

		@Override
		protected void compileStyle(final CellStyle style) {
			style.setAlignment(HorizontalAlignment.RIGHT);
		}

		@Override
		protected void setCellValue(final Cell cell, final K value) {
			if (value == null) {
				cell.setCellValue("");
			} else {
				cell.setCellValue(value.doubleValue());
			}
		}
	}

	public static class FormulaCellFiller<D> extends CellFiller<D, String> {

		public FormulaCellFiller(final String pattern, final BiFunction<Cell, D, String> valueProvider) {
			super(pattern, valueProvider);
		}

		@Override
		protected void setCellValue(final Cell cell, final String value) {
			cell.setCellFormula(value);
		}

	}
}