package com.azserve.azframework.poi;

import java.util.Objects;
import java.util.function.Function;

import org.apache.poi.ss.usermodel.Sheet;

public class Group<D, A> {

	private Object oldValue;
	private final Function<D, ?> evalFunction;

	private final GroupHeaderBuilder<D> headerBuilder;

	private final GroupFooterBuilder<D, A> footerBuilder;

	private final Accumulator<D, A> accumulator;

	public Group(final Function<D, ?> evalFunction, final Accumulator<D, A> accumulator, final GroupHeaderBuilder<D> headerBuilder, final GroupFooterBuilder<D, A> footerBuilder) {
		this.evalFunction = evalFunction;
		this.accumulator = accumulator;
		this.headerBuilder = headerBuilder;
		this.footerBuilder = footerBuilder;
	}

	public Group(final Function<D, ?> evalFunction, final GroupHeaderBuilder<D> headerBuilder) {
		this(evalFunction, Accumulator.getDummyAccumulator(), headerBuilder, GroupFooterBuilder.getDoNothing());
	}

	public Group(final Function<D, ?> evalFunction, final Accumulator<D, A> accumulator, final GroupFooterBuilder<D, A> footerBuilder) {
		this(evalFunction, accumulator, GroupHeaderBuilder.getDoNothing(), footerBuilder);
	}

	public Group(final Function<D, ?> evalFunction) {
		this(evalFunction, Accumulator.getDummyAccumulator(), GroupHeaderBuilder.getDoNothing(), GroupFooterBuilder.getDoNothing());
	}

	public boolean changed(final D dto) {

		final Object currentValue = this.evalFunction.apply(dto);
		final boolean b = Objects.equals(currentValue, this.oldValue);
		this.oldValue = currentValue;
		return !b;
	}

	public void accumulate(final D dto) {
		this.accumulator.accumulate(dto);
	}

	public void buildHeader(final Sheet sheet, final D dto) {
		this.headerBuilder.buildEdge(sheet, dto);
	}

	public void buildFooter(final Sheet sheet) {
		this.footerBuilder.buildEdge(sheet, this.accumulator.getLast(), this.accumulator.getValue());
	}
}
