package com.azserve.azframework.poi;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public abstract class SingleSheetExcelExporter<T> extends ExcelExporter {

	private final List<T> dataSource;
	private final String name;

	public SingleSheetExcelExporter(final List<T> dataSource, final String name) {
		this.dataSource = dataSource;
		this.name = name;
	}

	protected abstract String[] getHeader();

	protected abstract void fillSheet(final List<T> data, final Workbook workbook, final Sheet sheet);

	@Override
	protected void fillWorkbook(final Workbook workbook) {
		final Sheet sheet = workbook.createSheet(this.name);

		// creo lo stile delle celle della riga di testata
		final CellStyle headerCellStyle = workbook.createCellStyle();
		final Font headerCellFont = workbook.createFont();
		headerCellFont.setBold(true);
		headerCellStyle.setFont(headerCellFont);
		this.fillHeader(sheet, headerCellStyle);

		if (!this.dataSource.isEmpty()) {
			this.fillSheet(this.dataSource, workbook, sheet);
		}
	}

	/**
	 * @param sheet
	 * @param headerCellStyle
	 */
	protected void fillHeader(final Sheet sheet, final CellStyle headerCellStyle) {
		// creo e compilo la riga di testata
		final Row headerRow = appendRow(sheet);
		final String[] headerCaptions = this.getHeader();
		for (int i = 0; i < headerCaptions.length; i++) {
			final Cell cell = headerRow.createCell(i);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue(headerCaptions[i]);
		}
	}
}
