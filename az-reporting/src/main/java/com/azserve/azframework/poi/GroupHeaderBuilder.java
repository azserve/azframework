package com.azserve.azframework.poi;

import org.apache.poi.ss.usermodel.Sheet;

@FunctionalInterface
public interface GroupHeaderBuilder<D> {

	GroupHeaderBuilder<?> DO_NOTHING = (w, d) -> {};

	@SuppressWarnings("unchecked")
	static <T> GroupHeaderBuilder<T> getDoNothing() {
		return (GroupHeaderBuilder<T>) DO_NOTHING;
	}

	void buildEdge(Sheet workbook, D dto);
}
