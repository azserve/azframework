package com.azserve.azframework.poi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public abstract class ExcelExporter {

	public enum ExcelFormat {

		XLS(".xls") {

			@Override
			protected Workbook newWorkBook() {
				return new HSSFWorkbook();
			}
		},
		XLSX(".xlsx") { // NO_UCD (unused code)

			@Override
			protected Workbook newWorkBook() {
				return new XSSFWorkbook();
			}
		};

		private final String dotExtension;

		ExcelFormat(final String dotExtension) {
			this.dotExtension = dotExtension;
		}

		protected abstract Workbook newWorkBook();

		public String getDotExtension() {
			return this.dotExtension;
		}
	}

	protected abstract void fillWorkbook(Workbook wbook);

	protected void buildHeaderCells(final Workbook workbook, final Row row, final String... captions) {
		// creo lo stile delle celle della riga di testata
		final CellStyle headerCellStyle = workbook.createCellStyle();
		final Font headerCellFont = workbook.createFont();
		headerCellFont.setBold(true);
		headerCellStyle.setFont(headerCellFont);

		for (int i = 0; i < captions.length; i++) {
			final Cell cell = row.createCell(i);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue(captions[i]);
		}
	}

	protected <D> void buildDataCells(final Row row, final D data, final List<CellFiller<D, ?>> fillers) {
		for (int i = 0; i < fillers.size(); i++) {
			fillers.get(i).compileCell(row.createCell(i), data);
		}
	}

	public void export(final OutputStream os, final ExcelFormat format) throws IOException {
		try (final Workbook workbook = format.newWorkBook()) {
			this.fillWorkbook(workbook);
			workbook.write(os);
		}
	}

	public InputStream export(final ExcelFormat format) throws IOException {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			this.export(baos, format);
			return new ByteArrayInputStream(baos.toByteArray());
		}
	}

	protected static Row appendRow(final Sheet sheet) {
		final int lastRowNum = sheet.getLastRowNum();
		return sheet.createRow(lastRowNum == 0 ? sheet.getPhysicalNumberOfRows() : lastRowNum + 1);
	}
}
