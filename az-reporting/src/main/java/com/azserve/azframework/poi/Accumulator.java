package com.azserve.azframework.poi;

public abstract class Accumulator<D, A> {

	@SuppressWarnings("rawtypes")
	private static Accumulator DUMMY = new Accumulator() {

		@Override
		protected Object getInitialValue() {
			return null;
		}

		@Override
		protected Object accumulate1(final Object currentValue, final Object dto) {
			return currentValue;
		}
	};

	@SuppressWarnings("unchecked")
	public static <D, A> Accumulator<D, A> getDummyAccumulator() {
		return DUMMY;
	}

	private A acc;
	private D last;

	public Accumulator() {
		this.acc = this.getInitialValue();
	}

	protected abstract A getInitialValue();

	protected final void accumulate(final D dto) {
		this.last = dto;
		this.acc = this.accumulate1(this.acc, dto);
	}

	protected abstract A accumulate1(A currentValue, D dto);

	A getValue() {
		final A acc2 = this.acc;
		this.acc = this.getInitialValue();
		return acc2;
	}

	public D getLast() {
		return this.last;
	}
}
