package com.azserve.azframework.poi;

import java.util.Collections;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public abstract class FunctionalExcelExporter<T> extends SingleSheetExcelExporter<T> {

	public FunctionalExcelExporter(final List<T> dataList, final String name) {
		super(dataList, name);
	}

	protected abstract List<CellFiller<T, ?>> getCellFillers();

	/**
	 * Restituisce i gruppi per il report, 
	 * di default non ci sono gruppi e restituisce una lista vuota non modificabile.
	 * Per aggiungere gruppi sovrascrivere il metodo senza utilizzare super
	 * @return 
	 */
	protected List<Group<T, ?>> getGroups() {
		return Collections.emptyList();
	}

	protected int compileRow(final Workbook workbook, final Row row, final List<CellFiller<T, ?>> cellFillers, final T bean) {
		int col = 0;
		for (final CellFiller<T, ?> cellFiller : cellFillers) {
			final Cell cell = row.createCell(col);
			cellFiller.compileCell(workbook, cell, bean);
			col++;
		}
		return col;
	}

	@Override
	protected void fillSheet(final List<T> dataSource, final Workbook workbook, final Sheet sheet) {
		int usedColumns = 0;
		final List<CellFiller<T, ?>> cellFillers = this.getCellFillers();

		@SuppressWarnings("unchecked")
		final Group<T, ?>[] groups = this.getGroups().toArray(new Group[0]);

		// final List<Group<T, ?>> groups = this.getGroups();
		final boolean[] groupsOpen = new boolean[groups.length];
		// creo e compilo le righe del corpo

		for (final T data : dataSource) {
			int lastGroupIndex = groups.length;
			for (int i = groups.length - 1; i >= 0; i--) {
				if (groups[i].changed(data)) {
					lastGroupIndex = i;
				}
			}
			for (int i = groups.length - 1; i >= lastGroupIndex; i--) {
				final Group<T, ?> group = groups[i];
				if (groupsOpen[i]) {
					group.buildFooter(sheet);
					groupsOpen[i] = false;
				}
			}

			for (int i = 0; i < groups.length; i++) {
				groups[i].accumulate(data);
			}

			for (int i = lastGroupIndex; i < groups.length; i++) {
				groupsOpen[i] = true;
				final Group<T, ?> group = groups[i];
				group.buildHeader(sheet, data);
			}

			final Row row = sheet.createRow(sheet.getLastRowNum() + 1);
			final int usedCols = this.compileRow(workbook, row, cellFillers, data);
			usedColumns = Math.max(usedColumns, usedCols);

		}
		for (int i = groups.length - 1; i >= 0; i--) {
			if (groupsOpen[i]) {
				groups[i].buildFooter(sheet);
				groupsOpen[i] = false;
			}
		}

		for (int i = 0; i < usedColumns; i++) {
			sheet.autoSizeColumn(i);
		}
	}
}
