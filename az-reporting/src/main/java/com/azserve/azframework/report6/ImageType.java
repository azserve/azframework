package com.azserve.azframework.report6;

import java.net.URL;

import com.azserve.azframework.common.annotation.NonNull;

public interface ImageType {

	@NonNull
	String getImageName();

	@NonNull
	String getImagePath();

	default URL getURL() {
		return getClass().getResource(this.getImagePath() + this.getImageName() + ".png");
	}
}