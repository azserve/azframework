package com.azserve.azframework.report6;

public class ReportBean<T> {

	private final T field;

	public ReportBean(final T field) {
		this.field = field;
	}

	public T getField() {
		return this.field;
	}
}
