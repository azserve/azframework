package com.azserve.azframework.report6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public final class ReportUtils {

	/**
	 * Restituisce un data source jasper contenente una lista
	 * di oggetti {@linkplain ReportBean} che wrappano
	 * gli oggetti della collezione data.
	 *
	 * @param collection collezione di dati
	 */
	public static <T> JRBeanCollectionDataSource createReportBeanDataSource(final Collection<T> collection) {
		return new JRBeanCollectionDataSource(collection.stream().map(ReportBean<T>::new).collect(Collectors.toCollection(ArrayList::new)));
	}

	private ReportUtils() {}
}
