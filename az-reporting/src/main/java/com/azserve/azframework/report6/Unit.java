package com.azserve.azframework.report6;

import com.azserve.azframework.common.annotation.NonNull;

public enum Unit {

	METER(1.),
	CENTIMETER(0.01),
	INCH(0.0254),
	POINT(0.0254 / 72.);

	private final double thisOverMeter;

	Unit(final double thisOverMeter) {
		this.thisOverMeter = thisOverMeter;
	}

	public double toUnit(final @NonNull Unit otherUnit, final double value) {
		return value * this.thisOverMeter / otherUnit.thisOverMeter;
	}

	public static double convert(final @NonNull Unit sourceUnit, final @NonNull Unit resultUnit, final double value) {
		return sourceUnit.toUnit(resultUnit, value);
	}
}
