package com.azserve.azframework.report6;

import java.io.InputStream;
import java.net.URL;

import com.azserve.azframework.common.annotation.NonNull;

public interface ReportType {

	@NonNull
	String getReportName();

	@NonNull
	String getReportPath();

	boolean isPDFA();

	default URL getURL() {
		return getClass().getResource(this.getReportPath() + this.getReportName() + ".jasper");
	}

	default InputStream getInputStream() {
		return getClass().getResourceAsStream(this.getReportPath() + this.getReportName() + ".jasper");
	}
}
