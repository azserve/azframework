package com.azserve.azframework.report6;

import java.awt.color.ColorSpace;
import java.awt.color.ICC_Profile;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.PrinterName;

import com.azserve.azframework.common.annotation.NonNull;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.type.OrientationEnum;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleExporterInputItem;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;
import net.sf.jasperreports.export.SimplePrintServiceReportConfiguration;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import net.sf.jasperreports.export.type.PdfaConformanceEnum;

public abstract class AbstractReport<T> implements Serializable {

	private static final long serialVersionUID = -3385687220121024537L;

	private static String iccProfilePath = null;

	private static synchronized String createIccProfilePath() throws IOException {
		final ICC_Profile iccProfile = ICC_Profile.getInstance(ColorSpace.CS_sRGB);
		final Path iccTempFile = Files.createTempFile("icc", null);
		try (OutputStream os1 = Files.newOutputStream(iccTempFile)) {
			iccProfile.write(os1);
		}
		return iccTempFile.toString();
	}

	private static String getIccProfilePath() throws IOException {
		return iccProfilePath == null ? (iccProfilePath = createIccProfilePath()) : iccProfilePath;
	}

	protected static <B> JRDataSource createDataSource(final Collection<B> c) {
		return c == null ? null : new JRBeanCollectionDataSource(c.stream().map(ReportBean<B>::new).collect(Collectors.toList()));
	}

	private final List<ReportBean<T>> details;
	private final String author;
	private final String creator;
	private final Map<String, Object> parameters = new HashMap<>();

	private JasperPrint lastPrint;

	public AbstractReport(final @NonNull List<T> details) {
		this(details, null, null);
	}

	public AbstractReport(final @NonNull List<T> details, final String author, final String creator) {
		this.details = Objects.requireNonNull(details, "Details are required").stream()
				.map(ReportBean<T>::new).collect(Collectors.toList());
		this.author = author;
		this.creator = creator;
		this.parameters.put("TOTAL_RECORD_COUNT", Integer.valueOf(details.size()));
	}

	public int getPageCount() {
		if (this.lastPrint == null) {
			throw new IllegalStateException("Report has not been filled yet");
		}
		return this.lastPrint.getPages().size();
	}

	//

	public void print(final String printerName, final Integer copies, final Double offX, final Double offY) throws Exception {
		this.print(printerName, copies, offX, offY, null);
	}

	public void print(final String printerName, final Integer copies, final Double offX, final Double offY, final OrientationEnum orientation) throws Exception {
		this.lastPrint = this.createJasperPrint();
		if (orientation != null) {
			this.lastPrint.setOrientation(orientation);
		}

		final JRPrintServiceExporter exporter = new JRPrintServiceExporter();

		final SimplePrintServiceReportConfiguration reportConfig = new SimplePrintServiceReportConfiguration();
		if (offX != null) {
			reportConfig.setOffsetX(Integer.valueOf((int) Unit.convert(Unit.CENTIMETER, Unit.POINT, offX.doubleValue())));
		}
		if (offY != null) {
			reportConfig.setOffsetY(Integer.valueOf((int) Unit.convert(Unit.CENTIMETER, Unit.POINT, offY.doubleValue())));
		}
		exporter.setConfiguration(reportConfig);

		exporter.setExporterInput(new SimpleExporterInput(this.lastPrint));

		final PrintServiceAttributeSet serviceAttributeSet = new HashPrintServiceAttributeSet();
		if (printerName != null) {
			serviceAttributeSet.add(new PrinterName(printerName, null));
			// verifico che esista una stampante con quel nome...
			final PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, serviceAttributeSet);
			if (printServices != null && printServices.length > 0) {

				final SimplePrintServiceExporterConfiguration exportConfig = new SimplePrintServiceExporterConfiguration();

				// ok, esiste una stampante con quel nome!
				exportConfig.setPrintServiceAttributeSet(serviceAttributeSet);
				exporter.setConfiguration(exportConfig);
			}
		} else {
			final PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

			final SimplePrintServiceExporterConfiguration exportConfig = new SimplePrintServiceExporterConfiguration();
			exportConfig.setPrintService(defaultPrintService);
			exporter.setConfiguration(exportConfig);
		}

		final int numCopies = copies == null ? 1 : copies.intValue();
		for (int i = 0; i < numCopies; i++) {
			exporter.exportReport();
		}
	}

	//

	public byte[] createPdf() throws Exception {
		return this.createPdf(true, null, null, null);
	}

	public byte[] createPdf(final Integer copies, final Double offX, final Double offY) throws Exception {
		return this.createPdf(true, copies, offX, offY);
	}

	public byte[] createPdf(final boolean refill, final Integer copies, final Double offX, final Double offY) throws Exception {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			this.writePdf(baos, refill, copies, offX, offY);
			return baos.toByteArray();
		}
	}

	public void writePdf(final OutputStream os) throws Exception {
		this.writePdf(os, true, null, null, null);
	}

	public void writePdf(final OutputStream os, final Integer copies, final Double offX, final Double offY) throws Exception {
		this.writePdf(os, true, copies, offX, offY);
	}

	public void writePdf(final OutputStream os, final boolean refill, final Integer copies, final Double offX, final Double offY) throws Exception {
		this.writePdf(os, refill, copies, offX, offY, null);
	}

	public void writePdf(final OutputStream os, final boolean refill, final Integer copies, final Double offX, final Double offY, final OrientationEnum orientation) throws Exception {
		if (refill || this.lastPrint == null) {
			this.lastPrint = this.createJasperPrint();
		}

		if (orientation != null) {
			this.lastPrint.setOrientation(orientation);
		}

		final JRPdfExporter exporter = new JRPdfExporter();

		final SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();

		final SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
		exportConfig.setDisplayMetadataTitle(Boolean.TRUE);
		exporter.setConfiguration(exportConfig);

		if (this.creator != null) {
			exportConfig.setMetadataCreator(this.creator);
		}

		if (this.author != null) {
			exportConfig.setMetadataAuthor(this.author);
		}

		final String title = this.getReportTitle();
		if (title != null) {
			exportConfig.setMetadataTitle(title);
		}

		if (this.getReportType().isPDFA()) {
			exportConfig.setIccProfilePath(getIccProfilePath());
			exportConfig.setPdfaConformance(PdfaConformanceEnum.PDFA_1B);
		}

		if (offX != null) {
			reportConfig.setOffsetX(Integer.valueOf((int) Unit.convert(Unit.CENTIMETER, Unit.POINT, offX.doubleValue())));
		}
		if (offY != null) {
			reportConfig.setOffsetY(Integer.valueOf((int) Unit.convert(Unit.CENTIMETER, Unit.POINT, offY.doubleValue())));
		}
		exporter.setConfiguration(reportConfig);

		final SimpleExporterInput input = copies == null
				? new SimpleExporterInput(this.lastPrint)
				: new SimpleExporterInput(IntStream.range(0, Math.max(1, copies.intValue()))
						.mapToObj(i -> this.lastPrint)
						.map(SimpleExporterInputItem::new)
						.collect(Collectors.toList()));

		exporter.setExporterInput(input);
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(os));

		exporter.exportReport();
	}

	//

	public byte[] createXls() throws Exception {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			this.writeXls(baos);
			return baos.toByteArray();
		}
	}

	public void writeXls(final OutputStream os) throws Exception {
		this.lastPrint = this.createJasperPrint();

		final JRXlsExporter exporter = new JRXlsExporter();

		final SimpleXlsReportConfiguration reportConfig = new SimpleXlsReportConfiguration();

		reportConfig.setOnePagePerSheet(Boolean.FALSE);
		reportConfig.setDetectCellType(Boolean.TRUE);
		reportConfig.setWhitePageBackground(Boolean.FALSE);
		reportConfig.setRemoveEmptySpaceBetweenRows(Boolean.TRUE);
		// reportConfig.setIgnoreGraphics(Boolean.TRUE);
		// reportConfig.setRemoveEmptySpaceBetweenColumns(Boolean.TRUE);
		// reportConfig.setIgnoreCellBorder(Boolean.TRUE);
		// reportConfig.setIgnoreGraphics(Boolean.TRUE);

		exporter.setConfiguration(reportConfig);

		exporter.setExporterInput(new SimpleExporterInput(this.lastPrint));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(os));

		exporter.exportReport();
	}

	//

	@NonNull
	public final Map<String, Object> getParameters() {
		return this.parameters;
	}

	public final boolean isEmpty() {
		return this.details.isEmpty();
	}

	@NonNull
	protected abstract ReportType getReportType();

	protected String getReportTitle() {
		return null;
	}

	//

	/**
	 * Crea il {@linkplain JRDataSource} per il report.
	 *
	 * @return il datasource usato dal report.
	 */
	@NonNull
	protected final JRDataSource createDataSource() {
		return new JRBeanCollectionDataSource(this.details);
	}

	protected final List<ReportBean<T>> getDetails() {
		return this.details;
	}

	/**
	 * Aggiunge un parametro da passare al report.
	 *
	 * @param name nome del parametro.
	 * @param obj valore del parametro.
	 */
	protected final void addParameter(final String name, final Object obj) {
		this.parameters.put(name, obj);
	}

	/**
	 * Rimuove il parametro specificato.
	 *
	 * @param name nome del parametro.
	 */
	protected final void removeParameter(final String name) {
		this.parameters.remove(name);
	}

	//

	protected JasperPrint createJasperPrint() throws JRException, IOException {
		final ReportType type = this.getReportType();
		try (final InputStream is = this.findJasper(type)) {
			final JasperPrint jasperPrint = JasperFillManager.fillReport(is, this.parameters, this.createDataSource());
			jasperPrint.setName(type.getReportName() + UUID.randomUUID());
			return jasperPrint;
		}
	}

	private InputStream findJasper(final @NonNull ReportType report) {
		return report.getInputStream();
	}
}