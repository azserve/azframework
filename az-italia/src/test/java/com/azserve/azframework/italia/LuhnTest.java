package com.azserve.azframework.italia;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.azserve.azframework.common.test.TestUtils;
import com.azserve.azframework.italia.Luhn;

public class LuhnTest {

	private final String[] inputs = { "", "0", "0000000", "1", "9", "01", "09", "0311384027" };
	private final char[] checkDigits = { '0', '0', '0', '9', '1', '8', '1', '0' };

	@Test
	public void constructor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.testUtilsClassDefinition(Luhn.class);
	}

	@Test
	public void getString() {
		for (int i = 0; i < this.inputs.length; i++) {
			assertEquals(Luhn.getCheckDigit(this.inputs[i]), this.checkDigits[i]);
		}
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Luhn.getCheckDigit("0a"));
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Luhn.getCheckDigit(" "));
	}

	@Test
	public void testString() {
		for (int i = 0; i < this.inputs.length; i++) {
			assertTrue(Luhn.check(this.inputs[i] + this.checkDigits[i]));
		}
		for (int i = 0; i < this.inputs.length; i++) {
			assertFalse(Luhn.check(this.inputs[i] + (this.checkDigits[i] + 1) % 10));
		}
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Luhn.check("0a"));
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Luhn.check(" "));
	}

	@Test
	public void getInts() {
		for (int i = 0; i < this.inputs.length; i++) {
			assertEquals(Luhn.getCheckDigit(toIntArray(this.inputs[i])), this.checkDigits[i] - '0');
		}
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Luhn.getCheckDigit(0, 11));
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Luhn.getCheckDigit(-5));
	}

	@Test
	public void testInts() {
		for (int i = 0; i < this.inputs.length; i++) {
			assertTrue(Luhn.check(toIntArray(this.inputs[i] + this.checkDigits[i])));
		}
		for (int i = 0; i < this.inputs.length; i++) {
			assertFalse(Luhn.check(toIntArray(this.inputs[i] + (this.checkDigits[i] + 1) % 10)));
		}
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Luhn.check(0, 11));
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Luhn.check(-5));
	}

	private static int[] toIntArray(final String s) {
		final char[] charArray = s.toCharArray();
		final int[] result = new int[charArray.length];
		for (int i = 0; i < charArray.length; i++) {
			result[i] = charArray[i] - '0';
		}
		return result;
	}
}
