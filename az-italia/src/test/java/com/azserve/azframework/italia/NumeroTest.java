package com.azserve.azframework.italia;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

import com.azserve.azframework.common.test.TestUtils;

public class NumeroTest {

	@SuppressWarnings("static-method")
	@Test
	public void constructor() throws Throwable {
		TestUtils.testUtilsClassDefinition(Numero.class);
	}

	@Test
	public void int0_99() {
		// 1-19
		this.test(new BigDecimal("0"), "zero");
		this.test(new BigDecimal("1"), "uno");
		this.test(new BigDecimal("2"), "due");
		this.test(new BigDecimal("3"), "tre");
		this.test(new BigDecimal("4"), "quattro");
		this.test(new BigDecimal("5"), "cinque");
		this.test(new BigDecimal("6"), "sei");
		this.test(new BigDecimal("7"), "sette");
		this.test(new BigDecimal("8"), "otto");
		this.test(new BigDecimal("9"), "nove");
		this.test(new BigDecimal("10"), "dieci");
		this.test(new BigDecimal("11"), "undici");
		this.test(new BigDecimal("12"), "dodici");
		this.test(new BigDecimal("13"), "tredici");
		this.test(new BigDecimal("14"), "quattordici");
		this.test(new BigDecimal("15"), "quindici");
		this.test(new BigDecimal("16"), "sedici");
		this.test(new BigDecimal("17"), "diciassette");
		this.test(new BigDecimal("18"), "diciotto");
		this.test(new BigDecimal("19"), "diciannove");
		// decine
		this.test(new BigDecimal("20"), "venti");
		this.test(new BigDecimal("30"), "trenta");
		this.test(new BigDecimal("40"), "quaranta");
		this.test(new BigDecimal("50"), "cinquanta");
		this.test(new BigDecimal("60"), "sessanta");
		this.test(new BigDecimal("70"), "settanta");
		this.test(new BigDecimal("80"), "ottanta");
		this.test(new BigDecimal("90"), "novanta");
		// elisione dell'ultima lettera delle decine per uno e otto
		this.test(new BigDecimal("21"), "ventuno");
		this.test(new BigDecimal("31"), "trentuno");
		this.test(new BigDecimal("41"), "quarantuno");
		this.test(new BigDecimal("51"), "cinquantuno");
		this.test(new BigDecimal("61"), "sessantuno");
		this.test(new BigDecimal("71"), "settantuno");
		this.test(new BigDecimal("81"), "ottantuno");
		this.test(new BigDecimal("91"), "novantuno");
		this.test(new BigDecimal("28"), "ventotto");
		this.test(new BigDecimal("38"), "trentotto");
		this.test(new BigDecimal("48"), "quarantotto");
		this.test(new BigDecimal("58"), "cinquantotto");
		this.test(new BigDecimal("68"), "sessantotto");
		this.test(new BigDecimal("78"), "settantotto");
		this.test(new BigDecimal("88"), "ottantotto");
		this.test(new BigDecimal("98"), "novantotto");
		// ciclo tutte le decine con un numero senza elisione
		this.test(new BigDecimal("22"), "ventidue");
		this.test(new BigDecimal("32"), "trentadue");
		this.test(new BigDecimal("42"), "quarantadue");
		this.test(new BigDecimal("52"), "cinquantadue");
		this.test(new BigDecimal("62"), "sessantadue");
		this.test(new BigDecimal("72"), "settantadue");
		this.test(new BigDecimal("82"), "ottantadue");
		this.test(new BigDecimal("92"), "novantadue");
		// ciclo tutte le unità
		this.test(new BigDecimal("83"), "ottantatre");
		this.test(new BigDecimal("84"), "ottantaquattro");
		this.test(new BigDecimal("85"), "ottantacinque");
		this.test(new BigDecimal("86"), "ottantasei");
		this.test(new BigDecimal("87"), "ottantasette");
		this.test(new BigDecimal("89"), "ottantanove");
		// ultimo
		this.test(new BigDecimal("99"), "novantanove");
	}

	@Test
	public void int100_999() {
		this.test(new BigDecimal("100"), "cento");
		this.test(new BigDecimal("200"), "duecento");
		this.test(new BigDecimal("300"), "trecento");
		this.test(new BigDecimal("400"), "quattrocento");
		this.test(new BigDecimal("500"), "cinquecento");
		this.test(new BigDecimal("600"), "seicento");
		this.test(new BigDecimal("700"), "settecento");
		this.test(new BigDecimal("800"), "ottocento");
		this.test(new BigDecimal("900"), "novecento");

		this.test(new BigDecimal("128"), "centoventotto");
		this.test(new BigDecimal("256"), "duecentocinquantasei");
		this.test(new BigDecimal("512"), "cinquecentododici");
		this.test(new BigDecimal("999"), "novecentonovantanove");
	}

	@Test
	public void int1000_999999() {
		this.test(new BigDecimal("1000"), "mille");
		this.test(new BigDecimal("2000"), "duemila");
		this.test(new BigDecimal("3000"), "tremila");
		this.test(new BigDecimal("4000"), "quattromila");
		this.test(new BigDecimal("5000"), "cinquemila");
		this.test(new BigDecimal("6000"), "seimila");
		this.test(new BigDecimal("7000"), "settemila");
		this.test(new BigDecimal("8000"), "ottomila");
		this.test(new BigDecimal("9000"), "novemila");

		this.test(new BigDecimal("1024"), "milleventiquattro");
		this.test(new BigDecimal("2048"), "duemilaquarantotto");
		this.test(new BigDecimal("4096"), "quattromilanovantasei");
		this.test(new BigDecimal("8192"), "ottomilacentonovantadue");
		this.test(new BigDecimal("16384"), "sedicimilatrecentoottantaquattro");
		this.test(new BigDecimal("32768"), "trentaduemilasettecentosessantotto");
		this.test(new BigDecimal("65536"), "sessantacinquemilacinquecentotrentasei");
		this.test(new BigDecimal("131072"), "centotrentunomilasettantadue");
		this.test(new BigDecimal("262144"), "duecentosessantaduemilacentoquarantaquattro");
		this.test(new BigDecimal("524288"), "cinquecentoventiquattromiladuecentoottantotto");
		this.test(new BigDecimal("999999"), "novecentonovantanovemilanovecentonovantanove");
	}

	@Test
	public void intHuge() {
		this.test(new BigDecimal("1000000"), "unmilione");
		this.test(new BigDecimal("2000000"), "duemilioni");
		this.test(new BigDecimal("1000000000"), "unmiliardo");
		this.test(new BigDecimal("2000000000"), "duemiliardi");
		this.test(new BigDecimal(Integer.MAX_VALUE), "duemiliardicentoquarantasettemilioniquattrocentoottantatremilaseicentoquarantasette");
		assertEquals(Numero.inLettere(Numero.MAX_VALUE_LONG), "novecentonovantanovemiliardinovecentonovantanovemilioninovecentonovantanovemilanovecentonovantanove");
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Numero.inLettere(1000000000000L));
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Numero.inLettere(new BigDecimal("1000000000000"), 2));
	}

	@Test
	public void intNegative() {
		this.test(new BigDecimal("-10"), "-dieci");
		this.test(new BigDecimal(Integer.MIN_VALUE), "-duemiliardicentoquarantasettemilioniquattrocentoottantatremilaseicentoquarantotto");
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Numero.inLettere(-1000000000000L));
	}

	@SuppressWarnings("static-method")
	@Test
	public void decimals() {
		assertEquals(Numero.inLettere(new BigDecimal("-10"), 2), "-dieci/00");
		assertEquals(Numero.inLettere(new BigDecimal("-10.502"), 2), "-dieci/50");
		assertEquals(Numero.inLettere(new BigDecimal("10.502"), 2), "dieci/50");
		assertEquals(Numero.inLettere(new BigDecimal("10.502"), 4, " virgola "), "dieci virgola 5020");
		assertEquals(Numero.inLettere(null, 2), "");
		assertEquals(Numero.inLettere(Numero.MAX_VALUE, 3), "novecentonovantanovemiliardinovecentonovantanovemilioninovecentonovantanovemilanovecentonovantanove/000");
		TestUtils.assertThrows(IllegalArgumentException.class, () -> Numero.inLettere(new BigDecimal("-1000000000000"), 2));
	}

	@SuppressWarnings("static-method")
	private void test(final BigDecimal value, final String expectd) {
		assertEquals(Numero.inLettere(value, 0), expectd);
		assertEquals(Numero.inLettere(value.longValue()), expectd);
		assertEquals(Numero.inLettere(value.intValue()), expectd);
	}
}
