package com.azserve.azframework.italia;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.azserve.azframework.common.test.TestUtils;

public class PartitaIvaTest {

	@Test
	public void constructor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException {
		TestUtils.testUtilsClassDefinition(PartitaIva.class);
	}

	@Test
	public void isValida() {
		assertFalse(PartitaIva.isValida("0000000"));
		assertFalse(PartitaIva.isValida("0311384027a"));
		assertFalse(PartitaIva.isValida("0311384027 "));
		assertTrue(PartitaIva.isValida("03113840270"));
		assertFalse(PartitaIva.isValida("03113840271"));
	}
}
