package com.azserve.azframework.italia;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;

import com.azserve.azframework.italia.CodiceFiscale.Errore;
import com.azserve.azframework.italia.CodiceFiscale.Sesso;

public class CodiceFiscaleTest {

	private final String COGNOME_1 = "Rossi";
	private final String NOME_1 = "Mario";
	private final String CF_1 = "RSSMRA00A01A001J";
	private final String CODICE_CATASTALE_1 = "A001";
	private final LocalDate DATA_NASCITA_1 = LocalDate.of(2000, 1, 1);
	private final Sesso SESSO_1 = Sesso.M;

	private final String COGNOME_2 = "Bianchi";
	private final String NOME_2 = "Maria Genoveffa";
	private final String CF_2 = "BNCMGN99T71Z130I";
	private final String CODICE_CATASTALE_2 = "Z130";
	private final LocalDate DATA_NASCITA_2 = LocalDate.of(1999, 12, 31);
	private final Sesso SESSO_2 = Sesso.F;

	private final String COGNOME_3 = "AB";
	private final String NOME_3 = "ZU";
	private final String CF_3 = "BAXZUX99T31Z130J";
	private final String CODICE_CATASTALE_3 = "Z130";
	private final LocalDate DATA_NASCITA_3 = LocalDate.of(1999, 12, 31);
	private final Sesso SESSO_3 = Sesso.M;

	@Test
	public void fromData() {
		final CodiceFiscale cf1 = CodiceFiscale.of(this.COGNOME_1, this.NOME_1, this.DATA_NASCITA_1, this.SESSO_1, this.CODICE_CATASTALE_1);
		assertEquals(cf1.getCodiceCatastale(), this.CODICE_CATASTALE_1);
		assertEquals(cf1.getDataNascita(), this.DATA_NASCITA_1);
		assertEquals(cf1.getSesso(), this.SESSO_1);
		assertEquals(cf1.getCodiceFiscale(), this.CF_1);
		assertFalse(cf1.isOmocodia());
		assertTrue(cf1.getOmocodie().isEmpty());

		final CodiceFiscale cf2 = CodiceFiscale.of(this.COGNOME_2, this.NOME_2, this.DATA_NASCITA_2, this.SESSO_2, this.CODICE_CATASTALE_2);
		assertEquals(cf2.getCodiceCatastale(), this.CODICE_CATASTALE_2);
		assertEquals(cf2.getDataNascita(), this.DATA_NASCITA_2);
		assertEquals(cf2.getSesso(), this.SESSO_2);
		assertEquals(cf2.getCodiceFiscale(), this.CF_2);

		final CodiceFiscale cf3 = CodiceFiscale.of(this.COGNOME_3, this.NOME_3, this.DATA_NASCITA_3, this.SESSO_3, this.CODICE_CATASTALE_3);
		assertEquals(cf3.getCodiceCatastale(), this.CODICE_CATASTALE_3);
		assertEquals(cf3.getDataNascita(), this.DATA_NASCITA_3);
		assertEquals(cf3.getSesso(), this.SESSO_3);
		assertEquals(cf3.getCodiceFiscale(), this.CF_3);
	}

	@Test
	public void fromCf() {
		final Optional<CodiceFiscale> opt1 = CodiceFiscale.of(this.CF_1);
		assertTrue(opt1.isPresent());
		final CodiceFiscale codiceFiscale1 = opt1.get();
		assertEquals(codiceFiscale1.getCodiceCatastale(), this.CODICE_CATASTALE_1);
		assertEquals(codiceFiscale1.getDataNascita(), this.DATA_NASCITA_1);
		assertEquals(codiceFiscale1.getSesso(), this.SESSO_1);

		final Optional<CodiceFiscale> opt2 = CodiceFiscale.of(this.CF_2);
		assertTrue(opt2.isPresent());
		final CodiceFiscale codiceFiscale2 = opt2.get();
		assertEquals(codiceFiscale2.getCodiceCatastale(), this.CODICE_CATASTALE_2);
		assertEquals(codiceFiscale2.getDataNascita(), this.DATA_NASCITA_2);
		assertEquals(codiceFiscale2.getSesso(), this.SESSO_2);

		final Optional<CodiceFiscale> opt4 = CodiceFiscale.of(this.CF_1.substring(0, 15) + "Z");
		assertFalse(opt4.isPresent());
		final Optional<CodiceFiscale> opt3 = CodiceFiscale.of(this.CF_1.substring(0, 15) + "1");
		assertFalse(opt3.isPresent());
	}

	@Test
	public void valida() {
		assertTrue(CodiceFiscale.isValido(this.CF_1));
		assertTrue(CodiceFiscale.isValido(this.CF_1, this.COGNOME_1, this.NOME_1, this.DATA_NASCITA_1, this.SESSO_1, this.CODICE_CATASTALE_1));
		assertTrue(CodiceFiscale.valida(this.CF_1, this.COGNOME_1, this.NOME_1, this.DATA_NASCITA_1, this.SESSO_1, this.CODICE_CATASTALE_1).isEmpty());

		assertTrue(CodiceFiscale.isValido(this.CF_2));
		assertTrue(CodiceFiscale.isValido(this.CF_2, this.COGNOME_2, this.NOME_2, this.DATA_NASCITA_2, this.SESSO_2, this.CODICE_CATASTALE_2));
		assertTrue(CodiceFiscale.valida(this.CF_2, this.COGNOME_2, this.NOME_2, this.DATA_NASCITA_2, this.SESSO_2, this.CODICE_CATASTALE_2).isEmpty());

		assertFalse(CodiceFiscale.isValido(this.CF_1.substring(0, 15) + "Z"));
		assertFalse(CodiceFiscale.isValido(this.CF_1.substring(0, 15) + "Z", this.COGNOME_1, this.NOME_1, this.DATA_NASCITA_1, this.SESSO_1, this.CODICE_CATASTALE_1));
		final Set<Errore> errori3 = CodiceFiscale.valida(this.CF_1.substring(0, 15) + "Z", this.COGNOME_1, this.NOME_1, this.DATA_NASCITA_1, this.SESSO_1, this.CODICE_CATASTALE_1);
		assertEquals(errori3.size(), 1);
		assertTrue(errori3.contains(Errore.CARATTERE_CONTROLLO));

		final Set<Errore> errori4 = CodiceFiscale.valida(this.CF_1, this.COGNOME_2, this.NOME_1, this.DATA_NASCITA_2, this.SESSO_2, this.CODICE_CATASTALE_2);
		assertEquals(errori4.size(), 4);
		assertTrue(errori4.contains(Errore.COGNOME));
		assertTrue(errori4.contains(Errore.DATA_NASCITA));
		assertTrue(errori4.contains(Errore.SESSO));
		assertTrue(errori4.contains(Errore.CODICE_CATASTALE));

		final Set<Errore> errori5 = CodiceFiscale.valida(this.CF_2, this.COGNOME_2, this.COGNOME_2, this.DATA_NASCITA_2, this.SESSO_1, this.CODICE_CATASTALE_2);
		assertEquals(errori5.size(), 2);
		assertTrue(errori5.contains(Errore.NOME));
		assertTrue(errori5.contains(Errore.SESSO));

		final Set<Errore> errori6 = CodiceFiscale.valida(this.CF_1.substring(0, 15) + "1", this.COGNOME_2, this.COGNOME_2, this.DATA_NASCITA_2, this.SESSO_1, this.CODICE_CATASTALE_2);
		assertEquals(errori6.size(), 1);
		assertTrue(errori6.contains(Errore.FORMATO));

		final Set<Errore> errori7 = CodiceFiscale.valida(this.CF_1, null, null, null, null, null);
		assertTrue(errori7.isEmpty());
	}

	@Test
	public void omocodia() {
		final String cfOmocodia = "RSSMRALLALMALLMG";
		final Optional<CodiceFiscale> opt = CodiceFiscale.of(cfOmocodia);
		assertTrue(opt.isPresent());
		final CodiceFiscale cf1 = opt.get();
		assertEquals(cf1.getCodiceCatastale(), this.CODICE_CATASTALE_1);
		assertEquals(cf1.getDataNascita(), this.DATA_NASCITA_1);
		assertEquals(cf1.getSesso(), this.SESSO_1);
		assertTrue(cf1.isOmocodia());
		assertFalse(cf1.getOmocodie().get(0));
		assertFalse(cf1.getOmocodie().get(1));
		assertFalse(cf1.getOmocodie().get(2));
		assertFalse(cf1.getOmocodie().get(3));
		assertFalse(cf1.getOmocodie().get(4));
		assertFalse(cf1.getOmocodie().get(5));
		assertTrue(cf1.getOmocodie().get(6));
		assertTrue(cf1.getOmocodie().get(7));
		assertFalse(cf1.getOmocodie().get(8));
		assertTrue(cf1.getOmocodie().get(9));
		assertTrue(cf1.getOmocodie().get(10));
		assertFalse(cf1.getOmocodie().get(11));
		assertTrue(cf1.getOmocodie().get(12));
		assertTrue(cf1.getOmocodie().get(13));
		assertTrue(cf1.getOmocodie().get(14));
		assertFalse(cf1.getOmocodie().get(15));
	}
}
