package com.azserve.azframework.italia;

public final class PartitaIva {

	private PartitaIva() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	public static boolean isValida(final String partitaIva) {
		if (partitaIva.length() != 11) {
			return false;
		}
		// faster than regex, avoid IllegalArgumentException in Luhn.check
		if (!partitaIva.chars().allMatch(i -> i >= '0' && i <= '9')) {
			return false;
		}
		return Luhn.check(partitaIva);
	}
}
