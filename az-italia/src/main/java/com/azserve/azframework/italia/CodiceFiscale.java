package com.azserve.azframework.italia;

import java.text.Normalizer;
import java.time.LocalDate;
import java.util.BitSet;
import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.lang.StringUtils;

public class CodiceFiscale {

	public enum Sesso {
		M,
		F
	}

	public enum Errore {
		FORMATO,
		CARATTERE_CONTROLLO,
		COGNOME,
		NOME,
		DATA_NASCITA,
		SESSO,
		CODICE_CATASTALE
	}

	public static final String REGEX = "[A-Z]{6}[0-9LMNPQRSTUV]{2}[ABCDEHLMPRST]{1}[0-7LMNPQRST]{1}[0-9LMNPQRSTUV]{1}[A-Z]{1}[0-9LMNPQRSTUV]{3}[A-Z]{1}";

	private static final char[] OMOCODICI = new char[] { 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V' };
	private static final char[] MESI = new char[] { 'A', 'B', 'C', 'D', 'E', 'H', 'L', 'M', 'P', 'R', 'S', 'T' };
	private static final int[] CONTROLLO_PARI = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 };
	private static final int[] CONTROLLO_DISPARI = new int[] { 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, -1, -1, -1, -1, -1, -1, -1, 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 2, 4, 18, 20, 11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23 };
	private static final int[] POSIZIONE_CARATTERI_NUMERICI = { 6, 7, 9, 10, 12, 13, 14 };

	public static Optional<CodiceFiscale> of(@NonNull final String codiceFiscale) {
		final String cfUpper = codiceFiscale.toUpperCase();
		if (!isValido(cfUpper)) {
			return Optional.empty();
		}
		return Optional.of(new CodiceFiscale(cfUpper, getSesso(cfUpper), getCodiceCatastale(cfUpper), getDataNascita(cfUpper)));
	}

	public static @NonNull CodiceFiscale of(final @NonNull String cognome, final @NonNull String nome, final @NonNull LocalDate dataNascita, final @NonNull Sesso sesso, final @NonNull String codiceCatastale) {
		final String sezioneCognome = getSezioneCognome(cognome);
		final String sezioneNome = getSezioneNome(nome);
		final String sezioneDataSesso = getSezioneDataSesso(dataNascita, sesso);
		final String tmp = sezioneCognome + sezioneNome + sezioneDataSesso + codiceCatastale;
		return new CodiceFiscale(tmp + getCarattereControllo(tmp), sesso, codiceCatastale, dataNascita);
	}

	public static boolean isValido(@NonNull final String codiceFiscale) {
		return codiceFiscale.matches(REGEX) && codiceFiscale.charAt(15) == getCarattereControllo(codiceFiscale.substring(0, 15));
	}

	public static boolean isValido(@NonNull final String codiceFiscale, @Nullable final String cognome, @Nullable final String nome, @Nullable final LocalDate dataNascita, @Nullable final Sesso sesso, final @Nullable String codiceCatastale) {
		return valida(codiceFiscale, cognome, nome, dataNascita, sesso, codiceCatastale).isEmpty();
	}

	public static Set<Errore> valida(@NonNull final String codiceFiscale, @Nullable final String cognome, @Nullable final String nome, @Nullable final LocalDate dataNascita, @Nullable final Sesso sesso, final @Nullable String codiceCatastale) {
		final String cfUpper = codiceFiscale.toUpperCase();
		if (!cfUpper.matches(REGEX)) {
			return EnumSet.of(Errore.FORMATO);
		}
		final EnumSet<Errore> errori = EnumSet.noneOf(Errore.class);
		if (cfUpper.charAt(15) != getCarattereControllo(cfUpper.substring(0, 15))) {
			errori.add(Errore.CARATTERE_CONTROLLO);
		}
		if (cognome != null) {
			final String caratteriCognome = cfUpper.substring(0, 3);
			if (!caratteriCognome.equalsIgnoreCase(getSezioneCognome(cognome))) {
				errori.add(Errore.COGNOME);
			}
		}
		if (nome != null) {
			final String caratteriNome = cfUpper.substring(3, 6);
			if (!caratteriNome.equalsIgnoreCase(getSezioneNome(nome))) {
				errori.add(Errore.NOME);
			}
		}
		if (sesso != null) {
			char carattereSesso;
			carattereSesso = getNumeroOmocodia(cfUpper.charAt(9));
			if (sesso == Sesso.M) {
				if (carattereSesso < '0' || carattereSesso > '3') {
					errori.add(Errore.SESSO);
				}
			} else if (sesso == Sesso.F) {
				if (carattereSesso < '4' || carattereSesso > '7') {
					errori.add(Errore.SESSO);
				}
			}
		}
		if (dataNascita != null) {
			if (getDataNascita(cfUpper).compareTo(dataNascita) != 0) {
				errori.add(Errore.DATA_NASCITA);
			}
		}
		if (codiceCatastale != null) {
			if (!getCodiceCatastale(cfUpper).equals(codiceCatastale.toUpperCase())) {
				errori.add(Errore.CODICE_CATASTALE);
			}
		}
		return errori;
	}

	private final String codiceFiscale;
	private final Sesso sesso;
	private final String codiceCatastale;
	private final LocalDate dataNascita;
	private final int omocodie;

	private CodiceFiscale(final String codiceFiscale, final Sesso sesso, final String codiceCatastale, final LocalDate dataNascita) {
		this.codiceFiscale = codiceFiscale;
		this.sesso = sesso;
		this.codiceCatastale = codiceCatastale;
		this.dataNascita = dataNascita;
		this.omocodie = getOmocodie(codiceFiscale);
	}

	public String getCodiceFiscale() {
		return this.codiceFiscale;
	}

	public Sesso getSesso() {
		return this.sesso;
	}

	public String getCodiceCatastale() {
		return this.codiceCatastale;
	}

	public LocalDate getDataNascita() {
		return this.dataNascita;
	}

	public boolean isOmocodia() {
		return this.omocodie != 0;
	}

	public BitSet getOmocodie() {
		return BitSet.valueOf(new long[] { this.omocodie });
	}

	private static int getOmocodie(final String codiceFiscale) {
		int omocodie = 0;
		for (final int pos : POSIZIONE_CARATTERI_NUMERICI) {
			if (codiceFiscale.charAt(pos) > '9') {
				final int mask = 1 << pos;
				omocodie = omocodie | mask;
			}
		}
		return omocodie;
	}

	private static String getCodiceCatastale(final String codiceFiscale) {
		return codiceFiscale.substring(11, 12) + decodificaOmocodia(codiceFiscale.substring(12, 15));
	}

	private static Sesso getSesso(final String codiceFiscale) {
		final char c = codiceFiscale.toUpperCase().charAt(9);
		if (c >= '0' && c <= '3' || c == 'L' || c == 'M' || c == 'N' || c == 'P') {
			return Sesso.M;
		} else if (c >= '4' && c <= '7' || c >= 'Q' && c <= 'T') {
			return Sesso.F;
		}
		throw new IllegalArgumentException("Invalid char " + c);
	}

	private static LocalDate getDataNascita(final String codiceFiscale) {
		int anno = Integer.parseInt(decodificaOmocodia(codiceFiscale.substring(6, 8)));
		final int mese = getMese(codiceFiscale.charAt(8));
		final int giorno = Integer.parseInt(decodificaOmocodia(codiceFiscale.substring(9, 11))) % 40;
		final int annoCorrente = LocalDate.now().getYear();
		final int c = annoCorrente % 100;
		if (anno <= c) {
			anno += annoCorrente - annoCorrente % 100;
		} else {
			anno += annoCorrente - annoCorrente % 100 - 100;
		}
		return LocalDate.of(anno, mese + 1, giorno);
	}

	private static char getNumeroOmocodia(final char c) {
		if (c >= '0' && c <= '9') {
			return c;
		}
		for (int i = 0; i < OMOCODICI.length; i++) {
			if (OMOCODICI[i] == c) {
				return (i + "").charAt(0);
			}
		}
		throw new IllegalArgumentException("Invalid char " + c);
	}

	private static String decodificaOmocodia(final @NonNull String source) {
		final char[] result = new char[source.length()];
		for (int i = 0; i < source.length(); i++) {
			result[i] = getNumeroOmocodia(source.charAt(i));
		}
		return new String(result);
	}

	private static int getMese(final char c) {
		for (int i = 0; i < MESI.length; i++) {
			if (MESI[i] == c) {
				return i;
			}
		}
		throw new IllegalArgumentException("Invalid char " + c);
	}

	private static String getSezioneCognome(final String cognome) {
		final String normString = Normalizer.normalize(cognome, Normalizer.Form.NFD).toUpperCase();

		final char[] result = new char[3];
		int len = 0;
		int i = 0;
		while (len < 3 && i < normString.length()) {
			final char c = normString.charAt(i);
			if (isConsonante(c)) {
				result[len++] = c;
			}
			i++;
		}

		i = 0;
		while (len < 3 && i < normString.length()) {
			final char c = normString.charAt(i);
			if (isVocale(c)) {
				result[len++] = c;
			}
			i++;
		}

		while (len < 3) {
			result[len++] = 'X';
		}

		return new String(result);
	}

	private static String getSezioneNome(final String nome) {
		final String normString = Normalizer.normalize(nome, Normalizer.Form.NFD).toUpperCase();

		final char[] result = new char[4];
		int len = 0;
		int i = 0;
		while (len < 4 && i < normString.length()) {
			final char c = normString.charAt(i);
			if (isConsonante(c)) {
				result[len++] = c;
			}
			i++;
		}
		if (len == 4) {
			return new String(new char[] { result[0], result[2], result[3] });
		}

		i = 0;
		while (len < 3 && i < normString.length()) {
			final char c = normString.charAt(i);
			if (isVocale(c)) {
				result[len++] = c;
			}
			i++;
		}

		while (len < 3) {
			result[len++] = 'X';
		}

		return new String(result, 0, 3);
	}

	private static String getSezioneDataSesso(final LocalDate dataNascita, final Sesso sesso) {
		final String sezioneAnno = StringUtils.padLeft("" + dataNascita.getYear() % 100, 2, '0');
		final String sezioneMese = "" + MESI[dataNascita.getMonthValue() - 1];
		final String sezioneGiorno = StringUtils.padLeft("" + (dataNascita.getDayOfMonth() + (sesso == Sesso.M ? 0 : 40)), 2, '0');
		return sezioneAnno + sezioneMese + sezioneGiorno;
	}

	private static char getCarattereControllo(final String codiceFiscale) {
		int somma = 0;
		for (int i = 0; i < 15; i += 2) {
			somma += CONTROLLO_DISPARI[codiceFiscale.charAt(i) - '0'];
		}
		for (int i = 1; i < 15; i += 2) {
			somma += CONTROLLO_PARI[codiceFiscale.charAt(i) - '0'];
		}
		return (char) ('A' + somma % 26);
	}

	private static boolean isConsonante(final char c) {
		return c >= 'A' && c <= 'Z' && !isVocale(c);
	}

	private static boolean isVocale(final char c) {
		return c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U';
	}
}