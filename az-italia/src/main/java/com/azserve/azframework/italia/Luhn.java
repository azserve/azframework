package com.azserve.azframework.italia;

final class Luhn {

	private Luhn() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	public static char getCheckDigit(final String s) {
		final char[] cs = s.toCharArray();
		final int[] is = new int[cs.length];
		for (int i = 0; i < cs.length; i++) {
			final char c = cs[i];
			if (!isValid(c)) {
				throw new IllegalArgumentException("Invalid value " + c);
			}
			is[i] = c - '0';
		}
		return (char) ('0' + getCheckDigit(is));
	}

	public static boolean check(final String s) {
		final char[] cs = s.toCharArray();
		final int[] is = new int[cs.length];
		for (int i = 0; i < cs.length; i++) {
			final char c = cs[i];
			if (!isValid(c)) {
				throw new IllegalArgumentException("Invalid value " + c);
			}
			is[i] = c - '0';
		}
		return _check(is);
	}

	public static int getCheckDigit(final int... digits) {
		for (final int i : digits) {
			if (!isValid(i)) {
				throw new IllegalArgumentException("Invalid value " + i);
			}
		}
		return _getCheckDigit(false, digits);
	}

	public static boolean check(final int... digits) {
		for (final int i : digits) {
			if (!isValid(i)) {
				throw new IllegalArgumentException("Invalid value " + i);
			}
		}
		return _check(digits);
	}

	private static boolean isValid(final int digit) {
		return digit >= 0 && digit <= 9;
	}

	private static boolean isValid(final char digit) {
		return digit >= '0' && digit <= '9';
	}

	private static int _getCheckDigit(final boolean excludeLast, final int... digits) {
		final int length = digits.length - (excludeLast ? 1 : 0);
		int sum = 0;
		for (int i = 0; i < length; i++) {
			final int j = digits[i];
			if (i % 2 == 0) {
				sum += j;
			} else {
				sum += j * 2;
				if (j >= 5) {
					sum -= 9;
				}
			}
		}
		final int mod = sum % 10;
		return mod == 0 ? 0 : 10 - mod;
	}

	private static boolean _check(final int... digits) {
		return _getCheckDigit(true, digits) == digits[digits.length - 1];
	}
}
