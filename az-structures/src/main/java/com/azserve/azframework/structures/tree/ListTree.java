package com.azserve.azframework.structures.tree;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ListTree<T> implements Tree<T, ListTree<T>> {

	private static final long serialVersionUID = 5548306861268391576L;

	private final T data;
	private final ListTree<T> parent;
	private List<ListTree<T>> children;

	public ListTree(final T data) {
		this(data, null);
	}

	private ListTree(final T data, final ListTree<T> parent) {
		this.data = data;
		this.parent = parent;
	}

	/**
	 * Restituisce il dato contenuto
	 * rappresentato dal nodo.
	 */
	@Override
	public T getData() {
		return this.data;
	}

	/**
	 * Indica se il nodo è radice
	 * (non ha un nodo padre).
	 */
	@Override
	public boolean isRoot() {
		return this.parent == null;
	}

	/**
	 * Indica se il nodo è foglia
	 * (non ha nodi figli).
	 */
	@Override
	public boolean isLeaf() {
		return this.children == null
				|| this.children.isEmpty();
	}

	/**
	 * Restituisce il nodo padre.
	 *
	 * @return il nodo padre o {@code null}
	 *         se questo nodo è radice.
	 */
	@Override
	public ListTree<T> getParent() {
		return this.parent;
	}

	/**
	 * Restituisce il livello di profondita
	 * nell'albero.
	 *
	 * @return il livello, {@code 0} se
	 *         questo nodo è radice,
	 *         altrimenti da {@code 1}
	 *         in su.
	 */
	@Override
	public int getLevel() {
		if (this.isRoot()) {
			return 0;
		}
		return this.parent.getLevel() + 1;
	}

	/**
	 * Restituisce una view in sola lettura
	 * sulla lista dei nodi figli.
	 */
	public List<ListTree<T>> getChildren() {
		if (this.children == null) {
			return Collections.emptyList();
		}
		return Collections.unmodifiableList(this.children);
	}

	/**
	 * Genera un nodo rappresentate il dato
	 * indicato e lo aggiunge alla lista
	 * dei figli di questo nodo.
	 *
	 * @param childData il dato.
	 * @return il nodo generato.
	 */
	public ListTree<T> addChild(final T childData) {
		final ListTree<T> childNode = new ListTree<>(childData, this);

		if (this.children == null) {
			this.children = new LinkedList<>();
		}
		this.children.add(childNode);
		return childNode;
	}

	@Override
	public Iterator<ListTree<T>> iterator() {
		return this.getChildren().iterator();
	}

	@Override
	public String toString() {
		return "Node [" + this.data + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.data == null ? 0 : this.data.hashCode());
		result = prime * result + (this.parent == null ? 0 : this.parent.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final ListTree<?> other = (ListTree<?>) obj;
		if (this.data == null) {
			if (other.getData() != null) {
				return false;
			}
		} else if (!this.data.equals(other.getData())) {
			return false;
		}
		if (this.parent == null) {
			if (other.getParent() != null) {
				return false;
			}
		} else if (!this.parent.equals(other.getParent())) {
			return false;
		}
		return true;
	}

}
