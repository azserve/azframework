package com.azserve.azframework.structures.tree;

import java.io.Serializable;

/**
 * Struttura ad albero.
 * <p>
 * Ad un albero possono essere aggiunti
 * altri alberi, ma non può essere modificato.
 * </p>
 * <p>
 * Implementa {@linkplain Iterable} solo
 * ed esclusivamente per ciclare sugli alberi
 * figli di primo livello.
 * </p>
 *
 * @author filippo, luca
 * @since 19/01/2018
 * @param <T> tipo di dato rappresentato.
 * @param <C> tipo dei nodi figli.
 */
public interface Tree<T, C extends Tree<T, C>> extends Iterable<C>, Serializable {

	/**
	 * Restituisce il dato contenuto
	 * rappresentato dal nodo.
	 */
	public T getData();

	/**
	 * Indica se il nodo è radice
	 * (non ha un nodo padre).
	 */
	default boolean isRoot() {
		return this.getParent() == null;
	}

	/**
	 * Indica se il nodo è foglia
	 * (non ha nodi figli).
	 */
	public boolean isLeaf();

	/**
	 * Restituisce il nodo padre.
	 *
	 * @return il nodo padre o {@code null}
	 *         se questo nodo è radice.
	 */
	public C getParent();

	/**
	 * Restituisce il livello di profondita
	 * nell'albero.
	 *
	 * @return il livello, {@code 0} se
	 *         questo nodo è radice,
	 *         altrimenti da {@code 1}
	 *         in su.
	 */
	default int getLevel() {
		if (this.isRoot()) {
			return 0;
		}
		return this.getParent().getLevel() + 1;
	}

}
