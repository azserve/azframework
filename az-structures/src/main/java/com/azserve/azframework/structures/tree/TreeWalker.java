package com.azserve.azframework.structures.tree;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;
import java.util.function.Consumer;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.funct.Functions;

/**
 * Classe di utilità per l'attraversamento
 * di strutture al albero {@linkplain Tree}.
 *
 * @author luca
 * @since 19/01/2018
 */
public final class TreeWalker {

	private TreeWalker() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Attraversa una struttura ad albero.
	 *
	 * @param tree struttura ad albero.
	 * @param preVisitAction azione sul nodo prima di attraversare i figli.
	 * @param postVisitAction azione sul nodo dopo aver attraversato i figli.
	 */
	public static <T, C extends Tree<T, C>> void walk(final @NonNull C tree, final @NonNull Consumer<C> preVisitAction, final @NonNull Consumer<C> postVisitAction) {
		Objects.requireNonNull(tree);

		preVisitAction.accept(tree);
		tree.forEach(c -> walk(c, preVisitAction, postVisitAction));
		postVisitAction.accept(tree);
	}

	/**
	 * Attraversa una struttura ad albero
	 * in modalità "pre-order".
	 *
	 * @param tree struttura ad albero.
	 * @param action azione da effettuare sui nodi.
	 */
	public static <T, C extends Tree<T, C>> void preOrder(final @NonNull C tree, final @NonNull Consumer<T> action) {
		walk(Objects.requireNonNull(tree), t -> action.accept(t.getData()), Functions.nullConsumer());
	}

	/**
	 * Attraversa una struttura ad albero
	 * in modalità "post-order".
	 *
	 * @param tree struttura ad albero.
	 * @param action azione da effettuare sui nodi.
	 */
	public static <T, C extends Tree<T, C>> void postOrder(final @NonNull C tree, final @NonNull Consumer<T> action) {
		walk(Objects.requireNonNull(tree), Functions.nullConsumer(), t -> action.accept(t.getData()));
	}

	/**
	 * Attraversa una struttura ad albero binario.
	 *
	 * @param tree struttura ad albero binario.
	 * @param action azione da effettuare sui nodi.
	 */
	public static <T> void inOrder(final @NonNull BinaryTree<T> tree, final @NonNull Consumer<T> action) {
		Objects.requireNonNull(tree);

		final BinaryTree<T> left = tree.getLeft();
		if (left != null) {
			inOrder(left, action);
		}
		action.accept(tree.getData());
		final BinaryTree<T> right = tree.getRight();
		if (right != null) {
			inOrder(right, action);
		}
	}

	/**
	 * Attraversa una struttura ad albero
	 * livello per livello.
	 *
	 * @param tree struttura ad albero.
	 * @param action azione da effettuare sui nodi.
	 */
	public static <T, C extends Tree<T, C>> void levelOrder(final @NonNull C tree, final @NonNull Consumer<T> action) {
		Objects.requireNonNull(tree);

		final Queue<C> next = new LinkedList<>();
		next.add(tree);
		C t;
		while ((t = next.poll()) != null) {
			action.accept(t.getData());
			t.forEach(next::add);
		}
	}
}
