package com.azserve.azframework.structures.graph;

import java.util.ArrayList;
import java.util.Collection;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.util.Pair;

public interface Graph<V extends Vertex<T>, T, E> {

	@NonNull
	V addVertex(T v);

	boolean removeVertex(@NonNull V v, boolean onlyAlone);

	@Nullable
	E getEdge(@NonNull V from, @NonNull V to);

	@Nullable
	E setEdge(@NonNull V from, @NonNull V to, @Nullable E edge);

	@Nullable
	E removeEdge(@NonNull V from, @NonNull V to);

	@NonNull
	Collection<V> getVertices();

	@NonNull
	Collection<Pair<V, E>> outgoingEdgesOf(@NonNull SimpleVertex<T> vertex);

	@NonNull
	Collection<Pair<V, E>> incomingEdgesOf(@NonNull SimpleVertex<T> vertex);

	@NonNull
	default Collection<Pair<V, E>> edgesOf(final @NonNull SimpleVertex<T> vertex) {
		final Collection<Pair<V, E>> outgoingEdgesOf = this.outgoingEdgesOf(vertex);
		final Collection<Pair<V, E>> incomingEdgesOf = this.incomingEdgesOf(vertex);
		final Collection<Pair<V, E>> edges = new ArrayList<>(outgoingEdgesOf.size() + incomingEdgesOf.size());
		edges.addAll(outgoingEdgesOf);
		edges.addAll(incomingEdgesOf);
		return edges;
	}

}
