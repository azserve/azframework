package com.azserve.azframework.structures.tree;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

/**
 * Implementazione di albero binario.
 *
 * @author luca
 * @since 19/01/2018
 *
 * @param <T> tipo di dato rappresentato.
 */
public class BinaryTree<T> implements Tree<T, BinaryTree<T>> {

	private static final long serialVersionUID = -9072836175277987274L;

	private final T data;
	private final BinaryTree<T> parent;
	private BinaryTree<T> left;
	private BinaryTree<T> right;

	public BinaryTree(final T data) {
		this(data, null);
	}

	private BinaryTree(final T data, final BinaryTree<T> parent) {
		this.data = data;
		this.parent = parent;
	}

	@Override
	public Iterator<BinaryTree<T>> iterator() {
		if (this.left == null && this.right == null) {
			return Collections.emptyIterator();
		}
		if (this.left == null) {
			return Collections.singletonList(this.right).iterator();
		}
		if (this.right == null) {
			return Collections.singletonList(this.left).iterator();
		}
		return Arrays.asList(this.left, this.right).iterator();
	}

	@Override
	public T getData() {
		return this.data;
	}

	@Override
	public boolean isLeaf() {
		return this.left == null && this.right == null;
	}

	@Override
	public BinaryTree<T> getParent() {
		return this.parent;
	}

	public BinaryTree<T> addChildLeft(final T leftData) {
		if (this.left != null) {
			throw new IllegalStateException("Left child already set");
		}
		this.left = new BinaryTree<>(leftData, this);
		return this.left;
	}

	public BinaryTree<T> getLeft() {
		return this.left;
	}

	public BinaryTree<T> addChildRight(final T rightData) {
		if (this.right != null) {
			throw new IllegalStateException("Right child already set");
		}
		this.right = new BinaryTree<>(rightData, this);
		return this.right;
	}

	public BinaryTree<T> getRight() {
		return this.right;
	}

}
