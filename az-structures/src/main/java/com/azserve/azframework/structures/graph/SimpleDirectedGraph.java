package com.azserve.azframework.structures.graph;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.util.Pair;

public class SimpleDirectedGraph<T, E> implements Graph<SimpleVertex<T>, T, E> {

	private final Set<SimpleVertex<T>> vertices = new HashSet<>();

	private final Map<Pair<SimpleVertex<T>, SimpleVertex<T>>, E> edges = new HashMap<>();

	@Override
	public @NonNull SimpleVertex<T> addVertex(final T v) {
		final SimpleVertex<T> vertex = new SimpleVertex<>(v);
		this.vertices.add(vertex);
		return vertex;
	}

	@Override
	public boolean removeVertex(final @NonNull SimpleVertex<T> v, final boolean onlyAlone) {
		if (!this.vertices.contains(v)) {
			throw new IllegalStateException("Vertex doesn't exist");
		}

		final List<Pair<SimpleVertex<T>, SimpleVertex<T>>> edgesKey = this.getEdgesKey(v).map(Entry::getKey).collect(Collectors.toList());
		if (!edgesKey.isEmpty() && onlyAlone) {
			throw new IllegalStateException("Vertex has edges");
		}
		edgesKey.forEach(this.edges::remove);
		this.vertices.remove(v);
		return edgesKey.isEmpty();
	}

	@Override
	public E setEdge(final @NonNull SimpleVertex<T> from, final @NonNull SimpleVertex<T> to, final @Nullable E edge) {
		final Pair<SimpleVertex<T>, SimpleVertex<T>> pair = this.getVerticesPair(from, to);
		return this.edges.put(pair, edge);
	}

	@Override
	public E removeEdge(final @NonNull SimpleVertex<T> from, final @NonNull SimpleVertex<T> to) {
		final Pair<SimpleVertex<T>, SimpleVertex<T>> pair = this.getVerticesPair(from, to);
		return this.edges.remove(pair);
	}

	@Override
	public @NonNull Collection<SimpleVertex<T>> getVertices() {
		return Collections.unmodifiableCollection(this.vertices);
	}

	@Override
	public @NonNull Collection<Pair<SimpleVertex<T>, E>> outgoingEdgesOf(final @NonNull SimpleVertex<T> vertex) {
		if (!this.vertices.contains(vertex)) {
			throw new IllegalStateException("Vertex doesn't exist");
		}
		return this.getEdgesKey(vertex)
				.filter(e -> e.getKey().getValue1() == vertex)
				.map(e -> Pair.of(e.getKey().getValue2(), e.getValue()))
				.collect(Collectors.toList());
	}

	@Override
	public @NonNull Collection<Pair<SimpleVertex<T>, E>> incomingEdgesOf(final @NonNull SimpleVertex<T> vertex) {
		if (!this.vertices.contains(vertex)) {
			throw new IllegalStateException("Vertex doesn't exist");
		}
		return this.getEdgesKey(vertex)
				.filter(e -> e.getKey().getValue2() == vertex)
				.map(e -> Pair.of(e.getKey().getValue1(), e.getValue()))
				.collect(Collectors.toList());
	}

	@Override
	public E getEdge(final @NonNull SimpleVertex<T> from, final @NonNull SimpleVertex<T> to) {
		final Pair<SimpleVertex<T>, SimpleVertex<T>> pair = this.getVerticesPair(from, to);
		return this.edges.get(pair);
	}

	private Pair<SimpleVertex<T>, SimpleVertex<T>> getVerticesPair(final SimpleVertex<T> from, final SimpleVertex<T> to) {
		if (!this.vertices.contains(from)) {
			throw new IllegalStateException("Vertex \"from\" doesn't exist");
		}
		if (!this.vertices.contains(to)) {
			throw new IllegalStateException("Vertex \"to\" doesn't exist");
		}
		final Pair<SimpleVertex<T>, SimpleVertex<T>> pair = Pair.of(from, to);
		return pair;
	}

	private Stream<Entry<Pair<SimpleVertex<T>, SimpleVertex<T>>, E>> getEdgesKey(final SimpleVertex<T> vertex) {
		return this.edges.entrySet().stream().filter(e -> {
			final Pair<SimpleVertex<T>, SimpleVertex<T>> k = e.getKey();
			return k.getValue1() == vertex || k.getValue2() == vertex;
		});
	}
}
