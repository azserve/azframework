package com.azserve.azframework.structures.graph;

public interface Vertex<V> {

	V getValue();
}
