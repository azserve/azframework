package com.azserve.azframework.structures.graph;

import java.util.Collection;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.util.Pair;

public class SimpleUndirectedGraph<V, E> extends SimpleDirectedGraph<V, E> {

	@Override
	public E setEdge(final @NonNull SimpleVertex<V> from, final @NonNull SimpleVertex<V> to, final @Nullable E edge) {
		final E e1 = super.setEdge(from, to, edge);
		final E e2 = super.setEdge(to, from, edge);
		assert e1 == e2;
		return e1;
	}

	@Override
	public E removeEdge(final @NonNull SimpleVertex<V> from, final @NonNull SimpleVertex<V> to) {
		final E e1 = super.removeEdge(from, to);
		final E e2 = super.removeEdge(to, from);
		assert e1 == e2;
		return e1;
	}

	@Override
	public @NonNull Collection<Pair<SimpleVertex<V>, E>> edgesOf(final @NonNull SimpleVertex<V> vertex) {
		return this.incomingEdgesOf(vertex);
	}
}
