package com.azserve.azframework.structure.tree;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.azserve.azframework.structures.tree.BinaryTree;
import com.azserve.azframework.structures.tree.ListTree;
import com.azserve.azframework.structures.tree.TreeWalker;

public class TreeWalkerTest {

	@Test
	public void preOrderTest() {
		final StringBuilder result = new StringBuilder();
		TreeWalker.preOrder(createListTree(), result::append);
		assertEquals("FBADCEGIH", result.toString());
	}

	@Test
	public void postOrderTest() {
		final StringBuilder result = new StringBuilder();
		TreeWalker.postOrder(createListTree(), result::append);
		assertEquals("ACEDBHIGF", result.toString());
	}

	@Test
	public void inOrderTest() {
		final StringBuilder result = new StringBuilder();
		TreeWalker.inOrder(createBinaryTree(), result::append);
		assertEquals("ABCDEFGHI", result.toString());
	}

	@Test
	public void levelOrderTest() {
		final StringBuilder result = new StringBuilder();
		TreeWalker.levelOrder(createListTree(), result::append);
		assertEquals("FBGADICEH", result.toString());
	}

	private static ListTree<String> createListTree() {
		final ListTree<String> f = new ListTree<>("F");
		final ListTree<String> b = f.addChild("B");
		b.addChild("A");
		final ListTree<String> d = b.addChild("D");
		d.addChild("C");
		d.addChild("E");
		final ListTree<String> g = f.addChild("G");
		g.addChild("I").addChild("H");
		return f;
	}

	private static BinaryTree<String> createBinaryTree() {
		final BinaryTree<String> f = new BinaryTree<>("F");
		final BinaryTree<String> b = f.addChildLeft("B");
		b.addChildLeft("A");
		final BinaryTree<String> d = b.addChildRight("D");
		d.addChildLeft("C");
		d.addChildRight("E");
		f.addChildRight("G").addChildRight("I").addChildLeft("H");
		return f;
	}
}
