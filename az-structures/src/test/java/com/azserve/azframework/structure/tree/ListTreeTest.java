package com.azserve.azframework.structure.tree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.azserve.azframework.structures.tree.ListTree;

public class ListTreeTest {

	@Test
	public void testGetData() {
		final ListTree<String> ListTreeA = new ListTree<>("A");
		assertEquals("A", ListTreeA.getData());

		final ListTree<String> ListTreeAB = ListTreeA.addChild("AB");
		assertEquals("AB", ListTreeAB.getData());

		assertNotEquals("ABC", ListTreeA.getData());
		assertNotEquals("ABC", ListTreeAB.getData());
	}

	@Test
	public void testIsRoot() {
		final ListTree<String> ListTreeA = new ListTree<>("A");
		assertTrue(ListTreeA.isRoot());

		final ListTree<String> ListTreeAB = ListTreeA.addChild("AB");
		assertFalse(ListTreeAB.isRoot());
	}

	@Test
	public void testIsLeaf() {
		final ListTree<String> ListTreeA = new ListTree<>("A");
		assertTrue(ListTreeA.isLeaf());

		final ListTree<String> ListTreeAB = ListTreeA.addChild("AB");
		assertTrue(ListTreeAB.isLeaf());
		assertFalse(ListTreeA.isLeaf());
	}

	@Test
	public void testGetParent() {
		final ListTree<String> ListTreeA = new ListTree<>("A");
		assertNull(ListTreeA.getParent());

		final ListTree<String> ListTreeAB = ListTreeA.addChild("AB");
		assertTrue(ListTreeAB.getParent() == ListTreeA);
	}

	@Test
	public void testGetLevel() {
		final ListTree<String> ListTreeA = new ListTree<>("A");
		assertEquals(0, ListTreeA.getLevel());

		final ListTree<String> ListTreeAB = ListTreeA.addChild("AB");
		assertEquals(1, ListTreeAB.getLevel());

		final ListTree<String> ListTreeAC = ListTreeA.addChild("AC");
		assertEquals(1, ListTreeAC.getLevel());

		final ListTree<String> ListTreeABC = ListTreeAB.addChild("ABC");
		assertEquals(2, ListTreeABC.getLevel());

		final ListTree<String> ListTreeABD = ListTreeAB.addChild("ABD");
		assertEquals(2, ListTreeABD.getLevel());
	}

	@Test
	public void testGetChildren() {
		final ListTree<String> ListTreeA = new ListTree<>("A");
		assertTrue(ListTreeA.getChildren().isEmpty());

		final ListTree<String> ListTreeAB = ListTreeA.addChild("AB");
		assertTrue(ListTreeA.getChildren().size() == 1);

		final ListTree<String> ListTreeAC = ListTreeA.addChild("AC");
		assertTrue(ListTreeA.getChildren().contains(ListTreeAC)
				&& ListTreeA.getChildren().contains(ListTreeAB));
	}

	@Test
	public void testEquals() {
		final ListTree<String> ListTreeA_1 = new ListTree<>("A");
		final ListTree<String> ListTreeA_2 = new ListTree<>("A");
		final ListTree<String> ListTreeB = new ListTree<>("B");

		assertEquals(ListTreeA_1, ListTreeA_2);

		assertEquals(ListTreeA_1.addChild("AB"), ListTreeA_1.addChild("AB"));

		assertEquals(ListTreeA_1.addChild("AB"), ListTreeA_2.addChild("AB"));

		assertNotEquals(ListTreeA_1.addChild("AB"), ListTreeB.addChild("AB"));
	}
}
