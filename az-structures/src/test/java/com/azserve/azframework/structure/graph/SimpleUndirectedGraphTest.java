package com.azserve.azframework.structure.graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import com.azserve.azframework.common.test.TestUtils;
import com.azserve.azframework.common.util.Pair;
import com.azserve.azframework.structures.graph.SimpleUndirectedGraph;
import com.azserve.azframework.structures.graph.SimpleVertex;

public class SimpleUndirectedGraphTest {

	@Test
	public void addNode() {
		final SimpleUndirectedGraph<String, Integer> graph = new SimpleUndirectedGraph<>();
		final SimpleVertex<String> rome = graph.addVertex("Rome");
		final SimpleVertex<String> venice = graph.addVertex("Venice");
		final SimpleVertex<String> sdp = graph.addVertex("San Donà di Piave");

		final Collection<SimpleVertex<String>> nodes = graph.getVertices();
		assertEquals(3, nodes.size());
		assertTrue(nodes.contains(rome));
		assertTrue(nodes.contains(venice));
		assertTrue(nodes.contains(sdp));

		assertEquals("Rome", rome.getValue());
	}

	@Test
	public void setEdge() {
		final SimpleUndirectedGraph<String, Integer> graph = new SimpleUndirectedGraph<>();
		final SimpleVertex<String> rome = graph.addVertex("Rome");
		final SimpleVertex<String> venice = graph.addVertex("Venice");
		final SimpleVertex<String> sdp = graph.addVertex("San Donà di Piave");

		assertNull(graph.setEdge(rome, venice, Integer.valueOf(526)));
		assertEquals(Integer.valueOf(526), graph.setEdge(venice, rome, Integer.valueOf(528)));
		assertNull(graph.setEdge(venice, sdp, Integer.valueOf(51)));

		assertEquals(Integer.valueOf(528), graph.getEdge(rome, venice));
		assertEquals(Integer.valueOf(528), graph.getEdge(venice, rome));
		assertEquals(Integer.valueOf(51), graph.getEdge(venice, sdp));
		assertEquals(Integer.valueOf(51), graph.getEdge(sdp, venice));
		assertNull(graph.getEdge(sdp, rome));

		assertEquals(Integer.valueOf(528), graph.setEdge(rome, venice, Integer.valueOf(500)));

		final SimpleUndirectedGraph<String, Integer> graph2 = new SimpleUndirectedGraph<>();
		final SimpleVertex<String> rome2 = graph2.addVertex("Rome");
		TestUtils.assertThrows(IllegalStateException.class, () -> graph.setEdge(rome2, venice, Integer.valueOf(526)));
		TestUtils.assertThrows(IllegalStateException.class, () -> graph.getEdge(rome2, venice));
		TestUtils.assertThrows(IllegalStateException.class, () -> graph.getEdge(venice, rome2));
	}

	@Test
	public void removeEdge() {
		final SimpleUndirectedGraph<String, Integer> graph = new SimpleUndirectedGraph<>();
		final SimpleVertex<String> rome = graph.addVertex("Rome");
		final SimpleVertex<String> venice = graph.addVertex("Venice");
		final SimpleVertex<String> sdp = graph.addVertex("San Donà di Piave");

		graph.setEdge(rome, venice, Integer.valueOf(526));
		graph.setEdge(venice, sdp, Integer.valueOf(51));

		assertNull(graph.removeEdge(rome, sdp));
		assertEquals(Integer.valueOf(526), graph.removeEdge(rome, venice));
		assertNull(graph.getEdge(rome, venice));
		assertNull(graph.getEdge(venice, rome));

		graph.setEdge(rome, venice, Integer.valueOf(526));
		assertEquals(Integer.valueOf(526), graph.removeEdge(venice, rome));
		assertNull(graph.getEdge(rome, venice));
		assertNull(graph.getEdge(venice, rome));
	}

	@Test
	public void removeNode() {
		final SimpleUndirectedGraph<String, Integer> graph = new SimpleUndirectedGraph<>();
		final SimpleVertex<String> rome = graph.addVertex("Rome");
		final SimpleVertex<String> venice = graph.addVertex("Venice");
		final SimpleVertex<String> sdp = graph.addVertex("San Donà di Piave");
		final SimpleVertex<String> alone = graph.addVertex("Alone");

		graph.setEdge(rome, venice, Integer.valueOf(526));
		graph.setEdge(venice, sdp, Integer.valueOf(51));

		TestUtils.assertThrows(IllegalStateException.class, () -> graph.removeVertex(venice, true));

		assertFalse(graph.removeVertex(venice, false));
		TestUtils.assertThrows(IllegalStateException.class, () -> graph.removeVertex(venice, false));

		assertTrue(graph.removeVertex(alone, false));
		TestUtils.assertThrows(IllegalStateException.class, () -> graph.removeVertex(alone, false));

		final SimpleVertex<String> alone1 = graph.addVertex("Alone");
		graph.removeVertex(alone1, true);
		TestUtils.assertThrows(IllegalStateException.class, () -> graph.removeVertex(alone1, false));

	}

	@Test
	public void getNeighbours() {
		final SimpleUndirectedGraph<String, Integer> graph = new SimpleUndirectedGraph<>();
		final SimpleVertex<String> rome = graph.addVertex("Rome");
		final SimpleVertex<String> venice = graph.addVertex("Venice");
		final SimpleVertex<String> sdp = graph.addVertex("San Donà di Piave");

		graph.setEdge(rome, venice, Integer.valueOf(526));
		graph.setEdge(venice, sdp, Integer.valueOf(51));

		final Collection<Pair<SimpleVertex<String>, Integer>> outgoingEdgesOfVenice = graph.outgoingEdgesOf(venice);
		assertEquals(2, outgoingEdgesOfVenice.size());
		assertTrue(outgoingEdgesOfVenice.contains(Pair.of(rome, Integer.valueOf(526))));
		assertTrue(outgoingEdgesOfVenice.contains(Pair.of(sdp, Integer.valueOf(51))));

		final Collection<Pair<SimpleVertex<String>, Integer>> incomingEdgesOfVenice = graph.incomingEdgesOf(venice);
		assertEquals(2, incomingEdgesOfVenice.size());
		assertTrue(incomingEdgesOfVenice.contains(Pair.of(rome, Integer.valueOf(526))));
		assertTrue(incomingEdgesOfVenice.contains(Pair.of(sdp, Integer.valueOf(51))));

		final Collection<Pair<SimpleVertex<String>, Integer>> edgesOfVenice = graph.incomingEdgesOf(venice);
		assertEquals(2, edgesOfVenice.size());
		assertTrue(edgesOfVenice.contains(Pair.of(rome, Integer.valueOf(526))));
		assertTrue(edgesOfVenice.contains(Pair.of(sdp, Integer.valueOf(51))));

		final SimpleUndirectedGraph<String, Integer> graph2 = new SimpleUndirectedGraph<>();
		final SimpleVertex<String> rome2 = graph2.addVertex("Rome");
		TestUtils.assertThrows(IllegalStateException.class, () -> graph.outgoingEdgesOf(rome2));
		TestUtils.assertThrows(IllegalStateException.class, () -> graph.incomingEdgesOf(rome2));
		TestUtils.assertThrows(IllegalStateException.class, () -> graph.edgesOf(rome2));

	}
}
