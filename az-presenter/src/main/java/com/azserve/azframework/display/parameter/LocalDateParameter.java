package com.azserve.azframework.display.parameter;

import java.time.LocalDate;

import com.azserve.azframework.common.annotation.NonNull;

class LocalDateParameter extends DisplayParameter<LocalDate> {

	LocalDateParameter(final String name) {
		super(name);
	}

	@Override
	public LocalDate deserialize(final @NonNull String string) {
		return LocalDate.parse(string);
	}
}
