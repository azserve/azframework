package com.azserve.azframework.display.parameter;

import com.azserve.azframework.common.annotation.NonNull;

class BooleanParameter extends DisplayParameter<Boolean> {

	BooleanParameter(final String name) {
		super(name);
	}

	@Override
	public Boolean deserialize(final @NonNull String string) {
		return Boolean.valueOf(string);
	}
}
