package com.azserve.azframework.display.parameter;

import static java.util.Collections.singletonList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.ex.Checked;
import com.azserve.azframework.common.lang.StringUtils;

/**
 * Defines a type-safe parameter to be passed between
 * presenter and display implementation.
 * <p>
 * Static methods are provided as helper to build
 * a parameters map and to read values from
 * the parameters map.
 */
public abstract class DisplayParameter<T> {

	public static DisplayParameter<String> forString(final String name) {
		return new StringParameter(name);
	}

	public static DisplayParameter<Integer> forInteger(final String name) {
		return new IntegerParameter(name);
	}

	public static DisplayParameter<Boolean> forBoolean(final String name) {
		return new BooleanParameter(name);
	}

	public static DisplayParameter<BigDecimal> forBigDecimal(final String name) {
		return new BigDecimalParameter(name);
	}

	public static DisplayParameter<UUID> forUUID(final String name) {
		return new UUIDParameter(name);
	}

	public static <E extends Enum<E>> DisplayParameter<E> forEnum(final String name, final Class<E> enumClass) {
		return new EnumParameter<>(name, enumClass);
	}

	public static <T> DisplayParameter<T> forObject(final String name, final Function<T, String> serializer, final Function<String, T> deserializer) {
		return new ObjectParameter<>(name, serializer, deserializer);
	}

	/**
	 * Builds a modifiable map given two parameters and non-null values.
	 *
	 * @param <T> type of the first parameter
	 * @param <U> type of the second parameter
	 * @param param1 first parameter
	 * @param value1 first parameter value
	 * @param param2 second parameter
	 * @param value2 second parameter value
	 * @return a modifiable map with two entries
	 */
	public static <T, U> Map<String, List<String>> build(
			final @NonNull DisplayParameter<T> param1,
			final @NonNull T value1,
			final @NonNull DisplayParameter<U> param2,
			final @NonNull U value2) {
		final Map<String, List<String>> result = new HashMap<>();
		result.put(param1.name, singletonList(param1.serialize(value1)));
		result.put(param2.name, singletonList(param2.serialize(value2)));
		return result;
	}

	/**
	 * Builds a modifiable map given one parameter and non-null value.
	 *
	 * @param <T> type of the parameter
	 * @param param the parameter
	 * @param value parameter value
	 * @return a modifiable map with one entry
	 */
	public static <T> Map<String, List<String>> build(
			final @NonNull DisplayParameter<T> param,
			final @NonNull T value) {
		final Map<String, List<String>> result = new HashMap<>();
		result.put(param.name, singletonList(param.serialize(value)));
		return result;
	}

	/**
	 * Provides a builder to obtain a parameter map.
	 */
	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final Map<String, List<String>> internalMap = new HashMap<>();

		private Builder() {}

		public <T> Builder add(final @NonNull DisplayParameter<T> parameter, final T value) {
			if (value != null) {
				this.internalMap.computeIfAbsent(parameter.getName(), key -> new ArrayList<>()).add(parameter.serialize(value));
			}
			return this;
		}

		public <T> Builder addAll(final @NonNull DisplayParameter<T> parameter, @SuppressWarnings("unchecked") final @NonNull T... values) {
			List<String> list = this.internalMap.get(parameter.getName());
			if (list == null) {
				list = new ArrayList<>(values.length);
				this.internalMap.put(parameter.getName(), list);
			}
			for (final T value : values) {
				list.add(parameter.serialize(value));
			}
			return this;
		}

		public <T> Builder addAll(final @NonNull DisplayParameter<T> parameter, final @NonNull Collection<? extends T> values) {
			List<String> list = this.internalMap.get(parameter.getName());
			if (list == null) {
				list = new ArrayList<>(values.size());
				this.internalMap.put(parameter.getName(), list);
			}
			for (final T value : values) {
				list.add(parameter.serialize(value));
			}
			return this;
		}

		/**
		 * Builds the parameter map. The returned map
		 * will be a new modifiable istance.
		 *
		 * @return a modifiable map of the given parameters.
		 */
		public Map<String, List<String>> build() {
			return new HashMap<>(this.internalMap);
		}
	}

	private final String name;

	public DisplayParameter(final String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public Optional<T> value(final Map<String, List<String>> parameters) {
		final List<String> values = parameters.get(this.name);
		return values == null || values.isEmpty()
				? Optional.empty()
				: Optional.ofNullable(values.get(0))
						.map(StringUtils::trimToNull)
						.map(Checked.applyOrNull(this::deserialize));
	}

	public Stream<T> stream(final Map<String, List<String>> parameters) {
		final List<String> values = parameters.get(this.name);
		return values == null
				? Stream.empty()
				: values.stream()
						.map(StringUtils::trimToNull)
						.filter(Objects::nonNull)
						.map(Checked.applyOrNull(this::deserialize))
						.filter(Objects::nonNull);
	}

	public abstract T deserialize(@NonNull String string);

	public @NonNull String serialize(@NonNull final T value) {
		return value.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.name);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}
		final DisplayParameter<?> other = (DisplayParameter<?>) obj;
		return Objects.equals(this.name, other.name);
	}
}
