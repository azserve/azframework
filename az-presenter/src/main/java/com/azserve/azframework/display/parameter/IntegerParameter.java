package com.azserve.azframework.display.parameter;

import com.azserve.azframework.common.annotation.NonNull;

class IntegerParameter extends DisplayParameter<Integer> {

	IntegerParameter(final String name) {
		super(name);
	}

	@Override
	public Integer deserialize(final @NonNull String string) {
		return Integer.valueOf(string);
	}
}
