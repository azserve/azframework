package com.azserve.azframework.display.parameter;

import java.util.UUID;

import com.azserve.azframework.common.annotation.NonNull;

class UUIDParameter extends DisplayParameter<UUID> {

	UUIDParameter(final String name) {
		super(name);
	}

	@Override
	public UUID deserialize(final @NonNull String string) {
		return UUID.fromString(string);
	}
}
