package com.azserve.azframework.display.parameter;

import java.util.function.Function;

import com.azserve.azframework.common.annotation.NonNull;

class ObjectParameter<T> extends DisplayParameter<T> {

	private final Function<T, String> serializer;
	private final Function<String, T> deserializer;

	ObjectParameter(final String name, final Function<T, String> serializer, final Function<String, T> deserializer) {
		super(name);
		this.serializer = serializer;
		this.deserializer = deserializer;
	}

	@Override
	public T deserialize(final @NonNull String string) {
		return this.deserializer.apply(string);
	}

	@Override
	public @NonNull String serialize(final @NonNull T value) {
		return this.serializer.apply(value);
	}
}
