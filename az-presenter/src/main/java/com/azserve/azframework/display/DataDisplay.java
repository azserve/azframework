package com.azserve.azframework.display;

import com.azserve.azframework.common.funct.BooleanConsumer;
import com.azserve.azframework.presenter.DataListPresenter.DataOperation;

public interface DataDisplay<T> extends Display {

	/**
	 * Sets the callback runnable to be called
	 * to perform a save operation.
	 */
	void setOnSave(Runnable callback);

	/**
	 * Reads fields from the given data bean to show
	 * data in the UI.
	 *
	 * @param data
	 * @param operation
	 */
	void readData(T data, DataOperation operation);

	/**
	 * Writes changes to the given data bean if possible.
	 *
	 * @param data
	 * @return
	 */
	boolean writeData(T data);

	/**
	 * Check whether there are uncommitted changes to
	 * the given data bean.
	 *
	 * @param data
	 * @return whether there are uncommitted changes to
	 *         the given data bean
	 */
	boolean isUnsaved(T data);

	/**
	 * Informs the user that there are uncommitted changes
	 * to the given data bean that will be lost.
	 * The user must choose whether or not commit these
	 * changes to the given data bean.<p><u>
	 * Care must be taken not to forget calling the given
	 * {@code BooleanConsumer} saveCallback in order to proceed further.
	 * </u></p>
	 *
	 * @param data
	 * @param operation
	 * @param saveCallback
	 */
	void askUnsaved(T data, DataOperation operation, BooleanConsumer saveCallback);

	/**
	 * Performs action after the given data bean
	 * has been saved.
	 * <p><u>
	 * Care must be taken not to forget calling the given
	 * {@code Runnable} callback in order to proceed further.
	 * </u></p>
	 *
	 * @param data
	 * @param operation
	 * @param callback
	 */
	void onSuccessfulSave(T data, DataOperation operation, Runnable callback);

	/**
	 * Inform the user that the given data bean has not been
	 * saved because of an error.
	 * <p><u>
	 * Care must be taken not to forget calling the given
	 * {@code Runnable} callback in order to proceed further.
	 * </u></p>
	 *
	 * @param data
	 * @param operation
	 * @param ex
	 * @param callback
	 */
	void onFailedSave(T data, DataOperation operation, Exception ex, Runnable callback);

}
