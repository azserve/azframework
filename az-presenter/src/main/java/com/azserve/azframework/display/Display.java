package com.azserve.azframework.display;

import com.azserve.azframework.common.funct.Functions;

public interface Display {

	public enum Type {
		INFO,
		WARN,
		ERROR,
		SUCCESS;
	}

	void notifyMessage(String message, Type type);

	void showMessage(String message, Type type, Runnable callback);

	default void showMessage(final String message, final Type type) {
		this.showMessage(message, type, Functions.NULL_RUNNABLE);
	}

	void showError(Exception ex, Runnable callback);

	default void showError(final Exception ex) {
		this.showError(ex, Functions.NULL_RUNNABLE);
	}
}
