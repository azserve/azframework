package com.azserve.azframework.display;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.azserve.azframework.common.annotation.NonNull;

public interface DataListDisplay<I> extends Display {

	void setOnInsert(@NonNull Runnable callback);

	void setOnEdit(@NonNull Runnable callback);

	void setOnView(@NonNull Runnable callback);

	void setOnDelete(@NonNull Runnable callback);

	void openData(I sourceItem, Map<String, List<String>> parameters);

	Optional<I> getSelectedItem();

	Collection<I> getSelectedItems();

	void setItems(@NonNull List<I> items);

	void askDelete(@NonNull I item, Runnable confirmCallback);

	/**
	 * Performs action after the given item
	 * has been deleted.
	 *
	 * @param item the deleted item
	 */
	void onSuccessfulDelete(I item);
}
