package com.azserve.azframework.display.parameter;

import java.math.BigDecimal;

import com.azserve.azframework.common.annotation.NonNull;

class BigDecimalParameter extends DisplayParameter<BigDecimal> {

	BigDecimalParameter(final String name) {
		super(name);
	}

	@Override
	public BigDecimal deserialize(final @NonNull String string) {
		return new BigDecimal(string);
	}
}
