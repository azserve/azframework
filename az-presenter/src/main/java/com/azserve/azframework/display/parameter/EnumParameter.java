package com.azserve.azframework.display.parameter;

import com.azserve.azframework.common.annotation.NonNull;

class EnumParameter<E extends Enum<E>> extends DisplayParameter<E> {

	private final Class<E> enumClass;

	EnumParameter(final String name, final Class<E> enumClass) {
		super(name);
		this.enumClass = enumClass;
	}

	@Override
	public E deserialize(final @NonNull String string) {
		return Enum.valueOf(this.enumClass, string);
	}
}
