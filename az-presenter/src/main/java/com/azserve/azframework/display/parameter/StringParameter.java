package com.azserve.azframework.display.parameter;

import com.azserve.azframework.common.annotation.NonNull;

class StringParameter extends DisplayParameter<String> {

	StringParameter(final String name) {
		super(name);
	}

	@Override
	public String deserialize(final @NonNull String string) {
		return string;
	}
}
