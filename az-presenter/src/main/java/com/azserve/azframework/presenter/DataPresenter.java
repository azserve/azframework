package com.azserve.azframework.presenter;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.funct.Functions;
import com.azserve.azframework.common.lang.ObjectUtils;
import com.azserve.azframework.display.DataDisplay;
import com.azserve.azframework.display.parameter.DisplayParameter;
import com.azserve.azframework.presenter.DataListPresenter.DataOperation;

@SuppressWarnings("hiding")
public abstract class DataPresenter<D extends DataDisplay<T>, T> extends AbstractPresenter<D> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataPresenter.class);

	public static final DisplayParameter<DataOperation> PARAM_OPERATION = DisplayParameter.forEnum("op", DataOperation.class);
	public static final DisplayParameter<String> PARAM_ID = DisplayParameter.forString("id");

	private List<BiConsumer<T, DataOperation>> afterSaveObservers;

	private T data;
	private DataOperation operation;

	@Override
	void configureDisplayFirstTime() {
		super.configureDisplayFirstTime();
		this.getDisplay().setOnSave(this::save);
	}

	/**
	 * Gets the current managed data.
	 */
	public T getData() {
		return this.data;
	}

	/**
	 * Gets the current operation to be performed
	 * on the data.
	 */
	public DataOperation getOperation() {
		return this.operation;
	}

	@Override
	public void setParameters(final Map<String, List<String>> parameters) {
		final DataOperation op = PARAM_OPERATION.value(parameters).orElse(null);
		final String id = PARAM_ID.value(parameters).orElse(null);
		try {
			if (id == null && DataOperation.INSERT != op) {
				LOGGER.warn("Invalid view parameters op={}, id={}", op, id);
			} else {
				this.loadData(op, id);
				return;
			}
		} catch (final @SuppressWarnings("unused") Exception e) {
			LOGGER.warn("Invalid view parameters op={}, id={}", op, id);
		}
		this.loadData(DataOperation.INSERT, null);
	}

	/**
	 * Checks for uncommitted changes before leaving the underlying view
	 * and asks the user whether or not save these changes.
	 *
	 * <p><u>
	 * Care must be taken not to forget calling the given
	 * {@code Runnable} callback in order to proceed further.
	 * </u></p>
	 *
	 * @param leave action to proceed further.
	 */
	@Override
	public void onLeave(final Runnable proceedCallback) {
		final T currentData = this.getData();
		if (currentData != null && this.isUnsaved(currentData)) {
			this.getDisplay().askUnsaved(currentData, this.getOperation(), doSave -> {
				if (doSave) {
					this.save(proceedCallback, Functions.NULL_RUNNABLE);
				} else {
					proceedCallback.run();
				}
			});
		} else {
			proceedCallback.run();
		}
	}

	/**
	 * Loads a specific data by its identifier.
	 *
	 * @implSpec
	 *           If overridden this method must either call
	 *           super.{@linkplain #loadData(DataOperation, String)} or,
	 *           in order, {@linkplain #setCurrentData(DataOperation, Object)}
	 *           and then {@linkplain #loadDataToDisplay(DataOperation, Object)}.
	 *
	 *           <pre>
	 *
	 *           // method override example:
	 *           protected void loadData(DataOperation operation, String id) {
	 *           	getDisplay().doSomethingBefore();
	 *           	super.loadData(operation, id);
	 *           	getDisplay().doSomethingAfter();
	 *           }
	 *           </pre>
	 *
	 * @param operation the operation to be performed on the data.
	 * @param id the data identifier.
	 *
	 * @see #setCurrentData(DataOperation, Object)
	 * @see #loadDataToDisplay(DataOperation, Object)
	 */
	public void loadData(final DataOperation operation, final String id) {
		final DataOperation op = ObjectUtils.ifNull(operation, DataOperation.INSERT);
		T data;
		if (DataOperation.INSERT == op) {
			data = this.createInsertData();
		} else {
			data = id != null ? this.findData(id) : null;
		}
		this.setCurrentData(op, data);
	}

	/**
	 * Tells the presenter the current managed data
	 * and the operation that is being performed on
	 * that data. Then passes these parameters
	 * to the UI.
	 *
	 * @implSpec Can be used, for example, to change the kind
	 *           of the operation if the one being set is invalid
	 *           or to load data associated to the data being set.
	 *           <br>
	 *           NOTE: the super method must be called to ensure
	 *           that the correct data will be passed to the display
	 *           implementation and saved at the end.
	 *
	 *           <pre>
	 *
	 *           // method override example
	 *           protected void setCurrentData(DataOperation operation, MyData data) {
	 *           	// do not permit edit operations
	 *           	super.setCurrentData(DataOperation.EDIT == operation ? DataOperation.VIEW : operation, data);
	 *           	// load data details
	 *           	this.dataDetails = loadDetails(data);
	 *           	getDisplay().showDetails(this.dataDetails);
	 *           }
	 *           </pre>
	 *
	 * @param operation the operation to be performed on
	 *            the data.
	 * @param data the data to show. May be {@code null}.
	 */
	public void setCurrentData(final DataOperation operation, final T data) {
		this.operation = operation;
		this.data = data;
		this.getDisplay().readData(data, operation);
	}

	/**
	 * Saves the current data.
	 *
	 * @deprecated this method should not be called out of this class,
	 *             use {@link #save(Runnable, Runnable)} instead.
	 */
	@Deprecated
	public void save() {
		this.save(Functions.NULL_RUNNABLE, Functions.NULL_RUNNABLE);
	}

	/**
	 * Saves the current data.
	 *
	 * <p><u>
	 * Care must be taken not to forget calling the given
	 * {@code Runnable} callbacks in order to proceed further.
	 * </u></p>
	 *
	 * @param proceedCallback callback {@code Runnable} called
	 *            if the save action succeeds.
	 * @param errorCallback callback {@code Runnable} called
	 *            if the save action fails for any reason.
	 *
	 * @implNote By default inserts or updates the data
	 *           based on the current operation.
	 * @throws IllegalStateException in case of invalid current operation.
	 */
	public void save(final Runnable proceedCallback, final Runnable errorCallback) {
		if (DataOperation.INSERT == this.operation) {
			this.insert(this.data, proceedCallback, errorCallback);
		} else if (DataOperation.EDIT == this.operation) {
			this.update(this.data, proceedCallback, errorCallback);
		} else {
			throw new IllegalStateException("Invalid operation " + this.operation);
		}
	}

	/**
	 * Adds an observer for save events.
	 *
	 * @param observer the {@code BiConsumer} observer to add, not {@code null}.
	 *            The observer will be called passing the saved data and
	 *            the save operation.
	 */
	public void addAfterSaveObserver(final @NonNull BiConsumer<T, DataOperation> observer) {
		requireNonNull(observer, "Observer cannot be null");
		if (this.afterSaveObservers == null) {
			this.afterSaveObservers = new ArrayList<>();
		}
		this.afterSaveObservers.add(observer);
	}

	private void insert(final T _data, final @NonNull Runnable proceedCallback, final @NonNull Runnable errorCallback) {
		assert DataOperation.INSERT == this.operation;
		if (_data == null) {
			throw new IllegalStateException("No data object set");
		}
		if (!this.getDisplay().writeData(_data)) {
			errorCallback.run();
			return;
		}
		this.beforeSave(_data, DataOperation.INSERT, () -> {
			final T insertedData;
			try {
				insertedData = this.doInsert(_data);
			} catch (final Exception ex) {
				this.getDisplay().onFailedSave(_data, DataOperation.INSERT, ex, errorCallback);
				return;
			}
			if (insertedData != null) {
				this.fireAfterSaveEvent(insertedData, DataOperation.INSERT);
				this.afterSave(insertedData, DataOperation.INSERT, proceedCallback);
			} else {
				proceedCallback.run();
			}
		});
	}

	private void update(final T _data, final @NonNull Runnable proceedCallback, final @NonNull Runnable errorCallback) {
		assert DataOperation.EDIT == this.operation;
		if (_data == null) {
			throw new IllegalStateException("No data object set");
		}
		if (!this.getDisplay().writeData(_data)) {
			errorCallback.run();
			return;
		}
		this.beforeSave(_data, DataOperation.EDIT, () -> {
			try {
				this.doUpdate(_data);
			} catch (final Exception ex) {
				this.getDisplay().onFailedSave(_data, DataOperation.EDIT, ex, errorCallback);
				return;
			}
			this.fireAfterSaveEvent(_data, DataOperation.EDIT);
			this.afterSave(_data, DataOperation.EDIT, proceedCallback);
		});
	}

	/**
	 * Finds the data from the string representation
	 * of its identifier.
	 *
	 * @param key identifier as string.
	 * @return the data found by its identifier or {@code null}
	 *         if the data cannot be read or found.
	 */
	protected abstract T findData(@NonNull String key);

	/**
	 * Prepares new data for an insert operation.
	 */
	protected abstract T createInsertData();

	/**
	 * Checks for uncommitted changes on the given data bean.
	 */
	protected boolean isUnsaved(final T data) {
		return this.getDisplay().isUnsaved(data);
	}

	/**
	 * Performs an insert operation on the data.
	 *
	 * @return the inserted data or {@code null} if
	 *         the data has not been inserted.
	 */
	protected abstract T doInsert(T data) throws Exception;

	/**
	 * Performs an update operation on the data.
	 */
	protected abstract void doUpdate(T data) throws Exception;

	/**
	 * Performs something (e.g. validation or questions to the user) before
	 * saving the data.
	 *
	 * @param data the data to be saved.
	 * @param saveOperation the kind of operation to be performed
	 *            on the data.
	 * @param callback confirmation callback function. If not
	 *            called the data won't be saved.
	 */
	protected void beforeSave(final @NonNull T data, final @NonNull DataOperation saveOperation, final @NonNull Runnable callback) {
		callback.run();
	}

	/**
	 * Performs operations after a successful data save.
	 *
	 * @implNote By default this calls {@code getDisplay().saved(T, DataOperation)}.
	 *
	 * @param data the inserted data.
	 */
	protected void afterSave(final T data, final DataOperation saveOperation, final @NonNull Runnable callback) {
		this.getDisplay().onSuccessfulSave(data, saveOperation, callback);
	}

	/**
	 * Dispatches the event to all listeners registered for
	 * saving operations.
	 *
	 * @param data the saved data.
	 * @param saveOperation the operation.
	 */
	protected void fireAfterSaveEvent(final T data, final DataOperation saveOperation) {
		if (this.afterSaveObservers != null) {
			this.afterSaveObservers.forEach(observer -> observer.accept(data, saveOperation));
		}
	}
}
