package com.azserve.azframework.presenter;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.display.DataListDisplay;
import com.azserve.azframework.display.Display.Type;
import com.azserve.azframework.display.parameter.DisplayParameter;

public abstract class DataListPresenter<I, D extends DataListDisplay<I>> extends AbstractPresenter<D> {

	/**
	 * Type of operation on the data.
	 */
	public enum DataOperation {
		INSERT,
		EDIT,
		VIEW,
		DELETE;
	}

	@Override
	void configureDisplayFirstTime() {
		super.configureDisplayFirstTime();
		if (this.isOperationAllowed(DataOperation.INSERT)) {
			this.getDisplay().setOnInsert(this::openInsert);
		}
		if (this.isOperationAllowed(DataOperation.EDIT)) {
			this.getDisplay().setOnEdit(this::openEdit);
		}
		if (this.isOperationAllowed(DataOperation.VIEW)) {
			this.getDisplay().setOnView(this::openView);
		}
		if (this.isOperationAllowed(DataOperation.DELETE)) {
			this.getDisplay().setOnDelete(this::openDelete);
		}
	}

	/**
	 * Retrieves the items and shows them through the display.
	 */
	protected void loadItems() {
		this.getDisplay().setItems(this.retrieveItems());
	}

	/**
	 * Retrieves the items that must be shown.
	 */
	protected abstract List<I> retrieveItems();

	protected void openInsert() {
		this.getDisplay().openData(null, this.createParameters(DataOperation.INSERT, null));
	}

	protected void openEdit() {
		this.getOneSelectedItem().ifPresent(this::openEdit);
	}

	protected void openEdit(final @NonNull I item) {
		this.getDisplay().openData(item, this.createParameters(DataOperation.EDIT, item));
	}

	protected void openView() {
		this.getOneSelectedItem().ifPresent(this::openView);
	}

	protected void openView(final @NonNull I item) {
		this.getDisplay().openData(item, this.createParameters(DataOperation.VIEW, item));
	}

	protected void openDelete() {
		this.getOneSelectedItem().ifPresent(this::openDelete);
	}

	private void openDelete(final I item) {
		this.getDisplay().askDelete(item, () -> {
			try {
				this.doDelete(item);
				this.getDisplay().onSuccessfulDelete(item);
				this.loadItems();
			} catch (final Exception ex) {
				this.getDisplay().showError(ex);
			}
		});
	}

	protected final Optional<I> getOneSelectedItem() {
		final Collection<I> items = this.getDisplay().getSelectedItems();
		if (items.isEmpty()) {
			this.getDisplay().showMessage("Nessun dato selezionato", Type.ERROR);
			return Optional.empty();
		}
		if (items.size() != 1) {
			this.getDisplay().showMessage("Selezionare un solo dato", Type.ERROR);
			return Optional.empty();
		}
		return Optional.of(items.iterator().next());
	}

	protected abstract @NonNull String getItemKeyAsString(final @NonNull I item);

	protected abstract void doDelete(final @NonNull I item) throws Exception;

	/**
	 * Decides if the display implementation allows the specified operation,
	 * it is called before reading the parameters.
	 *
	 * @param operation
	 * @return
	 */
	protected boolean isOperationAllowed(final DataOperation operation) {
		return true;
	}

	/**
	 * Generates the parameters necessary to open a data
	 * through the display.
	 *
	 * @param operation kind of operation on the data
	 * @param item the data to open (or null in case of insert operation)
	 * @return the generated parameters
	 */
	protected Map<String, List<String>> createParameters(final @NonNull DataOperation operation, final I item) {
		if (item == null) {
			return DisplayParameter.build(DataPresenter.PARAM_OPERATION, operation);
		}
		return DisplayParameter.build(
				DataPresenter.PARAM_OPERATION, operation,
				DataPresenter.PARAM_ID, this.getItemKeyAsString(item));
	}
}
