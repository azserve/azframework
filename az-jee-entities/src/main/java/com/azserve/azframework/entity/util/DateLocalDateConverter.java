package com.azserve.azframework.entity.util;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class DateLocalDateConverter implements AttributeConverter<LocalDate, Date> {

	@Override
	public Date convertToDatabaseColumn(final LocalDate ld) {
		return ld == null ? null : Date.valueOf(ld);
	}

	@Override
	public LocalDate convertToEntityAttribute(final Date d) {
		return d == null ? null : d.toLocalDate();
	}
}
