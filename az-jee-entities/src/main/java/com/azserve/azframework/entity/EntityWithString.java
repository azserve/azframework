package com.azserve.azframework.entity;

import java.util.Objects;

public abstract class EntityWithString extends AbstractEntity<String> {

	private static final long serialVersionUID = 6064703216690471287L;

	protected abstract String getId1();

	@Override
	public final String getId() {
		final String id = this.getId1();
		return id != null ? id.trim() : null;
	}

	@Override
	public String getIdAsString() {
		return this.getId();
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || !obj.getClass().equals(this.getClass())) {
			return false;
		}
		final String id1 = this.getId();
		final String id2 = ((EntityWithString)obj).getId();
		return Objects.equals(id1 == null ? null : id1.toLowerCase(), id2 == null ? null : id2.toLowerCase());
	}

	@Override
	public int hashCode() {
		final String id = this.getId();
		return id == null ? 0 : id.toLowerCase().hashCode();
	}
}
