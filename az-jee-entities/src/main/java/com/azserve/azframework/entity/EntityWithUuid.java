package com.azserve.azframework.entity;

import java.util.Objects;
import java.util.UUID;

/**
 * Interfaccia per implementare i metodi
 * getGuid() e setGuid(UUID uuid) come in ModelWithGuid
 *
 * getPrimaryKey() serve per costruire l'oggetto chiave nella generazione dei messaggi
 *
 * @author luca
 *
 * @param <K> il tipo di chiave dell'entity.
 */
public abstract class EntityWithUuid<K extends UuidPk> implements IEntity<K> {

	private static final long serialVersionUID = -1229938481346069668L;

	public UUID getGuid() {
		final K pk = this.getId();
		return pk == null ? null : pk.getUuid();
	}

	public void setGuid(final UUID uuid) {
		if (this.getId() == null) {
			this.setId(this.newPrimaryKeyInstance());
		}
		this.getId().setUuid(uuid);
	}

	protected abstract K newPrimaryKeyInstance();

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " " + this.getId();
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || !obj.getClass().equals(this.getClass())) {
			return false;
		}
		return Objects.equals(this.getGuid(), ((EntityWithUuid<?>) obj).getGuid());
	}

	@Override
	public int hashCode() {
		final UUID uuid = this.getGuid();
		return uuid == null ? 0 : uuid.hashCode();
	}

	@Override
	public String getIdAsString() {
		return this.getGuid() + "";
	}
}
