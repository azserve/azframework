package com.azserve.azframework.entity;

import java.util.UUID;

import com.azserve.azframework.common.lang.NumberUtils;

public abstract class UuidPk implements PK {

	private static final long serialVersionUID = 7371420494841441342L;

	public UuidPk() {}

	public UuidPk(final UUID uuid) {
		this.setUuid(uuid);
	}

	protected abstract Long getLsbId();

	protected abstract Long getMsbId();

	protected abstract void setLsbId(Long lsbId);

	protected abstract void setMsbId(Long msbId);

	public UUID getUuid() {
		return new UUID(NumberUtils.removeNull(this.getMsbId()), NumberUtils.removeNull(this.getLsbId()));
	}

	public void setUuid(final UUID uuid) {
		if (uuid == null) {
			this.setLsbId(null);
			this.setMsbId(null);
		} else {
			this.setLsbId(Long.valueOf(uuid.getLeastSignificantBits()));
			this.setMsbId(Long.valueOf(uuid.getMostSignificantBits()));
		}
	}

	@Override
	public boolean equals(final Object other) {
		if (this == other) {
			return true;
		}
		if (other == null) {
			return false;
		}
		if (!(other.getClass().equals(this.getClass()))) {
			return false;
		}
		final UuidPk castOther = (UuidPk) other;
		return this.getLsbId().equals(castOther.getLsbId()) && this.getMsbId().equals(castOther.getMsbId());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + NumberUtils.hashCode(this.getLsbId());
		hash = hash * prime + NumberUtils.hashCode(this.getMsbId());

		return hash;
	}

	@Override
	public String toString() {
		return this.getUuid() + "";
	}

}
