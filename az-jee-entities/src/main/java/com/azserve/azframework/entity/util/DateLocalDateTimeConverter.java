package com.azserve.azframework.entity.util;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class DateLocalDateTimeConverter implements AttributeConverter<LocalDateTime, Timestamp> {

	@Override
	public Timestamp convertToDatabaseColumn(final LocalDateTime lt) {
		return lt == null ? null : Timestamp.valueOf(lt);
	}

	@Override
	public LocalDateTime convertToEntityAttribute(final Timestamp d) {
		return d == null ? null : d.toLocalDateTime();
	}
}