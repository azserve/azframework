package com.azserve.azframework.entity;

import java.io.Serializable;
import java.util.Objects;

public abstract class AbstractEntity<T extends Serializable> implements IEntity<T> {

	private static final long serialVersionUID = 2832277855150063387L;

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || !obj.getClass().equals(this.getClass())) {
			return false;
		}
		return Objects.equals(this.getId(), ((IEntity<?>)obj).getId());
	}

	@Override
	public int hashCode() {
		final T id = this.getId();
		return id == null ? 0 : id.hashCode();
	}

	@Override
	public String getIdAsString() {
		return this.getId() + "";
	}
}
