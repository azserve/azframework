package com.azserve.azframework.entity;

import java.util.UUID;

import javax.persistence.Embeddable;

@Embeddable
public class EntityUuid extends UuidPk {

	private static final long serialVersionUID = -2411688239691066230L;

	private Long idLsb;

	private Long idMsb;

	public EntityUuid() {

	}

	public EntityUuid(final UUID id) {
		this.setUuid(id);
	}

	@Override
	protected Long getLsbId() {
		return this.idLsb;
	}

	@Override
	protected Long getMsbId() {
		return this.idMsb;
	}

	@Override
	protected void setLsbId(final Long lsbId) {
		this.idLsb = lsbId;
	}

	@Override
	protected void setMsbId(final Long msbId) {
		this.idMsb = msbId;
	}
}
