package com.azserve.azframework.entity.util;

import java.sql.Time;
import java.time.LocalTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class DateLocalTimeConverter implements AttributeConverter<LocalTime, Time> {

	@Override
	public Time convertToDatabaseColumn(final LocalTime lt) {
		return lt == null ? null : Time.valueOf(lt);
	}

	@Override
	public LocalTime convertToEntityAttribute(final Time d) {
		return d == null ? null : d.toLocalTime();
	}
}
