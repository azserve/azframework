package com.azserve.azframework.entity.logger;

import com.azserve.azframework.entity.IEntity;

public interface IEntityLogger {

	void log(IEntity<?> entity, String operation);
}
