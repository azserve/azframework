package com.azserve.azframework.entity.util;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.metamodel.ManagedType;
import javax.persistence.metamodel.Metamodel;
import javax.persistence.metamodel.Type.PersistenceType;

import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.entity.AbstractEntity;
import com.azserve.azframework.entity.EntityWithLong;
import com.azserve.azframework.entity.EntityWithUuid;
import com.azserve.azframework.entity.IEntity;

/**
 * Classe di utilità per le entity.
 *
 * @author filippo
 * @since 02/04/2014
 */
public final class EntityUtils {

	/**
	 * Fornisce un insieme di tutte le classi
	 * che rappresentano entity.
	 *
	 * @param entityManager gestore delle entity.
	 */
	public static Set<Class<?>> getEntityClasses(final EntityManager entityManager) {
		final Metamodel metamodel = entityManager.getMetamodel();
		final Set<ManagedType<?>> managedTypes = metamodel.getManagedTypes();
		final Set<Class<?>> result = new HashSet<>();
		for (final ManagedType<?> managedType : managedTypes) {
			if (managedType.getPersistenceType() == PersistenceType.ENTITY) {
				result.add(managedType.getJavaType());
			}
		}
		return result;
	}

	/**
	 * Restituisce l'id di una entity.
	 * Se la entity è {@code null} restituisce {@code null}.
	 *
	 * @param entity entity da verificare.
	 */
	public static <T extends Serializable> T getId(final @Nullable AbstractEntity<T> entity) {
		return entity != null ? entity.getId() : null;
	}

	/**
	 * Restituisce la chiave di una entity come stringa.
	 * Se la entity è {@code null} restituisce {@code null}.
	 *
	 * @param entity entity da verificare.
	 */
	public static String getIdAsString(final @Nullable IEntity<?> entity) {
		return entity != null ? entity.getIdAsString() : null;
	}

	/**
	 * Restituisce l'id di una entity.
	 * Se la entity è {@code null} restituisce {@code null}.
	 *
	 * @param entity entity da verificare.
	 */
	public static Long getId(final @Nullable EntityWithLong entity) {
		return getId(entity, null);
	}

	/**
	 * Restituisce l'id di una entity.
	 * Se la entity è {@code null} restituisce
	 * un valore di default.
	 *
	 * @param entity entity da verificare.
	 */
	public static Long getId(final @Nullable EntityWithLong entity, final @Nullable Long defaultValue) {
		return entity != null ? entity.getId() : defaultValue;
	}

	/**
	 * Restituisce l'uuid di una entity.
	 * Se la entity è {@code null} restituisce {@code null}.
	 *
	 * @param entity entity da verificare.
	 */
	public static UUID getUuid(final @Nullable EntityWithUuid<?> entity) {
		return entity != null ? entity.getGuid() : null;
	}

	private EntityUtils() {}
}
