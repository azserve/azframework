package com.azserve.azframework.entity;

import java.io.Serializable;

public interface IEntity<T extends Serializable> extends Serializable {

	T getId();

	void setId(T id);

	String getIdAsString();

}
