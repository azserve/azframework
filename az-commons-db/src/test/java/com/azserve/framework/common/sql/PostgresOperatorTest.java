package com.azserve.framework.common.sql;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class PostgresOperatorTest {

	private static Connection connection;

	@BeforeClass
	public static void setUpBeforeClass() throws ClassNotFoundException, SQLException {
		connection = OperatorTest.setUpBeforeClass("org.postgresql.Driver", "jdbc:postgresql://db-postgres:5432/azframework", "azframework", "azframework");
	}

	@After
	public void tearDown() throws SQLException {
		OperatorTest.tearDown(connection);
	}

	@AfterClass
	public static void tearDownAfterClass() throws SQLException {
		OperatorTest.tearDownAfterClass(connection);
	}

	@Test
	public void testExistsColumn() throws SQLException {
		OperatorTest.testExistsColumn(connection);
	}

	@Test
	public void testCreateColumn() throws SQLException {
		OperatorTest.testCreateColumn(connection);
	}

	@Test
	public void testCreateNonNullColumn() throws SQLException {
		OperatorTest.testCreateNonNullColumn(connection);
	}

	@Test
	public void testEditColumn() throws SQLException {
		OperatorTest.testEditColumn(connection);
	}

	@Test
	public void testDropColumn() throws SQLException {
		OperatorTest.testDropColumn(connection);
	}

	@Test
	public void testExistsRenameDropIndex() throws SQLException {
		OperatorTest.testExistsRenameDropIndex(connection);
	}

	@Test
	public void testCreateIndex() throws SQLException {
		OperatorTest.testCreateIndex(connection);
	}

	@Test
	public void testExistsRenameDropForeignKey() throws SQLException {
		OperatorTest.testExistsRenameDropForeignKey(connection);
	}

	@Test
	public void testCreateForeignKey() throws SQLException {
		OperatorTest.testCreateForeignKey(connection);
	}

	@Test
	public void testExistsConstraint() throws SQLException {
		OperatorTest.testExistsConstraint(connection, false, false);
	}
}
