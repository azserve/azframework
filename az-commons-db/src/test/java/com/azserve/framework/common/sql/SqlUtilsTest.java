package com.azserve.framework.common.sql;

import static com.azserve.azframework.common.sql.SqlUtils.copyTable;
import static com.azserve.azframework.common.sql.SqlUtils.count;
import static com.azserve.azframework.common.sql.SqlUtils.execute;
import static com.azserve.azframework.common.sql.SqlUtils.executeAndGet;
import static com.azserve.azframework.common.sql.SqlUtils.executeAndGetFirst;
import static com.azserve.azframework.common.sql.SqlUtils.executeAndGetSingleResult;
import static com.azserve.azframework.common.sql.SqlUtils.executeQuery;
import static com.azserve.azframework.common.sql.SqlUtils.executeTransactionalUpdates;
import static com.azserve.azframework.common.sql.SqlUtils.executeUpdate;
import static com.azserve.azframework.common.sql.SqlUtils.executeUpdates;
import static com.azserve.azframework.common.sql.SqlUtils.exists;
import static com.azserve.azframework.common.sql.SqlUtils.firstBoolean;
import static com.azserve.azframework.common.sql.SqlUtils.firstString;
import static com.azserve.azframework.common.sql.SqlUtils.formatArguments;
import static com.azserve.azframework.common.sql.SqlUtils.in;
import static com.azserve.azframework.common.sql.SqlUtils.openConnection;
import static com.azserve.azframework.common.sql.SqlUtils.transaction;
import static com.azserve.azframework.common.test.TestUtils.assertThrows;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.azserve.azframework.common.lang.StringUtils.StringNotValorizedException;
import com.azserve.azframework.common.sql.DatabaseOperator;
import com.azserve.azframework.common.sql.SqlUtils;
import com.azserve.azframework.common.test.TestUtils;

public class SqlUtilsTest {

	private static Connection connection;

	private static final String TABLE_NAME = "table1";
	private static final String COL_PK_NAME = "id";
	private static final String COL1_NAME = "col1";
	private static final String COL2_NAME = "col2";

	@BeforeClass
	public static void setUpBeforeClass() throws ClassNotFoundException, SQLException {
		connection = openConnection("org.apache.derby.jdbc.EmbeddedDriver", "jdbc:derby:/tmp/azframework;create=true", "", "");
		dropTables(connection);
	}

	@AfterClass
	public static void tearDownAfterClass() throws SQLException {
		connection.close();
	}

	@Before
	public void setUp() throws SQLException {
		execute(SqlUtilsTest.connection, "create table " + TABLE_NAME + "("
				+ COL_PK_NAME + " int primary key,"
				+ COL1_NAME + " int,"
				+ COL2_NAME + " varchar(10))");
	}

	@After
	public void tearDown() throws SQLException {
		dropTables(connection);
	}

	private static void dropTables(final Connection connectionToClean) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connectionToClean);
		List<String> tables = operator.readTables();
		final int size = tables.size();
		int i = 0;
		while (!(tables = operator.readTables()).isEmpty()) {
			for (final String table : tables) {
				if (operator.existsTable(table)) {
					try {
						execute(connectionToClean, "drop table " + table);
					} catch (@SuppressWarnings("unused") final Exception e) {
						// dependency problems
					}
				}
			}
			i++;
			if (i > size) {
				throw new RuntimeException("Loop!");
			}
		}
	}

	@Test
	public void constructor() throws Throwable {
		TestUtils.testUtilsClassDefinition(SqlUtils.class);
	}

	@Test
	public void testExecute() throws SQLException {
		assertFalse(execute(connection, "insert into " + TABLE_NAME + " values (1,null,'foo')"));
		assertTrue(execute(connection, "select * from " + TABLE_NAME));
		assertThrows(SQLException.class, true, () -> execute(connection, "select * from NON_EXISTING_TABLE"));
	}

	@Test
	public void testExecuteUpdate() throws SQLException {
		assertEquals(1, executeUpdate(connection, "insert into " + TABLE_NAME + " values (1,null,'foo')"));
		assertEquals(1, executeUpdate(connection, "insert into " + TABLE_NAME + " values (2,null,'foo')"));
		assertEquals(2, executeUpdate(connection, "update " + TABLE_NAME + " set " + COL1_NAME + " = 0"));
		assertEquals(2, executeUpdate(connection, "update " + TABLE_NAME + " set " + COL1_NAME + " = ?", Integer.valueOf(1)));
		assertEquals(1, executeUpdate(connection, "update " + TABLE_NAME + " set " + COL1_NAME + " = ? where " + COL_PK_NAME + " = ?", Integer.valueOf(3), Integer.valueOf(1)));
		final List<String> results = executeAndGet(connection, "select * from " + TABLE_NAME + " order by " + COL_PK_NAME);
		assertEquals(2, results.size());
		assertEquals("1 3 foo", results.get(0));
		assertEquals("2 1 foo", results.get(1));
	}

	@Test
	public void testExecuteUpdates() throws SQLException {
		assertEquals(0, executeUpdates(connection));
		assertEquals(4, executeUpdates(connection,
				"insert into " + TABLE_NAME + " values (1,null,'foo')",
				"insert into " + TABLE_NAME + " values (2,null,'foo')",
				"update " + TABLE_NAME + " set " + COL1_NAME + " = 0",
				"update " + TABLE_NAME + " set " + COL1_NAME + " = 0 where " + COL_PK_NAME + " = 0"));
		final List<String> results = executeAndGet(connection, "select * from " + TABLE_NAME + " order by " + COL_PK_NAME);
		assertEquals(2, results.size());
		assertEquals("1 0 foo", results.get(0));
		assertEquals("2 0 foo", results.get(1));
	}

	@Test
	public void testExecuteQuery() throws SQLException {
		assertEquals(2, executeUpdates(connection,
				"insert into " + TABLE_NAME + " values (1,null,'foo')",
				"insert into " + TABLE_NAME + " values (2,3,'bar')"));
		{
			final List<Object[]> result = new ArrayList<>();
			executeQuery(connection, "select * from " + TABLE_NAME + " order by " + COL_PK_NAME, r -> {
				final Object[] v = new Object[3];
				for (int i = 0; i < v.length; i++) {
					v[i] = r.getObject(i + 1);
				}
				result.add(v);
			});
			assertEquals(2, result.size());
			assertArrayEquals(new Object[] { Integer.valueOf(1), null, "foo" }, result.get(0));
			assertArrayEquals(new Object[] { Integer.valueOf(2), Integer.valueOf(3), "bar" }, result.get(1));
		}
		{
			executeQuery(connection, "select * from " + TABLE_NAME + " where " + COL_PK_NAME + " = ?", r -> {
				fail();
			}, Integer.valueOf(-1));
		}
		{
			final List<Object[]> result = new ArrayList<>();
			executeQuery(connection, "select * from " + TABLE_NAME + " where " + COL1_NAME + "= ? and " + COL2_NAME + " = ?", r -> {
				final Object[] v = new Object[3];
				for (int i = 0; i < v.length; i++) {
					v[i] = r.getObject(i + 1);
				}
				result.add(v);
			}, Integer.valueOf(3), "bar");
			assertEquals(1, result.size());
			assertArrayEquals(new Object[] { Integer.valueOf(2), Integer.valueOf(3), "bar" }, result.get(0));
		}
	}

	@Test
	public void testExecuteTransactionalUpdates() throws SQLException {
		assertEquals(0, executeTransactionalUpdates(connection));
		assertEquals(2, executeTransactionalUpdates(connection,
				"insert into " + TABLE_NAME + " values (1,null,'foo')",
				"insert into " + TABLE_NAME + " values (2,3,'bar')"));

		assertThrows(SQLException.class, true, () -> executeTransactionalUpdates(connection,
				"insert into " + TABLE_NAME + " values (3,null,'xxx')",
				"insert into " + TABLE_NAME + " values (4,null,'yyy')",
				"insert into " + TABLE_NAME + " values (4,null,'www')", // duplicate PK
				"insert into " + TABLE_NAME + " values (5,null,'zzz')"));
		{
			final List<String> result = executeAndGet(connection, "select * from " + TABLE_NAME + " order by " + COL_PK_NAME);
			assertEquals(2, result.size());
			assertEquals("1 null foo", result.get(0));
			assertEquals("2 3 bar", result.get(1));
		}
	}

	@Test
	public void testExecuteAndGet() throws SQLException {
		assertEquals(2, executeUpdates(connection,
				"insert into " + TABLE_NAME + " values (1,null,'foo')",
				"insert into " + TABLE_NAME + " values (2,3,'bar')"));
		{
			final List<String> result = executeAndGet(connection, "select * from " + TABLE_NAME + " order by " + COL_PK_NAME);
			assertEquals(2, result.size());
			assertEquals("1 null foo", result.get(0));
			assertEquals("2 3 bar", result.get(1));
		}
		{
			final List<String> result = executeAndGet(connection, "select " + COL_PK_NAME + " from " + TABLE_NAME);
			assertEquals(2, result.size());
			assertEquals("1", result.get(0));
			assertEquals("2", result.get(1));
		}
		{
			final List<String> result = executeAndGet(connection, "select * from " + TABLE_NAME + " where " + COL_PK_NAME + " = -1");
			assertEquals(0, result.size());
		}
		{
			final List<String> result = new ArrayList<>();
			executeAndGet(connection, "select * from " + TABLE_NAME + " order by " + COL_PK_NAME, result);
			assertEquals(2, result.size());
			assertEquals("1 null foo", result.get(0));
			assertEquals("2 3 bar", result.get(1));
		}
		{
			final List<String> result = new ArrayList<>();
			executeAndGet(connection, "select * from " + TABLE_NAME + " where " + COL_PK_NAME + " = -1", result);
			assertEquals(0, result.size());
		}
		{
			final List<String> result = executeAndGet(connection, "select * from " + TABLE_NAME + " order by " + COL_PK_NAME, r -> r.getObject(1) + "*" + r.getObject(2) + "*" + r.getObject(3));
			assertEquals(2, result.size());
			assertEquals("1*null*foo", result.get(0));
			assertEquals("2*3*bar", result.get(1));
		}
		{
			final List<String> result = executeAndGet(connection, "select * from " + TABLE_NAME + " where " + COL_PK_NAME + " = -1", r -> r.getObject(1) + "*" + r.getObject(2) + "*" + r.getObject(3));
			assertEquals(0, result.size());
		}
	}

	@Test
	public void testExecuteAndGetFirst() throws SQLException {
		assertEquals(2, executeUpdates(connection,
				"insert into " + TABLE_NAME + " values (1,null,'foo')",
				"insert into " + TABLE_NAME + " values (2,3,'bar')"));
		{
			assertEquals("foo", executeAndGetFirst(connection, "select * from " + TABLE_NAME + " order by " + COL_PK_NAME, COL2_NAME));
			assertNull(executeAndGetFirst(connection, "select * from " + TABLE_NAME + " order by " + COL_PK_NAME, COL1_NAME));
			assertNull(executeAndGetFirst(connection, "select * from " + TABLE_NAME + " where " + COL_PK_NAME + "= -1", COL1_NAME));
		}

	}

	@Test
	public void testFirstString() throws SQLException {
		assertEquals(2, executeUpdates(connection,
				"insert into " + TABLE_NAME + " values (1,null,'foo')",
				"insert into " + TABLE_NAME + " values (2,3,null)"));
		{
			assertEquals("foo", firstString(connection, "select " + COL2_NAME + "," + COL1_NAME + " from " + TABLE_NAME + " order by " + COL_PK_NAME).get());
			assertFalse(firstString(connection, "select " + COL2_NAME + "," + COL1_NAME + " from " + TABLE_NAME + " where " + COL_PK_NAME + " = ? order by " + COL_PK_NAME, Integer.valueOf(2)).isPresent());
			assertFalse(firstString(connection, "select " + COL2_NAME + "," + COL1_NAME + " from " + TABLE_NAME + " where " + COL_PK_NAME + "= ?", Integer.valueOf(-1)).isPresent());
			assertThrows(SQLException.class, true, () -> firstString(connection, "select " + COL1_NAME + "," + COL2_NAME + " from " + TABLE_NAME + " from " + TABLE_NAME + " where " + COL_PK_NAME + "= ?", Integer.valueOf(2)));
		}

	}

	@Test
	public void testFirstBoolean() throws SQLException {
		assertEquals(2, executeUpdates(connection,
				"insert into " + TABLE_NAME + " values (1,1,'0')",
				"insert into " + TABLE_NAME + " values (2,0,null)"));
		assertTrue(firstBoolean(connection, "select " + COL1_NAME + "," + COL2_NAME + " from " + TABLE_NAME + " order by " + COL_PK_NAME).get().booleanValue());
		assertFalse(firstBoolean(connection, "select " + COL2_NAME + "," + COL1_NAME + " from " + TABLE_NAME + " order by " + COL_PK_NAME).get().booleanValue());
		assertFalse(firstBoolean(connection, "select " + COL1_NAME + "," + COL2_NAME + " from " + TABLE_NAME + " where " + COL_PK_NAME + " = ? order by " + COL_PK_NAME, Integer.valueOf(2)).get().booleanValue());
		assertFalse(firstBoolean(connection, "select " + COL2_NAME + "," + COL1_NAME + " from " + TABLE_NAME + " where " + COL_PK_NAME + " = ? order by " + COL_PK_NAME, Integer.valueOf(2)).isPresent());
		assertFalse(firstBoolean(connection, "select " + COL2_NAME + "," + COL1_NAME + " from " + TABLE_NAME + " where " + COL_PK_NAME + " = ? order by " + COL_PK_NAME, Integer.valueOf(3)).isPresent());
	}

	@Test
	public void testExecuteAndGetSingleResult() throws SQLException {
		assertEquals(2, executeUpdates(connection,
				"insert into " + TABLE_NAME + " values (1,1,'0')",
				"insert into " + TABLE_NAME + " values (2,0,null)"));
		assertEquals(Integer.valueOf(1), executeAndGetSingleResult(connection, "select " + COL1_NAME + "," + COL2_NAME + " from " + TABLE_NAME + " order by " + COL_PK_NAME, Integer.class));
		assertEquals("0", executeAndGetSingleResult(connection, "select " + COL2_NAME + "," + COL1_NAME + " from " + TABLE_NAME + " order by " + COL_PK_NAME, String.class));
		assertEquals(Integer.valueOf(0), executeAndGetSingleResult(connection, "select " + COL1_NAME + "," + COL2_NAME + " from " + TABLE_NAME + " where " + COL_PK_NAME + " = 2 order by " + COL_PK_NAME, Integer.class));
		assertNull(executeAndGetSingleResult(connection, "select " + COL2_NAME + "," + COL1_NAME + " from " + TABLE_NAME + " where " + COL_PK_NAME + " = 2 order by " + COL_PK_NAME, String.class));
		assertNull(executeAndGetSingleResult(connection, "select " + COL2_NAME + "," + COL1_NAME + " from " + TABLE_NAME + " where " + COL_PK_NAME + " = 3 order by " + COL_PK_NAME, String.class));
	}

	@Test
	public void testExists() throws SQLException {
		assertEquals(2, executeUpdates(connection,
				"insert into " + TABLE_NAME + " values (1,1,'0')",
				"insert into " + TABLE_NAME + " values (2,0,null)"));
		assertTrue(exists(connection, "select " + COL1_NAME + "," + COL2_NAME + " from " + TABLE_NAME + " order by " + COL_PK_NAME));
		assertTrue(exists(connection, "select " + COL1_NAME + "," + COL2_NAME + " from " + TABLE_NAME + " where " + COL_PK_NAME + " = ? order by " + COL_PK_NAME, Integer.valueOf(2)));
		assertFalse(exists(connection, "select " + COL2_NAME + "," + COL1_NAME + " from " + TABLE_NAME + " where " + COL_PK_NAME + " = ? order by " + COL_PK_NAME, Integer.valueOf(3)));

	}

	@Test
	public void testCount() throws SQLException {
		assertEquals(0, count(connection, TABLE_NAME));
		assertEquals(2, executeUpdates(connection,
				"insert into " + TABLE_NAME + " values (1,1,'0')",
				"insert into " + TABLE_NAME + " values (2,0,null)"));
		assertEquals(2, count(connection, TABLE_NAME));
		assertThrows(StringNotValorizedException.class, () -> count(connection, ""));
	}

	@Test
	public void testIn() {
		assertThrows(IllegalArgumentException.class, () -> in());
		assertEquals("('a')", in("a"));
		assertEquals("('a','b')", in("a", "b"));
		assertThrows(IllegalArgumentException.class, () -> in("a", null));
		assertEquals("('foo''','foo''bar''''')", in("foo'", "foo'bar''"));
	}

	@Test
	public void testTransaction() throws SQLException {
		transaction(connection, c -> {});

		transaction(connection, c -> {
			execute(c, "insert into " + TABLE_NAME + " values (1,null,'foo')");
			execute(c, "insert into " + TABLE_NAME + " values (2,3,'bar')");
		});
		{
			final List<String> result = executeAndGet(connection, "select * from " + TABLE_NAME + " order by " + COL_PK_NAME);
			assertEquals(2, result.size());
			assertEquals("1 null foo", result.get(0));
			assertEquals("2 3 bar", result.get(1));
		}
		assertThrows(SQLException.class, true, () -> transaction(connection, c -> {
			execute(c, "insert into " + TABLE_NAME + " values (3,null,'xxx')");
			execute(c, "insert into " + TABLE_NAME + " values (4,null,'yyy')");
			execute(c, "insert into " + TABLE_NAME + " values (4,null,'www')"); // duplicate PK
			execute(c, "insert into " + TABLE_NAME + " values (5,null,'zzz')");
		}));
		{
			final List<String> result = executeAndGet(connection, "select * from " + TABLE_NAME + " order by " + COL_PK_NAME);
			assertEquals(2, result.size());
			assertEquals("1 null foo", result.get(0));
			assertEquals("2 3 bar", result.get(1));
		}
		assertThrows(SQLException.class, true, () -> transaction(connection, c -> {
			execute(c, "insert into " + TABLE_NAME + " values (3,null,'xxx')");
			execute(c, "insert into " + TABLE_NAME + " values (4,null,'yyy')");
			transaction(connection, c1 -> {
				execute(c1, "insert into " + TABLE_NAME + " values (4,null,'www')"); // duplicate PK
				execute(c1, "insert into " + TABLE_NAME + " values (5,null,'zzz')");
			});
		}));
		assertThrows(SQLException.class, true, () -> transaction(connection, c -> {
			execute(c, "insert into " + TABLE_NAME + " values (3,null,'xxx')");
			execute(c, "insert into " + TABLE_NAME + " values (4,null,'yyy')");
			transaction(connection, c1 -> {
				throw new RuntimeException();
			});
		}));
		assertEquals(2, count(connection, TABLE_NAME));
		transaction(connection, c -> {
			execute(c, "insert into " + TABLE_NAME + " values (3,null,'xxx')");
			execute(c, "insert into " + TABLE_NAME + " values (4,null,'yyy')");
			transaction(connection, c1 -> {
				execute(c1, "insert into " + TABLE_NAME + " values (5,null,'www')");
				execute(c1, "insert into " + TABLE_NAME + " values (6,null,'zzz')");
			});
		});
		assertEquals(6, count(connection, TABLE_NAME));
	}

	@Test
	public void testCopyTable() throws SQLException, ClassNotFoundException {
		assertEquals(2, executeUpdates(connection,
				"insert into " + TABLE_NAME + " values (1,1,'0')",
				"insert into " + TABLE_NAME + " values (2,0,null)"));
		assertEquals(2, count(connection, TABLE_NAME));

		final String tableName2 = "table2";

		try (final Connection connection2 = openConnection("org.apache.derby.jdbc.EmbeddedDriver", "jdbc:derby:/tmp/azframework2;create=true", "", "")) {
			dropTables(connection2);
			// fields with different order
			execute(connection2, "create table " + tableName2 + "("
					+ COL_PK_NAME + " int primary key,"
					+ COL2_NAME + " varchar(10),"
					+ COL1_NAME + " int)");
			copyTable(connection, connection2, TABLE_NAME, tableName2);
			assertEquals(2, count(connection2, tableName2));

			// dest table doesn't exist
			execute(connection2, "drop table " + tableName2);
			assertThrows(SQLException.class, true, () -> copyTable(connection, connection2, TABLE_NAME, tableName2));

			// dest table with wrong metadata for a column
			execute(connection2, "create table " + tableName2 + "("
					+ COL_PK_NAME + " int primary key,"
					+ COL1_NAME + " varchar(10),"
					+ COL2_NAME + " varchar(10))");
			assertThrows(SQLException.class, () -> copyTable(connection, connection2, TABLE_NAME, tableName2));

			// dest table with wrong name for a column
			execute(connection2, "drop table " + tableName2);
			execute(connection2, "create table " + tableName2 + "("
					+ COL_PK_NAME + " int primary key,"
					+ "WRONG_NAME" + " int,"
					+ COL2_NAME + " varchar(10))");
			assertThrows(SQLException.class, () -> copyTable(connection, connection2, TABLE_NAME, tableName2));

			// dest table with wrong number of column
			execute(connection2, "drop table " + tableName2);
			execute(connection2, "create table " + tableName2 + "("
					+ COL_PK_NAME + " int primary key,"
					+ COL2_NAME + " varchar(10))");
			assertThrows(SQLException.class, () -> copyTable(connection, connection2, TABLE_NAME, tableName2));
			dropTables(connection2);
		}
	}

	@Test
	public void testFormatArguments() {
		assertEquals("()", formatArguments());
		assertEquals("(a)", formatArguments("a"));
		assertEquals("(a,b)", formatArguments("a", "b"));
	}

}
