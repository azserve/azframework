package com.azserve.framework.common.sql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

import com.azserve.azframework.common.lang.StringUtils.StringNotValorizedException;
import com.azserve.azframework.common.sql.DatabaseOperator;
import com.azserve.azframework.common.sql.DatabaseOperator.ConstraintType;
import com.azserve.azframework.common.sql.SqlUtils;
import com.azserve.azframework.common.test.TestUtils;

public class OperatorTest {

	public static Connection setUpBeforeClass(final String driver, final String url, final String user, final String password) throws ClassNotFoundException, SQLException {
		return SqlUtils.openConnection(driver, url, user, password);
	}

	public static void tearDown(final Connection connection) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connection);
		List<String> tables = operator.readTables();
		final int tableCount = tables.size();
		int i = 0;
		while (!tables.isEmpty()) {
			for (final String table : tables) {
				if (operator.existsTable(table)) {
					try {
						SqlUtils.execute(connection, "drop table " + table + "");
					} catch (@SuppressWarnings("unused") final Exception e) {
						// FK
					}
				}
			}
			if (i > tableCount) {
				throw new RuntimeException("Drop table loop");
			}
			i++;
			tables = operator.readTables();
		}
	}

	public static void tearDownAfterClass(final Connection connection) throws SQLException {
		connection.close();
	}

	public static void testExistsColumn(final Connection connection) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connection);
		final String tableName = "table1";
		final String columnIdName = "id";
		assertFalse(SqlUtils.execute(connection, "create table " + tableName + " (" + columnIdName + " int primary key)"));
		assertTrue(operator.existsColumn(tableName, columnIdName.toLowerCase()));
		assertTrue(operator.existsColumn(tableName, columnIdName.toUpperCase()));
		assertFalse(operator.existsColumn(tableName, "foo"));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> assertFalse(operator.existsColumn(tableName, nullString())));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> assertFalse(operator.existsColumn(nullString(), columnIdName)));
		assertFalse(operator.existsColumn(tableName + "X", "foo"));

	}

	public static void testCreateColumn(final Connection connection) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connection);
		final String tableName = "table1";
		final String columnIdName = "id";
		final String column2Name = "col2";
		assertFalse(SqlUtils.execute(connection, "create table " + tableName + " (" + columnIdName + " int primary key)"));
		assertFalse(SqlUtils.execute(connection, "insert into " + tableName + " values (1)"));

		assertTrue(operator.createColumn(tableName, column2Name, "varchar(255)", null));
		assertTrue(operator.existsColumn(tableName, column2Name));
		assertFalse(operator.createColumn(tableName, column2Name, "varchar(255)", null));
		assertTrue(operator.existsColumn(tableName, column2Name));
	}

	public static void testCreateNonNullColumn(final Connection connection) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connection);
		final String tableName = "table1";
		final String columnIdName = "id";
		final String column2Name = "col2";
		assertFalse(SqlUtils.execute(connection, "create table " + tableName + " (" + columnIdName + " int primary key)"));
		assertFalse(SqlUtils.execute(connection, "insert into " + tableName + " values (1)"));

		TestUtils.assertThrows(SQLException.class, true, () -> operator.createNotNullableColumn(tableName, column2Name, "varchar(255)", null, null));
		assertFalse(SqlUtils.execute(connection, "delete from " + tableName));

		assertTrue(operator.createNotNullableColumn(tableName, column2Name, "varchar(255)", null, null));
		assertTrue(operator.existsColumn(tableName, column2Name));
		assertFalse(operator.createNotNullableColumn(tableName, column2Name, "varchar(255)", null, null));
		assertTrue(operator.existsColumn(tableName, column2Name));

		TestUtils.assertThrows(SQLException.class, true, () -> SqlUtils.execute(connection, "insert into " + tableName + " (" + columnIdName + ") values (1)"));
		assertFalse(SqlUtils.execute(connection, "insert into " + tableName + " values (1, '')"));

		assertTrue(operator.dropColumn(tableName, column2Name));
		assertTrue(operator.createNotNullableColumn(tableName, column2Name, "varchar(255)", null, "foo"));
		final List<String> list1 = SqlUtils.executeAndGet(connection, "select " + columnIdName + ", " + column2Name + " from " + tableName);
		assertEquals(1, list1.size());
		assertEquals("1 foo", list1.get(0));
	}

	public static void testEditColumn(final Connection connection) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connection);
		final String tableName = "table1";
		final String columnIdName = "id";
		final String column2Name = "col2";
		final String column2renamed = "col3";
		assertFalse(SqlUtils.execute(connection, "create table " + tableName + " (" + columnIdName + " int primary key, " + column2Name + " varchar(5))"));
		assertFalse(operator.editColumn(tableName, column2Name + "X", "varchar(10)", column2Name));
		assertTrue(operator.editColumn(tableName, column2Name, "varchar(10)", column2Name));
		try (PreparedStatement ps = connection.prepareStatement("select " + column2Name + " from " + tableName);
				ResultSet rs = ps.executeQuery();) {
			assertEquals(10, rs.getMetaData().getPrecision(1));
		}
		assertTrue(operator.editColumn(tableName, column2Name, "varchar(20)", column2renamed));
		try (PreparedStatement ps = connection.prepareStatement("select " + column2renamed + " from " + tableName);
				ResultSet rs = ps.executeQuery();) {
			assertEquals(20, rs.getMetaData().getPrecision(1));
		}
		assertTrue(operator.editColumn(tableName, column2renamed, "varchar(30)", nullString()));
		try (PreparedStatement ps = connection.prepareStatement("select " + column2renamed + " from " + tableName);
				ResultSet rs = ps.executeQuery();) {
			assertEquals(30, rs.getMetaData().getPrecision(1));
		}

		operator.editColumn(tableName, column2renamed, "varchar(40)", false, nullString());
		try (PreparedStatement ps = connection.prepareStatement("select " + column2renamed + " from " + tableName);
				ResultSet rs = ps.executeQuery();) {
			assertEquals(ResultSetMetaData.columnNoNulls, rs.getMetaData().isNullable(1));
			assertEquals(40, rs.getMetaData().getPrecision(1));
		}
		operator.editColumn(tableName, column2renamed, "varchar(40)", true, nullString());
		try (PreparedStatement ps = connection.prepareStatement("select " + column2renamed + " from " + tableName);
				ResultSet rs = ps.executeQuery();) {
			assertEquals(ResultSetMetaData.columnNullable, rs.getMetaData().isNullable(1));
			assertEquals(40, rs.getMetaData().getPrecision(1));
		}

		TestUtils.assertThrows(SQLException.class, true, () -> operator.editColumn(tableName, column2renamed, "foo", column2renamed));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> operator.editColumn(nullString(), column2renamed, "varchar(20)", column2renamed));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> operator.editColumn(tableName, nullString(), "varchar(20)", column2renamed));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> operator.editColumn(tableName, column2renamed, nullString(), column2renamed));

	}

	public static void testDropColumn(final Connection connection) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connection);
		final String tableName = "table1";
		final String columnIdName = "id";
		final String column2Name = "col2";
		assertFalse(SqlUtils.execute(connection, "create table " + tableName + " (" + columnIdName + " int primary key, " + column2Name + " varchar(255))"));
		assertFalse(operator.dropColumn(tableName, column2Name + "X"));
		assertTrue(operator.dropColumn(tableName, column2Name));
		assertFalse(operator.existsColumn(tableName, column2Name));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> assertFalse(operator.dropColumn(tableName, nullString())));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> assertFalse(operator.dropColumn(nullString(), column2Name)));
	}

	public static void testExistsRenameDropIndex(final Connection connection) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connection);
		final String tableName = "table1";
		final String columnIdName = "id";
		final String column2Name = "col2";
		final String indexName = "IX_COL2";
		assertFalse(SqlUtils.execute(connection, "create table " + tableName + " (" + columnIdName + " int primary key, " + column2Name + " varchar(255) )"));
		assertFalse(SqlUtils.execute(connection, "create index " + indexName + " on " + tableName + " (" + column2Name + ")"));
		// exists
		assertTrue(operator.existsIndex(tableName, indexName));
		assertFalse(operator.existsIndex(tableName, indexName + "X"));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> assertFalse(operator.existsIndex(tableName, nullString())));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> assertFalse(operator.existsIndex(nullString(), indexName)));
		// rename
		final String indexNewName = indexName + "_NEW";
		assertTrue(operator.renameIndex(tableName, indexName, indexNewName));
		assertFalse(operator.existsIndex(tableName, indexName));
		assertTrue(operator.existsIndex(tableName, indexNewName));
		// drop
		assertFalse(operator.dropIndex(tableName, indexName));
		assertFalse(operator.existsIndex(tableName, indexName));
		assertTrue(operator.dropIndex(tableName, indexNewName));
		assertFalse(operator.existsIndex(tableName, indexNewName));
		assertFalse(operator.dropIndex(tableName, indexNewName));
		assertFalse(operator.existsIndex(tableName, indexNewName));

		assertFalse(operator.dropIndex(tableName + "X", indexNewName));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> operator.dropIndex(nullString(), indexNewName));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> operator.dropIndex(tableName, nullString()));
	}

	public static void testCreateIndex(final Connection connection) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connection);
		final String tableName = "table1";
		final String columnIdName = "id";
		final String column2Name = "col2";
		final String indexName = "IX_COL2";
		assertFalse(SqlUtils.execute(connection, "create table " + tableName + " (" + columnIdName + " int primary key, " + column2Name + " varchar(255))"));
		assertTrue(operator.createIndex(tableName, column2Name, indexName, false));
		assertTrue(operator.existsIndex(tableName, indexName));
		assertFalse(operator.createIndex(tableName, column2Name, indexName, false));
		assertTrue(operator.existsIndex(tableName, indexName));
		assertFalse(SqlUtils.execute(connection, "insert into " + tableName + " values (1, 'a')"));
		assertFalse(SqlUtils.execute(connection, "insert into " + tableName + " values (2, 'a')"));
		assertTrue(operator.dropIndex(tableName, indexName));
		TestUtils.assertThrows(SQLException.class, true, () -> operator.createIndex(tableName, column2Name, indexName, true));
		assertFalse(SqlUtils.execute(connection, "delete from " + tableName));
		assertTrue(operator.createIndex(tableName, column2Name, indexName, true));
		assertTrue(operator.existsIndex(tableName, indexName));
		assertFalse(operator.createIndex(tableName, column2Name, indexName, true));
		assertTrue(operator.existsIndex(tableName, indexName));
		assertFalse(SqlUtils.execute(connection, "insert into " + tableName + " values (1, 'a')"));
		TestUtils.assertThrows(SQLException.class, true, () -> SqlUtils.execute(connection, "insert into " + tableName + " values (2, 'a')"));

		TestUtils.assertThrows(StringNotValorizedException.class, () -> operator.createIndex(nullString(), column2Name, indexName, false));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> operator.createIndex(tableName, nullString(), indexName, false));
		TestUtils.assertThrows(StringNotValorizedException.class, () -> operator.createIndex(tableName, column2Name, nullString(), false));
	}

	public static void testExistsRenameDropForeignKey(final Connection connection) throws SQLException {
		testExistsRenameDropForeignKey(connection, true);
	}

	public static void testExistsDropForeignKey(final Connection connection) throws SQLException {
		testExistsRenameDropForeignKey(connection, false);
	}

	private static void testExistsRenameDropForeignKey(final Connection connection, final boolean rename) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connection);
		final String tableHeadName = "tableHead";
		final String tableRowName = "tableRow";
		final String columnIdHeadName = "id";
		final String columnIdRowName = "id";
		final String columnRefName = "head_id";
		final String fkName = "FK_ROW_HEAD_ID";
		assertFalse(SqlUtils.execute(connection, "create table " + tableHeadName + " (" + columnIdHeadName + " int primary key)"));
		assertFalse(SqlUtils.execute(connection, "create table " + tableRowName + " (" + columnIdRowName + " int primary key, " + columnRefName + " int, constraint " + fkName + " foreign key (" + columnRefName + ") references " + tableHeadName + "(" + columnIdHeadName + ") )"));
		// exists
		assertTrue(operator.existsForeignKey(tableRowName, fkName));
		assertFalse(operator.existsForeignKey(tableHeadName, fkName + "X"));
		assertFalse(operator.existsForeignKey(tableHeadName + "X", fkName));
		TestUtils.assertThrows(StringNotValorizedException.class, true, () -> operator.existsForeignKey(tableRowName, nullString()));
		TestUtils.assertThrows(StringNotValorizedException.class, true, () -> operator.existsForeignKey(nullString(), fkName));
		if (rename) {
			// rename
			final String fkNewName = fkName + "_NEW";
			assertTrue(operator.renameForeignKey(tableRowName, fkName, fkNewName));
			assertFalse(operator.renameForeignKey(tableRowName, fkName, fkNewName));
			// drop
			assertFalse(operator.existsForeignKey(tableRowName, fkName));
			assertTrue(operator.dropForeignKey(tableRowName, fkNewName));
			assertFalse(operator.existsForeignKey(tableRowName, fkNewName));
			assertFalse(operator.dropForeignKey(tableRowName, fkNewName));
			assertFalse(operator.existsForeignKey(tableRowName, fkNewName));
			assertFalse(operator.dropForeignKey(tableRowName + "X", fkNewName));
		} else {
			assertTrue(operator.dropForeignKey(tableRowName, fkName));
			assertFalse(operator.existsForeignKey(tableRowName, fkName));
			assertFalse(operator.dropForeignKey(tableRowName, fkName));
			assertFalse(operator.existsForeignKey(tableRowName, fkName));
			assertFalse(operator.dropForeignKey(tableRowName + "X", fkName));
		}
		TestUtils.assertThrows(StringNotValorizedException.class, true, () -> operator.dropForeignKey(tableRowName, nullString()));
		TestUtils.assertThrows(StringNotValorizedException.class, true, () -> operator.dropForeignKey(nullString(), fkName));
	}

	public static void testCreateForeignKey(final Connection connection) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connection);
		final String tableHeadName = "tableHead";
		final String tableRowName = "tableRow";
		final String columnIdHeadName = "id";
		final String columnIdRowName = "id";
		final String columnRefName = "head_id";
		final String fkName = "FK_ROW_HEAD_ID";
		assertFalse(SqlUtils.execute(connection, "create table " + tableHeadName + " (" + columnIdHeadName + " int primary key)"));
		assertFalse(SqlUtils.execute(connection, "create table " + tableRowName + " (" + columnIdRowName + " int primary key, " + columnRefName + " int)"));

		assertFalse(SqlUtils.execute(connection, "insert into " + tableRowName + " values(1,1)"));

		TestUtils.assertThrows(SQLException.class, true, () -> operator.createForeignKey(tableRowName, tableHeadName, fkName, columnRefName, columnIdHeadName));
		assertFalse(operator.existsForeignKey(tableRowName, fkName));

		assertFalse(SqlUtils.execute(connection, "insert into " + tableHeadName + " values(1)"));
		assertTrue(operator.createForeignKey(tableRowName, tableHeadName, fkName, columnRefName, columnIdHeadName));
		assertTrue(operator.existsForeignKey(tableRowName, fkName));
		assertFalse(operator.createForeignKey(tableRowName, tableHeadName, fkName, columnRefName, columnIdHeadName));
		assertTrue(operator.existsForeignKey(tableRowName, fkName));
	}

	public static void testExistsConstraint(final Connection connection, final boolean fixedPrimaryKeyName, final boolean skipUniqueIndexTest) throws SQLException {
		final DatabaseOperator operator = DatabaseOperator.of(connection);
		final String tableHeadName = "tableHead";
		final String tableRowName = "tableRow";
		final String columnIdHeadName = "id";
		final String columnIdRowName = "id";
		final String columnRefName = "head_id";
		final String fkName = "FK_ROW_HEAD_ID";
		final String pkName = "PK_ROW_ID";
		final String indexName = "IX_ROW_HEAD_ID";
		assertFalse(SqlUtils.execute(connection, "create table " + tableHeadName + " (" + columnIdHeadName + " int primary key)"));
		assertFalse(SqlUtils.execute(connection, "create table " + tableRowName + " (" + columnIdRowName + " int, " + columnRefName + " int, "
				+ "constraint " + fkName + " foreign key (" + columnRefName + ") references " + tableHeadName + "(" + columnIdHeadName + "), "
				+ "constraint " + pkName + " primary key (" + columnIdRowName + ")"
				+ ")"));
		assertTrue(operator.existsConstraint(tableRowName, fkName, ConstraintType.FOREIGN_KEY));
		assertFalse(operator.existsConstraint(tableRowName, fkName + "X", ConstraintType.FOREIGN_KEY));
		assertFalse(operator.existsConstraint(tableRowName + "X", fkName, ConstraintType.FOREIGN_KEY));
		assertTrue(operator.existsConstraint(tableRowName, pkName, ConstraintType.PRIMARY_KEY));
		if (fixedPrimaryKeyName) {
			assertTrue(operator.existsConstraint(tableRowName, pkName + "X", ConstraintType.PRIMARY_KEY));
		} else {
			assertFalse(operator.existsConstraint(tableRowName, pkName + "X", ConstraintType.PRIMARY_KEY));
		}
		assertFalse(operator.existsConstraint(tableRowName + "X", pkName, ConstraintType.PRIMARY_KEY));

		if (!skipUniqueIndexTest) {
			assertTrue(operator.createIndex(tableRowName, columnIdHeadName, indexName, true));
			assertTrue(operator.existsConstraint(tableRowName, indexName, ConstraintType.UNIQUE));
			assertFalse(operator.existsConstraint(tableRowName, indexName + "X", ConstraintType.UNIQUE));
			assertFalse(operator.existsConstraint(tableRowName + "X", indexName, ConstraintType.UNIQUE));
		}

		TestUtils.assertThrows(StringNotValorizedException.class, true, () -> operator.existsConstraint(tableRowName, nullString(), ConstraintType.FOREIGN_KEY));
		TestUtils.assertThrows(StringNotValorizedException.class, true, () -> operator.existsConstraint(nullString(), fkName, ConstraintType.FOREIGN_KEY));
		TestUtils.assertThrows(StringNotValorizedException.class, true, () -> operator.existsConstraint(tableRowName, nullString(), ConstraintType.PRIMARY_KEY));
		TestUtils.assertThrows(StringNotValorizedException.class, true, () -> operator.existsConstraint(nullString(), pkName, ConstraintType.PRIMARY_KEY));
		if (!skipUniqueIndexTest) {
			TestUtils.assertThrows(StringNotValorizedException.class, true, () -> operator.existsConstraint(tableRowName, nullString(), ConstraintType.UNIQUE));
			TestUtils.assertThrows(StringNotValorizedException.class, true, () -> operator.existsConstraint(nullString(), indexName, ConstraintType.UNIQUE));
		}
	}

	private static String nullString() {
		return null;
	}
}
