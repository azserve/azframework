/**
 * <p>
 * Fornisce varie classi di utilità per le operazioni più comuni su
 * oggetti del package {@code java.sql} e per operare su database
 * specifici come MySQL.
 * </p>
 */
package com.azserve.azframework.common.sql;