package com.azserve.azframework.common.sql.placeholder;

public class Now extends AbstractPlaceholder {

	public Now() {
		super(SqlPlaceholder.FN_NOW);
	}
}
