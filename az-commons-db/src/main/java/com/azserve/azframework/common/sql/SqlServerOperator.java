package com.azserve.azframework.common.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.sql.placeholder.DateAddDays;
import com.azserve.azframework.common.sql.placeholder.SqlPlaceholder;

public class SqlServerOperator extends InformationSchemaDatabaseOperator {

	public SqlServerOperator() {}

	public SqlServerOperator(final Connection connection) {
		super(connection);
	}

	@Override
	protected int getPriority(final Connection connection) {
		final String className = connection.getClass().getName();
		return className.startsWith("net.sourceforge.jtds") || className.startsWith("com.microsoft.sqlserver.jdbc") ? 0 : -1;
	}

	@Override
	protected String buildColumnToNotNullStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition) {
		return "alter table " + schema + "." + table + " alter column " + column + " " + definition + " not null";
	}

	@Override
	public boolean existsIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(index, "index");
		try (PreparedStatement ps = this.connection.prepareStatement("SELECT ind.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id where "
				+ "lower(schema_name(t.schema_id)) = ? "
				+ "and lower(t.name) = ? "
				+ "and lower(ind.name) = ? ")) {
			ps.setString(1, schema.toLowerCase());
			ps.setString(2, table.toLowerCase());
			ps.setString(3, index.toLowerCase());
			try (ResultSet rs = ps.executeQuery()) {
				return rs.next();
			}
		}
	}

	@Override
	public boolean existsConstraint(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final ConstraintType type) throws SQLException {
		if (ConstraintType.UNIQUE == type) {
			checkString(table, "Table");
			checkString(constraint, "Constraint");
			if (!this.existsTable(table)) {
				return false;
			}
			final String query = "SELECT ind.is_unique FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id where ind.is_unique = 1 and "
					+ "lower(t.name) = ? "
					+ "and lower(ind.name) = ? ";
			try (PreparedStatement ps = this.connection.prepareStatement(query)) {
				ps.setString(1, table);
				ps.setString(2, constraint);
				try (ResultSet rs = ps.executeQuery()) {
					return rs.next();
				}
			}
		}
		return super.existsConstraint(schema, table, constraint, type);
	}

	@Override
	protected void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final @Nullable String newName) throws SQLException {
		this.editExistentColumn(schema, table, column, definition, null, newName);
	}

	@Override
	protected void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final boolean nullable, @Nullable final String newName) throws SQLException {
		this.editExistentColumn(schema, table, column, definition, Boolean.valueOf(nullable), newName);
	}

	@Override
	protected String buildCreateIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String columns, final @NonNull String index, final boolean unique) {
		final StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("create");
		if (unique) {
			queryBuilder.append(" unique");
		}
		queryBuilder.append(" index ").append(index).append(" on ").append(schema).append(".").append(table)
				.append("(").append(columns).append(")");

		return queryBuilder.toString();
	}

	@Override
	protected String buildRenameIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index, final @NonNull String name) {
		return String.format("sp_rename '%s.%s.%s','%s','INDEX'", schema, table, index, name);
	}

	@Override
	protected String buildDropIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index) {
		return "drop index " + index + " on " + schema + "." + table;
	}

	@Override
	protected String buildRenamePrimaryKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String oldName, final @NonNull String newName) {
		return String.format("sp_rename '%s.%s.%s','%s'", schema, table, oldName, newName);
	}

	@Override
	protected String buildCreateForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String referencedTableSchema, final @NonNull String referencedTable, final @NonNull String constraint, final @NonNull String columns, final @NonNull String referencedColumns) {
		final StringBuilder sql = new StringBuilder();
		sql.append("alter table ");
		sql.append(schema).append(".").append(table);
		sql.append(" add constraint ").append(constraint);
		sql.append(" foreign key(");
		sql.append(columns);
		sql.append(") references ");
		sql.append(referencedTableSchema).append(".").append(referencedTable);
		sql.append("(").append(referencedColumns).append(")");
		return sql.toString();
	}

	@Override
	protected String buildRenameForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final @NonNull String name) {
		return String.format("sp_rename '%s.%s','%s','OBJECT'", schema, constraint, name);
	}

	@Override
	protected String buildDropForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) {
		return "alter table " + schema + "." + table + " drop constraint " + constraint;
	}

	@Override
	protected String getDefaultSchema() throws SQLException {
		return "dbo";
	}

	@Override
	protected String resolvePlaceholder(final SqlPlaceholder sqlPlaceholder, final String[] params) {
		switch (sqlPlaceholder) {
			case AUTOINCREMENT:
				return "int IDENTITY(1,1)";
			case BIT:
			case BOOLEAN:
				return "bit";
			case BYTES:
				return "varbinary(max)";
			case DATETIME:
				return "datetime";
			case TEXT:
				return "varchar(max)";
			case UUID:
				return "uniqueidentifier";
			case TRUE:
				return "1";
			case FALSE:
				return "0";
			case FN_NOW:
				return "getdate()";
			case FN_DATEADDDAYS:
				final DateAddDays dateAddDays = new DateAddDays(params);
				return String.format("dateadd(day, %s, %s)", dateAddDays.getDate(), dateAddDays.getDays());
		}
		throw new UnsupportedOperationException("Unknown placeholder " + sqlPlaceholder);
	}

	private void editExistentColumn(final String schema, final String table, final String column, final String definition, final Boolean nullable, final String newName) throws SQLException {
		// modifica colonna
		final StringBuilder sql = new StringBuilder()
				.append("alter table ")
				.append(schema).append(".").append(table)
				.append(" alter column ")
				.append(column)
				.append(" ").append(definition);
		if (nullable != null) {
			sql.append(nullable.booleanValue() ? " null" : " not null");
		}
		this.executeSql(sql.toString());
		// poi rinomina
		if (newName != null && !column.equalsIgnoreCase(newName)) {
			this.executeSql("EXEC sp_rename '" + schema + "." + table + "." + column + " ', '" + newName + "', 'COLUMN'");
		}
	}
}
