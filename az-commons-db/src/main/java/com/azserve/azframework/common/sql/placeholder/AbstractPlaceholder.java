package com.azserve.azframework.common.sql.placeholder;

public abstract class AbstractPlaceholder {

	private final SqlPlaceholder sqlPlaceholder;
	private final String[] params;

	protected AbstractPlaceholder(final SqlPlaceholder sqlPlaceholder, final String... params) {
		this.sqlPlaceholder = sqlPlaceholder;
		this.params = params;
	}

	protected String getParam(final int i) {
		return this.params[i];
	}

	public String get() {
		return this.getBasePart() + this.getParamPart();
	}

	public String getBasePart() {
		return this.sqlPlaceholder.name();
	}

	public String getParamPart() {
		if (this.params.length == 0) {
			return "";
		}
		return "(" + String.join(",", this.params) + ")";
	}

	@Override
	public String toString() {
		return "${" + this.get() + "}";
	}
}
