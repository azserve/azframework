package com.azserve.azframework.common.sql.placeholder;

public class DateAddDays extends AbstractPlaceholder {

	public DateAddDays(final String date, final String days) {
		this(new String[] { date, days });
	}

	public DateAddDays(final String... params) {
		super(SqlPlaceholder.FN_DATEADDDAYS, params);
		if (params.length != 2) {
			throw new IllegalArgumentException("Incorrect number of parameters");
		}
	}

	public String getDate() {
		return this.getParam(0);
	}

	public String getDays() {
		return this.getParam(1);
	}
}
