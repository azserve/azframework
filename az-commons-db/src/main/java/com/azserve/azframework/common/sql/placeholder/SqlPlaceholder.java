package com.azserve.azframework.common.sql.placeholder;

public enum SqlPlaceholder {

	// data types
	AUTOINCREMENT,
	BIT,
	BOOLEAN,
	BYTES,
	DATETIME,
	TEXT,
	UUID,

	// functions
	FN_NOW,
	FN_DATEADDDAYS,

	// values
	TRUE,
	FALSE;
}
