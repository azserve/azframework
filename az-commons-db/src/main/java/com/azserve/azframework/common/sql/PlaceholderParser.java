package com.azserve.azframework.common.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class PlaceholderParser {

	private static final Pattern PATTERN = Pattern.compile("\\$\\{([^\\{\\}]*)\\}");

	private PlaceholderParser() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	public static String parse(final String s, final BiFunction<String, String[], String> placeholderResolver) {

		final Matcher matcher = PATTERN.matcher(s);
		final StringBuilder sb = new StringBuilder();
		int i = 0;
		while (matcher.find()) {
			sb.append(s.substring(i, matcher.start()));
			final String group = matcher.group(1);
			final int indexOfBracket = group.indexOf('(');
			String basePlaceholder;
			String[] params;
			if (indexOfBracket < 0) {
				basePlaceholder = group;
				params = new String[0];
			} else {
				basePlaceholder = group.substring(0, indexOfBracket);
				params = splitParams(group.substring(indexOfBracket));
			}
			sb.append(placeholderResolver.apply(basePlaceholder, params));
			i = matcher.end();
		}
		if (i == 0) {
			return s;
		}
		sb.append(s.substring(i, s.length()));

		return parse(sb.toString(), placeholderResolver);
	}

	private static String[] splitParams(final String s) {
		final int sLength = s.length();
		if (s.charAt(0) != '(' || s.charAt(sLength - 1) != ')') {
			throw new IllegalArgumentException("Invalid param string " + s);
		}
		if (s.length() == 2) {
			return new String[0];
		}
		final String s1 = s.substring(1, sLength - 1);
		if (!s1.contains(",")) {
			return new String[] { s1 };
		}
		if (!s.contains("(") && !s.contains("'")) {
			return s1.split(",");
		}
		final List<String> result = new ArrayList<>();
		boolean inText = false;
		int bracketsCount = 0;
		int lastSplit = 0;
		final int s1Length = s1.length();
		for (int i = 0; i < s1Length; i++) {
			final char c = s1.charAt(i);
			if (inText) {
				if (c == '\'') {
					inText = false;
				}
			} else {
				if (c == '\'') {
					inText = true;
				} else {
					if (c == '(') {
						bracketsCount++;
					} else if (c == ')') {
						if (bracketsCount == 0) {
							throw new IllegalArgumentException("Invalid param string " + s);
						}
						bracketsCount--;
					} else if (c == ',') {
						if (bracketsCount == 0) {
							result.add(s1.substring(lastSplit, i));
							lastSplit = i + 1;
						}
					}
				}
			}
		}
		if (bracketsCount != 0) {
			throw new IllegalArgumentException("Invalid param string " + s);
		}
		result.add(s1.substring(lastSplit));
		return result.toArray(new String[result.size()]);
	}
}
