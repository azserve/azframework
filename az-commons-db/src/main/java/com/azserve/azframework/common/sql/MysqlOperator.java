package com.azserve.azframework.common.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.ex.Checked;
import com.azserve.azframework.common.lang.StringUtils;
import com.azserve.azframework.common.sql.placeholder.DateAddDays;
import com.azserve.azframework.common.sql.placeholder.SqlPlaceholder;

/**
 * Classe che fornisce metodi di utilità
 * per effettuare operazioni su database MySQL.
 *
 * @author filippo
 * @since 20/12/2016
 *
 *        TODO: può estendere {@link InformationSchemaDatabaseOperator}
 */
public class MysqlOperator extends DatabaseOperator {

	/** Funzione {@value} */
	public static final String FUNCTION_DATE = "date";

	public MysqlOperator(final Connection connection) {
		super(connection);
	}

	public MysqlOperator() {}

	@Override
	protected int getPriority(final Connection connection) {
		return connection.getClass().getName().startsWith("com.mysql") ? 0 : -1;
	}

	@Override
	public boolean existsTable(final @NonNull String schema, final @NonNull String table) throws SQLException {
		try (PreparedStatement ps = this.connection.prepareStatement("show tables from " + schema + " like ?")) {
			ps.setString(1, table);
			try (ResultSet rs = ps.executeQuery()) {
				return rs.next();
			}
		}
	}

	@Override
	public boolean existsColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException {
		checkString(schema, "Schema");
		checkString(table, "Table");
		checkString(column, "Column");
		if (!this.existsTable(schema, table)) {
			return false;
		}

		try (final PreparedStatement ps = this.connection.prepareStatement("show columns from " + schema + "." + table + " like ?")) {
			ps.setString(1, column);
			try (final ResultSet rsDescribe = ps.executeQuery()) {
				return rsDescribe.next();
			}
		}
	}

	@Override
	public boolean existsIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index) throws SQLException {
		checkString(schema, "Schema");
		checkString(table, "Table");
		checkString(index, "Index");
		if (!this.existsTable(schema, table)) {
			return false;
		}

		try (final PreparedStatement ps = this.connection.prepareStatement("show index from " + schema + "." + table + " where Key_name = ?")) {
			ps.setString(1, index);
			try (final ResultSet rsDescribe = ps.executeQuery()) {
				return rsDescribe.next();
			}
		}
	}

	@Override
	public boolean dropIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index) throws SQLException {
		checkString(schema, "Schema");
		checkString(table, "Table");
		checkString(index, "Index name");

		if (!this.existsIndex(schema, table, index)) {
			return false;
		}
		final String query = "alter table " + schema + "." + table + " drop index " + index;
		this.executeSql(query);
		return true;
	}

	@Override
	public boolean existsConstraint(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final ConstraintType type) throws SQLException {
		checkString(schema, "Schema");
		checkString(table, "Table");
		checkString(constraint, "Constraint name");
		try (final PreparedStatement preparedStatement = this.connection.prepareStatement("select * from information_schema.TABLE_CONSTRAINTS where CONSTRAINT_NAME = ? and TABLE_NAME = ? and TABLE_SCHEMA = ? and CONSTRAINT_TYPE = ?")) {
			preparedStatement.setString(1, type == ConstraintType.PRIMARY_KEY ? "PRIMARY" : constraint);
			preparedStatement.setString(2, table);
			preparedStatement.setString(3, schema);
			preparedStatement.setString(4, getConstraintTypeName(type));
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				return resultSet.next();
			}
		}
	}

	@Override
	public boolean renamePrimaryKey(final @NonNull String schema, final @NonNull String table, final @NonNull String newName) throws SQLException {
		LOGGER.warn("MySQL doesn't support Primary Key renaming");
		return false;
	}

	@Override
	protected String buildRenamePrimaryKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String oldName, final @NonNull String newName) {
		throw new UnsupportedOperationException("MySQL doesn't support Primary Key renaming");
	}

	@Override
	public boolean renameForeignKey(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final @NonNull String newName) throws SQLException {
		// renaming of a FK in MySQL means drop and recreate the FK with another name
		// we don't need to keep the FKs check enabled for this kind of operation
		try {
			SqlUtils.execute(this.connection, "SET FOREIGN_KEY_CHECKS=0");
			return super.renameForeignKey(schema, table, constraint, newName);
		} finally {
			try {
				SqlUtils.execute(this.connection, "SET FOREIGN_KEY_CHECKS=1");
			} catch (final SQLException ex) {
				LOGGER.warn("CAUTION!!! FOREIGN_KEY CHECKS MAY BE DISABLED!!!");
				throw ex;
			}
		}
	}

	private static String getConstraintTypeName(final @NonNull ConstraintType type) {
		switch (type) {
			case FOREIGN_KEY:
				return "FOREIGN KEY";
			case PRIMARY_KEY:
				return "PRIMARY KEY";
			case UNIQUE:
			case CHECK:
				return type.name();
			default:
		}
		throw new IllegalArgumentException("Invalid constraint type");
	}

	public static String readColumnDefinition(final @NonNull Statement st, final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException {
		checkString(schema, "Schema");
		checkString(table, "Table");
		checkString(column, "Column");

		try (ResultSet rs = st.executeQuery("describe " + schema + "." + table)) {
			while (rs.next()) {
				if (rs.getString("COLUMN_NAME").equalsIgnoreCase(column)) {
					return rs.getString("COLUMN_TYPE");
				}
			}
		}
		return null;
	}

	public static void copyTable(final @NonNull Statement st, final @NonNull String sourceSchema, final @NonNull String sourceTable, final @NonNull String destinationSchema, final @NonNull String destinationTable, final boolean replace) throws SQLException {
		checkString(sourceSchema, "Source schema");
		checkString(destinationSchema, "Destination schema");
		checkString(sourceTable, "Source table");
		checkString(destinationTable, "Destination table");

		if (sourceSchema.equalsIgnoreCase(destinationSchema) && sourceTable.equalsIgnoreCase(destinationTable)) {
			throw new IllegalArgumentException("Same table schema and name");
		}
		st.execute("create table if not exists " + destinationSchema + "." + destinationTable + " like " + sourceSchema + "." + sourceTable);
		st.executeUpdate((replace ? "replace" : "insert") + " into " + destinationSchema + "." + destinationTable + " select * from " + sourceSchema + "." + sourceTable);
	}

	@Override
	protected String buildCreateColumnStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final @Nullable String previousColumn) {
		String sql = "alter table " + schema + "." + table + " add " + column + " " + definition;
		if (previousColumn != null) {
			if (previousColumn.isEmpty()) {
				sql += " first";
			} else {
				sql += " after " + previousColumn;
			}
		}
		return sql;
	}

	@Override
	protected String buildColumnToNotNullStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition) {
		return "alter table " + schema + "." + table + " modify " + column + " " + definition + " not null";
	}

	@Override
	protected void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final @Nullable String newName) throws SQLException {
		this.editExistentColumn(schema, table, column, definition, null, newName);
	}

	@Override
	protected void editExistentColumn(@NonNull final String schema, @NonNull final String table, @NonNull final String column, @NonNull final String definition, final boolean nullable, @Nullable final String newName) throws SQLException {
		this.editExistentColumn(schema, table, column, definition, Boolean.valueOf(nullable), newName);
	}

	@Override
	protected String buildCreateIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String columns, final @NonNull String index, final boolean unique) {
		final StringBuilder sql = new StringBuilder();
		sql.append("create");
		if (unique) {
			sql.append(" unique");
		}
		sql.append(" index ").append(index).append(" on ").append(schema).append(".").append(table)
				.append("(").append(columns).append(")");
		return sql.toString();
	}

	@Override
	protected String buildRenameIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index, final @NonNull String name) {
		return "alter table " + schema + "." + table + " rename index " + index + " to " + name;
	}

	@Override
	protected String buildDropIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index) {
		return "alter table " + schema + "." + table + " drop index " + index;
	}

	@Override
	protected String buildCreateForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String referencedTableSchema, final @NonNull String referencedTable, final @NonNull String constraint, final @NonNull String columns, final @NonNull String referencedColumns) {
		return "alter table " + schema + "." + table + " " + buildCreateForeignKeyStatement(referencedTableSchema, referencedTable, constraint, columns, referencedColumns);
	}

	@Override
	protected String buildRenameForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final @NonNull String name) {

		final ForeignKey fk = this.findForeignKeyColumns(schema, table, constraint);
		if (fk == null) {
			throw new RuntimeException("Cannot find the foreign key \"" + constraint + "\"");
		}

		return "alter table " + schema + "." + table + " " + buildDropForeignKeyStatement(constraint) + ","
				+ buildCreateForeignKeyStatement(fk.referencedTableSchema, fk.referencedTable, name, fk.columns, fk.referencedColumns);
	}

	@Override
	protected String buildDropForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) {
		return "alter table " + schema + "." + table + " " + buildDropForeignKeyStatement(constraint);
	}

	private static String buildCreateForeignKeyStatement(final @NonNull String referencedSchema, final @NonNull String referencedTable, final @NonNull String constraint, final @NonNull String columns, final @NonNull String referencedColumns) {
		final StringBuilder sql = new StringBuilder();
		sql.append("add constraint ");
		sql.append(constraint);
		sql.append(" foreign key (");
		sql.append(columns);
		sql.append(") references ");
		sql.append(referencedSchema).append(".").append(referencedTable);
		sql.append("(").append(referencedColumns).append(")");
		return sql.toString();
	}

	private static String buildDropForeignKeyStatement(final @NonNull String constraint) {
		return "drop foreign key " + constraint;
	}

	@Override
	protected String buildDropCheckStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) {
		return "alter table " + schema + "." + table + " drop check " + constraint;
	}

	@Override
	protected boolean isColumnNotNullable(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException {
		try (final PreparedStatement ps = this.connection.prepareStatement("show columns from " + schema + "." + table + " like ?")) {
			ps.setString(1, column);
			try (final ResultSet rsDescribe = ps.executeQuery()) {
				rsDescribe.next();
				return rsDescribe.getString("Null").equalsIgnoreCase("NO");
			}
		}
	}

	@Override
	protected String fixForeignKeyName(final String constraint) {
		return StringUtils.left(constraint, 64);
	}

	@Override
	protected String fixIndexName(final String index) {
		return StringUtils.left(index, 64);
	}

	@Override
	protected String getDefaultSchema() throws SQLException {
		return this.connection.getCatalog();
	}

	@Override
	protected String resolvePlaceholder(final SqlPlaceholder sqlPlaceholder, final String[] params) {
		switch (sqlPlaceholder) {
			case AUTOINCREMENT:
				return "int AUTO_INCREMENT";
			case BIT:
			case BOOLEAN:
				return "bit";
			case BYTES:
				return "longblob";
			case DATETIME:
				return "datetime";
			case TEXT:
				return "longtext";
			case UUID:
				return "binary(16)"; // qui sorge un problema: nelle insert/update/where il dato non può essere indicato come stringa
			case TRUE:
				return "true";
			case FALSE:
				return "false";
			case FN_NOW:
				return "now()";
			case FN_DATEADDDAYS:
				final DateAddDays dateAddDays = new DateAddDays(params);
				return String.format("DATE_ADD(%s , interval %s day)", dateAddDays.getDate(), dateAddDays.getDays());
			default:
				break;
		}
		throw new UnsupportedOperationException("Unknown placeholder " + sqlPlaceholder);
	}

	private void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final Boolean nullable, final String newName) throws SQLException {
		final StringBuilder sql = new StringBuilder();
		sql.append("alter table ");
		sql.append(schema).append(".").append(table);
		if (newName == null || newName.equals(column)) {
			sql.append(" modify column ");
			sql.append(column);
		} else {
			sql.append(" change column ");
			sql.append(column).append(" ").append(newName);
		}
		sql.append(" ").append(definition);
		if (nullable != null) {
			sql.append(" ").append(nullable.booleanValue() ? "null" : "not null");
		}

		final String query = sql.toString();
		this.executeSql(query);
	}

	private @Nullable ForeignKey findForeignKeyColumns(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) {
		try (final PreparedStatement statement = this.getConnection().prepareStatement("select kcu.REFERENCED_TABLE_SCHEMA, kcu.REFERENCED_TABLE_NAME, kcu.COLUMN_NAME, kcu.REFERENCED_COLUMN_NAME"
				+ " from information_schema.KEY_COLUMN_USAGE kcu"
				+ " join information_schema.table_constraints tc"
				+ " on kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME and kcu.TABLE_SCHEMA = tc.TABLE_SCHEMA"
				+ " and kcu.TABLE_NAME = tc.TABLE_NAME"
				+ " where tc.TABLE_SCHEMA = ?"
				+ " and tc.TABLE_NAME = ?"
				+ " and tc.CONSTRAINT_NAME = ?"
				+ " and tc.CONSTRAINT_TYPE = 'FOREIGN KEY'"
				+ " order by kcu.ORDINAL_POSITION")) {
			statement.setString(1, schema);
			statement.setString(2, table);
			statement.setString(3, constraint);

			String referencedTableSchema = null;
			String referencedTable = null;
			String columns = "";
			String referencedColumns = "";

			try (final ResultSet rs = statement.executeQuery()) {
				while (rs.next()) {
					referencedTableSchema = rs.getString(1);
					referencedTable = rs.getString(2);
					columns += rs.getString(3) + ",";
					referencedColumns += rs.getString(4) + ",";
				}
				if (referencedTable == null) {
					return null;
				}
				return new ForeignKey(
						referencedTableSchema,
						referencedTable,
						columns.substring(0, columns.length() - 1),
						referencedColumns.substring(0, referencedColumns.length() - 1));
			}
		} catch (final SQLException ex) {
			Checked.sneakyThrow(ex);
			return null;
		}
	}

	private class ForeignKey {

		private final String referencedTableSchema;
		private final String referencedTable;
		private final String columns;
		private final String referencedColumns;

		ForeignKey(final String referencedTableSchema, final String referencedTable, final String columns, final String referencedColumns) {
			this.referencedTableSchema = referencedTableSchema;
			this.referencedTable = referencedTable;
			this.columns = columns;
			this.referencedColumns = referencedColumns;
		}
	}
}
