package com.azserve.azframework.common.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.ex.CheckedConsumer;
import com.azserve.azframework.common.ex.CheckedFunction;
import com.azserve.azframework.common.funct.Functions;
import com.azserve.azframework.common.lang.ArrayUtils;
import com.azserve.azframework.common.lang.StringUtils;
import com.azserve.azframework.common.lang.StringUtils.StringNotValorizedException;
import com.azserve.azframework.common.sql.placeholder.SqlPlaceholder;

/**
 * Classe astratta che fornisce metodi di utilità
 * per effettuare operazioni su database.
 *
 * <p>
 * Le estensioni di questa classe possono essere registrate
 * come servizi sfruttando il meccanisimo di {@link ServiceLoader}
 * per poi essere richiamate in base alla connessione al database:
 *
 * <pre>
 * DatabaseOperator.of(connection).dropColumn("mytable", "mycolumn");
 * </pre>
 *
 * pertanto le classi che estendono {@code DatabaseOperator} dovranno
 * avere un costruttore vuoto pubblico.
 * <br>
 * È inoltre possibile specificare una prorità che verrà utilizzata
 * quando più classi {@code DatabaseOperator} possono gestire
 * una certa connessione.
 * <br>
 * Le classi già integrate restituiscono una priorità pari a 0.
 * Restituire una priorità > 0 per dare priorità alle proprie classi
 * rispetto a quelle integrate.
 *
 * <p>
 * Questa classe è in grado di sostituire dei segnaposto nel formato: ${MYPLACEHOLDER}.
 * <br>
 * Alcuni segnaposto sono già gestiti da questa classe e sono presenti
 * nella enum {@link SqlPlaceholder}.
 * <br>
 * È possibile gestire ulteriori segnaposto impostando una funzione
 * tramite metodo {@linkplain #setPlaceholderResolver(BiFunction)}.
 * In questo caso la funzione specificata avrà la precedenza
 * sulla risoluzione dei segnaposto già gestiti nel caso
 * in cui venga restituito un valore {@code != null}.
 *
 * @author filippo
 * @since 20/12/2016
 */
public abstract class DatabaseOperator {

	protected static final Logger LOGGER = LoggerFactory.getLogger(DatabaseOperator.class);

	/**
	 * Restituisce un oggetto {@linkplain DatabaseOperator} apposito
	 * per operare sul database al quale la connessione specificata fa riferimento.
	 *
	 * @param connection connessione al database.
	 * @return l'oggetto {@code DatabaseOperator} apposito.
	 *
	 * @throws IllegalArgumentException se il database della connessione non è supportato.
	 */
	public static DatabaseOperator of(final Connection connection) {

		DatabaseOperator candidate = null;
		int currentPriority = -1;
		for (final DatabaseOperator databaseOperator : ServiceLoader.load(DatabaseOperator.class)) {
			final int priority = databaseOperator.getPriority(connection);
			if (priority > currentPriority) {
				candidate = databaseOperator;
				currentPriority = priority;
			}
		}
		if (candidate == null) {
			throw new IllegalArgumentException("No DatabaseOperator found for the connection");
		}
		candidate.initialize(connection);
		return candidate;
	}

	protected Connection connection;
	private boolean dryRun;
	private boolean resolvePlaceholders = true;
	private BiFunction<String, String[], String> placeholderResolver = Functions.nullBiFunction();

	public DatabaseOperator(final Connection connection) {
		this.initialize(connection);
	}

	protected DatabaseOperator() {}

	protected final void initialize(final Connection connection) {
		if (this.connection != null) {
			throw new AssertionError("Connession already set");
		}
		this.connection = connection;
	}

	protected abstract int getPriority(Connection connection);

	public boolean isDryRun() {
		return this.dryRun;
	}

	public void setDryRun(final boolean dryRun) {
		this.dryRun = dryRun;
	}

	public boolean isResolvePlaceholders() {
		return this.resolvePlaceholders;
	}

	public void setResolvePlaceholders(final boolean resolvePlaceholders) {
		this.resolvePlaceholders = resolvePlaceholders;
	}

	public void setPlaceholderResolver(final BiFunction<String, String[], String> placeholderResolver) {
		this.placeholderResolver = placeholderResolver;
	}

	public Connection getConnection() {
		return this.connection;
	}

	/**
	 * Restituisce la lista dei nomi delle tabelle di uno schema.
	 *
	 * @param schema schema interessato.
	 *
	 * @return una lista di stringhe rappresentanti i nomi delle tabelle.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} è {@code null} o vuoto.
	 */
	public List<String> readTables(final @NonNull String schema) throws SQLException {
		try (ResultSet rs = this.connection.getMetaData().getTables(this.connection.getCatalog(), schema, null, null)) {
			final List<String> result = new ArrayList<>();
			while (rs.next()) {
				result.add(rs.getString(3));
			}
			return result;
		}
	}

	/**
	 * Restituisce la lista dei nomi delle tabelle dello schema corrente.
	 *
	 * @return una lista di stringhe rappresentanti i nomi delle tabelle.
	 *
	 * @throws SQLException in caso di errore SQL.
	 */
	public List<String> readTables() throws SQLException {
		return this.readTables(this.getDefaultSchema());
	}

	/**
	 * Crea una tabella se non esiste.
	 *
	 * @see #createTable(String, String, String)
	 */
	public boolean createTable(final @NonNull String table, final @NonNull String definition) throws SQLException {
		return this.createTable(this.getDefaultSchema(), table, definition);
	}

	/**
	 * Crea una tabella se non esiste.
	 *
	 * @param schema nome dello schema dove create la tabella
	 * @param table nome della tabella da create
	 * @param definitions definizioni della tabella escluso "create table()"
	 *
	 * @return {@code true} se la tabella è stata creata,
	 *         altrimenti {@code false}.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table} o {@code definition}
	 *             sono {@code null} o vuoti.
	 */
	public boolean createTable(final @NonNull String schema, final @NonNull String table, final @NonNull String definitions) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(definitions, "definitions");
		if (!this.existsTable(schema, table)) {
			this.executeSql("create table " + schema + "." + table + "(" + definitions + ")");
			return true;
		}
		return false;
	}

	/**
	 * Elimina una tabella se esiste.
	 *
	 * @see #dropTable(String, String)
	 */
	public boolean dropTable(final @NonNull String table) throws SQLException {
		return this.dropTable(this.getDefaultSchema(), table);
	}

	/**
	 * Elimina una tabella se esiste.
	 *
	 * @param schema nome dello schema dove si trova la tabella.
	 * @param table nome della tabella da eliminare.
	 *
	 * @return {@code true} se la tabella è stata eliminata,
	 *         altrimenti {@code false}.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table} sono {@code null} o vuoti.
	 */
	public boolean dropTable(final @NonNull String schema, final @NonNull String table) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		if (this.existsTable(schema, table)) {
			this.executeSql("drop table " + schema + "." + table);
			return true;
		}
		return false;
	}

	/**
	 * Verifica se esiste una determinata tabella nello schema corrente.
	 *
	 * @see #existsTable(String, String)
	 */
	public boolean existsTable(final @NonNull String table) throws SQLException {
		return this.readTables().stream().anyMatch(t -> t.equalsIgnoreCase(table));
	}

	/**
	 * Verifica se esiste una determinata tabella
	 * nello schema specificato.
	 *
	 * @param schema nome dello schema su cui cercare.
	 * @param table nome della tabella da cercare.
	 *
	 * @return {@code true} se la tabella esiste,
	 *         altrimenti {@code false}.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table} sono {@code null} o vuoti.
	 */
	public boolean existsTable(final @NonNull String schema, final @NonNull String table) throws SQLException {
		return this.readTables(schema).stream().anyMatch(t -> t.equalsIgnoreCase(table));
	}

	/**
	 * Controlla se esiste una colonna.
	 *
	 * @see #existsColumn(String, String, String)
	 */
	public boolean existsColumn(final @NonNull String table, final @NonNull String column) throws SQLException {
		return this.existsColumn(this.getDefaultSchema(), table, column);
	}

	/**
	 * Controlla se esiste una colonna.
	 *
	 * @param schema nome dello schema su cui cercare.
	 * @param table nome della tabella su cui cercare.
	 * @param column nome della colonna da cercare.
	 *
	 * @return {@code true} se esiste la colonna.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table} o {@code column} sono {@code null} o vuoti.
	 */
	public abstract boolean existsColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException;

	/**
	 * Crea una nuova colonna.
	 *
	 * @see #createColumn(String, String, String, String, String)
	 */
	public boolean createColumn(final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn) throws SQLException {
		return this.createColumn(this.getDefaultSchema(), table, column, definition, previousColumn);
	}

	/**
	 * Crea una nuova colonna.
	 *
	 * È possibile indicare la colonna precedente ma alcuni database non supportano
	 * questa funzionalità e, in tal caso, sarà ignorata.
	 *
	 * @parma schema schema della tabella.
	 * @param table nome della tabella interessata.
	 * @param column nome della colonna da aggiungere.
	 * @param definition definizione della colonna.
	 * @param previousColumn il nome della colonna precedente, "" se in prima posizione.
	 *
	 * @return {@code true} se la colonna è stata aggiunta, {@code false} se la colonna
	 *         esisteva già.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table}, {@code column} sono {@code null} o vuoti.
	 */
	public boolean createColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(column, "column");
		checkString(definition, "column definition");

		if (this.existsColumn(schema, table, column)) {
			return false;
		}

		final String sql = this.buildCreateColumnStatement(schema, table, column, definition, previousColumn);
		this.executeSql(sql);
		return true;
	}

	/**
	 * Crea una nuova colonna "NOT NULL".
	 *
	 * @see #createNotNullableColumn(String, String, String, String, String, Object)
	 */
	public boolean createNotNullableColumn(final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn, final Object defaultValue) throws SQLException {
		return this.createNotNullableColumn(this.getDefaultSchema(), table, column, definition, previousColumn, defaultValue);
	}

	/**
	 * Crea una nuova colonna "NOT NULL".
	 *
	 * È possibile indicare la colonna precedente ma alcuni database non supportano
	 * questa funzionalità e, in tal caso, sarà ignorata.
	 *
	 * @param schema schema della tabella.
	 * @param table nome della tabella interessata.
	 * @param column nome della colonna da aggiungere.
	 * @param definition definizione della colonna.
	 * @param previousColumn il nome della colonna precedente, "" se in prima posizione.
	 * @param defaultValue valore di default.
	 *
	 * @return {@code true} se la colonna è stata aggiunta, {@code false} se la colonna
	 *         esisteva già.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table}, {@code column} o {@code previousColumn} sono {@code null} o vuoti.
	 */
	public boolean createNotNullableColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn, final Object defaultValue) throws SQLException {
		return this.createNotNullableColumn(schema, table, column, definition, previousColumn,
				defaultValue != null ? "?" : null,
				defaultValue != null ? new Object[] { defaultValue } : ArrayUtils.EMPTY);
	}

	/**
	 * Crea una nuova colonna "NOT NULL".
	 *
	 * @see #createNotNullableColumn(String, String, String, String, String, String, Object...)
	 */
	public boolean createNotNullableColumn(final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn, final String nonNullValueAssignment, final Object[] params) throws SQLException {
		return this.createNotNullableColumn(this.getDefaultSchema(), table, column, definition, previousColumn, nonNullValueAssignment, params);
	}

	/**
	 * Crea una nuova colonna "NOT NULL".
	 *
	 * È possibile indicare la colonna precedente ma alcuni database non supportano
	 * questa funzionalità e, in tal caso, sarà ignorata.
	 *
	 * @param schema schema della tabella.
	 * @param table nome della tabella interessata.
	 * @param column nome della colonna da aggiungere.
	 * @param definition definizione della colonna.
	 * @param previousColumn il nome della colonna precedente, "" se in prima posizione.
	 * @param nonNullValueAssignment assegnazione da effettuare per impostare il valore dove è {@code null}.
	 *            Se il valore di questo parametro è {@code null} non verrà eseguito alcun UPDATE.
	 * @param params eventuali parametri da applicare alla query che imposta il valore.
	 *            Ignorato se {@code nonNullValueAssignment} è {@code null}.
	 *
	 * @return {@code true} se la colonna è stata aggiunta, {@code false} se la colonna
	 *         esisteva già.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table}, {@code column} o {@code previousColumn} sono {@code null} o vuoti.
	 */
	public boolean createNotNullableColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn, final String nonNullValueAssignment, final Object[] params) throws SQLException {
		// creo la colonna se non esiste
		this.createColumn(schema, table, column, definition, previousColumn);
		// se è già NOT NULL non devo far niente
		if (this.isColumnNotNullable(schema, table, column)) {
			return false;
		}
		// non è NOT NULL quindi i valori null li sostituisco con un default
		if (nonNullValueAssignment != null) {
			final String updateQuery = "update " + schema + "." + table + " set " + column + " = " + nonNullValueAssignment + " where " + column + " is null";
			this.executeSql(updateQuery, params);
		}
		// imposto la colonna come NOT NULLABLE
		final String query = this.buildColumnToNotNullStatement(schema, table, column, definition);
		this.executeSql(query);
		return true;
	}

	/**
	 * Modifica la colonna di una tabella.
	 *
	 * @see #editColumn(String, String, String, String, String)
	 */
	public boolean editColumn(final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String newName) throws SQLException {
		return this.editColumn(this.getDefaultSchema(), table, column, definition, newName);
	}

	/**
	 * Modifica la colonna di una tabella.
	 *
	 * @param schema nome dello schema su cui si trova la tabella.
	 * @param table nome della tabella interessata.
	 * @param column nome della colonna da modificare.
	 * @param definition nuova definizione della colonna.
	 * @param newName nuovo nome della colonna (facoltativo).
	 *
	 * @return {@code true} se la modifica è stata effettuata,
	 *         {@code false} se la colonna non esiste.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table}, {@code column} o {@code definition} sono {@code null} o vuoti.
	 */
	public boolean editColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String newName) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(column, "column");
		checkString(definition, "column definition");

		if (!this.existsColumn(schema, table, column)) {
			return false;
		}

		this.editExistentColumn(schema, table, column, definition, newName);
		return true;
	}

	/**
	 * Modifica la colonna di una tabella.
	 *
	 * @see #editColumn(String, String, String, String,boolean, String)
	 */
	public boolean editColumn(final @NonNull String table, final @NonNull String column, final @NonNull String definition, final boolean nullable, final String newName) throws SQLException {
		return this.editColumn(this.getDefaultSchema(), table, column, definition, nullable, newName);
	}

	/**
	 * Modifica la colonna di una tabella.
	 *
	 * @param schema nome dello schema su cui si trova la tabella.
	 * @param table nome della tabella interessata.
	 * @param column nome della colonna da modificare.
	 * @param definition nuova definizione della colonna.
	 * @param nullable indica se la colonna è nullabile o meno.
	 * @param newName nuovo nome della colonna (facoltativo).
	 *
	 * @return {@code true} se la modifica è stata effettuata,
	 *         {@code false} se la colonna non esiste.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table}, {@code column} o {@code definition} sono {@code null} o vuoti.
	 */
	public boolean editColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final boolean nullable, final String newName) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(column, "column");
		checkString(definition, "column definition");

		if (!this.existsColumn(schema, table, column)) {
			return false;
		}

		this.editExistentColumn(schema, table, column, definition, nullable, newName);
		return true;
	}

	/**
	 * Rimuove una colonna.
	 *
	 * @see #dropColumn(String, String, String)
	 */
	public boolean dropColumn(final @NonNull String table, final @NonNull String column) throws SQLException {
		return this.dropColumn(this.getDefaultSchema(), table, column);
	}

	/**
	 * Rimuove una colonna.
	 *
	 * @param table nome della tabella interessata.
	 * @param column nome della colonna da rimuovere.
	 *
	 * @return {@code true} se la colonna è stata rimossa, {@code false} se la colonna
	 *         non esisteva già.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table} o {@code column} sono {@code null} o vuoti.
	 */
	public boolean dropColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(column, "column");

		if (!this.existsColumn(schema, table, column)) {
			return false;
		}
		final String query = this.buildDropColumnStatement(schema, table, column);
		this.executeSql(query);
		return true;
	}

	/**
	 * Controlla se esiste un indice.
	 *
	 * @see #existsIndex(String, String, String)
	 */
	public boolean existsIndex(final @NonNull String table, final @NonNull String index) throws SQLException {
		return this.existsIndex(this.getDefaultSchema(), table, index);
	}

	/**
	 * Controlla se esiste un indice.
	 *
	 * @param schema nome dello schema su cui si trova la tabella dove controllare.
	 * @param table nome della tabella dove controllare.
	 * @param index nome dell'indice da verificare.
	 *
	 * @return {@code true} se esiste l'indice.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table} o {@code index} sono {@code null} o vuoti.
	 */
	public abstract boolean existsIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index) throws SQLException;

	/**
	 * Crea un indice ad una tabella.
	 *
	 * @see #createIndex(String, String, String, String, boolean)
	 */
	public boolean createIndex(final @NonNull String table, final @NonNull String columns, final @NonNull String index, final boolean unique) throws SQLException {
		return this.createIndex(this.getDefaultSchema(), table, columns, index, unique);
	}

	/**
	 * Crea un indice ad una tabella.
	 *
	 * @param schema nome dello schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param columns nome delle colonne coinvolte separate da virgola.
	 * @param index nome dell'indice da creare.
	 * @param unique indica se l'indice è univoco.
	 *
	 * @return {@code true} se l'indice è stato creato, {@code false} se l'indice
	 *         esisteva già.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table} o {@code column} o {@code index} sono {@code null} o vuoti.
	 */
	public boolean createIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String columns, final @NonNull String index, final boolean unique) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(columns, "columns");
		checkString(index, "index name");

		final String indexFixed = this.fixIndexName(index);
		if (this.existsIndex(schema, table, indexFixed)) {
			return false;
		}

		final String query = this.buildCreateIndexStatement(schema, table, columns, indexFixed, unique);
		this.executeSql(query);
		return true;
	}

	/**
	 * Rinomina l'indice di una tabella.
	 * Se l'indice non è a livello di tabella, la tabella viene ignorata.
	 *
	 * @see #renameIndex(String, String, String, String)
	 */
	public boolean renameIndex(final @NonNull String table, final @NonNull String index, final @NonNull String name) throws SQLException {
		return this.renameIndex(this.getDefaultSchema(), table, index, name);
	}

	/**
	 * Rinomina l'indice di una tabella.
	 * Se l'indice non è a livello di tabella, la tabella viene ignorata.
	 * Se l'indice non cambia nome, viene considerato come rinominato
	 * senza far nulla.
	 *
	 * @param schema nome dello schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param index nome dell'indice da rinominare.
	 * @param name nuovo nome dell'indice.
	 *
	 * @return {@code true} se l'indice è stato rinominato,
	 *         {@code false} se l'indice non esiste.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se almeno uno dei parametri è {@code null} o vuoto.
	 */
	public boolean renameIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index, final @NonNull String name) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(index, "index name");
		checkString(name, "index new name");

		final String indexFixed = this.fixIndexName(index);

		if (!this.existsIndex(schema, table, indexFixed)) {
			return false;
		}
		final String nameFixed = this.fixIndexName(name);
		if (indexFixed.equals(nameFixed)) {
			return true;
		}
		final String query = this.buildRenameIndexStatement(schema, table, indexFixed, nameFixed);
		this.executeSql(query);
		return true;
	}

	/**
	 * Rimuove un indice da una tabella.
	 *
	 * @see #dropIndex(String, String, String)
	 */
	public boolean dropIndex(final @NonNull String table, final @NonNull String index) throws SQLException {
		return this.dropIndex(this.getDefaultSchema(), table, index);
	}

	/**
	 * Rimuove un indice da una tabella.
	 *
	 * @param schema schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param index nome dell'indice da rimuovere.
	 *
	 * @return {@code true} se l'indice è stato rimosso, {@code false} se l'indice
	 *         non esisteva.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table} o {@code index} sono {@code null} o vuoti.
	 */
	public boolean dropIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(index, "index name");

		if (!this.existsIndex(schema, table, index)) {
			return false;
		}
		final String query = this.buildDropIndexStatement(schema, table, index);
		this.executeSql(query);
		return true;
	}

	/**
	 * Aggiunge un vincolo alla tabella indicata.
	 *
	 * @see #createCheck(String, String, String, String)
	 */
	public boolean createCheck(final @NonNull String table, final @NonNull String constraint, final @NonNull String condition) throws SQLException {
		return this.createCheck(this.getDefaultSchema(), table, constraint, condition);
	}

	/**
	 * Aggiunge un vincolo alla tabella indicata.
	 *
	 * @param schema schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param constraint nome del vincolo da creare.
	 * @param condition condizioni del vincolo.
	 * @return {@code true} se il vincolo è stato creato, {@code false} se il vincolo
	 *         esisteva già.
	 * @throws SQLException
	 */
	public boolean createCheck(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final @NonNull String condition) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(constraint, "constraint name");
		checkString(condition, "check condition");

		if (this.existsConstraint(schema, table, constraint, ConstraintType.CHECK)) {
			return false;
		}
		final String query = "alter table " + schema + "." + table + " add constraint " + constraint + " check (" + condition + ")";
		this.executeSql(query);
		return true;
	}

	/**
	 * Rimuove un vincolo dalla tabella indicata.
	 *
	 * @see #dropCheck(String, String, String)
	 */
	public boolean dropCheck(final @NonNull String table, final @NonNull String constraint) throws SQLException {
		return this.dropCheck(this.getDefaultSchema(), table, constraint);
	}

	/**
	 * Rimuove un vincolo dalla tabella indicata.
	 *
	 * @param schema schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param constraint nome del vincolo da rimuovere.
	 * @return {@code true} se il vincolo è stato rimosso, {@code false} se il vincolo
	 *         non esisteva.
	 * @throws SQLException
	 */
	public boolean dropCheck(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(constraint, "constraint name");

		if (!this.existsConstraint(schema, table, constraint, ConstraintType.CHECK)) {
			return false;
		}
		final String query = this.buildDropCheckStatement(schema, table, constraint);
		this.executeSql(query);
		return true;
	}

	/**
	 * Controlla se esiste un vincolo nella tabella indicata.
	 *
	 * @see #existsConstraint(String, String, String, ConstraintType)
	 */
	public boolean existsConstraint(final @NonNull String table, final @NonNull String constraint, final ConstraintType type) throws SQLException {
		return this.existsConstraint(this.getDefaultSchema(), table, constraint, type);
	}

	/**
	 * Controlla se esiste un vincolo nella tabella indicata.
	 *
	 * @param schema nome dello schema su cui si trova la tabella da analizzare.
	 * @param table nome della tabella da analizzare.
	 * @param constraint nome del vincolo.
	 * @param type tipo di vincolo (chiave primaria, chiave esterna, indice univoco..).
	 *
	 * @return {@code true} se esiste il vincolo indicato.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws NullPointerException se {@code type} è {@code null}.
	 * @throws StringNotValorizedException se {@code schema} o {@code table} o {@code constraint} sono {@code null} o vuoti.
	 */
	public abstract boolean existsConstraint(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final ConstraintType type) throws SQLException;

	public boolean renamePrimaryKey(final @NonNull String table, final @NonNull String newName) throws SQLException {
		return this.renamePrimaryKey(this.getDefaultSchema(), table, newName);
	}

	/**
	 * Rinomina la primary key di una tabella.
	 * Se la tabella non ha primary key o la tabella non esiste ritorna false.
	 * Se la primary key non cambia nome, viene considerata come rinominata
	 * senza far nulla.
	 *
	 * @param schema nome dello schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param name nuovo nome della primary key.
	 *
	 * @return {@code true} se la primary key è stata rinominata,
	 *         {@code false} se la primary key non esiste.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se almeno uno dei parametri è {@code null} o vuoto.
	 */
	public boolean renamePrimaryKey(final @NonNull String schema, final @NonNull String table, final @NonNull String newName) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(newName, "constraint new name");

		final String constraintFixed = this.getPrimaryKeyName(schema, table);
		if (constraintFixed == null) {
			return false;
		}
		final String newNameFixed = this.fixPrimaryKeyName(newName);
		if (newNameFixed.equals(constraintFixed)) {
			return true;
		}
		final String query = this.buildRenamePrimaryKeyStatement(schema, table, constraintFixed, newNameFixed);
		this.executeSql(query);
		return true;
	}

	public String getPrimaryKeyName(final @NonNull String schema, final @NonNull String table) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		return this.findPrimaryKeyName(schema, table);
	}

	public String getPrimaryKeyName(final @NonNull String table) throws SQLException {
		return this.getPrimaryKeyName(this.getDefaultSchema(), table);
	}

	protected String fixPrimaryKeyName(final @NonNull String primaryKeyName) {
		return primaryKeyName;
	}

	protected String findPrimaryKeyName(final @NonNull String schema, final @NonNull String table) throws SQLException {
		try (ResultSet rs = this.connection.getMetaData().getPrimaryKeys(this.connection.getCatalog(), schema, table)) {
			if (rs.next()) {
				return rs.getString(6);
			}
			return null;
		}
	}

	protected abstract String buildRenamePrimaryKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String oldName, final @NonNull String newName);

	/**
	 * Controlla se esiste un vincolo di chiave esterna nella tabella indicata.
	 *
	 * @see #existsForeignKey(String, String, String)
	 */
	public boolean existsForeignKey(final @NonNull String table, final @NonNull String constraint) throws SQLException {
		return this.existsForeignKey(this.getDefaultSchema(), table, constraint);
	}

	/**
	 * Controlla se esiste un vincolo di chiave esterna nella tabella indicata.
	 *
	 * @param schema nome dello schema su cui si trova la tabella da analizzare.
	 * @param table nome della tabella da analizzare.
	 * @param constraint nome del vincolo.
	 *
	 * @return {@code true} se esiste il vincolo indicato.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table} o {@code constraint} sono {@code null} o vuoti.
	 */
	public boolean existsForeignKey(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) throws SQLException {
		return this.existsConstraint(schema, table, constraint, ConstraintType.FOREIGN_KEY);
	}

	/**
	 * Crea un vincolo di chiave esterna.
	 *
	 * @see #createForeignKey(String, String, String, String, String)
	 */
	public boolean createForeignKey(final @NonNull String table, final @NonNull String referencedTable, final @NonNull String constraint, final @NonNull String columns, final @NonNull String referencedColumns) throws SQLException {
		final String defaultSchema = this.getDefaultSchema();
		return this.createForeignKey(defaultSchema, table, defaultSchema, referencedTable, constraint, columns, referencedColumns);
	}

	/**
	 * Crea un vincolo di chiave esterna.
	 *
	 * @param schema nome dello schema su cui si trova la tabella da analizzare.
	 * @param table nome della tabella interessata.
	 * @param referencedTableSchema nome dello schema della tabella di riferimento.
	 * @param referencedTable nome della tabella di riferimento.
	 * @param constraint nome del vincolo.
	 * @param columns nomi della colonne della tabella interessata separati da virgola.
	 * @param referencedColumns nomi delle colonne della tabella di riferimento
	 *            separati da virgola.
	 *
	 * @return {@code true} se la chiave è stata creata, {@code false} se la chiave
	 *         esisteva già.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table}, {@code referencedTable}, {@code constraint},
	 *             {@code columns} o{@code referencedColumns} sono {@code null} o vuoti.
	 */
	public boolean createForeignKey(final @NonNull String schema, final @NonNull String table, final @NonNull String referencedTableSchema, final @NonNull String referencedTable, final @NonNull String constraint, final @NonNull String columns, final @NonNull String referencedColumns) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(referencedTableSchema, "referenced table schema");
		checkString(referencedTable, "referenced table");
		checkString(constraint, "constraint name");
		checkString(columns, "column names");
		checkString(referencedColumns, "referenced column names");

		final String constraintFixed = this.fixForeignKeyName(constraint);

		if (this.existsForeignKey(schema, table, constraintFixed)) {
			return false;
		}

		final String query = this.buildCreateForeignKeyStatement(schema, table, referencedTableSchema, referencedTable, constraintFixed, columns, referencedColumns);
		this.executeSql(query);
		return true;
	}

	protected String fixIndexName(final String index) {
		return index;
	}

	protected String fixForeignKeyName(final String constraint) {
		return constraint;
	}

	public boolean renameForeignKey(final @NonNull String table, final @NonNull String constraint, final @NonNull String newName) throws SQLException {
		return this.renameForeignKey(this.getDefaultSchema(), table, constraint, newName);
	}

	/**
	 * Rinomina la foreign key di una tabella.
	 * Se la foreign key non è a livello di tabella, la tabella viene ignorata.
	 * Se la foreign key non cambia nome, viene considerata come rinominata
	 * senza far nulla.
	 *
	 * @param schema nome dello schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param constraint nome della foreign key da rinominare.
	 * @param name nuovo nome della foreign key.
	 *
	 * @return {@code true} se la foreign key è stata rinominata,
	 *         {@code false} se la foreign key non esiste.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se almeno uno dei parametri è {@code null} o vuoto.
	 */
	public boolean renameForeignKey(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final @NonNull String newName) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(constraint, "constraint name");
		checkString(newName, "constraint new name");

		final String constraintFixed = this.fixForeignKeyName(constraint);
		if (!this.existsForeignKey(schema, table, constraintFixed)) {
			return false;
		}
		final String newNameFixed = this.fixForeignKeyName(newName);
		if (newNameFixed.equals(constraintFixed)) {
			return true;
		}
		final String query = this.buildRenameForeignKeyStatement(schema, table, constraintFixed, newNameFixed);
		this.executeSql(query);
		return true;
	}

	/**
	 * Rimuove un vincolo di chiave esterna da una tabella.
	 *
	 * @see #dropForeignKey(String, String, String)
	 */
	public boolean dropForeignKey(final @NonNull String table, final @NonNull String constraint) throws SQLException {
		return this.dropForeignKey(this.getDefaultSchema(), table, constraint);
	}

	/**
	 * Rimuove un vincolo di chiave esterna da una tabella.
	 *
	 * @param schema nome dello schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param constraint nome del vincolo.
	 *
	 * @return {@code true} se il vincolo è stato rimosso, {@code false} se il vincolo
	 *         non esisteva.
	 *
	 * @throws SQLException in caso di errore SQL.
	 * @throws StringNotValorizedException se {@code schema} o {@code table} o {@code constraint} sono {@code null} o vuoti.
	 */
	public boolean dropForeignKey(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(constraint, "constraint name");

		final String constraintFixed = this.fixForeignKeyName(constraint);
		if (!this.existsForeignKey(schema, table, constraintFixed)) {
			return false;
		}

		final String query = this.buildDropForeignKeyStatement(schema, table, constraintFixed);
		this.executeSql(query);
		return true;
	}

	/**
	 * Esegue una query.
	 *
	 * @param connection connessione da utilizzare.
	 * @param query la query da eseguire.
	 *
	 * @return {@code true} se la query ritorna un resultset oppure {@code false} se la query modifica delle righe o non ritorna nulla.
	 */
	public boolean execute(final @NonNull String query) throws SQLException {
		final String parseSql = this.parseSql(query);
		if (this.dryRun) {
			return false;
		}
		return SqlUtils.execute(this.connection, parseSql);
	}

	/**
	 * Esegue una query di inserimento, aggiornamento o cancellazione.
	 *
	 * @param connection connessione da utilizzare.
	 * @param query la query da eseguire.
	 *
	 * @return numero di righe manipolate.
	 */
	public int executeUpdate(final @NonNull String query) throws SQLException {
		final String parseSql = this.parseSql(query);
		if (this.dryRun) {
			return -1;
		}
		return SqlUtils.executeUpdate(this.connection, parseSql);
	}

	/**
	 * Esegue una query di inserimento, aggiornamento o cancellazione.
	 *
	 * @param connection connessione da utilizzare.
	 * @param query la query da eseguire.
	 * @param params parametri della query.
	 *
	 * @return numero di righe manipolate.
	 */
	public int executeUpdate(final @NonNull String query, final Object... params) throws SQLException {
		final String parseSql = this.parseSql(query);
		if (this.dryRun) {
			return -1;
		}
		return SqlUtils.executeUpdate(this.connection, parseSql, params);
	}

	/**
	 * Esegue più query di inserimento, aggiornamento o cancellazione non in transazione.
	 *
	 * @param connection connessione da utilizzare.
	 * @param queries una o più query da eseguire (non in transazione).
	 *
	 * @return numero di righe manipolate.
	 */
	public int executeUpdates(final @NonNull String... queries) throws SQLException {
		if (this.dryRun) {
			for (final String query : queries) {
				this.parseSql(query); // parsed query log
			}
			return -1;
		}
		int affectedRecords = 0;
		if (queries.length > 0) {
			try (final Statement st = this.connection.createStatement()) {
				for (final String query : queries) {
					final String parseSql = this.parseSql(query);
					affectedRecords += st.executeUpdate(parseSql);
				}
			}
		}
		return affectedRecords;
	}

	/**
	 * Esegue più query di inserimento, aggiornamento o cancellazione in transazione.
	 *
	 * @param connection connessione da utilizzare.
	 * @param queries una o più query da eseguire (in transazione).
	 *
	 * @return numero di righe manipolate,
	 */
	public int executeTransactionalUpdates(final @NonNull String... queries) throws SQLException {
		if (this.dryRun) {
			for (final String query : queries) {
				this.parseSql(query); // parsed query log
			}
			return -1;
		}
		final AtomicInteger affectedRecords = new AtomicInteger(0);
		if (queries.length > 0) {
			SqlUtils.transaction(this.connection, c -> {
				try (final Statement st = this.connection.createStatement()) {
					for (final String query : queries) {
						final String parseSql = this.parseSql(query);
						affectedRecords.addAndGet(st.executeUpdate(parseSql));
					}
				}
			});
		}
		return affectedRecords.intValue();
	}

	/**
	 * Esegue una query che restituisce un {@code ResultSet}
	 * e permette di effettuare delle elaborazioni
	 * tramite un'interfaccia consumer.
	 *
	 * @param connection connessione da utilizzare.
	 * @param query la query da eseguire.
	 * @param consumer interfaccia che effettua
	 *            elaborazioni su ogni riga restituita.
	 * @param params parametri della query.
	 */
	public void executeQuery(final @NonNull String query, final @NonNull CheckedConsumer<ResultSet, SQLException> resultSetRowConsumer, final Object... params) throws SQLException {
		SqlUtils.executeQuery(this.connection, this.parseSql(query), resultSetRowConsumer, params);
	}

	public <T> List<T> executeAndGet(final @NonNull String query, final @NonNull CheckedFunction<ResultSet, T, SQLException> rowSupplier, final Object... params) throws SQLException {
		return SqlUtils.executeAndGet(this.connection, this.parseSql(query), rowSupplier, params);
	}

	protected String parseSql(final String sql) {
		if (this.resolvePlaceholders) {
			final String parsedSql = PlaceholderParser.parse(sql, this::resolvePlaceholder);
			LOGGER.info(parsedSql);
			return parsedSql;
		}
		LOGGER.info(sql);
		return sql;
	}

	protected String resolvePlaceholder(final String placeholder, final String... params) {
		final String resolved = this.placeholderResolver.apply(placeholder, params);
		if (resolved != null) {
			return resolved;
		}
		final SqlPlaceholder sqlPlaceholder = SqlPlaceholder.valueOf(placeholder);
		return this.resolvePlaceholder(sqlPlaceholder, params);
	}

	protected abstract String resolvePlaceholder(final SqlPlaceholder sqlPlaceholder, final String[] params);

	/**
	 * Genera l'istruzione SQL per aggiungere una colonna.
	 *
	 * @param schema nome dello schema su cui si trov la tabella.
	 * @param table nome della tabella.
	 * @param column nome della colonna da modificare.
	 * @param definition definizione della colonna (senza NOT NULL).
	 * @param previousColumn colonna precedente (ignorabile).
	 *
	 * @return l'istruzione SQL.
	 *
	 * @see #createColumn(String, String, String, String, String)
	 * @see #createNotNullableColumn(String, String, String, String, String, Object)
	 */
	protected String buildCreateColumnStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final @Nullable String previousColumn) {
		return "alter table " + schema + "." + table + " add " + column + " " + definition;
	}

	/**
	 * Genera l'istruzione SQL per rendere una colonna "NOT NULL".
	 *
	 * @param schema nome dello schema.
	 * @param table nome della tabella.
	 * @param column nome della colonna da modificare.
	 * @param definition definizione della colonna (senza NOT NULL).
	 *
	 * @return l'istruzione SQL.
	 *
	 * @see #createNotNullableColumn(String, String, String, String, String, Object)
	 */
	protected abstract String buildColumnToNotNullStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition);

	/**
	 * Modifica la colonna di una tabella senza verificarne l'esistenza.
	 *
	 * @param schema nome dello schema sui cui si trova la tabella.
	 * @param table nome della tabella interessata.
	 * @param column nome della colonna da modificare.
	 * @param definition nuova definizione della colonna.
	 * @param newName nuovo nome della colonna (facoltativo).
	 *
	 * @throws SQLException in caso di errore SQL.
	 *
	 * @see #editColumn(String, String, String, String, String)
	 */
	protected abstract void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final @Nullable String newName) throws SQLException;

	/**
	 * Modifica la colonna di una tabella senza verificarne l'esistenza.
	 *
	 * @param schema nome dello schema sui cui si trova la tabella.
	 * @param table nome della tabella interessata.
	 * @param column nome della colonna da modificare.
	 * @param definition nuova definizione della colonna
	 *            senza includere null/not null.
	 * @param nullable indica se la colonna è nullabile o meno.
	 * @param newName nuovo nome della colonna (facoltativo).
	 *
	 * @throws SQLException in caso di errore SQL.
	 *
	 * @see #editColumn(String, String, String, String, String)
	 */
	protected abstract void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, boolean nullable, final @Nullable String newName) throws SQLException;

	/**
	 * Genera l'istruzione SQL per rimuovere una colonna.
	 *
	 * @param schema nome dello schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param column nome della colonna da rimuovere.
	 *
	 * @return l'istruzione SQL.
	 *
	 * @see #dropColumn(String, String, String)
	 */
	protected String buildDropColumnStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column) {
		return "alter table " + schema + "." + table + " drop column " + column;
	}

	/**
	 * Genera l'istruzione SQL per la creazione di un indice.
	 *
	 * @param schema nome dello schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param columns nome delle colonne coinvolte separate da virgola.
	 * @param index nome dell'indice da creare.
	 * @param unique indica se l'indice è univoco.
	 *
	 * @return l'istruzione SQL.
	 *
	 * @see #createIndex(String, String, String, String, boolean)
	 */
	protected abstract String buildCreateIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String columns, final @NonNull String index, final boolean unique);

	protected abstract String buildRenameIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index, final @NonNull String name);

	/**
	 * Genera l'istruzione SQL per la rimozione di un indice.
	 *
	 * @param schema schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param index nome dell'indice da rimuovere.
	 *
	 * @return l'istruzione SQL.
	 *
	 * @see #dropIndex(String, String, String)
	 */
	protected abstract String buildDropIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index);

	/**
	 * Genera l'istruzione SQL per la creazione di un vincolo di chiave esterna.
	 *
	 * @param schema nome dello schema su cui si trova la interessata.
	 * @param table nome della tabella interessata.
	 * @param referencedTableSchema nome dello schema della tabella di riferimento.
	 * @param referencedTable nome della tabella di riferimento.
	 * @param constraint nome del vincolo.
	 * @param columns nomi della colonne della tabella interessata separati da virgola.
	 * @param referencedColumns nomi delle colonne della tabella di riferimento
	 *            separati da virgola.
	 *
	 * @return l'istruzione SQL.
	 *
	 * @see #createForeignKey(String, String, String, String, String)
	 */
	protected abstract String buildCreateForeignKeyStatement(
			final @NonNull String schema,
			final @NonNull String table,
			final @NonNull String referencedTableSchema,
			final @NonNull String referencedTable,
			final @NonNull String constraint,
			final @NonNull String columns,
			final @NonNull String referencedColumns);

	protected abstract String buildRenameForeignKeyStatement(
			final @NonNull String schema,
			final @NonNull String table,
			final @NonNull String constraint,
			final @NonNull String name);

	/**
	 * Genera l'istruzione SQL per la rimozione di un vincolo di chiave esterna.
	 *
	 * @param schema nome dello schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param constraint nome del vincolo.
	 *
	 * @return l'istruzione SQL.
	 *
	 * @see #dropForeignKey(String, String, String)
	 */
	protected abstract String buildDropForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint);

	/**
	 * Genera l'istruzione SQL per la rimozione di un vincolo.
	 *
	 * @param schema nome dello schema su cui si trova la tabella interessata.
	 * @param table nome della tabella interessata.
	 * @param constraint nome del vincolo.
	 *
	 * @return l'istruzione SQL.
	 *
	 * @see #dropCheck(String, String, String)
	 */
	protected String buildDropCheckStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) {
		return "alter table " + schema + "." + table + " drop constraint " + constraint;
	}

	/**
	 * Controlla se una colonna è "NOT NULL".
	 *
	 * Il metodo si aspetta sempre tabella e colonna nè {@code null} nè vuoti.
	 *
	 * @param schema nome dello schema dove controllare.
	 * @param table nome della tabella dove controllare.
	 * @param column nome della colonna da verificare.
	 *
	 * @return {@code true} se la colonna indicata non può essere {@code null}.
	 *
	 * @throws SQLException in caso di errore SQL.
	 *
	 * @see #createNotNullableColumn(String, String, String, String, Object)
	 */
	protected abstract boolean isColumnNotNullable(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException;

	protected static void checkString(final String s, final String paramName) {
		StringUtils.requireNonEmpty(s, paramName + " not specified");
	}

	protected final void executeSql(final String sql, final Object... params) throws SQLException {
		final String parsedSql = this.parseSql(sql);

		if (!this.dryRun) {
			try (PreparedStatement ps = SqlUtils.statement(this.connection, parsedSql, params)) {
				ps.execute();
			}
		}
	}

	public enum ConstraintType {

		UNIQUE("UNIQUE"),
		PRIMARY_KEY("PRIMARY KEY"),
		FOREIGN_KEY("FOREIGN KEY"),
		CHECK("CHECK");

		private final String value;

		private ConstraintType(final String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}
	}

	protected String getDefaultSchema() throws SQLException {
		return this.connection.getSchema();
	}

	protected String getCatalog() throws SQLException {
		return this.connection.getCatalog();
	}
}
