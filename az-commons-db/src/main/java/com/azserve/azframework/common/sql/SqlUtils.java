package com.azserve.azframework.common.sql;

import static java.util.Objects.requireNonNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterators;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.ex.Checked;
import com.azserve.azframework.common.ex.CheckedConsumer;
import com.azserve.azframework.common.ex.CheckedFunction;
import com.azserve.azframework.common.lang.StringUtils;

public final class SqlUtils {

	private SqlUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Apre una nuova connessione.
	 *
	 * @param driver driver da utilizzare.
	 * @param url url di connessione al database.
	 * @param user utente del database.
	 * @param password password di autenticazione.
	 */
	public static Connection openConnection(final String driver, final String url, final String user, final String password) throws SQLException, ClassNotFoundException {
		Class.forName(driver);
		return DriverManager.getConnection(url, user, password);
	}

	/**
	 * Esegue una query.
	 *
	 * @param connection connessione da utilizzare.
	 * @param query la query da eseguire.
	 *
	 * @return {@code true} se la query ritorna un resultset oppure {@code false} se la query modifica delle righe o non ritorna nulla.
	 */
	public static boolean execute(final @NonNull Connection connection, final @NonNull String query) throws SQLException {
		try (final Statement st = connection.createStatement()) {
			return st.execute(query);
		}
	}

	/**
	 * Esegue una query di inserimento, aggiornamento o cancellazione.
	 *
	 * @param connection connessione da utilizzare.
	 * @param query la query da eseguire.
	 *
	 * @return numero di righe manipolate.
	 */
	public static int executeUpdate(final @NonNull Connection connection, final @NonNull String query) throws SQLException {
		try (final Statement st = connection.createStatement()) {
			return st.executeUpdate(query);
		}
	}

	/**
	 * Esegue una query di inserimento, aggiornamento o cancellazione.
	 *
	 * @param connection connessione da utilizzare.
	 * @param query la query da eseguire.
	 * @param params parametri della query.
	 *
	 * @return numero di righe manipolate.
	 */
	public static int executeUpdate(final @NonNull Connection connection, final @NonNull String query, final Object... params) throws SQLException {
		try (final PreparedStatement st = statement(connection, query, params)) {
			return st.executeUpdate();
		}
	}

	/**
	 * Esegue più query di inserimento, aggiornamento o cancellazione non in transazione.
	 *
	 * @param connection connessione da utilizzare.
	 * @param queries una o più query da eseguire (non in transazione).
	 *
	 * @return numero di righe manipolate.
	 */
	public static int executeUpdates(final @NonNull Connection connection, final @NonNull String... queries) throws SQLException {
		int affectedRecords = 0;
		if (queries.length > 0) {
			try (final Statement st = connection.createStatement()) {
				for (final String query : queries) {
					affectedRecords += st.executeUpdate(query);
				}
			}
		}
		return affectedRecords;
	}

	/**
	 * Esegue più query di inserimento, aggiornamento o cancellazione in transazione.
	 *
	 * @param connection connessione da utilizzare.
	 * @param queries una o più query da eseguire (in transazione).
	 *
	 * @return numero di righe manipolate,
	 */
	public static int executeTransactionalUpdates(final @NonNull Connection connection, final @NonNull String... queries) throws SQLException {
		final AtomicInteger affectedRecords = new AtomicInteger(0);
		if (queries.length > 0) {
			transaction(connection, c -> {
				try (final Statement st = connection.createStatement()) {
					for (final String query : queries) {
						affectedRecords.addAndGet(st.executeUpdate(query));
					}
				}
			});
		}
		return affectedRecords.intValue();
	}

	/**
	 * Esegue una query che restituisce un {@code ResultSet}
	 * e permette di effettuare delle elaborazioni
	 * tramite un'interfaccia consumer.
	 *
	 * @param connection connessione da utilizzare.
	 * @param query la query da eseguire.
	 * @param consumer interfaccia che effettua
	 *            elaborazioni su ogni riga restituita.
	 * @param params parametri della query.
	 */
	public static void executeQuery(final @NonNull Connection connection, final @NonNull String query, final @NonNull CheckedConsumer<ResultSet, SQLException> resultSetRowConsumer, final Object... params) throws SQLException {
		try (final PreparedStatement ps = statementRO(connection, query, params)) {
			try (final ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					resultSetRowConsumer.accept(rs);
				}
			}
		}
	}

	public static <T> List<T> executeAndGet(final @NonNull Connection conn, final @NonNull String query, final @NonNull CheckedFunction<ResultSet, T, SQLException> rowSupplier, final Object... params) throws SQLException {
		final List<T> data = new ArrayList<>();
		executeQuery(conn, query, rs -> data.add(rowSupplier.apply(rs)), params);
		return data;
	}

	/**
	 * Esegue la query e inserisce i campi concatenati di ogni record in una lista di stringhe.
	 *
	 * @param conn connessione da utilizzare.
	 * @param query la query che dev'essere eseguita.
	 *
	 * @return una lista di stringhe. Ogni stringa è formata dalla concatenazione dei campi
	 *         del record.
	 */
	public static List<String> executeAndGet(final @NonNull Connection conn, final @NonNull String query) throws SQLException {
		final List<String> data = new ArrayList<>();
		executeAndGet(conn, query, data);
		return data;
	}

	/**
	 * Esegue la query e inserisce i campi concatenati di ogni record in una {@code Collection} di stringhe.
	 *
	 * @param conn connessione da utilizzare.
	 * @param query la query che dev'essere eseguita.
	 * @param output collezione nella quale inserire l'output.
	 */
	public static void executeAndGet(final @NonNull Connection conn, final @NonNull String query, final @NonNull Collection<String> output) throws SQLException {
		try (final Statement st = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
			try (final ResultSet rs = st.executeQuery(query)) {
				final int columns = rs.getMetaData().getColumnCount();
				if (columns == 1) {
					while (rs.next()) {
						output.add(rs.getString(1));
					}
				} else {
					// la tabella ha più colonne: concateno i campi usando uno StringBuilder
					while (rs.next()) {
						final StringBuilder sb = new StringBuilder();
						for (int i = 1; i <= columns; i++) {
							sb.append(rs.getString(i)).append(" ");
						}
						output.add(sb.toString().trim());
					}
				}
			}
		}
	}

	/**
	 * Esegue una query restituendo il valore della prima colonna del primo record trovato.
	 * Se la query non restituisce alcun risultato viene restitutito {@code null}.
	 *
	 * @param conn connessione da utilizzare.
	 * @param query la query che dev'essere eseguita.
	 * @param resultClass classe del valore.
	 * @return il valore della prima colonna del primo record.
	 */
	public static <T> T executeAndGetSingleResult(final @NonNull Connection conn, final @NonNull String query, final @NonNull Class<T> resultClass) throws SQLException {
		try (final Statement st = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
			try (final ResultSet rs = st.executeQuery(query)) {
				if (rs.next()) {
					return rs.getObject(1, resultClass);
				}
				return null;
			}
		}
	}

	/**
	 * Esegue la query e restituisce il campo specificato
	 * della prima riga ottenuta sotto forma di stringa.
	 *
	 * @param conn connessione da utilizzare.
	 * @param query query da eseguire.
	 * @param column colonna da leggere.
	 */
	public static String executeAndGetFirst(final @NonNull Connection conn, final @NonNull String query, final @NonNull String column) throws SQLException {
		try (final Statement st = conn.createStatement(); ResultSet rs = st.executeQuery(query)) {
			if (rs.next()) {
				return Objects.toString(rs.getObject(column), null);
			}
			return null;
		}
	}

	/**
	 * Esegue la query e restituisce il campo specificato
	 * della prima riga.
	 *
	 * @param conn connessione da utilizzare.
	 * @param query query da eseguire.
	 * @param params parametri della query.
	 */
	public static Optional<Object> firstObject(final @NonNull Connection connection, final @NonNull String query, final Object... params) throws SQLException {
		try (final PreparedStatement st = statementRO(connection, query, params); ResultSet rs = st.executeQuery()) {
			return rs.next() ? Optional.ofNullable(rs.getObject(1)) : Optional.empty();
		}
	}

	/**
	 * Esegue la query e restituisce il campo specificato
	 * della prima riga ottenuta sotto forma di stringa.
	 *
	 * @param conn connessione da utilizzare.
	 * @param query query da eseguire.
	 * @param params parametri della query.
	 */
	public static Optional<String> firstString(final @NonNull Connection connection, final @NonNull String query, final Object... params) throws SQLException {
		try (final PreparedStatement st = statementRO(connection, query, params); ResultSet rs = st.executeQuery()) {
			return rs.next() ? Optional.ofNullable(rs.getString(1)) : Optional.empty();
		}
	}

	/**
	 * Esegue la query e restituisce il campo specificato
	 * della prima riga ottenuta sotto forma di boolean.
	 *
	 * @param conn connessione da utilizzare.
	 * @param query query da eseguire.
	 * @param params parametri della query.
	 */
	public static Optional<Boolean> firstBoolean(final @NonNull Connection connection, final @NonNull String query, final Object... params) throws SQLException {
		try (final PreparedStatement st = statementRO(connection, query, params); ResultSet rs = st.executeQuery()) {
			if (!rs.next()) {
				return Optional.empty();
			}
			final boolean value = rs.getBoolean(1);
			return rs.wasNull() ? Optional.empty() : Optional.of(Boolean.valueOf(value));
		}
	}

	/**
	 * Prepara uno {@code PreparedStatement} con i parametri specificati
	 * utilizzando {@linkplain PreparedStatement#setObject(int, Object)}.
	 *
	 * @param connection la connessione al database.
	 * @param sql la query da eseguire.
	 * @param params i parametri della query.
	 * @return lo statement pronto per eseguire la query.
	 * @throws SQLException in caso di errori di connessione o di
	 *             posizione dei parametri.
	 */
	public static PreparedStatement statement(final Connection connection, final String sql, final Object... params) throws SQLException {
		final PreparedStatement statement = connection.prepareStatement(sql);
		for (int i = 0; i < params.length; i++) {
			statement.setObject(i + 1, params[i]);
		}
		return statement;
	}

	/**
	 * Prepara uno {@code PreparedStatement} con i parametri specificati
	 * utilizzando {@linkplain PreparedStatement#setObject(int, Object)}.
	 * <p>
	 * Vengono inoltre settati i parametri
	 * {@linkplain ResultSet#TYPE_FORWARD_ONLY} e
	 * {@linkplain ResultSet#CONCUR_READ_ONLY}.
	 * </p>
	 *
	 * @param connection la connessione al database.
	 * @param sql la query da eseguire.
	 * @param params i parametri della query.
	 * @return lo statement pronto per eseguire la query.
	 * @throws SQLException in caso di errori di connessione o di
	 *             posizione dei parametri.
	 */
	public static PreparedStatement statementRO(final Connection connection, final String sql, final Object... params) throws SQLException {
		final PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
		for (int i = 0; i < params.length; i++) {
			statement.setObject(i + 1, params[i]);
		}
		return statement;
	}

	/**
	 * Restituisce la prima riga sotto forma di {@code Optional}
	 * dal {@code ResultSet} ottenuto eseguendo la query tramite
	 * lo statement specificato bloccandolo ad un massimo di un
	 * risultato con {@linkplain PreparedStatement#setMaxRows(int)}.
	 *
	 * @param <T> tipo dell'oggetto.
	 * @param ps statement da cui eseguire la query.
	 * @param mapper funzione che crea un oggetto (non null)
	 *            a partire dal {@code ResultSet}.
	 * @return lo stream sugli oggetti.
	 * @throws SQLException in caso di errore nell'esecuzione della query
	 *             o nella lettura del {@code ResultSet}.
	 * @throws NullPointerException se uno dei parametri in ingresso è {@code null}
	 *             oppure la funzione {@code mapper} restituisce {@code null}.
	 */
	public static <T> Optional<T> first(final @NonNull PreparedStatement ps, final @NonNull CheckedFunction<ResultSet, T, SQLException> mapper) throws SQLException {
		requireNonNull(ps, "Statement is null");
		requireNonNull(mapper, "Mapping function is null");
		ps.setMaxRows(1);
		try (ResultSet rs = ps.executeQuery()) {
			return rs.next()
					? Optional.of(mapper.apply(rs))
					: Optional.empty();
		}
	}

	/**
	 * Genera uno stream di oggetti a partire dal {@code ResultSet} ottenuto
	 * eseguendo la query tramite lo statement specificato.
	 *
	 * @param <T> tipo degli oggetti.
	 * @param ps statement da cui eseguire la query.
	 * @param mapper funzione che crea un oggetto a partire dal {@code ResultSet}.
	 * @return lo stream sugli oggetti.
	 * @throws SQLException in caso di errore nell'esecuzione della query
	 *             o nella lettura del {@code ResultSet}.
	 */
	public static <T> Stream<T> stream(final @NonNull PreparedStatement ps, final @NonNull CheckedFunction<ResultSet, T, SQLException> mapper) throws SQLException {
		requireNonNull(ps, "Statement is null");
		requireNonNull(mapper, "Mapping function is null");
		final ResultSetIterator<T> iterator = new ResultSetIterator<>(ps, mapper);
		return StreamSupport
				.stream(Spliterators.spliteratorUnknownSize(iterator, 0), false)
				.onClose(iterator::close);
	}

	/**
	 * Effettua una query e verifica se restituisce almeno una riga.
	 *
	 * @param connection connessione da utilizzare.
	 * @param query query da eseguire.
	 * @return {@code true} se esiste almeno una riga.
	 */
	public static boolean exists(final @NonNull Connection connection, final @NonNull String query) throws SQLException {
		StringUtils.requireNonEmpty(query, "Query not specified");
		try (final Statement st = connection.createStatement()) {
			try (final ResultSet rs = st.executeQuery(query)) {
				return rs.next();
			}
		}
	}

	/**
	 * Effettua una query e verifica se restituisce almeno una riga.
	 *
	 * @param connection connessione da utilizzare.
	 * @param query query da eseguire.
	 * @param params parametri della query.
	 *
	 * @return {@code true} se esiste almeno una riga.
	 */
	public static boolean exists(final @NonNull Connection connection, final @NonNull String query, final Object... params) throws SQLException {
		StringUtils.requireNonEmpty(query, "Query not specified");
		try (final PreparedStatement st = statementRO(connection, query, params); final ResultSet rs = st.executeQuery()) {
			return rs.next();
		}
	}

	/**
	 * Effettua la "count" sulla tabella specificata e restituisce il numero di record.
	 *
	 * @param connection connessione da utilizzare.
	 * @param table tabella sulla quale effettuare il conto.
	 */
	public static long count(final @NonNull Connection connection, final @NonNull String table) throws SQLException {
		StringUtils.requireNonEmpty(table, "Table not specified");
		try (final Statement st = connection.createStatement()) {
			try (final ResultSet rs = st.executeQuery("select count(*) from " + table)) {
				rs.next();
				return rs.getLong(1);
			}
		}
	}

	/**
	 * Prepara gli argomenti come da esempio:
	 *
	 * <pre>
	 * in("a1") -> "('a1')"
	 * in("a1","a2") -> "('a1','a2')"
	 * in("a1","a2'") -> "('a1','a2''')"
	 * </pre>
	 *
	 * Non accetta valori {@code null}
	 */
	public static @NonNull String in(final @NonNull String... values) {
		if (values.length == 0) {
			throw new IllegalArgumentException("At least one value is required");
		}
		final StringBuilder sb = new StringBuilder();
		sb.append("(");
		for (final String value : values) {
			if (value == null) {
				throw new IllegalArgumentException("value cannot be null");
			}
			sb.append("'").append(value.replace("'", "''")).append("',");
		}
		return sb.substring(0, sb.length() - 1) + ")";
	}

	public static void transaction(final Connection connection, final CheckedConsumer<Connection, SQLException> consumer) throws SQLException {
		final boolean autoCommit = connection.getAutoCommit();
		if (autoCommit) {
			connection.setAutoCommit(false);
		}
		try {
			consumer.accept(connection);
			if (!autoCommit) {
				connection.commit();
			}
		} catch (final Exception e) {
			connection.rollback();
			throw e instanceof SQLException ? (SQLException) e : new SQLException(e);
		} finally {
			if (autoCommit) {
				connection.setAutoCommit(true);
			}
		}
	}

	/**
	 * Inserisce le righe di una tabella X del database D1 nella tabella
	 * Y del database D2 verificando che le colonne siano compatibili.
	 *
	 * La tabella destinazione deve già esistere ed avere la stessa struttura della tabella sorgente
	 *
	 * TODO da provare
	 *
	 * @param sourceConnection connessione sorgente.
	 * @param destConnection connessione della tabella di destinazione.
	 * @param sourceTableName tabella sorgente
	 * @param destTableName tabella destinazione
	 * @throws SQLException in caso di errori di connessione,
	 *             tabelle mancanti
	 *             o colonne non compatibili.
	 */
	public static void copyTable(final Connection sourceConnection, final Connection destConnection, final String sourceTableName, final String destTableName) throws SQLException {
		try (final Statement st1 = sourceConnection.createStatement(); final ResultSet rs1 = st1.executeQuery("select * from " + sourceTableName)) {

			final Map<String, Integer> c1 = new HashMap<>();
			final Map<String, Integer> c2 = new HashMap<>();

			try (final Statement st2 = destConnection.createStatement(); final ResultSet rs2 = st2.executeQuery("select * from " + destTableName)) {
				final ResultSetMetaData metaData1 = rs1.getMetaData();
				final ResultSetMetaData metaData2 = rs2.getMetaData();

				if (metaData1.getColumnCount() != metaData2.getColumnCount()) {
					throw new SQLException("Column count not matching");
				}

				final int columnCount = metaData1.getColumnCount();

				for (int i = 1; i <= columnCount; i++) {
					c1.put(metaData1.getColumnName(i), Integer.valueOf(i));
					c2.put(metaData2.getColumnName(i), Integer.valueOf(i));
				}

				for (final Map.Entry<String, Integer> entry : c1.entrySet()) {
					final String key = entry.getKey();
					final Integer c2Index = c2.get(key);
					if (c2Index == null) {
						throw new SQLException("Column \"" + key + "\" not found in both source and destination tables");
					}
					if (metaData1.getColumnType(entry.getValue().intValue()) != metaData2.getColumnType(c2Index.intValue())) {
						throw new SQLException("Column \"" + key + "\" has different SQL type between tables");
					}
				}
			}

			final List<String> columnNames = new ArrayList<>(c2.keySet());
			final String insertQuery = "insert into " + destTableName + columnNames.stream().collect(Collectors.joining(",", "(", ")")) + " values " + columnNames.stream().map(c -> "?").collect(Collectors.joining(",", "(", ")"));
			try (PreparedStatement ps = destConnection.prepareStatement(insertQuery)) {
				while (rs1.next()) {
					int ix = 1;
					for (final String column : columnNames) {
						ps.setObject(ix++, rs1.getObject(column));
					}
					ps.executeUpdate();
				}
			}
		}
	}

	/**
	 * Prepara gli argomenti come da esempio:
	 *
	 * <pre>
	 * formatArguments() -> "()"
	 * formatArguments("id") -> "(id)"
	 * formatArguments("id","cognome") -> "(id,cognome)"
	 * </pre>
	 */
	public static @NonNull String formatArguments(final @NonNull String... args) {
		if (args.length == 0) {
			return "()";
		}
		final StringBuilder sb = new StringBuilder();
		sb.append("(");
		for (final String arg : args) {
			sb.append(arg).append(",");
		}
		return sb.substring(0, sb.length() - 1) + ")";
	}

	/**
	 *
	 * Legge la prossima riga del result set, se presente: <br />
	 * applica la funzione {@code update}, successivamente {@code insertUpdate} e infine aggiorna la riga con {@link ResultSet#updateRow()}; <br />
	 * se assente <br />
	 * applica la funzione {@code insert}, successivamente {@code insertUpdate} e infine inserisce la riga con {@link ResultSet#insertRow()}; <br />
	 *
	 * @param rs
	 * @param insert
	 * @param insertUpdate
	 * @param update
	 * @return true se è avvenuto un inserimento, false se è avvenuto un aggiornamento
	 * @throws SQLException
	 */
	public static boolean insertOrUpdate(final ResultSet rs, final CheckedConsumer<ResultSet, SQLException> insert, final CheckedConsumer<ResultSet, SQLException> insertUpdate, final CheckedConsumer<ResultSet, SQLException> update) throws SQLException {
		boolean isInsert = false;
		if (!rs.next()) {
			isInsert = true;
			rs.moveToInsertRow();
			insert.accept(rs);
		} else {
			update.accept(rs);
		}
		insertUpdate.accept(rs);
		if (isInsert) {
			rs.insertRow();
		} else {
			rs.updateRow();
		}
		return isInsert;
	}

	private static final class ResultSetIterator<T> implements Iterator<T> {

		private final ResultSet rs;
		private final CheckedFunction<ResultSet, T, SQLException> mapper;

		ResultSetIterator(final PreparedStatement ps, final CheckedFunction<ResultSet, T, SQLException> mapper) throws SQLException {
			this.rs = ps.executeQuery();
			this.mapper = mapper;
		}

		@Override
		public boolean hasNext() {
			try {
				final boolean hasNext = this.rs.next();
				if (!hasNext) {
					this.close();
				}
				return hasNext;
			} catch (final SQLException ex) {
				this.close();
				Checked.sneakyThrow(ex);
				return false;
			}
		}

		@Override
		public T next() {
			try {
				return this.mapper.apply(this.rs);
			} catch (final SQLException ex) {
				this.close();
				Checked.sneakyThrow(ex);
			}
			return null;
		}

		void close() {
			try {
				if (!this.rs.isClosed()) {
					this.rs.close();
				}
			} catch (@SuppressWarnings("unused") final SQLException ex) {
				// non possiamo farci nulla
			}
		}
	}
}
