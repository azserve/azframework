package com.azserve.azframework.common.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe che si occupa di fornire una connessione jdbc statica.
 */
public final class StaticConnection {

	private static StaticConnection staticInstance;

	public static Connection get() {
		checkInit();
		return staticInstance.connection;
	}

	/**
	 * Apre la connessione con i parametri specificati.
	 */
	public static void init(final String driver, final String url, final String user, final String password) throws SQLException, ClassNotFoundException {
		if (staticInstance != null) {
			throw new IllegalStateException("Connection already initialized");
		}
		staticInstance = new StaticConnection(driver, url, user, password);
	}

	public static synchronized void openNew() throws ClassNotFoundException, SQLException {
		checkInit();
		staticInstance.openNewConnection();
	}

	public static synchronized void closeCurrent() {
		checkInit();
		staticInstance.closeCurrentConnection();
	}

	private static void checkInit() {
		if (staticInstance == null) {
			throw new IllegalStateException("Connection not initialized yet");
		}
	}

	private final String driver;
	private final String url;
	private final String user;
	private final String password;

	private Connection connection;

	private StaticConnection(final String driver, final String url, final String user, final String password) throws SQLException, ClassNotFoundException {
		this.driver = driver;
		this.url = url;
		this.user = user;
		this.password = password;

		this.openNewConnection();
	}

	/**
	 * Apre una nuova connessione.
	 */
	private void openNewConnection() throws SQLException, ClassNotFoundException {
		Class.forName(this.driver);
		// chiudo la vecchia connessione se aperta
		this.closeCurrentConnection();
		// apro una nuova connesione
		this.connection = DriverManager.getConnection(this.url, this.user, this.password);
	}

	/**
	 * Chiude la connessione corrente.
	 */
	private void closeCurrentConnection() {
		try {
			if (this.connection != null && !this.connection.isClosed()) {
				this.connection.close();
			}
		} catch (@SuppressWarnings("unused") final Exception ex) {}
	}
}
