package com.azserve.azframework.common.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.sql.placeholder.DateAddDays;
import com.azserve.azframework.common.sql.placeholder.SqlPlaceholder;

public class DerbyOperator extends DatabaseOperator {

	public DerbyOperator(final Connection connection) {
		super(connection);
	}

	public DerbyOperator() {}

	@Override
	protected int getPriority(final Connection connection) {
		return connection.getClass().getName().startsWith("org.apache.derby") ? 0 : -1;
	}

	@Override
	public boolean existsIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(index, "index");
		// https://db.apache.org/derby/docs/10.9/ref/rrefsistabs39391.html
		return SqlUtils.exists(this.connection,
				"SELECT c.CONGLOMERATENAME"
						+ " from SYS.SYSCONGLOMERATES c"
						+ " join SYS.SYSSCHEMAS s on c.SCHEMAID = s.SCHEMAID"
						+ " join SYS.SYSTABLES t on c.TABLEID = c.TABLEID"
						+ " where lower(s.SCHEMANAME) = ? and lower(t.TABLENAME) = ? and lower(c.CONGLOMERATENAME) = ? and c.ISINDEX",
				schema.toLowerCase(), table.toLowerCase(), index.toLowerCase());
	}

	@Override
	protected String buildColumnToNotNullStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition) {
		return "ALTER TABLE " + table + " alter column " + column + " not null";
	}

	@Override
	protected void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final @Nullable String newName) throws SQLException {
		// modifica colonna
		this.executeSql(new StringBuilder()
				.append("alter table ")
				.append(schema).append(".").append(table)
				.append(" alter column ").append(column)
				.append(" SET DATA TYPE ").append(definition)
				.toString());
		// poi rinomina
		if (newName != null && !column.equalsIgnoreCase(newName)) {
			this.executeSql("RENAME COLUMN " + schema + "." + table + "." + column + " to " + newName);
		}
	}

	@Override
	protected void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final boolean nullable, @Nullable final String newName) throws SQLException {
		this.editExistentColumn(schema, table, column, definition, newName);
		this.executeSql(String.format("alter table %s.%s alter column %s %s", schema, table, column, nullable ? " null" : " not null"));
	}

	@Override
	protected String buildCreateIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String columns, final @NonNull String index, final boolean unique) {
		return "create " + (unique ? "unique " : "") + "index " + index + " on " + schema + "." + table + "(" + columns + ")";
	}

	@Override
	protected String buildRenameIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index, final @NonNull String name) {
		return "rename index " + index + " to " + name;
	}

	@Override
	protected String buildDropIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index) {
		return "drop index " + index;
	}

	@Override
	public boolean renamePrimaryKey(final @NonNull String schema, final @NonNull String table, final @NonNull String newName) throws SQLException {
		LOGGER.warn("Derby doesn't support Primary Key renaming");
		return false;
	}

	@Override
	protected String buildRenamePrimaryKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String oldName, final @NonNull String newName) {
		throw new UnsupportedOperationException("Derby doesn't support Primary Key renaming");
	}

	@Override
	protected String buildCreateForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String referencedTableSchema, final @NonNull String referencedTable, final @NonNull String constraint, final @NonNull String columns, final @NonNull String referencedColumns) {
		return new StringBuilder()
				.append("alter table ")
				.append(schema).append(".").append(table)
				.append(" add constraint ").append(constraint)
				.append(" foreign key(").append(columns)
				.append(") references ").append(referencedTableSchema).append(".").append(referencedTable)
				.append("(").append(referencedColumns).append(")")
				.toString();
	}

	@Override
	public boolean renameForeignKey(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final @NonNull String newName) throws SQLException {
		final List<String> columnsRef = new ArrayList<>();
		final List<String> columns = new ArrayList<>();
		String tableRef = null;
		String schemaRef = null;
		try (ResultSet rs = this.connection.getMetaData().getCrossReference(this.connection.getCatalog(), schema, table, null, null, null)) {
			while (rs.next()) {
				if (constraint.equalsIgnoreCase(rs.getString(12))) {
					columns.add(rs.getString(4));
					if (schemaRef != null) {
						schemaRef = rs.getString(6);
					}
					if (tableRef != null) {
						tableRef = rs.getString(7);
					}
					columnsRef.add(rs.getString(8));
				}
			}
		}
		if (schemaRef == null || tableRef == null) {
			return false;
		}
		if (!this.dropForeignKey(schema, table, constraint)) {
			return false;
		}
		return this.createForeignKey(schema, table, schemaRef, tableRef, newName, String.join(",", columns), String.join(",", columnsRef));
	}

	@Override
	protected String buildRenameForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final @NonNull String name) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected String buildDropForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) {
		return "alter table " + schema + "." + table + "  drop foreign key " + constraint;
	}

	@Override
	public boolean existsColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(column, "column");
		return SqlUtils.exists(this.connection,
				"SELECT COLUMNNAME"
						+ " from sys.SYSCOLUMNS c"
						+ " join sys.SYSTABLES t on c.REFERENCEID = t.TABLEID"
						+ " join sys.SYSSCHEMAS s on t.SCHEMAID = s.SCHEMAID"
						+ " where lower(s.SCHEMANAME) = ? and lower(t.TABLENAME) = ? and lower(COLUMNNAME) = ?",
				schema.toLowerCase(), table.toLowerCase(), column.toLowerCase());
	}

	@Override
	public boolean existsConstraint(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final ConstraintType type) throws SQLException {
		checkString(schema, "schema");
		checkString(table, "table");
		checkString(constraint, "constraint");
		String constraintType = null;
		switch (type) {
			case FOREIGN_KEY:
				constraintType = "F";
				break;
			case PRIMARY_KEY:
				constraintType = "P";
				break;
			case CHECK:
				constraintType = "C";
				break;
			case UNIQUE:
				throw new UnsupportedOperationException();
			default:
				throw new IllegalArgumentException("Invalid value " + type);

		}
		return SqlUtils.exists(this.connection,
				"SELECT CONSTRAINTNAME"
						+ " from sys.SYSCONSTRAINTS c"
						+ " join sys.SYSTABLES t on c.TABLEID = t.TABLEID"
						+ " join sys.SYSSCHEMAS s on t.SCHEMAID = s.SCHEMAID"
						+ " where lower(s.SCHEMANAME) = ? and lower(t.TABLENAME) = ? and lower(CONSTRAINTNAME) = ? and TYPE = ? and STATE = 'E'",
				schema.toLowerCase(), table.toLowerCase(), constraint.toLowerCase(), constraintType);
	}

	@Override
	protected boolean isColumnNotNullable(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException {
		// I can't find better method
		try (Statement st = this.connection.createStatement()) {
			st.setMaxRows(0);
			try (ResultSet rs = st.executeQuery("select " + column + " from " + schema + "." + table)) {
				return rs.getMetaData().isNullable(1) == ResultSetMetaData.columnNoNulls;
			}
		}
	}

	@Override
	protected String resolvePlaceholder(final SqlPlaceholder sqlPlaceholder, final String[] params) {
		switch (sqlPlaceholder) {
			case AUTOINCREMENT:
				return "INTEGER GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)";
			case BIT:
			case BOOLEAN:
				return "BOOLEAN";
			case BYTES:
				return "BLOB(2147483647)";
			case DATETIME:
				return "TIMESTAMP";
			case TEXT:
				return "LONG VARCHAR";
			case UUID:
				return "CHAR(16) FOR BIT DATA"; // qui sorge un problema: nelle insert/update/where il dato non può essere indicato come stringa
			case TRUE:
				return "true";
			case FALSE:
				return "false";
			case FN_NOW:
				return "CURRENT_TIMESTAMP";
			case FN_DATEADDDAYS:
				// non testato: https://db.apache.org/derby/docs/10.15/ref/rrefjdbc88908.html
				final DateAddDays dateAddDays = new DateAddDays(params);
				return String.format(" {fn TIMESTAMPADD(SQL_TSI_DAY, %s, %s)}", dateAddDays.getDays(), dateAddDays.getDate());
			default:
				break;
		}
		throw new UnsupportedOperationException("Unknown placeholder " + sqlPlaceholder);
	}
}
